
(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })

    $('.input200').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).find('i').removeClass('zmdi-eye');
            $(this).find('i').addClass('zmdi-eye-off');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).find('i').addClass('zmdi-eye');
            $(this).find('i').removeClass('zmdi-eye-off');
            showPass = 0;
        }
        
    });


    const modalService = () => {
        const d = document;
        const body = d.querySelector('body');
        const buttons = d.querySelectorAll('[data-modal-trigger]');
        
        // attach click event to all modal triggers
        for(let button of buttons) {
          triggerEvent(button);
        }
        
        function triggerEvent(button) {
          button.addEventListener('click', () => {
            const trigger = button.getAttribute('data-modal-trigger');
            const modal = d.querySelector(`[data-modal=${trigger}]`);
            const modalBody = modal.querySelector('.modal-body');
            const closeBtn = modal.querySelector('.cancel');
            
            closeBtn.addEventListener('click', () => modal.classList.remove('is-open'))
            modal.addEventListener('click', () => modal.classList.remove('is-open'))
            
            modalBody.addEventListener('click', (e) => e.stopPropagation());
      
            modal.classList.toggle('is-open');
            
            // Close modal when hitting escape
            body.addEventListener('keydown', (e) => {
              if(e.keyCode === 27) {
                modal.classList.remove('is-open')
              }
            });
          });
        }
      }
      
      modalService();



});