// TODO IMPORTACIONES
import BedRooms from "../../../assets/js/bedrooms.js";
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? GET ID OF HOUSE TO EDIT
const ID_HOME = D.getElementById("homestay-id").value;
// ? INSTANCIAS
const _Rooms = new BedRooms();
const _Messages = new Messages();

// TODO FUNCIONES

/** // ? REPLACE BUTTON BY THIRD BED (PLUS) SIGN IF ITS VALUE IS EMPTY
 * @param {Element} select Elemento select a reemplazar por el icono +
 */
const fixBedType = (select) => {
  if (select.value === "Share") {
    const $lastBed =
      select.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("#bed-type")[2];

    if ($lastBed.value === "NULL") {
      $lastBed.className = "px-2";
      $lastBed.parentElement.parentElement.parentElement.classList.remove("more");
    }
  }
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await _Rooms.homestayRooms(ID_HOME);
  D.querySelectorAll("#type-room").forEach((select) => fixBedType(select));

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", async (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }

  if (e.target.matches("#save-changes")) {
    if (D.querySelectorAll("[data-room]").length >= 0) {
      // ? GO TO SAVE BEDROOMS
      _Messages.showMessage("Please Wait...", 1, false);
      await _Rooms.saveBedrooms(D.querySelectorAll("[data-room]"), ID_HOME);
      _Messages.quitMessage();
    }
  }
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  // ? VALIDATION INPUT PRICE
  if (e.target.matches(".edit-price input")) {
    if (/[^0-9$]/gi.test(e.target.value)) e.target.value = e.target.value.substr(0, e.target.value.length - 1);
    else e.target.style.borderColor = "";
  }
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? REMOVE DESIGN RED TO THE SELECTS
  D.querySelectorAll(".invalid").forEach((elem) => elem.classList.remove("invalid"));

  // ? PREVIEW IMAGES
  if (e.target.matches("#house-gallery input") || e.target.matches(".carousel-item.active input")) {
    const $currentImage = e.target.parentElement.querySelector("label img");

    const Reader = new FileReader();
    Reader.readAsDataURL(e.target.files[0]);
    Reader.onload = (e) => ($currentImage.src = e.target.result);
  }
});
