export const MAP = ((e) => {
  const D = document;
  const W = window;

  const $dir = D.querySelectorAll("code")[0].textContent;
  const $city = D.querySelectorAll("code")[1].textContent;
  const $state = D.querySelectorAll("code")[2].textContent;
  const $p_code = D.querySelectorAll("code")[3].textContent;

  mapboxgl.accessToken = "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg";
  var client = new MapboxClient(mapboxgl.accessToken);

  var address = `${$dir}, ${$city}, ${$state} ${$p_code}`;
  var test = client.geocodeForward(address, function (err, data, res) {
    var coordinates = data.features[0].center;

    var map = new mapboxgl.Map({
      container: "map",
      style: "mapbox://styles/mapbox/streets-v10",
      center: coordinates,
      zoom: 14,
    });
    new mapboxgl.Marker().setLngLat(coordinates).addTo(map);
  });
})();
