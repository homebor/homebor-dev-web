function Register()
{
    var title = $("#title").val();
    var color = $("#color").val();
    var start = $("#start").val();
    var end = $("#end").val();
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "addEvent.php",
        data: "title="+title+"&color="+color+"&start="+start+"&end="+end,
        success: function(resp){
            Limpiar();
        }
    });
}   

function Limpiar()
{
    $("#title").val("");
    $("#color").val("");
    $("#start").val("");
    $("#end").val("");
}
