$(document).ready(function (){

  load_data(1);

  function load_data(page, query = '')
  {
    $.ajax({
      url:"fetch_voucher.php",
      method:"POST",
      data:{page:page, query:query},
      success:function(data)
      {
        $('#dynamic_content').html(data);
      }
    });
  }

  $('#vou').hide();

  $('#search_box').keyup(function(){

    if($('#search_box').val()) {

      $('#dynamic_content').hide();
      $('#vou').show();

      let search = $('#search_box').val();

      let mail = $('#mail').val();

      var searchLength = search.length;

      $.ajax({
        url: 'vou-search.php',
        type: 'POST',
        data: {search, mail},
        success: function(response){
          let vouchers = JSON.parse(response);
          let template = '';

          vouchers.forEach(vouche => {
            template += `
            <tr>

              <th style="font-weight:normal"> ${vouche.id_v} </th>
              <th style="font-weight:normal"> ${vouche.dates} </th>
              <th style="font-weight:normal"> ${vouche.title} </th>
              <th style="font-weight:normal"> ${vouche.sender} </th>
              <th style="font-weight:normal"> ${vouche.user} </th>
              <th>
                <a id="edit" href="voucher/${vouche.link}?art_id=${vouche.id_v}" class="btn btn-primary btn-lg float-right" name="edit" style="color: white; background-color: #232159; border: 2px solid #232159; font-size: 14px;" target="_blank">
                  <i class="fa fa-eye mr-2"></i>      
                    See Document
                </a></th>

            </tr>`
          });
          
          
          $('#vouchers').html(template);
        }
      });
    }else{ $('#dynamic_content').show();
      $('#vou').hide();}

  });


});