<link rel="stylesheet" href="assets/css/notification.css">

<?php include 'delete_reserve' ?>

<nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
  <div class="container" style="list-style: none;">

    <!--Brand Logo/ Logo HOMEBOR-->
    <a class="navbar-brand" href="index">
      <img class="logo" src="../assets/logos/page.png" alt="">
    </a>

    <style>
    .img-stu {
      width: 2.5em;
      height: auto;
    }
    </style>

    <?php include 'notification-after.php'; ?>

    <!--Responsive Collapse Button-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary"
      aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!--Collapsing Navigation-->
    <div class="collapse navbar-collapse" id="navbarPrimary">

      <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
      <ul class="navbar-nav">

        <!--Index (Main level)
                        =============================================================================================-->
        <li class="nav-item">
          <a class="nav-link bar-i" href="index" style="color: #982A72;">Calendar</a>
        </li>
        <!--end Index nav-item-->
        <!--Rooms Information (Main level)
                        =============================================================================================-->
        <li class="nav-item">
          <a class="nav-link bar-i" href="rooms" style="color: #982A72;">Rooms</a>
        </li>
        <!--end Rooms Information nav-item-->

        <!-- Status -->
        <li class="nav-item ts-has-child">
          <a class="nav-link bar-i" href="#" style="color: #982A72;">Status</a>
          <span class="sr-only">(current)</span>

          <!-- List (1st level) -->
          <ul class="ts-child ts-child-ii">

            <!-- Voucher (1st level)
                                =====================================================================================-->
            <li class="nav-item">

              <a class="nav-link" href="voucher.php" style="color: #982A72;">
                <p class="bar-ii zoom"> Voucher </p>
              </a>
            </li>

            <li class="nav-item">

              <a href="reports" class="nav-link">
                <p class="bar-ii zoom"> Reports </p>
              </a>
            </li>

            <li class="nav-item">

              <a class="nav-link" href="homestay_payments" style="color: #982A72;">
                <p class="bar-ii zoom"> Payments </p>
              </a>
            </li>
          </ul>
        </li>

        <!--Host (Main level)
                        =============================================================================================-->
        <li class="nav-item ts-has-child">
          <a class="nav-link bar-i" href="#" style="color: #982A72;">Configuration</a>
          <span class="sr-only">(current)</span>

          <!-- List (1st level) -->
          <ul class="ts-child ts-child-ii">

            <!-- Certificated Voucher (1st level)
                                =====================================================================================-->
            <li class="nav-item">

              <a href="edit-rooms" class="nav-link">
                <p class="bar-ii zoom"> Edit Rooms </p>
              </a>
            </li>

            <li class="nav-item">

              <a href="edit-property" class="nav-link">
                <p class="bar-ii zoom"> Edit Homestay </p>
              </a>
            </li>

            <!-- <li class="nav-item">

              <a href="delete_property" class="nav-link">
                <p class="bar-ii zoom"> Disable Account </p>
              </a>
            </li> -->

          </ul>
        </li>
        <!--end Host nav-item-->


        <?php include 'notification-before.php' ?>




      </ul>
      <!--end Left navigation main level-->

      <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
      <ul class="navbar-nav ml-auto">

        <li class="nav-item lo">
          <a class="nav-link" href="../logout" style="color: #982A72;">
            <p class="bar-i"> Logout </p>
          </a>
        </li>

      </ul>
      <!--end Right navigation-->

    </div>
    <!--end navbar-collapse-->
  </div>
  <!--end container-->
  <input type="hidden" id="type_user" value="homestay">

</nav>
<!--end #ts-primary-navigation.navbar-->

<script src="../assets/js/control_events.js?ver=1.0"></script>