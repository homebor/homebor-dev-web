<?php
include '../xeon.php';
error_reporting(0);
session_start();

date_default_timezone_set("America/Toronto");
$date = date('YmdHisv');

$usuario = $_SESSION['username'];
$homestayId = $_POST['homestay_id'];

$queryHome = $link->query("SELECT * FROM pe_home WHERE id_home = '$homestayId' ");
$row_home = $queryHome->fetch_assoc();


if (isset($_POST['basicInformation'])) {
  // TODO SAVE BASIC INFORMATION
  $houseType = $_POST['houseType'];
  $mainCity = $_POST['mainCity'];
  $houseName = $_POST['houseName'];
  $phoneNumber = $_POST['phoneNumber'];
  $totalRooms = $_POST['totalRooms'];
  $address = $_POST['address'];
  $city = $_POST['city'];
  $state = $_POST['state'];
  $postalCode = $_POST['postalCode'];

  $basicInformationQuery = "UPDATE pe_home SET h_name='$houseName',num='$phoneNumber', room ='$totalRooms',m_city='$mainCity',dir='$address',city='$city', state = '$state', p_code='$postalCode', h_type = '$houseType' WHERE id_home = '$homestayId' ";
  $basicInformationUpdate = $link->query($basicInformationQuery);
  $controlQuery = "UPDATE propertie_control SET h_name='$houseName', room ='$totalRooms',dir='$address',city='$city' WHERE id_home = '$homestayId' ";
  $controlUpdate = $link->query($controlQuery);

  $jsonToSend = array(
    'houseType' => $houseType,
    'mainCity' => $mainCity,
    'houseName' => $houseName,
    'phoneNumber' => $phoneNumber,
    'totalRooms' => $totalRooms,
    'address' => $address,
    'city' => $city,
    'state' => $state,
    'postalCode' => $postalCode,

    'result' => $basicInformationUpdate
  );
  echo json_encode($basicInformationUpdate);
} else if (isset($_POST['houseGallery'])) {
  // TODO SAVE HOUSE DETAILS
  $GLOBALS['resultHouseGallery'] = null;

  function insertImage($image, $dbRow, $link, $folderName, $idHome)
  {
    $imageName = $image['name'];
    $imageTMP = $image['tmp_name'];
    $destination = "public/$folderName/$imageName";

    $getCurrentImage = $link->query("SELECT * FROM photo_home WHERE id_home = '$idHome' ");
    $row_image = $getCurrentImage->fetch_assoc();

    if ($dbRow == "phome") {
      $getFrontageImage = $link->query("SELECT * FROM pe_home WHERE id_home = '$idHome' ");
      $row_frontageImage = $getFrontageImage->fetch_assoc();

      $updateImageQuery = "UPDATE pe_home SET phome = '$destination' WHERE id_home = '$idHome'  ";
      $updateImageQuery2 = "UPDATE propertie_control SET photo = '$destination' WHERE id_home = '$idHome'  ";
    } else {
      $updateImageQuery = "UPDATE photo_home SET $dbRow = '$destination' WHERE id_home = '$idHome' ";
    }


    if (!file_exists("../public/$folderName")) mkdir("../public/$folderName");
    $upload = move_uploaded_file($imageTMP, "../$destination");

    if ($upload) {
      $updateImage = $link->query($updateImageQuery);
      if (isset($updateImageQuery2)) $updateImage2 = $link->query($updateImageQuery2);
      if (isset($row_frontageImage)) unlink("../$row_frontageImage[phome]");
      unlink("../$row_image[$dbRow]");

      $GLOBALS['resultHouseGallery'][] = [$imageName, "OK"];
    } else $GLOBALS['resultHouseGallery'][] = [$imageName, "FAIL"];
  }

  $frontage = $_FILES['frontage'];
  $livingRoom = $_FILES['livingRoom'];
  $familyPicture = $_FILES['familyPicture'];
  $kitchen = $_FILES['kitchen'];
  $diningRoom = $_FILES['diningRoom'];
  $houseArea3 = $_FILES['houseArea3'];
  $houseArea4 = $_FILES['houseArea4'];
  $bathroom1 = $_FILES['bathroom1'];
  $bathroom2 = $_FILES['bathroom2'];
  $bathroom3 = $_FILES['bathroom3'];
  $bathroom4 = $_FILES['bathroom4'];

  $frontage['name'] = "frontage-photo-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $livingRoom['name'] = "living-room-photo-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $familyPicture['name'] = "family-picture-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $kitchen['name'] = "kitchen-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $diningRoom['name'] = "dining-room-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $houseArea3['name'] = "house-area-3-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $houseArea4['name'] = "house-area-4-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom1['name'] = "bathroom-1-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom2['name'] = "bathroom-2-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom3['name'] = "bathroom-3-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom4['name'] = "bathroom-4-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));


  // ? HOUSE GALLERY
  if (isset($frontage)) insertImage($frontage, "phome", $link,  $row_home['mail_h'], $homestayId);
  if (isset($livingRoom)) insertImage($livingRoom, "pliving", $link,  $row_home['mail_h'], $homestayId);
  if (isset($familyPicture)) insertImage($familyPicture, "fp", $link,  $row_home['mail_h'], $homestayId);

  if (isset($kitchen)) insertImage($kitchen, "parea1", $link,  $row_home['mail_h'], $homestayId);
  if (isset($diningRoom)) insertImage($diningRoom, "parea2", $link,  $row_home['mail_h'], $homestayId);
  if (isset($houseArea3)) insertImage($houseArea3, "parea3", $link,  $row_home['mail_h'], $homestayId);
  if (isset($houseArea4)) insertImage($houseArea4, "parea4", $link,  $row_home['mail_h'], $homestayId);

  if (isset($bathroom1)) insertImage($bathroom1, "pbath1", $link,  $row_home['mail_h'], $homestayId);
  if (isset($bathroom2)) insertImage($bathroom2, "pbath2", $link,  $row_home['mail_h'], $homestayId);
  if (isset($bathroom3)) insertImage($bathroom3, "pbath3", $link,  $row_home['mail_h'], $homestayId);
  if (isset($bathroom4)) insertImage($bathroom4, "pbath4", $link,  $row_home['mail_h'], $homestayId);

  echo json_encode($GLOBALS['resultHouseGallery']);
} else if (isset($_POST['additionalInformation'])) {
  // TODO SAVE ADDITIONAL INFORMATION
  $queryAdditionalInformation =
    "UPDATE pe_home SET 
      des ='$_POST[description]', 
      a_pre= '$_POST[academy]', 
      g_pre= '$_POST[gender]', 
      ag_pre= '$_POST[age]', 
      smoke= '$_POST[smokers]', 
      vegetarians= '$_POST[vegetarians]', 
      halal= '$_POST[halal]', 
      kosher= '$_POST[kosher]', 
      lactose= '$_POST[lactose]', 
      gluten= '$_POST[gluten]', 
      pork= '$_POST[pork]',  
      none= 'NULL',
      m_service = '$_POST[meals]', 
      y_service= '$_POST[totalYears]', 
      pet= '$_POST[pet]', 
      pet_num= '$_POST[totalPets]', 
      type_pet= '$_POST[specifyPets]', 
      dog= '$_POST[dog]', 
      cat= '$_POST[cat]', 
      other= '$_POST[other]'
      WHERE id_home = '$homestayId' ";
  $updateAdditionalInformation = $link->query($queryAdditionalInformation);
  $controlQuery =
    "UPDATE propertie_control SET 
      g_pre= '$_POST[gender]', 
      ag_pre= '$_POST[age]', 
      pet= '$_POST[pet]', 
      smoke= '$_POST[smokers]', 
      vegetarians= '$_POST[vegetarians]', 
      halal= '$_POST[halal]', 
      kosher= '$_POST[kosher]', 
      lactose= '$_POST[lactose]', 
      gluten= '$_POST[gluten]', 
      pork= '$_POST[pork]',  
      none= 'NULL'
      WHERE id_home = '$homestayId' ";
  $controlUpdate = $link->query($controlQuery);

  if ($updateAdditionalInformation == true) $status = "ok";
  else $status = 'fail';

  $resultAdditionalInformation = array(
    'status' => $status,
    'result' => array(
      'description' => $_POST['description'],
      'academy' => $_POST['academy'],
      'gender' => $_POST['gender'],
      'age' => $_POST['age'],
      'smokers' => $_POST['smokers'],
      'meals' => $_POST['meals'],
      'totalYears' => $_POST['totalYears'],

      'pet' => $_POST['pet'],
      'totalPets' => $_POST['totalPets'],
      'dog' => $_POST['dog'],
      'cat' => $_POST['cat'],
      'other' => $_POST['other'],
      'specifyPets' => $_POST['specifyPets'],

      'vegetarians' => $_POST['vegetarians'],
      'halal' => $_POST['halal'],
      'kosher' => $_POST['kosher'],
      'lactose' => $_POST['lactose'],
      'gluten' => $_POST['gluten'],
      'pork' => $_POST['pork'],
    ),
  );

  echo json_encode($resultAdditionalInformation);
} else if (isset($_POST['familyInformation'])) {
  // TODO SAVE FAMILY INFORMATION
  $queryUpdateFamilyInformation = "UPDATE pe_home SET 
    allergies = '$_POST[allergies]',
    medic_f = '$_POST[medications]',
    health_f = '$_POST[healthProblems]',
    name_h= '$_POST[nameContact]',
    l_name_h = '$_POST[lastNameContact]',
    db = '$_POST[dateBirthContact]',
    gender = '$_POST[genderContact]',
    cell = '$_POST[phoneContact]',
    occupation_m = '$_POST[occupationContact]',
    db_law = '$_POST[dateBackgroundCheckContact]',
    num_mem = '$_POST[totalMembers]',
    backg = '$_POST[background]',
    backl = '$_POST[backgroundLanguage]',
    religion = '$_POST[religion]',
    condition_m = '$_POST[condition]',
    misdemeanor = '$_POST[misdemeanor]',
    c_background = '$_POST[finalQuestion]' WHERE id_home = '$_POST[homestay_id]'  ";
  $updateFamilyInformation = $link->query($queryUpdateFamilyInformation);

  if ($updateFamilyInformation) $status = 'ok';
  else $status = 'fail';


  if (isset($_FILES['backgroundCheck'])) {
    $pdf = $_FILES['backgroundCheck'];
    $pdf['name'] = "background-check-main-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
    $folderName = $_POST['mail'];

    $updateBackgroundCheck = "UPDATE pe_home SET law = 'public/$folderName/$pdf[name]' WHERE id_home = '$_POST[homestay_id]'  ";

    if (!file_exists("../public/$folderName")) mkdir("../public/$folderName");
    $upload = move_uploaded_file($pdf['tmp_name'], "../public/$folderName/$pdf[name]");

    if ($upload) {
      $updateFile = $link->query($updateBackgroundCheck);

      $resultFile = [$pdf['name'], "OK"];
    } else $resultFile = [$pdf['name'], "FAIL"];
  } else $resultFile = ["Background Check Main", "NOT FOUND"];


  $resultFamilyInformation = array(
    'status' => $status,
    'result' => array(
      'allergies' => $_POST['allergies'],
      'medications' => $_POST['medications'],
      'healthProblems' => $_POST['healthProblems'],
      'nameContact' => $_POST['nameContact'],
      'lastNameContact' => $_POST['lastNameContact'],
      'dateBirthContact' => $_POST['dateBirthContact'],
      'genderContact' => $_POST['genderContact'],
      'phoneContact' => $_POST['phoneContact'],
      'occupationContact' => $_POST['occupationContact'],
      'dateBackgroundCheckContact' => $_POST['dateBackgroundCheckContact'],
      'totalMembers' => $_POST['totalMembers'],
      'background' => $_POST['background'],
      'backgroundLanguage' => $_POST['backgroundLanguage'],
      'religion' => $_POST['religion'],
      'condition' => $_POST['condition'],
      'misdemeanor' => $_POST['misdemeanor'],
      'finalQuestion' => $_POST['finalQuestion'],
    ),
    'filesResult' => $resultFile,
  );

  echo json_encode($resultFamilyInformation);
} else if (isset($_POST['familyMembers'])) {
  // TODO SAVE FAMILY MEMBERS
  $houseMail = $row_home['mail_h'];
  $idHome = $_POST['homestay_id'];

  $i = 1;
  while ($i <= 8) {
    if (
      isset($_POST["member" . $i . "_name"]) &&
      isset($_POST["member" . $i . "_lastName"]) &&
      isset($_POST["member" . $i . "_gender"]) &&
      isset($_POST["member" . $i . "_relation"])
    ) {
      $memberName = $_POST["member" . $i . "_name"];
      $memberLastName = $_POST["member" . $i . "_lastName"];
      $memberDateBirth = $_POST["member" . $i . "_dateBirth"];
      $memberGender = $_POST["member" . $i . "_gender"];
      $memberRelation = $_POST["member" . $i . "_relation"];
      $memberOccupation = $_POST["member" . $i . "_occupation"];
      $memberDateBackground = $_POST["member" . $i . "_dateBackground"];

      $queryUpdateFamilyMembers = "UPDATE mem_f SET 
          f_name$i = '$memberName',
          f_lname$i = '$memberLastName',
          db$i = '$memberDateBirth',
          gender$i = '$memberGender',
          re$i = '$memberRelation',
          occupation_f$i = '$memberOccupation',
          db_lawf$i = '$memberDateBackground' WHERE id_home = '$idHome'  ";
      $updateFamilyMembers = $link->query($queryUpdateFamilyMembers);

      $resultFile = null;
      if (isset($_FILES["member" . $i . "_backgroundCheck"])) {

        $memberBackgroundCheck = $_FILES["member" . $i . "_backgroundCheck"];
        $memberBackgroundCheck['name'] = "member-$i-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));


        if (!file_exists("../public/$houseMail")) mkdir("../public/$houseMail");
        $upload = move_uploaded_file($memberBackgroundCheck['tmp_name'], "../public/$houseMail/$memberBackgroundCheck[name]");

        if ($upload) {
          $getLastPDF = $link->query("SELECT * FROM mem_f WHERE id_home = '$idHome' ");
          $row_member = $getLastPDF->fetch_assoc();
          $PDF = $row_member['lawf' . $i];
          unlink("../$PDF");

          $queryPDF = "UPDATE mem_f SET 
          lawf$i = 'public/$houseMail/$memberBackgroundCheck[name]' WHERE id_home = '$idHome'  ";
          $updatePDF = $link->query($queryPDF);
          $resultFile = [$memberBackgroundCheck['name'], 'OK'];
        };
      }

      if ($updateFamilyMembers) $status = 'ok';
      else $status = 'fail';

      $resultFamilyMember = array(
        'status' => $status,
        'result' => array(
          'memberName' => $memberName,
          'memberLastName' => $memberLastName,
          'memberDateBirth' => $memberDateBirth,
          'memberGender' => $memberGender,
          'memberRelation' => $memberRelation,
          'memberOccupation' => $memberOccupation,
          'memberDateBackground' => $memberDateBackground,
        ),
        'resultFile' => $resultFile
      );

      echo json_encode($resultFamilyMember);
    }
    $i++;
  }
}