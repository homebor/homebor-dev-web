<?php
    require '../xeon.php';
    session_start();
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

      if ($row['usert'] != 'Agent') {
        
         header("location: ../logout.php");   
  
    } 

    $query2="SELECT * FROM manager WHERE mail = '$usuario'";
    $resultado2=$link->query($query2);

    $row2=$resultado2->fetch_assoc();

?>
<!DOCTYPE html>
<html>
  <head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <link  rel="icon"   href="../images/icon.png" type="image/png" />
    <title>Homebor - Administartor Panel</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
  </head>
  <style type="text/css">
  	.page-link {
    background-color: transparent;
    border: none;
    border-radius: 50px;
    color: #191919;
    margin: 0 .125rem;
    padding: .75rem 1rem;
    position: relative;
}

.page-link:hover {
    background-color: #815391;
    box-shadow: 0 .125rem .3125rem rgba(0, 0, 0, .1);
    color: #191919;
}

.page-link:after {
    border-style: solid;
    border-color: transparent transparent transparent transparent;
    bottom: -0.4375rem;
    content: "";
    height: 0;
    left: 0;
    position: absolute;
    width: 0;
    transition: .3s ease;
}

.page-item:first-child .page-link, .page-item:last-child .page-link {
    border-radius: 0;
}

.page-item.active .page-link {
    background-color: #232159;
    box-shadow: 0 .125rem .3125rem rgba(0, 0, 0, .1);
}

.page-item.active .page-link:after {
    border-color: white transparent transparent transparent;
}
  	@media screen and (max-width: 800px) {
  		#white-logo{display: none;}
  		#nav-footer{display: none;}
            #con-footer{display: none;}
  	}

    
  </style>
 <body style="background-color: #F3F8FC;">

<!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <main id="ts-main" style="background-color: #F3F8FC;">

        <!--PAGE TITLE
            =========================================================================================================-->


        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                              <br>
                              <br>
                              <br>
                                <br>
                             <h2>Properties Administrator Panel </h2>
    <!-- Echo username -->
    <?php
        $usuario = $_SESSION['username'];
        echo "<p>You are logged in as <b>$usuario</b>.</p>"; 
    ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <hr>
    <div class="container">
      <br />
      <div class="card">
        <div class="card-body">
          <div class="form-group">
            <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" hidden>
          </div>
          <div class="table-responsive" id="dynamic_content">
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<!-- LIST OF HOUSE ============================================================-->
<script>
  $(document).ready(function(){

    load_data(1);

    function load_data(page, query = '')
    {
      $.ajax({
        url:"fetch_propertie.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content').html(data);
        }
      });
    }

    $(document).on('click', '.page-link', function(){
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
    });

    $('#search_box').keyup(function(){
      var query = $('#search_box').val();
      load_data(1, query);
    });

  });
</script>
</section>

</body>
</main>
</body>


<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>

</body>
</html>
