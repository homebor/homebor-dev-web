<?php
// TODO WINYERSON
include '../xeon.php';
include 'ajax_noti.php';
include 'delete_reserve.php';

session_start();

$usuario = $_SESSION['username'];

$jsonToSend = null;

// TODO QUERY
$queryUser = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row_user = $queryUser->fetch_assoc();


$queryHome = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario'");
$row_home = $queryHome->fetch_assoc();


$queryRoom_IJ = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and room.id_home = pe_home.id_home");
$row_room = $queryRoom_IJ->fetch_assoc();


$queryPhotoHome_IJ = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and photo_home.id_home = pe_home.id_home");
$row_photo_home = $queryPhotoHome_IJ->fetch_assoc();


// TODO AUTHENTICATION
if ($row_user['usert'] != 'homestay') header("location: ../logout.php");


$roomNumber = 1;
foreach ($row_room as $key => $value) {
  // * ROOM
  if (
    $row_room['type' . $roomNumber] != 'NULL' &&
    $row_room['aprox' . $roomNumber] != '0'
  ) {
    if ($roomNumber == 1) $colorRoom = "#232159";
    if ($roomNumber == 2) $colorRoom = "#982A72";
    if ($roomNumber == 3) $colorRoom = "#394893";
    if ($roomNumber == 4) $colorRoom = "#A54483";
    if ($roomNumber == 5) $colorRoom = "#5D418D";
    if ($roomNumber == 6) $colorRoom = "#392B84";
    if ($roomNumber == 7) $colorRoom = "#B15391";
    if ($roomNumber == 8) $colorRoom = "#4F177D";


    // * CURRENT ROOM DATA MAIN
    $roomName = "Room " . $roomNumber;
    $roomPrice = $row_room['aprox' . $roomNumber] + $row_room['aprox_a' . $roomNumber];
    $roomType = $row_room['type' . $roomNumber];
    $roomBed_1 = [$row_room['bed' . $roomNumber], $row_room['date' . $roomNumber]];
    $roomBed_2 = [$row_room['bed' . $roomNumber . '_2'], $row_room['date' . $roomNumber . '_2']];
    $roomBed_3 = [$row_room['bed' . $roomNumber . '_3'], $row_room['date' . $roomNumber . '_3']];
    $roomFood = $row_room['food' . $roomNumber];

    if (
      $roomBed_1[1] == "Avalible" ||
      $roomBed_2[1] == "Avalible" ||
      $roomBed_3[1] == "Avalible"
    ) $roomAvalible = [$roomName, true];
    else $roomAvalible = [$roomName, false];

    // * STUDENT DATA IN CURRENT BED
    $strQueryEvents = "SELECT * FROM events WHERE color = '$colorRoom'  AND email = '$usuario' ";
    $queryEvents = $link->query($strQueryEvents);


    $allBeds = null;


    while ($value = mysqli_fetch_array($queryEvents)) {
      if ($value['color'] == $colorRoom) {
        $queryStudents = $link->query("SELECT * FROM pe_student WHERE mail_s = '$value[mail_s]'");
        $row_student = $queryStudents->fetch_assoc();


        $studentName = $row_student['name_s'] . " " . $row_student['l_name_s'];
        $studentImg = $row_student['photo_s'];
        $studentBed = $value['bed'];


        if ($value['bed'] === "A") {
          $allBeds[] = array(
            $roomName,
            $roomBed_1,
            $studentName,
            "../" . $studentImg,
            $studentBed,
          );
        } else if ($value['bed'] === "B") {
          $allBeds[] = array(
            $roomName,
            $roomBed_2,
            $studentName,
            "../" . $studentImg,
            $studentBed,
          );
        } else if ($value['bed'] === "C") {
          $allBeds[] = array(
            $roomName,
            $roomBed_3,
            $studentName,
            "../" . $studentImg,
            $studentBed,
          );
        }
      }
    }


    $jsonToSend[] = array(
      // ** HOMESTAY USER
      'homeFullName' => $row_user['name'] . " " . $row_user['l_name'],
      // ** HEADER
      'roomName' => $roomName,
      'roomPrice' => $roomPrice,
      'roomColor' => $colorRoom,
      // ** PHOTO
      'roomImage' => array(
        'image1' => $row_photo_home['proom' . $roomNumber],
        'image2' => $row_photo_home['proom' . $roomNumber . "_2"],
        'image3' => $row_photo_home['proom' . $roomNumber . "_3"],
      ),
      // ** FEATURES
      'roomType' => $roomType,
      'roomFoodService' => $roomFood,
      // ** ALL BEDS 
      'allBeds' => $allBeds,
      'roomAvalible' => $roomAvalible
    );
  }


  $roomNumber++;
  if ($roomNumber > 8) break;
}


echo json_encode($jsonToSend);
