<?php
include '../xeon.php';
include '../cript.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM academy");
$row6 = $query6->fetch_assoc();


$query7 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario' ");

$row7 = $query7->fetch_assoc();

$lastName = strtoupper($row5['l_name']);


if ($row5['usert'] != 'homestay') {
  header("location: ../logout.php");
}

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/carousel.css">
  <link rel="stylesheet" href="../agents/assets/css/homedit2.css">
  <link rel="stylesheet" href="../assets/css/utility.css">
  <link rel="stylesheet" href="assets/css/edit_probe1.css?ver=1.0.5.4.1">
  <link rel="stylesheet" href="../assets/css/rooms-cards.css">

  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <!-- REFERENCE OF MAPBOX API -->
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js">
  </script>
  <link rel="stylesheet"
    href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
    type="text/css" />
  <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/d3js/6.5.0/d3.min.js"></script>





  <title>Homestay Register</title>


  <script src="../assets/js/jquery-3.3.1.min.js"></script>


</head>

<body>

  <header id="ts-header" class="fixed-top" style="background-color: #eeee; z-index: 10 !important">
    <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
    <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light"
      style="background-color: #eeee; box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -webkit-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75);">
      <div class="container">

        <!--Brand Logo/ Logo HOMEBOR-->
        <a class="navbar-brand" href="index.php" style="margin-left:0">
          <img src="../assets/iHomestay/logos/iHomestay_logo.png" class="logo_white" alt="">
        </a>

        <ul class="navbar-nav ml-auto">

          <!--LOGIN (Main level)
                        =============================================================================================-->
          &nbsp;&nbsp;<li class="nav-item" style="list-style: none;">
            <a class="nav-link zoom logout" href="../logout.php" id="nav" style="color:#000">Logout</a>
          </li>


        </ul>

      </div>

    </nav>

  </header>


  <div class="container_register">

    <h1 class="title__register__s">Register your House</h1>

    <div class="root">
      <!-- root -->

      <section class="w-100">

        <div class="form-register__header">

          <ul class="progressbar">
            <li class="progressbar__option active color"> Required Fields </li>
            <li class="progressbar__option"> House Information </li>
            <li class="progressbar__option"> House </li>
            <li class="progressbar__option"> Family Members </li>
            <li class="progressbar__option"> Additional Information </li>
          </ul>

        </div>


        <div class="form-register__body">


          <!-- ------------------------- STEP 1 ----------------------------------- -->

          <div class="step active" id="step-1">

            <form action="#" method="POST" class="form__register" autocomplete="off" enctype="multipart/form-data">

              <!-- --------------------------------- SECTION STEP 1 ----------------------------------- -->

              <section class="d-flex justify-content-center">

                <!-- ------------------------ SECTION BASIC INFORMATION ----------------------------------- -->
                <section class="card_step card col-md-10 p-0">

                  <div class="step__header">
                    <h2 class="title__card">Required Fields</h2>
                  </div>

                  <div class="step__body row pb-4 d-flex justify-content-center">

                    <article class="form__group form__group-h_name col-lg-4" id="form__group-h_name">

                      <div class="form__group__icon-input">
                        <label for="h_name" class="form__label">House Name</label>

                        <label for="h_name" class="icon"><i class="icon icon_hname"></i></label>

                        <p class="form__group-input names"><?php echo $lastName . ', ' .$row5['name']; ?></p>
                        <input type="hidden" id="h_name" name="h_name"
                          value="<?php echo $lastName . ', ' .$row5['name']; ?>" readonly>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-num col-lg-4" id="form__group-num">

                      <div class="form__group__icon-input">
                        <label for="num" class="form__label">Phone Number *</label>

                        <label for="num" class="icon"><i class="icon icon_pnumber"></i></label>


                        <input type="text" id="num" name="num" class="form__group-input names"
                          placeholder="e.g. +(xxx) xxx-xxxx">

                      </div>
                      <p class="message__input_error"> Only numbers and special characters ( ) + - </p>

                    </article>

                    <article class="form__group form__group-room col-lg-4" id="form__group-room">

                      <div class="form__group__icon-input">
                        <label for="room" class="form__label">Rooms in your House for Students *</label>

                        <label for="room" class="icon"><i class="icon icon_rooms"></i></label>


                        <input type="text" id="room" name="room" class="form__group-input names" placeholder="e.g. 5"
                          onKeyDown="viewBed()" onKeyUp="viewBed()" maxlength="1">

                      </div>
                      <p class="message__input_error"> Only numbers. Max 8 rooms </p>

                    </article>

                    <article class="form__group form__group-m_city col-lg-4" id="form__group-m_city">

                      <div class="form__group__icon-input">
                        <label for="m_city" class="form__label">Main City *</label>

                        <label for="m_city" class="icon"><i class="icon icon__city"></i></label>


                        <select name="m_city" id="m_city" class="custom-select form__group-input address">

                          <option hidden="option" selected disabled>Select Option</option>
                          <option value="Toronto">Toronto</option>
                          <option value="Montreal">Montreal</option>
                          <option value="Ottawa">Ottawa</option>
                          <option value="Quebec">Quebec</option>
                          <option value="Calgary">Calgary</option>
                          <option value="Vancouver">Vancouver</option>
                          <option value="Victoria">Victoria</option>

                        </select>

                      </div>
                      <p class="message__input_error"> Enter a Valid Option </p>

                    </article>

                    <article class="form__group form__group-mail col-lg-4" id="form__group-mail">

                      <div class="form__group__icon-input">
                        <label for="mail" class="form__label">Homestay Email *</label>

                        <label for="mail" class="icon"><i class="icon icon__mail"></i></label>

                        <input type="text" id="mail" name="mail" class="form__group-input names" placeholder="e.g. John"
                          value="<?php echo $row5['mail'] ?>" readonly>

                      </div>
                      <p class="message__input_error"> Enter a Valid Email </p>

                    </article>

                    <article class="form__group form__group-pass col-lg-4" id="form__group-pass">

                      <div class="form__group__icon-input">
                        <label for="pass" class="form__label">Password *</label>

                        <label for="pass" class="icon"><i class="icon icon__password"></i></label>

                        <?php
                        $psw = SED::decryption($row5['psw']);
                        ?>

                        <input type="password" id="pass" name="pass" class="form__group-input names"
                          placeholder="e.g. John" value="<?php echo $psw ?>" readonly>
                        <button type="button" class="fa fa-eye mr-2 btn__eye" id="btn__eye"></button>

                      </div>
                      <p class="message__input_error"> Enter a Valid Email </p>

                    </article>

                    <article class="form__group form__group-pet col-lg-4" id="form__group-pet">

                      <div class="form__group__icon-input">
                        <label for="pet" class="form__label">Do you have pets?</label>

                        <label for="pet" class="icon"><i class="icon icon_pets"></i></label>


                        <select class="custom-select form__group-input ypets" id="pet" name="pet" onchange="yesPets()">
                          <option hidden="option" selected disabled> -- Select Option -- </option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Enter a Valid Option
                      </p>

                    </article>

                    <article class="form__group form__group-pet_num col-lg-4 d-none" id="form__group-pet_num">

                      <div class="form__group__icon-input">
                        <label for="pet_num" class="form__label">How many pets?</label>

                        <label for="pet_num" class="icon"><i class="icon icon_pets"></i></label>


                        <input type="number" id="pet_num" name="pet_num" min="1" max="10"
                          class="form__group-input names" placeholder="Pets Number">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-kpets col-lg-4 d-none" id="form__group-kpets">

                      <div class="form__group__icon-input">
                        <label for="kpets" class="form__label">Kind of Pet</label>


                        <div class="d-flex w-100 px-4 mt-3" style="justify-content: space-between">
                          <div class="pet_dog">
                            <input type="checkbox" id="dog" name="dog" value="yes">
                            <label for="">Dog</label>
                          </div>

                          <div class="pet_cat">
                            <input type="checkbox" id="cat" name="cat" value="yes">
                            <label for="">Cat</label>
                          </div>

                          <div class="pet_other">
                            <input type="checkbox" id="other" name="other" value="yes">
                            <label for="">Other</label>
                          </div>
                        </div>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-type_pet col-lg-4" id="form__group-type_pet">

                      <div class="form__group__icon-input">
                        <label for="type_pet" class="form__label">Specify</label>

                        <label for="type_pet" class="icon"><i class="icon icon_pets"></i></label>


                        <input type="text" id="type_pet" name="type_pet" class="form__group-input names"
                          placeholder="Specify">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                  </div>

                  <h2 class="title__group-section mt-0 mb-3 mr-auto ml-auto" style="margin-bottom: -1rem">Would you like
                    to receive students?</h2>

                  <div class="step__body row d-flex justify-content-center">

                    <article class="form__group form__group-ag_pre col-lg-4" id="form__group-ag_pre">

                      <div class="form__group__icon-input">
                        <label for="ag_pre" class="form__label">Age Preference *</label>

                        <label for="ag_pre" class="icon"><i class="icon icon_age"></i></label>


                        <select class="custom-select form__group-input age" id="ag_pre" name="ag_pre">
                          <option hidden="option" selected disabled>-- Select Age --</option>
                          <option value="Teenager">Teenager</option>
                          <option value="Adult">Adult</option>
                          <option value="Any">Any</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Enter a Valid Option </p>

                    </article>

                    <article class="form__group form__group-g_pre col-lg-4" id="form__group-g_pre">

                      <div class="form__group__icon-input">
                        <label for="g_pre" class="form__label">Gender Preference *</label>

                        <label for="g_pre" class="icon"><i class="icon icon_gender"></i></label>


                        <select class="custom-select form__group-input gender" id="g_pre" name="g_pre">
                          <option hidden="option" selected disabled>-- Select Gender --</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Any">Any</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Enter a Valid Option </p>

                    </article>




                    <!-- <article class="form__group form__group-tm_service col-md-9 col-lg-8 kind_meal d-none"
                      id="form__group-tm_service">

                      <h4 class="text-center mb-0 p-3">Next, choose the option that best suits your needs</h4>

                      <div class="d-flex justify-content-center meal_div-parents ml-auto mr-auto px-2">

                        <table class="table ">

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check1" class="d-none check__meals" value="private (3)"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check1">Private Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              3 Meals included (breakfast, lunch, dinner).
                            </th>

                          </tr>

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check2" class="d-none check__meals" value="private (2)"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check2">Private Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              2 Meals included (breakfast, dinner).
                            </th>

                          </tr>

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check3" class="d-none check__meals" value="shared (3)"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check3">Shared Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              3 Meals included (breakfast, lunch, dinner).
                            </th>

                          </tr>

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check4" class="d-none check__meals" value="share (2)"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check4">Shared Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              2 Meals included (breakfast, dinner).
                            </th>

                          </tr>

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check5" class="d-none check__meals" value="only private"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check5">Only Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              (Private Room) No food.
                            </th>

                          </tr>

                          <tr>

                            <th class="meal_div col-3 text-center align-middle title_tm">
                              <input type="checkbox" id="check6" class="d-none check__meals" value="only shared"
                                name="tm_service">
                              <label class="label__meals fa fa-check" for="check6">Only Room</label>
                            </th>

                            <th class="content__meals col-6 align-middle descrip_tm">
                              (Shared Room) No food.
                            </th>

                          </tr>

                        </table>

                      </div>

                      <div class="d-flex justify-content-center  col-lg-4">


                      </div>

                      <div class="d-flex justify-content-center meal_div col-lg-8">

                        <p></p>

                      </div>

                    </article> -->

                    <input type="text" value="next_step-1" name="value_step" hidden>

                  </div>

                </section>

              </section>

              <!-- <script>
              const $mealS = document.querySelector("#m_service");

              function tmService() {

                const mealSI = $mealS.selectedIndex;
                if (mealSI === -1) return; // Esto es cuando no hay elementos
                const optionMealS = $mealS.options[mealSI];

                if (optionMealS.value == 'Yes') document.getElementById('form__group-tm_service').classList.remove(
                  'd-none');
                else if (optionMealS.value == 'No') document.getElementById('form__group-tm_service').classList.add(
                  'd-none');
                else document.getElementById('form__group-tm_service').classList.add('d-none');

              }
              </script>-->

              <div class="step__footer">

                <button type="submit" id="next_step-1" class="btn btn__next step__button--next" data-to_step="2"
                  data-step="1">Next</button>

              </div>
            </form>
          </div>

          <script>
          const $pets = document.querySelector("#pet");

          function yesPets() {

            const petsI = $pets.selectedIndex;
            if (petsI === -1) return; // Esto es cuando no hay elementos
            const optionPets = $pets.options[petsI];

            if (optionPets.value == 'Yes') {

              document.getElementById('form__group-pet_num').classList.remove('d-none');
              document.getElementById('form__group-kpets').classList.remove('d-none');

            } else if (optionPets.value == 'No') {
              document.getElementById('form__group-pet_num').classList.add('d-none');
              document.getElementById('form__group-kpets').classList.add('d-none');
            } else {
              document.getElementById('form__group-pet_num').classList.add('d-none');
              document.getElementById('form__group-kpets').classList.add('d-none');

            }

          };

          // obtener campos ocultar div
          var checkbox = $("#other");
          var hidden = $("#form__group-type_pet");
          //var populate = $("#populate");

          hidden.hide();
          checkbox.change(function() {
            if (checkbox.is(':checked')) {
              //hidden.show();
              $("#form__group-type_pet").fadeIn("200")
            } else {
              //hidden.hide();
              $("#form__group-type_pet").fadeOut("100")
              $("#type_pet").val(""); // limpia los valores de lols input al ser ocultado

            }
          });
          </script>


          <!------------------------------------------------------------------------------------------->


          <!-- ------------------------- STEP 2 ------------------------------------ -->

          <div class="step" id="step-2">

            <form action="#" method="POST" class="form__register" autocomplete="off" enctype="multipart/form-data">

              <section class="d-flex justify-content-center">

                <section class="card_step card col-md-10 p-0">

                  <div class="step__header">
                    <h2 class="title__card">House Information</h2>
                  </div>

                  <h2 class="title__group-section mt-4 mr-auto ml-auto mb-0">Location</h2>

                  <div class="step__body row d-flex justify-content-center w-100 ml-auto mr-auto">

                    <article class="form__group form__group-dir col-xl-5" id="form__group-dir">

                      <div class="form__group__icon-input">

                        <label for="dir" class="form__label">Address</label>
                        <label for="dir" class="icon"><i class="icon icon_location"></i></label>
                        <input type="text" id="dir" name="dir" class="form__group-input names"
                          placeholder="Av, Street, etc.">

                      </div>
                      <p class="message__input_error"> Enter a valid Address </p>

                    </article>

                    <article class="form__group form__group-city col-xl-5" id="form__group-city">

                      <div class="form__group__icon-input">

                        <label for="city" class="form__label">City</label>
                        <label for="city" class="icon"><i class="icon icon_location"></i></label>
                        <input type="text" id="city" name="city" class="form__group-input names"
                          placeholder="e.g. Davenport">

                      </div>
                      <p class="message__input_error"> Enter a valid City </p>

                    </article>

                    <article class="form__group form__group-state col-xl-5" id="form__group-state">

                      <div class="form__group__icon-input">

                        <label for="state" class="form__label">State / Province</label>
                        <label for="state" class="icon"><i class="icon icon_location"></i></label>
                        <input type="text" id="state" name="state" class="form__group-input names"
                          placeholder="e.g. Ontario">

                      </div>
                      <p class="message__input_error"> Enter a valid State </p>

                    </article>

                    <article class="form__group form__group-p_code col-xl-5" id="form__group-p_code">

                      <div class="form__group__icon-input">

                        <label for="p_code" class="form__label">Postal Code</label>
                        <label for="p_code" class="icon"><i class="icon icon_location"></i></label>
                        <input type="text" id="p_code" name="p_code" class="form__group-input names"
                          placeholder="No Special Characters">

                      </div>
                      <p class="message__input_error"> Enter a Postal Code </p>

                    </article>

                    <article class="form__group form__group-h_type col-xl-5" id="form__group-h_type">

                      <div class="form__group__icon-input">

                        <label for="h_type" class="form__label">Type of Residence</label>
                        <label for="h_type" class="icon"><i class="icon icon_location"></i></label>

                        <select name="h_type" id="h_type group__h_type" class="custom-select form__group-input names">

                          <option selected hidden="option" disabled>-- Select Option --</option>
                          <option value="House"> House </option>
                          <option value="Apartment"> Apartment </option>
                          <option value="Condominium"> Condominium </option>

                        </select>

                      </div>
                      <p class="message__input_error"> Enter a Postal Code </p>

                    </article>

                  </div>



                  <h2 class="title__group-section mt-0 mr-auto ml-auto mb-0">Information for your Students</h2>

                  <div class="step__body row d-flex justify-content-center w-100 ml-auto mr-auto">

                    <article class="form__group form__group-y_service col-xl-5" id="form__group-y_service">

                      <div class="form__group__icon-input">
                        <label for="y_service" class="form__label years_experience">Since when have you been a
                          Homestay?</label>

                        <label for="y_service" class="icon"><i class="icon icon_years"></i></label>


                        <input type="text" id="y_service" name="y_service" class="form__group-input names"
                          placeholder="MM/DD/YYYY" readonly>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-m_service col-xl-5" id="form__group-m_service">

                      <div class="form__group__icon-input">
                        <label for="m_service" class="form__label">Do you want to offer food service?</label>

                        <label for="m_service" class="icon"><i class="icon icon_meals"></i></label>


                        <select class="custom-select form__group-input m_service" id="m_service" name="m_service">
                          <option hidden="option" selected disabled>- Select -</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Only numbers. Max 8 rooms </p>

                    </article>

                    <article class="form__group form__group-num_mem col-xl-5" id="form__group-num_mem">

                      <div class="form__group__icon-input">
                        <label for="num_mem" class="form__label">Number Members incluring you</label>

                        <label for="num_mem" class="icon"><i class="icon icon_family"></i></label>


                        <input type="number" class="form__group-input nmembers" id="num_mem" name="num_mem" min="0"
                          max="9" placeholder="Only Numbers">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-backl col-xl-5" id="form__group-backl">

                      <div class="form__group__icon-input">
                        <label for="backl" class="form__label">Background Language</label>

                        <label for="backl" class="icon"><i class="icon icon_bcheck"></i></label>


                        <input type="text" class="form__group-input backl" id="backl" name="backl"
                          placeholder="e.g. English">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-type_pet col-xl-12 d-flex justify-content-center mt-2"
                      id="form__group-type_pet">

                      <article class="card col-sm-11 col-md-9 col-lg-9 col-xl-7 py-4 px-2 card__special_diet">

                        <div class="header_card d-flex">
                          <h2 class="title__group-preference"><i class="icon icon_diet"></i>Special Diet </h2>
                        </div>

                        <div class="body_card">

                          <div class="row w-100 m-0 d-flex mb-4" style="justify-content: space-between">
                            <div class="col-sm-6 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians"
                                value="yes">
                              <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                            </div>
                            <div class="col-sm-5 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">
                              <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                            </div>
                          </div>

                          <div class="row w-100 m-0 d-flex mb-4" style="justify-content: space-between">
                            <div class="col-sm-6 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">
                              <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                            </div>
                            <div class="col-sm-5 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="lactose" name="lactose"
                                value="yes">
                              <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                            </div>
                          </div>

                          <div class="row w-100 m-0 d-flex mb-4" style="justify-content: space-between">
                            <div class="col-sm-6 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">
                              <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                            </div>
                            <div class="col-sm-5 w-100 text-left">
                              <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes">
                              <label class="custom-control-label" for="pork">No Pork</label>
                            </div>
                          </div>

                          <div class="row w-100 m-0 d-flex mb-4" style="justify-content: center">
                            <div class="col-sm-6 w-100 text-center">
                              <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">
                              <label class="custom-control-label" for="none">None</label>
                            </div>
                          </div>

                        </div>

                      </article>

                    </article>

                  </div>


                  <!-- // TODO ALL ROOMS CONTAINER -->
                  <section class="m-0 p-0 bedrooms">

                  </section>

                </section>
              </section>

              <div id="modal__kb"
                class="w-100 h-100 position-fixed d-none align-items-center justify-content-center parent__modal">
                <div class="justify-content-center align-items-center modal__div">

                  <div class="modal__header d-flex justify-content-end ">
                    <button type="button" id="close__modal" class="close__modal" onclick="closeModal()">X</button>
                  </div>

                  <div class="modal__content pt-4 px-4">
                    <h2 class="text-center title__middle-modal">Congratulations! You almost fill 50% of your
                      registration.</h2>

                    <div class="d-flex justify-content-center">
                      <img class="img__middle__step" src="../assets/img/admin.acomo.2.png" alt="">
                    </div>

                    <p class="text__modal"> You can now Access your profile immediately, or you can continue with the
                      registration. </p>
                  </div>

                  <div class="modal__footer d-flex justify-content-center p-3">

                    <div class="div__footer">
                      <button type="button" id="register" class="btn btn_modal-close" data-to_step="3"
                        data-step="2">Continue
                        Later</button>
                      <button type="submit" id="next_step-2" class="btn btn__modal step__button--next" data-to_step="3"
                        data-step="2">Continue Registration</button>
                    </div>
                  </div>

                </div>
              </div>




              <input type="text" value="next_step-2" name="value_step" hidden>


              <div class="step__footer">

                <button type="submit" id="back_step-2" class="btn step__button--back" data-to_step="1"
                  data-step="2">Back</button>

                <button type="submit" id="btn__modal" class="btn btn__open-modal btn__next">Next</button>
              </div>

            </form>



          </div>

          <!--------------------------------------------------------------------------------------------->





          <!-- ------------------------- Step 3 ----------------------------------- -->

          <div class="step" id="step-3">
            <form action="#" method="POST" class="form__register" autocomplete="off" enctype="multipart/form-data">

              <section class="d-flex justify-content-center">

                <section class="card_step card col-md-10 p-0">

                  <div class="step__header">
                    <h2 class="title__card">House</h2>
                  </div>

                  <h2 class="title__group-section mt-4 mr-auto ml-auto" style="margin-bottom: -1rem">Description of your
                    House</h2>

                  <div class="step__body w-100 ml-auto mr-auto">

                    <article class="form__group form__group-des col-lg-11 ml-auto mr-auto mb-0" id="form__group-des">

                      <div class="form__group__icon-input">

                        <label for="address" class="l_icon" title="Description"> <i class="icon icon_description"></i>
                        </label>
                        <textarea rows="3" maxlength="255" class="form__group-input description" name="des" id="des"
                          placeholder="Description: Describe your house using few words, no special characters."></textarea>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                  </div>

                  <h2 class="title__group-section mt-4 mb-2 mr-auto ml-auto" style="margin-bottom: -1rem">House Gallery
                  </h2>

                  <div class="step__body ml-auto mr-auto body__gellery">

                    <section class="row section__first">

                      <article class="card ">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                          Frontage Photo </h4>


                        <div class="div-img-r" align="center">


                          <div class="carousel-item  active">

                            <label for="file" class="photo-add" id="label-i">
                              <img class="gallery-photo w-100" src="../assets/emptys/frontage-empty.png" alt="">
                            </label>

                            <input type="file" name="main" id="file" data-preview="true" accept="image/*"
                              style="display: none">



                            <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                              title="Change Frontage Photo"></label>

                          </div>

                        </div>

                      </article>

                      <article class="card ">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                          Living Room Photo </h4>

                        <div class="div-img-r" align="center">

                          <div class="carousel-item active">

                            <label for="file-ii" class="photo-add" id="label-ii">
                              <img class="gallery-photo" src="../assets/emptys/living-room-empty.png" alt="">
                            </label>

                            <img id="preview-ii" class="add-photo d-none">

                            <input type="file" name="lroom" id="file-ii" data-preview="true" accept="image/*"
                              style="display: none">


                            <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-ii-i"
                              style="display: none" title="Change Living Room"> </label>

                          </div>

                        </div>
                      </article>

                      <article class="card ">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; ">
                          Family Picture </h4>

                        <div class="div-img-r" align="center">


                          <div class="carousel-item active">
                            <label for="family" class="photo-add" id="fp">
                              <img class="gallery-photo" src="../assets/emptys/family-empty.png" alt="">
                            </label>

                            <input type="file" name="fp" id="family" data-preview="true" accept="image/*"
                              style="display: none">

                            <img id="preview-fp" class="add-photo d-none">



                            <label for="family" class="add-photo-i fa fa-pencil-alt" id="fp-i" style="display: none;"
                              title="Change Frontage Photo"></label>

                          </div>

                        </div>

                      </article>

                    </section>

                    <section class="row mb-3">


                      <article class="card">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
                          Kitchen </h4>

                        <div class="div-img-r" align="center">

                          <div class="carousel-item active">

                            <label for="file-iii" class="photo-add" id="label-iii">
                              <img class="gallery-photo" src="../assets/emptys/kitchen-empty.png" alt="">
                            </label>

                            <img id="preview-iii" class="add-photo d-none">

                            <input type="file" name="area_i" id="file-iii" data-preview="true" accept="image/*"
                              style="display: none">


                            <label for="file-iii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i"
                              style="display: none" title="Change Kitchen Photo"></label>


                          </div>

                        </div>

                      </article>


                      <article class="card ">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
                          Dining Room </h4>


                        <div class="div-img-r" align="center">

                          <div class="carousel-item active">

                            <label for="file-iv" class="photo-add" id="label-iv">
                              <img class="gallery-photo" src="../assets/emptys/dinning-room-empty.png" alt="">
                            </label>

                            <img id="preview-iv" class="add-photo d-none">

                            <input type="file" name="area_ii" id="file-iv" data-preview="true" accept="image/*"
                              style="display: none">


                            <label for="file-iv" class="add-photo-i fa fa-pencil-alt" id="label-iv-i"
                              style="display: none" title="Change Dining Room Photo"> </label>

                          </div>

                        </div>

                      </article>

                      <article class="d-flex justify-content-center align-items-center">

                        <button class="add-room iii" id="iii" type="button" data-toggle="collapse"
                          data-target="#collapse-floor-1" aria-expanded="false" aria-controls="collapse-floor-1"
                          onclick="mostrarBoton_1()"
                          style="max-height: 2.5rem; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                          title="Add Another Area of the House">

                          +

                        </button>

                        <article class="card collapse w-100" id="collapse-floor-1">

                          <h4 class="title-room pl-2"
                            style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                            House Area 3 <button class="fa fa-window-close iii" id="iii-i" type="button"
                              data-toggle="collapse" data-target="#collapse-floor-1" aria-expanded="false"
                              aria-controls="collapse-floor-1" onclick="hidBoton_1()"
                              style=" font-size: 20px; border: none; background: white; display: none; color: #4F177D; float: right; margin-right: -.5em; ">
                            </button></h4>

                          <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                              <label for="file-v" class="photo-add" id="label-v">
                                <img class="gallery-photo" src="../assets/emptys/house-area-empty.png" alt="">
                              </label>

                              <img id="preview-v" class="add-photo d-none">

                              <input type="file" name="area_iii" id="file-v" data-preview="true" accept="image/*"
                                style="display: none">


                              <label for="file-v" class="add-photo-i fa fa-pencil-alt" id="label-v-i"
                                style="display: none" title="Change House Area Photo"> </label>


                            </div>



                          </div>

                        </article>

                      </article>

                      <article class=" justify-content-center align-items-center" id="area-iii" style="display:none">

                        <button class="add-room iii" id="iv" type="button" data-toggle="collapse"
                          data-target="#collapse-floor-2" aria-expanded="false" aria-controls="collapse-floor-2"
                          onclick="mostrarBoton_2()"
                          style=" padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px; margin-bottom: 1.5rem"
                          title="Add Another Area of the House">

                          +

                        </button>

                        <article class="card collapse w-100" id="collapse-floor-2">

                          <h4 class="title-room"
                            style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                            House Area 4 <button class="fa fa-window-close iii" id="iv-i" type="button"
                              data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false"
                              aria-controls="collapse-floor-2" onclick="hidBoton_2()"
                              style=" font-size: 20px; border: none; background: white; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
                          </h4>

                          <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                              <label for="file-vi" class="photo-add" id="label-vi">
                                <img class="gallery-photo" src="../assets/emptys/house-area-empty.png" alt="">
                              </label>

                              <img id="preview-vi" class="add-photo d-none">

                              <input type="file" name="area_iv" id="file-vi" data-preview="true" accept="image/*"
                                style="display: none">


                              <label for="file-vi" class="add-photo-i fa fa-pencil-alt" id="label-vi-i"
                                style="display: none" title="Change House Area Photo"> </label>

                            </div>

                          </div>

                        </article>


                      </article>

                    </section>

                    <section class="row ">

                      <article class="card ">

                        <h4 class="title-room"
                          style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                          Bathroom Photo 1 </h4>

                        <div class="div-img-r" align="center">

                          <div class="carousel-item active">

                            <label for="file-vii" class="photo-add" id="label-vii">
                              <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png" alt="">
                            </label>

                            <img id="preview-vii" class="add-photo d-none">

                            <input type="file" name="bath_i" id="file-vii" data-preview="true" accept="image/*"
                              style="display: none">


                            <label for="file-vii" class="add-photo-i fa fa-pencil-alt" id="label-vii-i"
                              style="display: none" title="Change Bathroom Photo"> </label>

                          </div>

                        </div>

                      </article>


                      <article class=" d-flex  justify-content-center align-items-center p-0">

                        <button class="add-room iii" id="vi" type="button" data-toggle="collapse"
                          data-target="#collapse-floor-4" aria-expanded="false" aria-controls="collapse-floor-4"
                          onclick="mostrarBoton_4()"
                          style="padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                          title="Add Another Bathroom">+</button>

                        <article class="card collapse w-100" id="collapse-floor-4">

                          <h4 class="title-room"
                            style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                            Bathroom 2 <button class="fa fa-window-close iii" id="vi-i" type="button"
                              data-toggle="collapse" data-target="#collapse-floor-4" aria-expanded="false"
                              aria-controls="collapse-floor-4" onclick="hidBoton_4()"
                              style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
                          </h4>

                          <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                              <label for="file-viii" class="photo-add" id="label-viii">
                                <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png" alt="">
                              </label>

                              <img id="preview-viii" class="add-photo d-none">

                              <input type="file" name="bath_ii" id="file-viii" data-preview="true" accept="image/*"
                                style="display: none">


                              <label for="file-viii" class="add-photo-i fa fa-pencil-alt" id="label-viii-i"
                                style="display: none" title="Change Bathroom Photo"> </label>

                            </div>

                          </div>

                        </article>

                      </article>

                      <article class="justify-content-center align-items-center" id="bath-iii" style="display: none">

                        <button class="add-room iii" id="vii" type="button" data-toggle="collapse"
                          data-target="#collapse-floor-5" aria-expanded="false" aria-controls="collapse-floor-5"
                          onclick="mostrarBoton_5()"
                          style="padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                          title="Add Another Bathroom">

                          +

                        </button>

                        <article class="card collapse w-100" id="collapse-floor-5">

                          <h4 class="title-room"
                            style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                            Bathroom 3 <button class="fa fa-window-close iii" id="vii-i" type="button"
                              data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false"
                              aria-controls="collapse-floor-5" onclick="hidBoton_5()"
                              style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
                          </h4>

                          <div class="div-img-r" align="center">

                            <div class="carousel-item active">

                              <label for="file-ix" class="photo-add" id="label-ix">
                                <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png" alt="">
                              </label>

                              <img id="preview-ix" class="add-photo d-none">

                              <input type="file" name="bath_iii" id="file-ix" data-preview="true" accept="image/*"
                                style="display: none">


                              <label for="file-ix" class="add-photo-i fa fa-pencil-alt" id="label-ix-i"
                                style="display: none" title="Change Bathroom Photo"> </label>

                            </div>

                          </div>

                        </article>

                      </article>

                      <article class="justify-content-center align-items-center" id="bath-iv" style="display: none;">

                        <button class="add-room iii" id="viii" type="button" data-toggle="collapse"
                          data-target="#collapse-floor-6" aria-expanded="false" aria-controls="collapse-floor-6"
                          onclick="mostrarBoton_6()"
                          style="padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px; margin-bottom: 1.5rem"
                          title="Add Bathroom Photo 4">


                          +

                        </button>

                        <article class="card collapse w-100" id="collapse-floor-6">

                          <h4 class="title-room"
                            style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                            Bathroom 4 <button class="fa fa-window-close iii" id="viii-i" type="button"
                              data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                              aria-controls="collapse-floor-6" onclick="hidBoton_6()"
                              style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em; "></button>
                          </h4>

                          <div class="div-img-r" align="center">

                            <div class="carousel-item active">

                              <label for="file-x" class="photo-add" id="label-x">
                                <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png" alt="">
                              </label>

                              <img id="preview-x" class="add-photo d-none">

                              <input type="file" name="bath_iv" id="file-x" data-preview="true" accept="image/*"
                                style="display: none">


                              <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i"
                                style="display: none" title="Change Bathroom Photo"> </label>

                            </div>

                          </div>

                        </article>

                      </article>

                    </section>



                    <input type="text" value="next_step-3" name="value_step" hidden>

                  </div>

                </section>

              </section>




              <div class="step__footer">

                <button type="submit" id="back_step-1" class="btn step__button--back" data-to_step="2"
                  data-step="3">Back</button>

                <button type="submit" class="btn btn__next step__button--next" data-to_step="4"
                  data-step="3">Next</button>
              </div>

            </form>

          </div>

          <!--------------------------------------------------------------------------------------------->


          <!-- Step 4 -->

          <div class="step" id="step-4">

            <form action="#" method="POST" class="form__register" autocomplete="off" enctype="multipart/form-data">

              <section class="d-flex justify-content-center">

                <section class="card_step card col-md-10 p-0">

                  <div class="step__header">
                    <h2 class="title__card">Family Members</h2>
                  </div>

                  <h2 class="title__group-section mt-4 mr-auto ml-auto" style="margin-bottom: -1rem">Main Contact
                    Information</h2>

                  <div class="step__body row d-flex justify-content-center w-100 ml-auto mr-auto mt-5">

                    <article class="form__group form__group-name_h col-xl-4" id="form__group-name_h">

                      <div class="form__group__icon-input">
                        <label for="name_h" class="form__label">Name Main Contact</label>

                        <label for="name_h" class="icon"><i class="icon icon_mname"></i></label>


                        <input type="text" id="name_h" name="name_h" class="form__group-input names"
                          value="<?php echo $row5['name']; ?>">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-l_name_h col-xl-4" id="form__group-l_name_h">

                      <div class="form__group__icon-input">
                        <label for="l_name_h" class="form__label">Last Name</label>

                        <label for="l_name_h" class="icon"><i class="icon icon_mname"></i></label>


                        <input type="text" id="l_name_h" name="l_name_h" class="form__group-input names"
                          value="<?php echo $row5['l_name']; ?>">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-db col-xl-4" id="form__group-db">

                      <div class="form__group__icon-input">
                        <label for="db" class="form__label">Date of Birth</label>

                        <label for="db" class="icon"><i class="icon icon_db"></i></label>


                        <input type="text" id="db" name="db" class="form__group-input names" readonly
                          placeholder="MM/DD/YYYY">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-gender col-xl-4" id="form__group-gender">

                      <div class="form__group__icon-input">
                        <label for="gender" class="form__label">Gender</label>

                        <label for="gender" class="icon"><i class="icon icon_gender"></i></label>

                        <select class="custom-select form__group-input mgender" id="gender" name="gender">
                          <option hidden="option" selected disabled>-- Select Gender --</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Private">Private</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-cell col-xl-4" id="form__group-cell">

                      <div class="form__group__icon-input">
                        <label for="cell" class="form__label">Phone Number</label>

                        <label for="cell" class="icon"><i class="icon icon_pnumber"></i></label>


                        <input type="text" id="cell" name="cell" class="form__group-input num"
                          placeholder="e.g. 55578994">

                      </div>
                      <p class="message__input_error"> Only numbers and special characters ( ) + -
                      </p>

                    </article>

                    <article class="form__group form__group-occupation_m col-xl-4" id="form__group-occupation_m">

                      <div class="form__group__icon-input">
                        <label for="occupation_m" class="form__label">Occupation</label>

                        <label for="occupation_m" class="icon"><i class="icon icon_bcheck"></i></label>


                        <input type="text" id="occupation_m" name="occupation_m" class="form__group-input num"
                          placeholder="e.g. Lawyer">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-db_law col-xl-4" id="form__group-db_law">

                      <div class="form__group__icon-input">
                        <label for="db_law" class="form__label">Date of Background Check</label>

                        <label for="db_law" class="icon"><i class="icon icon_db"></i></label>


                        <input type="text" id="db_law" name="db_law" class="form__group-input num"
                          placeholder="MM/DD/YYYY">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-db_file col-xl-8" id="form__group-db_file">

                      <div class="form__group__icon-input">
                        <label for="db_file" class="form__label">Background Check</label>

                        <label for="db_file" class="icon"><i class="icon icon_db"></i></label>

                        <input type="file" class="form__group-input db_law" name="b_check_main" id="profile-img"
                          maxlength="1" accept="pdf" style="width: 100%;">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>


                    <input type="text" value="next_step-4" name="value_step" hidden>

                  </div>

                  <div class="step__body contact_main py-0 mb-5 w-100 ml-auto mr-auto">

                    <button type="button" data-toggle="collapse" class="add__family" data-target="#family1"
                      aria-expanded="false"> Add Family Member </button>

                    <div id="family1" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 2</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name1 col-xl-4" id="form__group-f_name1">

                          <div class="form__group__icon-input">
                            <label for="f_name1" class="form__label">Name</label>

                            <label for="f_name1" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name1" name="f_name1"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname1 col-xl-4" id="form__group-f_lname1">

                          <div class="form__group__icon-input">
                            <label for="f_lname1" class="form__label">Last Name</label>

                            <label for="f_lname1" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname1" name="f_lname1"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db1 col-xl-4" id="form__group-db1">

                          <div class="form__group__icon-input">
                            <label for="db1" class="form__label">Date of Birth</label>

                            <label for="db1" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db1" name="db1"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender1 col-xl-4" id="form__group-gender1">

                          <div class="form__group__icon-input">
                            <label for="gender1" class="form__label">Gender</label>

                            <label for="gender1" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender1" name="gender1">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re1 col-xl-4" id="form__group-re1">

                          <div class="form__group__icon-input">
                            <label for="re1" class="form__label">Relation</label>

                            <label for="re1" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re1" name="re1">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f1 col-xl-4" id="form__group-occupation_f1">

                          <div class="form__group__icon-input">
                            <label for="occupation_f1" class="form__label">Occupation</label>

                            <label for="occupation_f1" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f1" name="occupation_f1"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf1 col-xl-4" id="form__group-db_lawf1">

                          <div class="form__group__icon-input">
                            <label for="db_lawf1" class="form__label">Date Background Check</label>

                            <label for="db_lawf1" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf1" name="db_lawf1"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file1 col-xl-8" id="form__group-db_file1">

                          <div class="form__group__icon-input">
                            <label for="db_file1" class="form__label">Background Check</label>

                            <label for="db_file1" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem1" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family2"
                        aria-expanded="false"> Add Family Member </button>

                    </div>

                    <div id="family2" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 3</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name2 col-xl-4" id="form__group-f_name2">

                          <div class="form__group__icon-input">
                            <label for="f_name2" class="form__label">Name</label>

                            <label for="f_name2" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name2" name="f_name2"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname2 col-xl-4" id="form__group-f_lname2">

                          <div class="form__group__icon-input">
                            <label for="f_lname2" class="form__label">Last Name</label>

                            <label for="f_lname2" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname2" name="f_lname2"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db2 col-xl-4" id="form__group-db2">

                          <div class="form__group__icon-input">
                            <label for="db2" class="form__label">Date of Birth</label>

                            <label for="db2" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db2" name="db2"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender2 col-xl-4" id="form__group-gender2">

                          <div class="form__group__icon-input">
                            <label for="gender2" class="form__label">Gender</label>

                            <label for="gender2" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender2" name="gender2">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re2 col-xl-4" id="form__group-re2">

                          <div class="form__group__icon-input">
                            <label for="re2" class="form__label">Relation</label>

                            <label for="re2" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re2" name="re2">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f2 col-xl-4" id="form__group-occupation_f2">

                          <div class="form__group__icon-input">
                            <label for="occupation_f2" class="form__label">Occupation</label>

                            <label for="occupation_f2" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f2" name="occupation_f2"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf2 col-xl-4" id="form__group-db_lawf2">

                          <div class="form__group__icon-input">
                            <label for="db_lawf2" class="form__label">Date Background Check</label>

                            <label for="db_lawf2" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf2" name="db_lawf2"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file2 col-xl-8" id="form__group-db_file2">

                          <div class="form__group__icon-input">
                            <label for="db_file2" class="form__label">Background Check</label>

                            <label for="db_file2" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem2" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family3"
                        aria-expanded="false"> Add Family Member </button>

                    </div>

                    <div id="family3" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 4</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name3 col-xl-4" id="form__group-f_name3">

                          <div class="form__group__icon-input">
                            <label for="f_name3" class="form__label">Name</label>

                            <label for="f_name3" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name3" name="f_name3"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname3 col-xl-4" id="form__group-f_lname3">

                          <div class="form__group__icon-input">
                            <label for="f_lname3" class="form__label">Last Name</label>

                            <label for="f_lname3" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname3" name="f_lname3"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db3 col-xl-4" id="form__group-db3">

                          <div class="form__group__icon-input">
                            <label for="db3" class="form__label">Date of Birth</label>

                            <label for="db3" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db3" name="db3"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender3 col-xl-4" id="form__group-gender3">

                          <div class="form__group__icon-input">
                            <label for="gender3" class="form__label">Gender</label>

                            <label for="gender3" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender3" name="gender3">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re3 col-xl-4" id="form__group-re3">

                          <div class="form__group__icon-input">
                            <label for="re3" class="form__label">Relation</label>

                            <label for="re3" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re3" name="re3">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f3 col-xl-4" id="form__group-occupation_f3">

                          <div class="form__group__icon-input">
                            <label for="occupation_f3" class="form__label">Occupation</label>

                            <label for="occupation_f3" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f3" name="occupation_f3"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf3 col-xl-4" id="form__group-db_lawf3">

                          <div class="form__group__icon-input">
                            <label for="db_lawf3" class="form__label">Date Background Check</label>

                            <label for="db_lawf3" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf3" name="db_lawf3"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file3 col-xl-8" id="form__group-db_file3">

                          <div class="form__group__icon-input">
                            <label for="db_file3" class="form__label">Background Check</label>

                            <label for="db_file3" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem3" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family4"
                        aria-expanded="false"> Add Family Member </button>

                    </div>


                    <div id="family4" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 5</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name4 col-xl-4" id="form__group-f_name4">

                          <div class="form__group__icon-input">
                            <label for="f_name4" class="form__label">Name</label>

                            <label for="f_name4" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name4" name="f_name4"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname4 col-xl-4" id="form__group-f_lname4">

                          <div class="form__group__icon-input">
                            <label for="f_lname4" class="form__label">Last Name</label>

                            <label for="f_lname4" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname4" name="f_lname4"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db4 col-xl-4" id="form__group-db4">

                          <div class="form__group__icon-input">
                            <label for="db4" class="form__label">Date of Birth</label>

                            <label for="db4" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db4" name="db4"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender4 col-xl-4" id="form__group-gender4">

                          <div class="form__group__icon-input">
                            <label for="gender4" class="form__label">Gender</label>

                            <label for="gender4" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender4" name="gender4">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re4 col-xl-4" id="form__group-re4">

                          <div class="form__group__icon-input">
                            <label for="re4" class="form__label">Relation</label>

                            <label for="re4" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re4" name="re4">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f4 col-xl-4" id="form__group-occupation_f4">

                          <div class="form__group__icon-input">
                            <label for="occupation_f4" class="form__label">Occupation</label>

                            <label for="occupation_f4" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f4" name="occupation_f4"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf4 col-xl-4" id="form__group-db_lawf4">

                          <div class="form__group__icon-input">
                            <label for="db_lawf4" class="form__label">Date Background Check</label>

                            <label for="db_lawf4" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf4" name="db_lawf4"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file4 col-xl-8" id="form__group-db_file4">

                          <div class="form__group__icon-input">
                            <label for="db_file4" class="form__label">Background Check</label>

                            <label for="db_file4" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem4" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family5"
                        aria-expanded="false"> Add Family Member </button>

                    </div>

                    <div id="family5" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 6</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name5 col-xl-4" id="form__group-f_name5">

                          <div class="form__group__icon-input">
                            <label for="f_name5" class="form__label">Name</label>

                            <label for="f_name5" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name5" name="f_name5"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname5 col-xl-4" id="form__group-f_lname5">

                          <div class="form__group__icon-input">
                            <label for="f_lname5" class="form__label">Last Name</label>

                            <label for="f_lname5" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname5" name="f_lname5"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db5 col-xl-4" id="form__group-db5">

                          <div class="form__group__icon-input">
                            <label for="db5" class="form__label">Date of Birth</label>

                            <label for="db5" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db5" name="db5"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender5 col-xl-4" id="form__group-gender5">

                          <div class="form__group__icon-input">
                            <label for="gender5" class="form__label">Gender</label>

                            <label for="gender5" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender5" name="gender5">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re5 col-xl-4" id="form__group-re5">

                          <div class="form__group__icon-input">
                            <label for="re5" class="form__label">Relation</label>

                            <label for="re5" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re5" name="re5">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f5 col-xl-4" id="form__group-occupation_f5">

                          <div class="form__group__icon-input">
                            <label for="occupation_f5" class="form__label">Occupation</label>

                            <label for="occupation_f5" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f5" name="occupation_f5"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf5 col-xl-4" id="form__group-db_lawf5">

                          <div class="form__group__icon-input">
                            <label for="db_lawf5" class="form__label">Date Background Check</label>

                            <label for="db_lawf5" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf5" name="db_lawf5"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file5 col-xl-8" id="form__group-db_file5">

                          <div class="form__group__icon-input">
                            <label for="db_file5" class="form__label">Background Check</label>

                            <label for="db_file5" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem5" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family6"
                        aria-expanded="false"> Add Family Member </button>

                    </div>

                    <div id="family6" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 7</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name6 col-xl-4" id="form__group-f_name6">

                          <div class="form__group__icon-input">
                            <label for="f_name6" class="form__label">Name</label>

                            <label for="f_name6" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name6" name="f_name6"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname6 col-xl-4" id="form__group-f_lname6">

                          <div class="form__group__icon-input">
                            <label for="f_lname6" class="form__label">Last Name</label>

                            <label for="f_lname6" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname6" name="f_lname6"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db6 col-xl-4" id="form__group-db6">

                          <div class="form__group__icon-input">
                            <label for="db6" class="form__label">Date of Birth</label>

                            <label for="db6" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db6" name="db6"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender6 col-xl-4" id="form__group-gender6">

                          <div class="form__group__icon-input">
                            <label for="gender6" class="form__label">Gender</label>

                            <label for="gender6" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender6" name="gender6">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re6 col-xl-4" id="form__group-re6">

                          <div class="form__group__icon-input">
                            <label for="re6" class="form__label">Relation</label>

                            <label for="re6" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re6" name="re6">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f6 col-xl-4" id="form__group-occupation_f6">

                          <div class="form__group__icon-input">
                            <label for="occupation_f6" class="form__label">Occupation</label>

                            <label for="occupation_f6" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f6" name="occupation_f6"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf6 col-xl-4" id="form__group-db_lawf6">

                          <div class="form__group__icon-input">
                            <label for="db_lawf6" class="form__label">Date Background Check</label>

                            <label for="db_lawf6" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf6" name="db_lawf6"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file6 col-xl-8" id="form__group-db_file6">

                          <div class="form__group__icon-input">
                            <label for="db_file6" class="form__label">Background Check</label>

                            <label for="db_file6" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem6" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family7"
                        aria-expanded="false"> Add Family Member </button>


                    </div>

                    <div id="family7" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 8</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name7 col-xl-4" id="form__group-f_name7">

                          <div class="form__group__icon-input">
                            <label for="f_name7" class="form__label">Name</label>

                            <label for="f_name7" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name7" name="f_name7"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname7 col-xl-4" id="form__group-f_lname7">

                          <div class="form__group__icon-input">
                            <label for="f_lname7" class="form__label">Last Name</label>

                            <label for="f_lname7" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname7" name="f_lname7"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db7 col-xl-4" id="form__group-db7">

                          <div class="form__group__icon-input">
                            <label for="db7" class="form__label">Date of Birth</label>

                            <label for="db7" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db7" name="db7"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender7 col-xl-4" id="form__group-gender7">

                          <div class="form__group__icon-input">
                            <label for="gender7" class="form__label">Gender</label>

                            <label for="gender7" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender7" name="gender7">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re7 col-xl-4" id="form__group-re7">

                          <div class="form__group__icon-input">
                            <label for="re7" class="form__label">Relation</label>

                            <label for="re7" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re7" name="re7">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f7 col-xl-4" id="form__group-occupation_f7">

                          <div class="form__group__icon-input">
                            <label for="occupation_f7" class="form__label">Occupation</label>

                            <label for="occupation_f7" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f7" name="occupation_f7"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf7 col-xl-4" id="form__group-db_lawf7">

                          <div class="form__group__icon-input">
                            <label for="db_lawf7" class="form__label">Date Background Check</label>

                            <label for="db_lawf7" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf7" name="db_lawf7"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file7 col-xl-8" id="form__group-db_file7">

                          <div class="form__group__icon-input">
                            <label for="db_file7" class="form__label">Background Check</label>

                            <label for="db_file7" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem7" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                      <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family8"
                        aria-expanded="false"> Add Family Member </button>

                    </div>

                    <div id="family8" class="panel-collapse collapse mt-3 pt-2">

                      <h4 class="subtitle__group-section mr-auto ml-auto" style="margin-bottom: 3rem">Member 9</h4>

                      <div class="step__body row m-0 d-flex justify-content-center">

                        <article class="form__group form__group-f_name8 col-xl-4" id="form__group-f_name8">

                          <div class="form__group__icon-input">
                            <label for="f_name8" class="form__label">Name</label>

                            <label for="f_name8" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_name8" name="f_name8"
                              placeholder="e.g. Melissa">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-f_lname8 col-xl-4" id="form__group-f_lname8">

                          <div class="form__group__icon-input">
                            <label for="f_lname8" class="form__label">Last Name</label>

                            <label for="f_lname8" class="icon"><i class="icon icon_mname"></i></label>


                            <input type="text" class="form__group-input f_name1" id="f_lname8" name="f_lname8"
                              placeholder="e.g. Smith">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db8 col-xl-4" id="form__group-db8">

                          <div class="form__group__icon-input">
                            <label for="db8" class="form__label">Date of Birth</label>

                            <label for="db8" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db8" name="db8"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-gender8 col-xl-4" id="form__group-gender8">

                          <div class="form__group__icon-input">
                            <label for="gender8" class="form__label">Gender</label>

                            <label for="gender8" class="icon"><i class="icon icon_gender"></i></label>

                            <select class="custom-select form__group-input f_name1" id="gender8" name="gender8">
                              <option hidden="option" selected disabled>-- Select Gender --</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Private">Private</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-re8 col-xl-4" id="form__group-re8">

                          <div class="form__group__icon-input">
                            <label for="re8" class="form__label">Relation</label>

                            <label for="re8" class="icon"><i class="icon icon_family"></i></label>


                            <select class="custom-select form__group-input f_name1" id="re8" name="re8">
                              <option hidden="option" selected disabled>-- Select Relation --</option>
                              <option value="Dad">Dad</option>
                              <option value="Mom">Mom</option>
                              <option value="Son">Son</option>
                              <option value="Daughter">Daughter</option>
                              <option value="Grandparents">Grandparents</option>
                              <option value="Other">Others</option>
                            </select>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-occupation_f8 col-xl-4" id="form__group-occupation_f8">

                          <div class="form__group__icon-input">
                            <label for="occupation_f8" class="form__label">Occupation</label>

                            <label for="occupation_f8" class="icon"><i class="icon icon_bcheck"></i></label>


                            <input type="text" class="form__group-input f_name1" id="occupation_f8" name="occupation_f8"
                              placeholder="e.g. Lawyer">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_lawf8 col-xl-4" id="form__group-db_lawf8">

                          <div class="form__group__icon-input">
                            <label for="db_lawf8" class="form__label">Date Background Check</label>

                            <label for="db_lawf8" class="icon"><i class="icon icon_db"></i></label>


                            <input type="text" class="form__group-input f_name1" id="db_lawf8" name="db_lawf8"
                              placeholder="MM/DD/YYYY" readonly>

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                        <article class="form__group form__group-db_file8 col-xl-8" id="form__group-db_file8">

                          <div class="form__group__icon-input">
                            <label for="db_file8" class="form__label">Background Check</label>

                            <label for="db_file8" class="icon"><i class="icon icon_db"></i></label>


                            <input type="file" class="form__group-input f_name1" name="b_check_mem8" id="profile-img"
                              maxlength="1" accept="pdf" style="width: 100%;">

                          </div>
                          <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                            characters
                          </p>

                        </article>

                      </div>

                    </div>

                  </div>

                </section>

              </section>

              <div class="step__footer">

                <button type="submit" id="back_step-1" class="btn step__button--back" data-to_step="3"
                  data-step="4">Back</button>

                <button type="submit" class="btn btn__next step__button--next" data-to_step="5"
                  data-step="4">Next</button>
              </div>

            </form>

          </div>




          <!-- Step 5 -->

          <div class="step" id="step-5">

            <form action="#" method="POST" class="form__register" autocomplete="off" enctype="multipart/form-data">

              <section class="d-flex justify-content-center">

                <section class="card_step card col-md-10 p-0">

                  <div class="step__header">
                    <h2 class="title__card">Additional Information</h2>
                  </div>

                  <div class="step__body row d-flex justify-content-center w-100 ml-auto mr-auto mt-3">

                    <article class="form__group form__group-a_pre col-xl-12" id="form__group-a_pre">

                      <div class="form__group__icon-input">
                        <label for="a_pre" class="form__label">Academy Preference</label>

                        <label for="a_pre" class="icon"><i class="icon icon_p_academy"></i></label>

                        <select class="custom-select form__group-input academy" id="a_pre" name="a_pre">
                          <?php
                          while ($row6 = $query6->fetch_array()) {
                          ?>
                          <option hidden="option" selected disabled> -- Select Preference -- </option>
                          <option value=" <?php echo $row6['id_ac'] ?> ">
                            <?php echo $row6['name_a']; ?><?php echo ', ' ?><?php echo $row6['dir_a']; ?>
                          </option>

                          <?php
                          }
                          ?>

                          <option value="Other">Other</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>

                    <article class="form__group form__group-backg col-xl-4" id="form__group-backg">

                      <div class="form__group__icon-input">
                        <label for="backg" class="form__label">Background</label>

                        <label for="backg" class="icon"><i class="icon icon_bcheck"></i></label>


                        <input type="text" class="form__group-input backg" id="backg" name="backg"
                          placeholder="e.g. Canadian">

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-religion col-xl-4" id="form__group-religion">

                      <div class="form__group__icon-input">
                        <label for="religion" class="form__label">Religion to which you belong?</label>

                        <label for="religion" class="icon"><i class="icon icon_bcheck"></i></label>

                        <select name="religion" id="religion" class="custom-select form__group-input religion"
                          onchange="otherReligion()">

                          <option hidden="option" selected disabled> -- Select Option -- </option>
                          <option value="Yes"> Yes </option>
                          <option value="No"> No </option>

                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-religion col-xl-4 d-none" id="group-religion">
                    </article>

                    <article class="form__group form__group-misdemeanor col-xl-4" id="form__group-misdemeanor">

                      <div class="form__group__icon-input">
                        <label for="misdemeanor" class="form__label">Have they committed a misdemeanor?</label>

                        <label for="misdemeanor" class="icon"><i class="icon icon_bcheck"></i></label>

                        <select name="misdemeanor" id="misdemeanor" class="custom-select form__group-input religion"
                          onchange="otherMisdemeanor()">

                          <option hidden="option" selected disabled> -- Select Option -- </option>
                          <option value="Yes"> Yes </option>
                          <option value="No"> No </option>

                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-misdemeanor col-xl-4 d-none" id="group-misdemeanor">
                    </article>

                    <article class="form__group form__group-c_background col-xl-12" id="form__group-c_background">

                      <div class="form__group__icon-input">
                        <label for="c_background" class="form__label">Do you give us your consent to go to the
                          authorities
                          and check your criminal background check?</label>

                        <label for="c_background" class="icon"><i class="icon icon_bcheck"></i></label>

                        <select name="c_background" id="c_background" class="custom-select form__group-input religion">

                          <option hidden="option" selected disabled> -- Select Option -- </option>
                          <option value="Yes"> Yes </option>
                          <option value="No"> No </option>

                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                  </div>

                  <h2 class="title__group-section mr-auto ml-auto" style="margin-bottom: 1rem">Health Information
                  </h2>

                  <div class="step__body row d-flex justify-content-center w-100 ml-auto mr-auto mt-3">

                    <article class="form__group form__group-smoke col-xl-4" id="form__group-smoke">

                      <div class="form__group__icon-input">
                        <label for="smoke" class="form__label">Smokers Politics</label>

                        <label for="smoke" class="icon"><i class="icon icon_smokers"></i></label>


                        <select class="custom-select form__group-input smoke" id="smoke" name="smoke">
                          <option hidden="option" selected disabled>-- Select Preference --</option>
                          <option value="Outside-OK">Outside-OK</option>
                          <option value="Inside-OK">Inside-OK</option>
                          <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters
                      </p>

                    </article>



                    <article class="form__group form__group-allergies col-xl-4" id="form__group-allergies">

                      <div class="form__group__icon-input">
                        <label for="allergies" class="form__label">Have Allergies?</label>

                        <label for="allergies" class="icon"><i class="icon icon_allergies2"></i></label>

                        <select class="custom-select form__group-input allergies_select" id="allergies" name="allergies"
                          onchange="otherAllergy()">
                          <option hidden="option" selected disabled>-- Select Option --</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-allergies col-xl-4 d-none" id="group-allergies">
                    </article>

                    <article class="form__group form__group-medic_f col-xl-4 " id="form__group-medic_f">

                      <div class="form__group__icon-input">
                        <label for="medic_f" class="form__label">Take any Medication?</label>

                        <label for="medic_f" class="icon"><i class="icon icon_allergies"></i></label>

                        <select class="custom-select form__group-input medic_f" id="medic_f" name="medic_f"
                          onchange="otherMedication()">
                          <option hidden="option" selected disabled>-- Select Option --</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-allergies col-xl-4  d-none" id="group-medic_f">
                    </article>

                    <article class="form__group form__group-condition_m col-xl-4" id="form__group-condition_m">

                      <div class="form__group__icon-input">
                        <label for="condition_m" class="form__label">Any Physical or Mental condition?</label>

                        <label for="condition_m" class="icon"><i class="icon icon_bcheck"></i></label>

                        <select name="condition_m" id="condition_m" class="custom-select form__group-input religion"
                          onchange="otherCondition()">

                          <option hidden="option" selected disabled> -- Select Option -- </option>
                          <option value="Yes"> Yes </option>
                          <option value="No"> No </option>

                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-condition_m col-xl-4 d-none" id="group-condition_m">
                    </article>

                    <article class="form__group form__group-health_f col-xl-4" id="form__group-health_f">

                      <div class="form__group__icon-input">
                        <label for="health_f" class="form__label">Have Health Problems?</label>

                        <label for="health_f" class="icon"><i class="icon icon_allergies"></i></label>

                        <select class="custom-select form__group-input health_f" id="health_f" name="health_f"
                          onchange="otherHealth()">
                          <option hidden="option" selected disabled>-- Select Option --</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>

                      </div>
                      <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4
                        characters
                      </p>

                    </article>

                    <article class="form__group form__group-allergies col-xl-4 d-none" id="group-health_f">
                    </article>

                  </div>

                  <script>
                  const $allergy = document.querySelector("#allergies");

                  function otherAllergy() {

                    const allergyI = $allergy.selectedIndex;

                    if (allergyI === -1) return; // Esto es cuando no hay elementos

                    const optionAllergy = $allergy.options[allergyI];

                    if (optionAllergy.value == 'Yes') {

                      document.getElementById('group-allergies').classList.remove('d-none');

                      $('#group-allergies').html(`
                        
                                                    <label class="label__other__city">Specify the Allergy</label>
                                                    <input class="input__other_city" type="text" name="allergies_spe"> 
                        
                                                `);

                    } else {
                      document.getElementById('group-allergies').classList.add('d-none');
                    }

                  }

                  const $medication = document.querySelector("#medic_f");

                  function otherMedication() {

                    const medicationI = $medication.selectedIndex;

                    if (medicationI === -1) return; // Esto es cuando no hay elementos

                    const optionMedication = $medication.options[medicationI];

                    if (optionMedication.value == 'Yes') {

                      document.getElementById('group-medic_f').classList.remove('d-none');

                      $('#group-medic_f').html(`
                        
                                                    <label class="label__other__city">Specify the Medication</label>
                                                    <input class="input__other_city" type="text" name="medic_f_spe"> 
                        
                                                `);

                    } else {
                      document.getElementById('group-medic_f').classList.add('d-none');
                    }

                  }

                  const $health = document.querySelector("#health_f");

                  function otherHealth() {

                    const healthI = $health.selectedIndex;

                    if (healthI === -1) return; // Esto es cuando no hay elementos

                    const optionHealth = $health.options[healthI];

                    if (optionHealth.value == 'Yes') {

                      document.getElementById('group-health_f').classList.remove('d-none');

                      $('#group-health_f').html(`
                        
                                                    <label class="label__other__city">Specify the Problems</label>
                                                    <input class="input__other_city" type="text" name="health_f_spe"> 
                        
                                                `);

                    } else {
                      document.getElementById('group-health_f').classList.add('d-none');
                    }

                  }
                  const $misdemeanor = document.querySelector("#misdemeanor");

                  function otherMisdemeanor() {

                    const misdemeanorI = $misdemeanor.selectedIndex;

                    if (misdemeanorI === -1) return; // Esto es cuando no hay elementos

                    const optionMisdemeanor = $misdemeanor.options[misdemeanorI];

                    if (optionMisdemeanor.value == 'Yes') {

                      document.getElementById('group-misdemeanor').classList.remove('d-none');

                      $('#group-misdemeanor').html(`

                                                    <label class="label__other__city">Specify?</label>
                                                    <input class="input__other_city" type="text" name="misdemeanor_spe"> 

                                                `);

                    } else {
                      document.getElementById('group-misdemeanor').classList.add('d-none');
                    }

                  }

                  const $religion = document.querySelector("#religion");

                  function otherReligion() {

                    const religionI = $religion.selectedIndex;

                    if (religionI === -1) return; // Esto es cuando no hay elementos

                    const optionReligion = $religion.options[religionI];

                    if (optionReligion.value == 'Yes') {

                      document.getElementById('group-religion').classList.remove('d-none');

                      $('#group-religion').html(`

                                                    <label class="label__other__city">Which Religion?</label>
                                                    <input class="input__other_city" type="text" name="religion_spe"> 

                                                `);

                    } else {
                      document.getElementById('group-religion').classList.add('d-none');
                    }

                  }


                  const $condition = document.querySelector("#condition_m");

                  function otherCondition() {

                    const conditionI = $condition.selectedIndex;

                    if (conditionI === -1) return; // Esto es cuando no hay elementos

                    const optionCondition = $condition.options[conditionI];

                    if (optionCondition.value == 'Yes') {

                      document.getElementById('group-condition_m').classList.remove('d-none');

                      $('#group-condition_m').html(`

                                                    <label class="label__other__city">Which Condition?</label>
                                                    <input class="input__other_city" type="text" name="condition_m_spe"> 

                                                `);

                    } else {
                      document.getElementById('group-condition_m').classList.add('d-none');
                    }

                  }
                  </script>




                </section>

              </section>




              <input type="text" value="next_step-5" name="value_step" id="value_step-5" hidden>


              <div class="step__footer">

                <button type="submit" id="back_step-3" class="btn step__button--back" data-to_step="4"
                  data-step="5">Back</button>

                <button type="submit" id="register" class="btn btn__next step__button--next">Register</button>
              </div>

            </form>

          </div>

        </div>



      </section>



    </div>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>
  <div id="house_ass" style="display: none;">
    <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-9" id="assh">

        </div>
      </div>
    </div>

  </div>

  <!-- // TODO MODAL DYNAMIC -->
  <div id="modal-dynamic" class="modal-dynamic">

    <section class="modal-content">
      <span class="close">X</span>
      <h3>Title</h3>


      <div class="modal-empty">
        <img src="../assets/img/homestay_home.png" alt="empty" rel="noopener">
      </div>


      <table class="modal-table">
        <thead>
          <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Bed</th>
            <th>Start</th>
            <th>End</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </section>

  </div>


  <?php include 'footer.php' ?>

  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    }

  }

  function previewImageI() {
    var file = document.getElementById("file-i").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview-i").setAttribute("src", event.target.result);
        document.getElementById("preview-i").classList.remove("d-none");
      };

      fileReader.readAsDataURL(file[0]);

      var label_11 = document.getElementById("label-ii");
      var label_11_1 = document.getElementById("label-ii-i");

      label_11.style.display = 'none';
      label_11_1.style.display = 'inline';

    } else {
      document.getElementById("preview-i").classList.add("d-none");
    }

  }

  function previewImageII() {
    var file = document.getElementById("file-ii").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview-ii").setAttribute("src", event.target.result);
        document.getElementById("preview-ii").classList.remove("d-none");
      };

      fileReader.readAsDataURL(file[0]);

      var label_111 = document.getElementById("label-iii");
      var label_111_1 = document.getElementById("label-iii-i");

      label_111.style.display = 'none';
      label_111_1.style.display = 'inline';

    } else {
      document.getElementById("preview-ii").classList.add("d-none");
    }

  }
  </script>

  <!-- Daterangepicker -->

  <script>
  $(document).ready(function() {

    $('#y_service').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#y_service').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#y_service').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });



    $('#db').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db1').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db2').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db2').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db2').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db3').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db3').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db3').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db4').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db4').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db4').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db5').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db5').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db5').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db6').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db6').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db6').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db7').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db7').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db7').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db8').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db8').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db8').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });


    $('#db_lawf1').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf2').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf2').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf2').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });


    $('#db_lawf3').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf3').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf3').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf4').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf4').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf4').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf5').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf5').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf5').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf6').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf6').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf6').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf7').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf7').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf7').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_lawf8').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_lawf8').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_lawf8').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });

    $('#db_law').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf('hour'),
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });

    $('#db_law').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
    });

    $('#db_law').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });



  });
  </script>

  <!-- WRAPPER
    =================================================================================================================-->

  <!--end page-->

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/galery-homestay.js?ver=1.0.5.4.1"></script>
  <script src="assets/js/rooms_register.js"></script>
  <script src="assets/js/validate_form.js?ver=1.0.5.4.6"></script>


  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


</body>

</html>