<?php
// TODO WINYERSON
include_once('../xeon.php');
include '../cript.php';
session_start();
error_reporting(0);

if (!$_POST || !$_SESSION) exit;
$usuario = $_SESSION['username'];
$homestayId = $_POST['homestay_id'];

// TODO CONSULTAS
$queryUser = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row_user = $queryUser->fetch_assoc();

$queryUserHome = $link->query("SELECT * FROM users WHERE mail = '$row_home[mail_h]' ");
$row_user_home = $queryUserHome->fetch_assoc();

$queryHome = $link->query("SELECT * FROM pe_home WHERE id_home = '$homestayId' ");
$row_home = $queryHome->fetch_assoc();

$queryAgents = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
$row_agent = $queryAgents->fetch_assoc();

$queryManager = $link->query("SELECT * FROM manager WHERE id_m = '$row_agent[id_m]' ");
$row_manager = $queryManager->fetch_assoc();

$queryAcademy = $link->query("SELECT * FROM academy");

$queryPControl = $link->query("SELECT * FROM propertie_control WHERE id_home = '$homestayId'");
$row_control = $queryPControl->fetch_assoc();

// TODO CONSULTAS INNER JOIN
$queryRoom_IJ = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$homestayId' and room.id_home = pe_home.id_home");
$row_room_IJ = $queryRoom_IJ->fetch_assoc();

$queryMemF = $link->query("SELECT * FROM mem_f WHERE id_home = '$homestayId' ");
$row_memF = $queryMemF->fetch_assoc();

$queryMemF_IJ = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$homestayId' and mem_f.id_home = pe_home.id_home");
$row_memF_IJ = $queryMemF_IJ->fetch_assoc();

$queryPhotoHome_IJ = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$homestayId' and photo_home.id_home = pe_home.id_home");
$row_photo_home_IJ = $queryPhotoHome_IJ->fetch_assoc();

$queryAcademy_IJ = $link->query("SELECT * FROM academy INNER JOIN pe_home ON pe_home.id_home = '$homestayId' AND academy.id_ac = pe_home.a_pre");
$row_academy_IJ = $queryAcademy_IJ->fetch_assoc();



while ($value = mysqli_fetch_array($queryAcademy)) $allAcademys[] = array($value);


$jsonToSend = array(
  'basicInformation' => array(
    'houseInformation' => array(
      'name' => $row_home['h_name'],
      'phone' => $row_home['num'],
      'totalRooms' => $row_home['room'],
      'typeResidence' => $row_home['h_type'],
      'mail' => $row_home['mail_h'],
    ),
    'locationInformation' => array(
      'mainCity' => $row_home['m_city'],
      'address' => $row_home['dir'],
      'city' => $row_home['city'],
      'state_province' => $row_home['state'],
      'postalCode' => $row_home['p_code'],
    ),
  ),
  'houseGallery' => array(
    'frontage' => $row_home['phome'],
    'livingRoom' => $row_photo_home_IJ['pliving'],
    'familyPicture' => $row_photo_home_IJ['fp'],
    'kitchen' => $row_photo_home_IJ['parea1'],
    'diningRoom' => $row_photo_home_IJ['parea2'],
    'area3' => $row_photo_home_IJ['parea3'],
    'area4' => $row_photo_home_IJ['parea4'],
    'bathroom1' => $row_photo_home_IJ['pbath1'],
    'bathroom2' => $row_photo_home_IJ['pbath2'],
    'bathroom3' => $row_photo_home_IJ['pbath3'],
    'bathroom4' => $row_photo_home_IJ['pbath4'],
  ),
  'additionalInformation' => array(
    'description' => $row_home['des'],
    'preferences' => array(
      'academy' => [$allAcademys, $row_home['a_pre']],
      'gender' => $row_home['g_pre'],
      'age' => $row_home['ag_pre'],
      'smokers' => $row_home['smoke'],
      'mealsService' => $row_home['m_service'],
      'totalYears' => $row_home['y_service'],
      'pets' => array(
        'response' => $row_home['pet'],
        'totalPets' => $row_home['pet_num'],
        'kindPet' => $row_home['type_pet'],
        'dog' => $row_home['dog'],
        'cat' => $row_home['cat'],
        'other' => $row_home['other'],
      )
    ),
    'specialDiet' => array(
      'vegetarians' => $row_home['vegetarians'],
      'halal' => $row_home['halal'],
      'kosher' => $row_home['kosher'],
      'lactose' => $row_home['lactose'],
      'gluten' => $row_home['gluten'],
      'pork' => $row_home['pork'],
    ),
  ),
  'familyInformation' => array(
    // * ANY MEMBER OF YOUR FAMILY
    'anyMembers' => array(
      'allergies' => $row_home['allergies'],
      'medication' => $row_home['medic_f'],
      'health' => $row_home['health_f'],
    ),
    // * MAIN CONTACT INFORMATION
    'contact' => array(
      'name' => $row_home['name_h'],
      'lastName' => $row_home['l_name_h'],
      'dateBirth' => $row_home['db'],
      'gender' => $row_home['gender'],
      'phone' => $row_home['cell'],
      'occupation' => $row_home['occupation_m'],
      'backgroundDate' => $row_home['db_law'],
      'pdf' => $row_home['law'],
    ),
    // * FAMILY PREFERENCES
    'preferences' => array(
      'totalMembers' => $row_home['num_mem'],
      'background' => $row_home['backg'],
      'backgroundLanguage' => $row_home['backl'],
      'religion' => $row_home['religion'],
      'conditionMental' => $row_home['condition_m'],
      'misdemeanor' => $row_home['misdemeanor'],
      'certifiedBackground' => $row_home['c_background'],
    ),
    // * ADDITIONAL MEMBER
    'additionalMembers' => $row_memF,
  ),
);

echo json_encode($jsonToSend);