<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_student WHERE id_student = '$id' ");
$row=$query->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$id' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
$row6=$query6->fetch_assoc();

 if ($row5['usert'] != 'Agent') {
        
         header("location: ../logout.php");   
        
    }

    if ($row6['id_ag'] != $row['id_ag']) {
            header("location: index.php");   
        
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Advance Options</title>

</head>
<body style="background-color: #F3F8FC;">

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main" style="background-color: #F3F8FC;">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <h1>Student Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
       <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

                        <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1"><i class="fa fa-cog"></i> Advance Options</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
         <button class="btn btn-info" data-toggle="collapse" data-target="#disable1" style="color: white; background-color: #982A72; border: 2px solid #982A72;"><i class="fa fa-user-times"></i> Disable Student</button>

<div id="disable1" class="collapse">
    <br>
<h3>Disable Student</h3>
        <p>The student will be disable immediately.<br/>

        This action will <b>disable the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its files, information and events.<br/>

        <b>If you wish disable this user account press the button?</b></p>
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2" style="color: white; background-color: #982A72; border: 2px solid #982A72;"><i class="fa fa-user-times"></i> Disable Student</button>

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Disable Student</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
      </div>
      <div class="modal-body">
      <div class="modal-body" style="background-color: #DFBABA;"><p>
         <i class="fa fa-asterisk"></i> You are about to disable this student<br/>
        Once a student is disable the user will not login in homebor until his user has been reactivated again. its files, all information and events will be not removing of the data.
      </p></div></div>
      <div class="modal-body">
        <p><b>All Fields Required</b></p>
        <p>Please type the following to confirm:<br>

<?php echo "<b>".$row['id_student']."</b>";?></p>
        <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agent.php" method="post" enctype="multipart/form-data">
            <div class="row">

                                    <!--House Name-->
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="del_id" name="del_id" required>
                                            <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>
                                            <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>
                                            <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>
                                             <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row6[id_m]";?>" hidden>
                                            <label for="des">Reason</label>
                                            <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                        </div>
                                    </div>
                                </div>
                    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="color: white; background-color: #232159; border: 2px solid #232159;">Close</button>
        <button type="submit" id="submit" name="disable" class="btn btn-default" style="color: white; background-color: #982A72; border: 2px solid #982A72;">
                                   <i class="fa fa-user-times"></i> Disable</button>
        </form>
      </div>
    </div>

  </div>
</div>
<br>
<br>
</div>
<br>
<br>
<button data-toggle="collapse" class="btn btn-info" data-target="#activate" style="color: white; background-color: #232159; border: 2px solid #232159;"><i class="fa fa-user-plus"></i> Activate Student</button>

<div id="activate" class="collapse">
          <br>
          <h3>Activate Student</h3>
        <p>The student will be reactivate immediately.<br/>

        This action will <b>reactivate the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its files, information and events.<br/>

        <b>If you wish reactivate this user account press the button?</b></p>
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal22" style="color: white; background-color: #232159; border: 2px solid #232159;"><i class="fa fa-user-plus"></i> Reactivate Student</button>

<!-- Modal -->
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reactivate Student</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
      </div>
      <div class="modal-body">
      <div class="modal-body" style="background-color: #DFBABA;"><p>
         <i class="fa fa-asterisk"></i> You are about to reactivate this student<br/>
        Once a student is reactivate the user will login in homebor.
      </p></div></div>
      <div class="modal-body">
        <p><b>All Fields Required</b></p>
        <p>Please type the following to confirm:<br>

<?php echo "<b>".$row['id_student']."</b>";?></p>
        <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agent.php" method="post" enctype="multipart/form-data">
            <div class="row">

                                    <!--House Name-->
                                   <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="del_id" name="del_id" required>
                                            <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>
                                            <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>
                                            <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>
                                            <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row6[id_m]";?>" hidden>
                                            <label for="des">Reason</label>
                                            <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                        </div>
                                    </div>
                                </div>
                    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="color: white; background-color: #982A72; border: 2px solid #982A72;">Close</button>
        <button type="submit" id="submit" name="reactivate" class="btn btn-default" style="color: white; background-color: #232159; border: 2px solid #232159;">
                                   <i class="fa fa-user-plus"></i> Reactivate</button>
        </form>
      </div>
    </div>

  </div>
</div>
<br>        
</div>
<br>
<br>
<button data-toggle="collapse" class="btn btn-info" data-target="#delete1"style="color: white; background-color: #982A72; border: 2px solid #982A72;"><i class="fa fa-eraser"></i> Delete Student</button>



<div id="delete1" class="collapse">
 <!-- Trigger the modal with a button -->
 <br>
 <!-- Trigger the modal with a button -->
        <h3 style="color: red;">Delete Student</h3>
        <p>The Student will be permanently deleted immediately.<br/>

        This action will <b>permanently delete</b> <?php echo "".$row['mail_s']."";?> <b>immediately</b>, including its files, information and events.<br/>

        <b>Are you ABSOLUTELY SURE you wish to delete this student?</b></p>
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" style="color: white; background-color: #982A72; border: 2px solid #982A72;"><i class="fa fa-eraser"></i> Delete Student</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="color: red;">Delete Student</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
      </div>
      <div class="modal-body">
      <div class="modal-body" style="background-color: #DFBABA;"><p>
         <i class="fa fa-asterisk"></i> You are about to permanently delete this student<br/>
Once a student is permanently deleted it cannot be recovered. Permanently deleting this student will immediately delete its files, all information and events.
      </p></div></div>
      <div class="modal-body">
        <p><b>All Fields Required</b></p>
        <p>This action cannot be undone. You will lose the student's information, events and all files.<br><br>

Please type the following to confirm:<br>

<?php echo "<b>".$row['id_student']."</b>";?></p>
        <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agent.php" method="post" enctype="multipart/form-data">
            <div class="row">

                                    <!--House Name-->
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="del_id" name="del_id" required>
                                            <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>
                                            <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>
                                            <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>
                                            <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row6[id_m]";?>" hidden>
                                            <label for="des">Reason</label>
                                            <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                        </div>
                                    </div>
                                </div>
                    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="color: white; background-color: #232159; border: 2px solid #232159;">Close</button>
        <button type="submit" id="submit" name="register" class="btn btn-default" style="color: white; background-color: #982A72; border: 2px solid #982A72;">
                                   <i class="fa fa-eraser"></i> Delete</button>
        </form>
      </div>
    </div>

  </div>
</div>
      </div>
</div>



        
      <br>
    </div>
  </div>
</div>


        

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Basic Information</h3>

                                <div class="text-center">
                                     <?php
                                    
                                        if($row['photo_s'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo_s'].'" width="200px;" height="200" style="margin-bottom: 20px;"/>';}
                                 
                                    ?>
                                </div>

                                 <div class="row">

                                    <!--Name-->
                                            <?php
                                            if($row['name_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Name</label>
                                        <p>".$row['name_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Last Name-->
                                   <?php
                                            if($row['l_name_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Last Name</label>
                                        <p>".$row['l_name_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Last Name-->
                                    <?php
                                            if($row['mail_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Mail</label>
                                        <p>".$row['mail_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                            </section>
                            <!--end #location-->
  <!--Personal
                                =====================================================================================-->
 <section id="location" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Personal Information</h3>

                                <!--Row-->
                                <div class="row">

                                    <!--Date of Birth-->
                                    <?php
                                            if($row['db_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Date of Birth</label>
                                        <p>".$row['db_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Date of Birth-->
                                    <?php
                                            if($row['db_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                        <label>Age</label>
                                        <br>";
                                         $from = new DateTime($row[db_s]);
                                    $to   = new DateTime('today');
                                    $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
                                    $sufix= " years old";   
                                        echo "<p>$age $sufix</p>
                                        </div>
                                    </div>";
                                    }
                                    ?>
                                    

                                   <!--Nacionality-->
                                    <?php
                                            if($row['nacionality'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Background</label>
                                        <p>".$row['nacionality']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--City-->
                                    <?php
                                            if($row['city'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Origin City</label>
                                        <p>".$row['city']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                

                                    <!--Passport-->
                                    <?php
                                            if($row['passport'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Passport</label>
                                        <p>".$row['passport']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Gender-->
                                    <?php
                                            if($row['gen_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Gender</label>
                                        <p>".$row['gen_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Date of Visa-->
                                    <?php
                                            if($row['db_visa'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Visa Expiration Date</label>
                                        <p>".$row['db_visa']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>
<!--File upload-->
                                    <?php
                                            if($row['visa'] == "NULL"){
                                            }
                                        else {
                                            echo " <!--File upload-->
                                                <div class='col-sm-4'>
                                                <div class='form-group'>
                                                <label for='bl'> Visa</label>
                                                    <br>
                                            <div class='embed-responsive embed-responsive-4by3'>";
                                        echo'<iframe class="embed-responsive-item" src="../'.$row['visa'].'" style="width: 100%;"></iframe>
                                            </div>';
                                        echo'<button><a href="../'.$row['visa'].'">Penal Antecedents Submitted</a></button>
                                                 </div>
                                            </div> ';
                                    }
                                    ?>
                                </div>

                            </section>

  <!--Academy Info
                                =====================================================================================-->
                            <section id="Academy_info" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Academy Information</h3>

                                <section id="academy-basic-info" class="mb-5">

                                <!--Row-->
                                <div class="row">

                                    <!--Academy Information-->
                                    <?php
                                            if($row['n_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Academy Name</label>
                                        <p>".$row2['name']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Origin Language-->
                                    <?php
                                            if($row['lang_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Origin Language</label>
                                        <p>".$row['lang_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Type Student-->
                                    <?php
                                            if($row['type_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Type of Student</label>
                                        <p>".$row['type_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                </div>
                                <!--end row-->

                                 <!--Row-->
                                <div class="row">

                                    <!--Arrive-->
                                    <?php
                                            if($row['firstd'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Arrive Date</label>
                                        <p>".$row['firstd']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Last-->
                                   <?php
                                            if($row['lastd'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Last Date</label>
                                        <p>".$row['lastd']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>
                                <!--end row-->
                            </section>
                            <!--end #location-->

                           <!--Preference Information
                                =====================================================================================-->
                            <section id="preference" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Preferences Information</h3>

                                <!--Row-->
                                <div class="row">

                                    <!--Smoker-->
                                    <?php
                                            if($row['smoke_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Smoker</label>
                                        <p>".$row['smoke_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Pets-->
                                    <?php
                                            if($row['pets'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Pets</label>
                                        <p>".$row['pets']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Food-->
                                    <?php
                                            if($row['food'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Food</label>
                                        <p>".$row['food']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                </div>
                                <!--end row-->

                            </section>
                            <!--end #additional-information-->

                             <!--Additional
                                =====================================================================================-->
                            <section id="additional-info" class="mb-5">


                                <!--Title-->
                                <h3 class="text-muted border-bottom">Additional Information</h3>

                                <!--Row-->
                                <div class="row">

                                    <!--Student Number-->
                                    <?php
                                            if($row['num_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Phone Number</label>
                                        <p>".$row['num_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Alternative Number-->
                                    <?php
                                            if($row['cell_s'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Alternative Number</label>
                                        <p>".$row['cell_s']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Emergency Number-->
                                    <?php
                                            if($row['num_conts'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Emergency Contact Number</label>
                                        <p>".$row['num_conts']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                </div>
                                <!--end row-->

                                 <!--Row-->
                                <div class="row">

                                    <!--Contact Name-->
                                    <?php
                                            if($row['cont_name'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Contact Name</label>
                                        <p>".$row['cont_name']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Contact Last Name-->
                                    <?php
                                            if($row['cont_lname'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Contact Last Name</label>
                                        <p>".$row['cont_lname']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                </div>
                                <!--end row-->
                            </section>
                            <!--
                            <hr>
                            
                            <section class="py-3">
                                <button type="submit" id="submit" name="register" onclick="window.location=''" class="btn btn-primary ts-btn-arrow btn-lg float-right" style="color: white; background-color: #232159; border: 2px solid #232159;">
                                   <i class="fa fa-save mr-2"></i> Submit
                                </button>
                            </section>-->

                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>

