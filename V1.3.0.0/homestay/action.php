<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

$pe_home = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
$rpe = $pe_home->fetch_assoc();

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');
$dateImage = date('YmdHisv');

/* ------------------------------------------- STEP 1 ----------------------------------------------------- */

if ($_POST['value_step'] == 'next_step-1') {

  if (empty($_POST['h_name'])) $h_name =  'NULL';
  else $h_name =  $_POST['h_name'];

  if (empty($_POST['num'])) $num =  'NULL';
  else $num =  $_POST['num'];

  if (empty($_POST['room'])) $room =  '0';
  else $room =  $_POST['room'];

  if (empty($_POST['m_city'])) $m_city =  'NULL';
  else $m_city =  $_POST['m_city'];

  if (empty($_POST['mail'])) $mail =  'NULL';
  else $mail =  $_POST['mail'];

  if (empty($_POST['pass'])) $pass =  'NULL';
  else $pass =  $_POST['pass'];

  if (empty($_POST['pet'])) $pet =  'NULL';
  else $pet =  $_POST['pet'];

  if (empty($_POST['pet_num'])) $pet_num =  '0';
  else $pet_num =  $_POST['pet_num'];

    if (empty($_POST['dog'])) $dog =  'no';
  else $dog =  $_POST['dog'];

  if (empty($_POST['cat'])) $cat =  'no';
  else $cat =  $_POST['cat'];

  if (empty($_POST['other'])) $other =  'no';
  else $other =  $_POST['other'];

  if (empty($_POST['type_pet'])) $type_pet =  'NULL';
  else $type_pet =  $_POST['type_pet'];

  if (empty($_POST['ag_pre'])) $ag_pre =  'NULL';
  else $ag_pre =  $_POST['ag_pre'];

  if (empty($_POST['g_pre'])) $g_pre =  'NULL';
  else $g_pre =  $_POST['g_pre'];

  $query = "UPDATE pe_home SET h_name='$h_name', num='$num', room='$room', m_city='$m_city', pet = '$pet', pet_num = '$pet_num', dog = '$dog', cat = '$cat', other = '$other', type_pet = '$type_pet', ag_pre='$ag_pre', g_pre='$g_pre' WHERE mail_h='$usuario'";

  $resultado = $link->query($query);

  $query10 = "UPDATE propertie_control SET h_name='$h_name', room='$room', g_pre='$g_pre', ag_pre='$ag_pre', pet = '$pet', status='Avalible' WHERE id_home = '$rpe[id_home]'";
  $result10 = $link->query($query10);

  if ($result10 == 1) {

    $json[] = array(
      'register' => 'Registered'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  } else {

    $json[] = array(
      'register' => 'Error'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  }
}



/* ------------------------------------------- STEP 2 ----------------------------------------------------- */



else if ($_POST['value_step'] == 'next_step-2' || isset($_POST['redirect'])) {

  /* ---------------------------------- LOCATION --------------------------------------------------  */

  if (empty($_POST['dir'])) $dir =  'NULL';
  else $dir =  $_POST['dir'];

  if (empty($_POST['city'])) $city =  'NULL';
  else $city =  $_POST['city'];

  if (empty($_POST['state'])) $state =  'NULL';
  else $state =  $_POST['state'];

  if (empty($_POST['p_code'])) $p_code =  'NULL';
  else $p_code =  $_POST['p_code'];

  if (empty($_POST['h_type'])) $h_type =  'NULL';
  else $h_type =  $_POST['h_type'];


  /* ---------------------------- INFORMATION FOR YOU STUDENTS -------------------------------- */

  if (empty($_POST['y_service'])) $y_service =  'NULL';
  else {
    $d_y_service = date_create($_POST['y_service']); //Se crea una fecha con el formato que recibes de la vista.
    $y_service = date_format($d_y_service, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($_POST['m_service'])) $m_service =  'NULL';
  else $m_service =  $_POST['m_service'];

  if (empty($_POST['num_mem'])) $num_mem =  '0';
  else $num_mem =  $_POST['num_mem'];

  if (empty($_POST['backl'])) $backl =  'NULL';
  else $backl =  $_POST['backl'];
  

  if (empty($_POST['vegetarians'])) $vegetarians = 'no';
  else $vegetarians = $_POST['vegetarians'];

  if (empty($_POST['halal'])) $halal =  'no';
  else $halal =  $_POST['halal'];

  if (empty($_POST['kosher'])) $kosher =  'no';
  else $kosher =  $_POST['kosher'];

  if (empty($_POST['lactose'])) $lactose =  'no';
  else $lactose =  $_POST['lactose'];
  
  if (empty($_POST['gluten'])) $gluten =  'no';
  else $gluten =  $_POST['gluten'];
  
  if (empty($_POST['pork'])) $pork = 'no';
  else $pork =  $_POST['pork'];
  
  if (empty($_POST['none'])) $none =  'no';
  else $none =  $_POST['none'];


  // TODO ---------------------------------- PHOTO ROOM ------------------------------------------------

  $paths = "../public/".$usuario;


    // TODO --------------------------------- Rooms Information ----------------------------------------

  if(
    ($_POST['type1'] != 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6']) || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7']) || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] == 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] != 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] != 'NULL' && $_POST['type7'] != 'NULL' && $_POST['type8'] == 'NULL') || 
    ($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] != 'NULL' && $_POST['type7'] != 'NULL' && $_POST['type8'] != 'NULL')
    ){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';


  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp4 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage4_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';

  
  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp5 = $_FILES['bed-v']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room6-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room6-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType6_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension6_2 = str_replace( "/", ".", $bedImageType6_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension6_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension6_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    }else $bedImage7_3Url = 'NULL';


  // * ROOM 8

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp8 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType8 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension8 = str_replace( "/", ".", $bedImageType8);
      $bedImage8Url = "public/".$usuario."/room8-a-" . $dateImage . $fileExtension8;
      $bedImage8 = "room8-a-" . $dateImage . $fileExtension8;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8, $paths . '/' . $bedImage8);

    }else $bedImage8Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage8_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp8_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType8_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension8_2 = str_replace( "/", ".", $bedImageType8_2);
      $bedImage8_2Url = "public/".$usuario."/room8-b-" . $dateImage . $fileExtension8_2;
      $bedImage8_2 = "room8-b-" . $dateImage . $fileExtension8_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8_2, $paths . '/' . $bedImage8_2);

    }else $bedImage8_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage8_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp8_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType8_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension8_3 = str_replace( "/", ".", $bedImageType8_3);
      $bedImage8_3Url = "public/".$usuario."/room8-c-" . $dateImage . $fileExtension8_3;
      $bedImage8_3 = "room8-c-" . $dateImage . $fileExtension8_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8_3, $paths . '/' . $bedImage8_3);

    } else $bedImage8_3Url = 'NULL';
  }

  else if($_POST['type1'] == 'NULL' && $_POST['type2'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp1 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);

    }else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage2_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp3 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';

  
  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp4 = $_FILES['bed-v']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage4_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';


  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp5 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';
  
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] == 'NULL' && $_POST['type3'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';
    
  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage2_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp3 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';

  
  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp4 = $_FILES['bed-v']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage4_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';


  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp5 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] == 'NULL' && $_POST['type4'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';
  
  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp3 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';

  
  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp4 = $_FILES['bed-v']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage4_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';


  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp5 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';

    
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] == 'NULL' && $_POST['type5'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';

  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp4 = $_FILES['bed-v']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage4_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';


  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp5 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';
  
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] == 'NULL' && $_POST['type6'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';


  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp4 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage4_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';

  
  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp5 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';

    
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] == 'NULL' && $_POST['type7'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';


  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp4 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage4_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';

  
  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp5 = $_FILES['bed-v']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  
  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room7-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension7_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';
  }

  else if($_POST['type1'] != 'NULL' && $_POST['type2'] != 'NULL' && $_POST['type3'] != 'NULL' && $_POST['type4'] != 'NULL' && $_POST['type5'] != 'NULL' && $_POST['type6'] != 'NULL' && $_POST['type7'] == 'NULL' && $_POST['type8'] != 'NULL'){
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';


  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp4 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage4_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';

  
  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp5 = $_FILES['bed-v']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room6-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room6-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType6_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension6_2 = str_replace( "/", ".", $bedImageType6_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension6_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension6_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  
  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    } else $bedImage7_3Url = 'NULL';
  }else{
  // * ROOM 1

    // * BED A

    if(!empty($_FILES['bed-i']['name'])){

      $bedImage_tmp1 = $_FILES['bed-i']['tmp_name'];
      $bedImageType = stristr($_FILES['bed-i']['type'], "/");
      $fileExtension = str_replace( "/", ".", $bedImageType);
      $bedImage1Url = "public/".$usuario."/room1-a-" . $dateImage . $fileExtension;
      $bedImage1 = "room1-a-" . $dateImage . $fileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1, $paths . '/' . $bedImage1);
      
    } else $bedImage1Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-i-ii']['name'])){

      $bedImage1_2 = $_FILES['bed-i-ii']['name'];
      $bedImage_tmp1_2 = $_FILES['bed-i-ii']['tmp_name'];
      $bedImageType1_2 = stristr($_FILES['bed-i-ii']['type'], "/");
      $fileExtension1_2 = str_replace( "/", ".", $bedImageType1_2);
      $bedImage1_2Url = "public/".$usuario."/room1-b-" . $dateImage . $fileExtension1_2;
      $bedImage1_2 = "room1-b-" . $dateImage . $fileExtension1_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_2, $paths . '/' . $bedImage1_2);

    }else $bedImage1_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-i-iii']['name'])){

      $bedImage1_3 = $_FILES['bed-i-iii']['name'];
      $bedImage_tmp1_3 = $_FILES['bed-i-iii']['tmp_name'];
      $bedImageType1_3 = stristr($_FILES['bed-i-iii']['type'], "/");
      $fileExtension1_3 = str_replace( "/", ".", $bedImageType1_3);
      $bedImage1_3Url = "public/".$usuario."/room1-c-" . $dateImage . $fileExtension1_3;
      $bedImage1_3 = "room1-c-" . $dateImage . $fileExtension1_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp1_3, $paths . '/' . $bedImage1_3);

    }else $bedImage1_3Url = 'NULL';


  // * ROOM 2

    // * BED A

    if(!empty($_FILES['bed-ii']['name'])){

      $bedImage_tmp2 = $_FILES['bed-ii']['tmp_name'];
      $bedImageType2 = stristr($_FILES['bed-ii']['type'], "/");
      $fileExtension2 = str_replace( "/", ".", $bedImageType2);
      $bedImage2Url = "public/".$usuario."/room2-a-" . $dateImage . $fileExtension2;
      $bedImage2 = "room2-a-" . $dateImage . $fileExtension2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2, $paths . '/' . $bedImage2);

    }else $bedImage2Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-ii-ii']['name'])){

      $bedImage2_2 = $_FILES['bed-ii-ii']['name'];
      $bedImage_tmp2_2 = $_FILES['bed-ii-ii']['tmp_name'];
      $bedImageType2_2 = stristr($_FILES['bed-ii-ii']['type'], "/");
      $fileExtension2_2 = str_replace( "/", ".", $bedImageType2_2);
      $bedImage2_2Url = "public/".$usuario."/room2-b-" . $dateImage . $fileExtension2_2;
      $bedImage2_2 = "room2-b-" . $dateImage . $fileExtension2_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_2, $paths . '/' . $bedImage2_2);

    }else $bedImage2_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-ii-iii']['name'])){

      $bedImage2_3 = $_FILES['bed-ii-iii']['name'];
      $bedImage_tmp2_3 = $_FILES['bed-ii-iii']['tmp_name'];
      $bedImageType2_3 = stristr($_FILES['bed-ii-iii']['type'], "/");
      $fileExtension2_3 = str_replace( "/", ".", $bedImageType2_3);
      $bedImage2_3Url = "public/".$usuario."/room2-c-" . $dateImage . $fileExtension2_3;
      $bedImage2_3 = "room2-c-" . $dateImage . $fileExtension2_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp2_3, $paths . '/' . $bedImage2_3);

    }else $bedImage2_3Url = 'NULL';


  // * ROOM 3

    // * BED A

    if(!empty($_FILES['bed-iii']['name'])){

      $bedImage_tmp3 = $_FILES['bed-iii']['tmp_name'];
      $bedImageType3 = stristr($_FILES['bed-iii']['type'], "/");
      $fileExtension3 = str_replace( "/", ".", $bedImageType3);
      $bedImage3Url = "public/".$usuario."/room3-a-" . $dateImage . $fileExtension3;
      $bedImage3 = "room3-a-" . $dateImage . $fileExtension3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3, $paths . '/' . $bedImage3);

    }else $bedImage3Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iii-ii']['name'])){

      $bedImage3_2 = $_FILES['bed-iii-ii']['name'];
      $bedImage_tmp3_2 = $_FILES['bed-iii-ii']['tmp_name'];
      $bedImageType3_2 = stristr($_FILES['bed-iii-ii']['type'], "/");
      $fileExtension3_2 = str_replace( "/", ".", $bedImageType3_2);
      $bedImage3_2Url = "public/".$usuario."/room3-b-" . $dateImage . $fileExtension3_2;
      $bedImage3_2 = "room3-b-" . $dateImage . $fileExtension3_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_2, $paths . '/' . $bedImage3_2);

    }else $bedImage3_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iii-iii']['name'])){
      
      $bedImage3_3 = $_FILES['bed-iii-iii']['name'];
      $bedImage_tmp3_3 = $_FILES['bed-iii-iii']['tmp_name'];
      $bedImageType3_3 = stristr($_FILES['bed-iii-iii']['type'], "/");
      $fileExtension3_3 = str_replace( "/", ".", $bedImageType3_3);
      $bedImage3_3Url = "public/".$usuario."/room3-c-" . $dateImage . $fileExtension3_3;
      $bedImage3_3 = "room3-c-" . $dateImage . $fileExtension3_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp3_3, $paths . '/' . $bedImage3_3);

    }else $bedImage3_3Url = 'NULL';


  // * ROOM 4

    // * BED A

    if(!empty($_FILES['bed-iv']['name'])){
      
      $bedImage_tmp4 = $_FILES['bed-iv']['tmp_name'];
      $bedImageType4 = stristr($_FILES['bed-iv']['type'], "/");
      $fileExtension4 = str_replace( "/", ".", $bedImageType4);
      $bedImage4Url = "public/".$usuario."/room4-a-" . $dateImage . $fileExtension4;
      $bedImage4 = "room4-a-" . $dateImage . $fileExtension4;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4, $paths . '/' . $bedImage4);

    }else $bedImage4Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-iv-ii']['name'])){

      $bedImage4_2 = $_FILES['bed-iv-ii']['name'];
      $bedImage_tmp4_2 = $_FILES['bed-iv-ii']['tmp_name'];
      $bedImageType4_2 = stristr($_FILES['bed-iv-ii']['type'], "/");
      $fileExtension4_2 = str_replace( "/", ".", $bedImageType4_2);
      $bedImage4_2Url = "public/".$usuario."/room4-b-" . $dateImage . $fileExtension4_2;
      $bedImage4_2 = "room4-b-" . $dateImage . $fileExtension4_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_2, $paths . '/' . $bedImage4_2);

    }else $bedImage4_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-iv-iii']['name'])){
      
      $bedImage4_3 = $_FILES['bed-iv-iii']['name'];
      $bedImage_tmp4_3 = $_FILES['bed-iv-iii']['tmp_name'];
      $bedImageType4_3 = stristr($_FILES['bed-iv-iii']['type'], "/");
      $fileExtension4_3 = str_replace( "/", ".", $bedImageType4_3);
      $bedImage4_3Url = "public/".$usuario."/room4-c-" . $dateImage . $fileExtension4_3;
      $bedImage4_3 = "room4-c-" . $dateImage . $fileExtension4_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp4_3, $paths . '/' . $bedImage4_3);

    }else $bedImage4_3Url = 'NULL';

  
  // * ROOM 5

    // * BED A

    if(!empty($_FILES['bed-v']['name'])){

      $bedImage_tmp5 = $_FILES['bed-v']['tmp_name'];
      $bedImageType5 = stristr($_FILES['bed-v']['type'], "/");
      $fileExtension5 = str_replace( "/", ".", $bedImageType5);
      $bedImage5Url = "public/".$usuario."/room5-a-" . $dateImage . $fileExtension5;
      $bedImage5 = "room5-a-" . $dateImage . $fileExtension5;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5, $paths . '/' . $bedImage5);

    }else $bedImage5Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-v-ii']['name'])){

      $bedImage5_2 = $_FILES['bed-v-ii']['name'];
      $bedImage_tmp5_2 = $_FILES['bed-v-ii']['tmp_name'];
      $bedImageType5_2 = stristr($_FILES['bed-v-ii']['type'], "/");
      $fileExtension5_2 = str_replace( "/", ".", $bedImageType5_2);
      $bedImage5_2Url = "public/".$usuario."/room5-b-" . $dateImage . $fileExtension5_2;
      $bedImage5_2 = "room5-b-" . $dateImage . $fileExtension5_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_2, $paths . '/' . $bedImage5_2);

    }else $bedImage5_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-v-iii']['name'])){

      $bedImage5_3 = $_FILES['bed-v-iii']['name'];
      $bedImage_tmp5_3 = $_FILES['bed-v-iii']['tmp_name'];
      $bedImageType5_3 = stristr($_FILES['bed-v-iii']['type'], "/");
      $fileExtension5_3 = str_replace( "/", ".", $bedImageType5_3);
      $bedImage5_3Url = "public/".$usuario."/room5-c-" . $dateImage . $fileExtension5_3;
      $bedImage5_3 = "room5-c-" . $dateImage . $fileExtension5_3;
      
      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp5_3, $paths . '/' . $bedImage5_3);

    }else $bedImage5_3Url = 'NULL';


  // * ROOM 6

    // * BED A

    if(!empty($_FILES['bed-vi']['name'])){

      $bedImage_tmp6 = $_FILES['bed-vi']['tmp_name'];
      $bedImageType6 = stristr($_FILES['bed-vi']['type'], "/");
      $fileExtension6 = str_replace( "/", ".", $bedImageType6);
      $bedImage6Url = "public/".$usuario."/room6-a-" . $dateImage . $fileExtension6;
      $bedImage6 = "room6-a-" . $dateImage . $fileExtension6;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6, $paths . '/' . $bedImage6);

    }else $bedImage6Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-vi-ii']['name'])){

      $bedImage6_2 = $_FILES['bed-vi-ii']['name'];
      $bedImage_tmp6_2 = $_FILES['bed-vi-ii']['tmp_name'];
      $bedImageType6_2 = stristr($_FILES['bed-vi-ii']['type'], "/");      
      $fileExtension6_2 = str_replace( "/", ".", $bedImageType6_2);
      $bedImage6_2Url = "public/".$usuario."/room6-b-" . $dateImage . $fileExtension6_2;
      $bedImage6_2 = "room6-b-" . $dateImage . $fileExtension6_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_2, $paths . '/' . $bedImage6_2);

    }else $bedImage6_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vi-iii']['name'])){

      $bedImage6_3 = $_FILES['bed-vi-iii']['name'];
      $bedImage_tmp6_3 = $_FILES['bed-vi-iii']['tmp_name'];
      $bedImageType6_3 = stristr($_FILES['bed-vi-iii']['type'], "/");
      $fileExtension6_3 = str_replace( "/", ".", $bedImageType6_3);
      $bedImage6_3Url = "public/".$usuario."/room6-c-" . $dateImage . $fileExtension6_3;
      $bedImage6_3 = "room6-c-" . $dateImage . $fileExtension6_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp6_3, $paths . '/' . $bedImage6_3);

    }else $bedImage6_3Url = 'NULL';


  // * ROOM 7

    // * BED A

    if(!empty($_FILES['bed-vii']['name'])){

      $bedImage_tmp7 = $_FILES['bed-vii']['tmp_name'];
      $bedImageType7 = stristr($_FILES['bed-vii']['type'], "/");
      $fileExtension7 = str_replace( "/", ".", $bedImageType7);
      $bedImage7Url = "public/".$usuario."/room7-a-" . $dateImage . $fileExtension7;
      $bedImage7 = "room7-a-" . $dateImage . $fileExtension7;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7, $paths . '/' . $bedImage7);

    }else $bedImage7Url = 'NULL';


    // * BED B

    if(!empty($_FILES['bed-vii-ii']['name'])){

      $bedImage7_2 = $_FILES['bed-vii-ii']['name'];
      $bedImage_tmp7_2 = $_FILES['bed-vii-ii']['tmp_name'];
      $bedImageType7_2 = stristr($_FILES['bed-vii-ii']['type'], "/");
      $fileExtension7_2 = str_replace( "/", ".", $bedImageType7_2);
      $bedImage7_2Url = "public/".$usuario."/room7-b-" . $dateImage . $fileExtension7_2;
      $bedImage7_2 = "room7-b-" . $dateImage . $fileExtension7_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_2, $paths . '/' . $bedImage7_2);

    }else $bedImage7_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-vii-iii']['name'])){

      $bedImage7_3 = $_FILES['bed-vii-iii']['name'];
      $bedImage_tmp7_3 = $_FILES['bed-vii-iii']['tmp_name'];
      $bedImageType7_3 = stristr($_FILES['bed-vii-iii']['type'], "/");
      $fileExtension7_3 = str_replace( "/", ".", $bedImageType7_3);
      $bedImage7_3Url = "public/".$usuario."/room7-c-" . $dateImage . $fileExtension7_3;
      $bedImage7_3 = "room7-c-" . $dateImage . $fileExtension7_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp7_3, $paths . '/' . $bedImage7_3);

    }else $bedImage7_3Url = 'NULL';


  // * ROOM 8

    // * BED A

    if(!empty($_FILES['bed-viii']['name'])){

      $bedImage_tmp8 = $_FILES['bed-viii']['tmp_name'];
      $bedImageType8 = stristr($_FILES['bed-viii']['type'], "/");
      $fileExtension8 = str_replace( "/", ".", $bedImageType8);
      $bedImage8Url = "public/".$usuario."/room8-a-" . $dateImage . $fileExtension8;
      $bedImage8 = "room8-a-" . $dateImage . $fileExtension8;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8, $paths . '/' . $bedImage8);

    }else $bedImage8Url = 'NULL';

    // * BED B

    if(!empty($_FILES['bed-viii-ii']['name'])){

      $bedImage8_2 = $_FILES['bed-viii-ii']['name'];
      $bedImage_tmp8_2 = $_FILES['bed-viii-ii']['tmp_name'];
      $bedImageType8_2 = stristr($_FILES['bed-viii-ii']['type'], "/");
      $fileExtension8_2 = str_replace( "/", ".", $bedImageType8_2);
      $bedImage8_2Url = "public/".$usuario."/room8-b-" . $dateImage . $fileExtension8_2;
      $bedImage8_2 = "room8-b-" . $dateImage . $fileExtension8_2;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8_2, $paths . '/' . $bedImage8_2);

    }else $bedImage8_2Url = 'NULL';

    // * BED C

    if(!empty($_FILES['bed-viii-iii']['name'])){

      $bedImage8_3 = $_FILES['bed-viii-iii']['name'];
      $bedImage_tmp8_3 = $_FILES['bed-viii-iii']['tmp_name'];
      $bedImageType8_3 = stristr($_FILES['bed-viii-iii']['type'], "/");
      $fileExtension8_3 = str_replace( "/", ".", $bedImageType8_3);
      $bedImage8_3Url = "public/".$usuario."/room8-c-" . $dateImage . $fileExtension8_3;
      $bedImage8_3 = "room8-c-" . $dateImage . $fileExtension8_3;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($bedImage_tmp8_3, $paths . '/' . $bedImage8_3);

    } else $bedImage8_3Url = 'NULL';
  
  }
  
  // * ROOM 1

  if($_POST['type1'] != 'NULL'){

    // * ROOM 1

    if (empty($_POST['type1'])) $type1 =  'NULL';
    else $type1 =  $_POST['type1'];

    if (empty($_POST['food1'])) $food1 =  'NULL';
    else $food1 =  $_POST['food1'];

    if (empty($_POST['bed1_1']) || $_POST['bed1_1'] == 'NULL') {
      $bed1 =  'NULL';
      $date1 = 'NULL';
    }
    else {
      $bed1 =  $_POST['bed1_1'];
      $date1 = 'Avalible';
    }
    
    if (empty($_POST['bed1_2']) || $_POST['bed1_2'] == 'NULL') {
      $bed1_2 =  'NULL';
      $date1_2 = 'NULL';
    }
    else {
      $bed1_2 =  $_POST['bed1_2'];
      $date1_2 = 'Avalible';
    }
    
    if (empty($_POST['bed1_3']) || $_POST['bed1_3'] == 'NULL') {
      $bed1_3 =  'NULL';
      $date1_3 = 'NULL';
    }
    else {
      $bed1_3 =  $_POST['bed1_3'];
      $date1_3 = 'Avalible';
    }

    if (empty($_POST['approx1'])) $approx1 =  '0';
    else $approx1 =  $_POST['approx1'];

    


    
    // * ROOM 2
    
    if (empty($_POST['type2'])) $type2 =  'NULL';
    else $type2 =  $_POST['type2'];

    if (empty($_POST['food2'])) $food2 =  'NULL';
    else $food2 =  $_POST['food2'];

    if (empty($_POST['bed2_1']) || $_POST['bed2_1'] == 'NULL') {
      $bed2 =  'NULL';
      $date2 = 'NULL';
    }
    else {
      $bed2 =  $_POST['bed2_1'];
      $date2 = 'Avalible';
    }
    
    if (empty($_POST['bed2_2']) || $_POST['bed2_2'] == 'NULL') {
      $bed2_2 =  'NULL';
      $date2_2 = 'NULL';
    }
    else {
      $bed2_2 =  $_POST['bed2_2'];
      $date2_2 = 'Avalible';
    }
    
    if (empty($_POST['bed2_3']) || $_POST['bed2_3'] == 'NULL') {
      $bed2_3 =  'NULL';
      $date2_3 = 'NULL';
    }
    else {
      $bed2_3 =  $_POST['bed2_3'];
      $date2_3 = 'Avalible';
    }

    if (empty($_POST['approx2'])) $approx2 =  '0';
    else $approx2 =  $_POST['approx2'];


    // * ROOM 3

    if (empty($_POST['type3'])) $type3 =  'NULL';
    else $type3 =  $_POST['type3'];

    if (empty($_POST['food3'])) $food3 =  'NULL';
    else $food3 =  $_POST['food3'];

    if (empty($_POST['bed3_1']) || $_POST['bed3_1'] == 'NULL') {
      $bed3 =  'NULL';
      $date3 = 'NULL';
    }
    else {
      $bed3 =  $_POST['bed3_1'];
      $date3 = 'Avalible';
    }
    
    if (empty($_POST['bed3_2']) || $_POST['bed3_2'] == 'NULL') {
      $bed3_2 =  'NULL';
      $date3_2 = 'NULL';
    }
    else {
      $bed3_2 =  $_POST['bed3_2'];
      $date3_2 = 'Avalible';
    }
    
    if (empty($_POST['bed3_3']) || $_POST['bed3_3'] == 'NULL') {
      $bed3_3 =  'NULL';
      $date3_3 = 'NULL';
    }
    else {
      $bed3_3 =  $_POST['bed3_3'];
      $date3_3 = 'Avalible';
    }

    if (empty($_POST['approx3'])) $approx3 =  '0';
    else $approx3 =  $_POST['approx3'];

    // * ROOM 4

    if (empty($_POST['type4'])) $type4 =  'NULL';
    else $type4 =  $_POST['type4'];

    if (empty($_POST['food4'])) $food4 =  'NULL';
    else $food4 =  $_POST['food4'];

    if (empty($_POST['bed4_1']) || $_POST['bed4_1'] == 'NULL') {
      $bed4 =  'NULL';
      $date4 = 'NULL';
    }
    else {
      $bed4 =  $_POST['bed4_1'];
      $date4 = 'Avalible';
    }
    
    if (empty($_POST['bed4_2']) || $_POST['bed4_2'] == 'NULL') {
      $bed4_2 =  'NULL';
      $date4_2 = 'NULL';
    }
    else {
      $bed4_2 =  $_POST['bed4_2'];
      $date4_2 = 'Avalible';
    }
    
    if (empty($_POST['bed4_3']) || $_POST['bed4_3'] == 'NULL') {
      $bed4_3 =  'NULL';
      $date4_3 = 'NULL';
    }
    else {
      $bed4_3 =  $_POST['bed4_3'];
      $date4_3 = 'Avalible';
    }

    if (empty($_POST['approx4'])) $approx4 =  '0';
    else $approx4 =  $_POST['approx4'];

    // * ROOM 5

    if (empty($_POST['type5'])) $type5 =  'NULL';
    else $type5 =  $_POST['type5'];

    if (empty($_POST['food5'])) $food5 =  'NULL';
    else $food5 =  $_POST['food5'];

    if (empty($_POST['bed5_1']) || $_POST['bed5_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed5_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed5_2']) || $_POST['bed5_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed5_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed5_3']) || $_POST['bed5_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed5_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx5'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx5'];

    // * ROOM 6

    if (empty($_POST['type6'])) $type6 =  'NULL';
    else $type6 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food6 =  'NULL';
    else $food6 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed6_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed6_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed6_3'];
      $date6_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx6'];

    // * ROOM 7

    if (empty($_POST['type7'])) $type7 =  'NULL';
    else $type7 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food7 =  'NULL';
    else $food7 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed7_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed7_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed7_3'];
      $date7_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx7'];

    // * ROOM 8

    if (empty($_POST['type8'])) $type8 =  'NULL';
    else $type8 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food8 =  'NULL';
    else $food8 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed8 =  'NULL';
      $date8 = 'NULL';
    }
    else {
      $bed8 =  $_POST['bed8_1'];
      $date8 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed8_2 =  'NULL';
      $date8_2 = 'NULL';
    }
    else {
      $bed8_2 =  $_POST['bed8_2'];
      $date8_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed8_3 =  'NULL';
      $date8_3 = 'NULL';
    }
    else {
      $bed8_3 =  $_POST['bed8_3'];
      $date8_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx8 =  '0';
    else $approx8 =  $_POST['approx8'];

  
  }else {

    if (empty($_POST['type2'])) $type1 = 'NULL';
    else $type1 = $_POST['type2'];

    if (empty($_POST['food2'])) $food1 =  'NULL';
    else $food1 = $_POST['food2'];

    if (empty($_POST['bed2_1']) || $_POST['bed2_1'] == 'NULL') {
      $bed1 =  'NULL';
      $date1 = 'NULL';
    }
    else {
      $bed1 =  $_POST['bed2_1'];
      $date1 = 'Avalible';
    }
    
    if (empty($_POST['bed2_2']) || $_POST['bed2_2'] == 'NULL') {
      $bed1_2 =  'NULL';
      $date1_2 = 'NULL';
    }
    else {
      $bed1_2 =  $_POST['bed2_2'];
      $date1_2 = 'Avalible';
    }
    
    if (empty($_POST['bed2_3']) || $_POST['bed2_3'] == 'NULL') {
      $bed1_3 =  'NULL';
      $date1_3 = 'NULL';
    }
    else {
      $bed1_3 =  $_POST['bed2_3'];
      $date1_3 = 'Avalible';
    }

    if (empty($_POST['approx2'])) $approx1 =  '0';
    else $approx1 =  $_POST['approx2'];

    // * ROOM 2

    if (empty($_POST['type3'])) $type2 =  'NULL';
    else $type2 =  $_POST['type3'];

    if (empty($_POST['food3'])) $food2 =  'NULL';
    else $food2 =  $_POST['food3'];

    if (empty($_POST['bed3_1']) || $_POST['bed3_1'] == 'NULL') {
      $bed2 =  'NULL';
      $date2 = 'NULL';
    }
    else {
      $bed2 =  $_POST['bed3_1'];
      $date2 = 'Avalible';
    }
    
    if (empty($_POST['bed3_2']) || $_POST['bed3_2'] == 'NULL') {
      $bed2_2 =  'NULL';
      $date2_2 = 'NULL';
    }
    else {
      $bed2_2 =  $_POST['bed3_2'];
      $date2_2 = 'Avalible';
    }
    
    if (empty($_POST['bed3_3']) || $_POST['bed3_3'] == 'NULL') {
      $bed2_3 =  'NULL';
      $date2_3 = 'NULL';
    }
    else {
      $bed2_3 =  $_POST['bed3_3'];
      $date2_3 = 'Avalible';
    }

    if (empty($_POST['approx3'])) $approx2 =  '0';
    else $approx2 =  $_POST['approx3'];

    // * ROOM 3

    if (empty($_POST['type4'])) $type3 =  'NULL';
    else $type3 =  $_POST['type4'];

    if (empty($_POST['food4'])) $food3 =  'NULL';
    else $food3 =  $_POST['food4'];

    if (empty($_POST['bed4_1']) || $_POST['bed4_1'] == 'NULL') {
      $bed3 =  'NULL';
      $date3 = 'NULL';
    }
    else {
      $bed3 =  $_POST['bed4_1'];
      $date3 = 'Avalible';
    }
    
    if (empty($_POST['bed4_2']) || $_POST['bed4_2'] == 'NULL') {
      $bed3_2 =  'NULL';
      $date3_2 = 'NULL';
    }
    else {
      $bed3_2 =  $_POST['bed4_2'];
      $date3_2 = 'Avalible';
    }
    
    if (empty($_POST['bed4_3']) || $_POST['bed4_3'] == 'NULL') {
      $bed3_3 =  'NULL';
      $date3_3 = 'NULL';
    }
    else {
      $bed3_3 =  $_POST['bed4_3'];
      $date3_3 = 'Avalible';
    }

    if (empty($_POST['approx4'])) $approx3 =  '0';
    else $approx3 =  $_POST['approx4'];

    // * ROOM 4

    if (empty($_POST['type5'])) $type4 =  'NULL';
    else $type4 =  $_POST['type5'];

    if (empty($_POST['food5'])) $food4 =  'NULL';
    else $food4 =  $_POST['food5'];

    if (empty($_POST['bed5_1']) || $_POST['bed5_1'] == 'NULL') {
      $bed4 =  'NULL';
      $date4 = 'NULL';
    }
    else {
      $bed4 =  $_POST['bed5_1'];
      $date4 = 'Avalible';
    }
    
    if (empty($_POST['bed5_2']) || $_POST['bed5_2'] == 'NULL') {
      $bed4_2 =  'NULL';
      $date4_2 = 'NULL';
    }
    else {
      $bed4_2 =  $_POST['bed5_2'];
      $date4_2 = 'Avalible';
    }
    
    if (empty($_POST['bed5_3']) || $_POST['bed5_3'] == 'NULL') {
      $bed4_3 =  'NULL';
      $date4_3 = 'NULL';
    }
    else {
      $bed4_3 =  $_POST['bed5_3'];
      $date4_3 = 'Avalible';
    }

    if (empty($_POST['approx5'])) $approx4 =  '0';
    else $approx4 =  $_POST['approx5'];

    // * ROOM 5

    if (empty($_POST['type6'])) $type5 =  'NULL';
    else $type5 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food5 =  'NULL';
    else $food5 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed6_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed6_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed6_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx6'];

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];

  }





  if($_POST['type2'] == 'NULL'){

    // * ROOM 2

    if (empty($_POST['type3'])) $type2 =  'NULL';
    else $type2 =  $_POST['type3'];

    if (empty($_POST['food3'])) $food2 =  'NULL';
    else $food2 =  $_POST['food3'];

    if (empty($_POST['bed3_1']) || $_POST['bed3_1'] == 'NULL') {
      $bed2 =  'NULL';
      $date2 = 'NULL';
    }
    else {
      $bed2 =  $_POST['bed3_1'];
      $date2 = 'Avalible';
    }
    
    if (empty($_POST['bed3_2']) || $_POST['bed3_2'] == 'NULL') {
      $bed2_2 =  'NULL';
      $date2_2 = 'NULL';
    }
    else {
      $bed2_2 =  $_POST['bed3_2'];
      $date2_2 = 'Avalible';
    }
    
    if (empty($_POST['bed3_3']) || $_POST['bed3_3'] == 'NULL') {
      $bed2_3 =  'NULL';
      $date2_3 = 'NULL';
    }
    else {
      $bed2_3 =  $_POST['bed3_3'];
      $date2_3 = 'Avalible';
    }

    if (empty($_POST['approx3'])) $approx2 =  '0';
    else $approx2 =  $_POST['approx3'];

    // * ROOM 3

    if (empty($_POST['type4'])) $type3 =  'NULL';
    else $type3 =  $_POST['type4'];

    if (empty($_POST['food4'])) $food3 =  'NULL';
    else $food3 =  $_POST['food4'];

    if (empty($_POST['bed4_1']) || $_POST['bed4_1'] == 'NULL') {
      $bed3 =  'NULL';
      $date3 = 'NULL';
    }
    else {
      $bed3 =  $_POST['bed4_1'];
      $date3 = 'Avalible';
    }
    
    if (empty($_POST['bed4_2']) || $_POST['bed4_2'] == 'NULL') {
      $bed3_2 =  'NULL';
      $date3_2 = 'NULL';
    }
    else {
      $bed3_2 =  $_POST['bed4_2'];
      $date3_2 = 'Avalible';
    }
    
    if (empty($_POST['bed4_3']) || $_POST['bed4_3'] == 'NULL') {
      $bed3_3 =  'NULL';
      $date3_3 = 'NULL';
    }
    else {
      $bed3_3 =  $_POST['bed4_3'];
      $date3_3 = 'Avalible';
    }

    if (empty($_POST['approx4'])) $approx3 =  '0';
    else $approx3 =  $_POST['approx4'];

    // * ROOM 4

    if (empty($_POST['type5'])) $type4 =  'NULL';
    else $type4 =  $_POST['type5'];

    if (empty($_POST['food5'])) $food4 =  'NULL';
    else $food4 =  $_POST['food5'];

    if (empty($_POST['bed5_1']) || $_POST['bed5_1'] == 'NULL') {
      $bed4 =  'NULL';
      $date4 = 'NULL';
    }
    else {
      $bed4 =  $_POST['bed5_1'];
      $date4 = 'Avalible';
    }
    
    if (empty($_POST['bed5_2']) || $_POST['bed5_2'] == 'NULL') {
      $bed4_2 =  'NULL';
      $date4_2 = 'NULL';
    }
    else {
      $bed4_2 =  $_POST['bed5_2'];
      $date4_2 = 'Avalible';
    }
    
    if (empty($_POST['bed5_3']) || $_POST['bed5_3'] == 'NULL') {
      $bed4_3 =  'NULL';
      $date4_3 = 'NULL';
    }
    else {
      $bed4_3 =  $_POST['bed5_3'];
      $date4_3 = 'Avalible';
    }

    if (empty($_POST['approx5'])) $approx4 =  '0';
    else $approx4 =  $_POST['approx5'];

    // * ROOM 5

    if (empty($_POST['type6'])) $type5 =  'NULL';
    else $type5 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food5 =  'NULL';
    else $food5 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed6_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed6_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed6_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx6'];

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];

  }






  if($_POST['type3'] == 'NULL'){

    // * ROOM 3

    if (empty($_POST['type4'])) $type3 =  'NULL';
    else $type3 =  $_POST['type4'];

    if (empty($_POST['food4'])) $food3 =  'NULL';
    else $food3 =  $_POST['food4'];

    if (empty($_POST['bed4_1']) || $_POST['bed4_1'] == 'NULL') {
      $bed3 =  'NULL';
      $date3 = 'NULL';
    }
    else {
      $bed3 =  $_POST['bed4_1'];
      $date3 = 'Avalible';
    }
    
    if (empty($_POST['bed4_2']) || $_POST['bed4_2'] == 'NULL') {
      $bed3_2 =  'NULL';
      $date3_2 = 'NULL';
    }
    else {
      $bed3_2 =  $_POST['bed4_2'];
      $date3_2 = 'Avalible';
    }
    
    if (empty($_POST['bed4_3']) || $_POST['bed4_3'] == 'NULL') {
      $bed3_3 =  'NULL';
      $date3_3 = 'NULL';
    }
    else {
      $bed3_3 =  $_POST['bed4_3'];
      $date3_3 = 'Avalible';
    }

    if (empty($_POST['approx4'])) $approx3 =  '0';
    else $approx3 =  $_POST['approx4'];

    // * ROOM 4

    if (empty($_POST['type5'])) $type4 =  'NULL';
    else $type4 =  $_POST['type5'];

    if (empty($_POST['food5'])) $food4 =  'NULL';
    else $food4 =  $_POST['food5'];

    if (empty($_POST['bed5_1']) || $_POST['bed5_1'] == 'NULL') {
      $bed4 =  'NULL';
      $date4 = 'NULL';
    }
    else {
      $bed4 =  $_POST['bed5_1'];
      $date4 = 'Avalible';
    }
    
    if (empty($_POST['bed5_2']) || $_POST['bed5_2'] == 'NULL') {
      $bed4_2 =  'NULL';
      $date4_2 = 'NULL';
    }
    else {
      $bed4_2 =  $_POST['bed5_2'];
      $date4_2 = 'Avalible';
    }
    
    if (empty($_POST['bed5_3']) || $_POST['bed5_3'] == 'NULL') {
      $bed4_3 =  'NULL';
      $date4_3 = 'NULL';
    }
    else {
      $bed4_3 =  $_POST['bed5_3'];
      $date4_3 = 'Avalible';
    }

    if (empty($_POST['approx5'])) $approx4 =  '0';
    else $approx4 =  $_POST['approx5'];

    // * ROOM 5

    if (empty($_POST['type6'])) $type5 =  'NULL';
    else $type5 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food5 =  'NULL';
    else $food5 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed6_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed6_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed6_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx6'];

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];

  }




  if($_POST['type4'] == 'NULL'){

    // * ROOM 4

    if (empty($_POST['type5'])) $type4 =  'NULL';
    else $type4 =  $_POST['type5'];

    if (empty($_POST['food5'])) $food4 =  'NULL';
    else $food4 =  $_POST['food5'];

    if (empty($_POST['bed5_1']) || $_POST['bed5_1'] == 'NULL') {
      $bed4 =  'NULL';
      $date4 = 'NULL';
    }
    else {
      $bed4 =  $_POST['bed5_1'];
      $date4 = 'Avalible';
    }
    
    if (empty($_POST['bed5_2']) || $_POST['bed5_2'] == 'NULL') {
      $bed4_2 =  'NULL';
      $date4_2 = 'NULL';
    }
    else {
      $bed4_2 =  $_POST['bed5_2'];
      $date4_2 = 'Avalible';
    }
    
    if (empty($_POST['bed5_3']) || $_POST['bed5_3'] == 'NULL') {
      $bed4_3 =  'NULL';
      $date4_3 = 'NULL';
    }
    else {
      $bed4_3 =  $_POST['bed5_3'];
      $date4_3 = 'Avalible';
    }

    if (empty($_POST['approx5'])) $approx4 =  '0';
    else $approx4 =  $_POST['approx5'];

    // * ROOM 5

    if (empty($_POST['type6'])) $type5 =  'NULL';
    else $type5 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food5 =  'NULL';
    else $food5 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed6_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed6_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed6_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx6'];

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8']; 
  }



  

  if($_POST['type5'] == 'NULL'){

    // * ROOM 5

    if (empty($_POST['type6'])) $type5 =  'NULL';
    else $type5 =  $_POST['type6'];

    if (empty($_POST['food6'])) $food5 =  'NULL';
    else $food5 =  $_POST['food6'];

    if (empty($_POST['bed6_1']) || $_POST['bed6_1'] == 'NULL') {
      $bed5 =  'NULL';
      $date5 = 'NULL';
    }
    else {
      $bed5 =  $_POST['bed6_1'];
      $date5 = 'Avalible';
    }
    
    if (empty($_POST['bed6_2']) || $_POST['bed6_2'] == 'NULL') {
      $bed5_2 =  'NULL';
      $date5_2 = 'NULL';
    }
    else {
      $bed5_2 =  $_POST['bed6_2'];
      $date5_2 = 'Avalible';
    }
    
    if (empty($_POST['bed6_3']) || $_POST['bed6_3'] == 'NULL') {
      $bed5_3 =  'NULL';
      $date5_3 = 'NULL';
    }
    else {
      $bed5_3 =  $_POST['bed6_3'];
      $date5_3 = 'Avalible';
    }

    if (empty($_POST['approx6'])) $approx5 =  '0';
    else $approx5 =  $_POST['approx6'];

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];

  }


  

  if($_POST['type6'] == 'NULL'){

    // * ROOM 6

    if (empty($_POST['type7'])) $type6 =  'NULL';
    else $type6 =  $_POST['type7'];

    if (empty($_POST['food7'])) $food6 =  'NULL';
    else $food6 =  $_POST['food7'];

    if (empty($_POST['bed7_1']) || $_POST['bed7_1'] == 'NULL') {
      $bed6 =  'NULL';
      $date6 = 'NULL';
    }
    else {
      $bed6 =  $_POST['bed7_1'];
      $date6 = 'Avalible';
    }
    
    if (empty($_POST['bed7_2']) || $_POST['bed7_2'] == 'NULL') {
      $bed6_2 =  'NULL';
      $date6_2 = 'NULL';
    }
    else {
      $bed6_2 =  $_POST['bed7_2'];
      $date6_2 = 'Avalible';
    }
    
    if (empty($_POST['bed7_3']) || $_POST['bed7_3'] == 'NULL') {
      $bed6_3 =  'NULL';
      $date6_3 = 'NULL';
    }
    else {
      $bed6_3 =  $_POST['bed7_3'];
      $date6_3 = 'Avalible';
    }
    
    if (empty($_POST['approx7'])) $approx6 =  '0';
    else $approx6 =  $_POST['approx7'];

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];

  }




  if($_POST['type7'] == 'NULL'){

    // * ROOM 7

    if (empty($_POST['type8'])) $type7 =  'NULL';
    else $type7 =  $_POST['type8'];
    
    if (empty($_POST['food8'])) $food7 =  'NULL';
    else $food7 =  $_POST['food8'];

    if (empty($_POST['bed8_1']) || $_POST['bed8_1'] == 'NULL') {
      $bed7 =  'NULL';
      $date7 = 'NULL';
    }
    else {
      $bed7 =  $_POST['bed8_1'];
      $date7 = 'Avalible';
    }
    
    if (empty($_POST['bed8_2']) || $_POST['bed8_2'] == 'NULL') {
      $bed7_2 =  'NULL';
      $date7_2 = 'NULL';
    }
    else {
      $bed7_2 =  $_POST['bed8_2'];
      $date7_2 = 'Avalible';
    }
    
    if (empty($_POST['bed8_3']) || $_POST['bed8_3'] == 'NULL') {
      $bed7_3 =  'NULL';
      $date7_3 = 'NULL';
    }
    else {
      $bed7_3 =  $_POST['bed8_3'];
      $date7_3 = 'Avalible';
    }

    if (empty($_POST['approx8'])) $approx7 =  '0';
    else $approx7 =  $_POST['approx8'];    

  
  }



  if(empty($type1)) $type1 = 'NULL';
  if(empty($food1)) $food1 = 'NULL';
  if(empty($bed1)) $bed1 = 'NULL';
  if(empty($date1)) $date1 = 'NULL';
  if(empty($bed1_2)) $bed1_2 = 'NULL';
  if(empty($bed1_3)) $bed1_3 = 'NULL';
  if(empty($date1_2)) $date1_2 = 'NULL';
  if(empty($date1_3)) $date1_3 = 'NULL';
  if(empty($approx1)) $approx1 = '0';
  
  if(empty($type2)) $type2 = 'NULL';
  if(empty($food2)) $food2 = 'NULL';
  if(empty($bed2)) $bed2 = 'NULL';
  if(empty($date2)) $date2 = 'NULL';
  if(empty($bed2_2)) $bed2_2 = 'NULL';
  if(empty($bed2_3)) $bed2_3 = 'NULL';
  if(empty($date2_2)) $date2_2 = 'NULL';
  if(empty($date2_3)) $date2_3 = 'NULL';
  if(empty($approx2)) $approx2 = '0';
  
  if(empty($type3)) $type3 = 'NULL';
  if(empty($food3)) $food3 = 'NULL';
  if(empty($bed3)) $bed3 = 'NULL';
  if(empty($date3)) $date3 = 'NULL';
  if(empty($bed3_2)) $bed3_2 = 'NULL';
  if(empty($bed3_3)) $bed3_3 = 'NULL';
  if(empty($date3_2)) $date3_2 = 'NULL';
  if(empty($date3_3)) $date3_3 = 'NULL';
  if(empty($approx3)) $approx3 = '0';

  if(empty($type4)) $type4 = 'NULL';
  if(empty($food4)) $food4 = 'NULL';
  if(empty($bed4)) $bed4 = 'NULL';
  if(empty($date4)) $date4 = 'NULL';
  if(empty($bed4_2)) $bed4_2 = 'NULL';
  if(empty($bed4_3)) $bed4_3 = 'NULL';
  if(empty($date4_2)) $date4_2 = 'NULL';
  if(empty($date4_3)) $date4_3 = 'NULL';
  if(empty($approx4)) $approx4 = '0';
  
  if(empty($type5)) $type5 = 'NULL';
  if(empty($food5)) $food5 = 'NULL';
  if(empty($bed5)) $bed5 = 'NULL';
  if(empty($date5)) $date5 = 'NULL';
  if(empty($bed5_2)) $bed5_2 = 'NULL';
  if(empty($bed5_3)) $bed5_3 = 'NULL';
  if(empty($date5_2)) $date5_2 = 'NULL';
  if(empty($date5_3)) $date5_3 = 'NULL';
  if(empty($approx5)) $approx5 = '0';

  if(empty($type6)) $type6 = 'NULL';
  if(empty($food6)) $food6 = 'NULL';
  if(empty($bed6)) $bed6 = 'NULL';
  if(empty($date6)) $date6 = 'NULL';
  if(empty($bed6_2)) $bed6_2 = 'NULL';
  if(empty($bed6_3)) $bed6_3 = 'NULL';
  if(empty($date6_2)) $date6_2 = 'NULL';
  if(empty($date6_3)) $date6_3 = 'NULL';
  if(empty($approx6)) $approx6 = '0';

  if(empty($type7)) $type7 = 'NULL';
  if(empty($food7)) $food7 = 'NULL';
  if(empty($bed7)) $bed7 = 'NULL';
  if(empty($date7)) $date7 = 'NULL';
  if(empty($bed7_2)) $bed7_2 = 'NULL';
  if(empty($bed7_3)) $bed7_3 = 'NULL';
  if(empty($date7_2)) $date7_2 = 'NULL';
  if(empty($date7_3)) $date7_3 = 'NULL';
  if(empty($approx7)) $approx7 = '0';

  if(empty($type8)) $type8 = 'NULL';
  if(empty($food8)) $food8 = 'NULL';
  if(empty($bed8)) $bed8 = 'NULL';
  if(empty($date8)) $date8 = 'NULL';
  if(empty($bed8_2)) $bed8_2 = 'NULL';
  if(empty($bed8_3)) $bed8_3 = 'NULL';
  if(empty($date8_2)) $date8_2 = 'NULL';
  if(empty($date8_3)) $date8_3 = 'NULL';
  if(empty($approx8)) $approx8 = '0';

  // TODO CAL NUM OF ROOMS
  
  if(!empty($type1) && $type1 != 'NULL') $num_type1 = 1;
  else $num_type1 = 0;

  if(!empty($type2) && $type2 != 'NULL') $num_type2 = 1;
  else $num_type2 = 0;

  if(!empty($type3) && $type3 != 'NULL') $num_type3 = 1;
  else $num_type3 = 0;

  if(!empty($type4) && $type4 != 'NULL') $num_type4 = 1;
  else $num_type4 = 0;

  if(!empty($type5) && $type5 != 'NULL') $num_type5 = 1;
  else $num_type5 = 0;

  if(!empty($type6) && $type6 != 'NULL') $num_type6 = 1;
  else $num_type6 = 0;
  
  if(!empty($type7) && $type7 != 'NULL') $num_type7 = 1;
  else $num_type7 = 0;

  if(!empty($type8) && $type8 != 'NULL') $num_type8 = 1;
  else $num_type8 = 0;

  $total_rooms = $num_type1 + $num_type2 + $num_type3 + $num_type4 + $num_type5 + $num_type6 + $num_type7 + $num_type8;
  




  $query = "UPDATE pe_home SET room='$total_rooms', dir = '$dir', city='$city', state = '$state', p_code = '$p_code', y_service = '$y_service', m_service='$m_service', num_mem='$num_mem', backl='$backl', vegetarians = '$vegetarians', halal = '$halal', kosher = '$kosher', lactose = '$lactose', gluten = '$gluten', pork = '$pork', none = '$none', h_type = '$h_type' WHERE mail_h='$usuario'";

  $resultado = $link->query($query);

  $query10 = "UPDATE propertie_control SET room='$total_rooms', dir='$dir', city='$city', status='$status', vegetarians='$vegetarians', halal='$halal', kosher='$kosher', lactose='$lactose', gluten='$gluten', pork='$pork', none='$none' WHERE id_home = '$rpe[id_home]'";
  $result10 = $link->query($query10);

  $query3 = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$usuario' SET type1='$type1', bed1='$bed1', bed1_2='$bed1_2', bed1_3 = '$bed1_3', food1='$food1', date1='$date1', date1_2 = '$date1_2', date1_3 = '$date1_3', aprox1='$approx1', type2='$type2', bed2='$bed2', bed2_2='$bed2_2', bed2_3='$bed2_3', food2='$food2', date2='$date2', date2_2='$date2_2', date2_3='$date2_3', aprox2='$approx2', type3='$type3', bed3='$bed3', bed3_2='$bed3_2', bed3_3='$bed3_3', food3='$food3', date3='$date3', date3_2='$date3_2', date3_3='$date3_3', aprox3='$approx3', type4='$type4', bed4='$bed4', bed4_2='$bed4_2', bed4_3='$bed4_3', food4='$food4', date4='$date4', date4_2='$date4_2', date4_3='$date4_3', aprox4='$approx4', type5='$type5', bed5='$bed5', bed5_2='$bed5_2', bed5_3='$bed5_3', food5='$food5', date5='$date5', date5_2='$date5_2', date5_3='$date5_3', aprox5='$approx5', type6='$type6', bed6='$bed6', bed6_2='$bed6_2', bed6_3='$bed6_3', food6='$food6', date6='$date6', date6_2='$date6_2', date6_3='$date6_3', aprox6='$approx6', type7='$type7', bed7='$bed7', bed7_2='$bed7_2', bed7_3='$bed7_3', food7='$food7', date7='$date7', date7_2='$date7_2', date7_3='$date7_3', aprox7='$approx7', type8='$type8', bed8='$bed8', bed8='$bed8', bed8_2='$bed8_2', bed8_3='$bed8_3', food8='$food8', date8='$date8', date8_2='$date8_2', date8_3='$date8_3', aprox8='$approx8'";

  $resultado3 = $link->query($query3);

  $query4 = "UPDATE photo_home INNER JOIN pe_home ON pe_home.id_home=photo_home.id_home AND pe_home.mail_h='$usuario' SET proom1='$bedImage1Url', proom1_2='$bedImage1_2Url', proom1_3='$bedImage1_3Url', proom2='$bedImage2Url', proom2_2='$bedImage2_2Url', proom2_3='$bedImage2_3Url', proom3='$bedImage3Url', proom3_2='$bedImage3_2Url', proom3_3='$bedImage3_3Url', proom4='$bedImage4Url', proom4_2='$bedImage4_2Url', proom4_3='$bedImage4_3Url', proom5='$bedImage5Url', proom5_2='$bedImage5_2Url', proom5_3='$bedImage5_3Url', proom6='$bedImage6Url', proom6_2='$bedImage6_2Url', proom6_3='$bedImage6_3Url', proom7='$bedImage7Url', proom7_2='$bedImage7_2Url', proom7_3='$bedImage7_3Url', proom8='$bedImage8Url', proom8_2='$bedImage8_2Url', proom8_3='$bedImage8_3Url'";
  $resultado4 = $link->query($query4);



  if ($result10 == 1) {

    $json[] = array(
      'register' => 'Registered'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  } else {

    $json[] = array(
      'register' => 'Error'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  }
}


/* -------------------------------------------------------------------------------------------------------- */





/* ------------------------------------------- STEP 3 ----------------------------------------------------- */ 
else if ($_POST['value_step'] == 'next_step-3') {

  $paths = '../public/' . $usuario;

  if (empty($_POST['des'])) $des =  'NULL';
  else $des =  $_POST['des'];

  // * FRONT IMAGE

  if(!empty($_FILES['main']['name'])){

    $frontImage = $_FILES['main']['name'];
    $frontImage_tmp = $_FILES['main']['tmp_name'];
    $frontImageType = stristr($_FILES['main']['type'], "/");
    $fileExtensionFront = str_replace( "/", ".", $frontImageType);
    $frontImageUrl = "public/".$usuario."/frontage-photo-" . $dateImage . $fileExtensionFront;
    $frontImage = "frontage-photo-" . $dateImage . $fileExtensionFront;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($frontImage_tmp, $paths . '/' . $frontImage);

  }else $frontImageUrl = 'NULL';

  // * LIVING ROOM

  if(!empty($_FILES['lroom']['name'])){

    $livingImage = $_FILES['lroom']['name'];
    $livingImage_tmp = $_FILES['lroom']['tmp_name'];
    $livingImageType = stristr($_FILES['lroom']['type'], "/");
    $fileExtensionLiving = str_replace( "/", ".", $livingImageType);
    $livingImageUrl = "public/".$usuario."/living-room-" . $dateImage . $fileExtensionLiving;
    $livingImage = "living-room-" . $dateImage . $fileExtensionLiving;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($livingImage_tmp, $paths . '/' . $livingImage);

  }else $livingImageUrl = 'NULL';

  // * KITCHEN

  if(!empty($_FILES['area-i']['name'])){

    $areaImage1 = $_FILES['area-i']['name'];
    $areaImage1_tmp = $_FILES['area-i']['tmp_name'];
    $areaImage1Type = stristr($_FILES['area-i']['type'], "/");
    $fileExtensionArea1 = str_replace( "/", ".", $areaImage1Type);
    $areaImage1Url = "public/".$usuario."/kitchen-" . $dateImage . $fileExtensionArea1;
    $areaImage1 = "kitchen-" . $dateImage . $fileExtensionArea1;

    if (!file_exists($paths)) mkdir($paths, 0777);;
    move_uploaded_file($areaImage1_tmp, $paths . '/' . $areaImage1);

  }else $areaImage1Url = 'NULL';

  //* DINING ROOM

  if(!empty($_FILES['area-ii']['name'])){

    $areaImage2 = $_FILES['area-ii']['name'];
    $areaImage2_tmp = $_FILES['area-ii']['tmp_name'];
    $areaImage2Type = stristr($_FILES['area-ii']['type'], "/");
    $fileExtensionArea2 = str_replace( "/", ".", $areaImage2Type);
    $areaImage2Url = "public/".$usuario."/dining-room-" . $dateImage . $fileExtensionArea2;
    $areaImage2 = "dining-room-" . $dateImage . $fileExtensionArea2;

    if (!file_exists($paths)) mkdir($paths, 0777); 
    move_uploaded_file($areaImage2_tmp, $paths . '/' . $areaImage2);
  
  }else $areaImage2Url = 'NULL';

  //* AREA 3

  if(!empty($_FILES['area-iii']['name'])){

    $areaImage3 = $_FILES['area-iii']['name'];
    $areaImage3_tmp = $_FILES['area-iii']['tmp_name'];
    $areaImage3Type = stristr($_FILES['area-iii']['type'], "/");
    $fileExtensionArea3 = str_replace( "/", ".", $areaImage3Type);
    $areaImage3Url = "public/".$usuario."/area-3-" . $dateImage . $fileExtensionArea3;
    $areaImage3 = "area-3-" . $dateImage . $fileExtensionArea3;

    if (!file_exists($paths)) mkdir($paths, 0777);;
    move_uploaded_file($areaImage3_tmp, $paths . '/' . $areaImage3);

  } else $areaImage3Url = 'NULL';

  //* AREA 4

  if(!empty($_FILES['area-iv']['name'])){

    $areaImage4 = $_FILES['area-iv']['name'];
    $areaImage4_tmp = $_FILES['area-iv']['tmp_name'];
    $areaImage4Type = stristr($_FILES['area-iv']['type'], "/");
    $fileExtensionArea4 = str_replace( "/", ".", $areaImage4Type);
    $areaImage4Url = "public/".$usuario."/area-4-" . $dateImage . $fileExtensionArea4;
    $areaImage4 = "area-4-" . $dateImage . $fileExtensionArea4;

    if (!file_exists($paths)) mkdir($paths, 0777);    
    move_uploaded_file($areaImage4_tmp, $paths . '/' . $areaImage4);

  } else $areaImage4Url = 'NULL';

  //* BATHROOM 1

  if(!empty($_FILES['bath-i']['name'])){

    $bathroomImage1 = $_FILES['bath-i']['name'];
    $bathroomImage1_tmp = $_FILES['bath-i']['tmp_name'];
    $bathroomImage1Type = stristr($_FILES['bath-i']['type'], "/");
    $fileExtensionBathroom1 = str_replace( "/", ".", $bathroomImage1Type);
    $bathroomImage1Url = "public/".$usuario."/bathroom-1-" . $dateImage . $fileExtensionBathroom1;
    $bathroomImage1 = "bathroom-1-" . $dateImage . $fileExtensionBathroom1;

    if (file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bathroomImage1_tmp, $paths . '/' . $bathroomImage1);

  } else $bathroomImage1Url = 'NULL';

  //* BATROOM 2

  if(!empty($_FILES['bath-ii']['name'])){

    $bathroomImage2 = $_FILES['bath-ii']['name'];
    $bathroomImage2_tmp = $_FILES['bath-ii']['tmp_name'];
    $bathroomImage2Type = stristr($_FILES['bath-ii']['type'], "/");
    $fileExtensionBathroom2 = str_replace( "/", ".", $bathroomImage2Type);
    $bathroomImage2Url = "public/".$usuario."/bathroom-2-" . $dateImage . $fileExtensionBathroom2;
    $bathroomImage2 = "bathroom-2-" . $dateImage . $fileExtensionBathroom2;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bathroomImage2_tmp, $paths . '/' . $bathroomImage2);

  }else $bathroomImage2Url = 'NULL';

  //* BATHROOM 3

  if(!empty($_FILES['bath-iii']['name'])){

    $bathroomImage3 = $_FILES['bath-iii']['name'];
    $bathroomImage3_tmp = $_FILES['bath-iii']['tmp_name'];
    $bathroomImage3Type = stristr($_FILES['bath-iii']['type'], "/");
    $fileExtensionBathroom3 = str_replace( "/", ".", $bathroomImage3Type);
    $bathroomImage3Url = "public/".$usuario."/bathroom-3-" . $dateImage . $fileExtensionBathroom3;
    $bathroomImage3 = "bathroom-3-" . $dateImage . $fileExtensionBathroom3;

    if (file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bathroomImage3_tmp, $paths . '/' . $bathroomImage3);

  }else $bathroomImage3Url = 'NULL';

  //* BATHROOM 4

  if(!empty($_FILES['bath-iv']['name'])){

    $bathroomImage4 = $_FILES['bath-iv']['name'];
    $bathroomImage4_tmp = $_FILES['bath-iv']['tmp_name'];
    $bathroomImage4Type = stristr($_FILES['bath-iv']['type'], "/");
    $fileExtensionBathroom4 = str_replace( "/", ".", $bathroomImage4Type);
    $bathroomImage4Url = "public/".$usuario."/bathroom-4-" . $dateImage . $fileExtensionBathroom4;
    $bathroomImage4 = "bathroom-4-" . $dateImage . $fileExtensionBathroom4;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bathroomImage4_tmp, $paths . '/' . $bathroomImage4);

  } else $bathroomImage4Url = 'NULL';

  // * FAMILY PICTURE

  if(!empty($_FILES['fp']['name'])){

    $familyImage = $_FILES['fp']['name'];
    $familyImage_tmp = $_FILES['fp']['tmp_name'];
    $familyImageType = stristr($_FILES['fp']['type'], "/");
    $fileExtensionFamily = str_replace( "/", ".", $familyImageType);
    $familyImageUrl = "public/".$usuario."/family-picture-" . $dateImage . $fileExtensionFamily;
    $familyImage = "family-picture-" . $dateImage . $fileExtensionFamily;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($familyImage_tmp, $paths . '/' . $familyImage);
  
  }else $familyImageUrl = 'NULL';



  $query = "UPDATE pe_home SET des = '$des', phome='$frontImageUrl' WHERE mail_h = '$usuario'";
  $resultado = $link->query($query);

  $query4 = "UPDATE photo_home INNER JOIN pe_home ON pe_home.id_home=photo_home.id_home AND pe_home.mail_h='$usuario' SET pliving='$livingImageUrl', parea1='$areaImage1Url', parea2='$areaImage2Url', parea3='$areaImage3Url', parea4='$areaImage4Url', pbath1='$bathroomImage1Url', pbath2='$bathroomImage2Url', pbath3='$bathroomImage3Url', pbath4='$bathroomImage4Url', fp='$familyImageUrl'";
  $resultado4 = $link->query($query4);

  $query10 = "UPDATE propertie_control SET photo='$frontImageUrl' WHERE id_home = '$rpe[id_home]'";
  $result10 = $link->query($query10);

  if ($result10 == 1) {

    $json[] = array(
      'register' => 'Registered'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  } else {

    $json[] = array(
      'register' => 'Error'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  }
}
/* ------------------------------------------- STEP 4 ----------------------------------------------------- */ 

else if ($_POST['value_step'] == 'next_step-4'){
  $paths = '../public/' . $usuario;

  if(!empty($_FILES['b_check_main']['name'])){
  
    $bCheckMain = $_FILES['b_check_main']['name'];
    $bCheckMain_tmp = $_FILES['b_check_main']['tmp_name'];
    $bCheckMainType = stristr($_FILES['b_check_main']['type'], "/");
    $fileExtensionBCheck = str_replace( "/", ".", $bCheckMainType);
    $bCheckMainUrl = "public/".$usuario."/background-check-main-" . $dateImage . $fileExtensionBCheck;
    $bCheckMain = "background-check-main-" . $dateImage . $fileExtensionBCheck;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMain_tmp, $paths . '/' . $bCheckMain);


  }else $bCheckMainUrl = 'NULL';
  
  if(!empty($_FILES['b_check_mem1']['name'])){
  
    $bCheckMem1 = $_FILES['b_check_mem1']['name'];
    $bCheckMem1_tmp = $_FILES['b_check_mem1']['tmp_name'];
    $bCheckMem1Type = stristr($_FILES['b_check_mem1']['type'], "/");
    $fileExtensionMem1 = str_replace( "/", ".", $bCheckMem1Type);
    $bCheckMem1Url = "public/".$usuario."/background-check-mem1-" . $dateImage . $fileExtensionMem1;
    $bCheckMem1 = "background-check-mem1-" . $dateImage . $fileExtensionMem1;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem1_tmp, $paths . '/' . $bCheckMem1);


  }else $bCheckMem1Url = 'NULL';

  if(!empty($_FILES['b_check_mem2']['name'])){
  
    $bCheckMem2 = $_FILES['b_check_mem2']['name'];
    $bCheckMem2_tmp = $_FILES['b_check_mem2']['tmp_name'];
    $bCheckMem2Type = stristr($_FILES['b_check_mem2']['type'], "/");
    $fileExtensionMem2 = str_replace( "/", ".", $bCheckMem2Type);
    $bCheckMem2Url = "public/".$usuario."/background-check-mem2-" . $dateImage . $fileExtensionMem2;
    $bCheckMem2 = "background-check-mem2-" . $dateImage . $fileExtensionMem2;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem2_tmp, $paths . '/' . $bCheckMem2);


  }else $bCheckMem2Url = 'NULL';

  if(!empty($_FILES['b_check_mem3']['name'])){
  
    $bCheckMem3 = $_FILES['b_check_mem3']['name'];
    $bCheckMem3_tmp = $_FILES['b_check_mem3']['tmp_name'];
    $bCheckMem3Type = stristr($_FILES['b_check_mem3']['type'], "/");
    $fileExtensionMem3 = str_replace( "/", ".", $bCheckMem3Type);
    $bCheckMem3Url = "public/".$usuario."/background-check-mem3-" . $dateImage . $fileExtensionMem3;
    $bCheckMem3 = "background-check-mem3-" . $dateImage . $fileExtensionMem3;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem3_tmp, $paths . '/' . $bCheckMem3);


  }else $bCheckMem3Url = 'NULL';

  if(!empty($_FILES['b_check_mem4']['name'])){
  
    $bCheckMem4 = $_FILES['b_check_mem4']['name'];
    $bCheckMem4_tmp = $_FILES['b_check_mem4']['tmp_name'];
    $bCheckMem4Type = stristr($_FILES['b_check_mem4']['type'], "/");
    $fileExtensionMem4 = str_replace( "/", ".", $bCheckMem4Type);
    $bCheckMem4Url = "public/".$usuario."/background-check-mem4-" . $dateImage . $fileExtensionMem4;
    $bCheckMem4 = "background-check-mem4-" . $dateImage . $fileExtensionMem4;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem4_tmp, $paths . '/' . $bCheckMem4);


  }else $bCheckMem4Url = 'NULL';

  if(!empty($_FILES['b_check_mem5']['name'])){
  
    $bCheckMem5 = $_FILES['b_check_mem5']['name'];
    $bCheckMem5_tmp = $_FILES['b_check_mem5']['tmp_name'];
    $bCheckMem5Type = stristr($_FILES['b_check_mem5']['type'], "/");
    $fileExtensionMem5 = str_replace( "/", ".", $bCheckMem5Type);
    $bCheckMem5Url = "public/".$usuario."/background-check-mem5-" . $dateImage . $fileExtensionMem5;
    $bCheckMem5 = "background-check-mem5-" . $dateImage . $fileExtensionMem5;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem5_tmp, $paths . '/' . $bCheckMem5);


  }else $bCheckMem5Url = 'NULL';

  if(!empty($_FILES['b_check_mem6']['name'])){
  
    $bCheckMem6 = $_FILES['b_check_mem6']['name'];
    $bCheckMem6_tmp = $_FILES['b_check_mem6']['tmp_name'];
    $bCheckMem6Type = stristr($_FILES['b_check_mem6']['type'], "/");
    $fileExtensionMem6 = str_replace( "/", ".", $bCheckMem6Type);
    $bCheckMem6Url = "public/".$usuario."/background-check-mem6-" . $dateImage . $fileExtensionMem6;
    $bCheckMem6 = "background-check-mem6-" . $dateImage . $fileExtensionMem6;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem6_tmp, $paths . '/' . $bCheckMem6);


  }else $bCheckMem6Url = 'NULL';

  if(!empty($_FILES['b_check_mem7']['name'])){
  
    $bCheckMem7 = $_FILES['b_check_mem7']['name'];
    $bCheckMem7_tmp = $_FILES['b_check_mem7']['tmp_name'];
    $bCheckMem7Type = stristr($_FILES['b_check_mem7']['type'], "/");
    $fileExtensionMem7 = str_replace( "/", ".", $bCheckMem7Type);
    $bCheckMem7Url = "public/".$usuario."/background-check-mem7-" . $dateImage . $fileExtensionMem7;
    $bCheckMem7 = "background-check-mem7-" . $dateImage . $fileExtensionMem7;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem7_tmp, $paths . '/' . $bCheckMem7);


  }else $bCheckMem7Url = 'NULL';

  if(!empty($_FILES['b_check_mem8']['name'])){
  
    $bCheckMem8 = $_FILES['b_check_mem8']['name'];
    $bCheckMem8_tmp = $_FILES['b_check_mem8']['tmp_name'];
    $bCheckMem8Type = stristr($_FILES['b_check_mem8']['type'], "/");
    $fileExtensionMem8 = str_replace( "/", ".", $bCheckMem8Type);
    $bCheckMem8Url = "public/".$usuario."/background-check-mem8-" . $dateImage . $fileExtensionMem8;
    $bCheckMem8 = "background-check-mem8-" . $dateImage . $fileExtensionMem8;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($bCheckMem8_tmp, $paths . '/' . $bCheckMem8);


  }else $bCheckMem8Url = 'NULL';

  /* ------------------------------------- MAIN CONTACT INFORMATION --------------------------------------- */

  if (empty($_POST['name_h'])) $name_h =  'NULL';
  else $name_h =  $_POST['name_h'];

  if (empty($_POST['l_name_h'])) $l_name_h =  'NULL';
  else $l_name_h =  $_POST['l_name_h'];

  if (empty($_POST['db'])) $db =  'NULL';
  else {
    $d_db = date_create($_POST['db']); //Se crea una fecha con el formato que recibes de la vista.
    $db = date_format($d_db, 'Y-m-d'); //Obtienes la fecha en el formato deseado.  
  }

  if (empty($_POST['gender'])) $gender =  'NULL';
  else $gender =  $_POST['gender'];

  if (empty($_POST['cell'])) $cell =  'NULL';
  else $cell =  $_POST['cell'];

  if (empty($_POST['occupation_m'])) $occupation_m =  'NULL';
  else $occupation_m =  $_POST['occupation_m'];

  if (empty($_POST['db_law'])) $db_law =  'NULL';
  else {
    $d_db_law = date_create($_POST['db_law']); //Se crea una fecha con el formato que recibes de la vista.
    $db_law = date_format($d_db_law, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  /* ------------------------------------- MEMBERS INFORMATION ------------------------------------- */

  /* ---------------------------------------- MEMBER 1 --------------------------------------------- */

  if (empty($_POST['f_name1'])) $f_name1 =  'NULL';
  else $f_name1 =  $_POST['f_name1'];

  if (empty($_POST['f_lname1'])) $f_lname1 =  'NULL';
  else $f_lname1 =  $_POST['f_lname1'];

  if (empty($_POST['db1'])) $db1 =  'NULL';
  else {
    $d_db1 = date_create($_POST['db1']); //Se crea una fecha con el formato que recibes de la vista.
    $db1 = date_format($d_db1, 'Y-m-d'); //Obtienes la fecha en el formato deseado.  
  }

  if (empty($_POST['gender1'])) $gender1 =  'NULL';
  else $gender1 =  $_POST['gender1'];

  if (empty($_POST['re1'])) $re1 =  'NULL';
  else $re1 =  $_POST['re1'];

  if (empty($_POST['occupation_f1'])) $occupation_f1 =  'NULL';
  else $occupation_f1 =  $_POST['occupation_f1'];

  if (empty($_POST['db_lawf1'])) $db_lawf1 =  'NULL';
  else {
    $d_db_lawf1 = date_create($_POST['db_lawf1']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf1 = date_format($d_db_lawf1, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }



  /* ---------------------------------------- MEMBER 2 --------------------------------------------- */

  if (empty($_POST['f_name2'])) $f_name2 =  'NULL';
  else $f_name2 =  $_POST['f_name2'];

  if (empty($_POST['f_lname2'])) $f_lname2 =  'NULL';
  else $f_lname2 =  $_POST['f_lname2'];


  if (empty($_POST['db2'])) $db2 =  'NULL';
  else {
    $d_db2 = date_create($_POST['db2']); //Se crea una fecha con el formato que recibes de la vista.
    $db2 = date_format($d_db2, 'Y-m-d'); //Obtienes la fecha en el formato deseado.     
  }

  if (empty($_POST['gender2'])) $gender2 =  'NULL';
  else $gender2 =  $_POST['gender2'];

  if (empty($_POST['re2'])) $re2 =  'NULL';
  else $re2 =  $_POST['re2'];

  if (empty($_POST['occupation_f2'])) $occupation_f2 =  'NULL';
  else $occupation_f2 =  $_POST['occupation_f2'];

  if (empty($_POST['db_lawf2'])) $db_lawf2 =  'NULL';
  else {
    $d_db_lawf2 = date_create($_POST['db_lawf2']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf2 = date_format($d_db_lawf2, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[2])) $lawf2 =  'NULL';
  else $lawf2 =  $path . "" . $image[2];

  /* ---------------------------------------- MEMBER 3 --------------------------------------------- */

  if (empty($_POST['f_name3'])) $f_name3 =  'NULL';
  else $f_name3 =  $_POST['f_name3'];

  if (empty($_POST['f_lname3'])) $f_lname3 =  'NULL';
  else $f_lname3 =  $_POST['f_lname3'];

  if (empty($_POST['db3'])) $db3 =  'NULL';
  else {
    $d_db3 = date_create($_POST['db3']); //Se crea una fecha con el formato que recibes de la vista.
    $db3 = date_format($d_db3, 'Y-m-d'); //Obtienes la fecha en el formato deseado.     
  }

  if (empty($_POST['gender3'])) $gender3 =  'NULL';
  else $gender3 =  $_POST['gender3'];

  if (empty($_POST['re3'])) $re3 =  'NULL';
  else $re3 =  $_POST['re3'];

  if (empty($_POST['occupation_f3'])) $occupation_f3 =  'NULL';
  else $occupation_f3 =  $_POST['occupation_f3'];

  if (empty($_POST['db_lawf3'])) $db_lawf3 =  'NULL';
  else {
    $d_db_lawf3 = date_create($_POST['db_lawf3']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf3 = date_format($d_db_lawf3, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[3])) $lawf3 =  'NULL';
  else $lawf3 =  $path . "" . $image[3];



  /* ---------------------------------------- MEMBER 4 --------------------------------------------- */

  if (empty($_POST['f_name4'])) $f_name4 =  'NULL';
  else $f_name4 =  $_POST['f_name4'];

  if (empty($_POST['f_lname4'])) $f_lname4 =  'NULL';
  else $f_lname4 =  $_POST['f_lname4'];

  if (empty($_POST['db4'])) $db4 =  'NULL';
  else {
    $d_db4 = date_create($_POST['db4']); //Se crea una fecha con el formato que recibes de la vista.
    $db4 = date_format($d_db4, 'Y-m-d'); //Obtienes la fecha en el formato deseado.      
  }

  if (empty($_POST['gender4'])) $gender4 =  'NULL';
  else $gender4 =  $_POST['gender4'];

  if (empty($_POST['re4'])) $re4 =  'NULL';
  else $re4 =  $_POST['re4'];

  if (empty($_POST['occupation_f4'])) $occupation_f4 =  'NULL';
  else $occupation_f4 =  $_POST['occupation_f4'];

  if (empty($_POST['db_lawf4'])) $db_lawf4 =  'NULL';
  else {
    $d_db_lawf4 = date_create($_POST['db_lawf4']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf4 = date_format($d_db_lawf4, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[4])) $lawf4 =  'NULL';
  else $lawf4 =  $path . "" . $image[4];

  /* ---------------------------------------- MEMBER 5 --------------------------------------------- */

  if (empty($_POST['f_name5'])) $f_name5 =  'NULL';
  else $f_name5 =  $_POST['f_name5'];

  if (empty($_POST['f_lname5'])) $f_lname5 =  'NULL';
  else $f_lname5 =  $_POST['f_lname5'];

  if (empty($_POST['db5'])) $db5 =  'NULL';
  else {
    $d_db5 = date_create($_POST['db5']); //Se crea una fecha con el formato que recibes de la vista.
    $db5 = date_format($d_db5, 'Y-m-d'); //Obtienes la fecha en el formato deseado.  
  }

  if (empty($_POST['gender5'])) $gender5 =  'NULL';
  else $gender5 =  $_POST['gender5'];

  if (empty($_POST['re5'])) $re5 =  'NULL';
  else $re5 =  $_POST['re5'];


  if (empty($_POST['occupation_f5'])) $occupation_f5 =  'NULL';
  else $occupation_f5 =  $_POST['occupation_f5'];

  if (empty($_POST['db_lawf5'])) $db_lawf5 =  'NULL';
  else {
    $d_db_lawf5 = date_create($_POST['db_lawf5']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf5 = date_format($d_db_lawf5, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }


  if (empty($image[5])) $lawf5 =  'NULL';
  else $lawf5 =  $path . "" . $image[5];



  /* ---------------------------------------- MEMBER 6 --------------------------------------------- */

  if (empty($_POST['f_name6'])) $f_name6 =  'NULL';
  else $f_name6 =  $_POST['f_name6'];

  if (empty($_POST['f_lname6'])) $f_lname6 =  'NULL';
  else $f_lname6 =  $_POST['f_lname6'];

  if (empty($_POST['db6'])) $db6 =  'NULL';
  else {
    $d_db6 = date_create($_POST['db6']); //Se crea una fecha con el formato que recibes de la vista.
    $db6 = date_format($d_db6, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($_POST['gender6'])) $gender6 =  'NULL';
  else $gender6 =  $_POST['gender6'];

  if (empty($_POST['re6'])) $re6 =  'NULL';
  else $re6 =  $_POST['re6'];

  if (empty($_POST['occupation_f6'])) $occupation_f6 =  'NULL';
  else $occupation_f6 =  $_POST['occupation_f6'];

  if (empty($_POST['db_lawf6'])) $db_lawf6 =  'NULL';
  else {
    $d_db_lawf6 = date_create($_POST['db_lawf6']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf6 = date_format($d_db_lawf6, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[6])) $lawf6 =  'NULL';
  else $lawf6 =  $path . "" . $image[6];




  /* ---------------------------------------- MEMBER 7 --------------------------------------------- */

  if (empty($_POST['f_name7'])) $f_name7 =  'NULL';
  else $f_name7 =  $_POST['f_name7'];

  if (empty($_POST['f_lname7'])) $f_lname7 =  'NULL';
  else $f_lname7 =  $_POST['f_lname7'];

  if (empty($_POST['db7'])) $db7 =  'NULL';
  else {
    $d_db7 = date_create($_POST['db7']); //Se crea una fecha con el formato que recibes de la vista.
    $db7 = date_format($d_db7, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($_POST['gender7'])) $gender7 =  'NULL';
  else $gender7 =  $_POST['gender7'];

  if (empty($_POST['re7'])) $re7 =  'NULL';
  else $re7 =  $_POST['re7'];

  if (empty($_POST['occupation_f7'])) $occupation_f7 =  'NULL';
  else $occupation_f7 =  $_POST['occupation_f7'];

  if (empty($_POST['db_lawf7'])) $db_lawf7 =  'NULL';
  else {
    $d_db_lawf7 = date_create($_POST['db_lawf7']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf7 = date_format($d_db_lawf7, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[7])) $lawf7 =  'NULL';
  else $lawf7 =  $path . "" . $image[7];



  /* ---------------------------------------- MEMBER 8 --------------------------------------------- */

  if (empty($_POST['f_name8'])) $f_name8 =  'NULL';
  else $f_name8 =  $_POST['f_name8'];

  if (empty($_POST['f_lname8'])) $f_lname8 =  'NULL';
  else $f_lname8 =  $_POST['f_lname8'];

  if (empty($_POST['db8'])) $db8 =  'NULL';
  else {
    $d_db8 = date_create($_POST['db8']); //Se crea una fecha con el formato que recibes de la vista.
    $db8 = date_format($d_db8, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($_POST['gender8'])) $gender8 =  'NULL';
  else $gender8 =  $_POST['gender8'];

  if (empty($_POST['re8'])) $re8 =  'NULL';
  else $re8 =  $_POST['re8'];

  if (empty($_POST['occupation_f8'])) $occupation_f8 =  'NULL';
  else $occupation_f8 =  $_POST['occupation_f8'];

  if (empty($_POST['db_lawf8'])) $db_lawf8 =  'NULL';
  else {
    $d_db_lawf8 = date_create($_POST['db_lawf8']); //Se crea una fecha con el formato que recibes de la vista.
    $db_lawf8 = date_format($d_db_lawf8, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
  }

  if (empty($image[8])) $lawf8 =  'NULL';
  else $lawf8 =  $path . "" . $image[8];

  $query = "UPDATE pe_home SET name_h='$name_h', l_name_h='$l_name_h', db='$db', gender='$gender', cell='$cell', occupation_m='$occupation_m', db_law= '$db_law', law='$bCheckMainUrl' WHERE mail_h='$usuario'";

  $resultado = $link->query($query);

  $query2 = "UPDATE mem_f INNER JOIN pe_home ON pe_home.id_home=mem_f.id_home AND pe_home.mail_h='$usuario' SET f_name1='$f_name1', f_lname1='$f_lname1', db1='$db1', gender1='$gender1', occupation_f1='$occupation_f1', db_lawf1='$db_lawf1', lawf1='$bCheckMem1Url', re1='$re1', f_name2='$f_name2', f_lname2='$f_lname2', db2='$db2', gender2='$gender2', occupation_f2='$occupation_f2', db_lawf2='$db_lawf2', lawf2='$bCheckMem2Url', re2='$re2', f_name3='$f_name3', f_lname3='$f_lname3', db3='$db3', gender3='$gender3', occupation_f3='$occupation_f3', db_lawf3='$db_lawf3', lawf3='$bCheckMem3Url', re3='$re3', f_name4='$f_name4', f_lname4='$f_lname4', db4='$db4', gender4='$gender4', occupation_f4='$occupation_f4', db_lawf4='$db_lawf4', lawf4='$bCheckMem4Url', re4='$re4', f_name5='$f_name5', f_lname5='$f_lname5', db5='$db5', gender5='$gender5', occupation_f5='$occupation_f5', db_lawf5='$db_lawf5', lawf5='$bCheckMem5Url', re5='$re5', f_name6='$f_name6', f_lname6='$f_lname6', db6='$db6', gender6='$gender6',occupation_f6='$occupation_f6', db_lawf6='$db_lawf6', lawf6='$bCheckMem6Url', re6='$re6', f_name7='$f_name7', f_lname7='$f_lname7', db7='$db7', gender7='$gender7', occupation_f7='$occupation_f7', db_lawf7='$db_lawf7', lawf7='$bCheckMem7Url', re7='$re7', f_name8='$f_name8', f_lname8='$f_lname8', db8='$db8', gender8='$gender8', occupation_f8='$occupation_f8', db_lawf8='$db_lawf8', lawf8='$bCheckMem8Url', re8='$re8'";

  $resultado2 = $link->query($query2);

  if ($resultado2 == 1) {

    $json[] = array(
      'register' => 'Registered'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  } else {

    $json[] = array(
      'register' => 'Error'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  }


}






/* ------------------------------------------- STEP 5 ----------------------------------------------------- */ 
else if ($_POST['value_step'] == 'next_step-5') {

  /* -------------------------------------- ADDITIONAL INFORMATION ---------------------------------------- */

  if (empty($_POST['a_pre'])) $a_pre =  'NULL';
  else $a_pre =  $_POST['a_pre'];
  
  if (empty($_POST['backg'])) $backg =  'NULL';
  else $backg =  $_POST['backg'];
  
  if (empty($_POST['religion'])) $religion =  'No';
  else $religion =  $_POST['religion'];
  
  if (empty($_POST['misdemeanor'])) $misdemeanor =  'No';
  else $misdemeanor =  $_POST['misdemeanor'];
  
  if (empty($_POST['c_background'])) $c_background =  'No';
  else $c_background =  $_POST['c_background'];

  if (empty($_POST['smoke'])) $smoke =  'NULL';
  else $smoke =  $_POST['smoke'];

  if (empty($_POST['allergies'])) $allergies =  'NULL';
  else $allergies =  $_POST['allergies'];

  if (empty($_POST['medic_f'])) $medic_f =  'NULL';
  else $medic_f =  $_POST['medic_f'];
  
  if (empty($_POST['condition_m'])) $condition_m =  'No';
  else $condition_m =  $_POST['condition_m'];
  
  if (empty($_POST['health_f'])) $health_f =  'NULL';
  else $health_f =  $_POST['health_f'];
  


  $query = "UPDATE pe_home SET a_pre = '$a_pre', backg='$backg', religion='$religion', misdemeanor='$misdemeanor', c_background='$c_background', smoke = '$smoke', allergies='$allergies', medic_f='$medic_f', condition_m='$condition_m', health_f='$health_f' WHERE mail_h='$usuario'";

  $resultado = $link->query($query);


  

  if ($resultado == 1) {

    $json[] = array(
      'register' => 'End Register'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  } else {

    $json[] = array(
      'register' => 'Error'
    );
    $jsonstring = json_encode($json);

    echo $jsonstring;
  }
}


if ($food1 == 'Yes') {
  $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
  $result3 = $link->query($query9);
} elseif ($food1 == 'No' || $food1 == 'NULL') {

  if ($food2 == 'Yes') {
    $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
    $result3 = $link->query($query9);
  } elseif ($food2 == 'No' || $food2 == 'NULL') {

    if ($food3 == 'Yes') {
      $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
      $result3 = $link->query($query9);
    } elseif ($food3 == 'No' || $food3 == 'NULL') {

      if ($food4 == 'Yes') {
        $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
        $result3 = $link->query($query9);
      } elseif ($food4 == 'No' || $food4 == 'NULL') {

        if ($food5 == 'Yes') {
          $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
          $result3 = $link->query($query9);
        } elseif ($food5 == 'No' || $food5 == 'NULL') {

          if ($food6 == 'Yes') {
            $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
            $result3 = $link->query($query9);
          } elseif ($food6 == 'No' || $food6 == 'NULL') {

            if ($food7 == 'Yes') {
              $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
              $result3 = $link->query($query9);
            } elseif ($food7 == 'No' || $food7 == 'NULL') {

              if ($food8 == 'Yes') {
                $query9 = "UPDATE pe_home SET food_s = 'Yes' WHERE mail_h = '$usuario'";
                $result3 = $link->query($query9);
              } elseif ($food8 == 'No' || $food8 == 'NULL') {
                $query9 = "UPDATE pe_home SET food_s = 'No' WHERE mail_h = '$usuario'";
                $result3 = $link->query($query9);
              }
            }
          }
        }
      }
    }
  }
}