<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

//Solicitud para mostrar solo la información y configuración del usuario en sesion
 
if ($usuario == 'sebi@homebor.com') {
    $query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario'");
    $row7=$query7->fetch_assoc();
    $row7['id_ag'] = '0'; 
} else {
    $query7 = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
    $row7=$query7->fetch_assoc();
}

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
         header("location: ../logout.php");
         }   
    }

    if ($row5['usert'] == 'Admin') {
        $way = 'index.php';
    } elseif ($row5['usert'] == 'Cordinator') {
       $way = 'cordinator.php';
       echo '<style type="text/css">a#admin{display:none;}</style>';
    }

$query6 = $link->query("SELECT * FROM academy");
$row6=$query6->fetch_assoc();

$sql = "SELECT * FROM `pe_student` WHERE id_m = '$row7[id_m]'";  
$connStatus = $link->query($sql);  
$numberOfRows = mysqli_num_rows($connStatus);

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_register.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">



    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    
    <title>Homebor - Students Profile</title>

</head>
<body>


<header id="ts-header" class="fixed-top" style="background: white;">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
       <?php include 'header.php' ?>

    </header>
    <!--end Header-->


<div class="container_register">

    <main class="card__register">
        <form id="form_master" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">

            <div class="row m-0 p-0">

                <div class="col-md-4 column-1">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Basic Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">
                                <img class="form__group__photo d-none" id="preview" src="" alt="">

                                <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">


                                <label for="file" class="photo-add" id="label-i"> <p class="form__group-l_title"> Add Profile Photo </p> </label>

                                <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;" title="Change Frontage Photo"></label>
                            </div>

                            <div class="info"></div>

                            

                                <!-- Group Name -->

                                <div class="form__group form__group__name" id="group__name">
                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">First Name</label>

                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                    
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__l_name" id="group__l_name">
                                    <div class="form__group__icon-input">

                                        <label for="l_name" class="form__label">Last Name</label>

                                        <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="e.g. Smith">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                                
                            <!-- Group Mail -->

                            <div class="form__group form__group__mail_s" id="group__mail_s">
                                <div class="form__group__icon-input">

                                    <label for="mail_s" class="form__label">Student Mail</label>

                                    <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>
                                    <input type="text" id="mail_s" name="mail_s" class="form__group-input mail" placeholder="e.g. jsmith@gmail.com">

                                </div>
                                <p class="form__group__input-error">It is not a valid Email</p>
                            </div>
                            
                            <!-- Group Password -->

                            <div class="form__group form__group__password" id="group__password">
                                <div class="form__group__icon-input">

                                    <label for="password" class="form__label">Password</label>

                                    <label for="password" class="icon"><i class="icon icon__password"></i></label>
                                    <input type="password" id="password" name="password" class="form__group-input password" placeholder="min 4 Characters">

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>
                            
                            <!-- Group Number -->

                            <div class="form__group form__group__num_s" id="group__num_s">

                                <div class="form__group__icon-input">

                                    <label for="num_s" class="form__label">Phone Number</label>

                                    <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>
                                    <input type="num" id="num_s" name="num_s" class="form__group-input num_s" placeholder="Personal Number">

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>
                        
                        </div>

                    </div>


                    <div class="card">


                        <h3 class="form__group-title__col-4">Flight Information</h3>

                        <div class="information__content">
                        
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_airline" id="group__n_airline">
                                <div class="form__group__icon-input">

                                    <label for="n_airline" class="form__label">Booking Confirmation</label>

                                    <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>
                                    <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline" placeholder="e.g. Air Canada (Canadá) AC">
                                    <p class="form__group__input-error">Minimum 4 Characters</p>

                                </div>
                            </div>
                            
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_flight" id="group__n_flight">
                                <div class="form__group__icon-input">

                                    <label for="n_flight" class="form__label">Landing Flight Number</label>

                                    <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>
                                    <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight" placeholder="e.g. B-9820">

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>


                                <div class="form__group form__group-f_date ">
                                    <div class="form__group__icon-input">

                                        <label for="f_date" class="form__label">Flight Date</label>
                                        <label for="f_date" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="f_date" name="f_date" class="form__group-input f_date" placeholder="Flight Date">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <div class="form__group form__group-h_date">

                                    <div class="form__group__icon-input">

                                        <label for="h_date" class="form__label">Arrival at the Homestay</label>
                                        <label for="h_date" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="h_date" name="h_date" class="form__group-input h_date" placeholder="Arrival Date at the Homestay">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>

                        </div>

                    </div>



                    <div class="card">

                        <h3 class="form__group-title__col-4">Emergency Contact</h3>

                        <div class="information__content">


                                <!-- Group Emergency Name -->
                                <div class="form__group form__group__cont_name" id="group__cont_name">
                                    <div class="form__group__icon-input">

                                        <label for="cont_name" class="form__label">Contact Name</label>

                                        <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name" placeholder="e.g. Joy">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Emergency Last Name -->
                                <div class="form__group form__group__cont_lname" id="group__cont_lname">

                                    <div class="form__group__icon-input">

                                        <label for="cont_lname" class="form__label">Contact Last Name</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname" placeholder="e.g. Smith">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                                <!-- Group Emergency phone -->
                                <div class="form__group form__group__num_conts" id="group__num_conts">

                                    <div class="form__group__icon-input">

                                        <label for="num_conts" class="form__label">Emergency Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>
                                        <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts" placeholder="Emergency Number">
                                    </div>
                                        <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Alternative Phone -->
                                <div class="form__group form__group__cell_s" id="group__cell_s">
                                    <div class="form__group__icon-input">

                                        <label for="cell_s" class="form__label">Alternative Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>
                                        <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s" placeholder="Alternative Number">
                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                        </div>

                    </div>

                </div>


                <input type="text" class="form-control" id="id_m" name="id_m" value="<?php echo $row7['id_m'];?>" hidden>

                <input type="text" class="form-control" id="agency" name="agency" value="<?php echo $row7['a_name'];?>" hidden>
                                            
                <input type="text" class="form-control" id="id_ag" name="id_ag" value="<?php echo $row7['id_ag'];?>" hidden>

                <input type="text" class="form-control" id="plan" name="plan" value="<?php echo $row7['plan'];?>" hidden>

                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo "$numberOfRows";?>" hidden>







                <div class="col-md-8 column-2">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Personal Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Date of Birth -->
                                <div class="form__group col-sm-4 form__group__db" id="group__db">
                                    <div class="form__group__icon-input">

                                        <label for="db" class="form__label">Date of Birth</label>
                                        <label for="db" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="db" name="db" class="form__group-input db" placeholder="Date of Birth">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Gender -->
                                <div class="form__group col-sm-4 form__group__basic form__group__gender" id="group__gender">

                                    <div class="form__group__icon-input">

                                        <label for="gender" class="form__label">Gender</label>

                                        <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                                        <select class="custom-select form__group-input gender" id="gender" name="gender">
                                            <option hidden="option" selected disabled> Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Any">Any</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a Gender</p>
                                </div>
                                
                                <!-- Group Background -->
                                <div class="form__group col-sm-4 form__group__basic form__group__nacionality" id="group__nacionality">
                                    <div class="form__group__icon-input">

                                        <label for="nacionality" class="form__label">Background</label>

                                        <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <input type="text" id="nacionality" name="nacionality" class="form__group-input nacionality" placeholder="e.g. Canadian">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Origin City -->
                                <div class="form__group col-sm-4 form__group__basic form__group__city" id="group__city">
                                    <div class="form__group__icon-input">

                                        <label for="city" class="form__label visa">Origin City</label>

                                        <label for="city" class="icon"><i class="icon icon__city"></i></label>
                                        <input type="text" id="city" name="city" class="form__group-input city" placeholder="e.g. Toronto">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Passport -->
                                <div class="form__group col-sm-4 form__group__basic form__group__pass" id="group__pass">
                                    <div class="form__group__icon-input">

                                        <label for="pass" class="form__label visa">Passport</label>

                                        <label for="pass" class="icon"><i class="icon icon__pass"></i></label>
                                        <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Visa Expiration Date -->
                                <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                                    <div class="form__group__icon-input">

                                        <label for="bl" class="form__label visa">Visa Expiration Date</label>
                                        <label for="bl" class="icon"><i class="icon icon__bl"></i></label>
                                        <input type="date" id="bl" name="bl" class="form__group-input bl" placeholder="Visa Expiration Date">
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <div class="form__group col-sm-6 form__group__basic form__group__visa" id="group__visa">
                                    <div class="form__group__icon-input">

                                        <label for="visa" class="form__label visa">Visa</label>
                                        <label for="visa" class="icon"><i class="icon icon__city"></i></label>
                                        <input type="file" id="visa" name="visa" class="form__group-input visa" placeholder="Visa" maxlength="1" accept="pdf">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>


                    <div class="card">

                        <h3 class="form__group-title__col-4">Academy Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">


                                <!-- Group Academy Preference -->
                                <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                                    <div class="form__group__icon-input">

                                        <label for="n_a" class="form__label">Academy Preference</label>

                                        <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                                        <select class="custom-select form__group-input n_a" id="n_a" name="n_a">   
                                            <?php    
                                                while ( $row6 = $query6->fetch_array() )    
                                                    {
                                            ?>
                                                    <option hidden="option" selected disabled>Select Academy Preference</option>
                                                    <option value=" <?php echo $row6['id_ac'] ?> " >
                                                        <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                                                    </option>
            
                                            <?php
                                                    }    
                                            ?>       
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <!-- Group Origin Language -->
                                <div class="form__group col-sm-4 form__group__basic form__group__lang_s" id="group__lang_s">
                                    <div class="form__group__icon-input">

                                        <label for="lang_s" class="form__label">Origin Language</label>

                                        <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s" placeholder="e.g. English">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Type Student -->
                                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                                    <div class="form__group__icon-input">

                                        <label for="type_s" class="form__label">Type Student</label>

                                        <label for="type_s" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="type_s" name="type_s" class="form__group-input type_s" placeholder="e.g. College">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Start Date of Stay -->
                                <div class="form__group col-sm-4 form__group__basic form__group__firstd" id="group__firstd">
                                    <div class="form__group__icon-input">
                                        
                                        <label for="firstd" class="form__label">Start Date of Stay</label>
                                        <label for="firstd" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="firstd" name="firstd" class="form__group-input firstd" placeholder="Start Date of Stay">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group End Date of stay -->
                                <div class="form__group col-sm-4 form__group__basic form__group__lastd" id="group__lastd">
                                    <div class="form__group__icon-input">
                                        
                                        <label for="lastd" class="form__label">End Date of Stay</label>
                                        <label for="lastd" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="lastd" name="lastd" class="form__group-input lastd" placeholder="End Date of stay">
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                            
                            </div>

                        </div>

                    </div>


                    <div class="card">

                        <h3 class="form__group-title__col-4">Preferences Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Smoker -->
                                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                                    <div class="form__group__icon-input">

                                        <label for="smoke_s" class="form__label smoker">Smoker</label>

                                        <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="smoke_s" name="smoke_s">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                                <!-- Group Pets -->
                                <div class="form__group col-sm-4 form__group__basic form__group__pets" id="group__pets">

                                    <div class="form__group__icon-input">

                                        <label for="pets" class="form__label pets_l">Pets</label>

                                        <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                                        <select class="custom-select form__group-input pets" id="pets" name="pets">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Food -->
                                <div class="form__group col-sm-4 form__group__basic form__group__food" id="group__food">

                                    <div class="form__group__icon-input">

                                        <label for="food" class="form__label pets_l">Food</label>

                                        <label for="food" class="icon"><i class="icon icon__food"></i></label>

                                        <select class="custom-select form__group-input food" id="food" name="food">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                
                                <!-- Group Allergies -->
                                <div class="form__group col-sm-12 form__group__basic form__group__allergies" id="group__allergies">

                                    <div class="form__group__icon-input">

                                        <label for="allergies" class="form__label allergies_l">Allergies</label>

                                        <label for="allergies" class="icon"><i class="icon icon__allergies"></i></label>

                                        <textarea class="form__group-input allergies" id="allergies" rows="3" name="allergies" placeholder="Describe in a few words if you have any allergies (Peanuts, Fish, Seafood, etc.)."></textarea>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <!-- Group Special -->
                                <div class="form__group form__group__basic form__group__special-diet" id="group__special-diet">
                                
                                    <div class="form__group__div-special__diet">
                                        <div class="form__group__icon-input">

                                            <label for="" class="icon"><i class="icon icon__diet"></i></label>
                                            <h4 class="title__special-diet">Special Diet</h4>

                                        </div>
                                    

                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes">
                                                <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">
                                                <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                                            </div>

                                        </div>

                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">
                                                <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes">
                                                <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                                            </div>

                                        </div>
                                        
                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">
                                                <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes">
                                                <label class="custom-control-label" for="pork">No Pork</label>
                                            </div>

                                        </div>
                                        
                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox-none custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">
                                                <label class="custom-control-label" for="none">None</label>
                                            </div>

                                        </div>

                                    </div>

                                </div>


                            </div>

                        </div>

                    </div>

                </div>
            </div><br>

            <div class="form__message" id="form__message">
                <p>llene los campos correctamente</p>
            </div>

            <div class="form__group__btn-send" align="right">
                <button type="submit" class="form__btn">Save Student</button>
                <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>
            </div>

            

            
        </form>
    </main>

</div>


    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


<?php include 'footer.php' ?>

<script>
                                function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").setAttribute("src", event.target.result);
                document.getElementById("preview").classList.remove("d-none");
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }else{
            document.getElementById("preview").classList.add("d-none");
        }

    }
                            </script>




    

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/validate_form.js"></script>

<!--Date Input Safari-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script src="assets/js/date-safari.js"></script>


</body>
</html>