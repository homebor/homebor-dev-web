<?php

$link = new PDO("mysql:host=localhost; dbname=u702954182_xeondb", "u702954182_xeon", "Xeon.2801");
error_reporting(0);

/*function get_total_row($link)
{
  $query = "
  SELECT * FROM tbl_webslesson_post
  ";
  $statement = $link->prepare($query);
  $statement->execute();
  return $statement->rowCount();
}

$total_record = get_total_row($link);*/

$limit = '5';
$page = 1;
if($_POST['page'] > 1)
{
  $start = (($_POST['page'] - 1) * $limit);
  $page = $_POST['page'];
}
else
{
  $start = 0;
}

$query = "
SELECT * FROM manager
";

if($_POST['query'] != '')
{
  $query .= '
  WHERE id_m LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" OR name LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" OR l_name LIKE "%'.str_replace(' ', '%', $_POST['query']).'%"
    OR a_name LIKE "%'.str_replace(' ', '%', $_POST['query']).'%"
    OR status LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" 
    OR mail LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" 
  ';
}

$query .= 'ORDER BY id_m DESC ';

$filter_query = $query . 'LIMIT '.$start.', '.$limit.'';

$statement = $link->prepare($query);
$statement->execute();
$total_data = $statement->rowCount();

$statement = $link->prepare($filter_query);
$statement->execute();
$result = $statement->fetchAll();
$total_filter_data = $statement->rowCount();

$output = '
<div class="text-center">
<label>Total - '.$total_data.'</label>
<table class="table table-hover table-striped text-center">
  <tr style="background-color: #232159; color: white">
    <th class="text-center align-middle">Logo</th>
    <th class="text-center align-middle">Providers Name</th>
    <th class="text-center align-middle">Owner</th>
    <th class="text-center align-middle">Mail</th>
    <th class="text-center align-middle">Status</th>
    <th class="text-center align-middle">Since</th>
    <th class="text-center align-middle"></th>
  </tr>
  <tbody class="table-bordered">
';
if($total_data > 0)
{
  foreach($result as $row)
  {

    if ($row['photo'] == 'NULL'){
      $output .= '
    <tr>
      <td class="text-center align-middle"></td>
      <td class="text-center align-middle">'.$row["a_name"].'</td>
      <td class="text-center align-middle">'.$row["name"].' '.$row["l_name"].'</td>
      <td class="text-center align-middle">'.$row["mail"].'</td>
      <td class="text-center align-middle">'.$row["status"].'</td>
      <td class="text-center align-middle">'.$row["db"].'</td>
      <td class="text-center align-middle" align="left">
        <ul>
                <a href="agency_info.php?art_id='.$row["id_m"].'" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                    <i class="fa fa-eye mr-2" id="preview"></i>
                                    Preview
                                </a>

            <br>
                 <a id="edit" href="edit_manager.php?art_id='.$row["id_m"].'" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                              <i class="fa fa-pencil-alt mr-2"></i>      
                                 Edit Providers
                                </a>
        </ul> 

            </td>
    </tr>
    </div>
    ';

    }
     elseif ($row['id_m'] == '0'){
      $output .= '
     <tr>
      <td class="text-center align-middle"><img src="../'.$row['photo'].'" width="90px;" height="90px";></td>
      <td class="text-center align-middle">'.$row["a_name"].'</td>
      <td class="text-center align-middle">'.$row["name"].' '.$row["l_name"].'</td>
      <td class="text-center align-middle">'.$row["mail"].'</td>
      <td class="text-center align-middle">'.$row["status"].'</td>
      <td class="text-center align-middle">'.$row["db"].'</td>
      <td class="text-center align-middle" align="left">
       

            </td>
    </tr>
    </div>
    ';

    }

     else {
      $output .= '
      <tr>
        <td class="text-center align-middle"><img src="../'.$row['photo'].'" width="90px;" height="90px";></td>
        <td class="text-center align-middle">'.$row["a_name"].'</td>
        <td class="text-center align-middle">'.$row["name"].' '.$row["l_name"].'</td>
        <td class="text-center align-middle">'.$row["mail"].'</td>
        <td class="text-center align-middle">'.$row["status"].'</td>
        <td class="text-center align-middle">'.$row["db"].'</td>
        <td class="text-center align-middle" align="left">
          <ul>
                  <a href="agency_info.php?art_id='.$row["id_m"].'" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview" id="preview">
                                      <i class="fa fa-eye mr-2"></i>
                                      Preview
                                  </a>
              <br>
                  <a id="edit" href="edit_manager.php?art_id='.$row["id_m"].'" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                <i class="fa fa-pencil-alt mr-2"></i>      
                                  Edit Providers
                                  </a>
          </ul> 

              </td>
      </tr>
    </div>
    ';
    }
    
  }
}
else
{
  $output .= '
  <tr>
    <td colspan="2" align="center">No Data Found</td>
  </tr>
  ';
}

$output .= '
</tbody>
</table>
<br />
<div align="center">
  <ul class="pagination justify-content-center">
';

$total_links = ceil($total_data/$limit);
$previous_link = '';
$next_link = '';
$page_link = '';

//echo $total_links;

if($total_links > 4)
{
  if($page < 5)
  {
    for($count = 1; $count <= 5; $count++)
    {
      $page_array[] = $count;
    }
    $page_array[] = '...';
    $page_array[] = $total_links;
  }
  else
  {
    $end_limit = $total_links - 5;
    if($page > $end_limit)
    {
      $page_array[] = 1;
      $page_array[] = '...';
      for($count = $end_limit; $count <= $total_links; $count++)
      {
        $page_array[] = $count;
      }
    }
    else
    {
      $page_array[] = 1;
      $page_array[] = '...';
      for($count = $page - 1; $count <= $page + 1; $count++)
      {
        $page_array[] = $count;
      }
      $page_array[] = '...';
      $page_array[] = $total_links;
    }
  }
}
else
{
  for($count = 1; $count <= $total_links; $count++)
  {
    $page_array[] = $count;
  }
}

for($count = 0; $count < count($page_array); $count++)
{
  if($page == $page_array[$count])
  {
    $page_link .= '
    <li class="page-item active">
      <a class="page-link" href="#">'.$page_array[$count].' <span class="sr-only">(current)</span></a>
    </li>
    ';

    $previous_id = $page_array[$count] - 1;
    if($previous_id > 0)
    {
      $previous_link = '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$previous_id.'">Previous</a></li>';
    }
    else
    {
      $previous_link = '
      <li class="page-item disabled">
        <a class="page-link" href="#">Previous</a>
      </li>
      ';
    }
    $next_id = $page_array[$count] + 1;
    if($next_id > $total_links)
    {
      $next_link = '
      <li class="page-item disabled">
        <a class="page-link" href="#">Next</a>
      </li>
        ';
    }
    else
    {
      $next_link = '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$next_id.'">Next</a></li>';
    }
  }
  else
  {
    if($page_array[$count] == '...')
    {
      $page_link .= '
      <li class="page-item disabled">
          <a class="page-link" href="#">...</a>
      </li>
      ';
    }
    else
    {
      $page_link .= '
      <li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$page_array[$count].'">'.$page_array[$count].'</a></li>
      ';
    }
  }
}

$output .= $previous_link . $page_link . $next_link;
$output .= '
  </ul>

</div>
';

echo $output;

?>

