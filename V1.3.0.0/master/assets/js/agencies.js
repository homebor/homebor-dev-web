//CORDINATOR PANEL LIST
$(document).ready(function() {

    load_data(1);

    function load_data(page, query = '') {
        $.ajax({
            url: "fetch_agencies.php",
            method: "POST",
            data: { page: page, query: query },
            success: function(data) {
                $('#dynamic_content').html(data);
            }
        });
    }

    $('#vou').hide();

    $('#search_box').keyup(function() {

        if ($('#search_box').val()) {

            $('#dynamic_content').hide();
            $('#vou').show();

            let search = $('#search_box').val();

            var searchLength = search.length;

            $.ajax({
                url: 'pro-search.php',
                type: 'POST',
                data: { search },
                success: function(response) {
                    let pro = JSON.parse(response);
                    let template = '';

                    pro.forEach(manager => {
                        if (manager.id_m != '0') {
                            if (manager.photo != 'NULL') {

                                template += `
          <tr>
            
            <th class="text-center align-middle" style="font-weight:normal"><img src="../${manager.photo}" width="90px;" height="90px";></th>
            <th class="text-center align-middle" style="font-weight:normal"> ${manager.a_name} </th>
            <th class="text-center align-middle" style="font-weight:normal"> ${manager.names} </th>
            <th class="text-center align-middle" style="font-weight:normal"> ${manager.mail} </th>
            <th class="text-center align-middle" style="font-weight:normal"> ${manager.status} </th>
            <th class="text-center align-middle" style="font-weight:normal"> ${manager.db} </th>
            <th class="text-center align-middle" align="left">
            <ul>
                <a href="agency_info.php?art_id=${manager.id_m}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                    <i class="fa fa-eye mr-2" id="preview"></i>
                                    Preview
                                </a>

            <br>
                 <a id="edit" href="edit_manager.php?art_id=${manager.id_m}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                              <i class="fa fa-pencil-alt mr-2"></i>      
                                 Edit Manager
                                </a>
           </ul>
            </th>

          </tr>`
                            } else {
                                template += `
            <tr>
              
              <th class="text-center align-middle" style="font-weight:normal"></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.a_name} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.mail} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.status} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.db} </th>
              <th class="text-center align-middle" align="left">
              <ul>
                  <a href="agency_info.php?art_id=${manager.id_m}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                      <i class="fa fa-eye mr-2" id="preview"></i>
                                      Preview
                                  </a>
  
              <br>
                   <a id="edit" href="edit_manager.php?art_id=${manager.id_m}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                <i class="fa fa-pencil-alt mr-2"></i>      
                                   Edit Manager
                                  </a>
             </ul>
              </th>
  
            </tr>`
                            }
                        } else {

                            template += `
            <tr>
  
              <th class="text-center align-middle" style="font-weight:normal"><img src="../${manager.photo}" width="90px;" height="90px";></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.a_name} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.mail} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.status} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${manager.db} </th>
              <th class="text-center align-middle" align="left">
              
              </th>
  
            </tr>`

                        }

                    });


                    $('#vouchers').html(template);
                }
            });
        } else {
            $('#dynamic_content').show();
            $('#vou').hide();
        }

    });

    $(document).on('click', '.page-link', function() {
        var page = $(this).data('page_number');
        var query = $('#search_box').val();
        load_data(page, query);
    });

});