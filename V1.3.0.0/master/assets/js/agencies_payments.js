//CORDINATOR PANEL LIST
$(document).ready(function() {

  load_data(1);

  function load_data(page, query = '') {
      $.ajax({
          url: "fetch_paymentagency.php",
          method: "POST",
          data: { page: page, query: query },
          success: function(data) {
              $('#dynamic_content').html(data);
          }
      });
  }

  $('#vou').hide();

  $('#search_box').keyup(function() {

      if ($('#search_box').val()) {

          $('#dynamic_content').hide();
          $('#vou').show();

          let search = $('#search_box').val();

          var searchLength = search.length;

          $.ajax({
              url: 'payp-search.php',
              type: 'POST',
              data: { search },
              success: function(response) {
                  let payp = JSON.parse(response);
                  let template = '';

                  payp.forEach(paypro => {

                    if(paypro.plan != 'Free'){
                      if(paypro.status == 'Waitlist'){
                        template += `
              <tr>

            <th style="font-weight:normal"> ${paypro.id_p} </th>
            <th style="font-weight:normal"> ${paypro.db} </th>
            <th style="font-weight:normal"> ${paypro.client} </th>
            <th style="font-weight:normal"> ${paypro.bank} </th>
            <th style="font-weight:normal"> ${paypro.plan} </th>
            <th style="font-weight:normal"> ${paypro.status} </th>
            <th align="left">
            <ul>
                <a href="../${paypro.photo}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview" target="_blank">
                                    <i class="fa fa-eye mr-2"></i>
                                    See Document
                                </a>

            <br>
            <form id="form-submit" class="ts-form" autocomplete="off" action="action-pay.php" method="post" enctype="multipart/form-data">
            <input type="text" class="form-control" id="num" name="mail" value="${paypro.mail}" hidden>
            <input type="text" class="form-control" id="num" name="id_p" value="${paypro.id_p}" hidden>
            <input type="text" class="form-control" id="num" name="id_m" value="${paypro.id_m}" hidden>
            <input type="text" class="form-control" id="num" name="plan" value="${paypro.plan}" hidden> 

            <button id="edit2" type="submit" class="btn btn-sm btn-block btn-outline-secondary " name="confirm">
                              <i class="fa fa-check"></i>      
                              Approve Payment
                                </button>
            <br>
            <button id="edit" type="submit" class="btn btn-sm btn-block btn-primary" name="deny">
                              <i class="fa fa-times mr-2"></i>      
                              Decline Payment
                                </button>
            </form>
        </ul>
            
            </th>

          </tr>`
                      }else {
                        template += `
              <tr>

            <th style="font-weight:normal"> ${paypro.id_p} </th>
            <th style="font-weight:normal"> ${paypro.db} </th>
            <th style="font-weight:normal"> ${paypro.client} </th>
            <th style="font-weight:normal"> ${paypro.bank} </th>
            <th style="font-weight:normal"> ${paypro.plan} </th>
            <th style="font-weight:normal"> ${paypro.status} </th>
            <th align="left">
            <ul>
                <a href="../${paypro.photo}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview" target="_blank">
                                    <i class="fa fa-eye mr-2"></i>
                                    See Document
                                </a>
        </ul>
            
            </th>

          </tr>`

                      }
                      
                    } else {
                      template += `
              <tr>

            <th style="font-weight:normal"> ${paypro.id_p} </th>
            <th style="font-weight:normal"> ${paypro.db} </th>
            <th style="font-weight:normal"> ${paypro.client} </th>
            <th style="font-weight:normal"> ${paypro.bank} </th>
            <th style="font-weight:normal"> ${paypro.plan} </th>
            <th style="font-weight:normal"> ${paypro.status} </th>
            <th align="left">
            </th>

          </tr>`

                    }
                   
            
                      
        });


                  $('#vouchers').html(template);
              }
          });
      } else {
          $('#dynamic_content').show();
          $('#vou').hide();
      }

  });

  $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
  });

});