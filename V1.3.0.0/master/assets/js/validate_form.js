const formulary = document.getElementById('form_master');

const inputs = document.querySelectorAll('#form_master input');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^\d{1,2}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/
}

const campos = {
    name: false,
    l_name: false,
    mail_s: false,
    password: false
}

const validateFormulary = (e) =>{
    switch(e.target.name){
        case "name":
            validateField(expresiones.nombre, e.target, 'name');
        break;
        
        case "l_name":
            validateField(expresiones.nombre, e.target, 'l_name');
        break;
        
        case "mail_s":
            validateField(expresiones.correo, e.target, 'mail_s');
        break;
        
        case "password":
            validateField(expresiones.password, e.target, 'password');
        break;

    }
}

const validateField = (expresion, input, campo) => {
    if(expresion.test(input.value)){
        document.getElementById(`group__${campo}`).classList.remove('form__group-incorrect');
        document.getElementById(`group__${campo}`).classList.add('form__group-correct');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.remove('form__group__input-error-active');
        campos[campo] = true;
    } else{
        document.getElementById(`group__${campo}`).classList.add('form__group-incorrect');
        document.getElementById(`group__${campo}`).classList.remove('form__group-correct');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.add('form__group__input-error-active');
        campos[campo] = false;
    }
}

inputs.forEach((input) =>{
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
})


formulary.addEventListener('submit', (e) =>{
    e.preventDefault();

    if(campos.name && campos.l_name && campos.mail_s && campos.password){

        document.getElementById('form__message-sucess').classList.add('form__message-sucess-active');

        setTimeout(() =>{
            document.getElementById('form__message-sucess').classList.remove('form__message-sucess-active');
        }, 5000);

        document.querySelectorAll('.form__group-correct').forEach((border) => {
            border.classList.remove('form__group-correct');
        });

        var registerForm = new FormData($('#form_master')[0]);

        $.ajax({
            url: 'action_student.php',
            type: 'POST',
            data: registerForm,
            contentType: false,
            processData: false,
            success: function(response) {

                window.top.location = 'edit_assigment.php?art_id='+response+'';

            }
        });

       

    } else{

        document.getElementById('form__message').classList.add('form__message-active');

        setTimeout(() =>{
            document.getElementById('form__message').classList.remove('form__message-active');
        }, 5000);

        setTimeout(() => {
            $("#house_much").fadeIn("slow");
    
            setTimeout(() => {
                $("#house_much").fadeOut("slow");
    
            }, 5000)
        }, 100);
    
        $('#close').on("click", function close(){
            event.preventDefault();
            $("#house_much").fadeOut("slow");
        });
        document.getElementById("contentp").innerHTML = "Please fill in the required fields correctly.";
        document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
        document.getElementById("contentp").style.marginTop  = "auto";
        document.getElementById("contentp").style.marginBottom  = "auto";
        document.getElementById("contentp").style.color = "#000";

        
    }
})