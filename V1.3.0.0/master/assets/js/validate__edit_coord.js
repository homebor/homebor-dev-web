const formulary = document.getElementById('form');

const inputs = document.querySelectorAll('#form input');
const textarea = document.querySelectorAll('#form textarea');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^\d{1,2}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
    message: /^[a-zA-ZÀ-ÿ0-9\s\W_.+-]{1,500}$/ // Description message.
}


const campos = {
    name: false,
    l_name: false,
    dir: false,
    city: false,
    state: false,
    p_code: false,
    a_name: false,
    num: false,
}


const validateFormulary = (e) =>{
    switch(e.target.name){
        case "name":
            validateField(expresiones.nombre, e.target, 'name');
        break;

        case "l_name":
            validateField(expresiones.nombre, e.target, 'l_name');
        break;
        
        case "a_mail":
            validateField(expresiones.correo, e.target, 'a_mail');
        break;
        
        case "pass":
            validateField(expresiones.password, e.target, 'pass');
        break;
        
        case "country":
            validateField(expresiones.message, e.target, 'country');
        break;
        
        case "city":
            validateField(expresiones.message, e.target, 'city');
        break;
        
        case "state":
            validateField(expresiones.message, e.target, 'state');
        break;
        

        
        case "num":
            validateField(expresiones.phone, e.target, 'num');
        break;
        
        case "num2":
            validateField(expresiones.phone, e.target, 'num2');
        break;
        
        case "experience":
            validateField(expresiones.rooms, e.target, 'experience');
        break;
        
        case "language":
            validateField(expresiones.message, e.target, 'language');
        break;
        
        case "specialization":
            validateField(expresiones.message, e.target, 'specialization');
        break;

    }
}

const validateField = (expresion, input, campo) => {
    if(expresion.test(input.value)){
        document.getElementById(`group__${campo}`).classList.remove('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.remove('form__group__input-error-active');
        campos[campo] = true;
    } else{
        document.getElementById(`group__${campo}`).classList.add('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.add('form__group__input-error-active');
        campos[campo] = false;
    }
}


inputs.forEach((input) =>{
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
});

textarea.forEach((textarea) =>{
    textarea.addEventListener('keyup', validateFormulary);
    textarea.addEventListener('blur', validateFormulary);
});


