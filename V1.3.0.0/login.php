<?php
error_reporting(0);?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->


  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/header/fonts/icomoon/style.css">

  <link rel="stylesheet" type="text/css" href="assets/css/html/index.css">

  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="assets/css/header_style.css">

  <link rel="stylesheet" type="text/css" href="assets/css/html/login.css?ver=1.1">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <script src="assets/js/scrollreveal.js"></script>


  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Login</title>

  <script src="https://www.google.com/recaptcha/api.js"></script>

  <script>
  function onSubmit(token) {
    document.getElementById("form__login").submit();
  }
  </script>



</head>

<body>

  <!-- WRAPPER
    =================================================================================================================-->
  <div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>

    <div class="site-mobile-menu-body"></div>
  </div>



  <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

    <?php include 'header.php' ?>

  </header>
  <!--HEADER ===============================================================-->






  <!--*********************************************************************************************************-->
  <!-- MAIN ***************************************************************************************************-->
  <!--*********************************************************************************************************-->

  <main id="ts-main" class="login__back">


    <div class="card card__div-login mt-auto mb-auto">

      <div class="login__div">

        <div class="col-lg-7 div_img_promo p-0">
          <div class="img__login"></div>
          <div class="logo_white"></div>
        </div>
        <div class="col-lg-5 login__div__col">

          <h2 class="title_login">Login</h2>

          <form action="#" id="form__login" class="login__content">

            <div class="form__group form__group__email" id="group__email">

              <div class="form__group__icon-input">
                <label for="email" class="form__label">Email</label>

                <label for="email" class="icon"><i class="icon icon__email"></i></label>
                <input type="text" id="email" name="email" class="form__group-input email" placeholder="User Mail">

              </div>

            </div>

            <div class="form__group form__group__password" id="group__password">

              <div class="form__group__icon-input">
                <label for="password" class="form__label">Password</label>

                <label for="password" class="icon"><i class="icon icon__password"></i></label>
                <input type="password" id="password" name="password" class="form__group-input password"
                  placeholder="Password">
                <button type="button" class="fa fa-eye mr-2 btn__eye" id="btn__eye"></button>

              </div>

            </div>

            <div class="not_robot__div mb-4">

              <div class="g-recaptcha" data-sitekey="6LdG5b0eAAAAAGz7pY6IAhBNQ4Vwyaov_0DgGDV6"></div>

            </div>

            <div class="btn__div">
              <a href="help" class="ts-text-small forgot__psw">
                <i class="fa fa-sync-alt ts-text-color-primary mr-2" id="login"></i>
                <span class="ts-text-color-light">I have forgot my password</span>
              </a>

              <div class="div__btn">
                <button type="submit" id="submit" class="btn__log-in">Log in</button>

              </div>

            </div>

            <?php 

                        if (isset($_GET["ucrt"]) && $_GET["ucrt"] == 'yes') { ?>

            <div class="created__div" id="created__div">
              <h6 class="created" id="created"></h6>
            </div>

            <?php 
                        }else{}

                        ?>

            <div class="message" id="message__div">
              <h6 id="message" class="form__group__input-error-message"></h6>
            </div>



          </form>

        </div>

      </div>

    </div>


  </main>
  <!--end #ts-main-->



  <!--end page-->

  <script>
  // If the user has created Successfully

  document.getElementById('created__div').classList.add('block2');

  setTimeout(() => {
    let message2 = 'User Created Successfully';
    $('#created').html(message2);

    setTimeout(() => {
      document.getElementById('created__div').classList.remove('block2');

    }, 20000)
  }, 100);
  </script>

  <script src="assets/js/custom.js"></script>

  <script src="assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/header/js/jquery.sticky.js"></script>
  <script src="assets/js/mainHeader.js"></script>
  <script src="assets/js/recover__psw.js"></script>
  <script src="assets/js/validate__login.js?ver=1.1"></script>

</body>

</html>