<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/header/fonts/icomoon/style.css">
    
    <!--CSS -->

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/header_style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/help.css">




    

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <script src="assets/js/jquery-3.3.1.min.js"></script>
    
    <title>Homebor - Help</title>
</head>
<body>

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        
        <div class="site-mobile-menu-body"></div>
    </div>

    

    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

        <?php include 'header.php' ?>

    </header>


    <main class="ts-main main">
    
        <!-- Help Content -->

        <div class="div__container__help">

            <div class="col-md-3">

                <div class="card card__section-help">
                    <a href="#" class="nav-link link__help active1" onclick="first()">FAQ</a>
                    <a href="#" class="nav-link link__help" id="second" onclick="second()">Contact Us</a>
                    <a href="#" class="nav-link link__help" onclick="third()">Recover Password</a>
                </div>

            </div>

            

            <div class="col-md-9 container__help" id="container__help">

                <!-- Recent Questions -->
                <div class="card content__help display1" id="questions">
                        
                    <h3 class="title__help">Recent Questions <a href="#" onclick="second()" id="submit_question" class="link__new__message a">Submit a Question</a> <br> <a href="#" onclick="second()" id="submit_question" class="link__new__message b">Submit a Question</a> </h3>


                    <div class="content__message-help">
                        
                        <div class="message__help">

                            <div class="div__photo__user-help p-0">
                                <img class="photo__user-help" src="public_s/m@gmail.com/man-avatar-profile-round-icon_24640-14046.jpg">
                            </div>
                            
                            <div class="div__content__user-help">
                                
                                <h5 class="name__user">How Register on Plataform? <p class="date__message">12-01-2022</p></h5>
                                    
                                <p class="content__question"> Duis ac dolor et enim volutpat semper. Morbi placerat tempor ornare. Quisque bibendum ultrices diam, ac fermentum massa egestas quis. Nullam eu nunc in sem efficitur dapibus. Aliquam ultricies elit nisi, rhoncus euismod purus iaculis et. Nulla facilisi. Sed aliquet ante quis dui congue, sed ornare diam feugiat. </p>
                                
                            </div>
                            
                        </div>

                        <div class="message__help">

                            <div class="div__photo__user-help p-0">
                                
                                <img class="photo__user-help" src="public_s/m@gmail.com/man-avatar-profile-round-icon_24640-14046.jpg">
                                
                            </div>
                                         
                            <div class="div__content__user-help">
                                    
                                <h5 class="name__user">Why have i been blocked? <p class="date__message">09-01-2022</p></h5>
                                    
                                <p class="content__question"> Proin odio dolor, tincidunt eget dictum volutpat, consequat cursus libero. Suspendisse potenti. Curabitur nec venenatis sapien, vel malesuada mi. Nunc malesuada nisl eu consectetur venenatis. </p>
                                
                            </div>
                            
                        </div>
                        
                        <div class="message__help">
 
                            <div class="div__photo__user-help p-0">
                                
                                <img class="photo__user-help" src="public_s/m@gmail.com/man-avatar-profile-round-icon_24640-14046.jpg">
                                
                            </div>
                                
                                
                            <div class="div__content__user-help">
                                    
                                <h5 class="name__user">How can i find my Homestay? <p class="date__message">05-01-2022</p></h5>
                                    
                                <p class="content__question"> Nullam sed pulvinar lectus, quis molestie dolor. Morbi vitae porta eros. Duis ut nulla pellentesque justo ornare condimentum quis ut magna. Aenean nec egestas ligula, at euismod ipsum. Praesent sed gravida libero. Pellentesque eleifend non diam eget molestie. Quisque est elit, tincidunt quis nisi a, placerat mollis eros. Mauris aliquam mollis volutpat. Mauris accumsan sed est in pulvinar. </p>

                                <p class="content__question"> Phasellus dictum id lorem sed dictum. Vivamus urna velit, condimentum sit amet pulvinar et, lobortis nec tellus. Fusce vel pellentesque massa, vel dapibus nunc. Proin odio dolor, tincidunt eget dictum volutpat, consequat cursus libero. Suspendisse potenti. Curabitur nec venenatis sapien, vel malesuada mi. Nunc malesuada nisl eu consectetur venenatis. </p>
                                
                            </div>
                            
                        </div>
                            

                    </div>

                </div>


                <!-- Contact Us -->
                <div class="card content__help display2" id="contact">
                        
                    <h3 class="title__help">Contact Us </h3>

                    <div class="div__contact">
                        
                        <div class="col-lg-3 p-0 num__contact ">
                            
                            <div class="num__contact-details">
                                
                                <label class="label__title" for="number">Phone</label>
                                    
                                <label class="icon__number" for="number"></label>
                                    
                                <p class="content__contact" id="number">+1 (385) 206-3514</p>

                            </div>
                                
                            <div class="num__contact-details">
                                
                                <label class="label__title" for="email">Email</label>
                                <label class="icon__email3" for="email"></label>
                                <p class="content__contact email" id="email">info@homebor.com</p>

                            </div>

                        </div>

                        <div class="col-lg-9 p-0 contact__form">
                            
                            <h3 class="title__message">Send us your Message</h3>

                            <form action="#" class="w-100" class="form_contact" id="form_contact">


                                <div class="fr__group">
                                    
                                    <div class="form__group form__group__name" id="group__name">

                                        <div class="form__group__icon-input">
                                            
                                            <label for="name" class="form__label">First Name</label>

                                            <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                            <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                                        </div>

                                        <p class="form__group__input-error">Without special characters or numbers</p>
                                                
                                    </div>
                                        
                                        
                                    <div class="form__group form__group__email" id="group__email">
                                            
                                        <div class="form__group__icon-input">
                                                
                                            <label for="email" class="form__label">Email</label>

                                            <label for="email" class="icon"><i class="icon icon__email"></i></label>
                                            <input type="text" id="email" name="email" class="form__group-input email1" placeholder="e.g. John Smith">

                                        </div>

                                        <p class="form__group__input-error">It is not a valid Email</p>
                                                
                                    </div>

                                </div>

                                <div class="fr__group">

                                    <div class="form__group form__group__subject" id="group__subject">

                                        <div class="form__group__icon-input">
                                            <label for="subject" class="form__label">Subject</label>

                                            <label for="subject" class="icon"><i class="icon icon__subject"></i></label>
                                            <input type="text" id="subject" name="subject" class="form__group-input subject" placeholder="e.g. How can I Register?">

                                        </div>

                                        <p class="form__group__input-error">It is not a valid Email</p>
                                            
                                    </div>

                                </div>
                                    
                                <div class="fr__group">

                                    <div class="form__group form__group__message" id="group__message">

                                        <div class="form__group__icon-input">
                                            <label for="message" class="form__label label__message">Message Content</label>

                                            <textarea rows="6" id="message" name="message" class="form__group-input message" placeholder="e.g. How can I Register?"></textarea>

                                        </div>
                                        <p class="form__group__input-error">Please write your question</p>
                                            
                                    </div>

                                </div>

                                <div class="form__message" id="form__message">
                                    <p>Please fill in the fields Correctly</p>
                                </div>

                                <div class="fr__group div__btn__send" id="btn__success">

                                    <button type="submit" name="submit" class="btn__send_concact" id="btn_"> Send Message </button>
                                        
                                </div>

                                <p class="form__message-sucess" id="form__message-sucess"> Message sent</p>


                            </form>


                        </div>

                    </div>

                </div>

                <!-- Recover Your Password -->
                <div class="card content__help display3" id="recover">
                        
                    <h3 class="title__help"> Recover Your Password </h3>

                    <form action="#" id="form__recover" method="POST" class="div__recover-pssw">

                        <div class="input__mail_r">

                            <div class="div__input__mail_r">

                                <label for="email_r" class="label__g label__email_r"> Enter Your Mail </label>
                                <label for="email_r" class="icon icon__email2"></label>
                                <input type="text" name="email" class="inputs__contact mail_r" id="email_r" placeholder="Write your Email">

                            </div>

                        </div>

                        <div class="div__send" id="div__send__recovery">

                            <button type="submit" name="submit" class="btn__send" id="btn__send_recover"> Send Email </button>

                        </div>

                        <p class="text__warning" id="message__sent"></p>
                        <p class="text__warning" id="message__error"></p>

                    </form>

                </div>

            </div>

        </div>

    </main>

    <?php include 'footer.php' ?>


    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/header/js/jquery.sticky.js"></script>
<script src="assets/js/mainHeader.js"></script>
<script src="assets/js/html/about_us.js"></script>
<script src="assets/js/validate_form.js"></script>
<script src="assets/js/recover__psw.js"></script>

    
</body>
</html>