<?php
include_once('../../xeon.php');
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$ID_STUDENT = $_POST['student_id'];
$section = $_POST['section'];

$queryUser = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row_user = $queryUser->fetch_assoc();

$queryAgents = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
$row_agent = $queryAgents->fetch_assoc();


$queryHomestay = $link->query("SELECT * FROM pe_home WHERE id_m = '$row_agent[id_m]' ");
$row_homestay = $queryHomestay->fetch_assoc();


$queryStudent = $link->query("SELECT * FROM pe_student WHERE id_student = '$ID_STUDENT' ");
$row_student = $queryStudent->fetch_assoc();

$queryAcademy_IJ = $link->query("SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$ID_STUDENT' and academy.id_ac = pe_student.n_a");
$row_academy_IJ = $queryAcademy_IJ->fetch_assoc();


if ($row_user['usert'] != 'Agent')  echo json_encode(['redirect', '../logout.php']), exit;
if ($row_agent['id_m'] != $row_student['id_m']) echo json_encode(['redirect', 'index.php']), exit;

while ($value = mysqli_fetch_array($queryHomestay)) {
  $queryEvents = $link->query("SELECT * FROM events WHERE email = '$value[mail_h]' ");

  $allEvents = null;
  while ($event = mysqli_fetch_array($queryEvents)) $allEvents[] = $event;


  $queryRooms = $link->query("SELECT * FROM room WHERE id_home = '$value[id_home]' ");
  $row_room = $queryRooms->fetch_assoc();

  if (isset($value) && isset($row_room)) $allHouses[] = array($value, $allEvents, $row_room);
}

if ($section == 'student') {
  $jsonToSend = array(
    'student' => array(
      'image' => $row_student['photo_s'],
      'name' => $row_student['name_s'],
      'lastname' => $row_student['l_name_s'],
      'nationality' => $row_student['nationality'],
      'academy' => $row_academy_IJ['name_a'],
      'addressAcademy' => $row_academy_IJ['dir_a'],
      'cityAcademy' => $row_academy_IJ['city_a'],
      'stateAcademy' => $row_academy_IJ['state_a'],
      'firstDay' => $row_student['firstd'],
      'lastDay' => $row_student['lastd'],
      'gender' => $row_student['gen_s'],
      'dateBirth' => $row_student['db_s'],
      'more' => array(
        ['Accommodation', $row_student['lodging_type']],
        ['Smoke', $row_student['smoke_s']],
        ['Pets', $row_student['pets']],
        ['Food', $row_student['food']],
        ['Vegetarian', $row_student['vegetarians']],
        ['Halal', $row_student['halal']],
        ['Kosher', $row_student['kosher']],
        ['Lactose', $row_student['lactose']],
        ['Gluten', $row_student['gluten']],
        ['Pork', $row_student['pork']],
      )
    ),
  );
} else {
  $jsonToSend = [$allHouses, $row_student];
}


echo json_encode($jsonToSend);