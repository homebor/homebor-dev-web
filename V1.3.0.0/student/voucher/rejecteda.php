<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';
session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM vouches WHERE email = '$usuario' AND id_v = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM manager WHERE id_m = '$row[id_m]'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

if ($usuario != $row['email']) {
	 header("location: ../index.php");
}
if ($row['reason'] == 'NULL') {
	$reason = " ";
}else{
	$reason = '1- '.$row['reason'];
}
if ($row['reason2'] == 'NULL') {
	$reason2 = " ";
}else{
	$reason2 =  '2- '.$row['reason2'];	
}
if ($row['reason3'] == 'NULL') {
	$reason3 = " ";
}else{
	$reason3 = '3- '. $row['reason3'];
}
}


$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetTitle('Rejected');
$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(0,0,255);
$pdf->Text(15,40,$row2['mail'],'C');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',11);
$pdf->Text(160,40,$row['dates'],'C');
$pdf->SetFont('Arial','B',26);
$pdf->SetXY(0,70);
$pdf->Cell(0,0,'Sorry, your application has not been',0,0,'C');
$pdf->SetFont('Arial','B',26);
$pdf->SetXY(0,80);
$pdf->Cell(0,0,'approved by the homestay!',0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,95);
$pdf->Cell(0,0,"'$row[h_name]' located at: '$row[dir]'; has rejected your application for the",0,0,'C');
$pdf->SetXY(0,100);
$pdf->Cell(0,0,'following reasons:',0,0,'C');

$pdf->SetFont('Arial','',40);
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,110);
$pdf->Cell(0,0,"$reason",0,0,'C');
$pdf->SetXY(0,115);
$pdf->Cell(0,0,"$reason2",0,0,'C');
$pdf->SetXY(0,120);
$pdf->Cell(0,0,"$reason3",0,0,'C');
$pdf->SetFont('Arial','',40);
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,130);
$pdf->Cell(0,0,'Keep looking for your ideal homestay with us.',0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,140);
$pdf->Cell(0,0,'From your personal area you can contact another homestay.',0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->Image('../../assets/logos/homebor.png',20,165,-700);
$pdf->Image("../../$row2[photo]",120,155,-200);
$pdf->SetXY(10,235);
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(152,42,114);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,10,'KEEP LOOKING FOR YOUR IDEAL HOMESTAY WITH US',1, 0, 'C', True, '../index.php');
$pdf->Output();
?>