<?php
session_start();
include_once('../xeon.php');   
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_student WHERE id_student = '$id' ");
$row=$query->fetch_assoc();

$_SESSION['mail_s'] = $row['mail_s']; 

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$id' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);
$row2=$resultado2->fetch_assoc();



$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if($row['status'] == 'Homestay Found'){
    $query7 = $link->query("SELECT * FROM events WHERE mail_s = '$row[mail_s]' ");
    $row7=$query7->fetch_assoc();

    $query4 = $link->query("SELECT * FROM pe_home WHERE mail_h = '$row7[email]' ");
    $row4=$query4->fetch_assoc();

    if($row7['color'] == '#232159'){
        $room = '1';
    }elseif($row7['color'] == '#982A72'){
        $room = '2';
    }elseif($row7['color'] == '#394893'){
        $room = '3';
    }elseif($row7['color'] == '#A54483'){
        $room = '4';
    }elseif($row7['color'] == '#5D418D'){
        $room = '5';
    }elseif($row7['color'] == '#392B84'){
        $room = '6';
    }elseif($row7['color'] == '#B15391'){
        $room = '7';
    }elseif($row7['color'] == '#4F177D'){
        $room = '8';
    }
    
    $status = $row7['status'];

    $start = date_create($row7['start']);
    $end = date_create($row7['end']);
}


if ($row5['usert'] != 'Admin') {
        if ($row5['usert'] != 'Cordinator') {
         header("location: ../logout.php");   
        }
    }

    if ($row5['usert'] == 'Admin') {
        $way = '../master/index.php';
    } elseif ($row5['usert'] == 'Cordinator') {
       $way = '../master/cordinator.php';
        echo '<style type="text/css"> a#admin{display:none;} </style>';
    }

}

if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
     header("location: ../logout.php");   
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_info2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/delete_stu.css">
    <link rel="stylesheet" type="text/css" href="../master/assets/css/notification.css">


    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Profile</title>

</head>
<body>

<header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
     =============================================================================================================-->
    <?php include '../master/header.php' ?>

</header>
<!--end Header-->


<div class="container_register">

    <main class="card__register">
        <form class="ts-form" action="edit.php" method="POST" autocomplete="off" enctype="multipart/form-data" >

            <div class="row m-0 p-0">

                <div class="col-md-4 column-1">

                    <!-- If the Student have a Active Reservation -->

                    <?php if($row['status'] == 'Homestay Found' && $status == 'Active'){ ?>

                    <div class="card active__res_card">

                        <h3 class="form__group-title__col-4 active_res">Active Reservation</h3>

                        <a href="../master/homestay_admin?art_id=<?php echo $row4['id_home'] ?>" class="m-0 p-0"><img class="img__house" src="../<?php echo $row4['phome'] ?>" alt=""></a>
                        
                        <div class="arrow__div"></div>

                        <div class="details__res_div">

                            <div class="row details__row">

                                <div class="detail detail__room col-md-3">
                                    <label class="label__details label__room" for="room">Room</label>
                                    <p id="room" class="text room__text"><?php echo $room ?></p>
                                </div>
                                <div class="detail detail__dates col-md-4">
                                    <label class="label__details label__arrive" for="aarive">Arrive</label>

                                    
                                    <p id="aarive" class="text aarive__text"><?php echo date_format($start, 'm / d / Y'); ?></p>

                                </div>
                                <div class="detail detail__datee col-md-4">
                                    <label class="label__details label__leave" for="leave">Leave</label>
                                    <p id="leave" class="text leave__text"><?php echo date_format($end, 'm / d / Y'); ?></p>
                                </div>

                            </div>

                        </div>

                    </div>

                    <?php }else{} ?>

                    <div class="card">

                        <h3 class="form__group-title__col-4">Basic Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">
                                
                                <img class="form__group__photo" id="preview" src="../<?php echo $row['photo_s'] ?>" alt="">
                                
                            </div>

                                <!-- Group Name -->

                                <?php if(empty($row['name_s']) || $row['name_s'] == 'NULL' ){}else{ ?>

                                    <div class="form__group form__group__name" id="group__name">
                                        <div class="form__group__icon-input">
                                            <label for="name" class="form__label">First Name</label>
                                            
                                            <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                            <p id="name" name="name" class="form__group-input names"><?php echo $row['name_s']?></p>

                                        </div>
                                        
                                    </div>

                                <?php } ?>

                                
                                <!-- Group Last Name -->

                                <?php if(empty($row['l_name_s']) || $row['l_name_s'] == 'NULL' ){}else{ ?>

                                    <div class="form__group form__group__l_name" id="group__l_name">
                                        <div class="form__group__icon-input">

                                            <label for="l_name" class="form__label">Last Name</label>
                                            <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                                            <p id="l_name" name="l_name" class="form__group-input names" > <?php echo "$row[l_name_s]"?> </p>

                                        </div>
                                    </div>

                                <?php } ?>
                            
                            <!-- Group Number -->

                            <?php if(empty($row['num_s']) || $row['num_s'] == 'NULL' ){}else{ ?>

                                <div class="form__group form__group__num_s" id="group__num_s">

                                    <div class="form__group__icon-input">

                                        <label for="num_s" class="form__label">Phone Number</label>

                                        <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>

                                        <p id="num_s" name="num_s" class="form__group-input num_s"> <?php echo $row['num_s']; ?> </p>
                                    

                                    </div>
                                </div>

                            <?php } ?>

                            <!-- Group Mail -->

                            <?php if(empty($row['mail_s']) || $row['mail_s'] == 'NULL' ){}else{ ?>

                                <div class="form__group form__group__mail_s" id="group__mail_s">

                                    <div class="form__group__icon-input">

                                        <label for="mail_s" class="form__label">Mail Student</label>

                                        <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>

                                        <p id="mail_s" name="mail_s" class="form__group-input mail"> <?php echo $row['mail_s']; ?> </p>
                                    

                                    </div>
                                </div>

                            <?php } ?>

                            <div class="btns__options">
                        
                                <a href="assignation.php?art_id=<?php echo $row["id_student"] ?>" class="btn__op assignation__hom"> Homestay Assignation </a>
                                
                                <a href="edit_assigment.php?art_id=<?php echo $row["id_student"] ?>" class="btn__op assignation__edit"> Edit Assigment </a>

                            </div>
                        
                        </div>

                    </div>


                    <?php if($row['n_airline'] == 'NULL' && $row['n_flight'] == 'NULL' && $row['departure_f'] == 'NULL' && $row['n_flight'] == 'NULL' ){}else{ ?>


                        <div class="card">


                            <h3 class="form__group-title__col-4">Flight Information</h3>

                            <div class="information__content">
                            
                                <!-- Group Booking Confirmation -->

                                <?php if(empty($row['n_airline']) || $row['n_airline'] == 'NULL' ){}else{ ?>

                                    <div class="form__group form__group__n_airline" id="group__n_airline">
                                        <div class="form__group__icon-input">

                                            <label for="n_airline" class="form__label">Booking Confirmation</label>

                                            <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                                            <p id="n_airline" name="n_airline" class="form__group-input n_airline"> <?php echo $row['n_airline']; ?> </p>

                                        </div>
                                    </div>

                                <?php } ?>
                                
                                <!-- Group Booking Confirmation -->

                                <?php if(empty($row['n_flight']) || $row['n_flight'] == 'NULL' ){}else{ ?>

                                    <div class="form__group form__group__n_flight" id="group__n_flight">
                                        <div class="form__group__icon-input">

                                            <label for="n_flight" class="form__label">Landing Flight Number</label>

                                            <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                                            <p id="n_flight" name="n_flight" class="form__group-input n_flight"> <?php echo $row['n_flight']; ?> </p>

                                        </div>
                                    </div>

                                <?php } ?>

                                <?php if(empty($row['departure_f']) || $row['departure_f'] == 'NULL' ){}else{ ?>

                                    <div class="form__group form__group-f_date ">
                                        <div class="form__group__icon-input">
                                            <label for="f_date" class="form__label">Flight Date</label>
                                            <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                                            <?php $roc1 = date_create($row['departure_f']); ?>

                                            <p id="f_date" name="f_date" class="form__group-input f_date"> <?php echo date_format($roc1, 'jS F Y'); ?> </p>
                                            
                                        </div>
                                    </div>

                                <?php } 
                                
                                if(empty($row['n_flight']) || $row['n_flight'] == 'NULL' ){}else{ ?>
                                    
                                    <div class="form__group form__group-h_date">

                                        <div class="form__group__icon-input">

                                            <label for="h_date" class="form__label">Arrival at the Homestay</label>
                                            <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                                            <?php $roc2 = date_create($row['arrive_f']); ?>

                                            <p id="h_date" name="h_date" class="form__group-input h_date"> <?php echo date_format($roc2, 'jS F Y'); ?> </p>


                                        </div>
                                    </div>

                                <?php } ?>

                            </div>

                        </div>

                    <?php } 
                    
                    
                    if($row['cont_name'] == 'NULL' && $row['cont_lname'] == 'NULL' && $row['num_conts'] == 'NULL' && $row['cell_s'] == 'NULL' ){}else{
                    ?>



                        <div class="card">

                            <h3 class="form__group-title__col-4">Emergency Contact</h3>

                            <div class="information__content">


                                    <!-- Group Emergency Name -->

                                    <?php if(empty($row['cont_name']) || $row['cont_name'] == 'NULL' ){}else{ ?>

                                        <div class="form__group form__group__cont_name" id="group__cont_name">
                                            <div class="form__group__icon-input">

                                                <label for="cont_name" class="form__label">Contact Name</label>

                                                <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>

                                                <p id="cont_name" name="cont_name" class="form__group-input cont_name" > <?php echo $row['cont_name']; ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Emergency Last Name -->

                                    <?php if(empty($row['cont_lname']) || $row['cont_lname'] == 'NULL' ){}else{ ?>

                                        <div class="form__group form__group__cont_lname" id="group__cont_lname">

                                            <div class="form__group__icon-input">

                                                <label for="cont_lname" class="form__label">Contact Last Name</label>

                                                <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>

                                                <p id="cont_lname" name="cont_lname" class="form__group-input cont_lname"> <?php echo $row['cont_lname'] ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>


                                    <!-- Group Emergency phone -->

                                    <?php if(empty($row['num_conts']) || $row['num_conts'] == 'NULL' ){}else{ ?>

                                        <div class="form__group form__group__num_conts" id="group__num_conts">

                                            <div class="form__group__icon-input">

                                                <label for="num_conts" class="form__label">Emergency Phone Number</label>

                                                <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                                                <p id="num_conts" name="num_conts" class="form__group-input num_conts"> <?php echo $row['num_conts']; ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Alternative Phone -->

                                    <?php if(empty($row['cell_s']) || $row['cell_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group form__group__cell_s" id="group__cell_s">
                                            <div class="form__group__icon-input">

                                                <label for="cell_s" class="form__label">Alternative Phone Number</label>

                                                <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                                                <p id="cell_s" name="cell_s" class="form__group-input cell_s"> <?php echo $row['cell_s'] ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>


                            </div>

                        </div>

                    <?php } ?>

                
                </div>



                <div class="col-md-8 column-2">

                    <?php if($row['db_s'] == 'NULL' && $row['gen_s'] == 'NULL' && $row['nacionality'] == 'NULL' && $row['city'] == 'NULL' && $row['passport'] == 'NULL' && $row['db_visa'] == 'NULL' && $row['visa'] == 'NULL' ){}else{ ?>

                        <div class="card">

                            <h3 class="form__group-title__col-4">Personal Information</h3>

                            <div class="information__content information__content-col-8">

                                <div class="row form__group-basic-content m-0 p-0">

                                    <!-- Group Date of Birth -->

                                    <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__db" id="group__db">
                                            <div class="form__group__icon-input">

                                                    <label for="db" class="form__label">Date of Birth</label>
                                                    <label for="db" class="icon"><i class="icon icon__date"></i></label>

                                                    <?php $roc3 = date_create($row['db_s']); ?>

                                                    <p id="db" name="db" class="form__group-input db"> <?php echo date_format($roc3, 'jS F Y'); ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Gender -->

                                    <?php if(empty($row['gen_s']) || $row['gen_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__gender" id="group__gender">

                                            <div class="form__group__icon-input">

                                                <label for="gender" class="form__label">Gender</label>

                                                <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                                                <p id="gender" name="gender" class="form__group-input gender"> <?php echo $row['gen_s'] ?> </p>

                                            </div>
                                        </div>

                                    <?php } ?>

                                    
                                    
                                    <!-- Group Background -->

                                    <?php if(empty($row['nacionality']) || $row['nacionality'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__nacionality" id="group__nacionality">
                                            <div class="form__group__icon-input">

                                                <label for="nacionality" class="form__label">Background</label>

                                                <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>

                                                <p id="nacionality" name="nacionality" class="form__group-input nacionality"> <?php echo $row['nacionality'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Origin City -->

                                    <?php if(empty($row['city']) || $row['city'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__city" id="group__city">
                                            <div class="form__group__icon-input">

                                                <label for="city" class="form__label visa">Origin City</label>

                                                <label for="city" class="icon"><i class="icon icon__city"></i></label>

                                                <p id="city" name="city" class="form__group-input city"> <?php echo $row['city'] ?> </p>


                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Passport -->

                                    <?php if(empty($row['passport']) || $row['passport'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__pass" id="group__pass">
                                            <div class="form__group__icon-input">

                                                <label for="pass" class="form__label visa">Passport</label>

                                                <label for="pass" class="icon"><i class="icon icon__pass"></i></label>

                                                <p id="pass" name="pass" class="form__group-input pass"> <?php echo $row['passport'] ?> </p>


                                            </div>
                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Visa Expiration Date -->

                                    <?php if(empty($row['db_visa']) || $row['db_visa'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                                            <div class="form__group__icon-input">

                                                <label for="bl" class="form__label visa">Visa Expiration Date</label>
                                                <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                                                <?php $roc4 = date_create($row['db_visa']); ?>

                                                <p id="bl" name="bl" class="form__group-input bl"> <?php echo date_format($roc4, 'jS F Y'); ?> </p>

                                            </div>

                                        </div>

                                    <?php } 
                                    
                                    
                                    if(empty($row['visa']) || $row['visa'] == 'NULL' ){}else{ ?>


                                        <div class="form__group col-sm-6 form__group__basic form__group__visa" id="group__visa">
                                            
                                            <div class="form__group__icon-input">
                                                

                                                <label for="visa" class="form__label visa">Visa</label>
                                                <label for="visa" class="icon"><i class="icon icon__city icon__visa"></i></label>

                                                <iframe class="embed-responsive-item w-100" src="../<?php echo $row['visa'] ?>" id="visa"></iframe>

                                            </div>
                                        </div>

                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                    <?php } 
                    
                    if($row['name_a'] == 'NULL' && $row['lang_s'] == 'NULL' && $row['type_s'] == 'NULL' && $row['firstd'] == 'NULL' && $row['lastd'] == 'NULL' ){}else{
                    
                    ?>


                        <div class="card">

                            <h3 class="form__group-title__col-4">Academy Information</h3>

                            <div class="information__content information__content-col-8">

                                <div class="row form__group-basic-content m-0 p-0">


                                    <!-- Group Academy Preference -->

                                    <?php if(empty($row2['name_a']) || $row2['name_a'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                                            <div class="form__group__icon-input">

                                                <label for="n_a" class="form__label">Academy Preference</label>

                                                <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                                                <p id="n_a" name="n_a" class="form__group-input n_a"> <?php echo $row2['name_a'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>


                                    <!-- Group Origin Language -->

                                    <?php if(empty($row['lang_s']) || $row['lang_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__lang_s" id="group__lang_s">
                                            <div class="form__group__icon-input">

                                                <label for="lang_s" class="form__label">Origin Language</label>

                                                <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>

                                                <p id="lang_s" name="lang_s" class="form__group-input lang_s"> <?php echo $row['lang_s'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Type Student -->

                                    <?php if(empty($row['type_s']) || $row['type_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                                            <div class="form__group__icon-input">

                                                <label for="type_s" class="form__label">Type Student</label>

                                                <label for="type_s" class="icon"><i class="icon icon__names"></i></label>
                                                
                                                <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['type_s'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>

                                    <!-- Group Start Date of Stay -->

                                    <?php if(empty($row['firstd']) || $row['firstd'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__firstd" id="group__firstd">
                                            <div class="form__group__icon-input">
                                                
                                                <label for="firstd" class="form__label">Start Date of Stay</label>
                                                <label for="firstd" class="icon"><i class="icon icon__date"></i></label>
                                                
                                                <?php $roc5 = date_create($row['firstd']); ?>

                                                <p id="firstd" name="firstd" class="form__group-input firstd"> <?php echo date_format($roc5, 'jS F Y'); ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group End Date of stay -->

                                    <?php if(empty($row['lastd']) || $row['lastd'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__lastd" id="group__lastd">
                                            <div class="form__group__icon-input">
                                                
                                                <label for="lastd" class="form__label">End Date of Stay</label>
                                                <label for="lastd" class="icon"><i class="icon icon__date"></i></label>

                                                <?php $roc6 = date_create($row['lastd']); ?>

                                                <p id="lastd" class="form__group-input lastd"> <?php echo date_format($roc6, 'jS F Y'); ?> </p>
                                            </div>

                                        </div>

                                    <?php } ?>
                                
                                </div>

                            </div>

                        </div>

                    <?php } 
                    
                    if($row['smoke_s'] == 'NULL' && $row['pets'] == 'NULL' && $row['food'] == 'NULL' && $row['allergies'] == 'NULL' && $row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{
                    
                    ?>


                        <div class="card">

                            <h3 class="form__group-title__col-4">Preferences Information</h3>

                            <div class="information__content information__content-col-8">

                                <div class="row form__group-basic-content m-0 p-0">

                                    <!-- Group Smoker -->

                                    <?php if(empty($row['smoke_s']) || $row['smoke_s'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                                            <div class="form__group__icon-input">

                                                <label for="smoke_s" class="form__label smoker">Smoker</label>

                                                <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                                                <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $row['smoke_s'] ?> </p>
                                            </div>


                                        </div>
                                    <?php } ?>

                                    <!-- Group Pets -->

                                    <?php if(empty($row['pets']) || $row['pets'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__pets" id="group__pets">

                                            <div class="form__group__icon-input">

                                                <label for="pets" class="form__label pets_l">Pets</label>

                                                <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                                                <p id="pets" class="form__group-input pets"> <?php echo $row['pets'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    <!-- Group Food -->

                                    <?php if(empty($row['food']) || $row['food'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-4 form__group__basic form__group__food" id="group__food">

                                            <div class="form__group__icon-input">

                                                <label for="food" class="form__label pets_l">Food</label>

                                                <label for="food" class="icon"><i class="icon icon__food"></i></label>

                                                <p id="food" class="form__group-input food"> <?php echo $row['food'] ?> </p>

                                            </div>

                                        </div>

                                    <?php } ?>
                                    
                                    
                                    <!-- Group Allergies -->

                                    <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL' ){}else{ ?>

                                        <div class="form__group col-sm-12 form__group__basic form__group__allergies" id="group__allergies">

                                            <div class="form__group__icon-input">

                                                <label for="allergies" class="form__label allergies_l">Allergies</label>

                                                <label for="allergies" class="icon"><i class="icon icon__allergies"></i></label>

                                                <p class="form__group-input allergies" id="allergies"><?php echo $row['allergies'] ?></p>


                                            </div>

                                        </div>

                                    <?php } ?>

                                    
                                    <?php if($row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{ ?>

                                        <!-- Group Special -->
                                        <div class="form__group form__group__basic form__group__special-diet" id="group__special-diet">
                                        
                                            <div class="form__group__div-special__diet">
                                                <div class="form__group__icon-input">

                                                    <label for="" class="icon"><i class="icon icon__diet"></i></label>
                                                    <h4 class="title__special-diet">Special Diet</h4>

                                                </div>
                                            

                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">
                                                        <?php if($row['vegetarians'] == "yes"){ ?>

                                                            <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes" checked disabled>

                                                        <?php } else{ ?>

                                                            <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">

                                                        <?php if($row['halal'] == "yes"){ ?>
                                                            
                                                            <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" checked disabled>

                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" disabled>

                                                        <?php } ?>

                                                        <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                                                    </div>

                                                </div>

                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">

                                                        <?php if($row['kosher'] == "yes"){ ?>

                                                            <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" checked disabled>

                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">

                                                        <?php if($row['lactose'] == "yes"){ ?>

                                                            <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" checked disabled>

                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                                                    </div>

                                                </div>
                                                
                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">

                                                        <?php if($row['gluten'] == "yes"){ ?>

                                                            <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" checked disabled>

                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <?php if($row['pork'] == "yes"){ ?>
                                                            <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" checked disabled>
                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="pork">No Pork</label>
                                                    </div>

                                                </div>
                                                
                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox-none custom-checkbox">
                                                        <?php if($row['none'] == "yes"){ ?>
                                                            <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" checked disabled>
                                                        <?php }else{ ?>

                                                            <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" disabled>

                                                        <?php } ?>
                                                        <label class="custom-control-label" for="none">None</label>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    <?php } ?>


                                </div>

                            </div>

                        </div>

                    <?php } ?>

                </div>
            </div><br>

            

            
        </form>
    </main>

</div>

<!--FOOTER ===============================================================-->
<?php 
    include '../master/footer.php';
?>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>

