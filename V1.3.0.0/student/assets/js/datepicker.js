// Modal Reserve 1

    // ticket details 

    if($('#fd_date1, #fa_date1').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date1, #fa_date1').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date1');
        $checkoutInput = $('#fa_date1');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#fd_date2, #fa_date2').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date2, #fa_date2').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date2');
        $checkoutInput = $('#fa_date2');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date3, #fa_date3').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date3, #fa_date3').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date3');
        $checkoutInput = $('#fa_date3');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date4, #fa_date4').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date4, #fa_date4').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date4');
        $checkoutInput = $('#fa_date4');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date5, #fa_date5').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date5, #fa_date5').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date5');
        $checkoutInput = $('#fa_date5');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date6, #fa_date6').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date6, #fa_date6').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date6');
        $checkoutInput = $('#fa_date6');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date7, #fa_date7').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date7, #fa_date7').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date7');
        $checkoutInput = $('#fa_date7');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }

    if($('#fd_date8, #fa_date8').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD HH:mm");

        $('#fd_date8, #fa_date8').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD hh:mm'
            },
            "alwaysShowCalendars": true,     
            autoApply: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD HH:mm'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD HH:mm'); // selected end

        $checkinInput = $('#fd_date8');
        $checkoutInput = $('#fa_date8');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });
    }






















    // Reserve Details

    if($('#search_checkin1, #search_checkout1').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin1, #search_checkout1').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin1');
        $checkoutInput = $('#search_checkout1');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin2, #search_checkout2').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin2, #search_checkout2').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin2');
        $checkoutInput = $('#search_checkout2');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin3, #search_checkout3').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin3, #search_checkout3').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin3');
        $checkoutInput = $('#search_checkout3');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin4, #search_checkout4').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin4, #search_checkout4').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin4');
        $checkoutInput = $('#search_checkout4');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }
    
    if($('#search_checkin5, #search_checkout5').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin5, #search_checkout5').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin5');
        $checkoutInput = $('#search_checkout5');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin6, #search_checkout6').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin6, #search_checkout6').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin6');
        $checkoutInput = $('#search_checkout6');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin7, #search_checkout7').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin7, #search_checkout7').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin7');
        $checkoutInput = $('#search_checkout7');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }

    if($('#search_checkin8, #search_checkout8').length){
        // check if element is available to bind ITS ONLY ON HOMEPAGE
        var currentDate = moment().format("YYYY-MM-DD");

        var firstd = $('#firstd').val();
        var lastd = $('#lastd').val();

        $('#search_checkin8, #search_checkout8').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            "alwaysShowCalendars": true,
            startDate: firstd, 
            endDate: lastd,      
            autoApply: true,
            autoUpdateInput: false
        }, function(start, end, label) {
        // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        // Lets update the fields manually this event fires on selection of range
        var selectedStartDate = start.format('YYYY-MM-DD'); // selected start
        var selectedEndDate = end.format('YYYY-MM-DD'); // selected end

        $checkinInput = $('#search_checkin8');
        $checkoutInput = $('#search_checkout8');

        // Updating Fields with selected dates
        $checkinInput.val(selectedStartDate);
        $checkoutInput.val(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
        var checkOutPicker = $checkoutInput.data('daterangepicker');
        checkOutPicker.setStartDate(selectedStartDate);
        checkOutPicker.setEndDate(selectedEndDate);

        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
        var checkInPicker = $checkinInput.data('daterangepicker');
        checkInPicker.setStartDate(selectedStartDate);
        checkInPicker.setEndDate(selectedEndDate);

        });

    }
