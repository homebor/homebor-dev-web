// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? LOADING HOUSES SVG
const loadingHouses = D.getElementById("loading-houses");

// TODO FUNCIONES
class HousesAssigned {
  constructor() {
    // ? SHOW OR HIDE LOADING SVG
    this.loading = (show) => (show ? this.showElement(true, loadingHouses) : this.showElement(false, loadingHouses));

    // ? SHOW OR HIDE ELEMENT
    this.showElement = (show, elem) => {
      show ? elem.classList.replace("d-none", "d-flex") : elem.classList.replace("d-flex", "d-none");
    };
  }

  async getHouseAssigned() {
    try {
      const getAllHouses = await axios("../helpers/students/get_house_assigned.php");
      const houseData = await getAllHouses.data;

      await this.renderHouse(houseData);
    } catch (error) {
      D.querySelector(".houses-container").innerHTML = `<h4 class="alert text-center">${error}</h4>`;
    }
  }

  async renderHouse(house) {
    return new Promise((resolve, reject) => {
      D.querySelector(".btn-more-info").href = `detail?art_id=${house.id_home}`;
      D.querySelector("[data-id-home]").href = `detail?art_id=${house.id_home}`;
      D.querySelector("[data-house-image]").src = "../" + house.phome;
      D.querySelector("[data-house-name]").textContent = `${house.l_name_h.toUpperCase()}, ${house.name_h}`;
      D.querySelector("[data-house-direction]").textContent = `${house.dir} ${house.city} ${house.state}`;
      D.querySelector("[data-house-gender-preference]").textContent = house.g_pre;
      D.querySelector("[data-house-age-preference]").textContent = house.a_pre;
      D.querySelector("[data-house-rooms]").textContent = house.room;

      if (house.backl === "NULL") house.backl = "Empty";
      D.querySelector("[data-house-background-language]").textContent = house.backl;

      D.querySelector("#house-email").textContent = house.mail_h;
      D.querySelector("#house-phone").textContent = house.num;
      D.querySelector("#house-postal-code").textContent = house.p_code;
      D.querySelector("#house-family-members").textContent = house.num_mem;
      D.querySelector("#house-nationality").textContent = house.backg;

      if (house.smoke === "NULL") house.smoke = "Strictly Non-Smoking";
      D.querySelector("#house-smoke").textContent = house.smoke;

      if (house.m_service.toLowerCase() !== "yes") house.m_service = "No";
      D.querySelector("#house-food").textContent = house.m_service;

      let $diet = "";
      if (house.vegetarians.toLowerCase() === "yes") $diet += "<span>Vegetarians</span>";
      if (house.halal.toLowerCase() === "yes") $diet += "<span>Halal</span>";
      if (house.kosher.toLowerCase() === "yes") $diet += "<span>Kosher</span>";
      if (house.lactose.toLowerCase() === "yes") $diet += "<span>Lactose</span>";
      if (house.gluten.toLowerCase() === "yes") $diet += "<span>Gluten</span>";
      if (house.pork.toLowerCase() === "yes") $diet += "<span>Pork</span>";
      $diet ? (D.querySelector("#house-diet").innerHTML = $diet) : (D.querySelector("#house-diet").textContent = "No");
      D.querySelector("#house-pets").textContent = house.pet;

      if (house.h_type === "NULL") house.h_type = "Empty";
      D.querySelector("#house-residence-type").textContent = house.h_type;
      D.querySelector("#house-description").textContent = house.des;

      resolve(true);
    });
  }
}
// TODO INSTANCIAS
const INSTANCE = new HousesAssigned();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  INSTANCE.loading(true);
  await INSTANCE.getHouseAssigned();
  INSTANCE.loading(false);

  _Messages.quitMessage();
});

// ! Click

// ! Error
