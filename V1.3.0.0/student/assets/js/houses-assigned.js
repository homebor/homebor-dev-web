// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? LOADING HOUSES SVG
const loadingHouses = D.getElementById("loading-houses");
// ? HOUSES CONTAINER
const allHousesContainer = D.getElementById("all-houses");
// ? TEMPLATES
const houseTemplate = D.getElementById("house-card-template").content;

// TODO FUNCIONES
class HousesAssigned {
  constructor() {
    // ? SHOW OR HIDE LOADING SVG
    this.loading = (show) => (show ? this.showElement(true, loadingHouses) : this.showElement(false, loadingHouses));

    // ? SHOW OR HIDE ELEMENT
    this.showElement = (show, elem) => {
      show ? elem.classList.replace("d-none", "d-flex") : elem.classList.replace("d-flex", "d-none");
    };
  }

  getHousesAssigned() {
    return new Promise(async (resolve, reject) => {
      const allHouses = [];
      try {
        const getAllHouses = await axios("../helpers/students/get_houses_assigned.php");
        const housesData = await getAllHouses.data;
        // ? VALIDATION USER
        if (housesData && housesData instanceof Array) W.location.href = housesData[1];
        // ? VALIDATION HOUSES
        if (
          housesData["house1"] instanceof Object === false &&
          housesData["house2"] instanceof Object === false &&
          housesData["house3"] instanceof Object === false &&
          housesData["house4"] instanceof Object === false &&
          housesData["house5"] instanceof Object === false
        ) {
          W.location.href = "student_info";
        }

        Object.values(housesData).forEach((house) => (house instanceof Object ? allHouses.push(house) : {}));
      } catch (error) {
        allHousesContainer.innerHTML = `<h4 class="alert text-center">${error}</h4>`;
      }

      resolve(allHouses);
    });
  }

  async renderHousesAssigned(houses) {
    return new Promise((resolve, reject) => {
      const $fragment = D.createDocumentFragment();
      houses.forEach((house) => {
        const $newHouse = D.importNode(houseTemplate, true);
        $newHouse.querySelector("[data-id-home]").href = `detail?art_id=${house.id_home}`;
        $newHouse.querySelector("[data-house-image]").src = "../" + house.phome;
        $newHouse.querySelector("[data-house-name]").textContent = `${house.l_name_h.toUpperCase()}, ${house.name_h}`;
        $newHouse.querySelector("[data-house-direction]").textContent = `${house.dir} ${house.city} ${house.state}`;
        $newHouse.querySelector("[data-house-gender-preference]").textContent = house.g_pre;
        $newHouse.querySelector("[data-house-age-preference]").textContent = house.a_pre;
        $newHouse.querySelector("[data-house-rooms]").textContent = house.room;
        if (house.backl === "NULL") house.backl = "Empty";
        $newHouse.querySelector("[data-house-background-language]").textContent = house.backl;

        $fragment.appendChild($newHouse);
      });
      allHousesContainer.appendChild($fragment);
      resolve(true);
    });
  }
}
// TODO INSTANCIAS
const INSTANCE = new HousesAssigned();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  INSTANCE.loading(true);
  const allHouses = await INSTANCE.getHousesAssigned();
  await INSTANCE.renderHousesAssigned(allHouses);
  INSTANCE.loading(false);
  _Messages.quitMessage();
});

// ! Click

// ! Error
