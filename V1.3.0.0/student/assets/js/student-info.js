// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? ELEMENTS
const $accommodation = D.querySelector(".price__accommodation").textContent;
const $nearToSchool = D.querySelector("#accomm-near").value;
const $accomUrgent = D.querySelector("#accomm-urgent").value;
const $summerFee = D.querySelector("#summer-fee").value;
const $minorFee = D.querySelector("#minor-fee").value;
/* const $accomGuardianship = D.querySelector("#accomm-guardianship").value; */
const $bookingFee = D.querySelector(".price__b-fee").textContent;

let totalPrice = 0;
let priceNearSchool = 0;
let priceAccomUrgent = 0;
let priceSummerFee = 0;
let priceMinorFee = 0;
let priceAccomGuardianship = 0;

// TODO NEAR TO SCHOOL
if ($nearToSchool === "Yes") {
  priceNearSchool = 35 * 5 * 4;
  D.querySelector("#container-price-a-near").classList.remove("d-none");
} else {
  D.querySelector("#container-price-a-near").classList.add("d-none");
  priceNearSchool = 0;
}

// TODO ACCOMMODATION URGENT
if ($accomUrgent === "Yes") {
  priceAccomUrgent = 150;
  D.querySelector("#container-price-a-urgent").classList.remove("d-none");
} else {
  priceAccomUrgent = 0;
  D.querySelector("#container-price-a-urgent").classList.add("d-none");
}

// TODO SUMMER FEE
if ($summerFee === "Yes") {
  priceSummerFee = 45 * 4;
  D.querySelector("#container-price-summer-fee").classList.remove("d-none");
} else {
  priceSummerFee = 0;
  D.querySelector("#container-price-summer-fee").classList.add("d-none");
}

// TODO MINOR FEE
if ($minorFee === "Yes") {
  priceMinorFee = 75 * 4;
  D.querySelector("#container-price-minor-fee").classList.remove("d-none");
} else {
  priceMinorFee = 0;
  D.querySelector("#container-price-minor-fee").classList.add("d-none");
}

// TODO GUARDIANSHIP
/* if ($accomGuardianship === "Yes") {
  priceAccomGuardianship = 550;
  D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
} else {
  priceAccomGuardianship = 0;
  D.querySelector("#container-price-a-guardianship").classList.add("d-none");
} */

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", (e) => {
  // TODO NEAR TO SCHOOL
  if ($nearToSchool === "Yes") {
    priceNearSchool = 35 * 5 * 4;
    D.querySelector("#container-price-a-near").classList.remove("d-none");
  } else {
    D.querySelector("#container-price-a-near").classList.add("d-none");
    priceNearSchool = 0;
  }

  // TODO ACCOMMODATION URGENT
  if ($accomUrgent === "Yes") {
    priceAccomUrgent = 150;
    D.querySelector("#container-price-a-urgent").classList.remove("d-none");
  } else {
    priceAccomUrgent = 0;
    D.querySelector("#container-price-a-urgent").classList.add("d-none");
  }

  // TODO SUMMER FEE
  if ($summerFee === "Yes") {
    priceSummerFee = 45 * 4;
    D.querySelector("#container-price-summer-fee").classList.remove("d-none");
  } else {
    priceSummerFee = 0;
    D.querySelector("#container-price-summer-fee").classList.add("d-none");
  }

  // TODO MINOR FEE
  if ($minorFee === "Yes") {
    priceMinorFee = 75 * 4;
    D.querySelector("#container-price-minor-fee").classList.remove("d-none");
  } else {
    priceMinorFee = 0;
    D.querySelector("#container-price-minor-fee").classList.add("d-none");
  }

  // TODO GUARDIANSHIP
  /* if ($accomGuardianship === "Yes") {
    priceAccomGuardianship = 550;
    D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
  } else {
    priceAccomGuardianship = 0;
    D.querySelector("#container-price-a-guardianship").classList.add("d-none");
  } */

  let totalPrice =
    parseFloat($accommodation) +
    parseFloat($bookingFee) +
    priceNearSchool +
    priceAccomUrgent +
    priceSummerFee +
    priceMinorFee;

  D.querySelector("#total__price").value = totalPrice;

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  if (e.target.matches("#btn_invoice")) D.querySelector("#table_invoice").classList.toggle("d-none");
});
