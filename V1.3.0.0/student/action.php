<?php 

include '../xeon.php';
include '../cript.php';

session_start();

$usuario = $_SESSION['username'];

//TODO DATE TIME
date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');
$dateImage = date('YmdHisv');

// TODO VARIABLE STEP
$value_step = $_POST["value_step"];

$paths = '../public_s/' . $usuario;

//TODO VARIABLE JSON
$jsonResponse = null;

//TODO QUERY

$studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
$row_student = $studentQuery->fetch_assoc();

//TODO STEP 1

if($value_step == "next_step-1"){

  //TODO ASSIGN VALUES TO VARIABLE 

  //* BASIC INFORMATION

    if(!empty($_FILES['profile_s']['name'])){

      if(empty($row_student["photo_s"]) || $row_student["photo_s"] == 'NULL') {}else{
        unlink("../".$row_student["photo_s"]);
      }

      $profile_tmp = $_FILES['profile_s']['tmp_name'];
      $profileType = stristr($_FILES['profile_s']['type'], "/");
      $profileExtension = str_replace( "/", ".", $profileType);
      $s_profileUrl = "public_s/".$usuario."/profilePhoto" . $dateImage . $profileExtension;
      $s_profile = "profilePhoto" . $dateImage . $profileExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($profile_tmp, $paths . '/' . $s_profile);
        
    } else $s_profileUrl = 'NULL';

    if(empty($_POST["name"])) $s_name = 'NULL';
    else $s_name = addslashes($_POST["name"]);
    
    if(empty($_POST["l_name"])) $s_lname = 'NULL';
    else $s_lname = addslashes($_POST["l_name"]);
    
    if(empty($_POST['mail_s'])) $s_mail = 'NULL';
    else $s_mail = $_POST['mail_s'];

    
    if(empty($_POST['arrive_g'])) $s_arrive = 'NULL';
    else {
      $s_arrive = date("Y-m-d", strtotime($_POST['arrive_g']));
    }
    
    if(empty($_POST['destination_c'])) $s_destinationC = 'NULL';
    else $s_destinationC = $_POST['destination_c'];
  
  //* ADDRESS
  
    if(empty($_POST["country"])) $s_country = "NULL";
    else $s_country = addslashes($_POST["country"]);
    
    if(empty($_POST["dir"])) $s_dir = 'NULL';
    else $s_dir = addslashes($_POST["dir"]);

    if(empty($_POST["city"])) $s_city = 'NULL';
    else $s_city = addslashes($_POST["city"]);

    if(empty($_POST["state"])) $s_state = 'NULL';
    else $s_state = addslashes($_POST["state"]);
    
    if(empty($_POST["p_code"])) $s_codeP = 'NULL';
    else $s_codeP = addslashes($_POST["p_code"]);
  
  //* PROFESSIONAL INFORMATION
  
    if(empty($_POST['n_a'])) $s_academyP = 'NULL';
    else $s_academyP = $_POST['n_a'];
    
    if(empty($_POST['name_a'])) $s_name_a = 'NULL';
    else $s_name_a = $_POST['name_a'];
    
    if(empty($_POST['english_l'])) $s_englishL = 'NULL';
    else $s_englishL = $_POST['english_l'];
    
    if(empty($_POST['type_s'])) $s_typeS = 'NULL';
    else $s_typeS = $_POST['type_s'];
    
    if(empty($_POST['prog_selec'])) $s_programS = 'NULL';
    else $s_programS = addslashes($_POST["prog_selec"]);
    
    if(empty($_POST['schedule'])) $s_schedule = 'NULL';
    else $s_schedule = $_POST['schedule'];
    
  //* ACCOMMODATION REQUEST
  
    if(empty($_POST['firstd'])) $s_firstd = 'NULL';
    else $s_firstd = date("Y-m-d", strtotime($_POST['firstd']));
    
    if(empty($_POST['lastd'])) $s_lastd = 'NULL';
    else $s_lastd = date("Y-m-d", strtotime($_POST['lastd']));

  //* SAVE INFORMATION STEP

    $saveStep1 = $link->query("UPDATE pe_student SET photo_s = '$s_profileUrl', name_s = '$s_name', l_name_s = '$s_lname', arrive_g = '$s_arrive', destination_c = '$s_destinationC', country = '$s_country', dir_s = '$s_dir', city_s= '$s_city', state_s = '$s_state', p_code_s = '$s_codeP', n_a = '$s_academyP', english_l = '$s_englishL', type_s = '$s_typeS', prog_selec = '$s_programS', schedule = '$s_schedule', firstd = '$s_firstd', lastd = '$s_lastd' WHERE mail_s = '$usuario'");

  if($saveStep1) $response = "registered";
  else $response = "error";

  $jsonResponse = array(
    'response' => $response
  );
  
  echo json_encode($jsonResponse); 
}

//TODO STEP 2

else if ($value_step == "next_step-2"){
  //* PERSONAL INFORMATION
  
    if(empty($_POST['db'])) $s_db = 'NULL';
    else {
      $s_db = date("Y-m-d", strtotime($_POST['db']));
    }
    
    if(empty($_POST['gender'])) $s_gender = 'NULL';
    else $s_gender = $_POST['gender'];

    if(empty($_POST['num_s'])) $s_num = 'NULL';
    else $s_num = addslashes($_POST["num_s"]);

    if(empty($_POST['nationality'])) $s_nationality = 'NULL';
    else $s_nationality = addslashes($_POST["nationality"]);

    if(empty($_POST['lang_s'])) $s_lang = 'NULL';
    else $s_lang = addslashes($_POST['lang_s']);
    
    if(empty($_POST['language_a'])) $s_langAnother = 'NULL';
    else $s_langAnother = addslashes($_POST['language_a']);
    
  //* FLIGHT INFORMATION
    
    if(empty($_POST['n_airline'])) $s_airlineName = 'NULL';
    else $s_airlineName = addslashes($_POST["n_airline"]);

    if(empty($_POST['n_flight'])) $s_flightNumber = 'NULL';
    else $s_flightNumber = addslashes($_POST["n_flight"]);

    if(empty($_POST['f_date'])) $s_flightDate = 'NULL';
    else $s_flightDate = date("Y-m-d h:i A", strtotime($_POST['f_date']));
    
    if(empty($_POST['h_date'])) $s_arrivalHomestay = 'NULL';
    else $s_arrivalHomestay = date("Y-m-d", strtotime($_POST['h_date']));
    
  //* EMERGENCY CONTACT INFORMTATION
    
    if(empty($_POST['cont_name'])) $s_contactName = 'NULL';
    else $s_contactName = addslashes($_POST["cont_name"]);

    if(empty($_POST['cont_lname'])) $s_contactLastName = 'NULL';
    else $s_contactLastName = addslashes($_POST["cont_lname"]);
    
    if(empty($_POST['num_conts'])) $s_numEmergency = 'NULL';
    else $s_numEmergency = addslashes($_POST['num_conts']);
    
    if(empty($_POST['relationship'])) $s_relationship = 'NULL';
    else $s_relationship = addslashes($_POST['relationship']);
    
    
  //* SAVE STEP
    $saveStep1 = $link->query("UPDATE pe_student SET db_s = '$s_db', gen_s = '$s_gender', num_s = '$s_num', nationality = '$s_nationality', lang_s = '$s_lang', language_a = '$s_langAnother', n_airline = '$s_airlineName', n_flight= '$s_flightNumber', departure_f = '$s_flightDate', arrive_f = '$s_arrivalHomestay', cont_name = '$s_contactName', cont_lname = '$s_contactLastName', num_conts = '$s_numEmergency', relationship = '$s_relationship' WHERE mail_s = '$usuario'");
    
    if($saveStep1) $response = "registered";
    else $response = "error";
    
    $jsonResponse = array(
      'response' => $response
    );

  echo json_encode($jsonResponse);
}

//TODO STEP 3

else if ($value_step == "next_step-3"){

  $saveStep3 = $link->query("UPDATE pe_student SET booking_fee = '250' WHERE mail_s = '$usuario'");

  if($saveStep3) $response = "registered";
  else $response = "error";
    
    $jsonResponse = array(
      'response' => $response,
    );

  echo json_encode($jsonResponse);

} 

//TODO STEP 4

else if ($value_step == "next_step-4"){

  //* PERSONAL INFORMATION

    if(empty($_POST['about_me'])) $s_aboutMe = 'NULL';
    else $s_aboutMe = addslashes($_POST["about_me"]);

    if(empty($_POST['pass'])) $s_passport = 'NULL';
    else $s_passport = addslashes($_POST["pass"]);

    if(empty($_POST['exp_pass'])) $s_passportDate = 'NULL';
    else $s_passportDate = date("Y-m-d", strtotime($_POST['exp_pass']));
    
    if(empty($_POST['bl'])) $s_visaExpiration = 'NULL';
    else $s_visaExpiration = date("Y-m-d", strtotime($_POST['bl']));

  //* HOUSE PREFERENCE

    //? CAN YOU SHARE WITH:
  
      if(empty($_POST['smoker_l'])) $s_canSmokers = 'NULL';
      else $s_canSmokers = $_POST['smoker_l'];
      
      if(empty($_POST['children'])) $s_canChildren = 'NULL';
      else $s_canChildren = $_POST['children'];

      if(empty($_POST['teenagers'])) $s_canTeenagers = 'NULL';
      else $s_canTeenagers = $_POST['teenagers'];
    
      if(empty($_POST['pets'])) $s_canPets = 'NULL';
      else $s_canPets = $_POST['pets'];
      
    //? ACCOMMODATION

      if(empty($_POST['lodging_type'])) $s_lodgingType = 'NULL';
      else $s_lodgingType = $_POST['lodging_type'];

      if(empty($_POST['meal_p'])) $s_mealPlan = 'NULL';
      else $s_mealPlan = $_POST['meal_p'];
      
      if(empty($_POST['food'])) $s_food = 'NULL';
      else $s_food = $_POST['food'];
      
      if (empty($_POST['diet'])) {
        $diet =  'NULL';
        $s_vegetarians = 'no';
        $s_halal =  'no';
        $s_kosher =  'no';
        $s_lactose =  'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'yes';
      }elseif($_POST['diet'] == 'Vegetarians'){
        $s_vegetarians = 'yes';
        $s_halal =  'no';
        $s_kosher =  'no';
        $s_lactose =  'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'no';
      }elseif($_POST['diet'] == 'Halal'){
        $s_halal =  'yes';
        $s_vegetarians = 'no';
        $s_kosher =  'no';
        $s_lactose =  'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'no';
      }elseif($_POST['diet'] == 'Kosher'){
        $s_kosher =  'yes';
        $s_halal =  'no';
        $s_vegetarians = 'no';
        $s_lactose =  'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'no';
      }elseif($_POST['diet'] == 'Lactose'){
        $s_lactose =  'yes';
        $s_kosher =  'no';
        $s_halal =  'no';
        $s_vegetarians = 'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'no';
      }elseif($_POST['diet'] == 'Gluten'){
        $s_gluten =  'yes';
        $s_lactose =  'no';
        $s_kosher =  'no';
        $s_halal =  'no';
        $s_vegetarians = 'no';
        $s_pork =  'no';
        $s_none =  'no';
      }elseif($_POST['diet'] == 'Pork'){
        $pork =  'yes';
        $gluten =  'no';
        $lactose =  'no';
        $kosher =  'no';
        $halal =  'no';
        $vegetarians = 'no';
        $none =  'no';
      }elseif($_POST['diet'] == 'None'){
        $s_none =  'yes';
        $s_pork =  'no';
        $s_gluten =  'no';
        $s_lactose =  'no';
        $s_kosher =  'no';
        $s_halal =  'no';
        $s_vegetarians = 'no';
      }else{
        $s_vegetarians = 'no';
        $s_halal =  'no';
        $s_kosher =  'no';
        $s_lactose =  'no';
        $s_gluten =  'no';
        $s_pork =  'no';
        $s_none =  'yes';
      }

      if(empty($_POST['a_price'])) $s_accommodationPrice = '0';
      else $s_accommodationPrice = $_POST['a_price'];

    //? TODO SUPPLEMENTS
        
      if(empty($_POST['accomm-near'])) $s_accommNear = 'no';
      else $s_accommNear = $_POST['accomm-near'];
        
      if(empty($_POST['accomm-urgent'])) $s_accommUrgent = 'no';
      else $s_accommUrgent = $_POST['accomm-urgent'];

      if(empty($_POST['summer-fee'])) $s_summerFee = 'no';
      else $s_summerFee = $_POST['summer-fee'];

      if(empty($_POST['minor-fee'])) $s_minorFee = 'no';
      else $s_minorFee = $_POST['minor-fee'];

      /* if(empty($_POST['accomm-guardianship'])) $s_accommGuard = 'no';
      else $s_accommGuard = $_POST['accomm-guardianship']; */


    //? TRANSPORT
    
      if(empty($_POST['pick_up'])) $s_pickUP = 'no';
      else $s_pickUP = $_POST['pick_up'];

      if(empty($_POST['drop_off'])) $s_dropOff = 'no';
      else $s_dropOff = $_POST['drop_off'];

      if(empty($_POST['t_price'])) $s_transportPrice = '0';
      else $s_transportPrice = $_POST['t_price'];
    
  //* HEALTH INFORMATION

    if(empty($_POST['smoke_s'])) $s_smoke = 'NULL';
    else $s_smoke = $_POST["smoke_s"];

    if(empty($_POST['drinks_alc'])) $s_drinkAlcochol = 'NULL';
    else $s_drinkAlcochol = $_POST["drinks_alc"];

    if(empty($_POST['drugs'])) $s_drugs = 'NULL';
    else $s_drugs = $_POST["drugs"];

    if(empty($_POST['allergy_a'])) $s_allergyAnimals = 'no';
    else $s_allergyAnimals = $_POST["allergy_a"];

    if(empty($_POST['allergy_m'])) $s_allergyMeals = 'no';
    else $s_allergyMeals = $_POST["allergy_m"];

    if(empty($_POST['healt_s'])) $s_healthState = 'NULL';
    else $s_healthState = addslashes($_POST["healt_s"]);

    if(empty($_POST['disease'])) $s_disease = 'no';
    else $s_disease = addslashes($_POST["disease"]);

    if(empty($_POST['treatment'])) $s_treatment = 'no';
    else $s_treatment = addslashes($_POST["treatment"]);

    if(empty($_POST['treatment_p'])) $s_treatmentP = 'no';
    else $s_treatmentP = addslashes($_POST["treatment_p"]);

    if(empty($_POST['allergies'])) $s_allergies = 'no';
    else $s_allergies = addslashes($_POST["allergies"]);

    if(empty($_POST['surgery'])) $s_surgery = 'no';
    else $s_surgery = addslashes($_POST["surgery"]);

  //* ATTACHED FILES

    if(!empty($_FILES['passport']['name'])){

      if(empty($row_student["pass_photo"]) || $row_student["pass_photo"] == 'NULL') {}else{
        unlink("../".$row_student["pass_photo"]);
      }

      $passportImage_tmp = $_FILES['passport']['tmp_name'];
      $passportImageType = stristr($_FILES['passport']['type'], "/");
      $passportImageExtension = str_replace( "/", ".", $passportImageType);
      $s_passportImageUrl = "public_s/".$usuario."/passportPhoto" . $dateImage . $passportImageExtension;
      $s_passportImage = "passportPhoto" . $dateImage . $passportImageExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($passportImage_tmp, $paths . '/' . $s_passportImage);
        
    } else $s_passportImageUrl = 'NULL';
    
    if(!empty($_FILES['visa']['name'])){

      if(empty($row_student["visa"]) || $row_student["visa"] == 'NULL') {}else{
        unlink("../".$row_student["visa"]); 
      }

      $visaImage_tmp = $_FILES['visa']['tmp_name'];
      $visaImageType = stristr($_FILES['visa']['type'], "/");
      $visaImageExtension = str_replace( "/", ".", $visaImageType);
      $s_visaImageUrl = "public_s/".$usuario."/visaPhoto" . $dateImage . $visaImageExtension;
      $s_visaImage = "visaPhoto" . $dateImage . $visaImageExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($visaImage_tmp, $paths . '/' . $s_visaImage);
        
    } else $s_visaImageUrl = 'NULL';

    if(!empty($_FILES['flight-image']['name'])){

      if(empty($row_student["flight_image"]) || $row_student["flight_image"] == 'NULL') {}else{
        unlink("../".$row_student["flight_image"]); 
      }

      $flightImage_tmp = $_FILES['flight-image']['tmp_name'];
      $flightImageType = stristr($_FILES['flight-image']['type'], "/");
      $flightImageExtension = str_replace( "/", ".", $flightImageType);
      $s_flightImageUrl = "public_s/".$usuario."/flightImage" . $dateImage . $flightImageExtension;
      $s_flightImage = "flightImage" . $dateImage . $flightImageExtension;

      if (!file_exists($paths)) mkdir($paths, 0777);
      move_uploaded_file($flightImage_tmp, $paths . '/' . $s_flightImage);

    } else $s_flightImageUrl = 'NULL';

    


  //* SAVE STEP
    $saveStep4 = $link->query("UPDATE pe_student SET smoker_l = '$s_canSmokers', children = '$s_canChildren', teenagers = '$s_canTeenagers', pets = '$s_canPets', lodging_type = '$s_lodgingType', meal_p = '$s_mealPlan', food = '$s_food', vegetarians = '$s_vegetarians', halal= '$s_halal', kosher = '$s_kosher', lactose = '$s_lactose', gluten = '$s_gluten', pork = '$s_pork', none = '$s_none', a_price = '$s_accommodationPrice', pick_up = '$s_pickUP', drop_off = '$s_dropOff', t_price = '$s_transportPrice', about_me = '$s_aboutMe', passport = '$s_passport', exp_pass = '$s_passportDate', db_visa = '$s_visaExpiration', smoke_s = '$s_smoke', drinks_alc = '$s_drinkAlcochol', drugs = '$s_drugs', allergy_a= '$s_allergyAnimals', allergy_m = '$s_allergyMeals', healt_s = '$s_healthState', disease = '$s_disease', treatment = '$s_treatment', treatment_p = '$s_treatmentP', allergies = '$s_allergies', surgery = '$s_surgery', pass_photo = '$s_passportImageUrl', visa = '$s_visaImageUrl', flight_image = '$s_flightImageUrl', a_near = '$s_accommNear', a_urgent = '$s_accommUrgent', summer_fee = '$s_summerFee', minor_fee = '$s_minorFee' WHERE mail_s = '$usuario'");

  if($saveStep4) $response = "registered";
    else $response = "error";
    
    $jsonResponse = array(
      'response' => $response,
    );

  echo json_encode($jsonResponse);


}

else if ($value_step == "next_step-5"){

  if(!empty($_POST["signature-image"])){
      
    if (!file_exists($paths)) mkdir($paths, 0777);
    
    if(empty($row_student["signature_s"]) || $row_student["signature_s"] == 'NULL') {}else{
      unlink("../".$row_student["signature_s"]); 
    }

    $signature = $_POST["signature-image"];

    $folderPath = "../public_s/".$usuario."/";
    $image_parts = explode(";base64", $signature);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);
    $file = $folderPath . "signature". $dateImage . ".png";
    file_put_contents($file, $image_base64);

    $signatureURL = "public_s/".$usuario."/signature". $dateImage . ".png";

  }else $signatureURL = 'NULL';

  //* SAVE STEP
    $saveStep5 = $link->query("UPDATE pe_student SET signature_s='$signatureURL' WHERE mail_s = '$usuario'");

    if($saveStep5) $response = "registered complete";
    else $response = "error";

    $jsonResponse = array(
      'response' => $response,
    );

    echo json_encode($jsonResponse);

}



?>