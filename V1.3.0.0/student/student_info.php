<?php
include_once('../xeon.php');
session_start();
$usuario = $_SESSION['username'];

$query = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario' ");
$row = $query->fetch_assoc();

$query2 = "SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a";
$resultado2 = $link->query($query2);

$row2 = $resultado2->fetch_assoc();


$studentd = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$sd = $studentd->fetch_assoc();

if ($sd['usert'] != 'student') header("location: ../logout.php");

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../assets/css/utility.css">
  <link rel="stylesheet" type="text/css" href="assets/css/student_info.css?ver=1.4">
  <link rel="stylesheet" type="text/css" href="assets/css/student_delete.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students Profile</title>


</head>

<!-- // TODO HEADER -->
<?php include 'header.php' ?>

<body>
  <div class="container_register">

    <main class="card__register">
      <?php if ($row['house1'] == 'NULL' && $row['house2'] == 'NULL' && $row['house3'] == 'NULL' && $row['house4'] == 'NULL' && $row['house5'] == 'NULL' && $row['house6'] == 'NULL' && $row['house7'] == 'NULL' && $row['house8'] == 'NULL') { ?>
      <h4 class="message__res">Wait for Homebor to assign you houses according to your registered information so you can
        reserve a room.</h1>

        <?php } else {
      } ?>
        <form class="ts-form" action="edit.php" method="POST" autocomplete="off" enctype="multipart/form-data">

          <div class="row m-0 p-0">

            <div class="col-md-4 column-1">

              <div class="card">

                <h3 class="form__group-title__col-4">Basic Information</h3>

                <div class="information__content">

                  <?php if ($row7['photo_s'] == 'NULL') {
                  } else { ?>

                  <div class="form__group__div__photo">

                    <img class="form__group__photo" id="preview" src="../<?php echo $row['photo_s'] ?>" alt="">

                  </div>

                  <?php } ?>

                  <!-- Group Name -->

                  <?php if (empty($row['name_s']) || $row['name_s'] == 'NULL') {
                  } else { ?>

                  <div class="form__group form__group__name" id="group__name">
                    <div class="form__group__icon-input">
                      <label for="name" class="form__label">First Name</label>

                      <label for="name" class="icon"><i class="icon icon__names"></i></label>
                      <p id="name" name="name" class="form__group-input names"><?php echo $row['name_s'] ?></p>

                    </div>

                  </div>

                  <?php } ?>


                  <!-- Group Last Name -->

                  <?php if (empty($row['l_name_s']) || $row['l_name_s'] == 'NULL') {
                  } else { ?>

                  <div class="form__group form__group__l_name" id="group__l_name">
                    <div class="form__group__icon-input">

                      <label for="l_name" class="form__label">Last Name</label>
                      <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                      <p id="l_name" name="l_name" class="form__group-input names"> <?php echo "$row[l_name_s]" ?> </p>

                    </div>
                  </div>

                  <?php } ?>



                  <?php if (empty($row['mail_s']) || $row['mail_s'] == 'NULL') {
                  } else { ?>


                  <!-- Group Mail -->

                  <div class="form__group form__group__mail_s" id="group__mail_s">
                    <div class="form__group__icon-input">

                      <label for="mail_s" class="form__label">Student Email</label>

                      <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>


                      <p id="mail_s" class="form__group-input mail"><?php echo $row['mail_s'] ?> </p>

                    </div>
                    <p class="form__group__input-error">It is not a valid Email</p>
                  </div>

                  <?php }

                  if (empty($row['arrive_g']) || $row['arrive_g'] == 'NULL') {
                  } else { ?>

                  <!-- Arrive Date -->
                  <div class="form__group form__group-arrive_g">

                    <div class="form__group__icon-input">

                      <label for="arrive_g" class="form__label">Arrive Date</label>
                      <label for="arrive_g" class="icon"><i class="icon icon__date"></i></label>

                      <?php $arrive_g = date_create($row['arrive_g']); ?>

                      <p id="arrive_g" name="arrive_g" class="form__group-input h_date">
                        <?php echo date_format($arrive_g, 'j, F Y'); ?></p>



                    </div>
                    <p class="form__group__input-error">Minimum 4 Characters</p>
                  </div>


                  <?php }

                  if (empty($row['destination_c']) || $row['destination_c'] == 'NULL') {
                  } else { ?>

                  <!-- Group Destination City  -->
                  <div class="form__group form__group__basic form__group__destination_c" id="group__destination_c">

                    <div class="form__group__icon-input">

                      <label for="destination_c" class="form__label"> Destination City </label>

                      <label for="destination_c" class="icon"><i class="icon icon__city"></i></label>

                      <p class=" form__group-input smoke_s" id="destination_c" name="destination_c">
                        <?php echo $row['destination_c'] ?> </p>


                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <?php } ?>


                  <div class="div__btn_edit">

                    <a href="edit_student" class="btn__edit">Edit Profile</a>

                  </div>

                </div>

              </div>

              <?php if ($row['country'] == 'NULL' && $row['dir_s'] == 'NULL' && $row['city_s'] == 'NULL' && $row['state_s'] == 'NULL' && $row['p_code_s'] == 'NULL') {
              } else { ?>

              <div class="card">

                <h3 class="form__group-title__col-4">Personal Student Address</h3>

                <div class="information__content">

                  <?php if (empty($row['country']) || $row['country'] == 'NULL') {
                    } else { ?>

                  <!-- Group Origin City -->
                  <div class="form__group form__group__basic form__group__country" id="group__country">
                    <div class="form__group__icon-input">

                      <label for="country" class="form__label">Country</label>

                      <label for="country" class="icon"><i class="icon icon__dir"></i></label>

                      <p id="country" name="country" class="form__group-input country"> <?php echo $row['country'] ?>
                      </p>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php }

                    if (empty($row['dir_s']) || $row['dir_s'] == 'NULL') {
                    } else { ?>


                  <!-- Group Address -->
                  <div class="form__group form__group__dir" id="group__dir">
                    <div class="form__group__icon-input">

                      <label for="dir" class="form__label">Address </label>

                      <label for="dir" class="icon"><i class="icon icon__dir"></i></label>

                      <p id="dir" class="form__group-input dir"> <?php echo $row['dir_s'] ?> </p>

                    </div>

                    <p class="form__group__input-error">Please enter a valid Address</p>
                  </div>

                  <?php }

                    if (empty($row['city_s']) || $row['city_s'] == 'NULL') {
                    } else { ?>


                  <!-- Group City -->
                  <div class="form__group form__group__city" id="group__city">
                    <div class="form__group__icon-input">

                      <label for="city" class="form__label">City </label>

                      <label for="city" class="icon"><i class="icon icon__dir"></i></label>

                      <p id="city" class="form__group-input city2"> <?php echo $row['city_s'] ?> </p>


                    </div>

                    <p class="form__group__input-error">Please enter a valid City</p>
                  </div>


                  <?php }

                    if (empty($row['state_s']) || $row['state_s'] == 'NULL') {
                    } else { ?>

                  <!-- Group State -->
                  <div class="form__group form__group__state" id="group__state">
                    <div class="form__group__icon-input">

                      <label for="state" class="form__label"> Province / State </label>

                      <label for="state" class="icon"><i class="icon icon__dir"></i></label>

                      <p id="state" class="form__group-input state"> <?php echo $row['state_s'] ?> </p>

                    </div>

                    <p class="form__group__input-error">Please enter a valid State</p>
                  </div>

                  <?php }


                    if (empty($row['p_code_s']) || $row['p_code_s'] == 'NULL') {
                    } else { ?>

                  <!-- Group Postal Code -->
                  <div class="form__group form__group__p_code" id="group__p_code">
                    <div class="form__group__icon-input">

                      <label for="p_code" class="form__label">Postal Code </label>

                      <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>


                      <p type="text" id="p_code" name="p_code" class="form__group-input p_code">
                        <?php echo $row['p_code_s'] ?> </p>

                    </div>

                    <p class="form__group__input-error">Please enter a valid Postal Code</p>
                  </div>

                  <?php } ?>

                </div>

              </div>

              <?php }

              if ($row['firstd'] == 'NULL' && $row['lastd'] == 'NULL') {
              } else {

              ?>


              <div class="card column3">


                <h3 class="form__group-title__col-4">Accommodation Request</h3>

                <div class="information__content">

                  <?php if (empty($row['firstd']) || $row['firstd'] == 'NULL') {
                    } else { ?>

                  <!-- Group Start Date of Stay -->
                  <div class="form__group form__group__firstd" id="group__firstd">
                    <div class="form__group__icon-input">

                      <label for="firstd" class="form__label">Start Date of Stay</label>
                      <label for="firstd" class="icon"><i class="icon icon__date"></i></label>

                      <?php $firstd = date_create($row['firstd']); ?>

                      <p id="firstd" class="form__group-input firstd"> <?php echo date_format($firstd, 'j, F Y'); ?>
                      </p>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php }


                    if (empty($row['lastd']) || $row['lastd'] == 'NULL') {
                    } else { ?>


                  <!-- Group End Date of stay -->
                  <div class="form__group form__group__lastd" id="group__lastd">
                    <div class="form__group__icon-input">

                      <label for="lastd" class="form__label">End Date of Stay</label>
                      <label for="lastd" class="icon"><i class="icon icon__date"></i></label>

                      <?php $lastd = date_create($row['lastd']);

                          $datetime1 = new DateTime($row["firstd"]);
                          $datetime2 = new DateTime($row["lastd"]);
                          $interval = $datetime1->diff($datetime2);

                          if ($interval->format("%a") % 7 == '0') {

                            $w_date = floor(($interval->format("%a") / 7)) . " weeks";
                          } else if (floor(($interval->format("%a") / 7)) / 7 == '0') {

                            $w_date = floor(($interval->format("%a") % 7));
                          } else {

                            $w_date = floor(($interval->format("%a") / 7)) . " weeks and " . ($interval->format("%a") % 7) . " days";
                          }

                          ?>

                      <p id="lastd" class="form__group-input lastd"> <?php echo date_format($lastd, 'j, F Y'); ?> </p>


                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php } ?>

                  <p id="calcWeeksPhp" class="calc__weeks"><?php echo $w_date ?></p>


                  <hr>

                  <div class="form__group form__group-invoice text-center">
                    <button type="button" id="btn_invoice" class="btn">Invoice Details</button>

                    <table id="table_invoice" class="table table-bordered mt-3 d-none">

                      <thead class="t_head_pay">

                        <tr>
                          <td>Name</td>
                          <td class="price__invoice">Price</td>
                        </tr>

                      </thead>

                      <tbody>
                        <tr>
                          <th>Booking Fee</th>
                          <th class="price__invoice price__b-fee">250</th>
                        </tr>

                        <tr>
                          <th>Accommodation</th>
                          <th class="price__invoice price__accommodation">
                            <?php
                              if ($row['a_price'] == 0) $a_price = 0;
                              else $a_price = $row['a_price'];
                              echo $a_price ?>
                          </th>
                        </tr>

                        <?php if (!empty($row['a_near']) || $row['a_near'] != 'no') { ?>
                        <tr id="container-price-a-near">
                          <th>Accommodation Near to School</th>
                          <th class="price__invoice price__a-near" id="price__a-near"><?php echo 35 * 5 * 4 ?></th>
                        </tr>
                        <?php }

                          if (!empty($row['a_urgent']) || $row['a_urgent'] != 'no') {
                          ?>
                        <tr id="container-price-a-urgent">
                          <th>Urgent Accommodation Search</th>
                          <th class="price__invoice price__a-urgent" id="price__a-urgent">150</th>
                        </tr>
                        <?php }

                          if (!empty($row['summer_fee']) || $row['summer_fee'] != 'no') { ?>
                        <tr id="container-price-summer-fee">
                          <th>Summer Fee</th>
                          <th class="price__invoice price__summer-fee" id="price__summer-fee"><?php echo 45 * 4 ?>
                          </th>
                        </tr>
                        <?php }

                          if (!empty($row['minor_fee']) || $row['minor_fee'] != 'no') { ?>
                        <tr id="container-price-minor-fee">
                          <th>Minor Fee</th>
                          <th class="price__invoice price__minor-fee" id="price__minor-fee"><?php echo 75 * 4 ?></th>
                        </tr>
                        <?php } ?>

                        <tr>
                          <th>Transport</th>
                          <th class="price__invoice price__transport">
                            <?php echo $row['t_price'] ?>
                          </th>
                        </tr>
                      </tbody>


                    </table>
                  </div>

                  <div class="d-flex justify-content-center container__price container__tprice mb-5 mr-5">
                    <label for="total_weckly-db" class="label__tprice ">Total Price</label>
                    <input type="text" class="form__group-input text-center font-weight-bold" readonly value="$">
                    <input type="text" class="form__group-input text-center" readonly id="total__price" value="0">
                    <input type="text" class="form__group-input text-center font-weight-bold" readonly value="CAD">
                  </div>



                </div>

              </div>

              <?php }
              if ($row['n_airline'] == 'NULL' && $row['n_flight'] == 'NULL' && $row['departure_f'] == 'NULL' && $row['arrive_f'] == 'NULL') {
              } else {
              ?>


              <div class="card column3">


                <h3 class="form__group-title__col-4">Flight Information</h3>

                <div class="information__content">
                  <!-- Group Booking Confirmation -->

                  <?php if (empty($row['n_airline']) || $row['n_airline'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group__n_airline" id="group__n_airline">
                    <div class="form__group__icon-input">

                      <label for="n_airline" class="form__label">Reservation Code</label>

                      <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                      <p id="n_airline" name="n_airline" class="form__group-input n_airline">
                        <?php echo $row['n_airline']; ?> </p>

                    </div>
                  </div>

                  <?php } ?>

                  <!-- Group Booking Confirmation -->

                  <?php if (empty($row['n_flight']) || $row['n_flight'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group__n_flight" id="group__n_flight">
                    <div class="form__group__icon-input">

                      <label for="n_flight" class="form__label">Landing Flight Number</label>

                      <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                      <p id="n_flight" name="n_flight" class="form__group-input n_flight">
                        <?php echo $row['n_flight']; ?> </p>

                    </div>
                  </div>

                  <?php } ?>

                  <?php if (empty($row['departure_f']) || $row['departure_f'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group-f_date ">
                    <div class="form__group__icon-input">
                      <label for="f_date" class="form__label">Flight Date</label>
                      <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                      <?php $roc1 = date_create($row['departure_f']); ?>

                      <p id="f_date" name="f_date" class="form__group-input f_date">
                        <?php echo date_format($roc1, 'jS F Y h:i A'); ?> </p>

                    </div>
                  </div>

                  <?php }

                    if (empty($row['arrive_f']) || $row['arrive_f'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group-h_date">

                    <div class="form__group__icon-input">

                      <label for="h_date" class="form__label">Arrival at the Homestay</label>
                      <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                      <?php $roc2 = date_create($row['arrive_f']); ?>

                      <p id="h_date" name="h_date" class="form__group-input h_date">
                        <?php echo date_format($roc2, 'jS F Y'); ?> </p>


                    </div>
                  </div>

                  <?php } ?>
                </div>
              </div>


              <?php }


              if ($row['cont_name'] == 'NULL' && $row['cont_lname'] == 'NULL' && $row['num_conts'] == 'NULL' && $row['relationship'] == 'NULL') {
              } else {
              ?>



              <div class="card">

                <h3 class="form__group-title__col-4">Emergency Contact</h3>

                <div class="information__content">


                  <!-- Group Emergency Name -->

                  <?php if (empty($row['cont_name']) || $row['cont_name'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group__cont_name" id="group__cont_name">
                    <div class="form__group__icon-input">

                      <label for="cont_name" class="form__label">Contact Name</label>

                      <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>

                      <p id="cont_name" name="cont_name" class="form__group-input cont_name">
                        <?php echo $row['cont_name']; ?> </p>

                    </div>
                  </div>

                  <?php } ?>

                  <!-- Group Emergency Last Name -->

                  <?php if (empty($row['cont_lname']) || $row['cont_lname'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group__cont_lname" id="group__cont_lname">

                    <div class="form__group__icon-input">

                      <label for="cont_lname" class="form__label">Contact Last Name</label>

                      <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>

                      <p id="cont_lname" name="cont_lname" class="form__group-input cont_lname">
                        <?php echo $row['cont_lname'] ?> </p>

                    </div>
                  </div>

                  <?php } ?>


                  <!-- Group Emergency phone -->

                  <?php if (empty($row['num_conts']) || $row['num_conts'] == 'NULL') {
                    } else { ?>

                  <div class="form__group form__group__num_conts" id="group__num_conts">

                    <div class="form__group__icon-input">

                      <label for="num_conts" class="form__label">Emergency Phone Number</label>

                      <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                      <p id="num_conts" name="num_conts" class="form__group-input num_conts">
                        <?php echo $row['num_conts']; ?> </p>

                    </div>
                  </div>

                  <?php } ?>

                  <!-- Group Alternative Phone -->

                  <?php if (empty($row['relationship']) || $row['relationship'] == 'NULL') {
                    } else { ?>

                  <!-- //TODO RELATIONSHIP -->
                  <div class="form__group form__group__cell_s" id="group__cell_s">
                    <div class="form__group__icon-input">
                      <label for="cell_s" class="form__label">Relationship</label>
                      <label for="cont_lname" class="icon"><i class="icon icon__relation"></i></label>
                      <p id="num_conts" name="num_conts" class="form__group-input num_conts">
                        <?php echo $row['relationship']; ?> </p>
                    </div>
                    <p class="form__group__input-error">Minimum 4 Characters</p>
                  </div>

                  <?php } ?>


                </div>

              </div>

              <?php } ?>

            </div>



            <div class="col-md-8 column-2">

              <?php if ($row['db_s'] == 'NULL' && $row['gen_s'] == 'NULL' && $row['num_s'] == 'NULL' && $row['nationality'] == 'NULL' && $row['lang_s'] == 'NULL' && $row['language_a'] == 'NULL' && $row['passport'] == 'NULL' && $row['exp_pass'] == 'NULL' && $row['db_visa'] == 'NULL') {
              } else { ?>

              <div class="card">

                <h3 class="form__group-title__col-4">Personal Information</h3>

                <div class="information__content information__content-col-8">

                  <div class="row form__group-basic-content m-0 p-0">

                    <?php if (empty($row['about_me']) || $row['about_me'] == 'NULL') {
                      } else { ?>

                    <!-- Group Current About Me -->
                    <div
                      class="form__group col-sm-12 form__group__basic form__group__preference form__group__about_me mb-4"
                      id="group__about_me">

                      <div class="form__group__icon-input">

                        <label for="about_me" class="form__label allergies_l">Tell us about yourself (max. 150
                          Characters).</label>

                        <p class="form__group-input allergies" id="about_me"><?php echo $row['about_me'] ?></p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php } ?>

                    <!-- Group Date of Birth -->

                    <?php if (empty($row['db_s']) || $row['db_s'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__db mt-3" id="group__db">
                      <div class="form__group__icon-input">

                        <label for="db" class="form__label">Date of Birth</label>
                        <label for="db" class="icon"><i class="icon icon__date"></i></label>

                        <?php $roc3 = date_create($row['db_s']); ?>

                        <p id="db" name="db" class="form__group-input db"> <?php echo date_format($roc3, 'jS F Y'); ?>
                        </p>

                      </div>
                    </div>

                    <?php } ?>

                    <!-- Group Gender -->

                    <?php if (empty($row['gen_s']) || $row['gen_s'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__basic form__group__gender" id="group__gender">

                      <div class="form__group__icon-input">

                        <label for="gender" class="form__label">Gender</label>

                        <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                        <p id="gender" name="gender" class="form__group-input gender"> <?php echo $row['gen_s'] ?> </p>

                      </div>
                    </div>

                    <?php } ?>


                    <!-- Group Number -->

                    <?php if (empty($row['num_s']) || $row['num_s'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__num_s" id="group__num_s">

                      <div class="form__group__icon-input">

                        <label for="num_s" class="form__label">Phone Number</label>

                        <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>

                        <p id="num_s" name="num_s" class="form__group-input num_s"> <?php echo $row['num_s']; ?> </p>


                      </div>
                    </div>

                    <?php } ?>



                    <!-- Group Background -->

                    <?php if (empty($row['nationality']) || $row['nationality'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__basic form__group__nacionality"
                      id="group__nacionality">
                      <div class="form__group__icon-input">

                        <label for="nacionality" class="form__label">Background</label>

                        <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>

                        <p id="nacionality" name="nacionality" class="form__group-input nacionality">
                          <?php echo $row['nationality'] ?> </p>

                      </div>

                    </div>

                    <?php }

                      if (empty($row['lang_s']) || $row['lang_s'] == 'NULL') {
                      } else {  ?>

                    <!-- Group Origin Language -->
                    <div class="form__group col-lg-4 form__group__basic form__group__lang_s" id="group__lang_s">
                      <div class="form__group__icon-input">

                        <label for="lang_s" class="form__label">Origin Language</label>

                        <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>

                        <p id="lang_s" class="form__group-input lang_s"> <?php echo $row['lang_s'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['language_a']) || $row['language_a'] == 'NULL') {
                      } else {  ?>


                    <!-- Group Another Language -->
                    <div class="form__group col-lg-4 form__group__basic form__group__language_a" id="group__language_a">
                      <div class="form__group__icon-input">

                        <label for="language_a" class="form__label">Another Language</label>

                        <label for="language_a" class="icon"><i class="icon icon__nacionality"></i></label>

                        <p id="language_a" class="form__group-input nacionality"> <?php echo $row['language_a'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>


                    <?php } ?>

                    <!-- Group Passport -->

                    <?php if (empty($row['passport']) || $row['passport'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__basic form__group__pass" id="group__pass">
                      <div class="form__group__icon-input">

                        <label for="pass" class="form__label">Passport</label>

                        <label for="pass" class="icon"><i class="icon icon__pass"></i></label>

                        <p id="pass" name="pass" class="form__group-input pass"> <?php echo $row['passport'] ?> </p>


                      </div>
                    </div>

                    <?php }

                      if (empty($row['exp_pass']) || $row['exp_pass'] == 'NULL') {
                      } else {  ?>

                    <!-- Group Passport -->
                    <div class="form__group col-lg-4 form__group__basic form__group__exp_pass" id="group__exp_pass">
                      <div class="form__group__icon-input">

                        <label for="exp_pass" class="form__label">Passport Expiration Date</label>

                        <label for="exp_pass" class="icon"><i class="icon icon__bl"></i></label>

                        <?php $exp_pass = date_create($row['exp_pass']); ?>

                        <p id="exp_pass" class="form__group-input pass"> <?php echo date_format($exp_pass, 'j, F Y'); ?>
                        </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php } ?>

                    <!-- Group Visa Expiration Date -->

                    <?php if (empty($row['db_visa']) || $row['db_visa'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-lg-4 form__group__basic form__group__bl" id="group__bl">
                      <div class="form__group__icon-input">

                        <label for="bl" class="form__label">Visa Expiration Date</label>
                        <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                        <?php $roc4 = date_create($row['db_visa']); ?>

                        <p id="bl" name="bl" class="form__group-input bl"> <?php echo date_format($roc4, 'j, F Y'); ?>
                        </p>

                      </div>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php }

              if ($row['smoke_s'] == 'NULL' && $row['drinks_alc'] == 'NULL' && $row['drugs'] == 'NULL' && $row['allergy_a'] == 'NULL' && $row['allergy_m'] == 'NULL' && $row['healt_s'] == 'NULL' && $row['disease'] == 'NULL' && $row['treatment'] == 'NULL' && $row['treatment_p'] == 'NULL' && $row['allergies'] == 'NULL' && $row['surgery'] == 'NULL') {
              } else { ?>

              <div class="card">

                <h3 class="form__group-title__col-4">Health Information</h3>

                <div class="information__content information__content-col-8">

                  <div class="row form__group-basic-content m-0 p-0">

                    <?php if (empty($row['smoke_s']) || $row['smoke_s'] == 'NULL') {
                      } else { ?>

                    <!-- Group Smoker -->
                    <div class="form__group col-lg-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                      <div class="form__group__icon-input">

                        <label for="smoke_s" class="form__label">Do you smoke?</label>

                        <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                        <p class="form__group-input smoke_s" id="smoke_s" name="smoke_s"><?php echo $row['smoke_s'] ?>
                        </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php }


                      if (empty($row['drinks_alc']) || $row['drinks_alc'] == 'NULL') {
                      } else {

                        if ($row['drinks_alc'] == 'no') {
                          $drinks_alc = 'No';
                        } else if ($row['drinks_alc'] == 'yes') {
                          $drinks_alc = 'Yes';
                        }

                      ?>

                    <!-- Group Alcoholic Beverages -->
                    <div class="form__group col-lg-4 form__group__basic form__group__drinks_alc" id="group__drinks_alc">

                      <div class="form__group__icon-input">

                        <label for="drinks_alc" class="form__label">Do you Drink Alcohol?</label>

                        <label for="drinks_alc" class="icon"><i class="icon icon__drinks_alc"></i></label>

                        <o class="form__group-input smoke_s" id="drinks_alc"><?php echo $drinks_alc ?></o>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php }

                      if (empty($row['drugs']) || $row['drugs'] == 'NULL') {
                      } else {

                        if ($row['drugs'] == 'no') {
                          $drugs = 'No';
                        } else if ($row['drugs'] == 'yes') {
                          $drugs = 'Yes';
                        }

                      ?>

                    <!-- Group Drugs -->
                    <div class="form__group col-lg-4 form__group__basic form__group__drugs" id="group__drugs">

                      <div class="form__group__icon-input">

                        <label for="drugs" class="form__label ">Have you used any drugs?</label>

                        <label for="drugs" class="icon"><i class="icon icon__drugs"></i></label>

                        <p class="form__group-input smoke_s" id="drugs"> <?php echo $drugs ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php }

                      if (empty($row['allergy_a']) || $row['allergy_a'] == 'NULL') {
                      } else {

                        if ($row['allergy_a'] == 'no') {
                          $allergy_a = 'No';
                        } else if ($row['allergy_a'] == 'yes') {
                          $allergy_a = 'Yes';
                        } else {
                          $allergy_a = $row['allergy_a'];
                        }

                      ?>

                    <!-- Group Allergy to Animals -->
                    <div class="form__group col-lg-6 form__group__basic form__group__allergy_a" id="group__allergy_a">

                      <div class="form__group__icon-input">

                        <label for="allergy_a" class="form__label">Any Allergy to Animals</label>

                        <label for="allergy_a" class="icon"><i class="icon icon__pets"></i></label>

                        <p class=" form__group-input pets" id="allergy_a"> <?php echo $allergy_a ?> </p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>


                    <?php }

                      if (empty($row['allergy_m']) || $row['allergy_m'] == 'NULL') {
                      } else {

                        if ($row['allergy_m'] == 'no') {
                          $allergy_m = 'No';
                        } else if ($row['allergy_m'] == 'yes') {
                          $allergy_m = 'Yes';
                        } else {
                          $allergy_m = $row['allergy_m'];
                        }

                      ?>


                    <!-- Group Dietary Restrictions -->
                    <div class="form__group col-lg-6 form__group__basic form__group__allergy_m" id="group__allergy_m">

                      <div class="form__group__icon-input">

                        <label for="allergy_m" class="form__label ">Dietary Restrictions</label>

                        <label for="allergy_m" class="icon"><i class="icon icon__food"></i></label>

                        <p class="form__group-input pets" id="allergy_m"> <?php echo $allergy_m ?> </p>

                      </div>


                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['healt_s']) || $row['healt_s'] == 'NULL') {
                      } else { ?>

                    <!-- Group Current Heal Information -->
                    <div class="form__group col-sm-12 form__group__basic form__group__preference form__group__healt_s"
                      id="group__healt_s">

                      <div class="form__group__icon-input">

                        <label for="healt_s" class="form__label allergies_l">What is your state of health at the
                          moment?</label>

                        <label for="healt_s" class="icon"><i class="icon icon__healt_s"></i></label>

                        <p class="form__group-input allergies" id="healt_s"><?php echo $row['healt_s'] ?></p>


                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['disease']) || $row['disease'] == 'NULL') {
                      } else {

                        if ($row['disease'] == 'no') {
                          $disease = 'No';
                        } else if ($row['disease'] == 'yes') {
                          $disease = 'Yes';
                        } else {
                          $disease = $row['disease'];
                        }

                      ?>


                    <!-- Group Disease -->
                    <div class="form__group col-xl-6 form__group__basic form__group__preference form__group__disease"
                      id="group__disease">

                      <div class="form__group__icon-input">

                        <label for="disease" class="form__label">Do you suffer from any disease?</label>

                        <label for="disease" class="icon"><i class="icon icon__allergies2"></i></label>

                        <p class="form__group-input pets" id="healt_s"><?php echo $disease ?></p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }

                      if (empty($row['treatment']) || $row['treatment'] == 'NULL') {
                      } else {

                        if ($row['treatment'] == 'no') {
                          $treatment = 'No';
                        } else if ($row['treatment'] == 'yes') {
                          $treatment = 'Yes';
                        } else {
                          $treatment = $row['treatment'];
                        }

                      ?>

                    <!-- Group Treatment -->
                    <div class="form__group col-xl-6 form__group__basic form__group__preference form__group__treatment"
                      id="group__treatment">

                      <div class="form__group__icon-input">

                        <label for="treatment" class="form__label">Any Treatment or Medication?</label>

                        <label for="treatment" class="icon"><i class="icon icon__allergies"></i></label>

                        <p class="form__group-input pets" id="treatment"><?php echo $treatment ?></p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['treatment_p']) || $row['treatment_p'] == 'NULL') {
                      } else {

                        if ($row['treatment_p'] == 'no') {
                          $treatment_p = 'No';
                        } else if ($row['treatment_p'] == 'yes') {
                          $treatment_p = 'Yes';
                        } else {
                          $treatment_p = $row['treatment_p'];
                        }

                      ?>

                    <!-- Group Psychological Treatment -->
                    <div
                      class="form__group col-sm-6 form__group__basic form__group__preference form__group__treatment_p"
                      id="group__treatment_p">

                      <div class="form__group__icon-input">

                        <label for="treatment_p" class="form__label">Any Psychological Treatment?</label>

                        <label for="treatment_p" class="icon"><i class="icon icon__allergies"></i></label>

                        <p class="form__group-input pets" id="treatment_p"><?php echo $treatment_p ?></p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['allergies']) || $row['allergies'] == 'NULL') {
                      } else {

                        if ($row['allergies'] == 'no') {
                          $allergies = 'No';
                        } else if ($row['allergies'] == 'yes') {
                          $allergies = 'Yes';
                        } else {
                          $allergies = $row['allergies'];
                        }

                      ?>


                    <!-- Group Allergies -->
                    <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__allergies"
                      id="group__allergies">

                      <div class="form__group__icon-input">

                        <label for="allergies" class="form__label">Any allergies?</label>

                        <label for="allergies" class="icon"><i class="icon icon__allergies2"></i></label>

                        <p class="form__group-input pets" id="allergies"><?php echo $allergies ?></p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                      if (empty($row['surgery']) || $row['surgery'] != 'no') {
                      } else {

                        if ($row['surgery'] == 'no') {
                          $surgery = 'No';
                        } else if ($row['surgery'] == 'yes') {
                          $surgery = 'Yes';
                        } else {
                          $surgery = $row['surgery'];
                        }


                      ?>

                    <!-- Group Surgery -->
                    <div
                      class="form__group col-sm-6 form__group__basic form__group__preference form__group__surgery mb-0"
                      id="group__surgery">

                      <div class="form__group__icon-input">

                        <label for="surgery" class="form__label">Any surgery in the last 12 months?</label>

                        <label for="surgery" class="icon"><i class="icon icon__allergies2"></i></label>

                        <p class="form__group-input pets" id="allergies"><?php echo $surgery ?></p>

                      </div>

                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>


                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php
              }

              if ($row2['name_a'] == 'NULL' && $row['english_l'] == 'NULL' && $row['type_s'] == 'NULL' && $row['prog_selec'] == 'NULL' && $row['schedule'] == 'NULL') {
              } else {

              ?>


              <div class="card">

                <h3 class="form__group-title__col-4">Professional Information</h3>

                <div class="information__content information__content-col-8">

                  <div class="row form__group-basic-content m-0 p-0">


                    <!-- Group Academy Preference -->

                    <?php if (empty($row2['name_a']) || $row2['name_a'] == 'NULL') {
                      } else { ?>

                    <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                      <div class="form__group__icon-input">

                        <label for="n_a" class="form__label">Academy Preference</label>

                        <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                        <p id="n_a" name="n_a" class="form__group-input n_a"> <?php echo $row2['name_a'] ?> </p>

                      </div>

                    </div>

                    <?php }

                      if (empty($row['english_l']) || $row['english_l'] == 'NULL') {
                      } else { ?>

                    <!-- Group English Level -->
                    <div class="form__group col-lg-4 form__group__basic form__group__english_l" id="group__english_l">

                      <div class="form__group__icon-input">

                        <label for="english_l" class="form__label">Current English level</label>

                        <label for="english_l" class="icon"><i class="icon icon__city"></i></label>

                        <p class="form__group-input smoke_s" id="english_l"> <?php echo $row['english_l'] ?> </p>



                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php }

                      if (empty($row['type_s']) || $row['type_s'] == 'NULL') {
                      } else { ?>

                    <!-- Group Type Student -->
                    <div class="form__group col-lg-4 form__group__basic form__group__type_s" id="group__type_s">
                      <div class="form__group__icon-input">

                        <label for="type_s" class="form__label">Type Student</label>

                        <label for="type_s" class="icon"><i class="icon icon__names"></i></label>

                        <p class="form__group-input smoke_s" id="type_s"> <?php echo $row['type_s'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }

                      if ($row['type_s'] == 'NULL' || $row['type_s'] == 'High School' || $row['type_s'] == 'English and French' || $row['prog_selec'] == "NULL") {
                      } else { ?>


                    <!-- Group Program to Study -->
                    <div class="form__group col-lg-4 form__group__basic form__group__prog_selec" id="group__prog_selec">

                      <div class="form__group__icon-input">

                        <label for="prog_selec" class="form__label">Program to Study </label>

                        <label for="prog_selec" class="icon"><i class="icon icon__prog_selec"></i></label>
                        <p class="form__group-input smoke_s" id="prog_selec"> <?php echo $row['prog_selec'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php }

                      if (empty($row['schedule']) || $row['schedule'] == 'NULL') {
                      } else {

                      ?>


                    <!-- Group Schedule  -->
                    <div class="form__group col-lg-4 form__group__basic form__group__schedule" id="group__schedule">

                      <div class="form__group__icon-input">

                        <label for="schedule" class="form__label smoker"> Schedule </label>

                        <label for="schedule" class="icon"><i class="icon icon__city"></i></label>
                        <p class="form__group-input smoke_s" id="schedule"> <?php echo $row['schedule'] ?> </p>
                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>

                    </div>

                    <?php } ?>


                  </div>

                </div>

              </div>

              <?php }

              if ($row['smoker_l'] == 'NULL' && $row['children'] == 'NULL' && $row['teenagers'] == 'NULL' && $row['pets'] == 'NULL' && $row['lodging_type'] == "no" && $row['food'] == "no" && $row['pick_up'] == "no" && $row['drop_off']) {
              } else {

              ?>


              <div class="card">

                <h3 class="form__group-title__col-4">House Preferences</h3>

                <?php

                  if ($row['smoker_l'] == 'NULL' && $row['children'] == 'NULL' && $row['teenagers'] == 'NULL' && $row['pets'] == 'NULL') {
                  } else {

                  ?>

                <h3 class="subtitle__section-card mb-4">Can you Share With:</h3>

                <div class="information__content information__content-col-8">

                  <div class="row form__group-basic-content m-0 p-0">

                    <!-- Group Smoker -->

                    <?php if (empty($row['smoker_l']) || $row['smoker_l'] == 'NULL') {
                        } else {

                          if ($row['smoker_l'] == 'no') {
                            $smoker_l = 'No';
                          } else if ($row['smoker_l'] == 'yes') {
                            $smoker_l = 'Yes';
                          }

                        ?>

                    <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                      <div class="form__group__icon-input">

                        <label for="smoke_s" class="form__label ">Smokers?</label>

                        <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                        <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $smoker_l ?> </p>
                      </div>


                    </div>
                    <?php }


                        if (empty($row['children']) || $row['children'] == 'NULL') {
                        } else {

                          if ($row['children'] == 'no') {
                            $children = 'No';
                          } else if ($row['children'] == 'yes') {
                            $children = 'Yes';
                          }

                        ?>

                    <!-- Group Can you live with Children? -->
                    <div class="form__group col-lg-4 form__group__basic form__group__children" id="group__children">

                      <div class="form__group__icon-input">

                        <label for="children" class="form__label">Children?</label>

                        <label for="children" class="icon"><i class="icon icon__children"></i></label>

                        <p id="children" class="form__group-input smoke_s"> <?php echo $children ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                        if (empty($row['teenagers']) || $row['teenagers'] == 'NULL') {
                        } else {

                          if ($row['teenagers'] == 'no') {
                            $teenagers = 'No';
                          } else if ($row['teenagers'] == 'yes') {
                            $teenagers = 'Yes';
                          }


                        ?>


                    <!-- Group Can you live with Teenagers? -->
                    <div class="form__group col-lg-4 form__group__basic form__group__teenagers" id="group__teenagers">

                      <div class="form__group__icon-input">

                        <label for="teenagers" class="form__label pets_l">Teenagers?</label>

                        <label for="teenagers" class="icon"><i class="icon icon__children"></i></label>

                        <p id="teenagers" class="form__group-input smoke_s"> <?php echo $teenagers ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php } ?>

                    <!-- Group Pets -->

                    <?php if (empty($row['pets']) || $row['pets'] == 'NULL') {
                        } else { ?>

                    <div class="form__group col-sm-4 form__group__basic form__group__pets" id="group__pets">

                      <div class="form__group__icon-input">

                        <label for="pets" class="form__label pets_l">Pets</label>

                        <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                        <p id="pets" class="form__group-input pets"> <?php echo $row['pets'] ?> </p>

                      </div>

                    </div>

                    <?php } ?>

                  </div>

                </div>


                <?php }

                  if ($row['lodging_type'] == 'NULL' && $row['food'] == 'NULL') {
                  } else {

                  ?>


                <h3 class="subtitle__section-card mb-4">Accommodation</p>
                </h3>

                <div class="information__content information__content-col-8">

                  <div class="row form__group-basic-content m-0 p-0">

                    <?php if (empty($row['lodging_type']) || $row['lodging_type'] == 'NULL') {
                        } else { ?>

                    <!-- Group End Date of stay -->
                    <div class="form__group col-lg-4 form__group__lodging_type" id="group__lodging_type">
                      <div class="form__group__icon-input">

                        <label for="lodging_type" class="form__label">Type of Accommodation</label>
                        <label for="lodging_type" class="icon"><i class="icon icon__lodging"></i></label>

                        <p id="lodging_type" class="form__group-input pets"> <?php echo $row['lodging_type'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }


                        if (empty($row['meal_p']) || $row['meal_p'] == 'NULL') {
                        } else { ?>

                    <!-- Group Food -->
                    <div class="form__group col-lg-4 form__group__basic form__group__food " id="group__food">

                      <div class="form__group__icon-input">

                        <label for="food" class="form__label">Meal Plan</label>

                        <label for="food" class="icon"><i class="icon icon__food"></i></label>

                        <p id="food" class="form__group-input pets"> <?php echo $row['meal_p'] ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php }

                        if ($row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'no') {
                        } else {

                          if ($row['vegetarians'] == 'yes' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'no') {

                            $diet = 'Vegetarians';
                          } elseif ($row['vegetarians'] == 'no' && $row['halal'] == 'yes' && $row['kosher'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'no') {

                            $diet = 'Halal (Muslims)';
                          } elseif ($row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'yes' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'no') {

                            $diet = 'Kosher (Jews)';
                          } elseif ($row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['lactose'] == 'yes' && $row['gluten'] == 'no' && $row['pork'] == 'no') {

                            $diet = 'Lactose Intolerant';
                          } elseif ($row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'yes' && $row['pork'] == 'no') {

                            $diet = 'Gluten Free Diet';
                          } elseif ($row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'yes') {

                            $diet = 'Pork';
                          }


                        ?>

                    <!-- Group Food -->
                    <div class="form__group col-lg-4 form__group__basic form__group__diet" id="group__diet">

                      <div class="form__group__icon-input">

                        <label for="diet" class="form__label">Special Diet</label>

                        <label for="diet" class="icon"><i class="icon icon__food"></i></label>

                        <p id="diet" class="form__group-input pets"> <?php echo $diet ?> </p>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <?php } ?>



                  </div>

                </div>
                <?php } ?>

                <h3 class="subtitle__section-card mb-4">Optional Supplements</h3>

                <div class="information__content information__content-col-8">

                  <!-- <p class="text__supplements"><b>Accommodation near the school:</b> Additional <b>$35 CAD</b> per night
                    applies (up to 30 minutes
                    walking).</p>
                  <p class="text__supplements"><b>Urgent accommodation search:</b> Less than 1 week = <b>$150 CAD</b>.
                  </p>
                  <p class="text__supplements"><b>Guardianship:</b> Guardianship is a practical and moral support system
                    for young students studying
                    in a foreign country, far from their parents = <b>$550 CAD</b>.</p>
                  <p class="text__supplements ">
                    <b>Minor fee (-18 years old): $75 CAD</b> per week.
                  </p>
                  <p class="text__supplements ">
                    <b>Summer fee: $45 CAD </b> per week.
                  </p> -->

                  <div class="row form__group-basic-content m-0 p-0 mt-5">

                    <!-- // TODO ACCOMMODATION NEAR THE SCHOOL -->
                    <div class="form__group col-lg-5 mt-2 mb-md-5 form__group__basic form__group__near-school"
                      id="group__near-school">

                      <div class="form__group__icon-input">

                        <label for="accomm-near" class="form__label">Accommodation near the school
                        </label>
                        <label for="accomm-near" class="form__label pl-1"></label>
                        <label for="accomm-near" class="icon"><i class="icon icon_accommodation"></i></label>

                        <?php if (empty($row['a_near']) || $row['a_near'] == 'NULL') { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-near" value="No" readonly>
                        <?php } else { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-near"
                          value="<?php echo ucfirst($row['a_near']) ?>" readonly>
                        <?php } ?>
                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <!-- // TODO ACCOMMODATION URGENT -->
                    <div class="form__group col-lg-5 mt-2 form__group__basic form__group__accomm-urgent"
                      id="group__accomm-urgent">

                      <div class="form__group__icon-input">

                        <label for="accomm-urgent" class="form__label pl-1">Urgent accommodation search
                        </label>
                        <label for="accomm-urgent" class="icon"><i class="icon icon_accommodation"></i></label>

                        <?php if (empty($row['a_urgent']) || $row['a_urgent'] == 'NULL') { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-urgent" value="No" readonly>
                        <?php } else { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-urgent"
                          value="<?php echo ucfirst($row['a_urgent']) ?>" readonly>
                        <?php } ?>


                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <!-- // TODO SUMMER FEE -->
                    <div class="form__group col-lg-5 mt-2 form__group__basic form__group__summer-fee"
                      id="group__summer-fee">

                      <div class="form__group__icon-input">

                        <label for="summer-fee" class="form__label pl-1">Do you want the Summer Fee plan?
                        </label>
                        <label for="summer-fee" class="icon"><i class="icon icon_accommodation"></i></label>

                        <?php if (empty($row['summer_fee']) || $row['summer_fee'] == 'NULL') { ?>
                        <input type="text" class="form__group-input pets w-100" id="summer-fee" value="No" readonly>
                        <?php } else { ?>
                        <input type="text" class="form__group-input pets w-100" id="summer-fee"
                          value="<?php echo ucfirst($row['summer_fee']) ?>" readonly>
                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <!-- // TODO MINOR FEE -->
                    <div class="form__group col-lg-5 mt-2 form__group__basic form__group__minor-fee"
                      id="group__minor-fee">

                      <div class="form__group__icon-input">

                        <label for="minor-fee" class="form__label pl-1">Are you under 18?</label>
                        <label for="minor-fee" class="icon"><i class="icon icon__relation"></i></label>

                        <?php if (empty($row['minor_fee']) || $row['minor_fee'] == 'NULL') { ?>
                        <input type="text" class="form__group-input pets w-100" id="minor-fee" value="No" readonly>
                        <?php } else { ?>
                        <input type="text" class="form__group-input pets w-100" id="minor-fee"
                          value="<?php echo ucfirst($row['minor_fee']) ?>" readonly>
                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div>

                    <!-- // TODO GUARDIANSHIP -->
                    <!-- <div class="form__group col-lg-5 mt-5 form__group__basic form__group__accomm-guardianship"
                      id="group__accomm-guardianship">

                      <div class="form__group__icon-input">

                        <label for="accomm-guardianship" class="form__label pl-1">Guardianship </label>
                        <label for="accomm-guardianship" class="icon"><i class="icon icon__relation"></i></label>

                        <?php if (empty($row['a_guardianship']) || $row['a_guardianship'] == 'NULL') { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-guardianship" value="Empty"
                          readonly>
                        <?php } else { ?>
                        <input type="text" class="form__group-input pets w-100" id="accomm-guardianship"
                          value="<?php echo ucfirst($row['a_guardianship']) ?>" readonly>
                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Enter a valid Date</p>
                    </div> -->
                  </div>

                </div>

                <?php

                  if ($row['pick_up'] == 'NULL' && $row['drop_off'] == 'NULL') {
                  } else {


                  ?>

                <h3 class="subtitle__section-card mb-4">Transport </h3>

                <div class="row justify-content-center suboption mt-5 mb-5">

                  <?php if (empty($row['pick_up']) || $row['pick_up'] == 'NULL') {
                      } else {

                        if ($row['pick_up'] == 'no') {
                          $pick_up = 'No';
                        } else if ($row['pick_up'] == 'yes') {
                          $pick_up = 'Yes';
                        }

                      ?>

                  <!-- Group Pick-Up Service -->
                  <div class="form__group col-sm-5 form__group__basic form__group__pick_up" id="group__pick_up">

                    <div class="form__group__icon-input">

                      <label for="pick_up" class="form__label">Pick Up Service</label>

                      <label for="pick_up" class="icon"><i class="icon icon__city"></i></label>

                      <p id="pick_up" class="form__group-input pets"> <?php echo $pick_up ?> </p>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php }

                      if (empty($row['drop_off']) || $row['drop_off'] == 'NULL') {
                      } else {

                        if ($row['drop_off'] == 'no') {
                          $drop_off = 'No';
                        } else if ($row['drop_off'] == 'yes') {
                          $drop_off = 'Yes';
                        }

                      ?>

                  <!-- Group Drop Off Service -->
                  <div class="form__group col-sm-5 form__group__basic form__group__drop_off" id="group__drop_off">

                    <div class="form__group__icon-input">

                      <label for="drop_off" class="form__label pets_l">Drop off service</label>

                      <label for="drop_off" class="icon"><i class="icon icon__city"></i></label>

                      <p id="drop_off" class="form__group-input pets"> <?php echo $drop_off ?> </p>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php } ?>

                </div>

                <?php } ?>


              </div>

              <?php } ?>



            </div>
            <?php if ($row['pass_photo'] == 'NULL' && $row['visa'] == 'NULL' && $row['flight_image'] == 'NULL' && $row['signature_s'] == 'NULL') {
            } else { ?>

            <div class="col-md-8 mr-auto ml-auto">

              <div class="card">

                <h3 class="form__group-title__col-4"> Attached Files </h3>

                <div class="step__body mt-3 mb-5">


                  <div class="row justify-content-center suboption ">

                    <?php if (empty($row['pass_photo']) || $row['pass_photo'] == 'NULL') {
                      } else { ?>

                    <div class="col-lg-5">

                      <h3 class="subtitle__section-card mb-4">Passport Photo</h3>

                      <div class="form__group form__group__basic form__group__visa" id="group__visa">

                        <div class="form__group__div__photo__visa">


                          <iframe class="form__group__photo__visa" id="preview-ii"
                            src="../<?php echo $row['pass_photo'] ?>" id="visa"></iframe>

                        </div>
                        <p class="form__group__input-error">Enter a valid Date</p>

                      </div>

                    </div>

                    <?php } ?>

                    <?php if (empty($row['visa']) || $row['visa'] == 'NULL') {
                      } else {  ?>

                    <div class="col-lg-5">

                      <h3 class="subtitle__section-card mb-4">Visa Photo</h3>

                      <div class="form__group form__group__basic form__group__visa" id="group__visa">

                        <div class="form__group__div__photo__visa">

                          <iframe class="form__group__photo__visa" id="preview-i" src="../<?php echo $row['visa'] ?>"
                            id="visa"></iframe>

                        </div>
                        <p class="form__group__input-error">Enter a valid Date</p>
                      </div>

                    </div>

                    <?php } ?>

                    <?php if (empty($row['flight_image']) || $row['flight_image'] == 'NULL') {
                      } else {  ?>

                    <div class="col-lg-5 mt-5">

                      <h3 class="subtitle__section-card mb-4">Flight Ticket Image</h3>

                      <div class="form__group form__group__basic form__group__visa" id="group__visa">

                        <div class="form__group__div__photo__visa">

                          <iframe class="form__group__photo__visa" id="preview-i"
                            src="../<?php echo $row['flight_image'] ?>" id="visa"></iframe>

                        </div>
                        <p class="form__group__input-error">Enter a valid Date</p>
                      </div>

                    </div>

                    <?php } ?>

                    <?php if (empty($row['signature_s']) || $row['signature_s'] == 'NULL') {
                      } else {  ?>

                    <div class="col-lg-5 mt-5">

                      <h3 class="subtitle__section-card mb-4">Signature</h3>

                      <div class="form__group form__group__basic form__group__visa" id="group__visa">

                        <div class="form__group__div__photo__visa">

                          <img class="form__group__photo__visa" id="preview-i"
                            src="../<?php echo $row['signature_s'] ?>" id="visa">

                        </div>
                        <p class="form__group__input-error">Enter a valid Date</p>
                      </div>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

            </div>

            <?php } ?>
          </div>


          <br>




        </form>
    </main>

  </div>


  <!-- // TODO FOOTER -->
  <?php include 'footer.php'; ?>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/ajax_noti.js"></script>
  <script src="assets/js/voucher.js" type="module"></script>
  <script src="assets/js/student-info.js?ver=1.4" type="module"></script>
</body>

</html>