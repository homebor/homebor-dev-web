<?php
    require '../xeon.php';
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

    $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
    $row_student = $studentQuery->fetch_assoc();


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $sql = "SELECT * FROM notification WHERE user_i_mail = '$usuario'"; 
    $connStatus = $link->query($sql); 
    $numberOfRows = mysqli_num_rows($connStatus);

    if (isset($row2['confirmed']) && $row2['confirmed'] == '1') {
        $way = 'student_confirmed.php';
    } else { 
        $way = 'index.php';
    }
?>
<!-- WRAPPER - FONDO
    =================================================================================================================-->

<!--*********************************************************************************************************-->
<!--HEADER **************************************************************************************************-->
<!--*********************************************************************************************************-->
<header id="ts-header" class="fixed-top">
  <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
  <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
  <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
    <div class="container">

      <!--Brand Logo/ Logo HOMEBOR-->
      <a class="navbar-brand" href="index.php">
        <?php if($row_student['id_m'] == '10'){ ?>
        <img src="../assets/iHomestay/logos/iHomestay_logo.png" alt="" style="width: 200px">
        <?php }else{ ?>
        <img src="../assets/logos/page.png" alt="">
        <?php } ?>
      </a>

      <?php include 'notification-after.php'; ?>

      <!--Responsive Collapse Button-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary"
        aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!--Collapsing Navigation-->
      <div class="collapse navbar-collapse" id="navbarPrimary">

        <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
        <ul class="navbar-nav">

          <!--HOME (Main level)
                        =============================================================================================-->
          <li class="nav-item ">

            <!--Main level link-->
            <a class="nav-link active" href="<?php echo $way;?>" id="active">
              Home
              <span class="sr-only">(current)</span>
            </a>

          </li>



          <!--PAGES (Main level)
                        =============================================================================================-->
          <li class="nav-item">

            <!--Main level link-->
            <a class="nav-link" href="voucher.php" id="nav">Voucher</a>


          </li>
          <!--end PAGES nav-item-->


          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link active" href="#" id="nav">
              Configuration
              <span class="sr-only">(current)</span>
            </a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- MAP (1st level)
                                =====================================================================================-->
              <li class="nav-item">

                <a href="student_info.php" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Profile </p>
                </a>

                <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
              <li class="nav-item">

                <a href="edit_student.php" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Edit Profile </p>
                </a>

              </li>
              <!--end OpenStreetMap-->


              <!--end MapBox-->

              <!--end SLIDER (1st level)-->

            </ul>
            <!--end List (1st level) -->

          </li>
          <!--end HOME nav-item-->

          <li class="nav-item ">

            <!--Main level link-->
            <a class="nav-link active" href="reports.php" id="active">
              Reports
              <span class="sr-only">(current)</span>
            </a>

          </li>

          <li class="nav-item noti" id="noti">
            <?php include 'notification-before.php' ?>
          </li>

          <!--ABOUT US (Main level)
                        =============================================================================================-->
          <li class="nav-item">
            <a class="nav-link" href="about-us.html"></a>
          </li>
          <!--end ABOUT US nav-item-->

          <!--CONTACT (Main level)
                        =============================================================================================-->
          <li class="nav-item">
            <a class="nav-link mr-2" href="../contact.html"></a>
          </li>
          <!--end CONTACT nav-item-->


        </ul>
        <!--end Left navigation main level-->

        <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
        <ul class="navbar-nav ml-auto">

          <!--LOGIN (Main level)
                        =============================================================================================-->
          &nbsp;&nbsp;<li class="nav-item" style="list-style: none;">
            <a class="nav-link zoom" href="../logout.php" id="nav">Logout</a>
          </li>


        </ul>
        <!--end Right navigation-->

      </div>
      <!--end navbar-collapse-->
    </div>
    <input type="hidden" id="type_user" value="student">

    <!--end container-->
  </nav>
  <!--end #ts-primary-navigation.navbar-->

</header>

<script src="../assets/js/control_events.js?ver=1.0"></script>