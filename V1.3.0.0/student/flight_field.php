<?php
include '../xeon.php';
session_start();
error_reporting(0);


// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

$query="SELECT * FROM pe_student WHERE mail_s = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/index.css">
    <link rel="stylesheet" type="text/css" href="assets/css/flight_field.css">
    <title>Document</title>
</head>
<body>

<style>
 
</style>

<header id="ts-header" class="fixed-top" style="background-color: #4f177d;">
        <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light" style="background-color: #4f177d; box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -webkit-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75);">
            <div class="container">

                <!--Brand Logo/ Logo HOMEBOR-->
                <a class="navbar-brand" href="index.php" style="margin-left:0">
                    <img src="../assets/logos/white.png" class="logo_white" alt="" >
                </a>

                <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        &nbsp;&nbsp;<li class="nav-item" style="list-style: none;">
                            <a class="nav-link zoom logout" href="../logout.php" id="nav" style="color:#fff">Logout</a>
                        </li>


                </ul>

            </div>

        </nav>

</header>


<main class="ts-main">
    <div class="container">
        <div class="col-12 card p-5 justify-content-center">
            <h2 class="text-sm-start border_bottom pl-3 pb-3 mb-4">Add information</h2>

            <h4 class="text-center">Please, to continue you must fill in the following fields correctly</h4>

            <form method="POST" action="save_flight.php" enctype="multipart/form-data">

                <div class="col-sm-10 ml-auto mr-auto mt-1 mb-4 row">

                    <div class="row w-100 justify-content-center mb-4 res_row">

                        <div class="col-sm-12 mb-5" align="center">

                            <div class="col-xl-5">

                                <div class="carousel-item active">

                                    <img id="preview-x" class="add-photo" >

                                    <input type="file" name="ticket" id="file-x" accept="jpg|png|jpeg|pdf" onchange="previewImage_x();" style="display: none">

                                    <label for="file-x" class="photo-add" id="label-x">  <p class="form__group-l_title"> Flight Ticket Picture </p> </label>

                                    <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" style="display: none" title="Change Bathroom Photo">  </label>

                                </div>

                            </div>
                        
                        </div>

                        <!--Origin Language-->
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label class="input_label" for="n_airline">Booking Confirmation</label>
                                <?php if($row['n_airline'] == 'NULL'){ ?> 
                                    <input type="text" class="form-control input__bordeless" id="n_airline" name="n_airline">
                                <?php }else{ ?>
                                    <input type="text" class="form-control input__bordeless" id="n_airline" name="n_airline" value="<?php echo $row['n_airline'] ?>">
                                <?php } ?>
                            </div>
                        </div>

                        <!--Type Student-->
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label class="input_label" for="n_flight">Landing Flight Number</label>
                                <?php if($row['n_flight'] == 'NULL'){ ?> 
                                    <input type="text" class="form-control input__bordeless" id="n_flight" name="n_flight">
                                <?php }else{ ?>
                                    <input type="text" class="form-control input__bordeless" id="n_flight" name="n_flight" value="<?php echo $row['n_flight'] ?>">
                                <?php } ?>
                            </div>
                        </div>

                        <!--Arrive-->
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label class="input_label" for="f_date">Flight Date</label>
                                <?php if($row['departure_f'] == 'NULL'){ ?> 
                                    <input type="date" class="form-control input__bordeless" id="f_date" name="f_date" placeholder="YYYY-MM-DD">
                                <?php }else{ ?>
                                    <input type="date" class="form-control input__bordeless" id="f_date" name="f_date" value="<?php echo $row['departure_f'] ?>">
                                <?php } ?>
                                
                            </div>
                        </div>

                    </div>

                    <div class="row w-100 justify-content-center mb-4 res_row">

                        <!--Last-->
                        <div class="col-xl-5">
                            <div class="form-group">
                                <label class="input_label" for="h_date">Arrival Date at the Homestay</label>
                                <?php if($row['firstd'] == 'NULL'){ ?> 
                                    <input type="date" class="form-control input__bordeless" id="h_date" placeholder="YYYY-MM-DD">
                                <?php }else{ ?>
                                    <input type="date" class="form-control input__bordeless" id="h_date" value="<?php echo $row['firstd'] ?>" readonly>
                                <?php } ?>
                            </div>
                        </div>

                        
                        <div class="col-xl-5">
                            <div class="form-group">
                                <label class="input_label1" for="h_date">Did you make the homestay payment?</label>
                                <select class="custom-select input__bordeless" name="payment" id="payment" style="color:#000">
                                    <option hidden="option" value="" selected disabled>Select Option</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <input type="hidden" name="mail_s" value="<?php echo $row['mail_s'] ?>">
                </div>

                <?php if($row['n_airline'] != 'NULL' && $row['n_flight'] != 'NULL' && $row['departure_f'] != 'NULL' && $row['firstd'] != 'NULL' && $row['ac_confirm'] == 'Awaiting Confirmation'){ ?>

                <div class="col-sm-8 ml-auto mr-auto mt-1 text-center">
                    <h4 class="saved" id="saved"> Status: Awaiting Confirmation</h4>
                    <p class="saved" id="saved">The added information has been sent to your respective Accomodation Provider for confirmation. Please enter later to see your assigned Homestays</p>
                </div>

                <?php } ?>

                <div class="col-md-11 mt-5 ml-auto mr-auto">
                    <button type="submit" class="btn btn_save right">Save</a>
                </div>
            
            </form>
        </div>
    </div>
</main>

<script>
    function previewImage_x() {
        var file = document.getElementById("file-x").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-x");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-x").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-x");
            var label_1_1 = document.getElementById("label-x-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }
    }
</script>
    
</body>
</html>