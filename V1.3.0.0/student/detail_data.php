<?php
// TODO WINYERSON
if ($_POST['homestay_id']) {
  session_start();
  include_once('../xeon.php');
  error_reporting(0);

  // TODO VARIABLES
  $id = $_POST['homestay_id'];
  $usuario = $_SESSION['username'];


  // TODO FUNCIONES
  function homestayDetails($link, $id, $usuario)
  {
    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS
    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row_homestay = $homestayQuery->fetch_assoc();

    $queryAcademy_IJ = $link->query("SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a");
    $row_academy = $queryAcademy_IJ->fetch_assoc();


    $jsonToSend = array(
      'name' => $row_homestay['h_name'],
      'id' => $row_homestay['id_home'],
      'fullname' => $row_homestay['name_h'] . " " . $row_homestay['l_name_h'],
      'gender' => $row_homestay['gender'],
      'status' => $row_homestay['status'],
      'smokePolitics' => $row_homestay['smoke'],
      'description' => $row_homestay['des'],
      'academyName' => $row_academy['name_a'],
      'genderPreference' => $row_homestay['g_pre'],
      'agePreference' => $row_homestay['ag_pre'],
      'address' => $row_homestay['dir'],
      'city' => $row_homestay['city'],
      'state' => $row_homestay['state'],

    );

    echo json_encode($jsonToSend);
  }


  function coordinatorDetails($link, $id, $usuario)
  {
    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS
    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row_homestay = $homestayQuery->fetch_assoc();

    $queryManager_IJ = $link->query("SELECT * FROM manager INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and manager.id_m = pe_student.id_m");
    $row_manager = $queryManager_IJ->fetch_assoc();

    $queryAgents_IJ = $link->query("SELECT * FROM agents INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and agents.id_ag = pe_student.id_ag");
    $row_agent = $queryAgents_IJ->fetch_assoc();

    $queryStudent = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
    $row_student = $queryStudent->fetch_assoc();


    if ($row_student['id_m'] == '0' and $row_student['id_ag'] == '0') {
      $jsonToSend = array(
        'case' => 1,
      );
    } else if ($row_student['id_m'] != '0' and $row_student['id_ag'] == '0') {
      $jsonToSend = array(
        'case' => 2,
        'agentHref' => "agent_info.php?art_id=" . $row_manager['id_m'],
        'agentPhoto' => "../" . $row_manager['photo'],
        'agencyName' => $row_manager['a_name'],
        'agentMail' => $row_manager['mail'],
        'agentNum' => $row_manager['num'],
        'agentType' => 'Manager',
      );
    } else {
      $jsonToSend = array(
        'case' => 3,
        'agentHref' => "agent_info.php?art_id=" . $row_agent['id_ag'],
        'agentPhoto' => "../" . $row_agent['photo'],
        'agencyName' => $row_agent['agency'],
        'agentFullname' => $row_agent['name'] . " " . $row_agent['l_name'],
        'agentMail' => $row_agent['a_mail'],
        'agentNum' => $row_agent['num'],
        'agentType' => 'Coordinator',
      );
    }

    $jsonToSend += array(
      'studentFullname' => $row_student['name_s'] . " " . $row_student['l_name_s'],
      'studentMail' => $row_student['mail_s'],
      'studentMessage' => '#' . $row_homestay['id_home'],
    );

    echo json_encode($jsonToSend);
  }


  function roomsDetails($link, $id, $usuario)
  {
    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS
    $userDataQuery = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
    $userData = $userDataQuery->fetch_assoc();


    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row_homestay = $homestayQuery->fetch_assoc();


    $queryRoom_IJ = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
    $row_room = $queryRoom_IJ->fetch_assoc();


    $queryPhotoHome_IJ = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
    $row_photo_home = $queryPhotoHome_IJ->fetch_assoc();


    $queryStudent = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
    $row_student = $queryStudent->fetch_assoc();


    $queryNotificationLimit = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND title = 'Reservation Request'";
    $notifications_limit = $link->query($queryNotificationLimit);
    $num_rows_notification_limit = mysqli_num_rows($notifications_limit);


    $roomNumber = 1;
    foreach ($row_room as $key => $value) {

      // * ROOM
      if (
        $row_room['type' . $roomNumber] != 'NULL' &&
        $row_room['date' . $roomNumber] != "Disabled" &&
        $row_room['aprox' . $roomNumber] != '0'
      ) {
        if ($roomNumber == 1) $colorRoom = "#232159";
        if ($roomNumber == 2) $colorRoom = "#982A72";
        if ($roomNumber == 3) $colorRoom = "#394893";
        if ($roomNumber == 4) $colorRoom = "#A54483";
        if ($roomNumber == 5) $colorRoom = "#5D418D";
        if ($roomNumber == 6) $colorRoom = "#392B84";
        if ($roomNumber == 7) $colorRoom = "#B15391";
        if ($roomNumber == 8) $colorRoom = "#4F177D";



        $queryNotification = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND color = '$colorRoom' AND user_r = '$row_homestay[mail_h]' AND reserve_h = '$row_homestay[mail_h]' AND confirmed = '0' AND title = 'Reservation Request'";
        $notification = $link->query($queryNotification);
        $row_notification = $notification->fetch_assoc();
        $num_rows_notification = mysqli_num_rows($notification);



        // ** DYNAMIC VARIABLES
        $myReserveDate = null;
        $allReservesDate = null;
        $roomStatus = null;


        // ** ROOM STATUS
        if ($num_rows_notification >= 1) {
          $roomStatus = "Required";
          $requestColorRoom = $row_notification['color'];
        } else if ($num_rows_notification_limit == 3) $roomStatus = 'Reserves Limit';
        else if ($row_room['date' . $roomNumber] == 'Avalible') {
          $roomStatus = "Verify";
          $strQueryEvents = "SELECT * FROM events WHERE color = '$colorRoom'  AND email = '$row_homestay[mail_h]' ";
          $queryEvents = $link->query($strQueryEvents);

          $myReserveDate = array($row_student['firstd'], $row_student['lastd']);

          while ($value = mysqli_fetch_array($queryEvents)) {
            $allReservesDate[] = array($value['title'], $value['start'], $value['end']);
          }
        }


        // ** ROOM DATA
        $jsonToSend[] = array(
          'roomName' => 'Room ' . $roomNumber,
          'roomColor' => $colorRoom,
          'myReserveRequest' => $requestColorRoom,
          'roomPrice' => 'CAD$ ' . $row_room['aprox' . $roomNumber] + $row_room['aprox_a' . $roomNumber],
          'roomImage' => array(
            'image1' => $row_photo_home['proom' . $roomNumber],
            'image2' => $row_photo_home['proom' . $roomNumber . "_2"],
            'image3' => $row_photo_home['proom' . $roomNumber . "_3"],
          ),
          'type' => $row_room['type' . $roomNumber],
          'bed' => $row_room['bed' . $roomNumber],
          'foodService' => $row_room['food' . $roomNumber],
          'status' => $roomStatus,
          'myReserveDate' => $myReserveDate,
          'allReservesDate' => $allReservesDate,
          'aproxPrice' => $row_room['aprox' . $roomNumber],
          'aproxPrice2' => $row_room['aprox_a' . $roomNumber],
          'reservations' => $row_room['reservations' . $roomNumber],
          // HOMESTAY DATA
          'homestayFullName' => $row_homestay['name_h'] . " " . $row_homestay['l_name_h'],
          'homestayMail' => $row_homestay['mail_h'],
          // STUDENT DATA
          'studentFullName' => $row_student['name_s'] . " " . $row_student['l_name_s'],
          'studentFirstDay' => $row_student['firstd'],
          'studentLastDay' => $row_student['lastd'],
        );
      }

      $roomNumber++;
      if ($roomNumber > 8) break;
    }

    echo json_encode($jsonToSend);
  }


  function reservesQuantity($link, $usuario)
  {
    $jsonToSend = null;


    $queryNotificationLimit = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND title = 'Reservation Request'";
    $notifications_limit = $link->query($queryNotificationLimit);
    $num_rows_notification_limit = mysqli_num_rows($notifications_limit);

    $jsonToSend = $num_rows_notification_limit;

    echo json_encode($jsonToSend);
  }

  // TODO RESPUESTAS
  if ($_POST['request'] == 'homestayDetails') homestayDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayCoordinator') coordinatorDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayRooms') roomsDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'reservesQuantity') reservesQuantity($link, $usuario);
  else echo json_encode("No hay ninguna petición");
}