<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

    $query="SELECT * FROM pe_student WHERE mail_s = '$id'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    $query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$id' and academy.id_ac = pe_student.n_a";
    $resultado2=$link->query($query2);

    $row2=$resultado2->fetch_assoc();

    $from = new DateTime($row['db_s']);
    $to   = new DateTime('today');
    $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
    $sufix= " Years old";

    if($age >= '21'){
        $age_s = 'Adult';
    }else {
        $age_s = 'Teenager';
    }


    $query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
    $row5=$query5->fetch_assoc();

    $query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
    $row7=$query7->fetch_assoc();

    $query6 = $link->query("SELECT propertie_control.*, pe_home.*, events.*, academy.id_ac, academy.name_a FROM propertie_control INNER JOIN pe_home ON propertie_control.agency = '$row[id_m]' AND propertie_control.id_home = pe_home.id_home INNER JOIN academy ON academy.id_ac = pe_home.a_pre LEFT OUTER JOIN events ON pe_home.mail_h = events.email GROUP BY propertie_control.id_home");
    $row6=$query6->fetch_assoc();

    $query8 = $link->query("SELECT propertie_control.*, pe_home.*, events.* FROM propertie_control INNER JOIN pe_home ON propertie_control.agency = '$row[id_m]' AND propertie_control.id_home = pe_home.id_home INNER JOIN events ON pe_home.mail_h = events.email");
    $row8=$query8->fetch_assoc();

 if ($row5['usert'] != 'Manager') {
        
         header("location: ../logout.php");   
        
    }

if ($row7['id_m'] != $row['id_m']) {
            header("location: index.php");   
        
    }
}
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/assigment3.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Profile</title>

</head>
<body>

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">

        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">

                <div class="ts-title">
                    <h1>Homestay Assignment</h1>
                </div>

            </div>
        </section>

        <!--AGENCIES LIST
            =========================================================================================================-->
        <section id="agencies-list">
            <div class="container">

                <!--AGENCIES
                    =================================================================================================-->
                <section id="agencies">

                    <!--Agency-->
                    <div class="card ts-item ts-item__list ts-item__company ts-card">

                        <!--Card Image-->
                        <div class="card-img">
                            <?php
                                
                                if($row['photo_s'] == 'NULL'){    
                                    }
                                else {
                                echo'<img id="student" src="../'.$row['photo_s'].'">';
                                }
                            ?>
                        </div>

                        <!--Card Body-->
                        <div class="card-body">

                            <figure class="ts-item__info">
                                <?php
                                    
                                    if($row['name_s'] == 'NULL'){    
                                        }
                                    else {
                                    echo'<h4>'.$row['name_s'].' '.$row['l_name_s'].'</h4>';
                                    }
                                ?>
                                <aside>
                                <?php
                                        
                                        if($row2['nacionality'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-map-marker mr-2"></i>
                                        '.$row2['nacionality'].'';
                                        }
                                    ?>
                                </aside>
                            </figure>

                            <div class="ts-company-info">

                                <div class="ts-company-contact mb-2 mb-sm-0">
                                    <div>
                                    <?php
                                        
                                        if($row2['name_a'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-university mr-2"></i>
                                        <span>'.$row2['name_a'].', '.$row2['dir_a'].'</span>';
                                        }
                                    ?>
                                    </div>

                                    <div>
                                    <?php
                                        
                                        if($row2['firstd'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-calendar"></i>
                                        <span>'.$row2['firstd'].'</span>';
                                        }
                                    ?>
                                    </div>
                                    <div>
                                    <?php
                                        
                                        if($row2['lastd'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-check"></i>
                                        <span>'.$row2['lastd'].'</span>';
                                        }
                                    ?>
                                    </div>

                                    <div>
                                    <?php
                                        
                                        if($row['gen_s'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-venus-mars"></i>
                                        <span id="gender">'.$row['gen_s'].'</span>';
                                        }
                                    ?>    
                                    </div>

                                    <div>
                                    <?php
                                        
                                        if($row['db_s'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<i class="fa fa-child"></i>
                                        <span id="age">'.$row['db_s'].'</span>';
                                        echo $age . $sufix;
                                        }
                                    ?>    
                                    </div>
                                </div>

                                <div class="ts-description-lists">
                                    <dl>
                                    <?php
                                        
                                        if($row['smoke_s'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<dt>Smoke</dt>
                                            <dd>'.$row['smoke_s'].'</dd>';
                                        }
                                    ?> 
                                    </dl>
                                    <dl>
                                    <?php
                                        
                                        if($row['pets'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<dt>Pets</dt>
                                            <dd>'.$row['pets'].'</dd>';
                                        }
                                    ?> 
                                    </dl>
                                    <dl>
                                    <?php
                                        
                                        if($row['food'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<dt>Food</dt>
                                            <dd>'.$row['food'].'</dd>';
                                        }
                                    ?> 
                                    </dl>
                                    <?php
                                        
                                        if($row['vegetarians'] == 'no'){      
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>Vegetarian</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['halal'] == 'no'){    
                                  
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>Halal</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['kosher'] == 'no'){    
                                              
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>Kosher (Jews)</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['lactose'] == 'no'){    
                                              
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>Lactose Intolerant</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['gluten'] == 'no'){    
                                              
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>Gluten Free Diet</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['pork'] == 'no'){    
                                               
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>No Pork</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                     <?php
                                        
                                        if($row['none'] == 'no'){    
                                             
                                        }
                                        else {
                                        echo'<dl>
                                        <dt>None</dt>
                                            <dd>Yes</dd>
                                            </dl>';
                                        }
                                    ?> 
                                </div>

                            </div>
                        </div>

                        <!--Card Footer-->
                        <div class="card-footer">
                            
                        </div>

                    </div>
                    <!--end agency-->

                    <form id="form-submit" class="ts-form" autocomplete="off" action="assignament.php" method="post" enctype="multipart/form-data">
                        <br />
                        <div class="card">
                            <div class="card-body">
                            <p>Select until 8 Homestay for the first student filter</p>
                                <?php
                                        
                                        if($row['id_student'] == 'NULL'){    
                                            }
                                        else {
                                        echo'<input type="text" value="'.$row['id_student'].'" name="id_student" hidden>';
                                        echo'<input type="text" value="'.$row7['mail'].'" name="a_mail"hidden>';
                                        echo'<input type="text" value="'.$row7['id_m'].'" name="id_m" hidden>';
                                        echo'<input type="text" value="'.$row['mail_s'].'" name="mail_s" hidden>';
                                        }
                                    ?> 
                                
                <!--Row-->
                <div class="row">
                                <?php
                    while($row6=mysqli_fetch_array($query6)){
                        //Only shows to me the houses who has the gender preference like the gender of student or any gender
                        if ($row6['g_pre'] == 'Any' OR $row6['g_pre'] == $row['gen_s']){
                            //Only shows to me the houses who has the age preference like the age of student or any age
                            if($row6['ag_pre'] == 'Any' OR $row6['ag_pre'] == $age_s){
                                if($row6['certified'] == 'Yes'){
                                $id_home = $row6['mail_h'];
                                echo '<tr> 
                            <!--Item 2-->
                            <div class="col-sm-6 col-lg-3">
                                <div class="card ts-item ts-card">
                                    <!--Card Image-->
                                    <div class="card-img ts-item__image" data-bg-image="../'.$row6['photo'].'">
                                        <figure class="ts-item__info">
                                            <h4>'.$row6['h_name'].'</h4>
                                        </figure>
                                        <div class="ts-item__info-badge">CAD$ 120</div>
                                    </div>
        
                                    <!--Card Body-->
                                    <div>
                                    <div class="card-body ts-item__body">
                                        <div class="ts-description-lists">
                                            <dl>
                                                <dt>Gender</dt>
                                                <dd>'.$row6['g_pre'].'</dd>
                                            </dl>
                                           
                                            <dl>
                                                <dt>Age</dt>
                                                <dd>'.$row6['ag_pre'].'</dd>
                                            </dl>
        
                                            <dl>
                                                <dt>Bedrooms</dt>
                                                <dd>'.$row6['room'].'</dd>
                                            </dl>
        
                                            <dl>
                                                <dt>Food Service</dt>
                                                <dd>'.$row6['food_s'].'</dd>
                                            </dl>
        
                                            <br />
                                            <dl>
                                                <dt>Direction</dt>
                                                <dd>'.$row6['dir'].', '.$row6['city'].' '.$row6['state'].'</dd>
                                            </dl>
        
                                            <br />
        
                                            <br />
                                                <button type="button" id="submit2" class="btn btn-info btn-sm" data-toggle="collapse" href="#id'.$row6['id_home'].'">More Info</button>
                                                <div id="id'.$row6['id_home'].'" class="collapse">
                                                    <br>
                                                    <dl>
                                                    <dt>Pets</dt>
                                                    <dd>'.$row6['pet'].'</dd>
                                                    </dl>
        
                                                    <dl>
                                                    <dt>Smoke Politics</dt>
                                                    <dd>'.$row6['smoke'].'</dd>
                                                    </dl>
                                                    <br>
                                                    <dl>
                                                    <dt>Special Diet</dt>
                                                    </dl>';
        
                                                    if($row6['vegetarians'] == "yes"){
                                                        echo "<br><li class='fa fa-check'> Vegetarians</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['halal'] == "yes"){
                                                        echo "<br><li class='fa fa-check' > Halal</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['kosher'] == "yes"){
                                                        echo "<br><li class='fa fa-check' > Kosher (Jews)</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['lactose'] == "yes"){
                                                        echo "<br><li class='fa fa-check' > Lactose Intolerant</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['gluten'] == "yes"){
                                                        echo "<br><li class='fa fa-check' > Gluten Free Diet</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['pork'] == "yes"){
                                                        echo "<br><li class='fa fa-check' > No Pork</li>";
                                                        }
                                                    else {}
        
                                                    if($row6['none'] == "yes"){
                                                        echo "<br><li class='fa fa-check'> None</li><br>";
                                                        }
                                                    else {}

                                                    if ($row8 == true) {
                                                        echo '
                                                        <dl>
                                                        <dt>Reservations</dt>
                                                        </dl><br>';
                                                    }

                                                    while($row8=mysqli_fetch_array($query8)){
                                                        if($row8['room_e'] == 'room1'){
                                                            $room = 'Room 1:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room2'){
                                                            $room = 'Room 2:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room3'){
                                                            $room = 'Room 3:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room4'){
                                                            $room = 'Room 4:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room5'){
                                                            $room = 'Room 5:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room6'){
                                                            $room = 'Room 6:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room7'){
                                                            $room = 'Room 7:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room8'){
                                                            $room = 'Room 8:';
                                                        }else{}
                                                        if($row8['room_e'] == 'room'){
                                                            $room = 'Other Activity:';
                                                        }else{} 
                                                        echo '
                                                        <div class="ts-description-lists">
                                                        <div class="text-left">
                                                        <table class="table-hover">
                                                        <tr><th id="top">'.$room.' '.$row8['title'].'</th></tr>
                                                        <tr>
                                                        <td>'.$row8['start'].' | '.$row8['end'].'</td>
                                                        <br>
                                                        </tr>
                                                        </table>
                                                        </div>
                                                        </div>';
                                                    }
                                                    
                                            echo '
                                                </div>
                                                </div>
        
                                        </div>
                                    </div>
        
                                    <!--Card Footer-->
                                    <div class="card-footer">
                                        <input type="checkbox" value="'.$row6['id_home'].'" name="n_a[]">
                                        Select this Homestay
                                    </div>
        
                                </div>
                                <!--end ts-item ts-card-->
                            </div>
                            <!--end Item col-md-4--></tr> ';

                            }else{}
                            }else{}

                        } else {}

                        
				        }
			                ?>
                                </div>
                                <div class="col text-center">
                                    <button type="submit" id="submit" name="register" onclick="window.location=''" class="btn btn-primary ts-btn-arrow btn-lg float-center">
                                        Assignment Homestay
                                
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </section>
                <!--end #agencies-->
    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>

</body>
</html>

