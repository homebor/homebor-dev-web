$(document).ready(function(){

  load_data(1);

  function load_data(page, query = '')
  {
    $.ajax({
      url:"fetch_students.php",
      method:"POST",
      data:{page:page, query:query},
      success:function(data)
      {
        $('#dynamic_content').html(data);
      }
    });
  }

  $('#vou').hide();

  $('#search_box').keyup(function() {

      if ($('#search_box').val()) {

          $('#dynamic_content').hide();
          $('#vou').show();

          let search = $('#search_box').val();

          var searchLength = search.length;

          $.ajax({
              url: 'stua-search.php',
              type: 'POST',
              data: { search, },
              success: function(response) {
                  let students = JSON.parse(response);
                  let template = '';

                  students.forEach(student => {
                    if(student.photo_s != 'NULL' && student.status == 'Waiting Answer'){
                      template += `
          <tr>
            <th class="align-middle text-center" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.names} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.gen_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.mail_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.nacionality} </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.status} Confirmation <a class="mail_reserve" href="detail.php?art_id=${student.id_home}">${student.mail_h}</a></b> </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b>${student.firstd} / ${student.lastd} </b><br> ${student.week}</th>
            <th class="align-middle text-center" align="left">
            <ul>
                <a href="student_info.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                    <i class="fa fa-eye mr-2"></i>
                                    Preview
                                </a>
            <br>
            <a id="edit" href="student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                            <i class="fa fa-id-card"></i>      
                                            Edit Student
                                              </a>
            <br>
            <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                              <i class="fa fa-pencil-alt mr-2"></i>      
                                Edit Assigment
                                </a>
            <br>
            <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                                    <i class="fa fa-list"></i>
                                  Assignation
                                </a>
            
              </ul> 
            </th>

          </tr>`
                    } else
                    if(student.photo_s == 'NULL' && student.status == 'Waiting Answer'){
                      template += `
          <tr>
            <th class="align-middle text-center" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.names} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.gen_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.mail_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.nacionality} </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.status} Confirmation <a class="mail_reserve" href="detail.php?art_id=${student.id_home}">${student.mail_h}</a></b> </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.firstd} / ${student.lastd} </b><br> ${student.week}</th>
            <th class="align-middle text-center" align="left">
            <ul>
      <a href="student_info.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                          <i class="fa fa-eye mr-2"></i>
                          Preview
                      </a>
  <br>
  <a id="edit" href="student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                   <i class="fa fa-id-card"></i>      
                                   Edit Student
                                     </a>
  <br>
  <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                    <i class="fa fa-pencil-alt mr-2"></i>      
                       Edit Assigment
                      </a>
  <br>
  <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                          <i class="fa fa-list"></i>
                        Assignation
                      </a>
  
    </ul> 
            </th>

          </tr>`
                    } else
                    if(student.photo_s != 'NULL' && student.status != 'Waiting Answer'){
                      template += `
          <tr>
            <th class="align-middle text-center" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.names} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.gen_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.mail_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.nacionality} </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.status} </b> </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.firstd} / ${student.lastd} </b><br> ${student.week}</th>
            <th class="align-middle text-center" align="left">
            <ul>
      <a href="student_info.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                          <i class="fa fa-eye mr-2"></i>
                          Preview
                      </a>
  <br>
  <a id="edit" href="student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                   <i class="fa fa-id-card"></i>      
                                   Edit Student
                                     </a>
  <br>
  <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                    <i class="fa fa-pencil-alt mr-2"></i>      
                       Edit Assigment
                      </a>
  <br>
  <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                          <i class="fa fa-list"></i>
                        Assignation
                      </a>
  
    </ul> 
            </th>

          </tr>`
                    } else
                    if(student.photo_s == 'NULL' && student.status != 'Waiting Answer'){
                      template += `
          <tr>
            <th class="align-middle text-center" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.names} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.gen_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.mail_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.nacionality} </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.status} </b> </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.firstd} / ${student.lastd} </b><br> ${student.week}</th>
            <th class="align-middle text-center" align="left">
            <ul>
            <a href="student_info.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                <i class="fa fa-eye mr-2"></i>
                                Preview
                            </a>
        <br>
        <a id="edit" href="student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                         <i class="fa fa-id-card"></i>      
                                         Edit Student
                                           </a>
        <br>
        <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                          <i class="fa fa-pencil-alt mr-2"></i>      
                             Edit Assigment
                            </a>
        <br>
        <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                                <i class="fa fa-list"></i>
                              Assignation
                            </a>
        
          </ul> 
            </th>

          </tr>`
                    } else {
                      template += `
          <tr>

            <th class="align-middle text-center" style="font-weight:normal"></th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.names} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.gen_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.mail_s} </th>
            <th class="align-middle text-center" style="font-weight:normal"> ${student.n_a} </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.status} </b> </th>
            <th class="align-middle text-center" style="font-weight:normal"> <b> ${student.firstd} / ${student.lastd} </b><br> ${student.week}</th>
            <th class="align-middle text-center" align="left">
            <ul>
            <a href="student_info.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                <i class="fa fa-eye mr-2"></i>
                                Preview
                            </a>
        <br>
        <a id="edit" href="student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                         <i class="fa fa-id-card"></i>      
                                         Edit Student
                                           </a>
        <br>
        <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                          <i class="fa fa-pencil-alt mr-2"></i>      
                             Edit Assigment
                            </a>
        <br>
        <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                                <i class="fa fa-list"></i>
                              Assignation
                            </a>
        
          </ul>   
            </th>

          </tr>`
                    }
                      
                  });


                  $('#vouchers').html(template);
              }
          });
      } else {
          $('#dynamic_content').show();
          $('#vou').hide();
      }

  });

  $(document).on('click', '.page-link', function() {
    var page = $(this).data('page_number');
    var query = $('#search_box').val();
    load_data(page, query);
  });

});