var room_title = `<h2 class="title__group-section" id="title__bedrooms"> Bedrooms Information </h2>`;


var room1 = `

    <!-- Room 1 -->
    <div class="tarjet tarjet-rooms " id="room1">
                
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>

                <div class="div-img-r" align="center">

                    <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                            <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner">

                            <div class="carousel-item active">
                            <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>

                                <img id="preview-xi" class="add-photo d-none" >

                                <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">


                                <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>

                            </div>

                            <div class="carousel-item">
                            <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>

                                <img id="preview-xi-ii" class="add-photo d-none" >

                                <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">


                                <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>

                            </div>

                            <div class="carousel-item">

                                <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>

                                <img id="preview-xi-iii" class="add-photo d-none" >

                                <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >


                                <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>

                            </div>

                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>

                    </div>

                </div><br>


                <div class="row init">

                    <div class="details in-d">

                        <div class="icon-type">
                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                        </div>

                        <select name="type1" class="fo custom-select" id="color" style="padding: 5px;">
                            <option disabled selected> Select</option>
                            <option value="Single">Single</option>
                            <option value="Share">Share</option>
                            <option value="Executive">Executive</option>
                        </select>

                    </div>

                    <div class="details">
                                            
                        <div class="icon-type">
                            <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                        </div>

                        <select name="bed1" class="fo custom-select" id="color" style="padding: 5px;">
                            <option disabled selected> Select</option>
                            <option value="Twin">Twin</option>
                            <option value="Double">Double</option>
                            <option value="Bunker">Bunker </option>
                                            
                        </select>

                    </div>

                </div><br>


                <div class="row init">

                    <div class="details in-d">

                        <div class="icon-type">
                            <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                        </div>

                        <select name="date1" class="fo custom-select" id="color" style="padding: 5px;">
                            <option disabled selected> Select</option>
                            <option value="Avalible" >Avalible</option>

                            <option value="Occupied" >Occupied</option>
                                                
                            <option value="Disabled" >Disabled</option>
                                            
                        </select>

                    </div>

                    <div class="details">
                                            
                        <div class="icon-type">
                            <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                        </div>

                        <select name="food1" class="fo custom-select" id="color" style="padding: 5px;">
                            <option disabled selected> Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                                            
                        </select>

                    </div>

                    <div class="row" style=" margin-top: 10%;">

                        <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                            <div style="display: flex;">
                                                
                                <div class="col-lg-6">
                                                
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">


                                </div>

                                <div class="col-lg-6">
                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">

                                </div>

                            </div>

                        </div>

                    </div>


                </div><br>


            </div>


`;


var room2 = `

    <!-- Room2 -->

    <div class="tarjet tarjet-rooms">
     
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>

        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                    <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>

                        <img id="preview-xii" class="add-photo d-none" >

                        <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">


                        <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>

                        <img id="preview-xii-ii" class="add-photo d-none" >

                        <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">


                        <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>

                        <img id="preview-xii-iii" class="add-photo d-none" >

                        <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">


                        <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div><br>


        <div class="row init">

            <div class="details in-d">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                </div>

                <select name="type2" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Single">Single</option>
                    <option value="Share">Share</option>
                    <option value="Executive">Executive</option>
                                
                </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                                
                </select>

            </div>

        </div><br>


        <div class="row init">

            <div class="details in-d">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date2" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                                
                </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food2" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                                
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                    
                    <div style="display: flex;">
                                                
                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                            
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>
               

    </div>

`;

var room3 = `

    <div class="tarjet tarjet-rooms">
                
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>


        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
            </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                    <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>

                        <img id="preview-xiii" class="add-photo d-none" >

                        <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">


                        <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>

                        <img id="preview-xiii-ii" class="add-photo d-none" >

                        <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >


                        <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>

                        <img id="preview-xiii-iii" class="add-photo d-none" >

                        <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >


                        <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

                </div>

                            
            </div><br>


            <div class="row init">

                <div class="details in-d">
                                
                    <div class="icon-type">
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                    </div>

                    <select name="type3" class="fo custom-select" id="color" style="padding: 5px;">
                        <option disabled selected> Select </option>
                        <option value="Single">Single</option>
                        <option value="Share">Share</option>
                        <option value="Executive">Executive</option>
                                
                    </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed3" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option></option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                                
                </select>

            </div>

        </div><br>


        <div class="row init">
                            
            <div class="details in-d">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date3" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                                
                </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food3" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                                
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                    
                    <div style="display: flex;">
                                                
                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                            
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>
                           

    </div>

`;

var room4 = `

    <div class="tarjet tarjet-rooms">
            
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>

        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                    <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                    <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>

                        <img id="preview-xiv" class="add-photo d-none" >

                        <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >


                        <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>

                        <img id="preview-xiv-ii" class="add-photo d-none" >

                        <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">


                        <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">
                    <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>

                        <img id="preview-xiv-iii" class="add-photo d-none" >

                        <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >


                        <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

                        
        </div><br>


        <div class="row init">

            <div class="details in-d">
                            
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                </div>

                <select name="type4" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Single">Single</option>
                    <option value="Share">Share</option>
                    <option value="Executive">Executive</option>
                            
                </select>

            </div>

            <div class="details">
                            
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed4" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option></option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                            
                </select>

            </div>

        </div><br>


        <div class="row init">
                        
            <div class="details in-d">
                            
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date4" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                            
                </select>

            </div>

            <div class="details">
                            
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food4" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                            
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                    
                    <div style="display: flex;">
                                                
                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                            
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>
                           

    </div>

`;

var room5 = `

    <div class="tarjet tarjet-rooms ">
                
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>

        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
            </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">

                        <label for="file-xv" class="photo-add" id="label-xv"> Bedroom Photo 1</label>

                        <img id="preview-xv" class="add-photo d-none" >

                        <input type="file" name="bed-v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none">


                        <label for="file-xv" class="add-photo-i fa fa-pencil-alt" id="label-xv-i" style="display: none" title="Change Bedroom Photo">  </label>


                    </div>

                    <div class="carousel-item">

                    <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> Bedroom Photo 2</label>
                        <img id="preview-xv-ii" class="add-photo d-none" >

                        <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none">

                        <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                    <div class="carousel-item">

                    <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> Bedroom Photo 3</label>
                        <img id="preview-xv-iii" class="add-photo d-none" >

                        <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" 
                                >


                        <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div><br>


        <div class="row init">

            <div class="details in-d">
                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                </div>

                <select name="type5" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Single">Single</option>
                    <option value="Share">Share</option>
                    <option value="Executive">Executive</option>
                
            </select>

            </div>

            <div class="details">
                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed5" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option></option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                
                </select>

            </div>

        </div><br>


        <div class="row init">
            
            <div class="details in-d">
                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date5" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                
                </select>

            </div>

            <div class="details">
                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food5" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                    
                    <div style="display: flex;">
                                                
                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                            
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>                                                
    </div>
`;

var room6 = `

    <div class="tarjet tarjet-rooms">
        
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 6 </h4>

        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                    <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">

                        <label for="file-xvi" class="photo-add" id="label-xvi"> Bedroom Photo 1 </label>

                        <img id="preview-xvi" class="add-photo d-none" >

                        <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >


                        <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" style="display: none" title="Change Bedroom Photo">  </label>


                    </div>

                    <div class="carousel-item">

                    <label for="file-xvi-ii" class="photo-add" id="label-xvi-ii"> Bedroom Photo 2 </label>
                        <img id="preview-xvi-ii" class="add-photo d-none" >

                        <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >


                        <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">

                    <label for="file-xvi-iii" class="photo-add" id="label-xvi-iii"> Bedroom Photo 3 </label>
                        <img id="preview-xvi-iii" class="add-photo d-none" >

                        <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >


                        <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

                        
        </div><br>


        <div class="row init">

            <div class="details in-d">
                        
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                </div>

                <select name="type6" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Single">Single</option>
                    <option value="Share">Share</option>
                    <option value="Executive">Executive</option>
                          
                </select>

            </div>

            <div class="details">
                        
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed6" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option></option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                          
                </select>

            </div>

        </div><br>


        <div class="row init">
                    
            <div class="details in-d">
                        
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date6" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                          
                </select>

            </div>

            <div class="details">
                        
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food6" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                          
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                    
                    <div style="display: flex;">
                                                
                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                    
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx6" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                            
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                    
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a6" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>                                                
    </div>

`;

var room7 = `

    <div class="tarjet tarjet-rooms">
            
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 7 </h4>

            <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>
                        <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">

                        <div class="carousel-item active">

                            <label for="file-xvii" class="photo-add" id="label-xvii"> Bedroom Photo 1 </label>

                            <img id="preview-xvii" class="add-photo d-none" >

                            <input type="file" name="bed-vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >


                            <label for="file-xvii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-i" style="display: none" title="Change Bedroom Photo">  </label>


                        </div>

                        <div class="carousel-item">

                        <label for="file-xvii-ii" class="photo-add" id="label-xvii-ii"> Bedroom Photo 2 </label>
                            <img id="preview-xvii-ii" class="add-photo d-none" >

                            <input type="file" name="bed-vii-ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >


                            <label for="file-xvii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                        </div>

                        <div class="carousel-item">

                        <label for="file-xvii-iii" class="photo-add" id="label-xvii-iii"> Bedroom Photo 3 </label>
                            <img id="preview-xvii-iii" class="add-photo d-none" >

                            <input type="file" name="bed-vii-iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >


                            <label for="file-xvii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                        </div>

                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>

                    <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>

                        
            </div><br>


            <div class="row init">

                <div class="details in-d">
                            
                    <div class="icon-type">
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                    </div>

                    <select name="type7" class="fo custom-select" id="color" style="padding: 5px;">
                        <option disabled selected> Select </option>
                        <option value="Single">Single</option>
                        <option value="Share">Share</option>
                        <option value="Executive">Executive</option>
                            
                    </select>

                </div>

                <div class="details">
                            
                    <div class="icon-type">
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                    </div>

                    <select name="bed7" class="fo custom-select" id="color" style="padding: 5px;">
                        <option disabled selected> Select </option></option>
                        <option value="Twin">Twin</option>
                        <option value="Double">Double</option>
                        <option value="Bunker">Bunker </option>
                            
                    </select>

                </div>

            </div><br>


            <div class="row init">
                        
                <div class="details in-d">
                            
                    <div class="icon-type">
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                    </div>

                    <select name="date7" class="fo custom-select" id="color" style="padding: 5px;">
                        <option disabled selected> Select </option>
                        <option value="Avalible" >Avalible</option>

                        <option value="Occupied" >Occupied</option>

                        <option value="Disabled" > Disabled </option>
                            
                    </select>

                </div>

                <div class="details">
                            
                    <div class="icon-type">
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                    </div>

                    <select name="food7" class="fo custom-select" id="color" style="padding: 5px;">
                        <option disabled selected> Select </option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                            
                    </select>

                </div>

                <div class="row" style=" margin-top: 10%;">

                    <div class="container" style="width: 95%; margin-left: -7%;">
                                                        
                        <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                        
                        <div style="display: flex;">
                                                    
                            <div class="col-lg-6">
                                                    
                                <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                        
                                <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();" name="approx7" maxlength="15">


                            </div>

                            <div class="col-lg-6">
                                                
                                <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                        
                                <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();" name="approx_a7" maxlength="15">

                            </div>

                        </div>

                    </div>

                </div>

            </div><br>                                                
        </div>

`;

var room8 = `

    <div class="tarjet tarjet-rooms">
                
        <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 8 </h4>

        <div class="div-img-r" align="center">

            <div id="carouselExampleIndicators-8" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-8" data-slide-to="0" class="active"> </li>
                    <li data-target="#carouselExampleIndicators-8" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-8" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">

                    <label for="file-xviii" class="photo-add" id="label-xviii"> Bedroom Photo 1</label>
                        <img id="preview-xviii" class="add-photo d-none" >

                        <input type="file" name="bed-viii" id="file-xviii" accept="image/*" onchange="previewImage_xviii();" style="display: none" >


                        <label for="file-xviii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">

                        <label for="file-xviii-ii" class="photo-add" id="label-xviii-ii"> Bedroom Photo 2 </label>

                        <img id="preview-xviii-ii" class="add-photo d-none" >

                        <input type="file" name="bed-viii-ii" id="file-xviii-ii" accept="image/*" onchange="previewImage_xviii_ii();" style="display: none" >


                        <label for="file-xviii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                    <div class="carousel-item">

                    <label for="file-xviii-iii" class="photo-add" id="label-xviii-iii"> Bedroom Photo 3 </label>
                        <img id="preview-xviii-iii" class="add-photo d-none" >

                        <input type="file" name="bed-viii-iii" id="file-xviii-iii" accept="image/*" onchange="previewImage_xviii_iii();" style="display: none" >


                        <label for="file-xviii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-8" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-8" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

                        
        </div><br>


        <div class="row init">

            <div class="details in-d">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                </div>

                <select name="type8" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Single">Single</option>
                    <option value="Share">Share</option>
                    <option value="Executive">Executive</option>
                                
                </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                </div>

                <select name="bed8" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option></option>
                    <option value="Twin">Twin</option>
                    <option value="Double">Double</option>
                    <option value="Bunker">Bunker </option>
                                
                </select>

            </div>

        </div><br>


        <div class="row init">
                            
            <div class="details in-d">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                </div>

                <select name="date8" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Avalible" >Avalible</option>

                    <option value="Occupied" >Occupied</option>

                    <option value="Disabled" > Disabled </option>
                                
                </select>

            </div>

            <div class="details">
                                
                <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                </div>

                <select name="food8" class="fo custom-select" id="color" style="padding: 5px;">
                    <option disabled selected> Select </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                                
                </select>

            </div>

            <div class="row" style=" margin-top: 10%;">

                <div class="container" style="width: 95%; margin-left: -7%;">
                                                        
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD $ </p>
                        
                    <div style="display: flex;">
                                                    
                        <div class="col-lg-6">
                                                    
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>
                                        
                            <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction8();" name="approx8" maxlength="15">


                        </div>

                        <div class="col-lg-6">
                                                
                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>
                                                        
                            <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction8_2();" name="approx_a8" maxlength="15">

                        </div>

                    </div>

                </div>

            </div>

        </div><br>                                                
    </div>

`;


function viewBed(){
    
    if($('#room').val() == '1'){

                       
        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 

        total += `
        </div>`;

    }else if($('#room').val() == '2'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;

        total += `
        </div>`;


    }else if($('#room').val() == '3'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;

        total += `
        </div>`;

    }else if($('#room').val() == '4'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;
            total += room4;

        total += `
        </div>`;

    }else if($('#room').val() == '5'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;
            total += room4;
            total += room5;

        total += `
        </div>`;

    }else if($('#room').val() == '6'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;
            total += room4;
            total += room5;
            total += room6;

        total += `
        </div>`;

    }else if($('#room').val() == '7'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;
            total += room4;
            total += room5;
            total += room6;
            total += room7;

        total += `
        </div>`;

    }else if($('#room').val() == '8'){

        total = room_title + `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

            total += room1; 
            total += room2;
            total += room3;
            total += room4;
            total += room5;
            total += room6;
            total += room7;
            total += room8;

        total += `
        </div>`;

    }

    $('.bedrooms').html(total);
}
