 <?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM academy");
$row6=$query6->fetch_assoc();


 if ($row5['usert'] != 'Manager') {
         header("location: ../logout");   
    }

$query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario'");
$row7=$query7->fetch_assoc();

if ($row7['mail'] == $usuario) {
    header("location: index");   
}

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/profile.css">

    <!-- REFERENCE OF MAPBOX API -->
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js"></script>
    <link
    rel="stylesheet"
    href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
    type="text/css"
    />
    <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Manager Profile</title>

</head>
<body>

<header id="ts-header" class="fixed-top" style="background-color: #232159;">
        <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light" style="background-color: #232159; box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -webkit-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75);">
            <div class="container">

                <!--Brand Logo/ Logo HOMEBOR-->
                <a class="navbar-brand" href="index.php" style="margin-left:0">
                    <img src="../assets/logos/white.png" class="logo_white" alt="" >
                </a>

                <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        &nbsp;&nbsp;<li class="nav-item" style="list-style: none;">
                            <a class="nav-link zoom logout" href="../logout.php" id="nav" style="color:#fff">Logout</a>
                        </li>


                </ul>

            </div>

        </nav>

</header>

<div class="container_register">

    <main class="card__register">
        <form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">
            
            <h1 class="title__register">Register Your Accommodation Provider</h1>
            
            <div class="row m-0 p-0">

                <div class="col-md-4 column-1">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Basic Provider Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">
                                <img class="form__group__photo" id="preview" src="" alt="">

                                <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">


                                <label for="file" class="photo-add" id="label-i"> <p class="form__group-l_title"> Add Profile Photo </p> </label>

                                <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;" title="Change Frontage Photo"></label>
                            </div>

                            <div class="info"></div>

                            

                                <!-- Group Name -->

                                <div class="form__group form__group__name" id="group__name">
                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">Name</label>

                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                    
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__l_name" id="group__l_name">
                                    <div class="form__group__icon-input">

                                        <label for="l_name" class="form__label">Last Name</label>

                                        <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="e.g. Smith">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                        
                        
                        </div>

                    </div>


                    <div class="card">


                        <h3 class="form__group-title__col-4">Location</h3>

                        <div class="information__content">
                        
                            <!-- Group Address -->
                            <div class="form__group form__group__dir" id="group__dir">
                                <div class="form__group__icon-input">

                                    <label for="dir" class="form__label">Address</label>

                                    <label for="dir" class="icon"><i class="icon icon__dir"></i></label>
                                    <input type="text" id="dir" name="dir" class="form__group-input dir" placeholder="Av, Street, etc.">
                                    
                                </div>
                                <p class="form__group__input-error">Please enter a valid Address</p>
                            </div>
                            
                            <!-- Group City -->
                            <div class="form__group form__group__city" id="group__city">
                                <div class="form__group__icon-input">

                                    <label for="city" class="form__label">City</label>

                                    <label for="city" class="icon"><i class="icon icon__dir"></i></label>
                                    <input type="text" id="city" name="city" class="form__group-input city2" placeholder="e.g. Toronto">

                                </div>
                                <p class="form__group__input-error">Please enter a valid City</p>
                            </div>
                            
                            <!-- Group State -->
                            <div class="form__group form__group__state" id="group__state">
                                <div class="form__group__icon-input">

                                    <label for="state" class="form__label">State</label>

                                    <label for="state" class="icon"><i class="icon icon__dir"></i></label>
                                    <input type="text" id="state" name="state" class="form__group-input state" placeholder="e.g. Ontario">

                                </div>
                                <p class="form__group__input-error">Please enter a valid State</p>
                            </div>
                            
                            <!-- Group Postal Code -->
                            <div class="form__group form__group__p_code" id="group__p_code">
                                <div class="form__group__icon-input">

                                    <label for="p_code" class="form__label">Postal Code</label>

                                    <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>
                                    <input type="text" id="p_code" name="p_code" class="form__group-input p_code" placeholder="e.g. 145187">

                                </div>
                                <p class="form__group__input-error">Please enter a valid Postal Code</p>
                            </div>


                                

                        </div>

                    </div>


                </div>



                <div class="col-md-8 column-2">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Acomodation Provider Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">
                                
                                <!-- Group Accommodation P. Name -->
                                <div class="form__group col-sm-4 form__group__basic form__group__a_name" id="group__a_name">
                                    <div class="form__group__icon-input">

                                        <label for="a_name" class="form__label">Accommodation P. Name</label>

                                        <label for="a_name" class="icon"><i class="icon icon__a_name"></i></label>
                                        <input type="text" id="a_name" name="a_name" class="form__group-input a_name" placeholder="e.g. Enterprise">

                                    </div>
                                    <p class="form__group__input-error">Write the name correctly</p>
                                </div>
                                
                                <!-- Group Alternative Mail -->
                                <div class="form__group col-sm-4 form__group__basic form__group__mail2" id="group__mail2">
                                    <div class="form__group__icon-input">

                                        <label for="mail2" class="form__label">Alternative Mail</label>

                                        <label for="mail2" class="icon"><i class="icon icon__mail2"></i></label>
                                        <input type="text" id="mail2" name="mail2" class="form__group-input mail2" placeholder="e.g. enterprise@gmail.com">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid email</p>
                                </div>
                                
                                <!-- Group Agency Number -->
                                <div class="form__group col-sm-4 form__group__basic form__group__num" id="group__num">
                                    <div class="form__group__icon-input">

                                        <label for="num" class="form__label">Agency Number</label>

                                        <label for="num" class="icon"><i class="icon icon__num"></i></label>
                                        <input type="text" id="num" name="num" class="form__group-input num" placeholder="e.g. 5557891">

                                    </div>
                                    <p class="form__group__input-error">Enter the number correctly</p>
                                </div>
                                
                                
                                <!-- Group Description -->
                                <div class="form__group col-sm-12 form__group__basic form__group__des" id="group__des">
                                    <div class="form__group__icon-input">

                                        <label for="des" class="form__label label__description">Description</label>

                                        <label for="des" class="icon"><i class="icon icon__des"></i></label>
                                        <textarea type="text" id="des" rows="4" name="des" class="form__group-input des" placeholder="e.g. Describe your Accommodation Provider using a few words"></textarea>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>






                    <div class="card">

                        <h3 class="form__group-title__col-4">Favourites Academies</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <h4 class="title__academies_f">You can only select 5 Academies</h4>

                                <div class="content__academies">
                                <?php
                                    while($row6=mysqli_fetch_array($query6)){
                                        ?>
                                        <tr>
                                            <td ><input type="checkbox" class="mb-2 mr-2" value="<?php echo $row6['id_ac']; ?>" name="n_a[]"></td>
                                            <td><?php echo $row6['name_a']; ?>,</td>
                                            <td><?php echo $row6['dir_a']; ?></td>
                                            <br>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                </div>
                            
                            </div>

                        </div>

                    </div>


                </div>
            </div><br>

            <div class="form__message" id="form__message">
                <p>llene los campos correctamente</p>
            </div>

            <div class="form__group__btn-send" align="center" id="btn__success">
                <button type="submit" class="form__btn" id="btn_">Continue</button>
            </div>
            
            <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>

            

            
        </form>
    </main>

</div>


    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


<?php include 'footer.php' ?>

<script>
                                function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }
                            </script>



<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/validate__register.js"></script>

</body>
</html>