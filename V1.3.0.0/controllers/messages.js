const D = document;
const W = window;

const $container = D.createElement("section");
$container.id = "homebor-messages";
$container.className = "homebor-messages";
$container.innerHTML = `<article><p></p><figure></figure><button id="error-message-ok" class="btn">Accept</button></article>`;

// TODO FUNCIONES
export default class HomeborMessages {
  constructor() {
    this.container = $container;
    this.message = this.container.querySelector("p");
    this.figure = this.container.querySelector("figure");
    this.button = this.container.querySelector("button");
  }

  /**
   * ? MOSTRAR MENSAJE
   * @param {String} text Mensaje a mostrar al usuario (Preferiblemente menos de 20 palabras)
   * @param {Number} type Tipo de mensaje ( 1.-Loading 2.-Success 3.-Error 4.-Error-OK )
   * @param {Number} timeQuit Los segundos para quitar el mensaje por defecto tiene 3 seg, FALSE para no quitar.
   * @param {String} scroll Selector del elemento hacia el cual scrollear por defecto no scrollea, colocar como valor "0" para restaurar el scroll
   */
  showMessage(text, type, timeQuit = 3, scroll = false) {
    if (!text || !type) return;

    this.container.className = "homebor-messages";

    const CLASSES = ["loading", "success", "error", "error-ok"];
    const TYPES = ["loading-message", "success-message", "error-message", "error-message-ok"];

    D.body.style.overflowY = "hidden";
    D.body.appendChild(this.container);

    this.container.classList.add(CLASSES[type - 1]);
    this.message.textContent = text;
    type !== 4 ? (this.figure.id = TYPES[type - 1]) : (this.button.id = "error-message-ok");

    // * HIDE MODAL
    if (typeof timeQuit === "number") setTimeout(() => this.quitMessage(scroll), Number(`${timeQuit}000`));

    // * RESTART SCROLL
    if (scroll === 0) scrollTo(0, 0);
    else if (!scroll) this.button.dataset.scroll = "";
    else this.button.dataset.scroll = scroll;
  }

  // ? QUITAR MENSAJE
  quitMessage(scroll) {
    D.body.style.cssText = "";
    this.container.remove();

    // * SCROLL MODAL TO FINISH MODAL
    if (scroll) D.querySelector(scroll).scrollIntoView({ block: "start", behavior: "smooth" });
  }
}
const INSTANCE = new HomeborMessages();

D.addEventListener("DOMContentLoaded", (e) => INSTANCE.showMessage("Please Wait...", 1, false, false));
D.addEventListener("click", (e) => {
  e.target.id === "error-message-ok" ? INSTANCE.quitMessage(e.target.dataset.scroll) : {};
});
D.addEventListener("keyup", (e) => (e.altKey && e.ctrlKey && e.key === "|" ? INSTANCE.quitMessage() : {}));
