 <?php
  include 'xeon.php';
  include 'cript.php';
  session_start();
  error_reporting(0);

  // Almacenando la variable de sesión
  $usuario = $_SESSION['username'];

  if(empty($usuario)) header("location: index");


  //Solicitud para mostrar solo la información y configuración del usuario en sesion
  $query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
  $row5 = $query5->fetch_assoc();

  $query6 = $link->query("SELECT * FROM academy");
  $row6 = $query6->fetch_assoc();

  $query7 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario' ");
  $row7 = $query7->fetch_assoc();


  ?>
 <!doctype html>
 <html lang="en">

 <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="author" content="ThemeStarz">

   <!--CSS -->
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
   <link rel="stylesheet" href="assets/css/leaflet.css">
   <link rel="stylesheet" href="assets/css/style.css">
   <link rel="stylesheet" type="text/css" href="assets/css/ihomestay_student_agency.css?ver=1.2">

   <!-- Favicons -->
   <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
   <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
   <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
   <link rel="manifest" href="/site.webmanifest">
   <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
   <meta name="msapplication-TileColor" content="#da532c">
   <meta name="theme-color" content="#ffffff">

   <title>Student Register</title>

   <script src="assets/js/jquery-3.3.1.min.js"></script>
 </head>

 <body>

   <header id="ts-header" class="fixed-top" style="background-color: #fff;">
     <link rel="stylesheet" type="text/css" href="assets/css/header.css">
     <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
     <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light"
       style="background-color: #fff; box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -webkit-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 13px 0px rgba(0,0,0,0.75);">
       <div class="container">
         <!--Brand Logo/ Logo HOMEBOR-->
         <a class="navbar-brand" href="index.php" style="margin-left:0">
           <img src="assets/iHomestay/logos/iHomestay_logo.png" class="ihomestay_logo" alt="">
         </a>
         <ul class="navbar-nav ml-auto"></ul>
       </div>
     </nav>
   </header>


   <!-- // TODO CONTAINER REGISTER -->
   <div class="container_register">
     <div class="root d-block">

       <!-- // TODO STEP'S PANEL -->
       <div class="form-register__header">
         <ul class="progressbar">
           <li class="progressbar__option active color"> Basic Information</li>
           <li class="progressbar__option"> Personal Information </li>
           <li class="progressbar__option"> House Preference </li>
           <li class="progressbar__option"> Student Preference </li>
           <li class="progressbar__option"> Terms and Conditions </li>
         </ul>
       </div>

       <input type="hidden" id="id_student" value="<?php echo $row7['id_student'] ?>">

       <!-- // TODO STEP'S CONTAINER -->
       <div class="form-register__body">

         <!-- // TODO Step 1 -->
         <div class="step active" id="step-1">

           <form action="#" method="POST" class="form__register" id="form__register" autocomplete="off"
             enctype="multipart/form-data">
             <div class="row row_step">

               <!-- // TODO FIRST COLUMN -->
               <div class="col-md-4">
                 <div class="card">
                   <div class="step__header">
                     <h2 class="title__card">Basic Information</h2>
                   </div>

                   <div class="step__body">
                     <!-- // TODO PROFILE PHOTO -->
                     <div class="form__group__div__photo">
                       <img class="form__group__photo" id="preview" src="" alt="">

                       <input type="file" accept="image/*" onchange="previewImage();" style="display:none"
                         name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                       <label for="file" class="photo-add" id="label-i">
                         <p class="form__group-l_title"> Add Profile Photo </p>
                       </label>

                       <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                         title="Change Frontage Photo"></label>
                     </div>

                     <!-- // TODO NAME STUDENT -->
                     <div class="form__group form__group__name" id="group__name">
                       <div class="form__group__icon-input">
                         <label for="name" class="form__label">First Name</label>
                         <label for="name" class="icon"><i class="icon icon__names"></i></label>
                         <?php if(empty($row5['name']) || $row5['name'] == 'NULL'){ ?>
                         <input type="text" id="name" name="name" class="form__group-input names"
                           placeholder="e.g. John">
                         <?php }else{ ?>
                         <input type="text" id="name" name="name" class="form__group-input names"
                           value="<?php echo $row5['name'] ?>">
                         <?php } ?>
                       </div>
                       <p class="form__group__input-error">Without special characters or numbers</p>
                     </div>

                     <!-- // TODO LAST NAME -->
                     <div class="form__group form__group__l_name" id="group__l_name">
                       <div class="form__group__icon-input">
                         <label for="l_name" class="form__label">Last Name</label>
                         <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                         <?php if(empty($row5['l_name']) || $row5['l_name'] == 'NULL'){ ?>
                         <input type="text" id="l_name" name="l_name" class="form__group-input names"
                           placeholder="e.g. Smith">
                         <?php }else{ ?>
                         <input type="text" id="l_name" name="l_name" class="form__group-input names"
                           value="<?php echo $row5['l_name'] ?>">
                         <?php } ?>
                       </div>
                       <p class="form__group__input-error">Without special characters or numbers</p>
                     </div>

                     <!-- // TODO MAIL -->
                     <div class="form__group form__group__mail_s" id="group__mail_s">
                       <div class="form__group__icon-input">
                         <label for="mail_s" class="form__label">Student Email</label>
                         <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>
                         <?php if(empty($row5['mail']) || $row5['mail'] == 'NULL'){ ?>
                         <input type="text" id="mail_s" name="mail_s" class="form__group-input mail"
                           placeholder="e.g. jsmith@gmail.com">
                         <?php }else{ ?>
                         <input type="text" id="mail_s" name="mail_s" class="form__group-input mail"
                           value="<?php echo $row5['mail'] ?>" readonly>
                         <?php } ?>
                       </div>
                       <p class="form__group__input-error">It is not a valid Email</p>
                     </div>

                     <!-- // TODO PASSWORD -->
                     <div class="form__group form__group__password" id="group__password">
                       <div class="form__group__icon-input">
                         <label for="password" class="form__label">Password</label>
                         <label for="password" class="icon"><i class="icon icon__password"></i></label>
                         <?php if(empty($row5['psw']) || $row5['psw'] == 'NULL'){ ?>
                         <input type="password" id="password" name="password" class="form__group-input password"
                           placeholder="min 8 Characters">
                         <?php }else{ 
                          $psw = SED::decryption($row5['psw']);
                          ?>
                         <input type="password" id="password" name="password" class="form__group-input password"
                           value="<?php echo $psw ?>" readonly>
                         <?php } ?>
                         <button type="button" class="fa fa-eye mr-2 btn__eye" id="btn__eye"></button>
                       </div>
                       <p class="form__group__input-error">Numbers, Uppercase, Lowercase, and Special Characters</p>
                     </div>

                     <!-- // TODO REPAT PASSWORD 
                     <div class="form__group form__group__password2" id="group__password2">
                       <div class="form__group__icon-input">
                         <label for="password2" class="form__label">Repeat Password</label>
                         <label for="password2" class="icon"><i class="icon icon__password"></i></label>
                         <?php /* if(empty($row5['psw']) || $row5['psw'] == 'NULL'){ ?>
                         <input type="password" id="password2" name="password2" class="form__group-input password"
                           placeholder="Repeat Password">
                         <?php }else{ 
                          $psw = SED::decryption($row5['psw']);
                          ?>
                         <input type="password" id="password2" name="password2" class="form__group-input password"
                           value="<?php echo $psw ?>" readonly>
                         <?php } */?>
                       </div>
                       <p class="form__group__input-error">Numbers, Uppercase, Lowercase, and Special Characters</p>
                     </div>-->

                     <!-- // TODO ARRIVE DATE -->
                     <div class="form__group form__group-arrive_g">
                       <div class="form__group__icon-input">

                         <label for="arrive_g" class="form__label">Arrive Date</label>
                         <label for="arrive_g" class="icon"><i class="icon icon__date"></i></label>
                         <?php if(empty($row7['arrive_g']) || $row7['arrive_g'] === 'NULL'){ ?>
                         <input type="text" id="arrive_g" name="arrive_g" class="form__group-input h_date" readonly>
                         <?php }else{ 
                          $date_arrive_g = date_create($row7['arrive_g']); //Se crea una fecha con el formato que recibes de la vista.
                          $arrive_g_date = date_format($date_arrive_g, 'm/d/Y'); //Obtienes la fecha en el formato deseado. 
                          ?>
                         <input type="text" id="arrive_g" class="form__group-input h_date"
                           value="<?php echo $arrive_g_date ?>" name="arrive_g" readonly>
                         <input type="text" id="date_arrive_g" class="form__group-input h_date"
                           value="<?php echo $arrive_g_date ?>" hidden readonly>
                         <?php } ?>
                       </div>
                       <p class="form__group__input-error">Minimum 4 Characters</p>
                     </div>

                     <!-- // TODO DESTINATION CITY  -->
                     <div class="form__group form__group__basic form__group__destination_c" id="group__destination_c">
                       <div class="form__group__icon-input">
                         <label for="destination_c" class="form__label smoker"> Destination City </label>
                         <label for="destination_c" class="icon"><i class="icon icon__city"></i></label>
                         <select class="custom-select form__group-input smoke_s" id="destination_c" name="destination_c"
                           onchange="otherCity()">
                           <option hidden="option" selected disabled>Select Option</option>
                           <option value="Toronto">Toronto</option>
                           <option value="Montreal">Montreal</option>
                           <option value="Ottawa">Ottawa</option>
                           <option value="Quebec">Quebec</option>
                           <option value="Calgary">Calgary</option>
                           <option value="Vancouver">Vancouver</option>
                           <option value="Victoria">Victoria</option>
                           <option value="Other">Other</option>
                         </select>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>
                     <div class="form__group form__group__basic form__group__other_c d-none" id="group__other_c"></div>

                     <script>
                     const $city = document.querySelector("#destination_c");

                     function otherCity() {
                       const citiI = $city.selectedIndex;
                       if (citiI === -1) return; // Esto es cuando no hay elementos
                       const optionCity = $city.options[citiI];

                       if (optionCity.value == 'Other') {

                         document.getElementById('group__other_c').classList.remove('d-none');

                         $('#group__other_c').html(`
                                                  <label class="label__other__city">Specify the Other City</label>
                                                  <input class="input__other_city" type="text" name="destination_c">`);

                       } else {
                         document.getElementById('group__other_c').classList.add('d-none');
                       }

                     };
                     </script>
                   </div>
                 </div>
               </div>

               <!-- //TODO SECOND COLUMN -->
               <div class="col-md-8">
                 <!-- //TODO LOCATION -->
                 <div class="card">
                   <div class="step__header">
                     <h2 class="title__card">Personal Student Address</h2>
                   </div>

                   <div class="step__body row justify-content-center">

                     <!-- // TODO ORIGIN CITY -->
                     <div class="form__group col-lg-4 form__group__basic form__group__country" id="group__country">
                       <div class="form__group__icon-input">
                         <label for="country" class="form__label">Country</label>
                         <label for="country" class="icon"><i class="icon icon__dir"></i></label>
                         <input type="text" id="country" name="country" class="form__group-input country"
                           placeholder="e.g. Mexico">
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>

                     <!-- //TODO ADDRESS -->
                     <div class="form__group col-lg-4 form__group__dir" id="group__dir">
                       <div class="form__group__icon-input">
                         <label for="dir" class="form__label">Address </label>
                         <label for="dir" class="icon"><i class="icon icon__dir"></i></label>
                         <input type="text" id="dir" name="dir" class="form__group-input dir"
                           placeholder="Av, Street, etc.">
                       </div>
                       <p class="form__group__input-error">Please enter a valid Address</p>
                     </div>

                     <!-- //TODO City -->
                     <div class="form__group col-lg-4 form__group__city" id="group__city">
                       <div class="form__group__icon-input">
                         <label for="city" class="form__label">City </label>
                         <label for="city" class="icon"><i class="icon icon__dir"></i></label>
                         <input type="text" id="city" name="city" class="form__group-input city2"
                           placeholder="e.g. Monterrey">
                       </div>
                       <p class="form__group__input-error">Please enter a valid City</p>
                     </div>

                     <!-- //TODO STATE -->
                     <div class="form__group col-lg-4 form__group__state" id="group__state">
                       <div class="form__group__icon-input">
                         <label for="state" class="form__label"> Province / State </label>
                         <label for="state" class="icon"><i class="icon icon__dir"></i></label>
                         <input type="text" id="state" name="state" class="form__group-input state"
                           placeholder="e.g. Yucatán">
                       </div>
                       <p class="form__group__input-error">Please enter a valid State</p>
                     </div>

                     <!-- //TODO POSTAL CODE -->
                     <div class="form__group col-lg-4 form__group__p_code" id="group__p_code">
                       <div class="form__group__icon-input">
                         <label for="p_code" class="form__label">Postal Code </label>
                         <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>
                         <input type="text" id="p_code" name="p_code" class="form__group-input p_code"
                           placeholder="e.g. 145187">
                       </div>
                       <p class="form__group__input-error">Please enter a valid Postal Code</p>
                     </div>

                   </div>

                 </div>

                 <!-- //TODO PROFESSIONAL INFORMATION -->
                 <div class="card">

                   <div class="step__header">
                     <h2 class="title__card">Professional Information</h2>
                   </div>

                   <div class="step__body row justify-content-center">

                     <!-- //TODO ACADEMY -->
                     <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">
                       <div class="form__group__icon-input">
                         <label for="n_a" class="form__label">Academy</label>
                         <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>
                         <select class="custom-select form__group-input n_a" id="n_a" name="n_a"
                           onchange="otherAcademy()">
                           <?php
                            while ($row6 = $query6->fetch_array()) {
                            ?>
                           <option hidden="option" selected disabled>Select Academy</option>
                           <option value=" <?php echo $row6['id_ac'] ?> ">
                             <?php echo $row6['name_a']; ?><?php echo ', ' ?><?php echo $row6['dir_a']; ?>
                           </option>
                           <?php }  ?>
                           <option value="Other">Other</option>
                         </select>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                       <div class="form__group form__group__basic form__group__other_a d-none" id="group__other_a">
                       </div>
                       <script>
                       const $academy = document.querySelector("#n_a");

                       function otherAcademy() {
                         const academyI = $academy.selectedIndex;
                         if (academyI === -1) return; // Esto es cuando no hay elementos
                         const optionAcademy = $academy.options[academyI];

                         if (optionAcademy.value == 'Other') {

                           document.getElementById('group__other_a').classList.remove('d-none');

                           $('#group__other_a').html(`

                                        <div class="row mt-3 mb-0 justify-content-center">
                                            <div class="form__group col-sm-6 form__group__basic form__group__n_a" id="group__n_a">
                                                <label class="label__other__city">Academy Name</label>
                                                <input class="input__other_city" type="text" name="name_a">
                                                
                                            </div>  
                                        </div>
                                        `);

                         } else document.getElementById('group__other_a').classList.add('d-none');
                       };
                       </script>
                     </div>

                     <!-- //TODO ENGLISH LEVEL -->
                     <div class="form__group col-lg-4 form__group__basic form__group__english_l" id="group__english_l">
                       <div class="form__group__icon-input">
                         <label for="english_l" class="form__label smoker">Current English level</label>
                         <label for="english_l" class="icon"><i class="icon icon__city"></i></label>
                         <select class="custom-select form__group-input smoke_s" id="english_l" name="english_l">
                           <option hidden="option" selected disabled>Select Option</option>
                           <option value="Beginner">Beginner</option>
                           <option value="Intermediate">Intermediate</option>
                           <option value="Advanced">Advanced</option>
                         </select>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>

                     <!-- //TODO TYPE STUDENT -->
                     <div class="form__group col-lg-4 form__group__basic form__group__type_s" id="group__type_s">
                       <div class="form__group__icon-input">
                         <label for="type_s" class="form__label">Type Student</label>
                         <label for="type_s" class="icon"><i class="icon icon__names"></i></label>
                         <select class="custom-select form__group-input smoke_s" id="type_s" name="type_s"
                           onchange="otherProgram()">
                           <option hidden="option" selected disabled>Select Option</option>
                           <option value="High School ">High School</option>
                           <option value="English and French">English and French Course</option>
                           <option value="College / University">College / University </option>
                           <option value="Summer Camps">Summer Camps</option>
                         </select>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>

                     <!-- //TODO PROGRAM TO STUDY IN CANADA -->
                     <div class="form__group col-lg-4 form__group__basic form__group__prog_selec d-none"
                       id="group__prog_selec"></div>

                     <script>
                     const $program = document.querySelector("#type_s");

                     function otherProgram() {
                       const programI = $program.selectedIndex;
                       if (programI === -1) return; // Esto es cuando no hay elementos
                       const optionProgram = $program.options[programI];

                       if (optionProgram.value == 'College / University' || optionProgram.value == 'Camps') {

                         document.getElementById('group__prog_selec').classList.remove('d-none');

                         $('#group__prog_selec').html(`

                                        <div class="form__group__icon-input">

                                            <label for="prog_selec" class="form__label">Program to Study in Canada</label>

                                            <label for="prog_selec" class="icon"><i class="icon icon__prog_selec"></i></label>
                                            <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s" placeholder="e.g. Business">

                                        </div>
                                        <p class="form__group__input-error">Enter a valid Date</p>
                                        `);

                       } else {
                         document.getElementById('group__prog_selec').classList.add('d-none');
                       }

                     };
                     </script>

                     <!-- //TODO SCHEDULE  -->
                     <div class="form__group col-lg-4 form__group__basic form__group__schedule" id="group__schedule">
                       <div class="form__group__icon-input">
                         <label for="schedule" class="form__label smoker"> Schedule </label>
                         <label for="schedule" class="icon"><i class="icon icon__city"></i></label>
                         <select class="custom-select form__group-input smoke_s" id="schedule" name="schedule">
                           <option hidden="option" selected disabled>Select Option</option>
                           <option value="Day">Day</option>
                           <option value="Evening">Evening</option>
                         </select>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>
                   </div>

                 </div>

                 <!-- //TODO ACCOMMODATION REQUEST -->
                 <div class="card">

                   <div class="step__header">
                     <h2 class="title__card">Accommodation Request</h2>
                   </div>

                   <div class="step__body row justify-content-center w-75 accom">

                     <!-- //TODO START DATE OF STAY -->
                     <div class="form__group col-lg-5 form__group__firstd" id="group__firstd">
                       <div class="form__group__icon-input">
                         <label for="firstd" class="form__label">Start Date of Stay</label>
                         <label for="firstd" class="icon"><i class="icon icon__date"></i></label>
                         <input type="text" id="firstd" name="firstd" class="form__group-input firstd" readonly>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>

                     <!-- //TODO END DATE OF STAY -->
                     <div class="form__group col-lg-5 form__group__lastd" id="group__lastd">
                       <div class="form__group__icon-input">
                         <label for="lastd" class="form__label">End Date of Stay</label>
                         <label for="lastd" class="icon"><i class="icon icon__date"></i></label>
                         <input type="text" id="lastd" name="lastd" class="form__group-input lastd" readonly>
                       </div>
                       <p class="form__group__input-error">Enter a valid Date</p>
                     </div>
                     <input type="date" id="firstd2" hidden>
                     <input type="date" id="lastd2" hidden>
                     <p id="calcWeeks" class="calc__weeks"></p>

                   </div>

                 </div>
               </div>

             </div>

             <input type="hidden" name="value_step" value="next_step-1">

             <div class="step__footer">
               <button type="button" id="btn__next-1" class="btn btn__next step__button--next" data-to_step="2"
                 data-step="1">Next</button>
             </div>

           </form>

         </div>

         <! //TODO ------------------------------------------------------------------------------------------->


           <!-- //TODO STEP 2 -->

           <div class="step" id="step-2">
             <form action="#" method="POST" class="form__register" id="form__register" autocomplete="off"
               enctype="multipart/form-data">
               <div class="row ">
                 <div class="col-md-4">

                   <!-- //TODO PERSONAL INFORMATION -->
                   <div class="card">

                     <div class="step__header">
                       <h2 class="title__card">Personal Information I</h2>
                     </div>

                     <div class="step__body mt-3">

                       <!-- //TODO DATE OF BIRTH -->
                       <div class="form__group form__group__db" id="group__db">
                         <div class="form__group__icon-input">

                           <label for="db" class="form__label">Date of Birth</label>
                           <label for="db" class="icon"><i class="icon icon__date"></i></label>
                           <input type="text" id="db" name="db" class="form__group-input db" readonly>

                         </div>
                         <p class="form__group__input-error">Enter a valid Date</p>
                       </div>

                       <!-- // TODO GENDER -->
                       <div class="form__group form__group__basic form__group__gender" id="group__gender">
                         <div class="form__group__icon-input">
                           <label for="gender" class="form__label">Gender</label>
                           <label for="gender" class="icon"><i class="icon icon__gender"></i></label>
                           <select class="custom-select form__group-input gender" id="gender" name="gender">
                             <option hidden="option" selected disabled> Select Gender</option>
                             <option value="Male">Male</option>
                             <option value="Female">Female</option>
                             <option value="Non-Binary">Non-Binary</option>
                             <option value="Private">Prefer not to say</option>
                           </select>
                         </div>
                         <p class="form__group__input-error">Enter a Gender</p>
                       </div>

                       <!-- //TODO NUMBER -->
                       <div class="form__group form__group__num_s" id="group__num_s">
                         <div class="form__group__icon-input">
                           <label for="num_s" class="form__label">Phone Number</label>
                           <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>
                           <input type="num" id="num_s" name="num_s" class="form__group-input num_s"
                             placeholder="Personal Number">
                         </div>
                         <p class="form__group__input-error">Enter a Valid Number</p>
                       </div>

                       <!-- //TODO ORIGIN CITY -->
                       <div class="form__group form__group__basic form__group__nationality" id="group__nationality">
                         <div class="form__group__icon-input">
                           <label for="nationality" class="form__label">Background</label>
                           <label for="nationality" class="icon"><i class="icon icon__city"></i></label>
                           <input type="text" id="nationality" name="nationality" class="form__group-input country"
                             placeholder="e.g. Mexican">
                         </div>
                         <p class="form__group__input-error">Enter a valid Date</p>
                       </div>

                       <!-- //TODO ORIGIN LANGUAGE -->
                       <div class="form__group form__group__basic form__group__lang_s" id="group__lang_s">
                         <div class="form__group__icon-input">
                           <label for="lang_s" class="form__label">Origin Language</label>
                           <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>
                           <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s"
                             placeholder="e.g. English">
                         </div>
                         <p class="form__group__input-error">Enter a valid Date</p>
                       </div>

                       <!-- //TODO ANOTHER LANGUAGE -->
                       <div class="form__group form__group__basic form__group__language_a mb-2" id="group__language_a">
                         <div class="form__group__icon-input">
                           <label for="language_a" class="form__label">Another Language</label>
                           <label for="language_a" class="icon"><i class="icon icon__nacionality"></i></label>
                           <input type="text" id="language_a" name="language_a" class="form__group-input nacionality"
                             placeholder="e.g. Spanish">
                         </div>
                         <p class="form__group__input-error">Enter a valid Date</p>
                       </div>

                     </div>

                   </div>

                 </div>

                 <div class="col-md-8">

                   <!-- // TODO FLIGHT INFORMATION -->
                   <div class="card">

                     <div class="step__header">
                       <h2 class="title__card">Flight Information</h2>
                     </div>

                     <div class="step__body mt-3">

                       <div class="row justify-content-center">

                         <!-- //TODO AIRLINE NAME -->
                         <div class="form__group col-sm-5 form__group__n_airline" id="group__n_airline">
                           <div class="form__group__icon-input">
                             <label for="n_airline" class="form__label">Reservation Code</label>
                             <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>
                             <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline"
                               placeholder="e.g. DASDTA">
                             <p class="form__group__input-error">Minimum 4 Characters</p>
                           </div>
                         </div>

                         <!-- //TODO LANDING FLIGHT NUMBER -->
                         <div class="form__group col-sm-5 form__group__n_flight" id="group__n_flight">
                           <div class="form__group__icon-input">
                             <label for="n_flight" class="form__label">Landing Flight Number</label>
                             <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>
                             <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight"
                               placeholder="e.g. B-9820">
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                         <!-- //TODO FLIGHT DATE -->
                         <div class="form__group col-sm-4 form__group-f_date ">
                           <div class="form__group__icon-input">
                             <label for="f_date" class="form__label">Flight Date</label>
                             <label for="f_date" class="icon"><i class="icon icon__date"></i></label>
                             <input type="text" id="f_date" name="f_date" class="form__group-input f_date" readonly>
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                         <!-- //TODO ARRIVAL AT THE HOMESTAY -->
                         <div class="form__group col-sm-4 form__group-h_date">
                           <div class="form__group__icon-input">
                             <label for="h_date" class="form__label">Arrival at the Homestay</label>
                             <label for="h_date" class="icon"><i class="icon icon__date"></i></label>
                             <input type="text" id="h_date" name="h_date" class="form__group-input h_date" readonly>
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                       </div>

                     </div>

                   </div>

                   <!-- //TODO EMERGENCY CONTACT -->
                   <div class="card">

                     <div class="step__header">
                       <h2 class="title__card">Emergency Contact</h2>
                     </div>

                     <div class="step__body mt-3">

                       <div class="row justify-content-center">

                         <!-- //TODO EMERGENCY CONTACT NAME -->
                         <div class="form__group col-sm-5 form__group__cont_name" id="group__cont_name">
                           <div class="form__group__icon-input">
                             <label for="cont_name" class="form__label">Contact Name</label>
                             <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>
                             <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name"
                               placeholder="e.g. Joy">
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                         <!-- //TODO EMERGENCY CONTACT LAST NAME -->
                         <div class="form__group col-sm-5 form__group__cont_lname" id="group__cont_lname">
                           <div class="form__group__icon-input">
                             <label for="cont_lname" class="form__label">Contact Last Name</label>
                             <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>
                             <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname"
                               placeholder="e.g. Smith">
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                         <!-- //TODO EMERGENCY NUMBER -->
                         <div class="form__group col-sm-4 form__group__num_conts" id="group__num_conts">
                           <div class="form__group__icon-input">
                             <label for="num_conts" class="form__label">Emergency Phone Number</label>
                             <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>
                             <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts"
                               placeholder="Emergency Number">
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                         <!-- //TODO RELATIONSHIP -->
                         <div class="form__group col-sm-4 form__group__cell_s" id="group__cell_s">
                           <div class="form__group__icon-input">
                             <label for="cell_s" class="form__label">Relationship</label>
                             <label for="cont_lname" class="icon"><i class="icon icon__relation"></i></label>
                             <select id="relationship" name="relationship"
                               class="custom-select form__group-input gender">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Dad">Dad</option>
                               <option value="Mom">Mom</option>
                               <option value="Son">Son</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Friends">Friends</option>
                               <option value="Other">Others</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Minimum 4 Characters</p>
                         </div>

                       </div>

                     </div>

                   </div>

                 </div>

               </div>

               <input type="hidden" name="value_step" value="next_step-2">

               <div class="step__footer">

                 <button type="button" id="btn__back-2" class="btn step__button--back" data-to_step="1"
                   data-step="2">Back</button>

                 <button type="button" id="btn__next-2" class="btn btn__next step__button--next" data-to_step="3"
                   data-step="2">Next</button>
               </div>
             </form>

           </div>

           <!-- // TODO --------------------------------------------------------------------------------------->


           <!-- // TODO STEP 3 -->

           <div class="step" id="step-3">
             <form action="#" method="POST" class="form__register" id="form__register" autocomplete="off"
               enctype="multipart/form-data">
               <div class="row justify-content-center">
                 <div class="col-md-9">

                   <!-- //TODO HOUSE PREFERENCES -->
                   <div class="card">
                     <div class="step__header">
                       <h2 class="title__card">House Preferences</h2>
                     </div>

                     <div class="step__body mt-3">

                       <!-- //TODO CAN YOU SHARE WITH -->
                       <h3 class="subtitle__section-card mb-4">Can you Share With:</h3>
                       <div class="row justify-content-center suboption mt-5">

                         <!-- //TODO SMOKERS? -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__smoker_l"
                           id="group__smoker_l">
                           <div class="form__group__icon-input">
                             <label for="smoker_l" class="form__label pets_l">Smokers?</label>
                             <label for="smoker_l" class="icon"><i class="icon icon__smoke_s"></i></label>
                             <select class="custom-select form__group-input pets" id="smoker_l" name="smoker_l">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO CHILDREN? -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__children"
                           id="group__children">
                           <div class="form__group__icon-input">
                             <label for="children" class="form__label pets_l">Children?</label>
                             <label for="children" class="icon"><i class="icon icon__children"></i></label>
                             <select class="custom-select form__group-input pets" id="children" name="children">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO TEENAGERS -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__teenagers"
                           id="group__teenagers">
                           <div class="form__group__icon-input">
                             <label for="teenagers" class="form__label pets_l">Teenagers?</label>
                             <label for="teenagers" class="icon"><i class="icon icon__children"></i></label>
                             <select class="custom-select form__group-input pets" id="teenagers" name="teenagers">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO PETS -->
                         <div class="form__group col-lg-5 form__group__basic form__group__pets" id="group__pets">
                           <div class="form__group__icon-input">
                             <label for="pets" class="form__label pets_l">Pets?</label>
                             <label for="pets" class="icon"><i class="icon icon__pets"></i></label>
                             <select class="custom-select form__group-input pets" id="pets" name="pets">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Yes">Yes</option>
                               <option value="No">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>
                       </div>

                       <!-- //TODO ACCOMMODATION -->
                       <h3 class="subtitle__section-card mt-4 mb-4">Accommodation</h3>

                       <div class="row justify-content-center suboption mt-5">

                         <!-- //TODO TYPE OF ACCOMMODATION -->
                         <div class="form__group col-lg-5 mb-5 form__group__lodging_type" id="group__lodging_type">
                           <div class="form__group__icon-input">
                             <label for="lodging_type" class="form__label">Type of Accommodation</label>
                             <label for="lodging_type" class="icon"><i class="icon icon__lodging"></i></label>
                             <select class="custom-select form__group-input gender" id="lodging_type"
                               name="lodging_type">
                               <option hidden="option" selected disabled> Select Option</option>
                               <option value="Single">Single</option>
                               <option value="Share">Share</option>
                               <option value="Executive">Executive</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO MEAL PLAN -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__food " id="group__food">
                           <div class="form__group__icon-input">
                             <label for="meal_p" class="form__label pets_l">Meal Plan</label>
                             <label for="meal_p" class="icon"><i class="icon icon__food"></i></label>
                             <select class="custom-select form__group-input food" id="meal_p" name="meal_p">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Only Room">Only Room</option>
                               <option value="2 Meals">2 Meals</option>
                               <option value="3 Meals">3 Meals</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                           <input type="text" name="food" id="food" class="d-none">
                         </div>

                         <!--  //TODO MEAL PLAN
                         <div class="form__group d-none col-lg-5 mb-5 form__group__basic form__group__food "
                           id="group__food">
                           <div class="form__group__icon-input">
                             <label for="food" class="form__label pets_l">Meal Plan</label>
                             <label for="food" class="icon"><i class="icon icon__food"></i></label>
                             <select class="custom-select form__group-input food" id="food" name="food"
                               onchange="mostrar()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Yes">Yes</option>
                               <option value="No">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div> -->

                         <!-- //TODO SPECIAL DIET -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__diet d-none"
                           id="group__diet">
                           <div class="form__group__icon-input">
                             <label for="diet" class="form__label pets_l">Special Diet</label>
                             <label for="diet" class="icon"><i class="icon icon__food"></i></label>
                             <select class="custom-select form__group-input food" id="diet" name="diet">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Vegetarians">Vegetarians</option>
                               <option value="Halal">Halal (Muslims)</option>
                               <option value="Kosher">Kosher (Jews)</option>
                               <option value="Lactose">Lactose Intolerant</option>
                               <option value="Gluten">Gluten Free Diet</option>
                               <option value="Pork">No Pork</option>
                               <option value="None">None</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>
                       </div>

                       <p class="text_diet d-none" id="text_meal">* The meal plan will cost an additional $35 CAD each
                         week *</p>
                       <p class="text_diet d-none" id="text_diet">* $15 CAD will be charged each day when special diet
                         is
                         required *</p>


                       <!-- //TODO ACTUAL PRICE ACCOMMODATION -->
                       <div class="d-flex justify-content-center container__price ml-auto mr-auto">
                         <label for="total_weckly" class="label__price">Total Weckly Price</label>
                         <input type="text" class="form__group-input text-center font-weight-bold" readonly value="$">
                         <input type="text" class="form__group-input text-center" readonly id="total_weckly">
                         <input type="text" class="form__group-input text-center font-weight-bold" readonly value="CAD">
                       </div>

                       <input type="hidden" readonly id="total_weckly-db" name="a_price">

                       <h3 class="subtitle__section-card mt-5 mb-4">Optional Supplements</h3>

                       <div class="information__content information__content-col-8">

                         <p class="text__supplements"><b>Accommodation near the school:</b> Additional <b>$35 CAD</b>
                           per
                           night
                           applies (up to 30 minutes
                           walking).</p>
                         <p class="text__supplements"><b>Urgent accommodation search:</b> Less than 1 week = <b>$150
                             CAD</b>.</p>
                         <!-- <p class="text__supplements"><b>Guardianship:</b> Guardianship is a practical and moral support
                           system
                           for young students studying
                           in a foreign country, far from their parents = <b>$550 CAD</b>.</p> -->
                         <p class="text__supplements ">
                           <b>Minor fee (-18 years old): $75 CAD</b> per week.
                         </p>
                         <p class="text__supplements ">
                           <b>Summer fee: $45 CAD </b> per week.
                         </p>

                         <div class="row form__group-basic-content m-0 p-0 mt-5">

                           <!-- // TODO ACCOMMODATION NEAR THE SCHOOL -->
                           <div class="form__group col-lg-5 mt-2 mb-md-5 form__group__basic form__group__near-school"
                             id="group__near-school">

                             <div class="form__group__icon-input">

                               <label for="accomm-near" class="form__label">Accommodation near the school
                               </label>
                               <label for="accomm-near" class="form__label pl-1"></label>
                               <label for="accomm-near" class="icon"><i class="icon icon_accommodation"></i></label>

                               <select class="custom-select form__group-input pets" id="accomm-near" name="accomm-near">
                                 <option hidden="option" selected disabled>Select Option</option>
                                 <option value="yes">Yes</option>
                                 <option value="no">No</option>
                               </select>

                             </div>
                             <p class="form__group__input-error">Enter a valid Date</p>
                           </div>

                           <!-- // TODO ACCOMMODATION NEAR THE SCHOOL -->
                           <div class="form__group col-lg-5 mt-2 form__group__basic form__group__accomm-urgent"
                             id="group__accomm-urgent">

                             <div class="form__group__icon-input">

                               <label for="accomm-urgent" class="form__label pl-1">Urgent accommodation search
                               </label>
                               <label for="accomm-urgent" class="icon"><i class="icon icon_accommodation"></i></label>

                               <select class="custom-select form__group-input pets" id="accomm-urgent"
                                 name="accomm-urgent">
                                 <option hidden="option" selected disabled>Select Option</option>
                                 <option value="yes">Yes</option>
                                 <option value="no">No</option>
                               </select>

                             </div>
                             <p class="form__group__input-error">Enter a valid Date</p>
                           </div>

                           <!-- // TODO SUMMER FEE -->
                           <div class="form__group col-lg-5 mt-2 form__group__basic form__group__summer-fee"
                             id="group__summer-fee">

                             <div class="form__group__icon-input">

                               <label for="summer-fee" class="form__label pl-1">Do you want the Summer Fee plan?
                               </label>
                               <label for="summer-fee" class="icon"><i class="icon icon_accommodation"></i></label>

                               <select class="custom-select form__group-input pets" id="summer-fee" name="summer-fee">
                                 <option hidden="option" selected disabled>Select Option</option>
                                 <option value="yes">Yes</option>
                                 <option value="no">No</option>
                               </select>

                             </div>
                             <p class="form__group__input-error">Enter a valid Date</p>
                           </div>

                           <!-- // TODO MINOR FEE -->
                           <div class="form__group col-lg-5 mt-2 form__group__basic form__group__minor-fee"
                             id="group__minor-fee">

                             <div class="form__group__icon-input">

                               <label for="minor-fee" class="form__label pl-1">Are you under 18?</label>
                               <label for="minor-fee" class="icon"><i class="icon icon__relation1"></i></label>

                               <select class="custom-select form__group-input pets" id="minor-fee" name="minor-fee">
                                 <option hidden="option" selected disabled>Select Option</option>
                                 <option value="yes">Yes</option>
                                 <option value="no">No</option>
                               </select>

                             </div>
                             <p class="form__group__input-error">Enter a valid Date</p>
                           </div>

                           <!-- // TODO GUARDIANSHIP -->
                           <!-- <div class="form__group col-lg-5 mt-2 form__group__basic form__group__accomm-guardianship"
                             id="group__accomm-guardianship">

                             <div class="form__group__icon-input">

                               <label for="accomm-guardianship" class="form__label pl-1">Guardianship </label>
                               <label for="accomm-guardianship" class="icon"><i class="icon icon__relation"></i></label>

                               <select class="custom-select form__group-input pets" id="accomm-guardianship"
                                 name="accomm-guardianship">
                                 <option hidden="option" selected disabled>Select Option</option>
                                 <option value="yes">Yes</option>
                                 <option value="no">No</option>
                               </select>

                             </div>
                             <p class="form__group__input-error">Enter a valid Date</p>
                           </div> -->
                         </div>

                       </div>

                       <!-- //TODO TRANSPORT -->

                       <h3 class="subtitle__section-card mb-4">Transport </h3>

                       <div class="row justify-content-center suboption mt-5">

                         <!-- //TODO PICK-UP SERVICE -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__pick_up"
                           id="group__pick_up">
                           <div class="form__group__icon-input">
                             <label for="pick_up" class="form__label pets_l">Pick Up Service</label>
                             <label for="pick_up" class="icon"><i class="icon icon__city"></i></label>
                             <select class="custom-select form__group-input pets" id="pick_up" name="pick_up">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO DROP-OFF SERVICE -->
                         <div class="form__group col-lg-5 mb-5 form__group__basic form__group__drop_off"
                           id="group__drop_off">
                           <div class="form__group__icon-input">
                             <label for="drop_off" class="form__label pets_l">Drop off service</label>
                             <label for="drop_off" class="icon"><i class="icon icon__city"></i></label>
                             <select class="custom-select form__group-input pets" id="drop_off" name="drop_off">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                       </div>


                       <!-- //TODO ACTUAL PRICE -->
                       <div class="d-flex justify-content-center container__price ml-auto mr-auto">
                         <label for="transport_total-db" class="label__price">Transport Price</label>
                         <input type="text" class="form__group-input text-center font-weight-bold" readonly value="$">
                         <input type="text" class="form__group-input text-center" readonly id="transport_total-db"
                           name="t_price">
                         <input type="text" class="form__group-input text-center font-weight-bold" readonly value="CAD">
                       </div>

                       <!-- <script>
                       const $pick_up = document.querySelector("#pick_up");
                       const $drop_off = document.querySelector("#drop_off");
                       var total = document.getElementById('total').value;

                       function mostrar() {
                         var pickUp = '0';
                         var dropOff = '0';

                         //TODO PICK UP
                         const pick_up = $pick_up.selectedIndex;
                         if (pick_up === -1) return; // Esto es cuando no hay elementos
                         const optionPickUp = $pick_up.options[pick_up];

                         if (optionPickUp.value == 'yes') pickUp = '200';
                         else {
                           pickUp = '0';
                           document.getElementById('total').value = pickUp;
                         }

                         // TODO Drop Off
                         const drop_off = $drop_off.selectedIndex;
                         if (drop_off === -1) return; // Esto es cuando no hay elementos
                         const optionDropOff = $drop_off.options[drop_off];

                         if (optionDropOff.value == 'yes') dropOff = '100';
                         else {
                           dropOff = '0';
                           document.getElementById('total').value = dropOff;
                         }

                         // TODO Total Transport Services

                         if (dropOff == '0') var tTotal = parseFloat(pickUp);
                         else if (pickUp == '0') var tTotal = parseFloat(dropOff);
                         else var tTotal = parseFloat(pickUp) + parseFloat(dropOff);

                         document.getElementById('total').value = "$" + tTotal + " CAD";

                       };
                       </script> -->
                     </div>
                     <hr class="ml-auto mr-auto" style="width: 85%">
                     <!-- //TODO ACTUAL PRICE -->

                     <div class="d-flex justify-content-center container__price container__tprice mb-5 mr-5">
                       <label for="total_weckly-db" class="label__price">Total Price</label>
                       <input type="text" class="form__group-input text-center font-weight-bold" readonly value="$">
                       <input type="text" class="form__group-input text-center" readonly id="total__price">
                       <input type="text" class="form__group-input text-center font-weight-bold" readonly value="CAD">
                     </div>

                   </div>
                 </div>
               </div>


               <input type="hidden" name="value_step" value="next_step-3">

               <div class="step__footer">
                 <button type="button" id="btn__back-3" class="btn step__button--back" data-to_step="2"
                   data-step="3">Back</button>
                 <button type="button" id="btn__next-3" class="btn btn__next step__button--next" data-to_step="4"
                   data-step="3">Next</button>
               </div>
             </form>

           </div>

           <!-- // TODO--------------------------------------------------------------------------------------------->


           <!-- //TODO STEP 4 -->

           <div class="step" id="step-4">
             <form action="#" method="POST" class="form__register" id="form__register" autocomplete="off"
               enctype="multipart/form-data">

               <div class="row justify-content-center">

                 <div class="col-md-8">

                   <!-- //TODO PERSONAL INFORMATION II -->
                   <div class="card">
                     <div class="step__header">
                       <h2 class="title__card">Personal Information II</h2>
                     </div>

                     <div class="step__body mt-3">
                       <div class="row justify-content-center">

                         <!-- //TODO ABOUT ME -->
                         <div
                           class="form__group col-sm-12 form__group__basic form__group__preference form__group__about_me"
                           id="group__about_me">
                           <div class="form__group__icon-input">
                             <label for="about_me" class="form__label allergies_l">Tell us about yourself (max. 300
                               Characters).</label>
                             <label for="about_me" class="icon"><i class="icon icon__about_me"></i></label>
                             <textarea class="form__group-input allergies" id="about_me" rows="2" name="about_me"
                               placeholder="About you. Maximum 300 characters." maxlength="300"></textarea>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO PASSPORT -->
                         <div class="form__group col-sm-4 form__group__basic form__group__pass mb-2" id="group__pass">
                           <div class="form__group__icon-input">
                             <label for="pass" class="form__label visa">Passport</label>
                             <label for="pass" class="icon"><i class="icon icon__pass"></i></label>
                             <input type="text" id="pass" name="pass" class="form__group-input pass"
                               placeholder="Passport">
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO PASSPORT EXPIRATE DATE -->
                         <div class="form__group col-sm-4 form__group__basic form__group__exp_pass mb-2"
                           id="group__exp_pass">
                           <div class="form__group__icon-input">
                             <label for="exp_pass" class="form__label visa">Passport Expiration Date</label>
                             <label for="exp_pass" class="icon"><i class="icon icon__bl"></i></label>
                             <input type="text" id="exp_pass" name="exp_pass" class="form__group-input pass" readonly>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO VISA EXPIRATION DATE -->
                         <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                           <div class="form__group__icon-input">
                             <label for="bl" class="form__label visa">Visa Expiration Date</label>
                             <label for="bl" class="icon"><i class="icon icon__bl"></i></label>
                             <input type="text" id="bl" name="bl" class="form__group-input bl" readonly>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                       </div>

                     </div>

                   </div>

                   <!-- //TODO HEALTH INFORMATION -->
                   <div class="card">
                     <div class="step__header">
                       <h2 class="title__card">Health Information</h2>
                     </div>
                     <div class="step__body mt-3">
                       <div class="row justify-content-center">

                         <!-- //TODO DO YOU SMOKE? -->
                         <div class="form__group col-lg-4 form__group__basic form__group__smoke_s" id="group__smoke_s">
                           <div class="form__group__icon-input">
                             <label for="smoke_s" class="form__label smoker">Do you smoke?</label>
                             <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>
                             <select class="custom-select form__group-input smoke_s" id="smoke_s" name="smoke_s">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="Yes">Yes</option>
                               <option value="No">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO DO YOU DRINK ALCOHOL? -->
                         <div class="form__group col-lg-4 form__group__basic form__group__drinks_alc"
                           id="group__drinks_alc">
                           <div class="form__group__icon-input">
                             <label for="drinks_alc" class="form__label smoker">Do you Drink Alcohol?</label>
                             <label for="drinks_alc" class="icon"><i class="icon icon__drinks_alc"></i></label>
                             <select class="custom-select form__group-input smoke_s" id="drinks_alc" name="drinks_alc">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO DRUGS? -->
                         <div class="form__group col-lg-4 form__group__basic form__group__drugs" id="group__drugs">
                           <div class="form__group__icon-input">
                             <label for="drugs" class="form__label smoker">Have you used any drugs?</label>
                             <label for="drugs" class="icon"><i class="icon icon__drugs"></i></label>
                             <select class="custom-select form__group-input smoke_s" id="drugs" name="drugs">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO ALLERGY TO ANIMALS -->
                         <div class="form__group col-lg-5 form__group__basic form__group__allergy_a"
                           id="group__allergy_a">
                           <div class="form__group__icon-input">
                             <label for="allergy_a" class="form__label pets_l">Any Allergy to Animals</label>
                             <label for="allergy_a" class="icon"><i class="icon icon__pets"></i></label>
                             <select class="custom-select form__group-input pets" id="allergy_a" name="allergy_a"
                               onchange="allergyAnimals()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_allergy_a"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO DIETARY RESTRICTIONS -->
                         <div class="form__group col-lg-5 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="allergy_m" class="form__label pets_l">Dietary Restrictions</label>
                             <label for="allergy_m" class="icon"><i class="icon icon__food"></i></label>
                             <select class="custom-select form__group-input pets" id="allergy_m" name="allergy_m"
                               onchange="allergyMeals()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_allergy_m"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                       </div>


                       <div class="row justify-content-center">

                         <!-- //TODO CURRENT HEALTH INFORMATION -->
                         <div
                           class="form__group col-sm-12 form__group__basic form__group__preference form__group__healt_s"
                           id="group__healt_s">
                           <div class="form__group__icon-input">
                             <label for="healt_s" class="form__label allergies_l">What is your state of health at the
                               moment?</label>
                             <label for="healt_s" class="icon"><i class="icon icon__healt_s"></i></label>
                             <textarea class="form__group-input allergies" id="healt_s" rows="3" name="healt_s"
                               placeholder="Write using few words."></textarea>
                           </div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO DISEASE -->
                         <div class="form__group col-lg-6 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="disease" class="form__label pets_l">Do you suffer from any disease?</label>
                             <label for="disease" class="icon"><i class="icon icon__allergies2"></i></label>
                             <select class="custom-select form__group-input pets" id="disease" name="disease"
                               onchange="diseaseSpecify()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_disease"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO ALLERGIES -->
                         <div class="form__group col-lg-6 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="allergies" class="form__label pets_l">Do you suffer from any
                               allergies?</label>
                             <label for="allergies" class="icon"><i class="icon icon__allergies"></i></label>
                             <select class="custom-select form__group-input pets" id="allergies" name="allergies"
                               onchange="allergiesSpecify()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_allergies"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO TREATMENT -->
                         <div class="form__group col-lg-6 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="treatment" class="form__label pets_l">Are you under treatment or taking any
                               medication?</label>
                             <label for="treatment" class="icon"><i class="icon icon__allergies__other"></i></label>
                             <select class="custom-select form__group-input pets" id="treatment" name="treatment"
                               onchange="treatmentSpecify()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_treatment"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO SURGERY -->
                         <div class="form__group col-lg-6 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="surgery" class="form__label pets_l">Have you had any surgery in the 12 months
                               prior to your trip?</label>
                             <label for="surgery" class="icon"><i class="icon icon__allergies2__other"></i></label>
                             <select class="custom-select form__group-input pets" id="surgery" name="surgery"
                               onchange="surgerySpecify()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_surgery"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>

                         <!-- //TODO PSYCHOLOGICAL TREATMENT -->
                         <div class="form__group col-lg-6 form__group__basic form__group__allergy_m"
                           id="group__allergy_m">
                           <div class="form__group__icon-input">
                             <label for="treatment_p" class="form__label pets_l">Are you or were you undergoing
                               psychological or psychiatric treatment?</label>
                             <label for="treatment_p" class="icon"><i class="icon icon__allergies__other"></i></label>
                             <select class="custom-select form__group-input pets" id="treatment_p" name="treatment_p"
                               onchange="treatmentPSpecify()">
                               <option hidden="option" selected disabled>Select Option</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                             </select>
                           </div>

                           <div class="form__group__icon-input d-none" id="specify_treatment_p"></div>
                           <p class="form__group__input-error">Enter a valid Date</p>
                         </div>





                         <script>
                         const $allergyA = document.querySelector("#allergy_a");

                         function allergyAnimals() {
                           const allergyA = $allergyA.selectedIndex;
                           if (allergyA === -1) return; // Esto es cuando no hay elementos
                           const optionAallergy = $allergyA.options[allergyA];

                           if (optionAallergy.value == 'yes') {

                             document.getElementById('specify_allergy_a').classList.remove('d-none');

                             $('#specify_allergy_a').html(`
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="allergy_a">`);

                           } else {
                             document.getElementById('specify_allergy_a').classList.add('d-none');
                           }

                         };


                         // Allergy 

                         const $allergyM = document.querySelector("#allergy_m");

                         function allergyMeals() {
                           const allergyM = $allergyM.selectedIndex;
                           if (allergyM === -1) return; // Esto es cuando no hay elementos
                           const optionMallergy = $allergyM.options[allergyM];

                           if (optionMallergy.value == 'yes') {

                             document.getElementById('specify_allergy_m').classList.remove('d-none');

                             $('#specify_allergy_m').html(`
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="allergy_m">`);

                           } else {
                             document.getElementById('specify_allergy_m').classList.add('d-none');
                           }

                         };

                         // Disease 

                         const $disease = document.querySelector("#disease");

                         function diseaseSpecify() {
                           const disease = $disease.selectedIndex;
                           if (disease === -1) return; // Esto es cuando no hay elementos
                           const optionDisease = $disease.options[disease];

                           if (optionDisease.value == 'yes') {

                             document.getElementById('specify_disease').classList.remove('d-none');

                             $('#specify_disease').html(`
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="disease">`);

                           } else {
                             document.getElementById('specify_disease').classList.add('d-none');
                           }

                         };

                         // Treatment 

                         const $treatment = document.querySelector("#treatment");

                         function treatmentSpecify() {
                           const treatment = $treatment.selectedIndex;
                           if (treatment === -1) return; // Esto es cuando no hay elementos
                           const optionTreatment = $treatment.options[treatment];

                           if (optionTreatment.value == 'yes') {

                             document.getElementById('specify_treatment').classList.remove('d-none');

                             $('#specify_treatment').html(`
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="treatment">`);

                           } else {
                             document.getElementById('specify_treatment').classList.add('d-none');
                           }

                         };

                         // Treatment_P

                         const $treatment_p = document.querySelector("#treatment_p");

                         function treatmentPSpecify() {
                           const treatment_p = $treatment_p.selectedIndex;
                           if (treatment_p === -1) return; // Esto es cuando no hay elementos
                           const optionTreatment_p = $treatment_p.options[treatment_p];

                           if (optionTreatment_p.value == 'yes') {

                             document.getElementById('specify_treatment_p').classList.remove('d-none');

                             $('#specify_treatment_p').html(
                               `
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="treatment_p">`);

                           } else {
                             document.getElementById('specify_treatment_p').classList.add('d-none');
                           }

                         };

                         // Allergies

                         const $allergies = document.querySelector("#allergies");

                         function allergiesSpecify() {
                           const allergies = $allergies.selectedIndex;
                           if (allergies === -1) return; // Esto es cuando no hay elementos
                           const optionAllergies = $allergies.options[allergies];

                           if (optionAllergies.value == 'yes') {

                             document.getElementById('specify_allergies').classList.remove('d-none');

                             $('#specify_allergies').html(
                               `
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="allergies">`);

                           } else {
                             document.getElementById('specify_allergies').classList.add('d-none');
                           }

                         };

                         // Surgery

                         const $surgery = document.querySelector("#surgery");

                         function surgerySpecify() {
                           const surgery = $surgery.selectedIndex;
                           if (surgery === -1) return; // Esto es cuando no hay elementos
                           const optionSurgery = $surgery.options[surgery];

                           if (optionSurgery.value == 'yes') {

                             document.getElementById('specify_surgery').classList.remove('d-none');

                             $('#specify_surgery').html(
                               `
                                                        <label class="label__specify2">Specify</label>
                                                        <input class="input__specify" type="text" name="surgery">`);

                           } else {
                             document.getElementById('specify_surgery').classList.add('d-none');
                           }

                         };
                         </script>

                       </div>


                     </div>

                   </div>






                 </div>




               </div>

               <input type="hidden" name="value_step" value="next_step-4">

               <div class="step__footer">

                 <button type="button" id="btn__back-4" class="btn step__button--back" data-to_step="3"
                   data-step="4">Back</button>

                 <button type="button" id="btn__next-4" class="btn btn__next step__button--next" data-to_step="5"
                   data-step="4">Next</button>
               </div>

             </form>

           </div>

           <!-- //TODO STEP 5 -->
           <div class="step" id="step-5">
             <form action="#" method="POST" class="form__register" id="form__register" autocomplete="off"
               enctype="multipart/form-data">

               <div class="row justify-content-center">

                 <div class="col-md-9">
                   <!-- //TODO HOUSE PREFERENCES -->
                   <div class="card">
                     <div class="step__header">
                       <h2 class="title__card">IMPORTANT POINTS TO CONSIDER DURING YOUR STAY</h2>
                     </div>
                     <div class="step__body mt-3">
                       <h3 class="subtitle__section-card mb-4">FAMILY AND LOCATION</h3>
                       <p class="text__terms">Student considers that the country has a different culture and the customs
                         of your host family
                         will be different from yours. Your host family may be native, have a foreign background, or
                         come from a different Paris than the one you are traveling to. Keep in mind that the average
                         distance from the accommodation to the school can vary between 45 to 90 minutes using public
                         transport. (this will depend on your destination) It is also likely that other students live in
                         the same house and/or that the family is made up of more than 1 member. When you arrive it is
                         important that if any indication of the host family is not clear to you, you ask again until it
                         is clear. remember that they have a different way of thinking than your country. Finally you
                         should know that if during your stay you generate any damage or prejudice it will be your
                         obligation to pay or fix it.</p>
                       <h3 class="subtitle__section-card mb-4">FOOD</h3>
                       <p class="text__terms">You will receive 1,2 or 3 meals a day for 5 or 7 days a week (depending on
                         the package you chose). You will have to coordinate with the host
                         family the time of the meal (dinner) to avoid misunderstandings. In case there are days when
                         you prefer to dine out, it is important that you
                         know that there is no monetary refund for missed meals. Breakfast and lunch are normally
                         prepared by the student; however, there are
                         times when the family can prepare breakfast. Prior authorization from the family is required to
                         use the kitchen. Sometimes your host family
                         is not at home and you will have to cook on your own (you will have to coordinate with the
                         family). If you require any special food (vegetarian,
                         vegan, etc.) there will be an additional charge because it is necessary to discuss it with your
                         iHomestay advisor before registering.</p>
                       <h3 class="subtitle__section-card mb-4">INTERNET AND CELLPHONE</h3>
                       <p class="text__terms">It is important that before your trip you consider talking to your
                         telephone provider about your international plan. the internet is included in
                         the cost of accommodation, which is expected to be used responsibly by students.</p>
                       <h3 class="subtitle__section-card mb-4">KEYS AND SECURITY</h3>
                       <p class="text__terms">It is important that before your trip you consider talking to your
                         telephone provider about your international plan. the internet is included
                         in the cost of accommodation, which is expected to be used responsibly by students.</p>
                       <h3 class="subtitle__section-card mb-4">SHOWER AND LAUNDRY</h3>
                       <p class="text__terms">You can take 1 shower per day 10-15 minutes. Keep the bathroom clean and
                         tidy out of respect for the family and other inhabitants who use
                         it, possibly you will share a bathroom with a family member or other students. Remember you are
                         responsible for your personal bathroom
                         items; It is recommended that you bring your own towel. As for the laundry, you will have to
                         coordinate the logistics with your host family.
                         as well as the days and the washing schedule.</p>
                       <h3 class="subtitle__section-card mb-4">BEDROOM</h3>
                       <ul class="list__terms">
                         <li>Host family will provide you with bedding.</li>
                         <li>Your room will be private or shared depending on what you chose.</li>
                         <li>Keep your room tidy, clean and undamased.</li>
                         <li>Forbidden to have guests in your room without permission.</li>
                         <li>In case your need more space for your clothes talk with the Host Family.</li>
                         <li>Your room can be located on the first or second floor even in the basement, this depends
                           directly on the construction of the house.</li>
                       </ul>
                       <h3 class="subtitle__section-card mb-4">ADITIONAL IMPORTANT INFORMATION</h3>
                       <ol class="list__terms">
                         <li>It is not allowed to bring guests to the house unless authorized by the host family.</li>
                         <li>If you do not wish to continue your accommodation, you must notify iHomestay Canada
                           (info@ihomestaycanada.com) by email with the reasons for the change at least 2-3 weeks before
                           the date of the change.</li>
                         <li>You will have 15 days, from your arrival, not to generate any additional charge (search fee
                           and/or transfer) if a change is required. iHomestay Canada will evaluate the reasons why you
                           request the change. Sometimes you will be asked to go directly to the accommodation of the
                           educational institution iHomestay Canada relies on overseas operators. The specifications and
                           requirements of the accommodation may not be similar to your request since iHomestay Canada
                           depends on the availability of the providers and service providers with which it works.</li>
                         <li>The selection of Homestay is subject to various factors, such as the season of your trip,
                           your preferences, the school of your choice, the time between your enrollment and the start
                           of your program, among others. Therefore, keep in mind that we will always do our best to
                           select the most suitable family for you; based on all the data provided and your preferences.
                           However, we cannot guarantee that the family will be 100% compatible with your
                           requirements.</li>
                       </ol>
                       <h3 class="subtitle__section-card mb-4">AUTORIZATION</h3>
                       <p class="text__terms">I authorize iHomestay to use photographs and videos of me for
                         promotional purposes. By signing and submitting this form I accept the "iHomestay Terms,
                         Conditions and Policies".</p>
                       <!-- <h3 class="subtitle__section-card mb-4">Signature Student:</h3> -->
                       <!-- <div class="col-md-6 mx-auto">
                         <canvas id="canvas" class="div__sign"></canvas>

                         <div class="btn__div">
                           <button type="button" id="btnLimpiar" class="btn btn_clean">Clear</button>
                           <button class="" type="button" id="btnDescargar">Descargar</button>
                           <button class="" id="btnGenerarDocumento">Pasar a documento</button>
                         </div>
                         <br>

                       </div> -->
                       <!-- <p class="text__terms font-italic">(Attach a valid official identification (INE, military card,
                         passport, ID)
                       </p> -->
                       </p>

                       <div class="row justify-content-center suboption">

                         <div>

                           <h3 class="subtitle__section-card mb-4">Sign here</h3>
                           <canvas id="canvas" class="div__sign"></canvas>

                           <div class="btn__div">
                             <button type="button" id="btnLimpiar" class="btn btn_clean">Clear</button>
                             <button class="d-none" type="button" id="btnDescargar">Descargar</button>
                             <button class="d-none" id="btnGenerarDocumento">Pasar a documento</button>
                           </div>
                           <br>

                         </div>

                       </div>

                       <input type="hidden" name="signature-image" id="signature-canvas">
                     </div>
                   </div>
                 </div>
               </div>

               <input type="hidden" name="value_step" value="next_step-5">

               <div class="step__footer">

                 <button type="button" id="btn-back-5" class="btn step__button--back" data-to_step="4"
                   data-step="5">Back</button>

                 <button type="button" id="btn__register" class="btn btn__next step__button--next" data-toggle="modal"
                   data-target="#pay">Register</button>
               </div>

             </form>
           </div>

       </div>


     </div>

   </div>


   <div id="house_much" style="display: none;">
     <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
     <div class="content_popup">

       <div class="content">
         <div class="col-2">
           <img class="img-stu" src="images/icon.png" />
         </div>
         <div class="col-10" id="contentp">

         </div>
       </div>
     </div>

   </div>
   <div id="house_ass" style="display: none;">
     <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
     <div class="content_popup">

       <div class="content">
         <div class="col-2">
           <img class="img-stu" src="images/icon.png" />
         </div>
         <div class="col-9" id="assh">

         </div>
       </div>
     </div>

   </div>


   <?php include 'footer.php' ?>

   <script>
   function previewImage() {
     var file = document.getElementById("file").files;

     if (file.length > 0) {
       var fileReader = new FileReader();

       fileReader.onload = function(event) {
         document.getElementById("preview").setAttribute("src", event.target.result);
       };

       fileReader.readAsDataURL(file[0]);

       var label_1 = document.getElementById("label-i");
       var label_1_1 = document.getElementById("label-i-i");

       label_1.style.display = 'none';
       label_1_1.style.display = 'inline';

     }

   }

   function previewImageI() {
     var file = document.getElementById("file-i").files;

     if (file.length > 0) {
       var fileReader = new FileReader();

       fileReader.onload = function(event) {
         document.getElementById("preview-i").setAttribute("src", event.target.result);
         document.getElementById("preview-i").classList.remove("d-none");
       };

       fileReader.readAsDataURL(file[0]);

       var label_11 = document.getElementById("label-ii");
       var label_11_1 = document.getElementById("label-ii-i");

       label_11.style.display = 'none';
       label_11_1.style.display = 'inline';

     } else {
       document.getElementById("preview-i").classList.add("d-none");
     }

   }

   function previewImageII() {
     var file = document.getElementById("file-ii").files;

     if (file.length > 0) {
       var fileReader = new FileReader();

       fileReader.onload = function(event) {
         document.getElementById("preview-ii").setAttribute("src", event.target.result);
         document.getElementById("preview-ii").classList.remove("d-none");
       };

       fileReader.readAsDataURL(file[0]);

       var label_111 = document.getElementById("label-iii");
       var label_111_1 = document.getElementById("label-iii-i");

       label_111.style.display = 'none';
       label_111_1.style.display = 'inline';

     } else {
       document.getElementById("preview-ii").classList.add("d-none");
     }

   }
   </script>


   <script>
   document.getElementById("arrive_g").onchange = function() {
     fields()
   };

   function fields() {

     var arrive = document.getElementById("arrive_g").value;

     document.getElementById("firstd").value = arrive;
     document.getElementById("firstd2").value = arrive;
     document.getElementById("f_date").value = arrive;
     document.getElementById("h_date").value = arrive;

   }

   document.getElementById("lastd").onchange = function() {
     calWeek()
   };

   function calWeek() {

     var firstd = document.getElementById("firstd").value;
     var lastd2 = document.getElementById("lastd").value;

     document.getElementById("firstd2").value = firstd;
     document.getElementById("lastd2").value = lastd2;

     //$('#calcWeeks').html(firstd);

     const date1 = new Date(firstd);
     const date2 = new Date(lastd2);
     const diffTime = Math.abs(date2 - date1);
     const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

     const diffWeeksD = diffDays / 7;

     if (diffWeeksD % 1 == 0) {

       const diffWeeks = diffDays / 7;

       $('#calcWeeks').html(diffWeeks + " weeks accommodation");

     } else {

       const diffWeeks = Math.floor(diffDays / 7);
       const diffDays2 = Math.floor(diffDays - diffWeeks * 7);

       $('#calcWeeks').html(diffWeeks + " weeks and " + diffDays2 + " days accommodation");

     }

   }


   function modalBooking() {
     $('#booking_fee').modal('show');
   }


   /* function myFunction(x) {
   if (x.matches) { // If media query matches
       document.getElementById('div__flex').classList.remove('col-md-4');
       document.getElementById('div__flex').classList.remove('col-md-8');
   } else {}
   }

   var x = window.matchMedia("(max-width: 767px)")
   myFunction(x) // Call listener function at run time
   x.addListener(myFunction) // Attach listener function on state changes 
   */
   </script>

   <!-- Daterangepicker -->

   <script>
   $(function() {

     var arrive_db = document.getElementById('date_arrive_g').value;
     var arrive_g = document.getElementById("arrive_g").value;


     $('#arrive_g').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       minDate: moment().startOf('hour'),
       startDate: arrive_db,
       locale: {
         format: 'MM/DD/YYYY'
       }
     });

     $('#firstd').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       minDate: moment().startOf('hour'),
       startDate: arrive_db,
       locale: {
         format: 'MM/DD/YYYY'
       }
     });

     var firstd = document.getElementById('firstd').value;

     $('#lastd').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       minDate: firstd,
       startDate: moment().startOf('hour'),
       locale: {
         format: 'MM/DD/YYYY'
       }
     });


     $('#f_date').daterangepicker({
       singleDatePicker: true,
       timePicker: true,
       minDate: moment().startOf('hour').add(32, 'hour'),
       startDate: moment().startOf('hour').add(32, 'hour'),
       endDate: moment().startOf('hour').add(32, 'hour'),
       locale: {
         format: 'MM/DD/YYYY hh:mm A'
       }
     });

     $('#h_date').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       minDate: arrive_g,
       startDate: arrive_g,
       locale: {
         format: 'MM/DD/YYYY'
       }
     });

     $('#db').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       showDropdowns: true,
       maxDate: moment().startOf('hour'),
       startDate: moment().startOf('hour'),
       locale: {
         format: 'MM/DD/YYYY'
       }
     });

     $('#exp_pass').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       showDropdowns: true,
       startDate: moment().startOf('hour'),
       locale: {
         format: 'MM/DD/YYYY'
       }
     });

     $('#bl').daterangepicker({
       autoApply: true,
       singleDatePicker: true,
       showDropdowns: true,
       startDate: moment().startOf('hour'),
       locale: {
         format: 'MM/DD/YYYY'
       }
     });
   });
   </script>

   <!-- WRAPPER
    =================================================================================================================-->

   <!--end page-->

   <script src="assets/js/jquery-3.3.1.min.js"></script>
   <script src="assets/js/popper.min.js"></script>
   <script src="assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="assets/js/leaflet.js"></script>
   <script src="assets/js/jQuery.MultiFile.min.js"></script>
   <script src="assets/js/custom.js"></script>
   <script src="assets/js/map-leaflet.js"></script>
   <script src="assets/js/validate_ihomestay_student_register.js?ver=1.2"></script>

   <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


 </body>

 </html>