<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>i Homestay Stay Canada - homestay in Canada for students</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/iHomestay/css/ihomestay.css">
</head>
<body>

    <header id="ts-header" class="fixed-top">
        <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light" style="background-color: #fff; box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.75); -webkit-box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.75);">
            <div class="container">

                <!--Brand Logo/ Logo HOMEBOR-->
                <a class="navbar-brand" href="index.php" style="margin-left:0">
                    <img src="assets/iHomestay/logos/iHomestay_logo.png" class="logo_white" alt="" >
                </a>

                <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        &nbsp;&nbsp;<li class="nav-item" style="list-style: none;">
                            <a class="nav-link zoom logout" href="../logout.php" id="nav" style="color:#fff">Logout</a>
                        </li>


                </ul>

            </div>

        </nav>

    </header>

    <main class="img__fon m-0">

        <div class="ihomestay__banner">

            <div class="opacity_banner"></div>

            <div class="div__banner">
                
                <div class="content__banner">
                    <h1 class="title__banner">Canadian</h1>
                    <h1 class="title__banner">Accommodation</h1>
                    <h1 class="title__banner mb__btn">Services</h1>


                    <a href="#" class="ihomestay_btn__register"> Apply for Accommodation </a>
                </div>

            </div>

        </div>


    </main>

    <main class="main__content bottom__footer">

        <!-- Content -->

        <div class="div__content">

            <div class="row div__icons__content">
                
                <div class="div__icons house__icon_div col-md-4">

                    <div class="house__icon"></div>
                    <h4 class="title__icon_div"> Accommodation </h4>
                    <p class="text__icon_div">Starting at $950 per month. </p>

                </div>

                <div class="div__icons wcare__icon_div col-md-4">

                    <span class="wcare__icon"></span>
                    <h4 class="title__icon_div"> We care </h4>
                    <p class="text__icon_div">We provide unparalled customer service. </p>

                </div>

                <div class="div__icons service__icon_div col-md-4">

                    <div class="service__icon"></div>
                    <h4 class="title__icon_div"> Other Services  </h4>
                    <p class="text__icon_div"> Education is the best way to extend your stay in Canada. </p>

                </div>

            </div>

            <div class="content__div">
                
                <div class="col-md-12 text_content">
                    <h3 class="title__text__content">We Provide The Best Experience To All Students</h3>
                    <p class="description__content mb-3">iHomestay Canada is committed to provide the best experience all students in our accommodations program, with host families who are also committed in keeping a high standard of care to all our students.</p>
                    
                    <p class="description__content mb-5">The health and safety of our employees, students and accomodation providers are our top priority.</p>

                    <div class="div__btn_register">

                        <a href="register" class="btn__register_description btn__homestay"> Homestay Accomodation </a>

                    </div>

                </div>
                <div class="col-md-12 image_content">

                    <img src="assets/iHomestay/images/house_description.jpg" class="image__description"></img>
                    
                </div>

            </div>
            
            <div class="content__div column2">

                <div class="col-md-12 image_content">

                    <img src="assets/iHomestay/images/hostFamily.webp" class="image__description description2"></img>

                </div>
                
                <div class="col-md-10 text_content">
                    <h3 class="title__text__content">Becoming a Host Family Has Been A Joy</h3>
                    <p class="description__content mb-5">These students brought joy and laughter into our home, and it truly feels like they are a part of our biological family. The most precious part of this experience is that we now have forever family all over the world whom we can visit at anytime.</p>

                    <div class="div__btn_register">

                        <a href="register" class="btn__register_description btn__homestay"> Host a Student </a>

                    </div>

                </div>

            </div>

        </div>
    
    </main>


    
</body>
</html>