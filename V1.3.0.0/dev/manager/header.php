
    <!-- WRAPPER - FONDO
    =================================================================================================================-->

    <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
    <header id="ts-header" class="fixed-top">
        <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
        <link rel="stylesheet" type="text/css" href="assets/css/header.css">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container">

                <!--Brand Logo/ Logo HOMEBOR-->
                <a class="navbar-brand" href="index">
                    <img src="../assets/logos/page.png" alt="">
                </a>

                 <?php include 'notification-after.php'; ?>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse header_m" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav">

                        <!--HOME (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link active" href="index" id="active">
                                Main
                                <span class="sr-only">(current)</span>
                            </a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- MAP (1st level)
                                =====================================================================================-->
                                <li class="nav-item ts-has-child">

                                    <a href="#" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Acomodation Provider</p>
                                    </a>

                                    <!--List (2nd level) -->
                                    <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="edit_manager" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Edit Info </p>
                                            </a>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="agency_info" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Manager Profile </p>
                                            </a>

                                        </li>

                                        <li class="nav-item">

                                            <a href="agency_delete" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Disable Account </p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>
                                    <!--end List (2nd level)-->

                                 <!-- MAP (1st level)
                                =====================================================================================-->
                                <li class="nav-item ts-has-child">

                                    <a href="#" class="nav-link pl-2 pr-2" id="nav">
                                        <p class="bar-ii zoom"> Coordinator </p>
                                    </a>

                                    <!--List (2nd level) -->
                                    <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="agent_profile" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Register a Coordinator </p>
                                            </a>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="edit_agents" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Edit Coordinator </p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>
                                    <!--end List (2nd level)-->

                                <!--end SLIDER (1st level)-->

                            </ul>
                            <!--end List (1st level) -->

                        </li>
                        <!--end HOME nav-item-->

                        <!--LISTING (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link" href="edit_propertie" id="nav">Homestay</a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- CATEGORY ICONS (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="edit_propertie" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Homestay Directory </p>
                                    </a>

                                </li>
                                <!--end CATEGORY ICONS (1st level)-->

                                <!-- GRID (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="homestay" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Register New Homestay </p>
                                    </a>

                                </li>
                                <!--end GRID (1st level)-->

                            </ul>
                            <!--end List (1st level) -->

                        </li>
                        <!--end LISTING nav-item-->


                        <!--PAGES (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link" href="edit_students" id="nav">Students</a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- AGENCY (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="edit_students" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Students Directory</p>
                                    </a>

                                </li>
                                <!--end AGENCY (1st level)-->

                                <!-- EDIT PROPERTY (1st level)
                                =====================================================================================-->
                                <li class="nav-item">
                                    <a href="waiting_confirmation" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Awaiting Confirmation</p>
                                    </a>
                                    
                                </li>

                                <!--end EDIT PROPERTY (1st level)-->

                                <li class="nav-item">
                                    <a href="student_profile" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Register a New Student </p>
                                    </a>
                                </li>

                            </ul>
                            <!--end List (1st level) -->

                        </li>
                        <!--end PAGES nav-item-->
                        
                        <!--PAGES (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link" href="edit_students" id="nav">Academies</a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- AGENCY (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="#" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Academy List</p>
                                    </a>

                                </li>
                                
                                <li class="nav-item">

                                    <a href="academy_register" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Academy Resgister</p>
                                    </a>

                                </li>
                                <!--end AGENCY (1st level)-->

                            </ul>
                            <!--end List (1st level) -->

                        </li>
                        <!--end PAGES nav-item-->



                        <!--ABOUT US (Main level)
                        =============================================================================================-->
                        
                        <li class="nav-item ts-has-child">
                            <a class="nav-link" href="#" id="nav">Work With Us</a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- AGENCY (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="work_with_us" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Plan</p>
                                    </a>

                                </li>
                                <!--end AGENCY (1st level)-->

                                <!-- EDIT PROPERTY (1st level)
                                =====================================================================================-->

                                <li class="nav-item">
                                    <a href="voucher.php" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Vouchers</p>
                                    </a>
                                </li>
                                <!--end EDIT PROPERTY (1st level)-->

                            </ul>
                            <!--end List (1st level) -->
                        </li>

                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link" href="#" id="nav">Status</a>

                            <!-- List (1st level) -->
                            <ul class="ts-child">

                                <!-- ACTIVITY LOG (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a class="nav-link pl-2 pr-2" href="activity" id="nav">
                                        <p class="bar-ii zoom"> Activity Log </p>
                                    </a>

                                </li>
                                <!--end ACTIVITY LOG (1st level)-->

                                <!-- REPORTS (1st level)
                                =====================================================================================-->
                                <li class="nav-item">
                                    <a href="reports" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom">Reports</p>
                                    </a>
                                </li>
                                <!--end REPORTS (1st level)-->

                                <li class="nav-item ts-has-child">

                                    <a href="#" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Payments List </p>
                                    </a>

                                    <!--List (2nd level) -->
                                    <ul class="ts-child">

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="homestay_payments" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Homestay Payments</p>
                                            </a>

                                        </li>

                                        <li class="nav-item">

                                            <a href="#" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Student Payments</p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>

                                </li>

                            </ul>
                            <!--end List (1st level) -->

                        </li>

                        <!--CONTACT (Main level)
                        =============================================================================================-->
                        
                        <!--end CONTACT nav-item-->

                        <?php include 'notification-before.php' ?>

                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link zoom" href="../logout" id="nav">Logout</a>
                        </li>
                    </ul>
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->

    </header>
