 <?php
    require '../xeon.php';
    session_start();

    include 'count.php';

// SESSION VARIABLE
$usuario = $_SESSION['username'];


//USER VERIFICATION

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    if ($row['usert'] != 'Manager') {
        header("location: ../logout");
    }

$query2="SELECT * FROM manager WHERE mail = '$usuario'";
$resultado=$link->query($query2);

$row2=$resultado->fetch_assoc();

if ($row2['plan'] == 'No Plan') {
        header("location: work_with_us");
    }

$query3 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.mail = '$usuario' and academy.id_ac = manager.n_a");
$row3 = $query3->fetch_assoc();

$query4 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.mail = '$usuario' and academy.id_ac = manager.n_a2");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.mail = '$usuario' and academy.id_ac = manager.n_a3");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.mail = '$usuario' and academy.id_ac = manager.n_a4");
$row6 = $query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.mail = '$usuario' and academy.id_ac = manager.n_a5");
$row7 = $query7->fetch_assoc();

//TOTAL OF STUDENTS
$sql2 = "SELECT * FROM pe_student WHERE n_a = $row3[id_ac]";  
$connStatus2 = $link->query($sql2); 
$numberOfRows2 = mysqli_num_rows($connStatus2);


$sql3 = "SELECT * FROM pe_student WHERE n_a = $row4[id_ac]";  
$connStatus3 = $link->query($sql3); 
$numberOfRows3 = mysqli_num_rows($connStatus3); 


$sql4 = "SELECT * FROM pe_student WHERE n_a = $row5[id_ac]";  
$connStatus4 = $link->query($sql4);  
$numberOfRows4 = mysqli_num_rows($connStatus4); 

$sql5 = "SELECT * FROM pe_student WHERE n_a = $row6[id_ac]";  
$connStatus5 = $link->query($sql5); 
$numberOfRows5 = mysqli_num_rows($connStatus5);

$sql6 = "SELECT * FROM pe_student WHERE n_a = $row7[id_ac]";  
$connStatus6 = $link->query($sql6); 
$numberOfRows6 = mysqli_num_rows($connStatus6);  

//TOTAL OF HOMESTAY
$sql = "SELECT * FROM propertie_control WHERE agency = $row2[id_m]"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/index.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Accomodation Provider</title>

</head>
<body>


 <div class="ts-page-wrapper ts-homepage" id="page-top">
<!--HEADER ============================================================================-->
        <?php 
            include 'header.php';
        ?>

<!--MAIN ============================================================================-->
    <main id="ts-main">
        <br>
        <br>
        <br>
            <section id="page-title">
                <div class="container">

                    <div class="ts-title">
                    </div>

                </div>
            </section>

<!--AGENCY INFO =========================================================================================================-->
         <section id="agency-info">
            <div class="container">

                <!--Box-->
                <div class="ts-box">

                    <!--Ribbon-->
                    <div class="ts-ribbon">
                        <i class="fa fa-star"></i>
                    </div>

                    <!--Row-->
                    <div class="row">

                        <!--Brand-->
                        <div class="col-md-4 ts-center__both">
                            <div class="ts-circle__xxl ts-shadow__md">
                                <?php if($row2['photo'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'  
                                                    <img src="../'.$row2['photo'].'" alt="" width="200px"/>';}?>
                            </div>
                        </div>

                        <!--Description-->
                        <div class="col-md-8">

                            <div class="py-4">

                                <!--Title-->
                                 <?php if($row2['a_name'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'  
                                                    <div class="ts-title mb-2">
                                    <h2 class="mb-1">'.$row2['a_name'].'</h2>
                                    <h5>
                                        <i class="fa fa-map-marker mr-2"></i>
                                        '.$row2['city'].'
                                    </h5>
                                </div>';}?>

                                <!--Row-->
                                <div class="row">
                                    <?php if($row2['des'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'  
                                                    <div class="col-md-7">
                                        <p>
                                           '.$row2['des'].'
                                        </p>
                                    </div>';}?>


                                    <div class="col-md-5 ts-opacity__50">

                                        <!--Phone-->

                                        <figure class="mb-1">
                                            <i class="fa fa-phone-square mr-2"></i>
                                            <?php if($row2['num'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'
                                           '.$row2['num'].'
                                        ';}?>
                                        </figure>

                                        <!--Mail-->
                                        <figure class="mb-1">
                                            <i class="fa fa-envelope mr-2"></i>
                                            <?php if($row2['mail'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'
                                           '.$row2['mail'].'
                                        ';}?>
                                        </figure>

                                        <!--Skype-->
                                        <figure class="mb-0">
                                            <i class="fa fa-envelope mr-2"></i>
                                            <?php if($row2['mail2'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'
                                           '.$row2['mail2'].'
                                        ';}?>
                                        </figure>

                                    </div>

                                </div>
                                <!--end row-->
                            </div>
                            <!--end p-4-->

                            <div class="ts-bg-light p-3 ts-border-radius__md d-block d-sm-flex ts-center__vertical justify-content-between ts-xs-text-center">
                                <a href="edit_propertie.php" id="allp" class="btn btn-outline-secondary btn-sm d-block d-sm-inline-block mb-2 mb-sm-0">All My
                                    Properties (<?php echo "$numberOfRows"?>)</a>
                                <small class="ts-opacity__50">Member since: <?php if($row2['db'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'
                                           '.$row2['db'].'
                                        ';}?></small>
                            </div>

                        </div>
                        <!--end col-md-8-->
                    </div>
                    <!--end row-->
                </div>
                <!--end ts-box-->

            </div>
            <!--end container-->
        </section>
        <!--end #agency-info-->

        <!--ITEMS LISTING & SEARCH
            =========================================================================================================-->
        <section id="items-listing-and-search">
            <div class="container">
                <div class="row flex-wrap-reverse">

                    <!--LEFT SIDE
                        =============================================================================================-->
                    <div class="col-md-12 text-center">

                        <center>

                        <div class="col-md-10 text-center">

                            <!--PROPERTIES & AGENTS TABS
                                =========================================================================================-->
                            <section id="properties-agents-tab" class="pb-2 mb-3">
                                <ul class="nav nav-tabs mb-1" id="login-register-tabs" role="tablist">

                                    <!--Properties tab-->
                                    <li class="nav-item">
                                        <a class="nav-link active" id="properties-tab" data-toggle="tab" href="#properties" role="tab" aria-controls="properties" aria-selected="true">
                                            <h3>Homestays</h3>
                                        </a>
                                    </li>

                                    <!--Agents tab-->
                                    <li class="nav-item">
                                        <a class="nav-link" id="agents-tab" data-toggle="tab" href="#agents" role="tab" aria-controls="agents" aria-selected="false">
                                            <h3>Coordinators</h3>
                                        </a>
                                    </li>

                                    <!--Students tab-->
                                    <li class="nav-item">
                                        <a class="nav-link" id="agents-tab" data-toggle="tab" href="#students" role="tab" aria-controls="agents" aria-selected="false">
                                            <h3>Students</h3>
                                        </a>
                                    </li>

                                </ul>
                            </section>

                            <!--TAB CONTENT
                                =========================================================================================-->
                            <div class="tab-content">

                            <!--PROPERTIES TAB
                                    =====================================================================================-->
                            <div class="tab-pane active" id="properties" role="tabpanel" aria-labelledby="properties-tab">

                                    <div class="container">
                                    <div class="card">
                                        <div class="card-body">
                                        <div class="form-group">
                                            <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" hidden>
                                        </div>
                                        <div class="table-responsive" id="dynamic_content">
                                            
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                
                                </div>
                                <!--end #properties-->

                                <!--Students TAB
                                    =====================================================================================-->
                                <div class="tab-pane" id="students" role="tabpanel" aria-labelledby="students-tab">

                                    <!--STUDENTS LISTING
                                        =================================================================================-->
                                    <section id="items-listing">
                                        <div class="container">
                                        <div class="card">
                                            <div class="card-body">
                                            <div class="form-group">
                                                <input type="text" name="search_box2" id="search_box2" class="form-control" placeholder="Type your search query here" hidden>
                                            </div>
                                            <div class="table-responsive" id="dynamic_content2">
                                                
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                </div>
                                <!--end #properties-->

                                <!--AGENTS TAB
                                    =====================================================================================-->
                                <div class="tab-pane" id="agents" role="tabpanel" aria-labelledby="agents-tab">

                                    <!--AGENTS LISTING
                                        =================================================================================-->
                                    <section id="agents-listing">
                                        <div class="row">
                                            <div class="container">
                                            <div class="card">
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <input type="text" name="search_box3" id="search_box3" class="form-control" placeholder="Type your search query here" hidden>
                                                </div>
                                                <div class="table-responsive" id="dynamic_content3">
                                                    
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!--end row-->
                                    </section>

                                </div>
                                <!--end #agents-->

                            </div>
                            <!--end tab-content-->
                        </div>
                        </center>

                    </div>
                    <!--end col-md-8 / left side -->
                </div>
            </div>
        </section>
    </div>
</div>
<!--FOOTER ===============================================================-->
    <?php 
        include 'footer.php';
    ?>
<!--end page-->
<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.magnific-popup.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/index_ajax.js"></script>
</body>
</html>