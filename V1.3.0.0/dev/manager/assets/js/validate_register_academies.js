const formulary = document.getElementById('form__academy');

const inputs = document.querySelectorAll('#form__academy input');

const expresiones = {
    // Basic Information

    name_a: /^[a-zA-ZÀ-ÿ0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    dir: /^[a-zA-Z0-9\_\-\s\°\,\.]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s\,\.]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s\,\.]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s\,\.]{3,10}$/,
}

const campos = {
    name_a: false,
    dir: false,
    city: false,
    state: false
}

const validateFormulary = (e) =>{
    switch(e.target.name){

        // Required fields
        
        case "name_a":
            validateField(expresiones.name_a, e.target, 'name_a');
        break;
        
        case "dir":
            validateField(expresiones.dir, e.target, 'dir');
        break;
        
        case "city":
            validateField(expresiones.city, e.target, 'city');
        break;
        
        case "state":
            validateField(expresiones.state, e.target, 'state');
        break;
        
        case "p_code":
            validateField(expresiones.p_code, e.target, 'p_code');
        break;


        // Optional Fields

        case "num_s":
            validateField(expresiones.phone, e.target, 'num_s');
        break;

    }
}

const validateField = (expresion, input, campo) => {
    if(expresion.test(input.value)){
        document.getElementById(`group__${campo}`).classList.remove('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.remove('form__group__input-error-active');
        campos[campo] = true;
    } else{
        document.getElementById(`group__${campo}`).classList.add('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.add('form__group__input-error-active');
        campos[campo] = false;
    }
}

inputs.forEach((input) =>{
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
})

