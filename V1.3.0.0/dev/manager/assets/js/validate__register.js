const formulary = document.getElementById('form');

const inputs = document.querySelectorAll('#form input');
const textarea = document.querySelectorAll('#form textarea');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^\d{1,2}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
    message: /^[a-zA-ZÀ-ÿ0-9\s\W_.+-]{1,500}$/ // Description message.
}


const campos = {
    name: false,
    l_name: false,
    dir: false,
    city: false,
    state: false,
    p_code: false,
    a_name: false,
    num: false,
}


const validateFormulary = (e) =>{
    switch(e.target.name){
        case "name":
            validateField(expresiones.nombre, e.target, 'name');
        break;

        case "l_name":
            validateField(expresiones.nombre, e.target, 'l_name');
        break;
        
        case "dir":
            validateField(expresiones.message, e.target, 'dir');
        break;
        
        case "city":
            validateField(expresiones.message, e.target, 'city');
        break;
        
        case "state":
            validateField(expresiones.message, e.target, 'state');
        break;
        
        case "p_code":
            validateField(expresiones.message, e.target, 'p_code');
        break;
        
        case "a_name":
            validateField(expresiones.message, e.target, 'a_name');
        break;
        
        case "num":
            validateField(expresiones.phone, e.target, 'num');
        break;
        
        
        
        case "mail2":
            validateField(expresiones.correo, e.target, 'mail2');
        break;
        
        case "des":
            validateField(expresiones.message, e.target, 'des');
        break;

    }
}

const validateField = (expresion, input, campo) => {
    if(expresion.test(input.value)){
        document.getElementById(`group__${campo}`).classList.remove('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.remove('form__group__input-error-active');
        campos[campo] = true;
    } else{
        document.getElementById(`group__${campo}`).classList.add('form__group-incorrect');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.add('form__group__input-error-active');
        campos[campo] = false;
    }
}


inputs.forEach((input) =>{
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
});

textarea.forEach((textarea) =>{
    textarea.addEventListener('keyup', validateFormulary);
    textarea.addEventListener('blur', validateFormulary);
});


formulary.addEventListener('submit', (e) =>{
    e.preventDefault();

    document.getElementById('btn_').classList.add('btn_none');
    document.getElementById('btn__success').classList.add('spin');

    if(campos.name && campos.l_name ){

        var registerForm = new FormData($('#form')[0]);

        $.ajax({
            url: 'action.php',
            type: 'POST',
            data: registerForm,
            contentType: false,
            processData: false,
            success: function(response) {
                
                document.getElementById('btn__success').classList.remove('spin');
                
                let register = JSON.parse(response);

                register.forEach(reg => {
                    if(reg.register == 'yes'){
                        window.top.location = 'index';
                    }
                    else if(reg.register == 'no'){
                        alert('Connection Error');
                    }

                });


            }
        });

    }else{
        alert('fallo'+response);
    }

});