//CORDINATOR PANEL LIST
$(document).ready(function() {

  load_data(1);

  function load_data(page, query = '') {
      $.ajax({
          url: "fetch_paymentagency.php",
          method: "POST",
          data: { page: page, query: query },
          success: function(data) {
              $('#dynamic_content').html(data);
          }
      });
  }

  $('#vou').hide();

  $('#search_box').keyup(function() {

      if ($('#search_box').val()) {

          $('#dynamic_content').hide();
          $('#vou').show();

          let search = $('#search_box').val();

          var searchLength = search.length;

          $.ajax({
              url: 'payp-search.php',
              type: 'POST',
              data: { search },
              success: function(response) {
                  let payp = JSON.parse(response);
                  let template = '';

                  payp.forEach(paypro => {

                    
                      template += `
              <tr>

            <th style="font-weight:normal"> ${paypro.id_p} </th>
            <th style="font-weight:normal"> ${paypro.db} </th>
            <th style="font-weight:normal"> ${paypro.client} </th>
            <th style="font-weight:normal"> ${paypro.bank} </th>
            <th style="font-weight:normal"> ${paypro.plan} </th>
            <th style="font-weight:normal"> ${paypro.status} </th>
            <th align="left">
            <ul>
                 <a id="edit" href="../${paypro.photo}" class="btn btn-primary btn-sm btn-block float-right" name="edit" target="_blank">
                              <i class="fa fa-eye mr-2"></i>      
                                 See Document
                                </a>
        </ul>
            </th>

          </tr>`

                    
                   
            
                      
        });


                  $('#vouchers').html(template);
              }
          });
      } else {
          $('#dynamic_content').show();
          $('#vou').hide();
      }

  });

  $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
  });

});