//CORDINATOR PANEL LIST
$(document).ready(function() {

  load_data(1);

  function load_data(page, query = '') {
      $.ajax({
          url: "activity_fecth.php",
          method: "POST",
          data: { page: page, query: query },
          success: function(data) {
              $('#dynamic_content').html(data);
          }
      });
  }

  $('#vou').hide();

  $('#search_box').keyup(function() {

      if ($('#search_box').val()) {

          $('#dynamic_content').hide();
          $('#vou').show();

          let search = $('#search_box').val();

          var searchLength = search.length;

          $.ajax({
              url: 'act-search.php',
              type: 'POST',
              data: { search },
              success: function(response) {
                  let act = JSON.parse(response);
                  let template = '';

                  act.forEach(activity => {
                      template += `
          <tr>

            <th style="font-weight:normal"> ${activity.id_act} </th>
            <th style="font-weight:normal"> ${activity.user} </th>
            <th style="font-weight:normal"> ${activity.dates} </th>
            <th style="font-weight:normal"> ${activity.activity} </th>
            <th style="font-weight:normal"> ${activity.edit_user} </th>

          </tr>`
                  });


                  $('#vouchers').html(template);
              }
          });
      } else {
          $('#dynamic_content').show();
          $('#vou').hide();
      }

  });

  $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
  });

});