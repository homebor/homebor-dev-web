
        var btn_1 = document.getElementById('iii');
        var btn_1_1 = document.getElementById('iii-i');
        var area_iii = document.getElementById('area-iii');

        var btn_2 = document.getElementById('iv');
        var btn_21 = document.getElementById('iv-i');
        var area_iv = document.getElementById('area-iv');


        function mostrarBoton_1 () {
            btn_1.style.display = 'none';
            btn_1_1.style.display = 'inline';
            area_iii.style.display = 'inline';
        }

        function hidBoton_1 () {
            btn_1.style.display = 'inline';
            btn_1_1.style.display = 'none';
            area_iii.style.display = 'none';
            area_iv.style.display = 'none';
        }

        function mostrarBoton_2 () {
            btn_2.style.display = 'none';
            btn_21.style.display = 'inline';
            area_iv.style.display = 'inline';
        }

        function hidBoton_2 () {
            btn_2.style.display = 'inline';
            btn_21.style.display = 'none';
            area_iv.style.display = 'none';
        }





        /* bathroom photo features */

        var btn_4 = document.getElementById('vi');
        var btn_41 = document.getElementById('vi-i');
        var bath_iii = document.getElementById('bath-iii');

        var btn_5 = document.getElementById('vii');
        var btn_51 = document.getElementById('vii-i');
        var bath_iv = document.getElementById('bath-iv');

        var btn_6 = document.getElementById('viii');
        var btn_61 = document.getElementById('viii-i');

        function mostrarBoton_4 () {
            btn_4.style.display = 'none';
            btn_41.style.display = 'inline';
            bath_iii.style.display = 'inline';
        }

        function hidBoton_4 () {
            btn_4.style.display = 'inline';
            btn_41.style.display = 'none';
            bath_iii.style.display = 'none';
            bath_iv.style.display = 'none';
        }

        function mostrarBoton_5 () {
            btn_5.style.display = 'none';
            btn_51.style.display = 'inline';
            bath_iv.style.display = 'inline';
        }

        function hidBoton_5 () {
            btn_5.style.display = 'inline';
            btn_51.style.display = 'none';
            bath_iv.style.display = 'none';
        }

        function mostrarBoton_6 () {
            btn_6.style.display = 'none';
            btn_61.style.display = 'inline';
        }

        function hidBoton_6 () {
            btn_6.style.display = 'inline';
            btn_61.style.display = 'none';
        }






        /* bedroom photo features */

        var btn_7 = document.getElementById('ix');
        var btn_71 = document.getElementById('ix-i');

        var btn_8 = document.getElementById('x');
        var btn_81 = document.getElementById('x-i');
        var bed_iii = document.getElementById('bed-iii');

        var btn_9 = document.getElementById('xi');
        var btn_91 = document.getElementById('xi-i');
        var bed_iv = document.getElementById('bed-iv');

        var btn_10 = document.getElementById('xii');
        var btn_101 = document.getElementById('xii-i');
        var bed_v = document.getElementById('bed-v');

        var btn_11 = document.getElementById('xiii');
        var btn_111 = document.getElementById('xiii-i');
        var bed_vi = document.getElementById('bed-vi');

        var btn_12 = document.getElementById('xiv');
        var btn_121 = document.getElementById('xiv-i');
        var bed_vii = document.getElementById('bed-vii');

        var btn_13 = document.getElementById('xv');
        var btn_131 = document.getElementById('xv-i');
        var bed_viii = document.getElementById('bed-viii');

        function mostrarBoton_7 () {
            btn_7.style.display = 'none';
            btn_71.style.display = 'inline';
            bed_iii.style.display = 'inline';
        }

        function hidBoton_7 () {
            btn_7.style.display = 'inline';
            btn_71.style.display = 'none';
            bed_iii.style.display = 'none';
            bed_iv.style.display = 'none';
            bed_v.style.display = 'none';
            bed_vi.style.display = 'none';
            bed_vii.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_8 () {
            btn_8.style.display = 'none';
            btn_81.style.display = 'inline';
            bed_iv.style.display = 'inline';
        }

        function hidBoton_8 () {
            btn_8.style.display = 'inline';
            btn_81.style.display = 'none';
            bed_iv.style.display = 'none';
            bed_v.style.display = 'none';
            bed_vi.style.display = 'none';
            bed_vii.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_9 () {
            btn_9.style.display = 'none';
            btn_91.style.display = 'inline';
            bed_v.style.display = 'inline';
        }

        function hidBoton_9 () {
            btn_9.style.display = 'inline';
            btn_91.style.display = 'none';
            bed_v.style.display = 'none';
            bed_vi.style.display = 'none';
            bed_vii.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_10 () {
            btn_10.style.display = 'none';
            btn_101.style.display = 'inline';
            bed_vi.style.display = 'inline';
        }

        function hidBoton_10 () {
            btn_10.style.display = 'inline';
            btn_101.style.display = 'none';
            bed_vi.style.display = 'none';
            bed_vii.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_11 () {
            btn_11.style.display = 'none';
            btn_111.style.display = 'inline';
            bed_vii.style.display = 'inline';
        }

        function hidBoton_11 () {
            btn_11.style.display = 'inline';
            btn_111.style.display = 'none';
            bed_vii.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_12 () {
            btn_12.style.display = 'none';
            btn_121.style.display = 'inline';
            bed_viii.style.display = 'inline';
        }

        function hidBoton_12 () {
            btn_12.style.display = 'inline';
            btn_121.style.display = 'none';
            bed_viii.style.display = 'none';
        }

        function mostrarBoton_13 () {
            btn_13.style.display = 'none';
            btn_131.style.display = 'inline';
        }

        function hidBoton_13 () {
            btn_13.style.display = 'inline';
            btn_131.style.display = 'none';
        }


function previewImage_room1() {
        var file = document.getElementById("room-i-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-i-i");
            var label_1_1 = document.getElementById("rphoto-i-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room1_2() {
        var file = document.getElementById("room-i-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-i-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-i-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-i-ii");
            var label_1_1 = document.getElementById("rphoto-i-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room1_3() {
        var file = document.getElementById("room-i-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-i-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-i-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-i-iii");
            var label_1_1 = document.getElementById("rphoto-i-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room2() {
        var file = document.getElementById("room-ii-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-ii-i");
            var label_1_1 = document.getElementById("rphoto-ii-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room2_2() {
        var file = document.getElementById("room-ii-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-ii-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-ii-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-ii-ii");
            var label_1_1 = document.getElementById("rphoto-ii-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room2_3() {
        var file = document.getElementById("room-ii-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-ii-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-ii-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-ii-iii");
            var label_1_1 = document.getElementById("rphoto-ii-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }



function previewImage_room3() {
        var file = document.getElementById("room-iii-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iii-i");
            var label_1_1 = document.getElementById("rphoto-iii-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room3_2() {
        var file = document.getElementById("room-iii-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iii-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iii-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iii-ii");
            var label_1_1 = document.getElementById("rphoto-iii-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room3_3() {
        var file = document.getElementById("room-iii-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iii-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iii-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iii-iii");
            var label_1_1 = document.getElementById("rphoto-iii-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room4() {
        var file = document.getElementById("room-iv-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iv");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iv").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iv-i");
            var label_1_1 = document.getElementById("rphoto-iv-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room4_2() {
        var file = document.getElementById("room-iv-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iv-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iv-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iv-ii");
            var label_1_1 = document.getElementById("rphoto-iv-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room4_3() {
        var file = document.getElementById("room-iv-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-iv-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-iv-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-iv-iii");
            var label_1_1 = document.getElementById("rphoto-iv-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room5() {
        var file = document.getElementById("room-v-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-v");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-v").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-v-i");
            var label_1_1 = document.getElementById("rphoto-v-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room5_2() {
        var file = document.getElementById("room-v-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-v-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-v-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-v-ii");
            var label_1_1 = document.getElementById("rphoto-v-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room5_3() {
        var file = document.getElementById("room-v-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-v-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-v-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-v-iii");
            var label_1_1 = document.getElementById("rphoto-v-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room6() {
        var file = document.getElementById("room-vi-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vi");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vi").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vi-i");
            var label_1_1 = document.getElementById("rphoto-vi-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room6_2() {
        var file = document.getElementById("room-vi-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vi-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vi-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vi-ii");
            var label_1_1 = document.getElementById("rphoto-vi-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room6_3() {
        var file = document.getElementById("room-vi-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vi-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vi-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vi-iii");
            var label_1_1 = document.getElementById("rphoto-vi-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room7() {
        var file = document.getElementById("room-vii-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vii-i");
            var label_1_1 = document.getElementById("rphoto-vii-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room7_2() {
        var file = document.getElementById("room-vii-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vii-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vii-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vii-ii");
            var label_1_1 = document.getElementById("rphoto-vii-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room7_3() {
        var file = document.getElementById("room-vii-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-vii-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-vii-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-vii-iii");
            var label_1_1 = document.getElementById("rphoto-vii-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


function previewImage_room8() {
        var file = document.getElementById("room-viii-i").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-viii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-viii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-viii-i");
            var label_1_1 = document.getElementById("rphoto-viii-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room8_2() {
        var file = document.getElementById("room-viii-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-viii-ii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-viii-ii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-viii-ii");
            var label_1_1 = document.getElementById("rphoto-viii-ii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }

function previewImage_room8_3() {
        var file = document.getElementById("room-viii-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-room-viii-iii");
            hid.style.display = 'inline';

            fileReader.onload = function (event) {
                document.getElementById("preview-room-viii-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("rphoto-viii-iii");
            var label_1_1 = document.getElementById("rphoto-viii-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }


/* Family Picture */

function previewImagefp() {
        var file = document.getElementById("family").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-fp").classList.remove('d-none');
                document.getElementById("preview-fp").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("fp");
            var label_1_1 = document.getElementById("fp-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }





/* Homestay Gallery register */

function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").classList.remove('d-none');
                document.getElementById("preview").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
        
    }
    
    
    function previewImage_ii() {
        var file = document.getElementById("file-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-ii").classList.remove('d-none');
                document.getElementById("preview-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-ii");
            var label_1_1 = document.getElementById("label-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_iii() {
        var file = document.getElementById("file-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-iii").classList.remove('d-none');
                document.getElementById("preview-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-iii");
            var label_1_1 = document.getElementById("label-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_iv() {
        var file = document.getElementById("file-iv").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-iv").classList.remove('d-none');
                document.getElementById("preview-iv").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-iv");
            var label_1_1 = document.getElementById("label-iv-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_v() {
        var file = document.getElementById("file-v").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-v").classList.remove('d-none');
                document.getElementById("preview-v").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-v");
            var label_1_1 = document.getElementById("label-v-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_vi() {
        var file = document.getElementById("file-vi").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-vi").classList.remove('d-none');
                document.getElementById("preview-vi").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-vi");
            var label_1_1 = document.getElementById("label-vi-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_vii() {
        var file = document.getElementById("file-vii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-vii").classList.remove('d-none');
                document.getElementById("preview-vii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-vii");
            var label_1_1 = document.getElementById("label-vii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_viii() {
        var file = document.getElementById("file-viii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-viii").classList.remove('d-none');
                document.getElementById("preview-viii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-viii");
            var label_1_1 = document.getElementById("label-viii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_ix() {
        var file = document.getElementById("file-ix").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-ix").classList.remove('d-none');
                document.getElementById("preview-ix").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-ix");
            var label_1_1 = document.getElementById("label-ix-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_x() {
        var file = document.getElementById("file-x").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            var hid = document.getElementById("preview-x");
            hid.style.display = 'inline';
            
            fileReader.onload = function (event) {
                document.getElementById("preview-x").classList.remove('d-none');
                document.getElementById("preview-x").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-x");
            var label_1_1 = document.getElementById("label-x-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_xi() {
        var file = document.getElementById("file-xi").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xi").classList.remove('d-none');
                document.getElementById("preview-xi").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xi");
            var label_1_1 = document.getElementById("label-xi-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xi_ii() {
        var file = document.getElementById("file-xi-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xi-ii").classList.remove('d-none');
                document.getElementById("preview-xi-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xi-ii");
            var label_1_1 = document.getElementById("label-xi-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_xi_iii() {
        var file = document.getElementById("file-xi-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xi-iii").classList.remove('d-none');
                document.getElementById("preview-xi-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xi-iii");
            var label_1_1 = document.getElementById("label-xi-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xii() {
        var file = document.getElementById("file-xii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xii").classList.remove('d-none');
                document.getElementById("preview-xii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xii");
            var label_1_1 = document.getElementById("label-xii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xii_ii() {
        var file = document.getElementById("file-xii-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xii-ii").classList.remove('d-none');
                document.getElementById("preview-xii-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xii-ii");
            var label_1_1 = document.getElementById("label-xii-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    
    function previewImage_xii_iii() {
        var file = document.getElementById("file-xii-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xii-iii").classList.remove('d-none');
                document.getElementById("preview-xii-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xii-iii");
            var label_1_1 = document.getElementById("label-xii-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xiii() {
        var file = document.getElementById("file-xiii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiii").classList.remove('d-none');
                document.getElementById("preview-xiii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiii");
            var label_1_1 = document.getElementById("label-xiii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xiii_ii() {
        var file = document.getElementById("file-xiii-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiii-ii").classList.remove('d-none');
                document.getElementById("preview-xiii-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiii-ii");
            var label_1_1 = document.getElementById("label-xiii-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xiii_iii() {
        var file = document.getElementById("file-xiii-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiii-iii").classList.remove('d-none');
                document.getElementById("preview-xiii-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiii-iii");
            var label_1_1 = document.getElementById("label-xiii-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xiv() {
        var file = document.getElementById("file-xiv").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiv").classList.remove('d-none');
                document.getElementById("preview-xiv").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiv");
            var label_1_1 = document.getElementById("label-xiv-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xiv_ii() {
        var file = document.getElementById("file-xiv-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiv-ii").classList.remove('d-none');
                document.getElementById("preview-xiv-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiv-ii");
            var label_1_1 = document.getElementById("label-xiv-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    
    function previewImage_xiv_iii() {
        var file = document.getElementById("file-xiv-iii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xiv-iii").classList.remove('d-none');
                document.getElementById("preview-xiv-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xiv-iii");
            var label_1_1 = document.getElementById("label-xiv-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xv() {
        var file = document.getElementById("file-xv").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xv").classList.remove('d-none');
                document.getElementById("preview-xv").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-xv");
            var label_1_1 = document.getElementById("label-xv-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xv_ii() {
        var file = document.getElementById("file-xv-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xv-ii").classList.remove('d-none');
                document.getElementById("preview-xv-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xv-ii");
            var label_1_1 = document.getElementById("label-xv-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_xv_iii() {
        var file = document.getElementById("file-xv-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xv-iii").classList.remove('d-none');
                document.getElementById("preview-xv-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xv-iii");
            var label_1_1 = document.getElementById("label-xv-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_xvi() {
        var file = document.getElementById("file-xvi").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvi").classList.remove('d-none');
                document.getElementById("preview-xvi").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvi");
            var label_1_1 = document.getElementById("label-xvi-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }

    function previewImage_xvi_ii() {
        var file = document.getElementById("file-xvi-ii").files;

        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvi-ii").classList.remove('d-none');
                document.getElementById("preview-xvi-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvi-ii");
            var label_1_1 = document.getElementById("label-xvi-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xvi_iii() {
        var file = document.getElementById("file-xvi-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvi-iii").classList.remove('d-none');
                document.getElementById("preview-xvi-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvi-iii");
            var label_1_1 = document.getElementById("label-xvi-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xvii() {
        var file = document.getElementById("file-xvii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvii").classList.remove('d-none');
                document.getElementById("preview-xvii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvii");
            var label_1_1 = document.getElementById("label-xvii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xvii_ii() {
        var file = document.getElementById("file-xvii-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvii-ii").classList.remove('d-none');
                document.getElementById("preview-xvii-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvii-ii");
            var label_1_1 = document.getElementById("label-xvii-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xvii_iii() {
        var file = document.getElementById("file-xvii-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xvii-iii").classList.remove('d-none');
                document.getElementById("preview-xvii-iii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xvii-iii");
            var label_1_1 = document.getElementById("label-xvii-iii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xviii() {
        var file = document.getElementById("file-xviii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xviii").classList.remove('d-none');
                document.getElementById("preview-xviii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xviii");
            var label_1_1 = document.getElementById("label-xviii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xviii_ii() {
        var file = document.getElementById("file-xviii-ii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xviii-ii").classList.remove('d-none');
                document.getElementById("preview-xviii-ii").setAttribute("src", event.target.result);
            };
            
            fileReader.readAsDataURL(file[0]);
            
            var label_1 = document.getElementById("label-xviii-ii");
            var label_1_1 = document.getElementById("label-xviii-ii-i");
            
            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';
            
        }
    }
    
    function previewImage_xviii_iii() {
        var file = document.getElementById("file-xviii-iii").files;
        
        if (file.length > 0) {
            var fileReader = new FileReader();
            
            fileReader.onload = function (event) {
                document.getElementById("preview-xviii-iii").classList.remove('d-none');
                document.getElementById("preview-xviii-iii").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-xviii-iii");
            var label_1_1 = document.getElementById("label-xviii-iii-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }
    }