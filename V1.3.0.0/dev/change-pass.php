<?php
error_reporting(0);

include 'email.php';

if(isset($_GET['id_user'])){
    $id = $_GET['id_user'];

    $d = base64_decode($id);

    $query = $link->query("SELECT * FROM users WHERE id_user = '$d' ");
    $row=$query->fetch_assoc();

}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/change-pass.css">

    <link  rel="icon"   href="images/icon.png" type="image/png" />
    <title>Homebor - Login</title>

</head>


<body>

<!-- WRAPPER
    =================================================================================================================-->
<div>
    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>





    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">

        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="ts-title">
                    <h1>Change Password</h1>
                </div>
            </div>
        </section>

        <!--LOGIN / REGISTER SECTION
            =========================================================================================================-->
        <section id="login-register">
            <div class="container">
                <div class="row">

                    <div class="offset-md-2 col-md-8 offset-lg-3 col-lg-6">

                        <!--LOGIN / REGISTER TABS
                            =========================================================================================-->
                        <ul class="nav nav-tabs" id="login-register-tabs" role="tablist">

                            <!--Login tab-->
                            <li class="nav-item">
                                <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">
                                    <h3>Change Password</h3>
                                </a>
                            </li>

                        </ul>

                        <!--TAB CONTENT
                            =========================================================================================-->
                        <div class="tab-content">

                            <!--LOGIN TAB
                                =====================================================================================-->
                            <div class="tab-pane active" id="login" role="tabpanel" aria-labelledby="login-tab">

                                <!--Login form-->
                                     

                                    <p> Please enter your new password </p>


                                    <form action="change.php" method="POST">

                                    <input type="hidden" name="mail" value="<?php echo $row['mail'] ?>">

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="pass" placeholder="New Password" name="pass" >
                                    </div>

                                    <div class="form-group">
                                        <input type="password" class="form-control" id="pass" placeholder="Confirm Password" name="pass-confirm" >
                                    </div>
                        

                                    <!--Checkbox and Submit button-->
                                    <div class="ts-center__vertical justify-content-between">



                                        <br>

                                        <!--Submit button-->
                                        <button type="submit" class="btn btn-primary" name="submit">Send</button>
                                        

                                    </div>

                                    </form>                                   



                                    

                                    <hr>

                                    <!--Forgot password link-->
                                    <a href="forgot_pass.php" class="ts-text-small">
                                        <i class="fa fa-sync-alt ts-text-color-primary mr-2" id="pass"></i>
                                        <span class="ts-text-color-light" >I have forgot my password</span>
                                    </a>
                            
                                
                            </div>
                            <!--end #login.tab-pane-->
                    </div>
                    <!--end offset-4.col-md-4-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->













    <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/custom.js"></script>

</body>
</html>