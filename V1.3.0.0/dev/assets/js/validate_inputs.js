/**
 * Validar cualquier tipo de campos
 * @param {Element HTML} input Elemento HTML del DOM
 */
export default function validateInput(input) {
  input.classList.remove("invalid");
}
