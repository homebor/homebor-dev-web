<?php
include_once('../xeon.php');
session_start();
error_reporting(0);
// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <!-- //TODO DATERANGEPICKER -->
  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.25"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.25">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.25">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.25">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.25">
  <link rel="stylesheet" type="text/css" href="assets/css/assigment3.css?ver=1.0.25">
  <link rel="stylesheet" type="text/css" href="assets/css/edit_assigment.css?ver=1.0.25">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.25">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css?ver=1.0.25"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js?ver=1.0.25"
    integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
    crossorigin=""></script>


  <!-- // TODO Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js?ver=1.0.25'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css?ver=1.0.25' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js?ver=1.0.25"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css?ver=1.0.25" rel="stylesheet" />
  <script src="../assets/js/mapbox_directions.js?ver=1.0.25"></script>
  <link rel="stylesheet" href="../assets/css/mapbox_directions.css?ver=1.0.25">

  <title>Homebor - Students Profile</title>

</head>

<?php include 'header.php'; ?>

<body>
  <!-- // TODO MAIN-->
  <main id="ts-main">
    <!-- // TODO MAP-->
    <div class="ts-map ts-shadow__sm hide-map">
      <div id='map' class="w-100" style="height: 400px;"></div>
      <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js?ver=1.0.25'></script>
      <script
        src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js?ver=1.0.25'>
      </script>
    </div>

    <h1 class="mt-3 mt-lg-0 text-center text-lg-left alert container">Homestay Assignment</h1>

    <section id="agencies-list">
      <!-- // TODO STUDENT-->
      <article
        class="col-9 my-4 p-0 d-flex flex-column flex-lg-row justify-content-between bg-white rounded shadow container-fluid student-card">
        <!-- // * IMAGE-->
        <div class="w-100 h-100">
          <img id="student" height="100%" src="../assets/emptys/profile-student-empty.png">
        </div>

        <!-- // * STUDENT INFORMATION-->
        <div class="col-12 col-lg-8 m-0 p-0 d-flex flex-column justify-content-between">
          <!-- // ** NAME AND NATIONALITY -->
          <div class="py-2 px-4 student-card-header">
            <h2 id="fullname-student" class="mb-1">Nombre y apellido</h2>
            <aside id="nationality-student" class=" fa fa-address-card">Nacionalidad</aside>
          </div>

          <div class="h-100 mt-3 mt-lg-0 d-flex flex-column flex-lg-row ">
            <!-- // ** BASIC INFORMATION -->
            <div class="col-lg-6 pt-2 pb-0 px-3 border-right">
              <h3 class="m-0 mb-2 p-0 text-center">Basic Information</h3>
              <aside class="mt-4 d-flex align-items-center justify-content-around fd-ld">
                <span id="firstday-student">First Day</span>
                <span id="lastday-student">Last Day</span>
              </aside>

              <br>

              <aside class="mt-2 mb-2 d-flex align-items-center justify-content-around g-d">
                <span id="gender-student" data-filter="gender">Gender</span>
                <span id="datebirth-student">Date of Birth</span>
              </aside>

              <br>

              <aside class="mt-2 d-flex align-items-center justify-content-around sa-s">
                <span id="student-age">Student Age</span>
                <span id="stay" data-stay="">Stay</span>
              </aside>
            </div>

            <hr class="border-bottom w-75 d-lg-none">

            <!-- // ** PREFERENCES STUDENT -->
            <div class="col-lg-6 py-2 px-3 ts-description-lists">
              <h3 class="m-0 text-center">Preferences</h3>

              <!-- // *** LIST TO PREFERENCES OF THE STUDENT -->
              <div class="w-100 d-flex justify-content-center"></div>

              <aside class="mt-4 mt-lg-4 mb-4 mb-lg-0 d-flex align-items-center justify-content-around academy">
                <span id="academy-student" class="m-0 p-0">
                  Name academy, Address Academy City Academy State
                  Academy
                </span>
              </aside>
            </div>
          </div>
        </div>

      </article>


      <hr class="w-75 my-5">


      <!-- // TODO HOUSES-->
      <article id="houses-container" class="container-fluid col-10 card bg-transparent shadow-none houses-container">


        <!-- // * LOADING HOUSES -->
        <div class="d-flex flex-column align-items-center justify-content-center loading-houses">
          <h4>Loading houses...</h4>
          <svg width="50" height="50" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#fff">
            <g fill="none" fill-rule="evenodd">
              <g transform="translate(1 1)" stroke-width="2">
                <circle stroke-opacity=".5" cx="18" cy="18" r="18" />
                <path d="M36 18c0-9.94-8.06-18-18-18">
                  <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s"
                    repeatCount="indefinite" />
                </path>
              </g>
            </g>
          </svg>
        </div>

        <!-- // * HOUSES SELECTED -->
        <section
          class="d-none pt-5 mb-lg-0 container-fluid align-items-center justify-content-center current-houses-selected">
          <article class="current-house">
            <label class="" for="dynamic">
              <input hidden type="checkbox" id="dynamic">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
            </label>
            <img src="../assets/emptys/frontage-empty.png" width="100%" height="auto" rel="noopener">
            <hr class="mb-3">
            <span class="d-flex">House Name</span>
          </article>


          <article class="current-house">
            <label for="dynamic">
              <input hidden type="checkbox" id="dynamic">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
            </label>
            <img src="../assets/emptys/frontage-empty.png" width="100%" height="auto" rel="noopener">
            <hr class="mb-3">
            <span class="d-flex">House Name</span>
          </article>


          <article class="current-house">
            <label class="" for="dynamic">
              <input hidden type="checkbox" id="dynamic">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
            </label>
            <img src="../assets/emptys/frontage-empty.png" width="100%" height="auto" rel="noopener">
            <hr class="mb-3">
            <span class="d-flex">House Name</span>
          </article>


          <article class="current-house">
            <label class="" for="dynamic">
              <input hidden type="checkbox" id="dynamic">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
            </label>
            <img src="../assets/emptys/frontage-empty.png" width="100%" height="auto" rel="noopener">
            <hr class="mb-3">
            <span class="d-flex">House Name</span>
          </article>


          <article class="mb-lg-0 current-house">
            <label class="" for="dynamic">
              <input hidden type="checkbox" id="dynamic">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
            </label>
            <img src="../assets/emptys/frontage-empty.png" width="100%" height="auto" rel="noopener">
            <hr class="mb-3">
            <span class="d-flex">House Name</span>
          </article>
        </section>

        <!-- // * TODO FINAL BUTTONS-->
        <section
          class="col-10 container-fluid mx-auto p-4 d-flex flex-column flex-lg-row justify-content-center justify-content-lg-around final-buttons">
          <a href="#" class="mb-3 mb-lg-0 btn btn-lg text-white back-to-panel">Back to panel</a>
          <button id="next-step" disabled class="btn btn-lg text-white next-step">
            See your selection
          </button>
        </section>

        <br class="d-lg-none"><br class="d-lg-none"><br class="d-lg-none">

        <!-- // * SEARCH HOUSE AND DATE  -->
        <section class="py-4 mb-0 d-flex flex-column flex-lg-row align-items-center">
          <!-- // ** SEARCH HOUSE -->
          <aside
            class="col-10 col-lg-6 mr-lg-5 d-flex flex-lg-column justify-content-between justify-content-lg-start align-items-center search-house">
            <input type="search" class="col-10 col-lg-12" style="border-radius: 1.5rem;" placeholder="Search house..."
              id="search-house">
            <button class="d-lg-none icon-filter-button"><span class="icon-filter"></span></button>
          </aside>

          <!-- // ** DATE -->
          <aside class="col-12 col-lg-3 d-flex flex-column search-date">
            <input type="text" id="search-date" class="text-center" data-filter="date" readonly>
          </aside>
        </section>


        <!-- // * HOUSES -->
        <section class="houses-section">
          <!-- // ** ALL FILTERS  -->
          <section id="all-filters" class="d-flex flex-column">
            <article id="filter-tags" class="w-100 px-4 px-lg-0 mx-auto row filter-tags">
              <h3 class="w-100 py-3 py-lg-0 px-lg-3 mb-2 d-flex justify-content-between font-weight-bold">
                Filters Selected
                <span class="d-lg-none hide-filters">X</span>
              </h3>
            </article>

            <hr class="w-100 my-4">

            <!-- // *** SEARCH PREFERENCES -->
            <article class="w-100 px-2 px-lg-0 d-flex flex-column justify-content-around">
              <h3 class="px-3 mb-2 font-weight-bold">Filter by</h3>

              <aside class="d-flex flex-lg-column search-status">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-available">
                  <input hidden type="checkbox" id="search-available" data-filter="status" value="only available">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Only Available</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-all">
                  <input hidden type="checkbox" id="search-all" data-filter="status" value="show all">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Show All</span>
                </label>
              </aside>


              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-gender">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-female">
                  <input hidden type="checkbox" id="search-female" data-filter="gender" value="female">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Female</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-male">
                  <input hidden type="checkbox" id="search-male" data-filter="gender" value="male">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Male</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-non-binary">
                  <input hidden type="checkbox" id="search-non-binary" data-filter="gender" value="non-binary">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Non-binary</span>
                </label>
              </aside>

              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-food-service">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-food-service-yes">
                  <input hidden type="checkbox" id="search-food-service-yes" data-filter="food" value="food yes">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Yes</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-food-service-no">
                  <input hidden type="checkbox" id="search-food-service-no" data-filter="food" value="food no">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">No</span>
                </label>
              </aside>


              <aside class="d-flex flex-column search-special-diet">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-vegetarian">
                  <input hidden type="checkbox" id="search-vegetarian" data-filter="vegetarian" value="vegetarian">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Vegetarian</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-halal">
                  <input hidden type="checkbox" id="search-halal" data-filter="halal" value="halal">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Halal</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-kosher">
                  <input hidden type="checkbox" id="search-kosher" data-filter="kosher" value="kosher">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Kosher</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-lactose">
                  <input hidden type="checkbox" id="search-lactose" data-filter="lactose" value="lactose">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Lactose</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-gluten">
                  <input hidden type="checkbox" id="search-gluten" data-filter="gluten" value="gluten">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Gluten</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pork">
                  <input hidden type="checkbox" id="search-pork" data-filter="pork" value="pork">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Pork</span>
                </label>
              </aside>

              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-pets">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pets-yes">
                  <input hidden type="checkbox" id="search-pets-yes" data-filter="pets" value="pets yes">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Yes</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pets-no">
                  <input hidden type="checkbox" id="search-pets-no" data-filter="pets" value="pets no">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">No</span>
                </label>
              </aside>


              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-type-room">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-single">
                  <input hidden type="checkbox" id="search-single" data-filter="typeRoom" value="single">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Single</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-executive">
                  <input hidden type="checkbox" id="search-executive" data-filter="typeRoom" value="executive">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Executive</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share">
                  <input hidden type="checkbox" id="search-share" data-filter="typeRoom" value="share">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Share</span>
                </label>
              </aside>


              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-type-bed">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-twin">
                  <input hidden type="checkbox" id="search-twin" data-filter="typeBed" value="twin">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Twin</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-bunk">
                  <input hidden type="checkbox" id="search-bunk" data-filter="typeBed" value="bunk">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Bunk</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-double">
                  <input hidden type="checkbox" id="search-double" data-filter="typeBed" value="double">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Double</span>
                </label>
              </aside>

              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-age">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-adult">
                  <input hidden type="checkbox" id="search-adult" data-filter="age" value="adult">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Adult</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-teenager">
                  <input hidden type="checkbox" id="search-teenager" data-filter="age" value="teenager">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Teenager</span>
                </label>
              </aside>

              <br class="d-lg-none">


              <aside class="d-flex flex-lg-column search-smokers">
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-outside-ok">
                  <input hidden type="checkbox" id="search-outside-ok" data-filter="smoke" value="outside-ok">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Outside</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-inside-ok">
                  <input hidden type="checkbox" id="search-inside-ok" data-filter="smoke" value="inside-ok">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Inside</span>
                </label>
                <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-non-smoking">
                  <input hidden type="checkbox" id="search-non-smoking" data-filter="smoke"
                    value="strictly non-smoking">
                  <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16"></path>
                  </svg>
                  <span class="px-2">Non-Smoking</span>
                </label>
              </aside>
              <br class="d-lg-none">
              <br class="d-lg-none">
            </article>
          </section>

          <!-- // ** ALL HOUSES  -->
          <section id="all-houses" class="d-flex flex-column align-items-center">
            <!-- // * SEE MORE HOUSES -->
            <button class="mt-4 btn see-more" data-page="0">See More</button>
            <button class="d-none mt-4 btn show-more">Show More</button>
          </section>
        </section>


        <!-- // * TEMPLATES -->
        <template id="house-card-template">
          <div class="d-flex flex-column flex-lg-row mb-5 mb-lg-4 pb-2 pb-lg-0 bg-white card-house" data-id-home>
            <!-- // ** CARD HEADER-->
            <div class="p-0 d-flex flex-column align-items-center card-header" data-house-profile title="See profile">
              <img src="../assets/emptys/frontage-empty.png" class="mx-auto" data-house-image alt="house, homestay">
              <div data-house-status>Available</div>
            </div>

            <!-- // ** CARD BODY CONTAINER-->
            <div class="w-100 py-2 card-body">
              <!-- // *** CARD MAIN INFORMATION-->
              <div class="p-0 main-information">
                <!-- // **** HOUSE NAME  -->
                <label class="w-100 h4 m-0 d-flex justify-content-between align-items-center" data-house-name>House
                  Name</label>

                <hr class="my-2 pt-1">

                <!-- // **** ROW 2  -->
                <dl class="m-0 mb-3 d-flex align-items-center">
                  <dt>Address:</dt>
                  <dd class="m-0 px-2" data-house-direction>99 Harbour Sq, Toronto Ontario</dd>
                </dl>


                <!-- // **** ROW 3  -->
                <div class="pb-0 mb-3 d-flex align-items-center justify-content-between">
                  <dl class="m-0">
                    <dt>Gender</dt>
                    <dd class="m-0" data-house-gender-preference>Female</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Age</dt>
                    <dd class="m-0" data-house-age-preference>Adult</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Bedrooms</dt>
                    <dd class="m-0" data-house-rooms>3</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Language</dt>
                    <dd class="m-0" data-house-background-language="">3</dd>
                  </dl>
                </div>


                <!-- // **** ROW 4  -->
                <div class="pb-0 mb-0 d-flex align-items-center justify-content-between">
                  <dl class="m-0">
                    <dt>Smoking Policys</dt>
                    <dd class="m-0" data-house-smokers-preference>Empty</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Food Service</dt>
                    <dd class="m-0" data-house-food-service="">Yes</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Special Diet</dt>
                    <dd class="m-0" data-house-special-diet="">Yes</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Pets</dt>
                    <dd class="m-0" data-house-pets-preference>Empty</dd>
                  </dl>
                </div>

                <!-- // **** MORE INFO  -->
                <button class="ml-auto py-lg-1 shadow btn btn-more-info" data-more-info-button>More Info</button>
              </div>

              <!-- // **** CARD BEDROOMS-->
              <div class="pt-2 container bedrooms-container" data-house-bedrooms>
                <dt class="py-2">Bedrooms Availables</dt>
                <div id="bedrooms"></div>
              </div>
            </div>
        </template>
        <template id="bedrooms-template">
          <div class="mt-3 mx-auto p-2" data-room-container>
            <b class="d-block pb-1 text-center" data-room-name>Room 1</b>
            <div class="d-flex align-items-center justify-content-center">
              <span class="px-2 mx-2 text-white bg-secondary rounded" data-room-bed-1>Type bed</span>
              <span class="px-2 mx-2 text-white bg-secondary rounded" data-room-bed-2>Type bed</span>
              <span class="px-2 mx-2 text-white bg-secondary rounded" data-room-bed-3>Type bed</span>
            </div>
          </div>
        </template>
      </article>


      <!-- // TODO RESULT ALL HOUSES SELECTED -->
      <section id="all-houses-selected" class="d-none col-10 col-lg-8 container flex-column align-items-center">
        <h2 class="alert border-bottom">Homestays selected</h2><br>
      </section>

      <article class="d-none mt-lg-4 save-buttons">
        <button id="previous-step" class="mb-3 mr-lg-5 mb-lg-0 d-none btn btn-lg text-white previous-step">
          Back to selection
        </button>
        <button id="save-assignment" disabled class="ml-lg-5 d-none btn btn-lg text-white next-step">
          Send selection to student
        </button>
      </article>

      <br><br>
      <br><br><br><br><br><br><br><br>


      <!-- // TODO MODAL DYNAMIC -->
      <div id="modal-dynamic" class="modal-dynamic">

        <section class="modal-content">
          <span class="close">X</span>
          <h3>Title</h3>
          <div class="d-flex justify-content-around "></div>
        </section>

      </div>
  </main>

  <script>
  $("#search-date").daterangepicker({
    "autoUpdateInput": false,
    format: "",
    opens: "right"
  }, (start, end, label) => {
    document.querySelector('#search-date').value =
      `${start.format('YYYY/MM/DD')} - ${end.format('YYYY/MM/DD')}`
  });
  </script>


  <script src="../assets/js/popper.min.js?ver=1.0.25"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.25"></script>
  <script src="../assets/js/custom.js?ver=1.0.25"></script>
  <script src="assets/js/edit_assigment.js?ver=1.0.25" type="module"></script>

</body>

</html>