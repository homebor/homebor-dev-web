  <?php
  session_start();
  include_once('../xeon.php');
  error_reporting(0);
  ?>

  <!doctype html>
  <html lang="en">

  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <!--// ? CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.0">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.0">
    <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.0">
    <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.0">
    <link rel="stylesheet" href="assets/css/blacklist.css?ver=1.0.0">

    <!--// ?  Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png?ver=1.0.0">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.0">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.0">
    <link rel="manifest" href="/site.webmanifest?ver=1.0.0">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?ver=1.0.0" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>Homebor - BlackList</title>

  </head>

  <?php include 'header.php' ?>

  <body>
    <!-- // TODO CONTAINER -->
    <main class="container">
      <!-- // TODO TITLE -->
      <br><br><br><br>
      <h1 class="m-0 mt-5 p-4 border-bottom">Blacklist</h1>

      <!-- // TODO BLACKLIST -->
      <section class="mt-5 alert shadow blacklist">
        <table class="my-lg-4 table table-responsive-md table-responsive-sm">
          <thead>
            <tr class="text-center">
              <th>Image</th>
              <th>Name</th>
              <th>Email</th>
              <th>Date</th>
              <th>Reason</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <template id="blacklist-template">
              <tr class="text-center">
                <td class="align-middle">
                  <img id="house-image" width="100px" height="100px" src="../assets/emptys/frontage-empty.png" alt="">
                </td>
                <td id="house-name" class="align-middle">California Home</td>
                <td id="house-email" class="align-middle">californiahome@gmail.com</td>
                <td id="disabled-date" class="align-middle">2022-08-10 <br><span>7 Days</span></td>
                <td id="disabled-reason" class="align-middle">Reason for disabled</td>
                <td class="align-middle">
                  <button id="edit-homestay" class="mb-3 btn btn-sm edit-btn">Edit</button>
                  <br>
                  <button id="disabled-homestay" class="btn btn-sm disabled-btn">Disabled</button>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </section>
    </main>

    <!-- // TODO MODALS -->
    <modals>
      <!-- // TODO MODAL DYNAMIC -->
      <div id="modal-dynamic" class="modal-dynamic">
        <section class="modal-content">
          <span class="close">X</span>
          <h3>Title</h3>
        </section>
      </div>
    </modals>


    <!--// TODO FOOTER ===============================================================-->
    <?php include 'footer.php'; ?>

    <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.0"></script>
    <script src="../assets/js/popper.min.js?ver=1.0.0"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.0"></script>
    <script src="../assets/js/leaflet.js?ver=1.0.0"></script>
    <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.0"></script>
    <!--// * Date Input Safari-->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js?ver=1.0.0"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js?ver=1.0.0"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js?ver=1.0.0"></script>
   <script src="assets/js/date-safari.js?ver=1.0.0"></script> -->


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.0"></script>
    <script src="assets/js/blacklist.js?ver=1.0.0" type="module"></script>

  </body>

  </html>