"use strict";
// TODO IMPORTACIONES
import validateInput from "../../../controllers/validation.js?ver=1.0.36";
import { MAP } from "./map.js?ver=1.0.36";
import BedRooms from "./homedit_rooms.js?ver=1.0.36.1";
import BasicInformation from "./basic_information.js?ver=1.0.36";
import HouseGallery from "./house_gallery.js?ver=1.0.36";
import AdditionalInformation from "./additional_information.js?ver=1.0.36";
import FamilyInformation from "./family_information.js?ver=1.0.36";

// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_HOME = searchParams.get("art_id");

// ? INSTANCIAS
const _Rooms = new BedRooms();
const _BasicInformation = new BasicInformation();
const _HouseGallery = new HouseGallery();
const _AdditionalInformation = new AdditionalInformation();
const _FamilyInformation = new FamilyInformation();

// TODO FUNCIONES
/** // ? ANIMATION OF PAGE LOADING
 * @param {Boolean} begin Renderizar animacion de loading, true=muestra, false=oculta
 */
const loadingStart = (begin = true) => {
  if (begin) {
    D.querySelector(".loading-saving").classList.remove("d-none");
    D.querySelector(".loading-saving h4").textContent = "Please wait...";
    D.querySelector("body").style.overflowY = "hidden";
  } else {
    D.querySelector("body").style.overflowY = "";
    D.querySelector(".loading-saving").style.opacity = "0";
    setTimeout(() => {
      D.querySelector(".loading-saving h4").textContent = "Please wait... Saving your data.";
      D.querySelector(".loading-saving").classList.add("d-none");
      D.querySelector(".loading-saving").style.opacity = "1";
    }, 500);
  }
};

/** // ? REPLACE BUTTON BY THIRD BED (PLUS) SIGN IF ITS VALUE IS EMPTY
 * @param {Element} select Elemento select a reemplazar por el icono +
 */
const fixBedType = (select) => {
  if (select.value === "Share") {
    const $lastBed =
      select.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("#bed-type")[2];

    if ($lastBed.value === "NULL") {
      $lastBed.className = "px-2";
      $lastBed.parentElement.parentElement.parentElement.classList.remove("more");
    }
  }
};

// ? CHECK AND FIX QUESTIONS
const checkItems = async () => {
  FamilyInformation.responseControl(D.querySelector("#religion").value, 1, "#family-responses");
  FamilyInformation.responseControl(D.querySelector("#condition").value, 2, "#family-responses");
  FamilyInformation.responseControl(D.querySelector("#misdemeanor").value, 3, "#family-responses");

  FamilyInformation.responseControl(D.querySelector("#allergies").value, 1, "#responses-any-members");
  FamilyInformation.responseControl(D.querySelector("#take-medication").value, 2, "#responses-any-members");
  FamilyInformation.responseControl(D.querySelector("#health-problems").value, 3, "#responses-any-members");

  if (
    !D.querySelector("#family-responses").classList.contains("response-1") &&
    !D.querySelector("#family-responses").classList.contains("response-2") &&
    !D.querySelector("#family-responses").classList.contains("response-3")
  )
    D.querySelector("#family-responses").classList.remove("show");

  if (
    !D.querySelector("#responses-any-members").classList.contains("response-1") &&
    !D.querySelector("#responses-any-members").classList.contains("response-2") &&
    !D.querySelector("#responses-any-members").classList.contains("response-3")
  )
    D.querySelector("#responses-any-members").classList.remove("show");
};

// ? SAVE ALL HOME DATA
const saveData = async (e) => {
  e.preventDefault();

  // * BEGIN LOADING
  D.querySelector(".loading-saving").classList.remove("d-none");

  // * GO TO SAVE BEDROOMS
  if (D.querySelectorAll("[data-room]").length >= 0) {
    await _Rooms.saveBedrooms(D.querySelectorAll("[data-room]"), ID_HOME);
  }

  // * GO TO SAVE BASIC INFORMATION
  if (D.querySelector("#basic-information")) {
    const $inputs = D.querySelectorAll("#basic-information input");
    const $selects = D.querySelectorAll("#basic-information select");
    await _BasicInformation.saveBasicInformation($inputs, $selects);
  }

  // * GO TO SAVE HOUSE GALLERY
  if (D.querySelector("#house-gallery")) {
    const $inputs = D.querySelectorAll("#house-gallery input");
    await _HouseGallery.saveHouseGallery($inputs);
  }

  // * GO TO SAVE ADDITIONAL INFORMATION
  if (D.querySelector("#additional-information")) {
    const $inputs = D.querySelectorAll("#additional-information input");
    const $selects = D.querySelectorAll("#additional-information select");
    const $textarea = D.querySelector("#additional-information textarea");
    await _AdditionalInformation.saveAdditionalInformation($inputs, $selects, $textarea);
  }

  // * GO TO SAVE FAMILY INFORMATION
  if (D.querySelector("#family-information")) {
    const $inputs = D.querySelectorAll("#family-information input");
    const $selects = D.querySelectorAll("#family-information select");
    await _FamilyInformation.saveFamilyInformation($inputs, $selects);
  }
  // * CERTIFY HOMESTAY
  if (e.target.id === "certified") certify("Yes");
  else {
    // ** END LOADING
    if (!D.querySelector("#modal-dynamic").classList.contains("show")) {
      D.querySelector("[data-house-name]").dataset.houseName = `${D.querySelector("#last-name-contact")
        .value.toUpperCase()
        .trim()}, ${D.querySelector("#name-contact").value.trim()} `;
      D.querySelector(".loading-content").classList.add("d-none");
      D.querySelector(".loaded-content").classList.remove("d-none");
      setTimeout(() => (window.location.href = "directory_homestay"), 1500);
    } else {
      D.querySelector("#modal-dynamic #save-changes-yes").disabled = false;
      D.querySelector("#modal-dynamic #save-changes-no").disabled = false;
    }
  }
};

// ? CERTIFY HOUSE
const certify = async (action, description) => {
  try {
    D.querySelector(".loading-saving").classList.remove("d-none");

    const roomData = new FormData();
    roomData.set("homestay_id", ID_HOME);
    roomData.set("certify", action);
    roomData.set("description", description);

    const options = {
      method: "POST",
      headers: { "Content-type": "application/json; charset=utf-8" },
      data: roomData,
    };

    const res = await axios("./edit-admin-propertie.php", options);
    const data = await res.data;

    D.querySelector(".loading-content").classList.add("d-none");
    D.querySelector(".loaded-content").classList.remove("d-none");
    D.querySelector(".loaded-content h4").textContent = data;

    if (action === "Disabled") setTimeout(() => (window.location.href = "blacklist"), 1500);
    else setTimeout(() => (window.location.href = "directory_homestay"), 1500);
  } catch (error) {
    console.error("Error en Certify", error);
  }
};

// ? DESCERTIFY HOUSE
const decertifyHouse = (step) => {
  switch (step) {
    case 1:
      if (
        D.querySelector(".reason").classList.contains("show") &&
        D.querySelector(".reason").dataset.type === "disabled"
      ) {
        D.querySelector(".reason").classList.remove("show");
        setTimeout(() => D.querySelector(".reason").classList.add("show"), 300);
      } else D.querySelector(".reason").classList.toggle("show");

      D.querySelector(".reason textarea").focus();
      D.querySelector(".reason").dataset.type = "decertify";
      D.querySelector(".reason #send-reason").dataset.action = "decertify";
      D.querySelector(".reason .reason-title").textContent = "Reason for Decertification";
      break;
    case 2:
      D.querySelector("#modal-dynamic").classList.add("show");
      D.querySelector(
        "#modal-dynamic"
      ).innerHTML = `<div class="d-flex flex-column text-center justify-content-center save-changes">  <h4>By continuing with the process, the house will go to your black list of houses and will <br> not be seen in your directories. Continue with the process?</h4>  <div class="d-flex align-items-center justify-content-center">    <button id="descertify-yes" class="btn w-25 mx-2 enable-room">Yes</button><button id="descertify-no" class="btn w-25 mx-2 disable-room">No</button></div></div>`;
      break;
    default:
      certify("No", D.querySelector("#reason").value);
      break;
  }
};

// ? DISABLED HOUSE
const disabledHouse = (step) => {
  switch (step) {
    case 1:
      if (
        D.querySelector(".reason").classList.contains("show") &&
        D.querySelector(".reason").dataset.type === "decertify"
      ) {
        D.querySelector(".reason").classList.remove("show");
        setTimeout(() => D.querySelector(".reason").classList.add("show"), 300);
      } else D.querySelector(".reason").classList.toggle("show");

      D.querySelector(".reason textarea").focus();
      D.querySelector(".reason").dataset.type = "disabled";
      D.querySelector(".reason #send-reason").dataset.action = "disabled";
      D.querySelector(".reason .reason-title").textContent = "Reason for Disabled";
      break;
    case 2:
      D.querySelector("#modal-dynamic").classList.add("show");
      D.querySelector(
        "#modal-dynamic"
      ).innerHTML = `<div class="d-flex flex-column text-center justify-content-center save-changes"><h4>Are you sure you want to disable the house?</h4>  <div class="d-flex align-items-center justify-content-center">    <button id="disabled-yes" class="btn w-25 mx-2 enable-room">Yes</button><button id="disabled-no" class="btn w-25 mx-2 disable-room">No</button></div></div>`;
      break;
    default:
      certify("Disabled", D.querySelector("#reason").value);
      break;
  }
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await _Rooms.homestayRooms();
  await _Rooms.statusRoom();
  await _BasicInformation.getBasicInformation();
  await _HouseGallery.getHouseGallery();
  await _AdditionalInformation.getAdditionalInformation();
  await _FamilyInformation.getFamilyInformation();

  await checkItems();

  D.querySelectorAll("#type-room").forEach((select) => fixBedType(select));

  loadingStart(false);
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }
  // ? SAVE (BASIC INFORMATION / HOUSE GALLERY / BEDROOMS/ADDITIONAL INFORMATION / FAMILY INFORMATION)
  if (e.target.id === "submit" || e.target.id === "certified") saveData(e);
  // ? DESCERTIFY HOUSE
  if (e.target.id === "decertify") decertifyHouse(1);
  if (e.target.id === "send-reason" && e.target.dataset.action === "decertify") decertifyHouse(2);
  if (e.target.id === "descertify-yes") decertifyHouse();
  if (e.target.id === "descertify-no") D.querySelector("#modal-dynamic").classList.remove("show");
  // ? DISABLE HOUSE
  if (e.target.id === "disabled") disabledHouse(1);
  if (e.target.id === "send-reason" && e.target.dataset.action === "disabled") disabledHouse(2);
  if (e.target.id === "disabled-yes") disabledHouse();
  if (e.target.id === "disabled-no") D.querySelector("#modal-dynamic").classList.remove("show");
  // ? DISCARD OR DELETE CHANGES
  if (e.target.id === "discard") W.location.reload();
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  // ? VALIDATION
  if (e.target.id === "house-name") validateInput(e.target, "name-special");
  if (e.target.id === "phone-number") validateInput(e.target, "phone");
  if (e.target.id === "total-rooms") validateInput(e.target, "rooms");
  if (e.target.id === "house-mail") validateInput(e.target, "email");
  if (e.target.id === "password") validateInput(e.target, "password");
  if (e.target.id === "address") validateInput(e.target, "address");
  if (e.target.id === "city") validateInput(e.target, "city");
  if (e.target.id === "state") validateInput(e.target, "state");
  if (e.target.id === "postal-code") validateInput(e.target, "postal-code");
  if (e.target.id === "homestay-price") validateInput(e.target, "number");
  if (e.target.id === "provider-price") validateInput(e.target, "number");
  if (e.target.id === "house-description") validateInput(e.target, "description");
  // if (e.target.id === "total-years") validateInput(e.target, "date");
  if (e.target.id === "total-pets") validateInput(e.target, "number");
  if (e.target.id === "specify-pet") validateInput(e.target, "name");
  if (e.target.id === "specify-allergy") validateInput(e.target, "name");
  if (e.target.id === "specify-medication") validateInput(e.target, "name");
  if (e.target.id === "specify-problems") validateInput(e.target, "name");
  if (e.target.id === "name-contact") validateInput(e.target, "name-special");
  if (e.target.id === "last-name-contact") validateInput(e.target, "name-special");
  // if (e.target.id === "date-birth-contact") validateInput(e.target, "date");
  if (e.target.id === "phone-contact") validateInput(e.target, "phone");
  // if (e.target.id === "date-background-check") validateInput(e.target, "date");
  if (e.target.id === "number-members") validateInput(e.target, "number");
  if (e.target.id === "background") validateInput(e.target, "name");
  if (e.target.id === "background-language") validateInput(e.target, "name");
  if (e.target.id === "specify-religion") validateInput(e.target, "name");
  if (e.target.id === "specify-condition") validateInput(e.target, "name");
  if (e.target.id === "specify-misdemeanor") validateInput(e.target, "name");
  if (e.target.matches("[data-name]")) validateInput(e.target, "name");
  if (e.target.matches("[data-last-name]")) validateInput(e.target, "name");
  // if (e.target.matches("[data-date-birth]")) validateInput(e.target, "date");
  if (e.target.matches("[data-occupation]")) validateInput(e.target, "name");
  // if (e.target.matches("[data-date-background-check]")) validateInput(e.target, "date");
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? VALIDATION
  if (e.target.id === "house-type") validateInput(e.target, ["House", "Apartment", "Condominium"]);
  if (e.target.id === "main-city") validateInput(e.target, "name");
  if (e.target.matches("[data-gallery-photo]") || e.target.matches(".carousel-item input")) {
    const result = validateInput(e.target, "image");
    if (result) _Rooms.anyError(result);
  }
  if (e.target.id === "type-room") validateInput(e.target, ["Single", "Executive", "Share"]);
  if (e.target.id === "food-service") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "bed-type") validateInput(e.target, ["Twin", "Bunk", "Double"]);
  if (e.target.id === "academy") validateInput(e.target, "number");
  if (e.target.id === "preferences-gender") validateInput(e.target, ["Male", "Female", "Any"]);
  if (e.target.id === "preferences-age") validateInput(e.target, ["Teenager", "Adult", "Any"]);
  if (e.target.id === "smokers-politics") validateInput(e.target, ["Outside-OK", "Inside-OK", "Strictly Non-Smoking"]);
  if (e.target.id === "meals-service") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "pets") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "allergies") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "take-medication") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "health-problems") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "gender-contact") validateInput(e.target, ["Male", "Female", "Private"]);
  if (e.target.id === "pdf-file") {
    const result = validateInput(e.target, "pdf");
    if (result) _Rooms.anyError(result);
  }
  if (e.target.id === "religion") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "condition") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "misdemeanor") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "family-final-question") validateInput(e.target, ["Yes", "No"]);
  if (e.target.matches("[data-gender]")) validateInput(e.target, ["Male", "Female", "Private"]);
  if (e.target.matches("[data-relation]"))
    validateInput(e.target, ["Dad", "Mom", "Son", "Daughter", "Grandparents", "Others"]);
  if (e.target.matches("[data-background-check]")) {
    const result = validateInput(e.target, "pdf");
    if (result) _Rooms.anyError(result);
  }

  if (e.target.id === "meals-service") {
    if (e.target.value === "Yes") {
      D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? (h3.style.cssText = "") : {}));
      D.querySelector(".special-diet").classList.replace("d-none", "d-flex");
    } else {
      D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? (h3.style.display = "none") : {}));
      D.querySelector(".special-diet").classList.replace("d-flex", "d-none");
    }
  }

  // ? REMOVE DESIGN RED TO THE SELECTS
  D.querySelectorAll(".invalid").forEach((elem) => elem.classList.remove("invalid"));
});
