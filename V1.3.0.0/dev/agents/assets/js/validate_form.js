const formulary = document.getElementById("form");

const inputs = document.querySelectorAll("#form input");

// TODO CANVAS

const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^\d{2,20}$/, // 2 a 14 numeros.
  rooms: /^\d{1,2}$/, // 1 a 14 numeros.
  dir: /^[a-zA-Z0-9\_\-\s\°\S]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s\S]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/, // 8 a 15 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
};

const campos = {
  name: false,
  l_name: false,
  mail_s: false,
  password: false,
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    // Required fields
    case "name":
      validateField(expresiones.nombre, e.target, "name");
      break;

    case "l_name":
      validateField(expresiones.nombre, e.target, "l_name");
      break;

    case "mail_s":
      validateField(expresiones.correo, e.target, "mail_s");
      break;

    case "password":
      validateField(expresiones.password, e.target, "password");
      break;

    /*case "firstd":
            validateField(expresiones.date, e.target, 'firstd');
        break;*/

    case "dir":
      validateField(expresiones.dir, e.target, "dir");
      break;

    case "city":
      validateField(expresiones.city, e.target, "city");
      break;

    case "state":
      validateField(expresiones.state, e.target, "state");
      break;

    case "p_code":
      validateField(expresiones.p_code, e.target, "p_code");
      break;

    // Optional Fields

    case "num_s":
      validateField(expresiones.phone, e.target, "num_s");
      break;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`group__${campo}`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  } else {
    document.getElementById(`group__${campo}`).classList.add("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.add("form__group__input-error-active");
    campos[campo] = false;
  }
};

inputs.forEach((input) => {
  input.addEventListener("keyup", validateFormulary);
  input.addEventListener("blur", validateFormulary);
});

formulary.addEventListener("submit", (e) => {
  e.preventDefault();

  const enlace = document.createElement("a");
  // Convertir la imagen a Base64 y ponerlo en el enlace
  enlace.href = $canvas.toDataURL();

  document.querySelector("#signature-canvas").value = enlace.href;
  if (campos.name && campos.l_name && campos.mail_s && campos.password) {
    var registerForm = new FormData($("#form")[0]);

    $.ajax({
      url: "action_student.php",
      type: "POST",
      data: registerForm,
      contentType: false,
      processData: false,
      success: function (response) {
        let register = JSON.parse(response);

        register.forEach((reg) => {
          if (reg.register == "Exist") {
            window.top.location = `edit_assigment?art_id=${reg.id_student}`;
          } else if (reg.register == "New") {
            window.top.location = `academy_register?art_id=${reg.id_ac}`;
          } else if (reg.register == "Mail Exist") {
            setTimeout(() => {
              $("#house_much").fadeIn("slow");

              setTimeout(() => {
                $("#house_much").fadeOut("slow");
              }, 5000);
            }, 100);

            $("#close").on("click", function close() {
              event.preventDefault();
              $("#house_much").fadeOut("slow");
            });
            document.getElementById("contentp").innerHTML =
              "There is already a student with that email. Enter another email.";
            document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
            document.getElementById("contentp").style.marginTop = "auto";
            document.getElementById("contentp").style.marginBottom = "auto";
            document.getElementById("contentp").style.color = "#000";
          } else if (reg.register == "Plan") {
            setTimeout(() => {
              $("#house_much").fadeIn("slow");

              setTimeout(() => {
                $("#house_much").fadeOut("slow");
              }, 5000);
            }, 100);

            $("#close").on("click", function close() {
              event.preventDefault();
              $("#house_much").fadeOut("slow");
            });
            document.getElementById("contentp").innerHTML =
              "Exceed the limit of accepted students. Change your plan to continue.";
            document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
            document.getElementById("contentp").style.marginTop = "auto";
            document.getElementById("contentp").style.marginBottom = "auto";
            document.getElementById("contentp").style.color = "#000";
          }
        });
      },
    });
  } else {
    setTimeout(() => {
      $("#house_much").fadeIn("slow");

      setTimeout(() => {
        $("#house_much").fadeOut("slow");
      }, 5000);
    }, 100);

    $("#close").on("click", function close() {
      event.preventDefault();
      $("#house_much").fadeOut("slow");
    });
    document.getElementById("contentp").innerHTML = "Please fill in the required fields correctly.";
    document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
    document.getElementById("contentp").style.marginTop = "auto";
    document.getElementById("contentp").style.marginBottom = "auto";
    document.getElementById("contentp").style.color = "#000";
  }
});

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
/* $btnDescargar.onclick = () => {}; */

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});
["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
  });
});
