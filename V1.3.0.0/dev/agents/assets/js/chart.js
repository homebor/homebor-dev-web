// TODO CHART DEFAULTS
Chart.defaults.font.size = 16;
Chart.defaults.color = "#fff";

// TODO VARIABLES
const $forCity = document.getElementById("forCity");
const $forCertified = document.getElementById("forCertified");

const cityLegends = {
  labels: ["Toronto", "Montreal", "Ottawa", "Quebec", "Calgary", "Vancouver", "Victoria", "Waterloo", "Guelph"],
  datasets: [
    {
      data: [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
      backgroundColor: [
        "#f2ccd9",
        "#4ab0dc",
        "#a57ec6",
        "#fbc04d",
        "#95a7e2",
        "#f75489",
        "#cb9cd0",
        "#f4ab80",
        "#513146",
      ],
    },
  ],
};
const certifiedLegends = {
  labels: ["Uncertified", "Total"],
  datasets: [
    {
      data: [undefined, undefined],
      backgroundColor: ["#e056fd", "#686de0"],
    },
  ],
};

// ? ALL HOMESTAYS DATA
(async () => {
  const formData = new FormData();

  const getHomestays = await axios("../helpers/homestay/get_all_homestays.php");
  const allHomestays = await getHomestays.data;

  // ? DEFINE HOMESTAYS FOR CITY
  const allCities = {
    Toronto: 0,
    Montreal: 0,
    Ottawa: 0,
    Quebec: 0,
    Calgary: 0,
    Vancouver: 0,
    Victoria: 0,
    Waterloo: 0,
    Guelph: 0,
  };

  allHomestays.forEach((homestay) => {
    if (homestay.m_city !== "NULL") {
      homestay.m_city.toLowerCase().includes("toronto") ? (allCities.Toronto += 1) : {};
      homestay.m_city.toLowerCase().includes("montreal") ? (allCities.Montreal += 1) : {};
      homestay.m_city.toLowerCase().includes("ottawa") ? (allCities.Ottawa += 1) : {};
      homestay.m_city.toLowerCase().includes("quebec") ? (allCities.Quebec += 1) : {};
      homestay.m_city.toLowerCase().includes("calgary") ? (allCities.Calgary += 1) : {};
      homestay.m_city.toLowerCase().includes("vancouver") ? (allCities.Vancouver += 1) : {};
      homestay.m_city.toLowerCase().includes("victoria") ? (allCities.Victoria += 1) : {};
      homestay.m_city.toLowerCase().includes("waterloo") ? (allCities.Waterloo += 1) : {};
      homestay.m_city.toLowerCase().includes("guelph") ? (allCities.Guelph += 1) : {};
    }
  });

  cityLegends.datasets[0].data[0] = allCities.Toronto;
  cityLegends.datasets[0].data[1] = allCities.Montreal;
  cityLegends.datasets[0].data[2] = allCities.Ottawa;
  cityLegends.datasets[0].data[3] = allCities.Quebec;
  cityLegends.datasets[0].data[4] = allCities.Calgary;
  cityLegends.datasets[0].data[5] = allCities.Vancouver;
  cityLegends.datasets[0].data[6] = allCities.Victoria;
  cityLegends.datasets[0].data[7] = allCities.Waterloo;
  cityLegends.datasets[0].data[8] = allCities.Guelph;

  // ? DEFINE HOMESTAYS UNCERTIFIED
  const allHomestaysUncertified = allHomestays
    .map((homestay) => (homestay.certified.toLowerCase().includes("no") ? homestay : []))
    .flat();

  certifiedLegends.datasets[0].data[0] = allHomestaysUncertified.length;
  certifiedLegends.datasets[0].data[1] = allHomestays.length;

  // ? OPTIONS
  const options = {
    responsive: false,
    layout: {
      padding: 10,
    },
    plugins: {
      title: {
        display: true,
        color: "#fff",
        position: "top",
        text: "Homestay for City",
      },
      legend: { display: false },
    },
  };

  // ? HOMESTAY FOR CITY
  const pieForCity = new Chart($forCity, {
    type: "pie",
    data: cityLegends,
    options: options,
  });

  // ? HOMESTAY FOR CERTIFIED
  options.plugins.title.text = "Homestay for Certified";
  // options.plugins.legend.display = true;
  // options.plugins.legend.position = "bottom";
  // options.plugins.legend.labels = { boxWidth: 18, boxHeight: 15, padding: 16, textAlign: "center" };
  const pieForCertified = new Chart($forCertified, {
    type: "doughnut",
    data: certifiedLegends,
    options: options,
  });
})();
