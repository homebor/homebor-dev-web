<?php

$link = new PDO("mysql:host=localhost; dbname=u702954182_xeondb", "u702954182_xeon", "Xeon.2801");
error_reporting(0);
session_start();
$usuario = $_SESSION['username'];

$sql = "SELECT * FROM agents WHERE a_mail = '$usuario'";
foreach ($link->query($sql) as $row2);

/*function get_total_row($link)
{
  $query = "
  SELECT * FROM tbl_webslesson_post
  ";
  $statement = $link->prepare($query);
  $statement->execute();
  return $statement->rowCount();
}

$total_record = get_total_row($link);*/

$limit = '4';
$page = 1;
if($_POST['page'] > 1)
{
  $start = (($_POST['page'] - 1) * $limit);
  $page = $_POST['page'];
}
else
{
  $start = 0;
}

$query = "
SELECT * FROM propertie_control WHERE id_m = '$row2[id_m]' 
";

if($_POST['query'] != '')
{
  $query .= '
  WHERE certified LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" OR pet LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" OR ag_pre LIKE "%'.str_replace(' ', '%', $_POST['query']).'%"
    OR h_name LIKE "%'.str_replace(' ', '%', $_POST['query']).'%"
    OR room LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" 
    OR g_pre LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" 
    OR dir LIKE "%'.str_replace(' ', '%', $_POST['query']).'%"
  ';
}

$query .= 'ORDER BY id_home DESC ';

$filter_query = $query . 'LIMIT '.$start.', '.$limit.'';

$statement = $link->prepare($query);
$statement->execute();
$total_data = $statement->rowCount();

$statement = $link->prepare($filter_query);
$statement->execute();
$result = $statement->fetchAll();
$total_filter_data = $statement->rowCount();

$output = '';
if($total_data > 0)
{

  $output .='


        <!-- **** PROPERTY CONTAINER -->
        <div class="col-xl-12 col-md-12 col-sm-10 col-12 mb-5 m-0 d-flex flex-column property__container">
  ';

  foreach($result as $row)
  {
      
    if(empty($row['photo']) || $row['photo'] == 'NULL' ){
      $photo = '../assets/img/empty.png';
    }else{
        $photo = '../'. $row['photo'];
    }
      
    if($row['dir'] == 'NULL' && $row['city'] != 'NULL'){
        $direction = $row['city'];
    }else{
        if($row['dir'] != 'NULL' && $row['city'] == 'NULL'){
            $direction = $row['dir'];
        }else if($row['dir'] != 'NULL' && $row['city'] != 'NULL'){
            $direction = $row['dir'] . ', ' . $row['city'];
        }else{
            $direction = 'Empty';
        }
    }
    
    if(empty($row['room']) || $row['room'] == 'NULL'){
        $room = 'Empty';
    }else{
        $room = $row['room'];
    }
    
    if(empty($row['status']) || $row['status'] == 'NULL'){
        $status = 'Empty';
    }else{
        $status = $row['status'];
    }
    
    if(empty($row['g_pre']) || $row['g_pre'] == 'NULL'){
        $g_pre = 'Empty';
    }else{
        $g_pre = $row['g_pre'];
    }
    
    if(empty($row['pet']) || $row['pet'] == 'NULL'){
        $pet = 'Empty';
    }else{
        $pet = $row['pet'];
    }
    
    if(empty($row['ag_pre']) || $row['ag_pre'] == 'NULL'){
        $ag_pre = 'Empty';
    }else{
        $ag_pre = $row['ag_pre'];
    }
    
    if(empty($row['certified']) || $row['certified'] == 'NULL'){
        $certified = 'Empty';
    }else{
        $certified = $row['certified'];
    }

    $output .= '
    
          <!-- **** PROPERTY -->
          <div class="w-100 mb-5 d-flex flex-xl-row flex-column bg-white shadow position-relative property">
          
          <!-- **** PROPERTY IMAGE -->
          <div class="d-flex justify-content-center shadow property__image">
              <img src="'.$photo.'">
              <span class="py-1 px-2 bg-white small rounded shadow">'.$status.'</span>
            </div>

            <!-- **** PROPERTY INFORMATION -->
            <div class="w-100 d-flex flex-column align-items-center justify-content-between bg-white property__info">

              <!-- TITLE --> 
              <div class="w-100 py-2 text-center">
                <span class="fs-5">'.$row['h_name'].'</span>
                <span class="d-flex align-items-center justify-content-center fs-6 text-black-50 text-capitalize">
                  <img src="../assets/icon/location-border.png" class="opacity-50 mr-2" width="16px" height="16px">
                  '.$direction.'
                </span>
              </div>

              <!-- DETAILS -->   
              <div class="w-100 d-flex px-xl-2 py-2 pb-4 mx-auto justify-content-evenly text-center small property__details">
                
                <div class="px-md-2 mx-0" style="width: 32% !important;">
                  <div>
                    <p class="mb-1">Bedrooms</p>
                    <span class="text-black-50">'.$room.'</span>
                  </div>

                  <hr class="w-50">

                  <div class="">
                    <p class="mb-1">Gender</p>
                    <span class="text-black-50">'.$g_pre.'</span>
                  </div>
                </div>



                <div class="px-md-2 mx-0" style="width: 32% !important;">
                  <div>
                    <p class="mb-1">Pets</p>
                    <span class="text-black-50">'.$pet.'</span>
                  </div>

                  <hr class="w-50">

                  <div>
                    <p class="mb-1">Age</p>
                    <span class="text-black-50">'.$ag_pre.'</span>
                  </div>
                </div>


                <div class="px-md-2 mx-0 align-self-center" style="width: 32% !important;">
                  <p class="mb-1">Certified</p>
                  <span class="text-black-50">'.$certified.'</span>
                </div>

              </div>


              <!-- MORE DETAILS -->
              <a href="detail?art_id='.$row['id_home'].'" class="w-100 py-3 rounded-0 border-0 more__details">
                More details
              </a>
            </div>

          </div>
          <!-- **** END PROPERTY -->

    ';
    
  }
}
else
{
  $output .= '
  <tr>
    <td colspan="2" align="center">No Data Found</td>
  </tr>
  ';
}

 $output .='
 
 </div>
              
 
 ';

$output .= '
<br />
<div align="center" class="pagination__coordination">
  <ul class="pagination justify-content-center">
';

$total_links = ceil($total_data/$limit);
$previous_link = '';
$next_link = '';
$page_link = '';

//echo $total_links;

if($total_links > 4)
{
  if($page < 5)
  {
    for($count = 1; $count <= 5; $count++)
    {
      $page_array[] = $count;
    }
    $page_array[] = '...';
    $page_array[] = $total_links;
  }
  else
  {
    $end_limit = $total_links - 5;
    if($page > $end_limit)
    {
      $page_array[] = 1;
      $page_array[] = '...';
      for($count = $end_limit; $count <= $total_links; $count++)
      {
        $page_array[] = $count;
      }
    }
    else
    {
      $page_array[] = 1;
      $page_array[] = '...';
      for($count = $page - 1; $count <= $page + 1; $count++)
      {
        $page_array[] = $count;
      }
      $page_array[] = '...';
      $page_array[] = $total_links;
    }
  }
}
else
{
  for($count = 1; $count <= $total_links; $count++)
  {
    $page_array[] = $count;
  }
}

if (isset($page_array)) {
for($count = 0; $count < count($page_array); $count++)
{
  if($page == $page_array[$count])
  {
    $page_link .= '
    <li class="page-item active">
      <a href="#" class="page-link">'.$page_array[$count].'<span class="sr-only">(current)</span></a>
    </li>
    ';

    $previous_id = $page_array[$count] - 1;
    if($previous_id > 0)
    {
      $previous_link = '
      <li class="page-item">
        <a class="page-link page_previous" href="javascript:void(0)" data-page_number="'.$previous_id.'">
          <img class="" src="../assets/icon/arrow-left-click.png" width="32px" height="32px">
        </a>
      </li>';
    }
    else
    {
      $previous_link = '
      <li class="page-item disabled">
        <a class="page-link page_previous" href="#">
          <img class="" src="../assets/icon/arrow-left-light.png" width="32px" height="32px">
        </a>
      </li>
      ';
    }
    $next_id = $page_array[$count] + 1;
    if($next_id > $total_links)
    {
      $next_link = '
      <li class="page-item disabled">
        <a class="page-link page_next" href="#">
          <img src="../assets/icon/arrow-right-light.png" width="32px" height="32px">
        </a>
      </li>
        ';
    }
    else
    {
      $next_link = '
        <li class="page-item">
        <a class="page-link page_next" href="javascript:void(0)" data-page_number="'.$next_id.'">
          <img src="../assets/icon/arrow-right-click.png" width="32px" height="32px">
        </a>
        </li>';
    }
  }
  else
  {
    if($page_array[$count] == '...')
    {
      $page_link .= '
      <li class="page-item disabled">
          <a class="page-link" href="#">...</a>
      </li>
      ';
    }
    else
    {
      $page_link .= '
      <li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$page_array[$count].'">'.$page_array[$count].'</a></li>
      ';
    }
  }
}
}

$output .= $previous_link . $page_link . $next_link;
$output .= '
  </ul>

';

echo $output;

?>