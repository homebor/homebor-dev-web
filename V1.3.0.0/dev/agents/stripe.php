<?php
require '../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$query = "SELECT * FROM users WHERE mail = '$usuario'";
$resultado = $link->query($query);

$row = $resultado->fetch_assoc();

if ($row['usert'] != 'Agent') header("location: ../logout.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../assets/css/utility.css">
  <link rel="stylesheet" href="assets/css/stripe.css">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Stripe API</title>
</head>

<!-- //TODO INCLUDE HEADER -->
<?php include 'header.php' ?>


<body>

  <main>
    <br><br><br><br>
    <h1 class="col-10 mx-auto mt-5">Stripe API</h1>

    <hr>

    <div class="d-flex align-items-center container list-buttons">
      <button id="show-customers" class="btn">Customers</button>
      <button id="show-products" class="btn">Products</button>
    </div>
    <section id="container" class="align-items-start my-5 container list-container">
    </section>

    <!-- // * CREATE CUSTOMER FORM -->
    <form id="create-customer-form" action="#" class="d-none col-6 mx-auto shadow alert alert-light">
      <h3 class="mt-2 text-center">Register a New Customer</h3>
      <hr>

      <label for="">Customer Email *</label>
      <input class="form-control" type="text" name="email" placeholder="example@gmail.com">

      <br>

      <label for="">Customer Name *</label>
      <input class="form-control" type="text" name="name" placeholder="David Bronny">

      <br>

      <label for="">Customer Address *</label>
      <input class="form-control" type="text" name="address" placeholder="Toronto">

      <br>

      <label for="">Customer Phone Number *</label>
      <input class="form-control" type="text" name="phone" placeholder="+1 623 325 2381">

      <br>

      <label for="">Customer Description</label>
      <textarea class="form-control" name="description" placeholder="Insert description" cols="10" rows="4"></textarea>

      <br>

      <label for="">Customer type</label>
      <select class="form-control" name="rol">
        <option value="" selected hidden>Select type *</option>
        <option value="Agent">Agent</option>
        <option value="Student">Student</option>
        <option value="Homestay">Homestay</option>
      </select>


      <input class="w-25 mx-auto my-2 mt-5 form-control font-weight-bold text-white bg-primary" type="submit"
        value="Register">
    </form>


    <!-- // * CREATE PRODUCT FORM-->
    <form id="create-product-form" action="#" class="d-none col-6 mx-auto shadow alert alert-light">
      <h3 class="mt-2 text-center">Register a New Product</h3>
      <hr>

      <label for="">Product Name *</label>
      <input class="form-control" type="text" name="name" placeholder="T-shirt">

      <br>

      <label for="">Product Price *</label>
      <input class="form-control" type="text" name="price" placeholder="150">

      <br>

      <label for="">Product Image *</label>
      <input class="form-control" type="file" name="image">

      <br>

      <label for="">Product Description</label>
      <textarea class="form-control" name="description" placeholder="Insert description" cols="10" rows="4"></textarea>

      <br>


      <input class="w-25 mx-auto my-2 mt-5 form-control font-weight-bold text-white bg-primary" type="submit"
        value="Register">
    </form>

  </main>

  <TEMPLATES>
    <template id="customer-template">
      <article class="mr-4 shadow customer">
        <div class="mx-auto d-flex justify-content-center align-items-center customer-image">
          <img data-customer-image src="../assets/emptys/profile-student-empty.png" alt="">
        </div>
        <h3 class="py-2 text-center text-capitalize text-white font-weight-bold" data-customer-name
          data-customer-rol="ROL">
          Insert fullname
        </h3>

        <div class="mt-5 customer-address">
          <p><b>Address:</b> <span data-customer-address></span></p>
        </div>

        <div class="customer-phone">
          <p><b>Phone:</b> <span data-customer-phone></span></p>
        </div>

        <div class="customer-created">
          <p><b>Created:</b> <span data-customer-created></span></p>
        </div>

        <div class="customer-description">
          <p><b>Description:</b> <span data-customer-description></span></p>
        </div>

      </article>
    </template>


    <template id="product-template">
      <article class="mr-4 shadow product">
        <div class="d-flex justify-content-between align-items-center product-header">
          <h3 class="m-0 p-0" data-product-name>Insert product name</h3>
          <span class="m-0 p-0" data-product-price>$0</span>
        </div>

        <div class="mx-auto d-flex justify-content-center align-items-center product-image">
          <img data-product-image src="" alt="">
        </div>

        <br>

        <div class="product-created">
          <p data-product-created><b>Created:</b> Insert created date</p>
          <p data-product-update><b>Last Update </b> Insert last update</p>
        </div>

        <div class="product-description">
          <p data-product-description><b>Description:</b> Insert the description of user</p>
        </div>
      </article>
    </template>
  </TEMPLATES>

  <?php include 'footer.php' ?>

  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <!-- <script src="../assets/js/popper.min.js"></script> -->
  <!-- <script src="../assets/bootstrap/js/bootstrap.min.js"></script> -->
  <script src="assets/js/stripe_test.js" type="module"></script>
</body>

</html>