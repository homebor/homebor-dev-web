<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';

define('Tag',chr(149));
define('Tag2',chr(155));


session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM pe_home WHERE id_home = '$id'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

if ($usuario = $row['a_mail']) {
   
} else {
     header("location: ../index.php");
}
}

$username = $row2['name_h'].' '.$row2['l_name_h'];
$address = $row2['dir'].', '.$row2['city'];

$orgDate = $row2['db'];
$newDate = date("d-m-Y", strtotime($orgDate));

 
$pdf = new FPDF('p', 'mm', 'Letter');
$pdf->AddPage(); 
$pdf->SetTitle('Homestay Resident Suitability Declaration');

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',120,10,-300);


$pdf->SetFont('Arial','B',16);
$pdf->SetXY(0,50);
$pdf->Cell(0,0,"Homestay Resident Suitability Declaration",0,0,'C');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,60);
$pdf->Cell(0,0,"Full Name",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(39,61);
$pdf->Cell(0,0,"_________________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(40,60);
$pdf->Cell(0,0,$username,0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(97,60);
$pdf->Cell(0,0,"Date of Birth (YYYY-MM-DD)",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(139,61);
$pdf->Cell(0,0,"___________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(141,60);
$pdf->Cell(0,0,$row2['db'],0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,65);
$pdf->Cell(0,0,"Address",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(39,66);
$pdf->Cell(0,0,"___________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(40,65);
$pdf->Cell(0,0,$row2['dir'],0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(88,65);
$pdf->Cell(0,0,"City",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(96,66);
$pdf->Cell(0,0,"___________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(98,65);
$pdf->Cell(0,0,$row2['city'],0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,69);
$pdf->Cell(0,0,"Province",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(39,70);
$pdf->Cell(0,0,"___________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(40,69);
$pdf->Cell(0,0,$row2['state'],0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(88,69);
$pdf->Cell(0,0,"Postal Code",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(108,70);
$pdf->Cell(0,0,"___________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(110,69);
$pdf->Cell(0,0,$row2['p_code'],0,0,'L');


$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,78);
$pdf->Cell(0,0,"All accredited institutions of Languages Canada (LC) are committed to providing their students a secure and safe",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,82);
$pdf->Cell(0,0,"environment in homestay",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,94);
$pdf->Cell(0,0,"To comply with Languages Canada standards, institutions that wish to place students in homestay must require",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,98);
$pdf->Cell(0,0,"Criminal Record Checks (CRC's) from all individuals normally resident in the home who have reached the age of",0,0,'L');


$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,102);
$pdf->Cell(0,0,"majority.",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,114);
$pdf->Cell(0,0,"This declaration does not replace the CRC, except as an interim measure. For those members who wish to place",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,118);
$pdf->Cell(0,0,"students in homestay prior to the required CRC documents being received by the institution, this form must be",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,122);
$pdf->Cell(0,0,"used, and it must be received by the homestay coordinator at the LC institution before any students are placed",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,126);
$pdf->Cell(0,0,"in the home. For institutions that require CRC's to be received prior to placement, this form is not required.",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,138);
$pdf->Cell(0,0,"Have you ever:",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,146);
$pdf->Cell(0,0,"1.Been convicted of any offence against a minor? ..................................................................................... YES   NO",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,154);
$pdf->Cell(0,0,"2.Had a minor removed from your care by order of a court? ...................................................................... YES   NO",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,162);
$pdf->Cell(0,0,"3.Been disqualified from acting as a foster parent? .................................................................................... YES   NO",0,0,'L');


$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,170);
$pdf->Cell(0,0,"4.Been proven negligent while supervising minors under 18 years of age on activities/courses organized by any",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,174);
$pdf->Cell(0,0,"other organization? ..................................................................................................................................... YES    NO",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,186);
$pdf->Cell(0,0,"You agree to the following conditions:",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,194);
$pdf->Cell(0,0,"1. I will not engage in any sexual conduct with the student or sexually harass or use my position of",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,198);
$pdf->Cell(0,0,"authority as an adult with the student in an inappropriate manner.",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,204);
$pdf->Cell(0,0,"2. I agree that if I am convicted of an offence, I shall report it within 24 hours to the LC institution or to",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,208);
$pdf->Cell(0,0,"the homestay placement agency under contract with the LC institution.",0,0,'L');


$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,216);
$pdf->Cell(0,0,"3. I hereby declare that the foregoing information is true and complete to my knowledge. I understand",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,220);
$pdf->Cell(0,0,"that a false statement may disqualify me from providing homestay accommodation.",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,226);
$pdf->Cell(0,0,"4.I agree to apply for a Criminal Record Check (CRC) from the appropriate authorities within seven (7)",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,230);
$pdf->Cell(0,0,"days of completing this Suitability Declaration; I further agree to submit the completed CRC to the LC",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,234);
$pdf->Cell(0,0,"institution upon receipt of the CRC.",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,254);
$pdf->Cell(0,0,"________________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(110,254);
$pdf->Cell(0,0,"________________________________",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(23,258);
$pdf->Cell(0,0,"Signature",0,0,'L');

$pdf->SetFont('Arial','',9);
$pdf->SetXY(109,258);
$pdf->Cell(0,0,"Date (YYYY-MM-DD)",0,0,'L');


$pdf->Output();
?>