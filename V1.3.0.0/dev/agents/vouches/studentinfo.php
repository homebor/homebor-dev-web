<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';

define('Tag',chr(149));
define('Tag2',chr(155));


session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM pe_student WHERE id_student = '$id'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query3="SELECT * FROM pe_student INNER JOIN academy ON pe_student.mail_s = '$row2[mail_s]' AND pe_student.n_a = academy.id_ac";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$academy = $row3['name_a'].' - '.$row3['dir_a'];

$name = $row2['name_s'].' '.$row2['l_name_s'];

$namEmergency = $row2['cont_name'].' '.$row2['cont_lname'];

$photo_s = 'https://homebor.com/'.$row2['photo_s'];


if ($row['id_m'] != $row2['id_m']) {
	 header("location: ../index.php");
}
}

class PDF extends FPDF
{

function Footer()
{
    
    //Footer Message
    $this->Image('../../assets/img/whatsapp-document-pdf.png',87,277,5,5);
    $this->SetXY(15,-18);
    $this->SetFont('Arial','B',12);
    $this->Cell(0,0,'+1(416)579 95 41',0,0,'C');

    $this->Image('../../assets/img/email-document-pdf.png',145,277,5,5);
    $this->SetXY(155,-18);
    $this->SetFont('Arial','B',12);
    $this->Cell(0,0,'info@ihomestaycanada.com',0,0,'C');

    $this->Image('../../assets/img/web-document-pdf.png',110,284,5,5);
    $this->SetXY(85,-10);
    $this->SetFont('Arial','B',12);
    $this->Cell(0,0,'www.ihomestaycanada.com',0,0,'C');
}
}


$pdf = new PDF();
$pdf->AddPage();

//Title
$pdf->SetTitle('Student Information');

//Frame Up
$pdf->SetXY(0,0);
$pdf->SetFillColor(255,224,240,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,50,'',0, 0, 'C', True);

//Student Name
if($row2['name_s'] != 'NULL' && $row2['l_name_s'] != 'NULL') {
    $pdf->SetXY(100,25);
    $pdf->SetFont('Arial','',18);
    $pdf->SetMargins(5, 5);
    $pdf->MultiCell( 80, 5,$name, 0,'C');
}

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',115,5,50,10);

//Frame Left
$pdf->SetXY(10,20);
$pdf->SetFillColor(209,193,225,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(70,250,'',0, 0, 'C', True);

$pdf->Image('../../assets/img/piezas-ihomestay-rectangulo.png',15,25,60,60);
if($row2['photo_s'] == 'NULL') {
    $pdf->Image('../../assets/img/vacios-homebor-estudiante.png',18,27,55,55);
}else {
    $pdf->Image($photo_s,18,27,55,55);
}

//About Me Title
$pdf->SetXY(15,100);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'ABOUT ME:',0,0,'L');

//Frame Inside Left
$pdf->SetXY(15,110);
$pdf->SetFillColor(241,230,242,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(60,155,'',0, 0, 'C', True);


if($row2['about_me'] == 'NULL') {
    $pdf->SetXY(17,125);
    $pdf->SetFont('Arial','B',12);
    $pdf->SetMargins(5, 5);
    $pdf->MultiCell( 55, 10,'No Information', 0);
}else {
    $pdf->SetXY(17,125);
    $pdf->SetFont('Arial','B',12);
    $pdf->SetMargins(5, 5);
    $pdf->MultiCell( 55, 10,utf8_decode($row2['about_me']), 0);
}



//About Me Title
$pdf->SetXY(90,60);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,Tag.' ABOUT ME:',0,0,'L');

//DATE OF Birth Title
$pdf->SetXY(85,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'DATE OF BIRTH:',0,0,'L');

//DATE OF Birth Frame
$pdf->SetXY(121,66);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(86,8,'',0, 0, 'C', True);

//DATE OF Birth Variable
if($row2['db_s'] == 'NULL') {
    $pdf->SetXY(122,70);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(122,70);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['db_s']),0,0,'L');
}


//CITIZENSHIP Title
$pdf->SetXY(85,80);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'CITIZENSHIP:',0,0,'L');

//CITIZENSHIP Frame
$pdf->SetXY(115,76);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(92,8,'',0, 0, 'C', True);

//CITIZENSHIP Variable
if($row2['nationality'] == 'NULL') {
    $pdf->SetXY(116,80);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(116,80);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['nationality']),0,0,'L');
}

//EMAIL Title
$pdf->SetXY(85,90);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'EMAIL:',0,0,'L');

//EMAIL Frame
$pdf->SetXY(101,86);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(106,8,'',0, 0, 'C', True);

//EMAIL Variable
$pdf->SetXY(102,90);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,utf8_decode($row2['mail_s']),0,0,'L');

//CELLPHONE Title
$pdf->SetXY(85,100);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'TELEPHONE:',0,0,'L');

//CELLPHONE Frame
$pdf->SetXY(114,96);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(93,8,'',0, 0, 'C', True);

//CELLPHONE Variable
if($row2['num_s'] == 'NULL') {
    $pdf->SetXY(115,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(115,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['num_s']),0,0,'L');
}

//Additional Information Title
$pdf->SetXY(90,118);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,Tag.' ADDITIONAL INFORMATION:',0,0,'L');

//SPECIAL DIET Title
$pdf->SetXY(85,128);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'SPECIAL DIET:',0,0,'L');

//SPECIAL DIET Frame
$pdf->SetXY(117,124);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(90,8,'',0, 0, 'C', True);

if($row2['vegetarians'] == 'no' AND $row2['halal'] == 'no' AND $row2['kosher'] == 'no' AND $row2['lactose'] == 'no' AND $row2['gluten'] == 'no' AND $row2['pork'] == 'no'){
    //SPECIAL DIET Variable
    $pdf->SetXY(118,128);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No',0,0,'L');
} else {
    $pdf->SetXY(118,128);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'Yes',0,0,'L');
}


//PET ALLERGIES Title
$pdf->SetXY(85,138);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'PET ALLERGIES:',0,0,'L');

//PET ALLERGIES Frame
$pdf->SetXY(121,134);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(86,8,'',0, 0, 'C', True);

//PET ALLERGIES Variable
if($row2['pets'] == 'NULL') {
    $pdf->SetXY(122,138);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(122,138);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['pets']),0,0,'L');
}


//ALLERGIES Title
$pdf->SetXY(85,148);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'ALLERGIES:',0,0,'L');

//ALLERGIES Frame
$pdf->SetXY(112,144);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(95,8,'',0, 0, 'C', True);

//ALLERGIES Variable
if($row2['allergies'] == 'NULL') {
    $pdf->SetXY(113,148);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(113,148);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['allergies']),0,0,'L');
}

//FOOD ALLERGIES Title
$pdf->SetXY(85,158);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'FOOD ALLERGIES:',0,0,'L');

//FOOD ALLERGIES Frame
$pdf->SetXY(125,154);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(82,8,'',0, 0, 'C', True);

//FOOD ALLERGIES Variable
if($row2['allergy_m'] == 'NULL') {
    $pdf->SetXY(126,158);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(126,158);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['allergy_m']),0,0,'L');
}

//Smoke Title
$pdf->SetXY(85,168);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'DO YOU SMOKE?:',0,0,'L');

//Smoke Frame
$pdf->SetXY(125,164);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(82,8,'',0, 0, 'C', True);

//Smoke Variable
if($row2['smoke_s'] == 'NULL') {
    $pdf->SetXY(126,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(126,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['smoke_s']),0,0,'L');
}

//STUDENT INFORMATION Title
$pdf->SetXY(90,180);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,Tag.' STUDENT INFORMATION:',0,0,'L');

//SCHOOL Title
$pdf->SetXY(85,190);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'SCHOOL:',0,0,'L');

//SCHOOL Frame
$pdf->SetXY(106,186);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(100,8,'',0, 0, 'C', True);

//SCHOOL Variable
if($row3['acronyms'] == 'NULL') {
    $pdf->SetXY(107,190);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(107,190);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['acronyms']),0,0,'L');
}


//WHAT WILL YOU STUDY IN CANADA? Title
$pdf->SetXY(85,200);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'WHAT WILL YOU STUDY IN CANADA?:',0,0,'L');

//WHAT WILL YOU STUDY IN CANADA? Frame
$pdf->SetXY(165,196);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(41,8,'',0, 0, 'C', True);

//WHAT WILL YOU STUDY IN CANADA? Variable
if($row2['type_s'] == 'NULL') {
    $pdf->SetXY(166,200);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(166,200);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['type_s']),0,0,'L');
}

$pdf->Image('../../assets/img/calendario-aplicacion-blanco.png',90,205,25,25);
//ARRIVAL DATE Title
$pdf->SetXY(125,215);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'ARRIVAL DATE:',0,0,'L');

//ARRIVAL DATE Frame
$pdf->SetXY(160,210);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(46,8,'',0, 0, 'C', True);

//ARRIVAL DATE Variable
if($row2['firstd'] == 'NULL') {
    $pdf->SetXY(161,215);
$pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(161,215);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['firstd']),0,0,'L');
}

//DEPARTURE DATE Title
$pdf->SetXY(135,225);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'END DATE:',0,0,'L');

//DEPARTURE DATE Frame
$pdf->SetXY(160,220);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(46,8,'',0, 0, 'C', True);

//DEPARTURE DATE Variable
if($row2['lastd'] == 'NULL') {
    $pdf->SetXY(161,225);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(161,225);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['lastd']),0,0,'L');
}

//DOWN Frame
$pdf->SetXY(79,230);
$pdf->SetFillColor(209,193,225,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(170,8,'',0, 0, 'C', True);

//EMERGENCY CONTACT Title
$pdf->SetXY(90,245);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,Tag.' EMERGENCY CONTACT:',0,0,'L');

//NAME OF EMERGENCY CONTACT Title
$pdf->SetXY(85,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Name of Emergency Contact:',0,0,'L');

//NAME OF EMERGENCY CONTACT Frame
$pdf->SetXY(145,250);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(61,8,'',0, 0, 'C', True);

//NAME OF EMERGENCY CONTACT Variable
if($row2['cont_name'] == 'NULL' AND $row2['cont_lname'] == 'NULL') {
    $pdf->SetXY(146,255);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(146,255);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($namEmergency),0,0,'L');
}

//Phone Number Title
$pdf->SetXY(85,265);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Phone Number:',0,0,'L');

//Phone Number Frame
$pdf->SetXY(119,260);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(87,8,'',0, 0, 'C', True);

//Phone Number Variable
if($row2['num_conts'] == 'NULL') {
    $pdf->SetXY(120,265);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Information',0,0,'L');
}else {
    $pdf->SetXY(120,265);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row2['num_conts']),0,0,'L');
}

$pdf->Output();
?>