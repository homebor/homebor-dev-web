<?php
require '../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];


$query2 = "SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado2 = $link->query($query2);

$row2 = $resultado2->fetch_assoc();

$query3 = "SELECT * FROM manager INNER JOIN agents ON agents.a_mail = '$usuario' and manager.id_m = agents.id_m";
$resultado3 = $link->query($query3);

$row3 = $resultado3->fetch_assoc();

$query4 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.id_m = '$row3[id_m]' and academy.id_ac = manager.n_a");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.id_m = '$row3[id_m]' and academy.id_ac = manager.n_a2");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.id_m = '$row3[id_m]' and academy.id_ac = manager.n_a3");
$row6 = $query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.id_m = '$row3[id_m]' and academy.id_ac = manager.n_a4");
$row7 = $query7->fetch_assoc();

$query8 = $link->query("SELECT * FROM manager INNER JOIN academy ON manager.id_m = '$row3[id_m]' and academy.id_ac = manager.n_a5");
$row8 = $query8->fetch_assoc();

$sql = "SELECT * FROM pe_home WHERE id_m = '$row2[id_m]' AND certified = 'No'";

$connStatus = $link->query($sql);

$numberOfRows = mysqli_num_rows($connStatus);

$query9 = $link->query("SELECT * FROM propertie_control WHERE id_m = '$row2[id_m]'");

//TOTAL OF STUDENTS
$sql2 = "SELECT * FROM pe_student WHERE n_a = $row4[id_ac]";
$connStatus2 = $link->query($sql2);
$numberOfRows2 = mysqli_num_rows($connStatus2);

$sql3 = "SELECT * FROM pe_student WHERE n_a = $row5[id_ac]";
$connStatus3 = $link->query($sql3);
$numberOfRows3 = mysqli_num_rows($connStatus3);

$sql4 = "SELECT * FROM pe_student WHERE n_a = $row6[id_ac]";
$connStatus4 = $link->query($sql4);
$numberOfRows4 = mysqli_num_rows($connStatus4);

$sql5 = "SELECT * FROM `pe_student` WHERE n_a = $row7[id_ac]";
$connStatus5 = $link->query($sql5);
$numberOfRows5 = mysqli_num_rows($connStatus5);

$sql6 = "SELECT * FROM `pe_student` WHERE n_a = $row8[id_ac]";
$connStatus6 = $link->query($sql6);
$numberOfRows6 = mysqli_num_rows($connStatus6);

$query10 = "SELECT * FROM manager WHERE id_m = '$row2[id_m]' ";
$resultado10 = $link->query($query10);

$row10 = $resultado10->fetch_assoc();
?>


<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.16">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css?ver=1.0.16">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.16">
  <link rel="stylesheet" href="assets/css/index.css?ver=1.0.16">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.16">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js?ver=1.0.16"></script>

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Coordinators</title>

</head>

<!-- // TODO HEADER -->
<?php require 'header.php';  ?>


<body>


  <!-- // TODO MAIN  -->
  <main>
    <br><br><br><br><br><br>

    <!-- // TODO TITLE -->
    <section id="page-title" class="col-10 mx-auto">
      <h1 class="text-lg-left text-center title-agents">Coordinator Dashboard</h1>
    </section>

    <div class="d-lg-none">
      <br><br>
    </div>

    <div class="col-9 col-md-10 mx-auto container-gradient">
      <!-- // TODO AGENT DATA CONTAINER -->
      <section class="p-0 mx-auto d-flex flex-md-row flex-column align-items-md-stretch justify-content-between align-items-center">

        <!-- // * ARTICLE LEFT -->
        <article class="mb-4 mb-md-0 py-4 px-2 px-lg-4 px-xl-0 d-xl-flex justify-content-center justify-content-xl-around align-items-center agent-card">
          <aside class="d-flex flex-column justify-content-center align-items-center agent-image">
            <h3 class="text-capitalize username-agent">
              <?php if ($row2['name'] != 'NULL') echo $row2['name'] . ' ' . $row2['l_name'] ?>
            </h3>

            <div class="d-flex justify-content-center align-items-center coordinator-img">
              <?php if ($row2['photo'] != 'NULL') echo '<img src="../' . $row2['photo'] . '">' ?>
            </div>
          </aside>



          <aside class="col-10 col-sm-7 col-md-10 col-lg-11 col-xl-5 mx-auto m-xl-0 p-0 pb-3 pb-lg-0 d-flex flex-column justify-content-lg-between align-items-center agent-information">
            <!-- // *** Mail-->
            <div class="w-100 d-flex flex-column">
              <span class="d-flex align-items-center">
                <img src="../assets/icon/email-black2.png">
                <b class="font-weight-bold">Email</b>
              </span>
              <span class="font-weight-light">
                <?php if ($row2['a_mail'] != 'NULL') echo $row2['a_mail'] ?>
              </span>
            </div>


            <!-- // *** Phone-->
            <div class="w-100 d-flex flex-column">
              <span class="d-flex align-items-center">
                <img src="../assets/icon/phone.png">
                <b class="font-weight-bold">Phone</b>
              </span>
              <span class="font-weight-light">
                <?php if ($row2['num'] != 'NULL') echo $row2['num'] ?>
              </span>
            </div>


            <!-- // *** User Type-->
            <div class="w-100 d-flex flex-column">
              <span class="d-flex align-items-center">
                <img src="../assets/icon/house2.png">
                <b class="font-weight-bold">Homestay Provider</b>
              </span>
              <span class="font-weight-light">
                <?php if ($row10['a_name'] != 'NULL') echo $row10['a_name'] ?>
              </span>
            </div>


            <!-- // *** Address-->
            <div class="w-100 d-flex flex-column">
              <span class="d-flex align-items-center">
                <img src="../assets/icon/location-border.png">
                <b class="font-weight-bold">Location</b>
              </span>
              <span class=" font-weight-light">
                <?php
                if ($row2['base_home'] == 'NULL') echo "Empty";
                else echo $row2['base_home'];
                ?>
              </span>
            </div>
          </aside>
        </article>



        <!-- // * ARTICLE RIGHT -->
        <article class="mt-5 mt-md-0 py-3 py-xl-0 d-flex flex-xl-row flex-column-reverse justify-content-around align-items-center agency-graphics">
          <!-- <aside class="position-absolute pie-legends">
          <span id="toronto-legend">Toronto</span>
          <span id="montreal-legend">Montreal</span>
          <span id="ottawa-legend">Ottawa</span>
          <span id="quebec-legend">Quebec</span>
          <span id="calgary-legend">Calgary</span>
          <span id="vancouver-legend">Vancouver</span>
          <span id="victoria-legend">Victoria</span>
          <span id="waterloo-legend">Waterloo</span>
          <span id="guelph-legend">Guelph</span>
        </aside> -->


          <div><canvas id="forCity"></canvas></div>
          <br class="d-lg-none">
          <div><canvas id="forCertified"></canvas></div>

        </article>

      </section>


      <section class="mx-auto px-4 d-flex flex-column flex-lg-row justify-content-center align-items-center agency-statistics">
        <!-- // ** TOTAL STUDENTS / TOTAL HOMESTAY -->
        <article class="px-2 px-md-0">
          <div class="mx-lg-auto d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/profile3.png" alt="icon logo" id="profile-icon">
              Total Students
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-total-students>120</span>
          </div>

          <br class="d-none d-lg-block"><br class="d-none d-md-block">

          <div class="mx-lg-auto d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/home.png" alt="icon logo" id="homestay-icon">
              Total Homestay
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-total-homestay>180</span>
          </div>
        </article>


        <!-- // ** CONFIRM REGISTER / SEARCH FOR HOMESTAY -->
        <article class="px-2 px-md-0">
          <div class="mx-lg-auto d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/user-register.png" alt="icon logo" id="confirm-icon">
              Students for Confirm Register
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-students-for-confirm>4</span>
          </div>

          <br class="d-none d-lg-block"><br class="d-none d-md-block">

          <div class="mx-lg-auto d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/search-user.png" alt="icon logo" id="search-icon">
              Students in Search for Homestay
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-students-in-search>20</span>
          </div>
        </article>


        <!-- // ** SELECT HOUSE / ACCEPT STUDENTS -->
        <article class="mt-md-4 mt-lg-0 px-2 px-md-0 d-md-flex d-lg-block align-items-center">
          <div class="mx-lg-auto d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/user-list.png" alt="icon logo" id="select-icon">
              Students for Select House
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-students-for-select>12</span>
          </div>

          <br class="d-none d-lg-block"><br class="d-none d-md-block">

          <div class="mx-lg-auto pl-md-3 pl-lg-0 d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/user-accept.png" alt="icon logo" id="accept-icon">
              House for Accept Students
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-homestay-for-accept>2</span>
          </div>
        </article>


        <br class="d-none d-md-block d-lg-none">

        <!-- // ** ACTIVE STUDENTS -->
        <article class="px-2 px-md-0 d-md-flex justify-content-center align-items-center">
          <div class="d-flex flex-row flex-md-column justify-content-between justify-content-md-center align-items-center">
            <h6 class="m-0 p-0 d-flex align-items-center justify-content-md-between justify-content-lg-center">
              <img src="../assets/icon/remove-user.png" alt="icon logo" id="active-icon">
              Active Students in Homestay
            </h6>
            <span class="ml-3 m-0 p-0 mt-md-2" data-active-students>8</span>
          </div>
        </article>
      </section>

    </div>

    <br><br><br><br><br>

    <!-- // TODO STUDENTS CONTAINER -->
    <h3 id="students-title" class="col-10 m-0 px-0 mx-auto mb-2 font-weight-bold text-center text-md-left">
      Students for Confirm Register
    </h3>
    <section id="all-students" class="col-12 d-flex align-items-center students-container" data-student-page="0">

    </section>

    <br>

    <!-- // TODO HOMESTAYS CONTAINER -->
    <h3 id="homestays-title" class="col-10 m-0 px-0 mx-auto mb-2 font-weight-bold text-center text-md-left">
      Homestay for Certified
    </h3>
    <section id="all-homestays" class="col-12 d-flex align-items-center homestays-container" data-homestay-page="0">

    </section>
  </main>


  <!-- // TODO TEMPLATES -->
  <TEMPLATES>
    <!-- // ! STUDENT TEMPLATE-->
    <template id="student-template">
      <article class="d-flex flex-column student-card">
        <div class="d-flex student-information">
          <img class="student-image" src="../assets/emptys/profile-student-empty.png" data-student-image alt="people">
          <div class="p-3 d-flex flex-column justify-content-center align-items-center text-capitalize">
            <h5 data-student-name>Student Name</h5>
            <span class="text-muted" data-student-nationality>Nationality</span>
            <dl class="d-flex m-0 mt-3 mt-lg-2">
              <dt class="mr-2">First Date:</dt>
              <dd data-student-firstdate>4 Jun 2022</dd>
            </dl>

            <dl class="d-flex m-0 mt-lg-2">
              <dt class="mr-2">Last Date:</dt>
              <dd data-student-lastdate>25 Jun 2022</dd>
            </dl>

            <dl class="text-center m-0 mt-2">
              <dt>School</dt>
              <dd data-student-school>Name</dd>
            </dl>
          </div>
        </div>
        <a href="#" class="btn student-more-details" data-student-profile>
          More Details
          <img src="../assets/icon/arrow-next.png" alt="arrow">
        </a>
      </article>
    </template>


    <!-- // ! HOMESTAY TEMPLATE-->
    <template id="homestay-template">
      <article class="d-flex flex-column homestay-card">
        <span class="homestay-status">Non Certified</span>
        <img class="homestay-image" src="../assets/emptys/frontage-empty.png" data-homestay-image alt="people">

        <div class="d-flex flex-column justify-content-center align-items-center text-capitalize homestay-information">
          <h5 class="pt-3 mb-0" data-homestay-name>Homestay Name</h5>
          <span class="mb-4 text-muted" data-homestay-address>Address</span>

          <div class="w-100 m-0 p-0 d-flex justify-content-around mb-2">
            <p class="d-flex flex-column align-items-center justify-content-center">
              <b>Bedrooms</b>
              <span data-homestay-bedrooms>4</span>
            </p>
            <p class="d-flex flex-column align-items-center justify-content-center">
              <b>Pets</b>
              <span data-homestay-pets>Yes</span>
            </p>
          </div>

          <div class="w-100 m-0 p-0 d-flex justify-content-around mb-2">
            <p class="d-flex flex-column align-items-center justify-content-center">
              <b>Gender</b>
              <span data-homestay-gender>Female</span>
            </p>
            <p class="d-flex flex-column align-items-center justify-content-center">
              <b>Age</b>
              <span data-homestay-age>Adult</span>
            </p>
          </div>
        </div>

        <a href="#" class="w-100 btn homestay-more-details" data-homestay-profile>
          More Details
          <img src="../assets/icon/arrow-next.png" alt="arrow">
        </a>
      </article>
    </template>
  </TEMPLATES>

  <!-- // TODO FOOTER -->
  <?php include 'footer.php'; ?>

  <!-- // TODO SCRIPTS  -->

  <!-- // ? CHARTS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js?ver=1.0.16"></script>

  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.16"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.16"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.16"></script>
  <script src="../assets/js/jquery.magnific-popup.min.js?ver=1.0.16"></script>
  <script src="assets/js/index.js?ver=1.0.16" type="module"></script>
  <script src="assets/js/chart.js?ver=1.0.16"></script>

</body>

</html>