<?php
include '../../xeon.php';
session_start();
// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);
$row=$resultado->fetch_assoc();

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/agent_profile.css?ver=1.0">
  <!--Mapbox Links-->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Coordinator Profile</title>

</head>

<body>

  <?php include 'header.php' ?>

  <div class="container_register">

    <main class="card__register">
      <form id="form" class="ts-form" autocomplete="off" action="edit_agent_profile.php" method="post"
        enctype="multipart/form-data">

        <h1 class="title__register">Profile Information</h1>


        <div class="row m-0 p-0 center">

          <div class="col-md-4 column-1">

            <div class="card">

              <h3 class="form__group-title__col-4">Basic Information</h3>

              <div class="information__content">

                <div class="form__group__div__photo">
                  <img class="form__group__photo" id="preview" src="../../<?php echo $row['photo'] ?>" alt="">

                  <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_c"
                    id="file" maxlength="1" accept="jpg|png|jpeg|pdf">
                </div>

                <div class="info"></div>



                <!-- Group Name -->

                <div class="form__group form__group__name" id="group__name">
                  <div class="form__group__icon-input">
                    <label for="name" class="form__label">Name *</label>

                    <label for="name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['name']) || $row['name'] == 'NULL'){}else{ ?>

                    <p class="form__group-input names"><?php echo $row['name'] ?></p>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>

                </div>


                <!-- Group Last Name -->

                <div class="form__group form__group__l_name" id="group__l_name">
                  <div class="form__group__icon-input">

                    <label for="l_name" class="form__label">Last Name *</label>

                    <label for="l_name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['l_name']) || $row['l_name'] == 'NULL'){}else{ ?>

                    <p class="form__group-input names"><?php echo $row['l_name'] ?></p>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>
                </div>

                <div class="d-flex justify-content-center">
                  <button type="button" class="btn_edit">Edit Profile</button>
                </div>

                <script>
                document.addEventListener('click', (e) => {
                  if (e.target.matches(".btn_edit")) window.top.location = 'edit_agent';

                })
                </script>



              </div>

            </div>


          </div>




          <div class="col-md-8 ">

            <div class="card column-2">

              <h3 class="form__group-title__col-4">Coordinator Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">



                  <!-- Group Coordinator Contact -->
                  <div class="form__group col-md-4 form__group__basic form__group__num" id="group__num">
                    <div class="form__group__icon-input">

                      <label for="num" class="form__label">Contact Number</label>

                      <label for="num" class="icon"><i class="icon icon__num"></i></label>

                      <?php if(empty($row['num']) || $row['num'] == 'NULL'){}else{ ?>

                      <p class="form__group-input num"><?php echo $row['num'] ?></p>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Coordinator Alternative Contact -->
                  <div class="form__group col-md-4 form__group__basic" id="group__num2">
                    <div class="form__group__icon-input">

                      <label for="num2" class="form__label">Alternative Contact</label>

                      <label for="num2" class="icon"><i class="icon icon__num"></i></label>

                      <?php if(empty($row['num2']) || $row['num2'] == 'NULL'){}else{ ?>

                      <p type="text" id="num2" name="num2" class="form__group-input num2"><?php echo $row['num2'] ?></p>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Language Spoken -->
                  <div class="form__group col-md-4 form__group__basic form__group__language" id="group__language">
                    <div class="form__group__icon-input">

                      <label for="language" class="form__label">Language Spoken</label>

                      <label for="language" class="icon"><i class="icon icon__city"></i></label>

                      <?php if(empty($row['language']) || $row['language'] == 'NULL'){}else{ ?>

                      <p class="form__group-input language"><?php echo $row['language'] ?></p>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>


                  <!-- Group Specialization -->
                  <div class="form__group col-md-4 form__group__basic form__group__specialization"
                    id="group__specialization">
                    <div class="form__group__icon-input">

                      <label for="specialization" class="form__label">Specialization</label>

                      <label for="specialization" class="icon"><i class="icon icon__spe"></i></label>

                      <?php if($row['specialization'] == 'homestay'){ ?>

                      <p class="form__group-input specialization">Homestay</p>

                      <?php }else if($row['specialization'] == 'students'){ ?>

                      <p class="form__group-input specialization">Students</p>

                      <?php }else if($row['specialization'] == 'both'){ ?>

                      <p class="form__group-input specialization">Both</p>

                      <?php } ?>


                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>

                </div>

              </div>

            </div>





          </div>


        </div><br>




      </form>
    </main>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>
  <div id="house_ass" style="display: none;">
    <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-9" id="assh">

        </div>
      </div>
    </div>

  </div>


  <?php include 'footer.php' ?>

  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    }

  }
  </script>

  <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
  <script>
  mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
  console.log(mapboxgl.accessToken);
  var client = new MapboxClient(mapboxgl.accessToken);
  console.log(client);


  var address = "<?php echo "$row[country], $row[city], $row[state]"; ?>"
  var test = client.geocodeForward(address, function(err, data, res) {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties

    console.log(res);
    console.log(res.url);
    console.log(data);

    var coordinates = data.features[0].center;

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v10',
      center: coordinates,
      zoom: 6
    });
    new mapboxgl.Marker()
      .setLngLat(coordinates)
      .addTo(map);


  });
  </script>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>


</body>

</html>