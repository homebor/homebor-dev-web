const formulary = document.getElementById('form');

const inputs = document.querySelectorAll('#form input');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^[1-8]{1}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    dir: /^.{1,200}$/, // 4 a 200 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const campos = {
    h_name: false,
    num: false,
    room: false,
    mail: false,
    pass: false,
    dir: false,
    city: false,
    state: false,
    p_code: false,
    name_h: false,
    l_name_h: false,
    cell: false,
}

const validateFormulary = (e) => {
    switch (e.target.name) {
        case "h_name":
            validateField(expresiones.name, e.target, 'h_name');
            break;
        case "num":
            validateField(expresiones.phone, e.target, 'num');
            break;
        case "room":
            validateField(expresiones.rooms, e.target, 'room');
            break;
        case "mail":
            validateField(expresiones.correo, e.target, 'mail');
            break;
        case "pass":
            validateField(expresiones.password, e.target, 'pass');
            break;


        case "dir":
            validateField(expresiones.dir, e.target, 'dir');
            break;
        case "city":
            validateField(expresiones.dir, e.target, 'city');
            break;
        case "state":
            validateField(expresiones.dir, e.target, 'state');
            break;
        case "p_code":
            validateField(expresiones.dir, e.target, 'p_code');
            break;

        
        case "name_h":
            validateField(expresiones.name_h, e.target, 'name_h');
            break;
        case "l_name_h":
            validateField(expresiones.name_h, e.target, 'l_name_h');
            break;
        case "cell":
            validateField(expresiones.phone, e.target, 'cell');
            break;
        

    }
}

const validateField = (expresion, input, campo) => {

    if (expresion.test(input.value)) {
        document.getElementById(`form__group-${campo}`).classList.remove('input_false');
        document.getElementById(`form__group-${campo}`).classList.add('input_true');
        document.querySelector(`.form__group-${campo} .message__input_error`).classList.remove('message__input_error_active');
        campos[campo] = true;

    } else {
        document.getElementById(`form__group-${campo}`).classList.add('input_false');
        document.getElementById(`form__group-${campo}`).classList.remove('input_true');
        document.querySelector(`.form__group-${campo} .message__input_error`).classList.add('message__input_error_active');
        campos[campo] = false;
    }

}

inputs.forEach((input) => {
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
});

formulary.addEventListener('submit', (e) => {
    e.preventDefault();

    if (campos.h_name && campos.num && campos.room && campos.mail && campos.pass) {

        var registerForm = new FormData($('#form')[0]);

        $.ajax({
            url: 'action_propertie.php',
            type: 'POST',
            data: registerForm,
            contentType: false,
            processData: false,
            success: function(response) {

                window.top.location = 'homestay';
            }
        });

        formulary.reset();

    } else {
        setTimeout(() => {
            document.getElementById('error_r').classList.add('message_error_active');

            setTimeout(() => {
                document.getElementById('error_r').classList.remove('message_error_active');

            }, 5000)
        }, 100);

    }
});