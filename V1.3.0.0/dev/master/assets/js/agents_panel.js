//CORDINATOR PANEL LIST
$(document).ready(function() {

    load_data(1);

    function load_data(page, query = '') {
        $.ajax({
            url: "fetch_agents.php",
            method: "POST",
            data: { page: page, query: query },
            success: function(data) {
                $('#dynamic_content').html(data);
            }
        });
    }

    $('#vou').hide();

    $('#search_box').keyup(function() {

        if ($('#search_box').val()) {

            $('#dynamic_content').hide();
            $('#vou').show();

            let search = $('#search_box').val();

            var searchLength = search.length;

            $.ajax({
                url: 'age-search.php',
                type: 'POST',
                data: { search },
                success: function(response) {
                    let age = JSON.parse(response);
                    let template = '';

                    age.forEach(agents => {
                        if (agents.photo != 'NULL') {
                            template += `
                        <tr>
                          <th class="text-center align-middle" style="font-weight:normal"><img src="../${agents.photo}" width="90px;" height="90px";></th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.names} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.agency} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.a_mail} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.status} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.specialization} </th>
                          <th class="text-center align-middle" align="left">
                          <ul>
                          <a href="agent_info.php?art_id=${agents.id_ag}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                              <i class="fa fa-eye mr-2"></i>
                                              Preview
                                          </a>
              
                      <br>
                           <a id="edit" href="agent_edit.php?art_id=${agents.id_ag}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                        <i class="fa fa-pencil-alt mr-2"></i>      
                                           Edit Agents
                                          </a>
                          </ul> 
                          </th>
              
                        </tr>`
                        } else {
                            template += `
                        <tr>
                          <th class="text-center align-middle" style="font-weight:normal"></th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.names} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.agency} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.a_mail} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.status} </th>
                          <th class="text-center align-middle" style="font-weight:normal"> ${agents.specialization} </th>
                          <th class="text-center align-middle" align="left">
                          <ul>
                          <a href="agent_info.php?art_id=${agents.id_ag}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                                              <i class="fa fa-eye mr-2"></i>
                                              Preview
                                          </a>
              
                      <br>
                           <a id="edit" href="agent_edit.php?art_id=${agents.id_ag}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                        <i class="fa fa-pencil-alt mr-2"></i>      
                                           Edit Agents
                                          </a>
                          </ul> 
                          </th>
              
                        </tr>`
                        }

                    });


                    $('#vouchers').html(template);
                }
            });
        } else {
            $('#dynamic_content').show();
            $('#vou').hide();
        }

    });

    $(document).on('click', '.page-link', function() {
        var page = $(this).data('page_number');
        var query = $('#search_box').val();
        load_data(page, query);
    });

});