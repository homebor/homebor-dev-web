$(document).ready(function() {

    load_data(1);

    function load_data(page, query = '') {
        $.ajax({
            url: "academy_list.php",
            method: "POST",
            data: { page: page, query: query },
            success: function(data) {
                $('#dynamic_content').html(data);
            }
        });
    }

    $('#vou').hide();

    $('#search_box').keyup(function() {

        if ($('#search_box').val()) {

            $('#dynamic_content').hide();
            $('#vou').show();

            let search = $('#search_box').val();

            var searchLength = search.length;

            $.ajax({
                url: 'aca-search.php',
                type: 'POST',
                data: { search, },
                success: function(response) {
                    let academies = JSON.parse(response);
                    let template = '';

                    academies.forEach(academy => {
                        if (academy.photo_a != 'NULL') {
                            template += `
            <tr>
              <th style="font-weight:normal"><img src="../${academy.photo_a}" width="90px;" height="90px"></th>
              <th style="font-weight:normal"> ${academy.name_a} </th>
              <th style="font-weight:normal"> ${academy.dir_a} </th>
              <th style="font-weight:normal"> ${academy.state_a} </th>
              <th style="font-weight:normal"> ${academy.city_a} </th>
              <th style="font-weight:normal"> ${academy.p_code_a} </th>
              <th align="left">
                <ul>
                  <a href="academy_preview.php?art_id=${academy.id_ac}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                    <i class="fa fa-eye mr-2"></i>
                      Preview
                   </a>
                  <br>

                  <a id="edit" href="academy_edit.php?art_id=${academy.id_ac}" class="btn btn-primary btn-sm btn-block float-right" name="edit" >
                    <i class="fa fa-pencil-alt mr-2"></i>      
                      Edit Academy
                  </a>
                </ul> 
              </th>

            </tr>`
                        } else {
                            template += `
            <tr>
              <th style="font-weight:normal"></th>
              <th style="font-weight:normal"> ${academy.name_a} </th>
              <th style="font-weight:normal"> ${academy.dir_a} </th>
              <th style="font-weight:normal"> ${academy.state_a} </th>
              <th style="font-weight:normal"> ${academy.city_a} </th>
              <th style="font-weight:normal"> ${academy.p_code_a} </th>
              <th align="left">
                <ul>
                  <a href="academy_preview.php?art_id=${academy.id_ac}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                    <i class="fa fa-eye mr-2"></i>
                      Preview
                   </a>
                  <br>

                  <a id="edit" href="academy_edit.php?art_id=${academy.id_ac}" class="btn btn-primary btn-sm btn-block float-right" name="edit" >
                    <i class="fa fa-pencil-alt mr-2"></i>      
                      Edit Academy
                  </a>
                </ul> 
              </th>

            </tr>`
                        }

                    });


                    $('#vouchers').html(template);
                }
            });
        } else {
            $('#dynamic_content').show();
            $('#vou').hide();
        }

    });

    $(document).on('click', '.page-link', function() {
        var page = $(this).data('page_number');
        var query = $('#search_box').val();
        load_data(page, query);
    });


});