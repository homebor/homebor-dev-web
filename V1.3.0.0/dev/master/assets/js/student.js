  $(document).ready(function() {

      load_data(1);

      function load_data(page, query = '') {
          $.ajax({
              url: "student_list.php",
              method: "POST",
              data: { page: page, query: query },
              success: function(data) {
                  $('#dynamic_content').html(data);
              }
          });
      }

      $('#vou').hide();

      $('#search_box').keyup(function() {

        if ($('#search_box').val()) {

          $('#dynamic_content').hide();
          $('#vou').show();

          let search = $('#search_box').val();

          var searchLength = search.length;

          $.ajax({
            url: 'stu-search.php',
            type: 'POST',
            data: { search, },
            success: function(response) {
              let students = JSON.parse(response);
              let template = '';

              students.forEach(student => {
                if (student.photo_s != 'NULL' && student.status == 'Waiting Answer') {
                  template += `
                    
                    <tr>

                      <th class="text-center align-middle" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.names} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.gen_s} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.mail_s} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.n_a} </th>
                      <th class="text-center align-middle" style="font-weight:normal"><b> ${student.status} Confirmation <a style="color: #232159; line-height: 30px; text-decoration: none" class="mail_reserve" href="homestay_admin.php?art_id=${student.id_home}">${student.mail_h} </b></a></th>
                      <th class="text-center align-middle" style="font-weight:normal"><b> ${student.firstd} / ${student.lastd} </b><br> (${student.week})</th>
                      <th class="text-center align-middle" align="left">
                        <ul>
                            <a href="../student/student_admin.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                              <i class="fa fa-eye mr-2"></i>
                                Preview
                            </a>
                          <br>
                            <a id="edit" href="../student/student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                              <i class="fa fa-id-card"></i>      
                                Edit Student
                            </a>
                          <br>
                            <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                              <i class="fa fa-pencil-alt mr-2"></i>      
                                Edit Assigment
                            </a>
                          <br>
                            <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                              <i class="fa fa-list"></i>
                                Assignation
                            </a>
  
                        </ul>
                      </th>

                    </tr>`
                } else if(student.photo_s == 'NULL' && student.status == 'Waiting Answer') {
                  
                  template += `
                    <tr>

                      <th class="text-center align-middle" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.names} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.gen_s} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.mail_s} </th>
                      <th class="text-center align-middle" style="font-weight:normal"> ${student.n_a} </th>
                      <th class="text-center align-middle" style="font-weight:normal"><b> ${student.status} Confirmation <a style="color: #232159; line-height: 30px; text-decoration: none" class="mail_reserve" href="homestay_admin.php?art_id=${student.id_home}">${student.mail_h} <br> (${student.week})</b></a></th>
                      <th class="text-center align-middle" style="font-weight:normal"><b> ${student.firstd} / ${student.lastd} </b></th>
                      <th class="text-center align-middle" align="left">
                        <ul>
                          <a href="../student/student_admin.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                            <i class="fa fa-eye mr-2"></i>
                              Preview
                          </a>
                        <br>
                          <a id="edit" href="../student/student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                            <i class="fa fa-id-card"></i>      
                              Edit Student
                          </a>
                        <br>
                          <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                            <i class="fa fa-pencil-alt mr-2"></i>      
                              Edit Assigment
                          </a>
                        <br>
                          <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                            <i class="fa fa-list"></i>
                              Assignation
                          </a>
  
                        </ul>
                      </th>

                    </tr>`
                } else if (student.photo_s != 'NULL' && student.status == 'Homestay Found') {
                  
                  template += `
            <tr>

              <th class="text-center align-middle" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.gen_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.mail_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.n_a} </th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.status} <a style="color: #232159; line-height: 30px; text-decoration: none" class="mail_reserve" href="homestay_admin.php?art_id=${student.id_home}">${student.mail_h} </b></a></th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.firstd} / ${student.lastd} </b><br> (${student.week})</th>
              <th class="text-center align-middle" align="left">
              <ul>
      <a href="../student/student_admin.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                          <i class="fa fa-eye mr-2"></i>
                          Preview
                      </a>
  <br>
  <a id="edit" href="../student/student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                   <i class="fa fa-id-card"></i>      
                                   Edit Student
                                     </a>
  <br>
  <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                    <i class="fa fa-pencil-alt mr-2"></i>      
                       Edit Assigment
                      </a>
  <br>
  <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                          <i class="fa fa-list"></i>
                        Assignation
                      </a>
  
    </ul>
              </th>

            </tr>`
                          } else
                          if (student.photo_s == 'NULL' && student.status == 'Homestay Found') {
                              template += `
            <tr>

              <th class="text-center align-middle" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.gen_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.mail_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.n_a} </th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.status} <a style="color: #232159; line-height: 30px; text-decoration: none" class="mail_reserve" href="homestay_admin.php?art_id=${student.id_home}">${student.mail_h}</b></a></th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.firstd} / ${student.lastd} </b><br> (${student.week})</th>
              <th class="text-center align-middle" align="left">
              <ul>
      <a href="../student/student_admin.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                          <i class="fa fa-eye mr-2"></i>
                          Preview
                      </a>
  <br>
  <a id="edit" href="../student/student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                   <i class="fa fa-id-card"></i>      
                                   Edit Student
                                     </a>
  <br>
  <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                    <i class="fa fa-pencil-alt mr-2"></i>      
                       Edit Assigment
                      </a>
  <br>
  <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                          <i class="fa fa-list"></i>
                        Assignation
                      </a>
  
    </ul>
              </th>

            </tr>`
                          } else {
                              template += `
            <tr>
              <th class="text-center align-middle" style="font-weight:normal"><img src="../${student.photo_s}" class="img_photo_s"></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.gen_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.mail_s} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${student.n_a} </th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.status} </b></th>
              <th class="text-center align-middle" style="font-weight:normal"><b> ${student.firstd} / ${student.lastd} </b><br> (${student.week})</th>
              <th class="text-center align-middle" align="left">
              <ul>
      <a href="../student/student_admin.php?art_id=${student.id_student}" id="preview" class="btn btn-outline-secondary btn-sm btn-block float-left" name="preview">
                          <i class="fa fa-eye mr-2"></i>
                          Preview
                      </a>
  <br>
  <a id="edit" href="../student/student_edit.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                                   <i class="fa fa-id-card"></i>      
                                   Edit Student
                                     </a>
  <br>
  <a id="edit2" href="edit_assigment.php?art_id=${student.id_student}" class="btn btn-primary btn-sm btn-block float-right" name="edit2">
                    <i class="fa fa-pencil-alt mr-2"></i>      
                       Edit Assigment
                      </a>
  <br>
  <a href="assignation.php?art_id=${student.id_student}" id="previews" class="btn btn-outline-secondary btn-sm btn-block float-left" name="previews">
                          <i class="fa fa-list"></i>
                        Assignation
                      </a>
  
    </ul>
              </th>

            </tr>`
                          }

                      });


                      $('#vouchers').html(template);
                  }
              });
          } else {
              $('#dynamic_content').show();
              $('#vou').hide();
          }

      });

      $(document).on('click', '.page-link', function() {
          var page = $(this).data('page_number');
          var query = $('#search_box').val();
          load_data(page, query);
      });

  });