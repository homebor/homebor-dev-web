<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM academy WHERE id_ac = '$id' ");
$row=$query->fetch_assoc();


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
     header("location: ../logout.php");   
    }
}

if ($row5['usert'] == 'Admin') {
    $way = '../master/index.php';
} elseif ($row5['usert'] == 'Cordinator') {
   $way = '../master/cordinator.php';
    echo '<style type="text/css"> a#admin{display:none;} </style>';
}
}

?>
<!DOCTYPE>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/academy_edit.css">

    <!--Mapbox Links-->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Edit Academy</title>

</head>
<body>

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER===============================================================-->
<?php 
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <h1>Academy Information</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
        <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

        <form id="form-submit" class="ts-form" autocomplete="off" action="edit_academy.php" method="post" enctype="multipart/form-data">

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Basic Information</h3>

                                <div class="text-center">
                                     <?php
                                    
                                        if($row['photo_a'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo_a'].'" id="image-profile"/>';}
                                 
                                    ?>
                                </div>

                                 <div class="row">

                                    <div class="col-sm-6">

                                    <!--Name-->
                                    <?php
                                    
                                        if($row['name_a'] == 'NULL'){
                                        	echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="name">Manager Name</label>
                                            <input type="text" class="form-control" id="name" name="name">
                                        </div>
                                    </div>';
                                        
                                    }
                                    else {
                                        echo' <div class="form-group">
                                            <label for="name">Academy Name</label>
                                            <input type="text" class="form-control" id="name" name="name" value="'.$row['name_a'].'">
                                        </div>

                                        <input type="text" class="form-control" id="id_ac" name="id_ac" value="'.$row['id_ac'].'" hidden>';}


                                 
                                    ?>
                                    </div>
                                </div>


                                <div class="row">
                                

                                    <!--Image-->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                             <label for="mail_s">Provider Photo</label>
                                            <div class="file-upload-previews"></div>
                                               <div class="panel-body">
                                                    <div class="input-group-text">
                                                    <input type="file" name="image[]" id="profile-img"  class="file-upload-input with-preview" multiple title="Click to add files" maxlength="1" accept="jpg|png|jpeg">
                                                    
                                                 </div>
                                    </div>
                                        </div>
                                    </div>

                                     

                            </section>
                            <!--end #location-->

                             <!--LOCATION
                                =====================================================================================-->
                            <section id="location" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Location</h3>

                                <div class="row">

                                    <div class="col-sm-6">

                                        <!--Address-->
                                        <?php
                                    
                                        if($row['dir_a'] == 'NULL'){
                                            echo'  <div id="dir" class="input-group">
                                            <label for="address">Address</label>
                                            <input type="text" name="dir" id="dir" class="form-control border-right-0" id="address">
                                            <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Find My Location">
                                                <a href="#" class="input-group-text bg-white border-left-0 ">
                                                    <i class="fa fa-map-marker ts-text-color-primary">
                                                    </i>
                                                </a>
                                            </div>
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div id="dir" class="input-group">
                                            <label for="address">Address</label>
                                            <input type="text" name="dir" id="dir" class="form-control border-right-0" id="address" value="'.$row['dir_a'].'">
                                            <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Find My Location">
                                                <a href="#" class="input-group-text bg-white border-left-0 ">
                                                    <i class="fa fa-map-marker ts-text-color-primary">
                                                        </i>
                                                </a>
                                            </div>
                                        </div>';}
                                 
                                    ?>

                                        <!--City-->
                                        <?php
                                    
                                        if($row['city_a'] == 'NULL'){
                                            echo'  <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city">
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city" value="'.$row['city_a'].'">
                                        </div>';}
                                 
                                    ?>

                                        <!--State-->
                                        <?php
                                    
                                        if($row['state_a'] == 'NULL'){
                                            echo'  <div class="form-group">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" id="state" name="state" >
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" id="state" name="state" value="'.$row['state_a'].'">
                                        </div>';}
                                 
                                    ?>

                                        

                                        <!--Postal Code-->
                                        <?php
                                    
                                        if($row['p_code_a'] == 'NULL'){
                                            echo'  <div class="form-group mb-0">
                                            <label for="zip">Postal Code</label>
                                            <input type="text" class="form-control" id="p_code" name="p_code">
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group mb-0">
                                            <label for="zip">Postal Code</label>
                                            <input type="text" class="form-control" id="p_code" name="p_code" value="'.$row['p_code_a'].'">
                                        </div>';}
                                 
                                    ?>
                                        

                                    </div>
                                    <!--end col-md-6-->

                                    <!--Map-->
                                         <div id='map'></div>
                                    <!--end col-md-6-->

                                </div>
                                <!--end row-->
                            </section>
                            <!--end #location-->
    <!--Map-->
                           
                                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 14
                                    });
                                    var ei = document.createElement('div');
                                    ei.id = 'marker';

                                    new mapboxgl.Marker(ei)
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
  </script>
                                <!--end col-md-6-->
                            
                            <hr>
                            
                            <section class="py-3">
                                
                                
                            </section>
                            <div class="container">
  <div class="row">
    <div class="col-sm">
      <button id="discard" class="btn btn-outline-secondary btn-lg float-left" name="discard">
                                    <i class="fa fa-times mr-2"></i>
                                    Discard Changes
                                </button>
    </div>
    <div class="col-sm">
       <button id="options" class="btn btn-primary btn-lg float-right" name="options">
                                    <i class="fa fa-cog"></i>
                                    Advance Options
    </div>
    <div class="col-sm">
      <button id="submit" class="btn btn-primary btn-lg float-right" name="update">
                                    <i class="fa fa-save ts-opacity__50 mr-2"></i>
                                    Save Changes
                                </button>
    </div>
  </div>
</div>
                            </section>



                           </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>