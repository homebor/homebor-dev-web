<?php
error_reporting(0);?>
         <!--MAIN FOOTER CONTENT
        =============================================================================================================-->
<footer id="ts-footer" style="margin-bottom:0;">
    <link rel="stylesheet" type="text/css" href="assets/css/footer.css?ver=1.0">

        <!--MAIN FOOTER CONTENT
            =========================================================================================================-->
        <section id="ts-footer-main" >
            <div class="container">
                <div class="row">

                    <div class="col-sm-6 div__image__iprom">
                        <div class="div__image_prom mt-auto mb-auto">
                            <img src="assets/iHomestay/logos/iHomestay_logo.png" alt="" class="image__iprom ">
                            <p class="text__ihomestay">Being with iHomestay gives the opportunity to Inspires new language possibilities, learn more about their world and learn different culture as well as share your own cultures with international students.</p>
                        </div>
                    </div>
                    
                    <div class="col-sm-6 image__prom">
                        <div class="div__image__footer">

                            <img src="assets/img/download.png" alt="Image App" class="img__footer">
                            <a href="#" class="btn__app app_store"></a>
                            <a href="https://play.google.com/store/apps/details?id=com.xeondevelopersvzla.homebor&hl=es_VE&gl=US" class="btn__app play_store" target="_blank"></a>

                        </div>

                        <div class="text__prom mt-auto mb-auto">
                            <h2 class="text-center">Homebor app for hosts is now available.</h2>
                            <h2 class="text-center">Download it now!!</h2>
                        </div>
                    </div>

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-main-->
        <!--SECONDARY FOOTER CONTENT
            =========================================================================================================-->
        <section id="ts-footer-secondary" data-bg-pattern="assets/img/bg-pattern-dot.png" style="background-image: url('assets/img/bg-pattern-dot.png');">
            <div class="container">
                <!--Copyright-->
                <img id="white-logo" class="logo_f" src="assets/logos/white.png">
                
                <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
                <!--Social Icons-->
                <div class="ts-footer-nav">
                    <nav class="nav">
                        <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
                            <i class="fab fa-facebook-f" id="footer"></i>
                        </a>
                        <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
                            <i class="fab fa-twitter" id="footer"></i>
                        </a>
                        <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
                            <i class="fab fa-instagram" id="footer"></i>
                        </a>
                    </nav>
                </div>
                <!--end ts-footer-nav-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-secondary-->


    </footer>
    <!--end #ts-footer-->
