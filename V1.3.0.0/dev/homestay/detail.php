<?php
error_reporting(0);
session_start();
include_once('../xeon.php');
// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail_h FROM pe_home WHERE mail_h = '$usuario'";



if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2=$query2->fetch_assoc();

$query3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();


$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM manager INNER JOIN pe_home ON pe_home.id_home = '$id' and manager.id_m = pe_home.id_m");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM agents INNER JOIN pe_home ON pe_home.id_home = '$id' and agents.id_ag = pe_home.id_ag");
$row6 = $query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM propertie_control WHERE id_home = '$id'");
$row7=$query7->fetch_assoc();

$query8 = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
$row8 = $query8->fetch_assoc();

 if ($row8['id_ag'] != $row7['id_ag'] ) {
     if ($row8['id_m'] != $row7['agency'] ) {
           header("location: index.php");
        }   }

$sql = "SELECT * FROM `pe_home` WHERE id_home = '$id'"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

if ($numberOfRows <= 0 ) {
    header("location: index.php");
}

}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/header.css">
    <link rel="stylesheet" href="../assets/css/style-detail.css">
    <link rel="stylesheet" href="../assets/css/modal-i.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/detail-rooms.css">

     <!-- Mapbox Link -->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />

    <title>Homebor - Detail Homestay</title>
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    


</head>
<style type="text/css">
    <style type="text/css">
    a#button{ color: #5D418D; border: 2px solid #5D418D;}
    a#button:hover{ background-color: #982A72; border: 2px solid #982A72; color: white; }
    button#button{ color: #5D418D; border: 2px solid #5D418D;}
    button#button:hover{ background-color: #982A72; border: 2px solid #982A72; color: white; }

    .btn-light{ color: #5D418D; border: 2px solid #5D418D;}
    .btn-light:hover{ background-color: #982A72; border: 2px solid #982A72; color: white; }
    
    @media screen and (max-width: 420px) {
        h1{margin-top: 20px;}
        div.custom-control {margin-top: 25px;}
        #white-logo{display: none;}
        #nav-footer{display: none;}
        #con-footer{display: none;}
        }

    .zoom{

    transition: width .5s, height .5s, transform .5s;
    -moz-transition: width .5s, height .5s, -moz-transform .5s;
    -webkit-transition: width .5s, height .5s, -webkit-transform .5s;
    -o-transition: width .5s, height .5s, -o-transform .5s;
}

.zoom:hover{
    transform: scale(1.05);
    -moz-transform: scale(1.05);
    -webkit-transform: scale(1.05);
    -o-transform: scale(1.05);
}

.div-photo{ margin-top: -.8em; }

@media (min-width: 320px) and (max-width: 399px) {

    .bar-i{padding-top:3.5%; padding-bottom: 3.5%; margin-left: 2%; margin-bottom: 0%; color: #982A72;}

    .bar-ii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; color: #454293;}

    .bar-iii{padding:3%; padding-left: 5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #4F177D;}

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:3%; padding-left: 7%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-iv:hover{background-color: #4F177D; color: white;}
}

@media (min-width: 400px) and (max-width: 499px){

    .bar-i{padding-top:3.5%; padding-bottom: 3.5%; margin-left: 2%; margin-bottom: 0%; color: #982A72;}

    .bar-i:hover{color: white;}

    .bar-ii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-ii:hover{background-color: #982A72; color: white;}

    .bar-iii{padding:3%; padding-left: 5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #4F177D;}

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:3%; padding-left: 7%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-iv:hover{background-color: #4F177D; color: white;}

}

@media (min-width: 500px) and (max-width: 599px){

    .bar-i{padding-top:3.5%; padding-bottom: 3.5%; margin-left: 2%; margin-bottom: 0%; color: #982A72;}

    .bar-i:hover{color: white;}

    .bar-ii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-ii:hover{background-color: #982A72; color: white;}

    .bar-iii{padding:3%; padding-left: 5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #4F177D;}

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:3%; padding-left: 7%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-iv:hover{background-color: #4F177D; color: white;}

}

@media (min-width: 600px) and (max-width: 767px){

    .bar-i{padding-top:3.5%; padding-bottom: 3.5%; margin-left: 2%; margin-bottom: 0%; color: #982A72;}

    .bar-i:hover{color: white;}

    .bar-ii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-ii:hover{background-color: #982A72; color: white;}

    .bar-iii{padding:3%; padding-left: 5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #4F177D;}

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:3%; padding-left: 7%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293;}

    .bar-iv:hover{background-color: #4F177D; color: white;}

}

@media (min-width: 768px) and (max-width: 899px){

    .bar-i{color: #982A72;}

    .bar-ii{padding:5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293; width: auto;}

    .bar-ii:hover{background-color: #982A72; color: white;}

    .bar-iii{padding:5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #4F177D; width: auto;}

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:5%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 11px; color: #454293; width: auto;}

    .bar-iv:hover{background-color: #4F177D; color: white; }
}

@media (min-width: 900px){

    .bar-i{color: #982A72;}

    .bar-ii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 13px; color: #454293; width: auto;}

    .bar-ii:hover{background-color: #982A72; color: white;}

    .bar-iii{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 13px; color: #4F177D; width: auto;}        

    .bar-iii:hover{background-color: #5D418D; color: white;}

    .bar-iv{padding:3%; margin-left: -1%; margin-right: -1%; margin-bottom: 0%; font-size: 13px; color: #454293; width: auto;}

    .bar-iv:hover{background-color: #4F177D; color: white; }
}

@media (min-width: 992px){
    .div-photo{ margin-top: -.8em; }
}

@media (min-width: 992px) and (max-width: 1920px){

    .bar-ii{padding: 5%; width: auto;}

    .bar-iii{padding: 5%; width: auto;}

    .bar-iv{padding: 5%; width: auto;}
}


@media (min-width: 1420px){

    .bar-ii{height: auto; width: auto;}

    .bar-iii{height: auto; width: auto;}

    .bar-iv{height: auto; width: auto;}
}

@media (min-width: 1420px) and (max-width: 1600px){

    .bar-i{font-size: 15px}

    .bar-ii{font-size: 13px}

    .bar-iii{font-size: 13px}

    .bar-iv{font-size: 13px}
}

@media (min-width: 1600px) {
    .bar-i{font-size: 18px}

    .bar-ii{font-size: 15px}

    .bar-iii{font-size: 15px}

    .bar-iv{font-size: 15px}
}

#house { 
      background-image: url('../student/assets/images/house1.png');
      background-size: cover;
      width: 27px;
      height: 49px;
      cursor: pointer;}

</style>


<body>


 <div class="ts-page-wrapper ts-homepage" id="page-top">
     <!--HEADER ===============================================================-->
<?php
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->


    <main id="ts-main">


        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">

                <div class="d-block d-sm-flex justify-content-between">

                    <!--Title-->
                    <div class="ts-title mb-0">
                        <br>
                        <br>
                        <br>
                        <br>
                        <h1><?php echo $row['h_name'] ?></h1>
                        <h5 class="ts-opacity__90">
                            <i class="fa fa-map-marker text-primary"></i>
                            <?php echo $row['dir'] ?>
                        </h5>
                    </div>

                </div>

            </div>
        </section>

        <!--GALLERY CAROUSEL
            =========================================================================================================-->
        <section id="gallery-carousel">

            <div class="owl-carousel ts-gallery-carousel ts-gallery-carousel__multi" data-owl-dots="1" data-owl-items="3" data-owl-center="1" data-owl-loop="1">

                
                    <?php
                                    
                                     if($row['phome'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row[phome].'">
                        <a href="../'.$row[phome].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                    
                

                
                    <?php
                                    
                                     if($row4['pliving'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row4[pliving].'">
                        <a href="../'.$row4[pliving].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

                
                    <?php
                                    
                                     if($row4['parea1'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row4[parea1].'">
                        <a href="../'.$row4[parea1].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

                
                   <?php
                                    
                                     if($row4['parea2'] == 'NULL'){
                                     echo'
                                     <!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row4[proom1].'">
                        <a href="../'.$row4[proom1].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';   
                                    }
                                    else {
                                        echo'
                                        <!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row4[parea2].'">
                        <a href="../'.$row4[parea2].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div></div>';}
                                    
                                    ?>
                

                
                    <?php
                                    
                                     if($row4['pbath1'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="../'.$row4[pbath1].'">
                        <a href="../'.$row4[pbath1].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

            </div>

        </section>

        <!--CONTENT
            =========================================================================================================-->
        <section id="content">
            <div class="container">
                <div class="row flex-wrap-reverse">

                    <!--LEFT SIDE
                        =============================================================================================-->
                    <div class="col-md-5 col-lg-4">

                        <!--DETAILS
                            =========================================================================================-->
                        <section>
                            <h3>Details</h3>
                            <div class="ts-box">

                                <dl class="ts-description-list__line mb-0">

                                    <dt>ID:</dt>
                                    <dd>#<?php echo $row['id_home'] ?></dd>

                                    <dt>Property:</dt>
                                    <dd><?php echo $row['name_h'] ?>  <?php echo $row['l_name_h'] ?></dd>

                                    <dt>Gender:</dt>
                                    <dd><?php echo $row['gender'] ?></dd>

                                    <dt>Category:</dt>
                                    <dd>Homestay</dd>

                                    <dt>Status:</dt>
                                    <dd><?php echo $row['status'] ?></dd>

                                    <dt>Smoke Politics:</dt>
                                    <dd><?php echo $row['smoke'] ?></dd>

                                    <dt>Bedrooms:</dt>
                                    <dd>2</dd>

                                    <dt>Stars:</dt>
                                    <dd>1</dd>

                                    <dt>Certified:</dt>
                                    <dd><?php echo $row['certified'] ?></dd>

                                </dl>

                            </div>
                        </section>

                         <?php
                            if ($row3['f_name1'] == 'NULL' AND $row3['f_name2'] == 'NULL' AND $row3['f_name3'] == 'NULL' AND $row3['f_name4'] == 'NULL' AND $row3['f_name5'] == 'NULL' AND $row3['f_name6'] == 'NULL' AND $row3['f_name7'] == 'NULL' AND $row3['f_name8'] == 'NULL') {
                            } else {
                                echo '<section>
                                <h3>Family Information</h3>
                                <div class="ts-box">
                                <dl class="ts-description-list__line mb-0">';

                                //Family Members
                                if ($row3['f_name1'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name1].' '.$row3[f_lname1].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db1].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender1].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re1].'</dd>';
                                }

                                if ($row3['f_name2'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name2].' '.$row3[f_lname2].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db2].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender2].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re2].'</dd>';
                                }
                                if ($row3['f_name3'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name3].' '.$row3[f_lname3].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db3].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender3].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re3].'</dd>';
                                }
                                if ($row3['f_name4'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name4].' '.$row3[f_lname4].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db4].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender4].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re4].'</dd>';
                                }

                                if ($row3['f_name5'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name5].' '.$row3[f_lname5].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db5].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender5].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re5].'</dd>';
                                }
                                if ($row3['f_name6'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name6].' '.$row3[f_lname6].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db6].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender6].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re6].'</dd>';
                                }
                                if ($row3['f_name7'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name7].' '.$row3[f_lname7].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db7].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender7].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re7].'</dd>';
                                }
                                if ($row3['f_name8'] == 'NULL') {
                                    
                                } else{ 
                                        echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3[f_name8].' '.$row3[f_lname8].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3[db8].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3[gender8].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3[re8].'</dd>';
                                }
                                echo '

                            </div>
                        </section>';
                            }

                        ?>

                        <!--CONTACT THE AGENT
                            =========================================================================================-->

                             <?php
                            if ($row[id_m] == '0') { echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../assets/logos/7.png"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">Homebor</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            sebi@homebor.com
                                        </p>
                                    </figure>
                                </div>
                                <form id="form-agent" class="ts-form">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7[name].' '.$row7[l_name].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7[a_mail].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row[id_home].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>


                            </div>
                        </section>';
                                # code...
                            } else { 
                                if ($row[id_ag] == '0') {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../'.$row5[photo].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row5[a_name].'</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            '.$row5[mail].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                                            '.$row5[num].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                                            Manager
                                        </p>
                                    </figure>
                                </div>

                                <form id="form-agent" class="ts-form">

                                   <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7[name].' '.$row7[l_name].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7[a_mail].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row[id_home].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </section>';
                                } else {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../'.$row6[photo].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row6[agency].'</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-user ts-opacity__50 mr-2"></i>
                                            '.$row6[name].' '.$row6[l_name].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            '.$row6[a_mail].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                                            '.$row6[num].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                                            Agent
                                        </p>
                                    </figure>
                                </div>

                                <form id="form-agent" class="ts-form">

                                   <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7[name].' '.$row7[l_name].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7[a_mail].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row[id_home].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </section>';

                                }
                                
                            }

                            ?>



                          <!--LOCATION
                        =============================================================================================-->
                        <section id="location">
                            <h3>Location</h3>

                            <div class="ts-box">

                                <dl class="ts-description-list__line mb-0">

                                    <dt><i class="fa fa-home  ts-opacity__30 mr-2"></i>Address:</dt>
                                    <dd class="border-bottom pb-2"><?php echo $row['dir'] ?></dd>

                                    <dt><i class="fa fa-building ts-opacity__30 mr-2"></i>City:</dt>
                                    <dd class="border-bottom pb-2"><?php echo $row['city'] ?></dd>

                                    <dt><i class="fa fa-map-marker ts-opacity__30 mr-2"></i>State:</dt>
                                    <dd class="border-bottom pb-2"><a href="#"><?php echo $row['state'] ?></a></dd>

                                    <dt><i class="fa fa-globe ts-opacity__30 mr-2"></i>Email:</dt>
                                    <dd><a href="#"><?php echo $row['mail_h'] ?></a></dd>

                                </dl>

                            </div>

                        </section>

                        <!--ACTIONS
                        =============================================================================================-->
                        <section id="actions">

                            <div class="d-flex justify-content-between">

                                
                                   
                                
                                    <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top" title="Print">
                                            <i class="fa fa-print"></i>
                                    </a>
                                

                                

                                

                            </div>

                        </section>


                    </div>
                    <!--end col-md-4-->

                    <!--RIGHT SIDE
                        =============================================================================================-->
                    <div class="col-md-7 col-lg-8">

                        <!--QUICK INFO
                            =========================================================================================-->
                        <section id="quick-info">
                            <h3>Quick Info</h3>

                            <!--Quick Info-->
                            <div class="ts-quick-info ts-box">

                                <!--Row-->
                                <div class="row no-gutters">

                                    <!--Bathrooms-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/icon-quick-info-shower.png">
                                            <h6>Bedrooms</h6>
                                            <figure><?php echo $row['room'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Bedrooms-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/icon-quick-info-bed.png">
                                            <h6>Gender Preference</h6>
                                            <figure><?php echo $row['g_pre'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Area-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/icon-quick-info-area.png">
                                            <h6>Pets</h6>
                                            <figure><?php echo $row['pet'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Garages-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/icon-quick-info-garages.png">
                                            <h6>Age Preference</h6>
                                            <figure><?php echo $row['ag_pre'] ?></figure>
                                        </div>
                                    </div>

                                </div>
                                <!--end row-->

                            </div>
                            <!--end ts-quick-info-->

                        </section>

                        <!--DESCRIPTION
                            =========================================================================================-->
                        <section id="description">

                            <h3>Description</h3>

                            <p>
                               <?php echo $row['des'] ?>
                            </p>

                        </section>

                        <!--FEATURES
                            =========================================================================================-->
                        <section id="features">

                            <h3>Features</h3>

                            <ul class="list-unstyled ts-list-icons ts-column-count-4 ts-column-count-sm-2 ts-column-count-md-2">
                                <li>
                                    <i class="fa fa-university"></i>
                                    <?php echo $row['a_pre'] ?>
                                </li>
                                <li>
                                    <i class="fa fa-female"></i><i class="fa fa-male"></i>
                                    <?php echo $row['g_pre'] ?>
                                </li>
                                <li>
                                    <i class="fa fa-child"></i><i class="fa fa-male"></i>
                                    <?php echo $row['ag_pre'] ?>
                                </li>
                            </ul>

                        </section>

                         <!--MAP
                            =========================================================================================-->
                        <section id="map-location">

                            <!--Map-->

                            <!--Map-->
                                         <div id='map' style="height: 200px;"></div>
                                    <!--end col-md-6-->
                           
                                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 14
                                    });

                                    var ea = document.createElement('div');
                                    ea.id = 'house';

                                    new mapboxgl.Marker(ea)
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
  </script>

                        </section>


                       <!--FLOOR PLANS
                            =========================================================================================-->
                        <section id="floor-plans">

                            <h3>Bedrooms Info</h3>


                            <?php 

                                $usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
                                $u=$usu->fetch_assoc();

                            ?>

                            <div class="c-log">

                            <?php
                                if($row4['proom1'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login100">

                                        <h3 class="title-room"> Room 1</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom1'] == 'NULL') {
                                        ?>
                                
                                                <img src="../assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom1]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type1] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed1] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date1] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food1] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row4['proom2'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login200">

                                        <h3 class="title-room"> Room 2</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom2'] == 'NULL') {
                                        ?>
                                
                                                <img src="../assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom2]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type2] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed2] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date2] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food2] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type3'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login300">

                                        <h3 class="title-room"> Room 3</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom3'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom3]; ?>" alt="Room3" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type3] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed3] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date3] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food3] ?>">
                                                </div>                           

                                            </div>


                                        </div>

                                        
                                    </div>
                            <?php } ?>



                            <?php
                                if($row2['type4'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login400">

                                        <h3 class="title-room"> Room 4</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom4'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom4]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type4] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed4] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date4] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food4] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type5'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login500">

                                        <h3 class="title-room"> Room 5</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom5'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="<?php echo $row4[proom5]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type5] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed5] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date5] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food5] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                        
                                    </div>
                            <?php } ?>


                            <?php
                                if($row2['type6'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login600">

                                        <h3 class="title-room"> Room 6</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom6'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom6]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type6] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed6] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date6] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food6] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type7'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login700">

                                        <h3 class="title-room"> Room 7</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom7'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="../<?php echo $row4[proom7]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type7] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed7] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date7] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food7] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type8'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login800">

                                        <h3 class="title-room"> Room 8</h3>



                                        <div class='div-img-r' align='center'>

                                        <?php 

                                            if ($row4['proom8'] == 'NULL') {
                                        ?>
                                
                                                <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                        <?php
                                            }else{

                                        ?>

                                        <img src="<?php echo $row4[proom8]; ?>" alt="Room1" class="img-room">

                                        <?php } ?>

                                        </div>

                                        <div class="items-ro">

                                            <div class="wrap-input100 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[type8] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input200 validate-input">
                                                
                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[bed8] ?>">
                                                </div>

                                            </div>


                                            <div class="wrap-input300 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[date8] ?>">
                                                </div>
                                                
                                            </div>


                                            <div class="wrap-input400 validate-input">

                                                <div class="icon-type">
                                                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                                </div>

                                                <div class="wrap-input600">
                                                    <input class="input200" type="text" readonly value="<?php echo $row2[food8] ?>">
                                                </div>                           

                                            </div>

                                        </div>

                                    </div>            

                            <?php } ?>
                            


                        </section>


                        <!--AMENITIES
                            =========================================================================================-->
                        <section id="amenities">

                            <h3>Special Diet</h3>

                            <div class="container" style="background: white; padding: 1em 1.5em; padding-top: 2em; box-shadow: 2px 2px 5px #D6D6D6; border-radius: 4px; ">

                            <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                                <?php
                                            if($row['vegetarians'] == "yes"){
                                            echo "<li>Vegetarians</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['halal'] == "yes"){
                                            echo "<li>Halal</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['kosher'] == "yes"){
                                            echo "<li>Kosher (Jews)</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['lactose'] == "yes"){
                                            echo "<li>Lacotose Intolerant</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['gluten'] == "yes"){
                                            echo "<li>Gluten Free Diet</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['pork'] == "no"){
                                            
                                            }
                                        else {
                                            echo "<li>No Pork</li>";
                                        }
                                    ?>
                                     <?php
                                            if($row['none'] == "yes"){
                                            echo "<li>None</li>";
                                            }
                                        else {
                                            
                                        }
                                    ?>
                            </ul>

                        </div>

                        </section><br>

                        <section id="amenities">

                            <h3>Pets Information</h3>

                            <div class="container" style="background: white; padding: 1em 1.5em; padding-top: 2em; box-shadow: 2px 2px 5px #D6D6D6; border-radius: 4px; ">

                            <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                                <?php
                                            if($row['pet_num'] == "0"){
                                            
                                            }
                                        else {
                                            echo "<p><i class='fa fa-paw'></i> ".$row[pet_num]."</p>";
                                            if ($row['type_pet'] == 'NULL') {
                                                
                                            } else { echo "<p>".$row[type_pet]."</p>";}
                                        }
                                    ?>
                                    <?php
                                            if($row['dog'] == "no"){
                                            
                                            }
                                        else {
                                            echo "<li>Dog</li>";
                                        }
                                    ?>
                                    <?php
                                            if($row['cat'] == "no"){
                                            }
                                        else {
                                            echo "<li>Cat</li>";
                                        }
                                    ?>
                                    <?php
                                            if($row['other'] == "no"){
                                            }
                                        else {
                                            echo "<li>Others</li>";
                                        }
                                    ?>
                            </ul>

                        </div>

                        </section>

                    </div>
                    <!--end col-md-8-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>


    </main>
    <!--end #ts-main-->

    <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>


</div>
<!--end page-->

<script>
                                const modalService = () => {
  const d = document;
  const body = d.querySelector('body');
  const buttons = d.querySelectorAll('[data-modal-trigger]');
  
  // attach click event to all modal triggers
  for(let button of buttons) {
    triggerEvent(button);
  }
  
  function triggerEvent(button) {
    button.addEventListener('click', () => {
      const trigger = button.getAttribute('data-modal-trigger');
      const modal = d.querySelector(`[data-modal=${trigger}]`);
      const modalBody = modal.querySelector('.modal-body');
      const closeBtn = modal.querySelector('.cancel');
      
      closeBtn.addEventListener('click', () => modal.classList.remove('is-open'))
      modal.addEventListener('click', () => modal.classList.remove('is-open'))
      
      modalBody.addEventListener('click', (e) => e.stopPropagation());

      modal.classList.toggle('is-open');
      
      // Close modal when hitting escape
      body.addEventListener('keydown', (e) => {
        if(e.keyCode === 27) {
          modal.classList.remove('is-open')
        }
      });
    });
  }
}

modalService();
</script>

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/jquery.magnific-popup.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>
