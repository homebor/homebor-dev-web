<?php
require '../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$queryHome = $link->query("SELECT id_home, h_name FROM pe_home WHERE mail_h = '$usuario' ");
$row_homestay = $queryHome->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!--Rellenado con JS-->
  <title>Rooms</title>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css?ver=1.0.14">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css?ver=1.0.14">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.14">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.14">
  <link rel="stylesheet" href="assets/css/notification.css?ver=1.0.14">
  <link rel="stylesheet" href="assets/css/header-index.css?ver=1.0.14">
  <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.14">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.14">
  <link rel="stylesheet" href="assets/css/carousel.css?ver=1.0.14">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.14">


  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.14"></script>

</head>

<!-- // TODO HEADER -->
<header id="ts-header" class="fixed-top bg-white"> <?php include 'header.php' ?> </header>

<body style="background: #eee;">
  <main>
    <!-- // TODO CONTAINER -->
    <div class="container-fluid mt-4 pt-5 rooms-container">
      <!-- // TODO INPUTS DE UTILIDAD -->
      <input type="hidden" value="<?php echo $row_homestay['id_home'] ?>" id="homestay-id">
      <input type="hidden" value="<?php echo $row_homestay['h_name'] ?>" id="house-name">
      <input type="hidden" data-view="edit-rooms" value="homestay" id="current-user">

      <h1 class="d-flex justify-content-between mx-auto mt-5 px-4 py-2">Edit Your Rooms
        <button id="add-new-room" class="btn add-new-room">Add new room</button>
      </h1>

      <!-- // TODO ALL ROOMS CONTAINER -->
      <div id="all-rooms" class="container rooms-list-homestay mt-5">

        <template id="reserve-template">

          <!-- // TODO ROOM CONTAINER -->
          <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded room-card-container-homestay">

            <div class="d-flex py-4">

              <!-- // TODO ROOM CARD -->
              <div class="mx-3 pb-4 bg-white room-card">

                <!-- // TODO ROOM HEADER -->
                <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title">
                  <h3 class="p-0 m-0 text-white">Room 1</h3>
                  <p class="m-0 p-0 text-white">CAD$ 14</p>
                </div>

                <!-- // TODO ROOM PHOTO -->
                <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
                  <button id="delete-photo-btn" title="Delete image">
                    <svg fill="#fff" style="opacity: 0.7;" viewBox="0 0 24 24" width="26px" height="26px">
                      <path d="M 10 2 L 9 3 L 4 3 L 4 5 L 5 5 L 5 20 C 5 20.522222 5.1913289 21.05461 5.5683594 21.431641 C 5.9453899 21.808671 6.4777778 22 7 22 L 17 22 C 17.522222 22 18.05461 21.808671 18.431641 21.431641 C 18.808671 21.05461 19 20.522222 19 20 L 19 5 L 20 5 L 20 3 L 15 3 L 14 2 L 10 2 z M 7 5 L 17 5 L 17 20 L 7 20 L 7 5 z M 9 7 L 9 18 L 11 18 L 11 7 L 9 7 z M 13 7 L 13 18 L 15 18 L 15 7 L 13 7 z" />
                    </svg>
                  </button>
                  <button id="edit-photo-btn" title="Edit image">
                    <img src="../assets/icon/edit.png" rel="noopener" alt="edit">
                  </button>

                  <img src="" id="single-image-room" class="d-none">

                  <!-- // TODO CAROUSEL FOTOS -->
                </div>

                <!-- // TODO INFO ROOM -->
                <div class="d-flex justify-content-center mt-5">

                  <!-- // TODO TYPE ROOM -->
                  <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
                    <div class="feature type-room">
                      <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
                      <select class="custom-select" name="type-room" id="type-room" required>
                        <option value="NULL" hidden selected>Type room</option>
                        <option value="Single">Single</option>
                        <option value="Executive">Executive</option>
                        <option value="Share">Share</option>
                      </select>
                    </div>
                  </div>

                  <!-- // TODO ROOM FOOD SERVICE -->
                  <div class="col-auto p-0">
                    <div class="feature food">
                      <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
                      <select class="custom-select" name="food-service" id="food-service" required>
                        <option value="NULL">Food</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>

                </div>

                <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
                  <label>Weekly Price</label>
                  <div class="d-flex align-items-center justify-content-center">
                    <span class="rounded">CAD$</span>
                    <input type="text" class="rounded" placeholder="Insert price" maxlength="10" id="homestay-price">
                  </div>
                </div>

                <div class="d-flex align-items-center justify-content-center">
                  <button id="disable-room" class="w-auto mt-3 py-1 mx-auto btn disable-room">Disable room</button>
                  <button id="enable-room" class="w-auto mt-3 py-1 mx-auto btn enable-room d-none">Enable room</button>
                </div>

              </div>

              <!-- // TODO BEDS CONTAINER -->
              <div id="beds-container" class="d-flex mx-lg-3 flex-column align-items-center room-beds-homestay edit">
                <!-- // TODO BEDS -->
                <h3 class="font-weight-light text-muted">Beds</h3>

                <!-- // TODO IMAGE ONLY ONE BED -->
                <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
              </div>
            </div>

          </div>

        </template>

      </div>

      <template id="bed-template">
        <!-- // TODO BED -->
        <div id="bed" class="my-auto mx-4 mx-md-3 rounded d-flex justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center" style="width: 130px !important;">
              <img src="icon/cama 64.png" width="25px" height="25px" alt="" class="mx-2">
              <select class="custom-select" name="bed-type" id="bed-type" disabled required>
                <option value="NULL" selected>Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

      </template>


    </div>

    <div class="d-flex align-items-center justify-content-center">
      <button id="save-changes" class="btn save-changes">Save Changes</button>
    </div>


    <!-- // TODO MODAL DYNAMIC -->
    <div id="modal-dynamic" class="modal-dynamic">

      <section class="modal-content">
        <span class="close">X</span>
        <h3>Title</h3>


        <div class="modal-empty">
          <img src="../assets/img/homestay_home.png" alt="empty" rel="noopener">
        </div>


        <table class="modal-table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Bed</th>
              <th>Start</th>
              <th>End</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </section>

    </div>

  </main>

  <script src="assets/js/main.js?ver=1.0.14"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.14"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.14"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.14"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.14"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.14"></script>
  <script src="assets/js/ajax_noti.js?ver=1.0.14"></script>

  <!-- // ? DETAIL (IMPORTANT) -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.14"></script>
  <script src="assets/js/edit_rooms.js?ver=1.0.14" type="module"></script>



  <?php include 'footer.php' ?>

</body>

</html>