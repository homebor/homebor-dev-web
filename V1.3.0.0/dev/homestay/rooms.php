<?php
require '../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$queryHome = $link->query("SELECT id_home FROM pe_home WHERE mail_h = '$usuario' ");
$row_homestay = $queryHome->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Rooms</title>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css?ver=1.0.15">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.15">
  <link rel="stylesheet" href="assets/css/style.css?ver=1.0.15">
  <link rel="stylesheet" href="assets/css/notification.css?ver=1.0.15">
  <link rel="stylesheet" href="assets/css/header-index.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.15">
  <link rel="stylesheet" href="assets/css/carousel.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.15">

  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.15"></script>

</head>

<!-- // TODO HEADER -->
<header id="ts-header" class="fixed-top bg-white"> <?php include 'header.php' ?> </header>

<body style="background-color: #eee;">

  <main>
    <!-- // TODO CONTAINER -->
    <div class="container-fluid mt-4 pt-5 rooms-container">
      <!-- // TODO INPUTS DE UTILIDAD -->
      <input type="hidden" value="<?php echo $row_homestay['id_home'] ?>" id="homestay-id">
      <input type="hidden" data-view="rooms" value="homestay" id="current-user">

      <h1 class="mx-auto mt-5 py-4">Your Rooms</h1>

      <!-- // TODO ALL ROOMS CONTAINER -->
      <div id="all-rooms" class="container rooms-list-homestay mt-5">

        <template id="reserve-template">

          <!-- // TODO ROOM CONTAINER -->
          <div id="reserve" class="d-flex py-4 px-2 rounded room-card-container-homestay">

            <!-- // TODO ROOM CARD -->
            <div class="mx-3 pb-5 bg-white room-card" style="width: 280px !important;">

              <!-- // TODO ROOM HEADER -->
              <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title">
                <h3 class="p-0 m-0 text-white">Room 1</h3>
                <p class="m-0 p-0 text-white">CAD$ 14</p>
              </div>

              <!-- // TODO ROOM PHOTO -->
              <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo">

                <img src="" id="single-image-room" class="d-none">

                <!-- // TODO CAROUSEL FOTOS -->
              </div>

              <!-- // TODO INFO ROOM -->
              <div class="d-flex mb-4">

                <!-- // TODO TYPE ROOM -->
                <div class="col-6 p-0">
                  <div class="feature">
                    <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
                    <span id="type-room" class="px-2">Type</span>
                  </div>
                </div>

                <!-- // TODO ROOM FOOD SERVICE -->
                <div class="col-6 p-0">
                  <div class="feature">
                    <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
                    <span id="food-service" class="px-2">Food Service</span>
                  </div>
                </div>

              </div>

              <!-- // TODO ROOM RESERVE BUTTON -->
              <div class="m-0 d-flex align-items-center justify-content-center see-all-reserves">
                <button type="button" id="see-all-reserves" class="btn px-3 text-light">See all reserves</button>
              </div>

              <span class="status-detail"></span>
            </div>

            <!-- // TODO BEDS CONTAINER -->
            <div id="beds-container" class="d-flex mx-lg-2 flex-column align-items-center room-beds-homestay">
              <!-- // TODO BEDS -->
              <h3 class="font-weight-light text-muted mt-4">Beds</h3>

              <!-- // TODO IMAGE ONLY ONE BED -->
              <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
            </div>


          </div>


        </template>

      </div>

      <template id="bed-template">
        <!-- // TODO BED -->
        <div id="bed" class="my-auto mx-4 mx-md-3 rounded d-flex justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="icon/cama 64.png" width="25px" height="25px" alt="">
              <span id="bed-type" class="py-4 px-2 text-dark">Type</span>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <div class="d-none align-items-center justify-content-center student-info py-3">
            <img id="student-img" src="../image" rel="noopener" alt="profile">
          </div>
          <a href="#" class="student-details">
            <span id="student-name" class="d-flex align-items-center justify-content-center">Nombre</span>
            <small id="student-date" class="d-flex align-items-center justify-content-center" title="Arrive - Leaving">First day - Last day</small>
          </a>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

      </template>


    </div>

    <!-- // TODO NOTICE -->
    <div class="d-flex align-items-center justify-content-center">
      <b class="small font-weight-bold text-black-50">
        Notice: The disabled rooms will not be shown in this section, go to Edit Rooms and modify the status of the
        room.
      </b>
    </div>


    <!-- // TODO MODAL DYNAMIC -->
    <div id="modal-dynamic" class="modal-dynamic">

      <section class="modal-content">
        <span class="close">X</span>
        <h3>Title</h3>


        <div class="modal-empty">
          <img src="../assets/img/homestay_home.png" alt="empty" rel="noopener">
        </div>


        <table class="modal-table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Bed</th>
              <th>Start</th>
              <th>End</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </section>

    </div>


  </main>

  <script src="assets/js/main.js?ver=1.0.15"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.15"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.15"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.15"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.15"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.15"></script>
  <script src="assets/js/ajax_noti.js?ver=1.0.15"></script>

  <!-- // ? DETAIL (IMPORTANT) -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.15"></script>
  <script src="assets/js/rooms.js?ver=1.0.15" type="module"></script>


  <?php include 'footer.php' ?>

</body>

</html>