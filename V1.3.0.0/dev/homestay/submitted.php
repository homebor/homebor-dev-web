<?php
include '../xeon.php';
session_start();
error_reporting(0);


// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion

$query="SELECT * FROM pe_home WHERE mail_h = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and room.id_home = pe_home.id_home";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query3="SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and mem_f.id_home = pe_home.id_home";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$query4="SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and photo_home.id_home = pe_home.id_home";
$resultado4=$link->query($query4);

$row4=$resultado4->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

 if ($row5['usert'] != 'homestay') {
         header("location: ../logout.php");   
    }
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">

	<!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Submitted</title>

</head>
<style type="text/css">
    a.btn-link{color:  #982A72;}
    a.btn-link:hover{color:  #982A72;}
    a#Calendar{width: 160px; font-size: 16px;}
    a#contact{width: 130px; font-size: 14px;}
     @media screen and (max-width: 420px) { 
        div.offset-2{
            margin-left: -3px;
        }
        #nav-footer{display: none;}
        #con-footer{display: none;}
        #white-logo{display: none;}
</style>

<body style="background-color: #F3F8FC;">

<!-- WRAPPER - FONDO
    =================================================================================================================-->

    <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
    <header id="ts-header" class="fixed-top" style="background-color: white;">

        <?php include 'header.php' ?>

    </header>













    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main" style="background-color: #F3F8FC;">

        <!--SUBMITTED ALERT
            =========================================================================================================-->
        <section id="submitted" class="ts-block">
            <div class="container">
                <div class="row">

                    <div class="offset-2 col-md-8 text-center">

                        <i class="far fa-check-circle ts-text-color-primary display-4 mb-2"></i>
                        <h1 class="ts-text-color-primary">Thank You <?php echo $row['name_h']?>!</h1>
                        <h4 class="ts-text-color-light">Your Property <?php echo $row['h_name']?> Was Submitted Successfully</h4>
                        <a href="voucher/welcome.php">PDF</a>
                        <a id="Calendar" href="index.php" class="btn btn-secondary">Back to Calendar</a>
                        <style type="text/css">
                            i.fa-check-circle {
                                color: purple;
                            }
                            h1.ts-text-color-primary {
                                color: purple;
                            }
                            a.btn-secondary {
                                color: white;
                                background-color: #982A72; border: 2px solid #982A72;
                            }
                        </style>

                    </div>

                </div>
            </div>
        </section>

    </main>
    <!--end #ts-main-->











    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->

     <footer id="ts-footer">

        <?php include 'footer.php' ?>
    </footer>
    <!--end #ts-footer-->
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>
