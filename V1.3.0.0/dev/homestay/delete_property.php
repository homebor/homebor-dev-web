<?php
require '../xeon.php';
// include 'ajax_noti.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$resultado = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario'");
$row = $resultado->fetch_assoc();

$resultado3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and mem_f.id_home = pe_home.id_home");
$row3 = $resultado3->fetch_assoc();

$resultado4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and photo_home.id_home = pe_home.id_home");
$row4 = $resultado4->fetch_assoc();

$resultado7 = $link->query("SELECT id_ac, name_a FROM academy INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and academy.id_ac = pe_home.a_pre");
$row7 = $resultado7->fetch_assoc();

?>


<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />

  <!--CSS -->
  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="assets/css/style.css">

  <link rel="stylesheet" href="assets/css/notification.css">
  <link rel="stylesheet" href="assets/css/delete_property.css">
  <link rel="stylesheet" href="assets/css/header-index.css">

  <!--Mapbox Links-->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Preview</title>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>

<?php include 'header.php' ?>

<body style="background: #eee;">

  <main id="ts-main">


    <section id="page-title">
      <div class="container">
        <div class="row">
          <div class="offset-lg-1 col-lg-10">
            <div class="ts-title">
              <br>
              <br>
              <h1>Home Preview Information</h1>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="preview">
      <div class="container">
        <div class="row">

          <div class="offset-lg-1 col-lg-10">

            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1"><i class="fa fa-cog"></i> Advance Options</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">

                    <!-- Trigger the modal with a button -->
                    <h3>Disable Account</h3>
                    <p>The homestay will be disable immediately.<br />

                      This action will <b>disable the</b> <?php echo "" . $row['mail_h'] . ""; ?> <b>account</b>,
                      including its files, information and events.<br />

                      <b>If you wish disable this user account press the button?</b>
                    </p>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"
                      style="color: white; background-color: #982A72; border: 2px solid #982A72;"><i
                        class="fa fa-user-times"></i> Disable Homestay</button>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" style="color: red;">Delete Account</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                            <div class="modal-body" style="background-color: #DFBABA;">
                              <p>
                                <i class="fa fa-asterisk"></i> You are about to disable this account<br />
                                Once a propertie is disable the user will not login in homebor until his user has been
                                reactivated again. its files, all information and events will be not removing of the
                                data.
                              </p>
                            </div>
                          </div>
                          <div class="modal-body">
                            <p><b>All Fields Required</b></p>
                            <p>Please type the following to confirm:<br>
                              <?php echo "<b>" . $row['id_home'] . "</b>"; ?></p>
                            <form id="form-submit" class="ts-form" autocomplete="off" action="delete_homestay.php"
                              method="post" enctype="multipart/form-data">
                              <div class="row">

                                <!--House Name-->
                                <div class="col-sm-8">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="del_id" name="del_id" required>
                                    <input type="text" class="form-control" id="user" name="user" required
                                      value="<?php echo "$usuario"; ?>" hidden>
                                    <input type="text" class="form-control" id="id_h" name="id_h" required
                                      value="<?php echo "$row[id_home]"; ?>" hidden>
                                    <input type="text" class="form-control" id="name" name="name" required
                                      value="<?php echo "$row[mail_h]"; ?>" hidden>
                                    <input type="text" class="form-control" id="id_m" name="id_m" required
                                      value="<?php echo "$row[id_m]"; ?>" hidden>
                                    <label for="des">Reason</label>
                                    <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                  </div>
                                </div>
                              </div>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"
                              style="color: white; background-color: #232159; border: 2px solid #232159;">Close</button>
                            <button type="submit" id="submit" name="register" class="btn btn-default"
                              style="color: white; background-color: #982A72; border: 2px solid #982A72;">
                              <i class="fa fa-user-times"></i> Disable</button>
                            </form>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <br>
                </div>
              </div>
            </div>


            <!--BASIC INFORMATION
                            =====================================================================================-->
            <section id="basic-information" class="mb-5">

              <!--Title-->
              <h3 class="text-muted border-bottom">Basic Information</h3>

              <!--Row-->
              <div class="row">

                <!--Title-->
                <div class="col-sm-8">

                  <?php
                  if ($row['h_name'] == "NULL") {
                  } else {
                    echo "<label>House Name</label>
            <p>" . $row['h_name'] . "</p>";
                  }

                  ?>
                </div>

                <!--Number-->
                <div class="col-sm-4">

                  <?php
                  if ($row['num'] == "NULL") {
                  } else {
                    echo "<label>Phone Number</label>
            <p>" . $row['num'] . "</p>";
                  }

                  ?>
                </div>

                <div class="col-sm-12 col-md-4">

                  <div class="row">

                    <!--Room-->
                    <div class="col"><?php
                                      if ($row['room'] == "0") {
                                      } else {

                                        echo "<label>Room</label>
            <p>" . $row['room'] . "</p>";
                                      }
                                      ?>
                    </div>

                  </div>
                  <!--end row-->

                </div>
                <!--end col-md-4-->

              </div>
              <!--end row-->
            </section>



















            <!--LOCATION
=====================================================================================-->
            <section id="location" class="mb-5">

              <!--Title-->
              <h3 class="text-muted border-bottom">Location</h3>

              <div class="row">

                <div class="col-sm-6">

                  <!--Address-->
                  <div class="input-group"><?php
                                            if ($row['dir'] == "NULL") {
                                            } else {

                                              echo "<label>Address</label>
            <p>" . $row['dir'] . "</p>";
                                            }

                                            ?>
                  </div>

                  <!--City-->
                  <div class="form-group">

                    <?php
                    if ($row['city'] == "NULL") {
                    } else {
                      echo "<label>City</label>
            <p>" . $row['city'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--State-->
                  <div class="form-group">

                    <?php
                    if ($row['state'] == "NULL") {
                    } else {
                      echo "<label>State</label>
            <p>" . $row['state'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--ZIP-->
                  <div class="form-group mb-0">

                    <?php
                    if ($row['p_code'] == "NULL") {
                    } else {
                      echo "<label>Postal Code</label>
            <p>" . $row['p_code'] . "</p>";
                    }

                    ?>
                  </div>

                </div>
                <!--end col-md-6-->

                <!--Map-->
                <div id='map'></div>
                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                <script>
                mapboxgl.accessToken =
                  'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                console.log(mapboxgl.accessToken);
                var client = new MapboxClient(mapboxgl.accessToken);
                console.log(client);


                var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                var test = client.geocodeForward(address, function(err, data, res) {
                  // data is the geocoding result as parsed JSON
                  // res is the http response, including: status, headers and entity properties

                  console.log(res);
                  console.log(res.url);
                  console.log(data);

                  var coordinates = data.features[0].center;

                  var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    center: coordinates,
                    zoom: 16
                  });
                  var ei = document.createElement('div');
                  ei.id = 'house';

                  new mapboxgl.Marker(ei)
                    .setLngLat(coordinates)
                    .addTo(map);


                });
                </script>
                <!--end col-md-6-->

              </div>
              <!--end row-->
            </section>
            <!--end #location-->















            <!--GALLERY
    =====================================================================================-->
            <section id="gallery" class="mb-5">

              <!--Title-->
              <h3 class="text-muted border-bottom">House Photos </h3>


              <div class="row"
                style="margin: 0; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

                <?php if ($row4['phome'] == 'NULL' or $row4['phome'] == '') {
                  #
                } else { ?>

                <!-- MAIN PHOTO -->

                <div class="tarjet">


                  <h4 class="title-room"
                    style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                    Frontage Photo </h4>



                  <div class="div-img-r" align="center">

                    <?php if (empty($row['phome'])) {
                        #



                      } else { ?>

                    <div class="carousel-item active">

                      <img src="<?php echo '../' . $row['phome'] . '' ?>" alt="Frontage Photo" class="img-room">

                    </div>



                    <?php } ?>



                  </div><br>


                </div>


                <?php } ?>

                <?php if ($row4['fp'] == 'NULL' or $row4['fp'] == '') {
                  #
                } else { ?>


                <div class="tarjet">


                  <h4 class="title-room"
                    style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                    Family Picture </h4>



                  <div class="div-img-r" align="center">

                    <?php if (empty($row4['fp'])) {
                        #



                      } else { ?>

                    <div class="carousel-item active">

                      <img src="<?php echo '../' . $row4['fp'] . '' ?>" alt="Photo" class="img-room">

                    </div>



                    <?php } ?>



                  </div><br>


                </div>



                <?php
                }


                if ($row4['pliving'] == 'NULL' or $row4['pliving'] == '') {
                  #
                } else {

                ?>




                <!-- Living Room Photo -->
                <div class="tarjet">

                  <h4 class="title-room"
                    style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                    Living Room Photo </h4>



                  <div class="div-img-r" align="center">


                    <div class="carousel-item active">

                      <img src="<?php echo '../' . $row4['pliving'] . '' ?>" alt="Living Room" class="img-room">

                    </div>

                  </div><br>

                </div>

                <?php }

                if ($row4['parea1'] == 'NULL' or $row4['parea1'] == '' and $row4['parea2'] == 'NULL' or $row4['parea2'] == '' and $row4['parea3'] == 'NULL' or $row4['parea3'] == '' and $row4['parea4'] == 'NULL' or $row4['parea4'] == '') {
                  #
                } else {

                ?>


                <!-- Common Area Photos -->
                <div class="tarjet">

                  <h4 class="title-room"
                    style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                    House Areas Photos </h4>



                  <div class="div-img-r" align="center">

                    <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">



                      <ol class="carousel-indicators">
                        <?php if ($row4['parea1'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                        <?php } ?>

                        <?php if ($row4['parea2'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"> </li> <?php } ?>

                        <?php if ($row4['parea3'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"> </li> <?php } ?>

                        <?php if ($row4['parea4'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-1" data-slide-to="3"> </li> <?php } ?>

                      </ol>

                      <div class="carousel-inner">


                        <?php if ($row4['parea1'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item active">

                          <img src="<?php echo '../' . $row4['parea1'] . '' ?>" alt="Kitchen" class="img-room">

                        </div>

                        <?php }


                          if ($row4['parea2'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['parea2'] . '' ?>" alt="Dining Room" class="img-room">

                        </div>

                        <?php }


                          if ($row4['parea3'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['parea3'] . '' ?>" alt="Area 3" class="img-room">

                        </div>

                        <?php }


                          if ($row4['parea4'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['proom1_3'] . '' ?>" alt="Area 4" class="img-room">

                        </div>

                        <?php } ?>


                      </div>

                      <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button"
                        data-slide="prev" style="height: 50%; margin-top: 15%;">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>

                      <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button"
                        data-slide="next" style="height: 50%; margin-top: 15%;">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>

                    </div>


                  </div><br>

                </div>



                <?php }

                if ($row4['pbath1'] == 'NULL' or $row4['pbath1'] == '' and $row4['pbath2'] == 'NULL' or $row4['pbath2'] == '' and $row4['pbath3'] == 'NULL' or $row4['pbath3'] == '' and $row4['pbath4'] == 'NULL' or $row4['pbath4'] == '') {
                  #
                } else {

                ?>


                <!-- Bathroom Photos -->
                <div class="tarjet">

                  <h4 class="title-room"
                    style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                    Bathroom Photos </h4>



                  <div class="div-img-r" align="center">

                    <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">



                      <ol class="carousel-indicators">
                        <?php if ($row4['pbath1'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                        <?php } ?>

                        <?php if ($row4['pbath2'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"> </li> <?php } ?>

                        <?php if ($row4['pbath3'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"> </li> <?php } ?>

                        <?php if ($row4['pbath4'] != 'NULL') { ?>
                        <li data-target="#carouselExampleIndicators-2" data-slide-to="3"> </li> <?php } ?>

                      </ol>

                      <div class="carousel-inner">


                        <?php if ($row4['pbath1'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item active">

                          <img src="<?php echo '../' . $row4['pbath1'] . '' ?>" alt="Bathroom 1" class="img-room">

                        </div>

                        <?php }


                          if ($row4['pbath2'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['pbath2'] . '' ?>" alt="Bathroom 2" class="img-room">

                        </div>

                        <?php } ?>


                        <?php if ($row4['pbath3'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['pbath3'] . '' ?>" alt="Area 3" class="img-room">

                        </div>

                        <?php }


                          if ($row4['pbath4'] == 'NULL') {
                            #
                          } else { ?>


                        <div class="carousel-item ">

                          <img src="<?php echo '../' . $row4['pbath4'] . '' ?>" alt="Area 4" class="img-room">

                        </div>

                        <?php } ?>


                      </div>

                      <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button"
                        data-slide="prev" style="height: 50%; margin-top: 15%;">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>

                      <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button"
                        data-slide="next" style="height: 50%; margin-top: 15%;">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>

                    </div>


                  </div><br>

                </div>

                <?php } ?>

              </div>






              <br>

            </section>






















            <!--ADDITIONAL INFORMATION
=====================================================================================-->
            <section id="additional-information" class="mb-5">

              <!--Title-->
              <h3 class="text-muted border-bottom">Additional Information</h3>

              <!--Description-->
              <section>

                <?php
                if ($row['des'] == "NULL") {
                } else {
                  echo "<label>Description</label>
            <p>" . $row['des'] . "</p>";
                }

                ?>

              </section>

              <section>
                <!--Row-->
                <div class="row">

                  <!--Family-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['num_mem'] == "0") {
                    } else {
                      echo "<label>Number of Family Members</label>
            <p>" . $row['num_mem'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--Nacionality-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['backg'] == "NULL") {
                    } else {
                      echo "<label>Background</label>
            <p>" . $row['backg'] . "</p>";
                    }

                    ?>
                  </div>


                  <!--Garages-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['backl'] == "NULL") {
                    } else {
                      echo "<label>Background Language</label>
            <p>" . $row['backl'] . "</p>";
                    }

                    ?>
                  </div>

                </div>
                <div class="row">

                  <!--Academy-->
                  <div class="col-sm-4">

                    <?php
                    if ($row7['id_ac'] == "NULL") {
                    } else {
                      echo "<label>Academy Preference</label>
            <p>" . $row7['name_a'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--Gender Preference-->
                  <div id="gendermain" class="col-sm-4">

                    <?php
                    if ($row['g_pre'] == "NULL") {
                    } else {
                      echo "<label>Gender Preference</label>
            <p>" . $row['g_pre'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--Age-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['ag_pre'] == "NULL") {
                    } else {
                      echo "<label>Age Preference</label>
            <p>" . $row['ag_pre'] . "</p>";
                    }

                    ?>
                  </div>



                  <!--Status-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['status'] == "NULL") {
                    } else {
                      echo "<label>Status</label>
            <p>" . $row['status'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--Garages-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['cell'] == "NULL") {
                    } else {
                      echo "<label>Cellphone</label>
            <p>" . $row['cell'] . "</p>";
                    }

                    ?>
                  </div>

                  <!--Smokers Politics-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['smoke'] == "NULL") {
                    } else {
                      echo "<label> Smokers Politics</label>
            <p>" . $row['smoke'] . "</p>";
                    }

                    ?>
                  </div>
                </div>

                <?php

                ?>

                <!--Checkboxes-->
                <div class="col-sm-4">

                  <?php

                  if ($row['vegetarians'] == "yes" or $row['halal'] == "yes" or $row['kosher'] == "yes" or $row['lactose'] == "yes" or $row['gluten'] == "yes" or $row['pork'] == "yes" or $row['none'] == "yes") {
                    echo "<label>Special Diet</label>";
                  } else {
                  }
                  ?>
                  <ul class="ts-list-check-marks">

                    <?php
                    if ($row['vegetarians'] == "yes") {
                      echo "<li class='ts-checked'>Vegetarians</li>";
                    } else {
                    }
                    if ($row['halal'] == "yes") {
                      echo "<li class='ts-checked'>Halal (Muslims)</li>";
                    } else {
                    }
                    if ($row['kosher'] == "yes") {
                      echo "<li class='ts-checked'>Kosher (Jews)</li>";
                    } else {
                    }
                    if ($row['lactose'] == "yes") {
                      echo "<li class='ts-checked'>Lactose Intolerant</li>";
                    } else {
                    }
                    if ($row['gluten'] == "yes") {
                      echo "<li class='ts-checked'>Gluten Free Diet</li>";
                    } else {
                    }
                    if ($row['pork'] == "yes") {
                      echo "<li class='ts-checked'>No Pork</li>";
                    } else {
                    }
                    if ($row['none'] == "yes") {
                      echo "<li class='ts-checked'>None</li>";
                    } else {
                    }
                    ?>

                  </ul>
                </div>



                <!--end ts-column-count-3-->
                <br>
                <br>
                <h3 class="text-muted border-bottom">Pets Information</h3>
                <div class="row">

                  <!--Pets-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['pet'] == "NULL") {
                    } else {
                      echo "<label>Pets</label>
            <p>" . $row['pet'] . "</p>";
                    }


                    ?>
                  </div>

                  <!--Pet Numbers-->
                  <div class="col-sm-4">

                    <?php
                    if ($row['pet_num'] == "0") {
                    } else {
                      echo '<label>Number of Pets</label>';
                      echo "<p>" . $row['pet_num'] . "</p>";
                    }
                    ?>
                  </div>

                  <div id="typepet" class="col-sm-4">

                    <?php
                    if ($row['type_pet'] == "NULL") {
                    } else {
                      echo '<label>Type of Pets</label>';
                      echo "<p>" . $row['type_pet'] . "</p>";
                    }
                    ?>
                  </div>


                </div>
                <div class="row">
                  <br>
                  <br>


                  <!--Number Pets-->

                  <div class="col-sm-4">

                    <?php

                    if ($row['dog'] == "yes" or $row['cat'] == "yes" or $row['other'] == "yes") {
                      echo "<label>Type Of Pets</label>";
                    } else {
                    }
                    ?>
                    <ul class="ts-list-check-marks">
                      <?php
                      if ($row['dog'] == "yes") {
                        echo "<li class='ts-checked'>Dogs</li>";
                      } else {
                      }
                      if ($row['cat'] == "yes") {
                        echo "<li class='ts-checked'>Cats</li>";
                      } else {
                      }
                      if ($row['other'] == "yes") {
                        echo "<li class='ts-checked'>Others</li>";
                      } else {
                      }
                      ?>
                    </ul>
                  </div>




                </div>
                <h3 class="text-muted border-bottom">Main Contact Info</h3>
                <!--Family-->
                <section id="main-information" class="mb-5">
                  <div class="row">

                    <!--Name-->
                    <div class="col-sm-4">

                      <?php
                      if ($row['name_h'] == "NULL") {
                      } else {
                        echo "<div class='form-group'>
                <label>Name</label>
            <p>" . $row['name_h'] . "</p>
            </div>";
                      }

                      ?>

                    </div>

                    <!--Last Name-->
                    <div class="col-sm-4">

                      <?php
                      if ($row['l_name_h'] == "NULL") {
                      } else {
                        echo " <div class='form-group'>
                <label>Last Name</label>
            <p>" . $row['l_name_h'] . "</p>
            </div>";
                      }

                      ?>

                    </div>

                    <!--Date of Birth-->
                    <div class="col-sm-4">

                      <?php
                      if ($row['db'] == "NULL") {
                      } else {
                        echo "<div class='form-group'>
                <label>Date of Birth</label>
            <p>" . $row['db'] . "</p>
            </div>";
                      }

                      ?>

                    </div>

                    <!--Gender-->
                    <div class="col-sm-4">

                      <?php
                      if ($row['gender'] == "NULL") {
                      } else {
                        echo "<div class='form-group'>
                <label>Gender</label>
            <p>" . $row['gender'] . "</p>
            </div>";
                      }

                      ?>
                    </div>

                    <!--Date of Background Check-->
                    <div class="col-sm-4">

                      <?php
                      if ($row['db_law'] == "NULL") {
                      } else {
                        echo "<div class='form-group'>
                <label>Date of Background Check</label>
            <p>" . $row['db_law'] . "</p>
            </div>";
                      }

                      ?>
                    </div>

                    <?php
                    if ($row['law'] == "NULL") {
                    } else {
                      echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
                      echo '<iframe class="embed-responsive-item" src="../' . $row['law'] . '"></iframe>
                </div>';
                      echo '<button><a href="../' . $row['law'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
                    }
                    ?>

                  </div>
                </section>
                <br>








                <h3 class="text-muted border-bottom">Family Information</h3>
                <section id="family-information" class="mb-5">




                  <?php


                  if ($row3['f_name1'] == "NULL" and $row3['f_lname1'] == "NULL" and $row3['db1'] == "NULL" and $row3['gender1'] == "NULL" and $row3['db_lawf1'] == "NULL" and $row3['lawf1'] == "NULL" and $row3['re1'] == "NULL") {

                    echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
                  } else {
                    echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 1:";
                  }
                  ?>

                  </h4>
          </div>
        </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name1'] == "NULL" and $row3['f_lname1'] == "NULL" and $row3['db1'] == "NULL" and $row3['gender1'] == "NULL" and $row3['db_lawf1'] == "NULL" and $row3['lawf1'] == "NULL" and $row3['re1'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name1'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name1'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname1'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname1'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db1'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db1'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender1'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender1'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re1'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re1'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf1'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf1'] . "</p>";
          }
          ?>

        </div>
      </div>



      <?php
      if ($row3['lawf1'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf1'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf1'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name2'] == "NULL" and $row3['f_lname2'] == "NULL" and $row3['db2'] == "NULL" and $row3['gender2'] == "NULL" and $row3['db_lawf2'] == "NULL" and $row3['lawf2'] == "NULL" and $row3['re2'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 2:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name2'] == "NULL" and $row3['f_lname2'] == "NULL" and $row3['db2'] == "NULL" and $row3['gender2'] == "NULL" and $row3['db_lawf2'] == "NULL" and $row3['lawf2'] == "NULL" and $row3['re2'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name2'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name2'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname2'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname2'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db2'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db2'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender2'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender2'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re2'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re2'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf2'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf2'] . "</p>";
          }
          ?>

        </div>
      </div>

      <?php
      if ($row3['lawf2'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf2'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf2'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name3'] == "NULL" and $row3['f_lname3'] == "NULL" and $row3['db3'] == "NULL" and $row3['gender3'] == "NULL" and $row3['db_lawf3'] == "NULL" and $row3['lawf3'] == "NULL" and $row3['re3'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 3:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name3'] == "NULL" and $row3['f_lname3'] == "NULL" and $row3['db3'] == "NULL" and $row3['gender3'] == "NULL" and $row3['db_lawf3'] == "NULL" and $row3['lawf3'] == "NULL" and $row3['re3'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name3'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name3'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname3'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname3'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db3'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db3'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender3'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender3'] . "</p>";
          }
          ?>

        </div>
      </div>


      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re3'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re3'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf3'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf3'] . "</p>";
          }
          ?>

        </div>
      </div>



      <?php
      if ($row3['lawf3'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf3'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf3'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name4'] == "NULL" and $row3['f_lname4'] == "NULL" and $row3['db4'] == "NULL" and $row3['gender4'] == "NULL" and $row3['db_lawf4'] == "NULL" and $row3['lawf4'] == "NULL" and $row3['re4'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 4:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name4'] == "NULL" and $row3['f_lname4'] == "NULL" and $row3['db4'] == "NULL" and $row3['gender4'] == "NULL" and $row3['db_lawf4'] == "NULL" and $row3['lawf4'] == "NULL" and $row3['re4'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name4'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name4'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname4'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname4'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db4'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db4'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender4'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender4'] . "</p>";
          }
          ?>

        </div>
      </div>


      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re4'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re4'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf4'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf4'] . "</p>";
          }
          ?>

        </div>
      </div>




      <?php
      if ($row3['lawf4'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf4'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf4'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name5'] == "NULL" and $row3['f_lname5'] == "NULL" and $row3['db5'] == "NULL" and $row3['gender5'] == "NULL" and $row3['db_lawf5'] == "NULL" and $row3['lawf5'] == "NULL" and $row3['re5'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 5:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name5'] == "NULL" and $row3['f_lname5'] == "NULL" and $row3['db5'] == "NULL" and $row3['gender5'] == "NULL" and $row3['db_lawf5'] == "NULL" and $row3['lawf5'] == "NULL" and $row3['re5'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name5'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name5'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname5'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname5'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db5'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db5'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender5'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender5'] . "</p>";
          }
          ?>

        </div>
      </div>


      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re5'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re5'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf5'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf5'] . "</p>";
          }
          ?>

        </div>
      </div>



      <?php
      if ($row3['lawf5'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf5'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf5'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name6'] == "NULL" and $row3['f_lname6'] == "NULL" and $row3['db6'] == "NULL" and $row3['gender6'] == "NULL" and $row3['db_lawf6'] == "NULL" and $row3['lawf6'] == "NULL" and $row3['re6'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 6:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name6'] == "NULL" and $row3['f_lname6'] == "NULL" and $row3['db6'] == "NULL" and $row3['gender6'] == "NULL" and $row3['db_lawf6'] == "NULL" and $row3['lawf6'] == "NULL" and $row3['re6'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name6'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name6'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname6'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname6'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db6'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db6'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender6'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender6'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re6'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re6'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf6'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf6'] . "</p>";
          }
          ?>

        </div>
      </div>



      <?php
      if ($row3['lawf6'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf6'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf6'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name7'] == "NULL" and $row3['f_lname7'] == "NULL" and $row3['db7'] == "NULL" and $row3['gender7'] == "NULL" and $row3['db_lawf7'] == "NULL" and $row3['lawf7'] == "NULL" and $row3['re7'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 7:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name7'] == "NULL" and $row3['f_lname7'] == "NULL" and $row3['db7'] == "NULL" and $row3['gender7'] == "NULL" and $row3['db_lawf7'] == "NULL" and $row3['lawf7'] == "NULL" and $row3['re7'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name7'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name7'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname7'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname7'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db7'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db7'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender7'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender7'] . "</p>";
          }
          ?>

        </div>
      </div>


      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re7'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re7'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf7'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf7'] . "</p>";
          }
          ?>

        </div>
      </div>



      <?php
      if ($row3['lawf7'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf7'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf7'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

      <?php
      if ($row3['f_name8'] == "NULL" and $row3['f_lname8'] == "NULL" and $row3['db8'] == "NULL" and $row3['gender8'] == "NULL" and $row3['db_lawf8'] == "NULL" and $row3['lawf8'] == "NULL" and $row3['re8'] == "NULL") {

        echo '<div class="row" style="margin-bottom: -30px;">
        <div class="col-sm-4">
            <div class="form-group">
        <h4 class="panel-title"> ';
      } else {
        echo "<div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>
        <h4 class='panel-title'>
                Member 8:";
      }
      ?>

      </h4>
      </div>
      </div>
      </div>

      <!--Name-->
      <?php
      if ($row3['f_name8'] == "NULL" and $row3['f_lname8'] == "NULL" and $row3['db8'] == "NULL" and $row3['gender8'] == "NULL" and $row3['db_lawf8'] == "NULL" and $row3['lawf8'] == "NULL" and $row3['re8'] == "NULL") {

        echo " <div class='row' style='margin-bottom: -30px;'>

        <div class='col-sm-4'>
            <div class='form-group'>";
      } else {

        echo " <div class='row'>
        <div class='col-sm-4'>
            <div class='form-group'>";
      }
      ?>
      <?php
      if ($row3['f_name8'] == "NULL") {
      } else {
        echo " <label>Name</label>
            <p>" . $row3['f_name8'] . "</p>";
      }
      ?>

      </div>
      </div>

      <!--Last Name-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['f_lname8'] == "NULL") {
          } else {
            echo " <label>Last Name</label>
            <p>" . $row3['f_lname8'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Date of Birth-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db8'] == "NULL") {
          } else {
            echo " <label>Date of Birth</label>
            <p>" . $row3['db8'] . "</p>";
          }
          ?>


        </div>
      </div>

      <!--Gender-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['gender8'] == "NULL") {
          } else {
            echo " <label>Gender</label>
            <p>" . $row3['gender8'] . "</p>";
          }
          ?>

        </div>
      </div>

      <!--Relation-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['re8'] == "NULL") {
          } else {
            echo " <label>Relation</label>
            <p>" . $row3['re8'] . "</p>";
          }
          ?>

        </div>
      </div>


      <!--Date of Background Check-->
      <div class="col-sm-4">
        <div class="form-group">

          <?php
          if ($row3['db_lawf8'] == "NULL") {
          } else {
            echo " <label>Date of Background Check</label>
            <p>" . $row3['db_lawf8'] . "</p>";
          }
          ?>

        </div>
      </div>


      <?php
      if ($row3['lawf8'] == "NULL") {
      } else {
        echo " <!--File upload-->
                    <div class='col-sm-4'>
                    <div class='form-group'>
                    <label for='bl'> Background Check</label>
                        <br>
                <div class='embed-responsive embed-responsive-4by3'>";
        echo '<iframe class="embed-responsive-item" src="../' . $row3['lawf8'] . '"></iframe>
                </div>';
        echo '<button><a href="../' . $row3['lawf8'] . '">Penal Antecedents Submitted</a></button>
                     </div>
                </div> ';
      }
      ?>
      </div>

    </section>

    <!-- Tarjet edit rooms -->



    </section>

    <hr>

    <section class="py-3">
      <a id="edit" href="edit-property.php" id="edit" class="btn btn-outline-secondary btn-lg float-left">
        <i class="fa fa-pencil-alt mr-2"></i>
        Edit Property
      </a>
      <button type="submit" id="submit" onclick="window.location='submitted.php'"
        class="btn btn-primary ts-btn-arrow btn-lg float-right">Submit</button>
    </section>
    </div>
    <!--end offset-lg-1 col-lg-10-->
    </div>
    <!--end row-->
    </div>
    <!--end container-->
    </section>

  </main>
  <!--end #ts-main-->

  <!--*********************************************************************************************************-->
  <!--************ FOOTER *************************************************************************************-->
  <!--*********************************************************************************************************-->

  <footer id="ts-footer">

    <?php include 'footer.php' ?>
  </footer>
  <!--end #ts-footer-->
  </div>
  <!--end page-->


  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/galery-homestay.js"></script>
  <script src='assets/js/ajax_noti.js'></script>

</body>

</html>