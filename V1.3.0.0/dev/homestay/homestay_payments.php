<?php
require '../xeon.php';
include '../cript.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$payments = $link->query("SELECT * FROM payments WHERE r_mail = '$usuario'");
$rp = mysqli_num_rows($payments);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Homestay Payments</title>

  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">

  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/header-index.css">
  <link rel="stylesheet" href="assets/css/homestay_payments.css">
  <link rel="stylesheet" href="assets/css/notification.css">

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
</head>

<?php include 'header.php' ?>

<body style="background-color: #eee;">
  <main class="w-100" id="ts-main">

    <div class="payments__title_div">
      <h1>Homestay Payments</h1>
    </div>

    <hr>

    <div class="container col-lg-11">
      <br />
      <div class="card">
        <div class="card-body">
          <!--<div class="form-group">
                  <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" />
                </div>-->
          <div class="table-responsive" id="dynamic_content">
          </div>

          <div id="table-voun" class="table-responsive text-center ">
            <table class="table table-hover" id="vou">
              <thead style="background-color: #232159; color: white">
                <tr>
                  <th class="text-center align-middle">N° Invoice</th>
                  <th class="text-center align-middle">Date</th>
                  <th class="text-center align-middle">Price</th>
                  <th class="text-center align-middle">Student</th>
                  <th class="text-center align-middle">Status</th>
                  <th class="text-center align-middle"></th>
                </tr>
              </thead>

              <tbody class="table-bordered">
                <?php while ($pay = mysqli_fetch_array($payments)) {

                  $manager = $link->query("SELECT * FROM manager WHERE mail = '$pay[r_mail]'");
                  $r_man = $manager->fetch_assoc();

                  $student = $link->query("SELECT * FROM pe_student WHERE mail_s = '$pay[reserve_s]'");
                  $r_stu = $student->fetch_assoc();

                  $id_p = strlen($pay['id_p']);
                ?>
                  <tr>
                    <th class="text-center align-middle text_table1">

                      <?php if ($id_p == '1') { ?>

                        #0000<?php echo $pay['id_p'] ?>

                      <?php } else if ($id_p == '2') { ?>

                        #000<?php echo $pay['id_p'] ?>

                      <?php } else if ($id_p == '3') { ?>

                        #00<?php echo $pay['id_p'] ?>

                      <?php } else if ($id_p == '4') { ?>

                        #0<?php echo $pay['id_p'] ?>

                      <?php } else if ($id_p == '5') { ?>

                        #<?php echo $pay['id_p'] ?>

                      <?php } ?>

                    </th>

                    <th class="text-center align-middle"><?php echo substr($pay['date_p'], 0, 10); ?></th>
                    <th class="text-center align-middle"><b> CAD$ </b><?php echo $pay['price_p'] ?></th>
                    <th class="text-center align-middle"><a class="link_stu" href="student_info?art_id=<?= $r_stu['id_student'] ?>"><?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?></a>
                    </th>

                    <?php if ($pay['status_p'] == 'Payable') { ?>
                      <th class="text-center align-middle payable"><?php echo $pay['status_p'] ?></th>
                    <?php } else if ($pay['status_p'] == 'Budgeted') { ?>
                      <th class="text-center align-middle budgeted"><?php echo $pay['status_p'] ?></th>
                    <?php } else if ($pay['status_p'] == 'Paid') {  ?>
                      <th class="text-center align-middle paid"><?php echo $pay['status_p'] ?></th>
                    <?php } else {
                    } ?>
                    <th class="text-center align-middle"><button class="btn" style="background-color: #4f177d; color: white;"> See Invoice</button></th>
                  </tr>

                <?php } ?>




              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>

  </main>


  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <!-- <script src="../assets/js/custom.js"></script> -->
  <script src="../assets/js/map-leaflet.js"></script>


  <?php include 'footer.php' ?>
</body>

</html>