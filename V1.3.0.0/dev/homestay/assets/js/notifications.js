// TODO IMPORTACIONES
import Notifications from "../../../controllers/notifications.js";
import Header from "./header.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? NOTIFICATION CONTAINER
const $notificationList = D.getElementById("notification-list");

// TODO FUNCIONES Y/O CLASES
class HomestayNotifications extends Notifications {
  constructor() {
    super();
  }

  // ? GESTIONAR NOTIFICACIONES
  async manageNotifications() {
    const allNotifications = await super.getNotifications();
    if (allNotifications.notifications) {
      allNotifications.notifications.forEach((notification) => {
        this.insertNotification(notification);
      });
    }
    if (allNotifications.newNotifications) this.evaluateNewNotifications(allNotifications.newNotifications);
  }
}

// TODO INSTANCIA
const INSTANCE = new HomestayNotifications();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await INSTANCE.manageNotifications();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // * MOSTRAR PANEL DE NOTIFICACIONES
  if (e.target.id === "bell-btn") INSTANCE.showPanel();
  if (e.target.id === "mobile-bell-btn" || e.target.matches(".close-notifications")) INSTANCE.showPanel();
  // * OCULTAR PANEL DE NOTIFICACIONES
  if (
    !e.target.id.includes("bell-btn") &&
    !e.target.id.includes("notification-list") &&
    !e.target.matches("#notification-list *") &&
    $notificationList.classList.contains("d-flex")
  ) {
    INSTANCE.showElement(false, $notificationList);
    INSTANCE.bellBtn.dataset.show = "false";
  }
  // * VER NOTIFICACION
  if (e.target.matches(".notification-item") || e.target.matches(".notification-item *")) INSTANCE.markAsRead(e.target);
});

// ! SCROLL
D.addEventListener("scroll", (e) => {
  // * OCULTAR PANEL DE NOTIFICACIONES
  if (D.querySelector("#notification-list").classList.contains("show")) INSTANCE.showPanel();
});

// ! RESIZE
W.addEventListener("resize", (e) => {
  // * OCULTAR PANEL DE NOTIFICACIONES
  if ($notificationList.classList.contains("d-flex")) {
    INSTANCE.showElement(false, $notificationList);
    INSTANCE.bellBtn.dataset.show = "false";
  }
});

// ! ERROR
