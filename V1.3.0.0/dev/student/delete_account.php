<?php
include '../xeon.php';  
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$query = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario' ");
$row=$query->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_delete.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_info2.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Advance Options</title>

</head>
<body>

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <br>
                            <h1 id="student_title_name">
                                <?php
                                    
                                        if($row['photo_s'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo_s'].'" id="profile-image"/><br>';}
                                 
                                    ?>
                                    <?php echo $row['name_s'] . ' ' . $row['l_name_s']; ?>
                                
                                </h1>
                                
                        </div>
                        <hr id="title">
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
       <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

                        


        

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                 <section id="content">
        <div class="container sec">
            <div class="row flex-wrap-reverse">

                    <!--LEFT SIDE
                        =============================================================================================-->
                    <div class="col-md-5 col-lg-4">

                        <!--DETAILS
                            =========================================================================================-->
                            <section id="content">
                                <div class="container">
                                    
                        <section>
                            <h3 class="title-pe">Personal Information</h3>
                            <div class="ts-box">

                                <dl class="ts-description-list__line mb-0">

                                    <?php if ($row['mail_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                        <dt>Mail</dt>
                                        <dd><?php echo $row['mail_s'] ?></dd>

                                        <dt> Date of bird </dt>
                                        <dd><?php echo $row['db_s'] ?></dd>

                                        <?php }

                                        $from = new DateTime($row['db_s']);
                                        $to   = new DateTime('today');
                                        $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
                                        $sufix= " Years old";



                                    if ($row['db_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <dt> Age </dt>
                                    <dd><?php echo $age . $sufix ?></dd>

                                    <dt> Background </dt>
                                    <dd><?php echo $row['nacionality'] ?></dd>

                                    <dt> Origin City </dt>
                                    <dd><?php echo $row['city'] ?></dd>

                                    <dt> Origin Language </dt>
                                    <dd><?php echo $row['lang_s'] ?></dd>


                                    <?php
                                    }
                                    
                                    if ($row['num_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <?php 
                                    }

                                    if ($row['db_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <?php 
                                    }

                                    if ($row['gen_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <dt> Gender </dt>
                                    <dd><?php echo $row['gen_s'] ?></dd>

                                    <?php 
                                    }

                                    if ($row['nacionality'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <?php 
                                    }

                                    if ($row['city'] == 'NULL') {
                                        
                                    } else{
                                    ?>


                                    <?php 
                                    }

                                    if ($row['lang_s'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <?php 
                                    }

                                    if ($row['passport'] == 'NULL') {
                                        
                                    } else{
                                    ?>

                                    <dt> Passport </dt>
                                    <dd><?php echo $row['passport'] ?></dd>

                                    <dt> Visa Expiration Date </dt>
                                    <dd><?php echo $row['db_visa'] ?></dd>

                                    <?php } ?>

                                </dl>


                            </div>

                            <section id="actions">

                            <div class="d-flex justify-content-between">
                                    <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top" title="Visa" href="../<?php echo $row['visa'];?>" target="_blank">
                                            <i class="fa fa-file"></i>
                                    </a>                                

                            </div>

                        </section>

                        </section>

                                        <!--DELETE SECTION-->
                        <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1"><i class="fa fa-cog" id="options"></i> Advance Options</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
         <button class="btn btn-info" data-toggle="collapse" data-target="#disable1"><i class="fa fa-user-times"></i> Disable Student</button>

<div id="disable1" class="collapse">
    <br>
<h3>Disable Student</h3>
        <p>The student will be disable immediately.<br/>

        This action will <b>disable the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its files, information and events.<br/>

        <b>If you wish disable this user account press the button?</b></p>
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2"><i class="fa fa-user-times" ></i> Disable Student</button>

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Disable Student</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
      </div>
      <div class="modal-body" id="background">
      <div class="modal-body" id="body"><p>
         <i class="fa fa-asterisk" id="options"></i> You are about to disable this student<br/>
        Once a student is disable the user will not login in homebor until his user has been reactivated again. its files, all information and events will be not removing of the data.
      </p></div></div>
      <div class="modal-body" id="background">
        <p><b>All Fields Required</b></p>
        <p>Please type the following to confirm:<br>

<?php echo "<b>".$row['id_student']."</b>";?></p>
        <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student.php" method="post" enctype="multipart/form-data">
            <div class="row">

                                    <!--House Name-->
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="del_id" name="del_id" required>
                                            <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>
                                            <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>
                                            <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>
                                             <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row[id_m]";?>" hidden>
                                            <label for="des">Reason</label>
                                            <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                        </div>
                                    </div>
                                </div>
                    
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
        <button type="submit" id="submit" name="register" class="btn btn-default">
                                   <i class="fa fa-user-times"></i> Disable</button>
        </form>
      </div>
    </div>

  </div>
</div>
<br>
<br>
</div>
  
      <br>
    </div>
  </div>
</div>
<!--END DELETE SECTION-->


                    </div>
                
            </section>

                    </div>

                    <!--RIGHT SIDE
                        =============================================================================================-->
                    <div class="col-md-7 col-lg-8">

                        <h3 class="title-aca">Academy Information </h3>

                        <!--QUICK INFO
                            =========================================================================================-->
                        <section id="location" class="mb-5" style="background: white; padding: 1em 1em; padding-top: 2em; box-shadow: 2px 2px 5px #4F177D;">

                                <!--Title-->
                                

                                <!--Row-->
                                <div class="row" style="padding:0;">

                                    <!--Academy Name-->
                                    <?php
                                            if($row2['name_a'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>
                
                                            <span class="focus"><b> Academy Name </b></span>
                                            <p style="text-align: center;"> <?php echo $row2['name_a']; ?></p>

                                            </div>
                                        </div>
                                    <?php         
                                    }
                                    ?>

                                    <?php



                                            if($row2['city_a'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-3" style="text-align: center;">
                                            <div class='form-group'>

                                                <span class="focus"><b> City </b></span>

                                                <p style="text-align: center;"> <?php echo $row2['city_a']; ?></p> 

                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>

                                    <?php
                                            if($row2['dir_a'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>
                
                                            <span class="focus"><b> Academy Address </b></span>

                                            <p style="text-align: center;"> <?php echo $row2['dir_a']; ?></p>

                                            </div>
                                        </div>

                                    <?php         
                                    }
                                    ?>



                                    <!--Type of Student-->
                                    <?php
                                            if($row['type_s'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>

                                                <span class="focus"><b> Type of Student </b></span>

                                                <p style="text-align: center;"> <?php echo $row['type_s']; ?></p> 

                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>
                                    

                                   <!--Arrive Date-->
                                    <?php
                                            if($row['firstd'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                            <div class="col-sm-3" style="text-align: center;">
                                                <div class='form-group'>
                
                                                <span class="focus"><b> Start Date </b></span>

                                                <p style="text-align: center;"> <?php echo $row['firstd']; ?></p>

                                                </div>

                                            </div>
                                    <?php        
                                    }
                                    ?>

                                    <!--Last Date-->
                                    <?php
                                            if($row['lastd'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                            <div class="col-sm-4" style="text-align: center;">
                                                <div class='form-group'>
                
                                                <span class="focus"><b> End Date </b></span>

                                                <p style="text-align: center;"> <?php echo $row['lastd']; ?></p>

                                                </div>

                                            </div>
                                    <?php   
                                    }
                                    ?>

                                    
                                </div>

                        </section>

                        <h3 class="title-pre">Preferences Information </h3>

                        <section id="location" class="mb-5" style="background: white; padding: 1em 0em; padding-right: 3em; padding-top: 2em; box-shadow: 2px 2px 5px #4F177D; ">

                                
                                <?php

                                    if ($row['smoke_s'] == "NULL" && $row['pets'] == "NULL" && $row['food'] == "NULL") {
                                ?>

                                    <div class="container" style="text-align: center;" align="center">

                                        <h5 style="font-weight: normal;"> No Preferences </h5>

                                    </div>

                                <?php
                                    }

                                ?>
                                

                                <!--Row-->
                                 <div class="row" >

                                    <!--Smoker-->
                                    <?php
                                            if($row['smoke_s'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>
                
                                            <span class="focus"><b>Smoker</b></span>

                                            <p style="text-align: center;"> <?php echo $row['smoke_s']; ?></p>

                                            </div>
                                        </div>

                                    <?php         
                                    }
                                    ?>

                                    <!--Pets-->
                                    <?php
                                            if($row['pets'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>

                                                <span class="focus"><b>Pets</b></span>

                                                <p style="text-align: center;"> <?php echo $row['pets']; ?></p>

                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>
                                    

                                   <!--Food-->
                                    <?php
                                            if($row['food'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                            <div class="col-sm-4" style="text-align: center;">
                                                <div class='form-group'>
                
                                                <span class="focus"><b>Food</b></span>

                                                <p style="text-align: center;"> <?php echo $row['food']; ?></p>

                                                </div>

                                            </div>
                                    <?php        
                                    }
                                    ?>

                                    

                                    
                                </div>

                            </section>

                            <h3 class="title-aca">Additional Information</h3>

                        <!--QUICK INFO
                            =========================================================================================-->
                        <section id="location" class="mb-5" style="background: white; padding: 1em 1em; padding-top: 2em; box-shadow: 2px 2px 5px #4F177D;">

                                <!--Title-->
                                

                                <!--Row-->
                                <div class="row" style="padding:0;">

                                    <!--Academy Name-->
                                    <?php
                                            if($row['num_s'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>
                
                                            <span class="focus"><b>Contact</b></span>

                                            <p style="text-align: center;"> <?php echo $row['num_s']; ?></p>

                                            </div>
                                        </div>
                                    <?php         
                                    }
                                    ?>

                                    <?php



                                            if($row['cell_s'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-3" style="text-align: center;">
                                            <div class='form-group'>

                                                <span class="focus"><b>Alternative</b></span>

                                                <p style="text-align: center;"> <?php echo $row['cell_s']; ?></p>

                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>

                                    <?php
                                            if($row['num_conts'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>
                
                                            <span class="focus"><b>Emergecy Contact</b></span>

                                            <p style="text-align: center;"> <?php echo $row['num_conts']; ?></p>

                                            </div>
                                        </div>

                                    <?php         
                                    }
                                    ?>



                                    <!--Type of Student-->
                                    <?php
                                            if($row['cont_name'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                        <div class="col-sm-4" style="text-align: center;">
                                            <div class='form-group'>

                                                <span class="focus"><b>Emergency Contact Name</b></span>

                                                <p style="text-align: center;"> <?php echo $row['cont_name']; ?></p>

                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>
                                    

                                    <!--Last Date-->
                                    <?php
                                            if($row['cont_lname'] == "NULL"){
                                            }
                                        else {
                                    ?>
                                            <div class="col-sm-4" style="text-align: center;">
                                                <div class='form-group'>
                
                                                <span class="focus"><b>Emergency Contact Last Name</b></span>

                                                <p style="text-align: center;"> <?php echo $row['cont_lname']; ?></p> 

                                                </div>

                                            </div>
                                    <?php   
                                    }
                                    ?>

                                    
                                </div>

                        </section>

                        <h3>Special Diet</h3>

                        <section id="location" class="mb-5" style="background: white; padding: 1em 1em; padding-top: 2em; box-shadow: 2px 2px 5px #4F177D;">

                            <div class="row" style="padding:0; margin-left: 2px;">
                            <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                                <?php
                                            if($row['vegetarians'] == "yes"){
                                            echo "<li>Vegetarians</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['halal'] == "yes"){
                                            echo "<li>Halal</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['kosher'] == "yes"){
                                            echo "<li>Kosher (Jews)</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['lactose'] == "yes"){
                                            echo "<li>Lacotose Intolerant</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['gluten'] == "yes"){
                                            echo "<li>Gluten Free Diet</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['pork'] == "yes"){
                                                echo "<li>No Pork</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['none'] == "yes"){
                                            echo "<li>None</li>";
                                            }
                                        else {
                                            
                                        }
                                    ?>
                            </ul>

                            </div>

                            </section>


                            
                </div>

                                    
                                    

                            </section>
                            
                            <!--
                            <hr>
                            
                            <section class="py-3">
                                <button type="submit" id="submit" name="register" onclick="window.location=''" class="btn btn-primary ts-btn-arrow btn-lg float-right" style="color: white; background-color: #232159; border: 2px solid #232159;">
                                   <i class="fa fa-save mr-2"></i> Submit
                                </button>
                            </section>-->

                           </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->


<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>

