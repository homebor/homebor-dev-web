<?php ?>
    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->

    <footer id="ts-footer">
        <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
        <!--MAIN FOOTER CONTENT
            =========================================================================================================-->
            <section id="ts-footer-main" class="main__student__footer">
            <div class="container">
                <div class="row">

                    <div class="col-sm-6 div__image__iprom">
                        <div class="div__image_prom mt-auto mb-auto">
                            <img src="../assets/iHomestay/logos/iHomestay_logo.png" alt="" class="image__iprom ">
                        </div>
                    </div>
                    
                    <div class="col-sm-6 image__prom">

                        <div class="text__prom__student mt-auto mb-auto">
                            <h2 class="text-center">Here at iHomestay Canada we believe homestays are the perfect solution for international students. allows you to experience first hand other Canadian cultures, to live with fellow Canadian familys and of course improve your language skills.</h2>
                        </div>
                    </div>

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-main-->
        <!--SECONDARY FOOTER CONTENT
        =============================================================================================================-->
        <section id="ts-footer-secondary" data-bg-pattern="../assets/img/bg-pattern-dot.png" style="background-image: url('../assets/img/bg-pattern-dot.png');">
            <div class="container">
                <!--Copyright-->
                <img id="white-logo" src="../assets/logos/white.png">
                <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
                <!--Social Icons-->
                <div class="ts-footer-nav">
                <nav class="nav">
                        <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
                            <i class="fab fa-facebook-f" id="footer"></i>
                        </a>
                        <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
                            <i class="fab fa-twitter" id="footer"></i>
                        </a>
                        <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
                            <i class="fab fa-instagram" id="footer"></i>
                        </a>
                    </nav>
                </div>
                <!--end ts-footer-nav-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-secondary-->
    </footer>
    <!--end #ts-footer-->