<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';
session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM vouches WHERE email = '$usuario' AND id_v = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

if ($usuario != $row['email']) {
	 header("location: ../index.php");
}
}

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetTitle('Accepted');
$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(0,0,255);
$pdf->Text(15,40,'xeon@homebor.com','C');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',11);
$pdf->Text(160,40,$row['dates'],'C');
$pdf->SetFont('Arial','B',26);
$pdf->SetXY(0,70);
$pdf->Cell(0,0,'You have been accepted satisfactorily!',0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,90);
$pdf->Cell(0,0,"$row[user], we confirm your application to the '$row[h_name]' offer located at:",0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,95);
$pdf->Cell(0,0,"'$row[dir]'.",0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,115);
$pdf->Cell(0,0,"From your personal area you can contact your homestay, they wait for you on the day $row[arrive]",0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,120);
$pdf->Cell(0,0,"until $row[leave]. We hope you enjoy your stay.",0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->SetXY(0,130);
$pdf->Cell(0,0,'Continue on your profile.',0,0,'C');
$pdf->SetFont('Arial','',11);
$pdf->Image('../../assets/logos/homebor.png',35,140,-400);
$pdf->SetXY(10,195);
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(152,42,114);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,10,'CONTINUE ON YOUR PROFILE',1, 0, 'C', True, '../index.php');
$pdf->Output();
?>