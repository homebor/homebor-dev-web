<?php
require '../xeon.php';
session_start();
error_reporting(0);

$USER = $_SESSION['username'];

$queryUser = $link->query("SELECT * FROM users WHERE mail = '$USER' ");
$row_user = $queryUser->fetch_assoc();

$queryStudent = $link->query("SELECT * FROM pe_student WHERE mail_s = '$USER' ");
$row_student = $queryStudent->fetch_assoc();

if (strtolower($row_user['usert']) != 'student' || $row_user['status'] != "Activate")  echo json_encode(['redirect', '../logout.php']), exit;

$queryNotification = $link->query("SELECT * FROM notification WHERE user_i_mail = '$USER' AND title = 'Reservation Request'");
$row_notification = $queryNotification->fetch_assoc();

if ($row_student['status'] == 'Homestay Found') echo json_encode(['redirect', 'student_confirmed']), exit;


$queryHome1 = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_student[house1]' ");
$row_home1 = $queryHome1->fetch_assoc();
$queryHome2 = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_student[house2]' ");
$row_home2 = $queryHome2->fetch_assoc();
$queryHome3 = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_student[house3]' ");
$row_home3 = $queryHome3->fetch_assoc();
$queryHome4 = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_student[house4]' ");
$row_home4 = $queryHome4->fetch_assoc();
$queryHome5 = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_student[house5]' ");
$row_home5 = $queryHome5->fetch_assoc();

echo json_encode(array(
  'house1' => $row_home1,
  'house2' => $row_home2,
  'house3' => $row_home3,
  'house4' => $row_home4,
  'house5' => $row_home5,
));
