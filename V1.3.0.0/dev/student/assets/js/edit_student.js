// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? ELEMENTS
const $accommodation = D.querySelector(".price__accommodation").textContent;
const $nearToSchool = D.querySelector("#accomm-near").value;
const $accomUrgent = D.querySelector("#accomm-urgent").value;
const $summerFee = D.querySelector("#summer-fee").value;
const $minorFee = D.querySelector("#minor-fee").value;
/* const $accomGuardianship = D.querySelector("#accomm-guardianship").value; */
const $bookingFee = D.querySelector(".price__b-fee").textContent;

let finishedSignature = false;

let totalPrice = 0;
let priceNearSchool = 0;
let priceAccomUrgent = 0;
let priceSummerFee = 0;
let priceMinorFee = 0;
let priceAccomGuardianship = 0;

// TODO NEAR TO SCHOOL
if ($nearToSchool === "Yes") {
  priceNearSchool = 35 * 5 * 4;
  D.querySelector("#container-price-a-near").classList.remove("d-none");
} else {
  D.querySelector("#container-price-a-near").classList.add("d-none");
  priceNearSchool = 0;
}

// TODO ACCOMMODATION URGENT
if ($accomUrgent === "Yes") {
  priceAccomUrgent = 150;
  D.querySelector("#container-price-a-urgent").classList.remove("d-none");
} else {
  priceAccomUrgent = 0;
  D.querySelector("#container-price-a-urgent").classList.add("d-none");
}

// TODO SUMMER FEE
if ($summerFee === "Yes") {
  priceSummerFee = 45 * 4;
  D.querySelector("#container-price-summer-fee").classList.remove("d-none");
} else {
  priceSummerFee = 0;
  D.querySelector("#container-price-summer-fee").classList.add("d-none");
}

// TODO MINOR FEE
if ($minorFee === "Yes") {
  priceMinorFee = 75 * 4;
  D.querySelector("#container-price-minor-fee").classList.remove("d-none");
} else {
  priceMinorFee = 0;
  D.querySelector("#container-price-minor-fee").classList.add("d-none");
}

// TODO GUARDIANSHIP
/* if ($accomGuardianship === "Yes") {
  priceAccomGuardianship = 550;
  D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
} else {
  priceAccomGuardianship = 0;
  D.querySelector("#container-price-a-guardianship").classList.add("d-none");
} */

const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);

  let finishedSignature = false;
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
$btnDescargar.onclick = () => {
  const enlace = document.createElement("a");
  // El título
  enlace.download = "Firma.png";
  // Convertir la imagen a Base64 y ponerlo en el enlace
  enlace.href = $canvas.toDataURL();
  // Hacer click en él
  enlace.click();
};

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});
["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
  });
});
["mouseup", "mousedown"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    finishedSignature = true;
  });
});

// ! EVENTS

D.addEventListener("click", (e) => {
  if (e.target.matches("#btn_invoice")) D.querySelector("#table_invoice").classList.toggle("d-none");
  if (e.target.id === "form__btn" && finishedSignature == true) {
    const enlace = document.createElement("a");
    // Convertir la imagen a Base64 y ponerlo en el enlace
    enlace.href = $canvas.toDataURL();

    document.querySelector("#signature-canvas").value = enlace.href;
    console.log(document.querySelector("#signature-canvas").value);
  }
});

D.addEventListener("DOMContentLoaded", (e) => {
  // TODO NEAR TO SCHOOL
  if ($nearToSchool === "Yes") {
    priceNearSchool = 35 * 5 * 4;
    D.querySelector("#container-price-a-near").classList.remove("d-none");
  } else {
    D.querySelector("#container-price-a-near").classList.add("d-none");
    priceNearSchool = 0;
  }

  // TODO ACCOMMODATION URGENT
  if ($accomUrgent === "Yes") {
    priceAccomUrgent = 150;
    D.querySelector("#container-price-a-urgent").classList.remove("d-none");
  } else {
    priceAccomUrgent = 0;
    D.querySelector("#container-price-a-urgent").classList.add("d-none");
  }

  // TODO SUMMER FEE
  if ($summerFee === "Yes") {
    priceSummerFee = 45 * 4;
    D.querySelector("#container-price-summer-fee").classList.remove("d-none");
  } else {
    priceSummerFee = 0;
    D.querySelector("#container-price-summer-fee").classList.add("d-none");
  }

  // TODO MINOR FEE
  if ($minorFee === "Yes") {
    priceMinorFee = 75 * 4;
    D.querySelector("#container-price-minor-fee").classList.remove("d-none");
  } else {
    priceMinorFee = 0;
    D.querySelector("#container-price-minor-fee").classList.add("d-none");
  }

  // TODO GUARDIANSHIP
  /* if ($accomGuardianship === "Yes") {
    priceAccomGuardianship = 550;
    D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
  } else {
    priceAccomGuardianship = 0;
    D.querySelector("#container-price-a-guardianship").classList.add("d-none");
  } */

  let totalPrice =
    parseFloat($accommodation) +
    parseFloat($bookingFee) +
    priceNearSchool +
    priceAccomUrgent +
    priceSummerFee +
    priceMinorFee;

  D.querySelector("#total__price").value = totalPrice;

  _Messages.quitMessage();
});
