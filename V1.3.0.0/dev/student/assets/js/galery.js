function previewImage_v() {
    var file = document.getElementById("file-v").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-v").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-v");
        var label_1_1 = document.getElementById("label-v-i");
        var label_ticket = document.getElementById("ticket");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';
        label_ticket.style.display = 'none';

    }
}

function previewImage_vi() {
    var file = document.getElementById("file-vi").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-vi").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-vi");
        var label_1_1 = document.getElementById("label-vi-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_vii() {
    var file = document.getElementById("file-vii").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-vii").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-vii");
        var label_1_1 = document.getElementById("label-vii-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_viii() {
    var file = document.getElementById("file-viii").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-viii").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-viii");
        var label_1_1 = document.getElementById("label-viii-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_ix() {
    var file = document.getElementById("file-ix").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-ix").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-ix");
        var label_1_1 = document.getElementById("label-ix-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_x() {
    var file = document.getElementById("file-x").files;

    if (file.length > 0) {
        var fileReader = new FileReader();
        var hid = document.getElementById("preview-x");
        hid.style.display = 'inline';

        fileReader.onload = function(event) {
            document.getElementById("preview-x").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-x");
        var label_1_1 = document.getElementById("label-x-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_xi() {
    var file = document.getElementById("file-xi").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-xi").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-xi");
        var label_1_1 = document.getElementById("label-xi-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}

function previewImage_xii() {
    var file = document.getElementById("file-xii").files;

    if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            document.getElementById("preview-xii").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-xii");
        var label_1_1 = document.getElementById("label-xii-i");

        label_1.style.display = 'none';
        label_1_1.style.display = 'inline';

    }
}