// TODO IMPORTACION
import Bedrooms from "./bedrooms.js";
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
const _Bedrooms = new Bedrooms();

const $modal = D.getElementById("modal-zoom-image");
const HOMESTAY_DETAILS = {
  // ? HOMESTAY
  _ID: D.getElementById("homestay-id").value,
  $name: D.getElementById("homestay-name"),
  $address: D.getElementById("homestay-address"),
  detailsHome: {
    $email: D.getElementById("email-home"),
    $fullName: D.getElementById("fullname-home"),
    $dateBirth: D.getElementById("date-birth-home"),
    $gender: D.getElementById("gender-home"),
    $phoneNumber: D.getElementById("phone-number-home"),
    $occupation: D.getElementById("occupation-home"),
    $backgroundCheck: D.getElementById("background-check-home"),
  },
  $description: D.getElementById("homestay-description"),
  quickInfo: {
    $rooms: D.getElementById("room-home"),
    $years: D.getElementById("y-experience-home"),
    $language: D.getElementById("backl-home"),
    $pets: D.getElementById("pet-home"),
    $age: D.getElementById("a-preference-home"),
    $gender: D.getElementById("genpre-home"),
    $food: D.getElementById("foodservice-home"),
    $diet: D.getElementById("diet-home"),
  },
  $addressHome: D.getElementById("address-home"),
  $city: D.getElementById("city-home"),
  $state: D.getElementById("state-home"),
  $postalCode: D.getElementById("postal-code-home"),

  // ? COORDINATOR
  $logo: D.getElementById("agency-link"),
  $agencyName: D.getElementById("agency-name"),
  $agentName: D.getElementById("agent-name"),
  $agencyMail: D.getElementById("agency-mail"),
  $agencyNumber: D.getElementById("agency-number"),
  $agencyCoordinator: D.getElementById("agency-type"),
  $studentName: D.getElementById("name"),
  $studentMail: D.getElementById("email"),
  $studentMessage: D.getElementById("form-contact-message"),
};

// TODO FUNCIONES
class DetailHomestay {
  constructor(HOMESTAY = HOMESTAY_DETAILS) {
    // ? HOMESTAY
    this.ID = HOMESTAY._ID;
    this.$name = HOMESTAY.$name;
    this.$address = HOMESTAY.$address;
    this.details = HOMESTAY.detailsHome;
    this.$description = HOMESTAY.$description;
    this.quickInfo = HOMESTAY.quickInfo;
    this.$addressHome = HOMESTAY.$addressHome;
    this.$city = HOMESTAY.$city;
    this.$state = HOMESTAY.$state;
    this.$postalCode = HOMESTAY.$postalCode;
    // ? COORDINATOR
    this.$logo = HOMESTAY.$logo;
    this.$agencyName = HOMESTAY.$agencyName;
    this.$agentName = HOMESTAY.$agentName;
    this.$agencyMail = HOMESTAY.$agencyMail;
    this.$agencyNumber = HOMESTAY.$agencyNumber;
    this.$agencyCoordinator = HOMESTAY.$agencyCoordinator;
    this.$studentName = HOMESTAY.$studentName;
    this.$studentMail = HOMESTAY.$studentMail;
    this.$studentMessage = HOMESTAY.$studentMessage;

    // ? CONVERTIR FECHA A FORMATO (DAY MONTH OF YEAR)
    this.newDate = (date) => {
      const day = new Date(date).getDate();
      const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      const month = MONTHS[new Date(date).getMonth()];
      const year = new Date(date).getFullYear();
      return `${day} ${month} of ${year}`;
    };
    /** // ? GET DAYS left:
     * @param {Date} initialDate Initial date to count
     * @param {Date} finalDate Final date to count
     * @returns Remaining days
     */
    this.getYears = (initialDate, finalDate) => {
      let firstDate = initialDate.split("-");
      let firstDateResult = Date.UTC(firstDate[0], firstDate[1] - 1, firstDate[2]);
      let lastDateResult = Date.UTC(finalDate.getFullYear(), finalDate.getMonth() - 1, finalDate.getDate());
      const CALCULATION = lastDateResult - firstDateResult;
      const DAYS = Math.floor(CALCULATION / (1000 * 60 * 60 * 24));

      if (DAYS >= 365) return `${(DAYS / 365).toFixed(0)} Years`;
      else return `${DAYS} Days`;
    };
  }

  // ! REQUERIR DETALLES DE LA CASA
  async homestayDetails() {
    // * OBTENIENDO DATOS
    const DATA = new FormData();
    DATA.set("homestay_id", this.ID);
    const OPTIONS = {
      method: "POST",
      headers: { "Content-type": "application/json; charset=utf-8" },
      data: DATA,
    };
    const REQUEST = await axios("../helpers/homestay/homestay_data.php", OPTIONS);
    const RESPONSE = await REQUEST.data;
    this.renderHomestayDetails(RESPONSE);
  }
  // ! RENDERIZAR DETALLES DE LA CASA
  renderHomestayDetails(data) {
    // * RELLENANDO TITLE
    const $addressIcon = '<i class="fa fa-map-marker text-primary"></i>';
    this.$name.textContent = `${data.l_name_h.toUpperCase()}, ${data.name_h}`;
    this.$address.innerHTML = `${$addressIcon} ${data.dir} ${data.city} ${data.state}`;

    // * RELLENANDO DESCRIPTION
    this.$description.textContent = data.des;

    // * RELLENANDO DETAILS
    this.details.$email.textContent = data.mail_h;
    this.details.$fullName.textContent = `${data.name_h} ${data.l_name_h}`;
    this.details.$dateBirth.textContent = this.newDate(data.db);
    this.details.$gender.textContent = data.gender;
    this.details.$phoneNumber.textContent = data.num;
    if (data.occupation_m === "NULL") data.occupation_m = "Empty";
    this.details.$occupation.textContent = data.occupation_m;
    if (data.db_law === "NULL") data.db_law = "Empty";
    this.details.$backgroundCheck.textContent = this.newDate(data.db_law);

    // * RELLENANDO QUICK INFO
    this.quickInfo.$rooms.textContent = data.room;
    this.quickInfo.$years.textContent = this.getYears(data.y_service, new Date());
    this.quickInfo.$language.textContent = data.backl;
    this.quickInfo.$pets.textContent = data.pet;
    this.quickInfo.$age.textContent = data.a_pre;
    this.quickInfo.$gender.textContent = data.g_pre;
    this.quickInfo.$food.textContent = data.m_service;
    if (
      data.vegetarians.toLowerCase() === "yes" ||
      data.halal.toLowerCase() === "yes" ||
      data.kosher.toLowerCase() === "yes" ||
      data.lactose.toLowerCase() === "yes" ||
      data.gluten.toLowerCase() === "yes" ||
      data.pork.toLowerCase() === "yes"
    ) {
      this.quickInfo.$diet.textContent = "Yes";
    } else this.quickInfo.$diet.textContent = "No";

    // * RELLENANDO LOCATION
    this.$addressHome.textContent = data.dir;
    this.$city.textContent = data.city;
    this.$state.textContent = data.state;
    this.$postalCode.textContent = data.p_code;
  }

  // ! REQUERIR RELLENAR DETALLES DEL COORDINADOR DE LA CASA
  async homestayCoordinator() {
    // * OBTENIENDO DATOS
    const DATA = new FormData();
    DATA.set("homestay_id", this.ID);
    const OPTIONS = {
      method: "POST",
      headers: { "Content-type": "application/json; charset=utf-8" },
      data: DATA,
    };
    const REQUEST = await axios("../helpers/agents/coordinator_data.php", OPTIONS);
    const RESPONSE = await REQUEST.data;
    this.renderHomestayCoordinator(RESPONSE);
  }
  // ! RENDERIZAR DETALLES DEl COORDINADOR
  renderHomestayCoordinator(data) {
    // * RELLENANDO COORDINATOR
    if (data.case === 1) this.$agencyMail.innerHTML += "sebi@homebor.com";
    else if (data.case === 2) {
      this.$agencyNumber.classList.remove("d-none");
      this.$agencyCoordinator.classList.remove("d-none");

      this.$agencyName.textContent = data.agencyName;
      this.$agencyMail.innerHTML += data.agentMail;
      this.$agencyNumber.innerHTML += data.agentNum;
      this.$agencyCoordinator.innerHTML += data.agentType;
    } else {
      if (data.agentFullname) this.$agentName.classList.remove("d-none");
      this.$agencyCoordinator.classList.remove("d-none");
      this.$agencyNumber.classList.remove("d-none");

      this.$agencyName.textContent = data.agencyName;
      this.$agentName.innerHTML += data.agentFullname;
      this.$agencyMail.innerHTML += data.agentMail;
      this.$agencyNumber.innerHTML += data.agentNum;
      this.$agencyCoordinator.innerHTML += data.agentType;
    }

    this.$logo.href = data.agentHref;
    this.$logo.dataset.bgImage = data.agentPhoto;
    this.$logo.style.setProperty("background-image", "url(" + data.agentPhoto + ")");

    this.$studentName.placeholder = data.studentFullname;
    this.$studentMail.placeholder = data.studentMail;
    this.$studentMessage.placeholder += data.studentMessage;
  }
}

/** // ? VERIFY IF THERE ARE REQUIRED ROOMS
 * @param {Element} begin Elemento padre de las habitaciones a evaluar
 */
const verifyRequiredRooms = async (container) => {
  const roomRequired = [];
  container.forEach((room) => {
    if (room.querySelector("#homestay-found") && !room.querySelector("#homestay-found").classList.contains("d-none")) {
      roomRequired.push(room);
    } else if (
      room.querySelector(".required-message") &&
      !room.querySelector(".required-message").classList.contains("d-none")
    ) {
      roomRequired.push(room);
    }
  });

  if (roomRequired[0]) {
    container.forEach((room) => {
      if (room !== roomRequired[0]) {
        if (room.querySelector("#reserve-required")) room.querySelector("#reserve-required").remove();
        if (room.querySelector("#reserve-limit")) room.querySelector("#reserve-limit").remove();
        if (room.querySelector("#reserve-occupied")) room.querySelector("#reserve-occupied").remove();
        if (room.querySelector(".btn-reserve")) room.querySelector(".btn-reserve").remove();
      }
    });
  }
};

// TODO INSTANCIA DE LA CLASE
const INSTANCE = new DetailHomestay();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  D.querySelectorAll("[data-bg-image]").forEach((img) => {
    if (!img.style.cssText) img.style.cssText = `background-image: url("${img.dataset.bgImage}")`;
  });

  $(".owl-carousel").owlCarousel({
    loop: true,
    center: true,
    autoWidth: true,
  });

  INSTANCE.homestayDetails();
  INSTANCE.homestayCoordinator();
  await _Bedrooms.homestayRooms();
  await verifyRequiredRooms(D.querySelectorAll(".room-card"));

  _Messages.quitMessage();
});

// ! Click
D.addEventListener("click", (e) => {
  if (e.target.matches(".zoom-image")) {
    $modal.classList.toggle("show");

    if ($modal.classList.contains("show")) {
      $modal.querySelector("img").src = e.target.parentElement.querySelector("img").src;
    } else $modal.querySelector("img").src = "";
  }
  if (e.target.matches("#modal-zoom-image") || e.target.matches("#modal-zoom-image span")) {
    $modal.classList.remove("show");
    $modal.querySelector("img").src = "";
  }
});

// ! Error
W.addEventListener(
  "error",
  (e) => {
    if (e.target.matches("[data-empty-img]")) e.target.src = e.target.dataset.emptyImg;
  },
  true
);
