export default ((D, W) => {
  // TODO FUNCIONES Y/O CLASES
  function showHeaderMenu(btn) {
    btn.classList.toggle("active");
    btn.classList.contains("active")
      ? D.querySelector(".header-links").classList.add("show")
      : D.querySelector(".header-links").classList.remove("show");
  }

  function dropDownSize() {
    D.querySelectorAll(".link-dropdown").forEach((menu) => {
      const menuHeight = W.getComputedStyle(menu.lastElementChild).getPropertyValue("height");
      menu.lastElementChild.style.marginTop = `-${menuHeight}`;
    });
  }

  // TODO EVENTOS
  // ! DOMContentLoaded
  D.addEventListener("DOMContentLoaded", (e) => (W.innerWidth < 768 ? dropDownSize() : {}));

  // ! CLICK
  D.addEventListener("click", (e) => {
    if (e.target.matches(".link-dropdown > div")) e.target.parentElement.classList.toggle("active");
    if (e.target.matches("#show-menu") || e.target.matches("#hide-menu")) showHeaderMenu(e.target);
  });

  // ! RESIZE
  W.addEventListener("resize", (e) => (W.innerWidth > 992 ? W.location.reload() : {}));
})(document, window);
