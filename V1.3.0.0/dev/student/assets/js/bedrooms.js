// TODO VARIABLES
const D = document;
const W = window;

// ? HOMESTAY ID
const _ID = D.getElementById("homestay-id").value;
const HOMESTAY_DETAILS = {
  // ? HOMESTAY ROOMS
  $reserveTemplate: D.getElementById("reserve-template").content.cloneNode(true),
  $bedTemplate: D.getElementById("bed-template").content.cloneNode(true),
  $allReserves: D.getElementById("all-reserves"),
  $reserveBtn: D.getElementById("reserve-btn"),
};
// TODO FUNCIONES

export default class HomestayRooms {
  constructor(HOMESTAY = HOMESTAY_DETAILS) {
    // ? HOMESTAY ROOMS
    this.reserveTemplate$ = HOMESTAY.$reserveTemplate;
    this.bedTemplate$ = HOMESTAY.$bedTemplate;
    this.allReserves$ = HOMESTAY.$allReserves;
  }

  // ! REQUERIR DATOS DE LAS HABITACIONES E INSERTARLAS
  async homestayRooms() {
    try {
      // * OBTENIENDO DATOS
      const DATA = new FormData();
      DATA.set("homestay_id", _ID);
      const OPTIONS = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: DATA,
      };
      const REQUEST = await axios("../helpers/homestay/rooms_data.php", OPTIONS);
      const RESPONSE = await REQUEST.data;

      // ** FRAGMENTO HTML A INSERTAR
      const $fragment = D.createDocumentFragment();

      // * AGREGANDO RESERVAS EXISTENTES
      for (const key in RESPONSE) {
        // ** ¡DATA! **
        const ROOM = RESPONSE[key];

        if (
          (!ROOM.roomReservesInfo.allBeds.bed1[3] ||
            ROOM.roomReservesInfo.allBeds.bed1[3] === "Disabled" ||
            ROOM.roomReservesInfo.allBeds.bed1[3] === "NULL") &&
          (!ROOM.roomReservesInfo.allBeds.bed2[3] ||
            ROOM.roomReservesInfo.allBeds.bed2[3] === "Disabled" ||
            ROOM.roomReservesInfo.allBeds.bed2[3] === "NULL") &&
          (!ROOM.roomReservesInfo.allBeds.bed3[3] ||
            ROOM.roomReservesInfo.allBeds.bed3[3] === "Disabled" ||
            ROOM.roomReservesInfo.allBeds.bed3[3] === "NULL")
        ) {
          continue;
        }

        // ** TEMPLATES
        const $newReserve = this.reserveTemplate$.firstElementChild.cloneNode(true);

        // ** RELLENANDO HEADER
        await this.roomHeader(ROOM.roomTitle, $newReserve);

        // ** RELLENANDO PHOTO
        const image1 = ROOM.roomImage.image1;
        const image2 = ROOM.roomImage.image2;
        const image3 = ROOM.roomImage.image3;

        if ((!image2 || image2 === "NULL") && (!image3 || image3 === "NULL"))
          this.roomPhoto(false, [image1], $newReserve);
        else this.roomPhoto(ROOM, [image1, image2, image3], $newReserve);

        // ** RELLENANDO FEATURES
        this.roomFeatures(ROOM.roomFeatures, $newReserve);

        await this.roomVerify(ROOM.roomTitle, ROOM.myReservesInfo, ROOM.roomReservesInfo, $newReserve);

        // ** RELLENANDO FOOTER
        if (
          ROOM.myReservesInfo.myRequest ||
          (ROOM.myReservesInfo.myRequestsLimit && ROOM.myReservesInfo.myRequestsLimit[0]) ||
          ROOM.myReservesInfo.homestayFound
        ) {
          await this.denyReservation(ROOM.myReservesInfo, ROOM.roomReservesInfo, $newReserve);
        }

        // ** AGREGAR RESERVA
        $fragment.appendChild($newReserve);
      }

      // * AGREGAR TODAS LAS RESERVAS
      this.allReserves$.appendChild($fragment);
    } catch (error) {
      console.error("Ha ocurrido un error:", error);
    }
  }

  // ! RELLENAR HEADER DE LA HABITACIÓN
  roomHeader(header, reserveTemplate) {
    return new Promise((resolve, reject) => {
      const $roomHeader = reserveTemplate.querySelector("#reserve-header");
      const $roomName = reserveTemplate.querySelector("#reserve-header h3");
      const $roomPrice = reserveTemplate.querySelector("#reserve-header p");

      $roomHeader.style.background = header.color;
      $roomHeader.parentElement.parentElement.dataset.roomColor = header.color;
      $roomName.textContent = header.name;
      $roomPrice.textContent = `CAD$ ${parseInt(header.priceHomestay) + parseInt(header.priceAgent)}`;

      resolve(true);
    });
  }
  // ! RELLENAR IMÁGENES
  roomPhoto(roomData, images, reserveTemplate) {
    if (roomData) {
      const $fragment = D.createDocumentFragment();

      const $carouselContainer = D.createElement("div");
      $carouselContainer.id = roomData.roomTitle.name.replace(" ", "-");
      $carouselContainer.className = "w-100 carousel slide";
      $carouselContainer.dataset.ride = "carousel";

      const $indicators = D.createElement("ol");
      $indicators.className = "carousel-indicators";

      const $indicator = D.createElement("li");
      $indicator.dataset.target = `#${$carouselContainer.id}`;

      const $imagesContainer = D.createElement("div");
      $imagesContainer.className = "w-100 carousel-inner";

      const $btnPrevious = D.createElement("a");
      $btnPrevious.className = "carousel-control-prev";
      $btnPrevious.dataset.slide = "prev";
      $btnPrevious.setAttribute("role", "button");
      $btnPrevious.href = `#${$carouselContainer.id}`;

      const $btnNext = D.createElement("a");
      $btnNext.className = "carousel-control-next";
      $btnNext.dataset.slide = "next";
      $btnNext.setAttribute("role", "button");
      $btnNext.href = `#${$carouselContainer.id}`;

      images.forEach((image, i) => {
        if (image && image !== "NULL") {
          $indicator.dataset.slideTo = i;
          $indicators.appendChild($indicator.cloneNode(true));
          $indicators.firstElementChild.className = "active";

          const $imageItem = D.createElement("div");
          $imageItem.className = "w-100 carousel-item";
          const $image = D.createElement("img");
          $image.src = "../" + image;
          $image.className = "w-100 img-room";
          $imageItem.appendChild($image);
          $imagesContainer.appendChild($imageItem);
          $imagesContainer.firstElementChild.classList.add("active");

          $btnPrevious.innerHTML = `<span class="carousel-control-prev-icon mr-5" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>`;
          $btnNext.innerHTML = `<span class="carousel-control-next-icon ml-5" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>`;

          $carouselContainer.appendChild($indicators);
          $carouselContainer.appendChild($imagesContainer);
          $carouselContainer.appendChild($btnPrevious);
          $carouselContainer.appendChild($btnNext);

          $fragment.appendChild($carouselContainer);
        }
      });

      return reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
    } else {
      const $imageRoom = reserveTemplate.querySelector("#single-image-room");
      $imageRoom.classList.remove("d-none");
      if (images[0] && images[0] !== "NULL") $imageRoom.src = "../" + images[0];
      else $imageRoom.src = "../assets/img/homestay_home2.png";
    }
  }
  // ! RELLENAR CARACTERÍSTICAS
  roomFeatures(features, reserveTemplate) {
    const $typeRoom = reserveTemplate.querySelector("#type-room");
    const $roomFoodService = reserveTemplate.querySelector("#food-service");

    $typeRoom.textContent = features.type;
    $roomFoodService.textContent = features.food;
  }
  // ! VERIFICAR DISPONIBILIDAD DE LA HABITACIÓN
  roomVerify(room, userReservesInfo, roomReservesInfo, reserveTemplate) {
    return new Promise((resolve, reject) => {
      if (!userReservesInfo || !roomReservesInfo) return console.error("No hay datos a verificar");
      if (!reserveTemplate) return console.error("Se necesita una plantilla");

      const $newBed = this.bedTemplate$.cloneNode(true);
      const { name, color, priceHomestay, priceAgent } = room;
      const { myReserveDate, homestayFound } = userReservesInfo;
      const { allBeds, allBedsReserved } = roomReservesInfo;

      // * CONVERTIR FECHA DE USUARIO A UNIX
      const my_FD_Unix = new Date(myReserveDate[0]).getTime();
      const my_LD_Unix = new Date(myReserveDate[1]).getTime();

      // * TODAS LAS FECHAS DE RESERVAS CONVERTIDAS EN UNIX
      const allReservesDateUnix = [];

      // * CONVERTIR TODAS LAS FECHAS DE RESERVAS A UNIX
      if (allBedsReserved && allBedsReserved instanceof Array) {
        allBedsReserved.forEach((bed) => {
          console.log(bed, homestayFound);
          // ** VERIFICAR QUE LA CAMA NO SEA LA QUE EL USUARIO YA TIENE RESERVADA
          if (bed[0] === homestayFound.title) return;

          const bedLetter = bed[1];
          const FD_Unix = new Date(bed[2]).getTime();
          const LD_Unix = new Date(bed[3]).getTime();

          allReservesDateUnix.push([bedLetter, FD_Unix, LD_Unix]);
        });
      }

      // * COINCIDENCIAS ENCONTRADAS
      const matchesFound = [];

      // * COMPARAR FECHAS DE LAS RESERVAS CON LAS DEL USUARIO
      allReservesDateUnix.forEach((arr, i) => {
        const FD_Unix = arr[1];
        const LD_Unix = arr[2];

        if (FD_Unix >= my_FD_Unix && LD_Unix <= my_LD_Unix) matchesFound.push([...arr, "CASE 1"]);
        else if (FD_Unix < my_FD_Unix && LD_Unix > my_LD_Unix) matchesFound.push([...arr, "CASE 2"]);
        else if (FD_Unix > my_FD_Unix && FD_Unix < my_LD_Unix) matchesFound.push([...arr, "CASE 3"]);
        else if (LD_Unix > my_FD_Unix && LD_Unix < my_LD_Unix) matchesFound.push([...arr, "CASE 4"]);
      });
      // ***** LOG IMPORTANTE *****
      // console.log(name, my_FD_Unix + "-" + my_LD_Unix, matchesFound);
      // ***** LOG IMPORTANTE *****

      if (matchesFound.length > 0) {
        const allFirstDays = [];
        matchesFound.forEach((match, i) => allFirstDays.push(match[1]));
        const minDate = new Date(Math.min(...allFirstDays));
        const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        reserveTemplate.querySelector(`#reserve-occupied`).classList.remove("d-none");
        reserveTemplate.querySelector(`#reserve-occupied p`).innerHTML = `
        Occupied Room up to <br>
        ${minDate.getDate()} ${months[minDate.getMonth()]} ${minDate.getFullYear()}
      `;
      }

      if (allReservesDateUnix) {
        // * CAMAS DISPONIBLES
        const beds = new Set();

        for (const key in allBeds) {
          const BED = allBeds[key];

          if (BED[1] !== "NULL" && BED[3] !== "Disabled" && BED[3] !== "NULL") {
            BED.forEach((data, i) => {
              if (i === 2) {
                const matches = [];
                matchesFound.forEach((match) => matches.push(match[0]));
                matches.includes(data) ? {} : beds.add(BED);
              }
            });
          }
        }

        if (beds.size) {
          reserveTemplate.querySelector(`#reserve-occupied`).remove();
        } else reserveTemplate.querySelector(`#reserve-btn`).remove();

        this.bedsAvailable([...beds], color, $newBed, reserveTemplate);
      } else {
        const beds = [];

        Object.values(allBeds).forEach((bed) => (bed[3] !== "Disabled" ? beds.push(bed) : {}));
        this.bedsAvailable(beds, color, $newBed, reserveTemplate);
      }

      resolve(true);
    });
  }
  // ! AGREGAR A LA HABITACION LAS CAMAS DISPONIBLES
  bedsAvailable(beds, roomColor, bedTemplate, reserveTemplate) {
    const $fragment = D.createDocumentFragment();
    const $bedsContainer = reserveTemplate.querySelector("#beds-container");
    reserveTemplate.querySelector(".btn-reserve").dataset.color = roomColor;

    if (beds && beds instanceof Array) {
      beds.forEach((bed) => {
        if (bed[1] === "Bunk-bed") bed[1] = "Bunk";
        const TYPE = bed[1];
        let BED;
        if (bed[2] === "A") BED = "Bed 1";
        if (bed[2] === "B") BED = "Bed 2";
        if (bed[2] === "C") BED = "Bed 3";
        const STATUS = bed[3];

        if (STATUS === "Available" && TYPE !== "NULL") {
          bedTemplate.querySelector("#bed-type").textContent = TYPE;
          bedTemplate.querySelector("#bed-letter").textContent = BED;
          bedTemplate.querySelector("#bed").setAttribute("for", bed[0] + bed[2]);
          bedTemplate.querySelector("#bed input").id = bed[0] + bed[2];

          const $clone = D.importNode(bedTemplate, true);
          $fragment.appendChild($clone);
        }
      });
      $bedsContainer.appendChild($fragment);

      // * IMAGEN SI HAY 1 SOLA CAMA
      if ($bedsContainer.children.length < 3) {
        $bedsContainer.querySelector("#students-img").classList.remove("d-none");
        $bedsContainer.appendChild($bedsContainer.querySelector("#students-img"));
      }
      // * IMAGEN SI NO HAY CAMAS
    } else {
      $bedsContainer.querySelector("#students-img").classList.remove("d-none");
    }
  }
  // ! DENEGAR RESERVACIÓN
  async denyReservation(reserveData, allReservesData, reserveTemplate) {
    return new Promise((resolve, reject) => {
      const myRequest = reserveData.myRequest;
      const myRequestsLimit = reserveData.myRequestsLimit;
      const homestayFound = reserveData.homestayFound;

      reserveTemplate.querySelector(".btn-reserve").classList.replace("d-flex", "d-none");

      // * ROOM
      if (homestayFound) {
        let roomName;
        if (homestayFound.color === "#232159") roomName = "Room 1";
        if (homestayFound.color === "#982A72") roomName = "Room 2";
        if (homestayFound.color === "#394893") roomName = "Room 3";
        if (homestayFound.color === "#A54483") roomName = "Room 4";
        if (homestayFound.color === "#5D418D") roomName = "Room 5";
        if (homestayFound.color === "#392B84") roomName = "Room 6";
        if (homestayFound.color === "#B15391") roomName = "Room 7";
        if (homestayFound.color === "#4F177D") roomName = "Room 8";
        if (
          D.getElementById("email-home").textContent === homestayFound.email &&
          roomName === reserveTemplate.querySelector("h3").textContent
        ) {
          reserveTemplate.querySelector("#homestay-found").classList.remove("d-none");
          let bedInfo = [];
          if (homestayFound.bed === "A") bedInfo.push(allReservesData.allBeds["bed1"][1], "Bed 1");
          if (homestayFound.bed === "B") bedInfo.push(allReservesData.allBeds["bed2"][1], "Bed 2");
          if (homestayFound.bed === "C") bedInfo.push(allReservesData.allBeds["bed3"][1], "Bed 3");
          if (bedInfo[0] == "Bunk-bed") bedInfo[0] = "Bunk";
          let top;
          if (bedInfo[1] === "Bed 1") top = "0rem";
          if (bedInfo[1] === "Bed 2") top = "7.5rem";
          if (bedInfo[1] === "Bed 3") top = "15.5rem";
          reserveTemplate.querySelector("#beds-container h3").style.top = top;
          reserveTemplate.querySelector("#beds-container h3").style.fontSize = "95%";
          reserveTemplate.querySelector("#beds-container h3").textContent = "Your Bed Reserved";
          reserveTemplate.querySelectorAll("#bed").forEach((bed) => {
            if (bed.querySelector("#bed-letter").textContent === bedInfo[1]) bed.classList.add("checked");
            bed.querySelector("input").remove();
          });
          if (reserveTemplate.querySelector(".occupied-message")) {
            reserveTemplate.querySelector(".occupied-message").remove();
          }
        } else {
          if (reserveTemplate.querySelector("#beds-container")) {
            reserveTemplate.querySelector("#beds-container").remove();
          }
          if (reserveTemplate.querySelector(".reserved-message")) {
            reserveTemplate.querySelector(".reserved-message").remove();
          }
          if (reserveTemplate.querySelector(".required-message")) {
            reserveTemplate.querySelector(".required-message").remove();
          }
          if (reserveTemplate.querySelector(".limit-message")) {
            reserveTemplate.querySelector(".limit-message").remove();
          }
          if (reserveTemplate.querySelector(".occupied-message")) {
            reserveTemplate.querySelector(".occupied-message").remove();
          }
          if (reserveTemplate.querySelector(".btn-reserve")) {
            reserveTemplate.querySelector(".btn-reserve").remove();
          }
        }
      } else if (myRequestsLimit) {
        const roomRequired = [];
        if (myRequestsLimit[1].includes(D.querySelector("#email-home").textContent)) {
          roomRequired.push(...myRequestsLimit[1]);
        } else if (myRequestsLimit[2].includes(D.querySelector("#email-home").textContent)) {
          roomRequired.push(...myRequestsLimit[2]);
        } else if (myRequestsLimit[3].includes(D.querySelector("#email-home").textContent)) {
          roomRequired.push(...myRequestsLimit[3]);
        }

        if (reserveTemplate.dataset.roomColor === roomRequired[2]) {
          reserveTemplate.querySelector("#reserve-required").classList.remove("d-none");

          // ELIMINAR CAMAS QUE NO REQUERISTE
          reserveTemplate.querySelectorAll("#beds-container #bed").forEach((bed) => {
            if (bed.getAttribute("for")[bed.getAttribute("for").length - 1] === roomRequired[1]) {
              bed.classList.add("checked");
              bed.querySelector("input").remove();
              let top;
              if (bed.querySelector("#bed-letter").textContent === "Bed 1") top = "0rem";
              if (bed.querySelector("#bed-letter").textContent === "Bed 2") top = "7.5rem";
              if (bed.querySelector("#bed-letter").textContent === "Bed 3") top = "15.5rem";
              bed.parentElement.querySelector("h3").style.top = top;
              bed.parentElement.querySelector("h3").style.fontSize = "95%";
              bed.parentElement.querySelector("h3").textContent = "Your Bed Required";
            }
          });
        } else reserveTemplate.querySelector("#beds-container").remove();

        if (myRequest && myRequest === reserveTemplate.querySelector("h3").textContent) {
          reserveTemplate.querySelector(".required-message").classList.remove("d-none");
        } else reserveTemplate.querySelector(".required-message").remove();
        reserveTemplate.querySelector(".btn-reserve").remove();
        reserveTemplate.querySelector("#reserve-limit").remove();
        reserveTemplate.querySelector("#homestay-found").remove();
        if (reserveTemplate.querySelector("#reserve-occupied")) {
          reserveTemplate.querySelector("#reserve-occupied").remove();
        }
      } else if (myRequest) {
        if (myRequest === reserveTemplate.querySelector("h3").textContent) {
          reserveTemplate.querySelector("#reserve-required").classList.remove("d-none");
        } else {
          reserveTemplate.querySelector(".btn-reserve").remove();
          reserveTemplate.querySelector("#reserve-limit").remove();
          reserveTemplate.querySelector("#homestay-found").remove();
          if (reserveTemplate.querySelector("#reserve-occupied")) {
            reserveTemplate.querySelector("#reserve-occupied").remove();
          }
        }
      } else {
        reserveTemplate.querySelector("#reserve-occupied").classList.remove("d-none");
      }
      resolve(true);
    });
  }
  // ! RESERVAR HABITACIÓN
  async reserveRoom(btn, bed) {
    let description = bed;

    const DATA_1 = new FormData();
    DATA_1.set("id", D.getElementById("homestay-id").value);

    const OPTIONS_1 = {
      method: "POST",
      headers: { "Content-type": "application/json; charset=utf-8" },
      data: DATA_1,
    };
    const REQUEST_1 = await axios("data_for_reservation.php", OPTIONS_1);
    const RESPONSE_1 = await REQUEST_1.data;

    if (RESPONSE_1 === "Limit") window.location.reload();
    else if (RESPONSE_1 === "Required") window.location.reload();
    else {
      const DATA_2 = new FormData();
      DATA_2.set(btn.parentElement.dataset.color, true);
      DATA_2.set("mail", RESPONSE_1.homestayMail);
      DATA_2.set("date_i", RESPONSE_1.studentFirstDay);
      DATA_2.set("date_e", RESPONSE_1.studentLastDay);
      DATA_2.set("des", description);
      const OPTIONS_2 = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: DATA_2,
      };
      const REQUEST_2 = await axios("addNot.php", OPTIONS_2);

      window.location.reload();
    }
  }
}

// TODO INSTANCIA DE LA CLASE
const INSTANCE = new HomestayRooms();

// TODO EVENTOS
// ! Click
D.addEventListener("click", (e) => {
  if (e.target.matches("#reserve-btn")) {
    const INPUTS = e.target.parentElement.parentElement.parentElement.querySelectorAll("#beds-container input");

    INPUTS.forEach((input) => (input.checked ? INSTANCE.reserveRoom(e.target, input.id[input.id.length - 1]) : {}));
  }

  if (e.target.matches("#bed input")) {
    const $inputSelected = e.target;
    D.querySelectorAll("#bed input").forEach((input, i) => {
      const $bedsContainer = input.parentElement.parentElement;
      const $inputLabel = $inputSelected.parentElement;
      const $reserveContainer = $inputSelected.parentElement.parentElement.parentElement;

      if (input !== $inputSelected) input.checked = false;

      if ($inputSelected.checked) {
        input.parentElement.classList.remove("checked");
        $inputLabel.classList.add("checked");
        $reserveContainer.querySelector(".room-card #reserve-btn").disabled = false;

        if (!input.id.includes($inputSelected.id.slice(0, 6))) {
          input.parentElement.parentElement.parentElement.querySelector("#reserve-btn").disabled = true;
        }
      } else {
        $inputLabel.classList.remove("checked");
        $reserveContainer.querySelector(".room-card #reserve-btn").disabled = true;
      }
    });
  }
});
