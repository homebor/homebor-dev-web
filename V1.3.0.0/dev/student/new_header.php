<?php
$queryUser = $link->query("SELECT usert FROM users WHERE mail = '$usuario'");
$row_user = $queryUser->fetch_assoc();

if ($row_user['usert'] != 'student') header("location: ../logout.php");

?>


<link rel="stylesheet" href="assets/css/new_header.css">
<link rel="stylesheet" href="../assets/css/notifications.css">

<header class="m-0 p-0 header-container">
  <nav class="m-0 d-flex align-items-center justify-content-around header-navbar">
    <!-- // * LOGO -->
    <a href="index" class="header-logo">
      <figure class="m-0 p-0">
        <img src="../assets/logos/homebor.png" alt="ihomestay, homebor" class="w-100 h-auto">
      </figure>
    </a>

    <!-- // * LINKS -->
    <ul class="m-0 p-0 d-flex align-items-center justify-content-center header-links">
      <a href="student_info">You Profile</a>
      <a href="voucher.php">Voucher</a>
      <a href="edit_student">Configuration</a>
      <!-- <a href="#">Reports</a> -->

      <!-- // * NOTIFICATION BELL BUTTON -->
      <button id="bell-btn" class="d-none d-md-block bg-transparent shadow-none notification-btn" data-show="false">
        <img src="../assets/icon/notification 64.png" class="m-0 p-0" width="32" height="32">
        <!-- // * NEW NOTIFICATIONS BADGE -->
        <span id="new-notifications" class="new-notifications"></span>
      </button>
    </ul>



    <!-- // * NOTIFICATIONS CONTAINER  -->
    <ul id="notification-list" class="m-0 p-0 pt-5 pt-lg-0 d-none flex-column notification-list"
      data-total-notifications="You have 0 notification">
      <span class="d-lg-none close-notifications">X</span>
      <!-- ALL NOTIFICATIONS -->
    </ul>


    <!-- // * LOGOUT -->
    <a href="../logout" class="m-0 d-flex align-items-center justify-content-center header-logout">
      <span class="m-0 mr-2 p-0">Logout</span>
      <figure class="m-0 p-0">
        <img src="../assets/icon/logout2.png" alt="ihomestay, homebor" class="w-100 h-auto">
      </figure>
    </a>
  </nav>
</header>


<template id="notification-template">
  <!-- // * NOTIFICATION ITEM  -->
  <a href="#" data-id-note="" class="m-0 p-0 d-flex flex-column justify-content-start notification-item">
    <!-- // ** NOTIFICATION HEADER  -->
    <!-- <div class="p-0 m-0 py-md-0 py-4 d-flex w-100 align-items-center justify-content-between px-4 message-header">
                <h5 class="m-0 p-0" data-title>Message Title</h5>
                <small data-time-ago>3 days ago</small> -->
    <!-- <img src="../assets/svg/icon-trash.svg" title="Delete notification" alt="delete"> -->
    <!-- </div> -->



    <!-- // ** NOTIFICATION BODY  -->
    <div class="d-flex pl-4 align-items-center justify-content-start message-body">
      <img src="../assets/icon/home 64.png" class="mr-4" data-issuer-image>
      <img src="../assets/icon/home 64.png" class="m-0 p-0" data-image-1>
      <p class="text-left m-0 p-0 text-muted" data-message data-time-ago>
        <!-- Content and information of the notification -->
      </p>
    </div>



    <!-- // ** NOTIFICATION FOOTER  -->
    <div class="w-100 px-3 pb-2 d-flex align-items-center message-footer">
      <img src="../public_s/rexts@gmail.com/rex.jpg" alt="profile" data-image-2>
      <small class="w-100 m-0 p-0 text-center text-muted btn" data-student-date>
        <!-- 2022-09-15 / 2022-10-25 -->
      </small>
      <div class="m-0 p-0 d-flex align-items-center message-buttons">
        <button class="btn mr-2 confirm-btn" data-confirm-student>Confirm</button>
        <button class="btn reject-btn" data-reject-student>Reject</button>
      </div>
    </div>

  </a>
</template>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="assets/js/notifications.js" type="module"></script>