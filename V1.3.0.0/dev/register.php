<?php
error_reporting(0);?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/header/fonts/icomoon/style.css">

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/html/index.css">
  <link rel="stylesheet" href="assets/css/header_style.css">

  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/html/register.css?ver=1.2">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <script src="assets/js/jquery-3.3.1.min.js"></script>

  <title>Homebor - Registrer</title>


  <script src="https://www.google.com/recaptcha/api.js" async defer></script>





</head>

<body>

  <!-- WRAPPER
    =================================================================================================================-->
  <div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>

    <div class="site-mobile-menu-body"></div>
  </div>



  <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

    <?php include 'header.php' ?>

  </header>
  <!--HEADER ===============================================================-->

  <main id="ts-main" class="register__back">

    <div class="card card__div-register mt-auto mb-auto">

      <div class="register__div">

        <div class="col-lg-12 register__div__col">

          <h2 class="title_login">Register and Work with Us!</h2>

          <form action="#" id="form__register" class="register__content">

            <div class="row fr_group">

              <div class="form__group form__group__name col-md-6" id="group__name">

                <div class="form__group__icon-input">
                  <label for="name" class="form__label">Name</label>

                  <label for="name" class="icon"><i class="icon icon__name"></i></label>
                  <input type="text" id="name" name="name" class="form__group-input name" placeholder="e.g. John"
                    autocomplete="off">

                  <p class="form__group__input-error">No special Characters or Numbers</p>

                </div>

              </div>

              <div class="form__group form__group__l_name col-md-6" id="group__l_name">

                <div class="form__group__icon-input">
                  <label for="l_name" class="form__label">Last Name</label>

                  <label for="l_name" class="icon"><i class="icon icon__name"></i></label>
                  <input type="text" id="l_name" name="l_name" class="form__group-input l_name" placeholder="e.g. Smith"
                    autocomplete="off">

                  <p class="form__group__input-error">No special Characters or Numbers</p>

                </div>

              </div>
            </div>

            <div class="fr_group">

              <div class="form__group form__group__email" id="group__email">

                <div class="form__group__icon-input">
                  <label for="email" class="form__label">Mail</label>

                  <label for="email" class="icon"><i class="icon icon__email"></i></label>
                  <input type="text" id="email" name="email" class="form__group-input email" placeholder="e.g. John"
                    autocomplete="off">

                  <p class="form__group__input-error">Enter a valid Email</p>

                </div>

              </div>
            </div>

            <div class="row fr_group">

              <div class="form__group form__group__pass col-md-6" id="group__pass">

                <div class="form__group__icon-input">
                  <label for="pass" class="form__label">Password</label>

                  <label for="pass" class="icon"><i class="icon icon__pass"></i></label>
                  <input type="password" id="pass" name="pass" class="form__group-input pass" placeholder="Password"
                    autocomplete="off">
                  <button type="button" class="fa fa-eye mr-2 btn__eye" id="btn__eye"></button>

                  <p class="form__group__input-error pass_input">Numbers, Uppercase, Lowercase, and Special Characters
                  </p>

                </div>

              </div>



              <div class="form__group form__group__pass2 col-md-6" id="group__pass2">

                <div class="form__group__icon-input">
                  <label for="pass2" class="form__label">Repeat Password</label>

                  <label for="pass2" class="icon"><i class="icon icon__pass"></i></label>
                  <input type="password" id="pass2" name="pass2" class="form__group-input pass2"
                    placeholder="Repeat Password" autocomplete="off">
                  <button type="button" class="fa fa-eye mr-2 btn__eye" id="btn__eye2"></button>

                  <p class="form__group__input-error pass_input">Passwords do not match</p>
                </div>

              </div>

            </div>

            <div class="fr_group registeras">

              <div class="form__group form__group__opt" id="group__opt">

                <div class="form__group__icon-input">
                  <label for="opt" class="form__label">Register As</label>

                  <label for="opt" class="icon"><i class="icon icon__opt"></i></label>
                  <select class="form__group-input custom-select my-2" id="opt" name="opt">
                    <option value="0" hidden="option">-- Select Option --</option>
                    <option value="1">Register as Student</option>
                    <option value="2">Register as Homestay</option>
                    <!--<option value="3">Register as Accomodation Provider</option>-->
                  </select>

                </div>

              </div>
            </div>

            <div class="fr_group registeras">

              <div class="form__group form__group__opt" id="group__opt">

                <div class="form__group__icon-input">
                  <label for="opt" class="form__label">Select your Homestay Provider</label>

                  <label for="provider" class="icon"><i class="icon icon__opt"></i></label>
                  <select class="form__group-input custom-select my-2" id="provider" name="provider">
                    <option value="NULL" hidden="option">-- Select Option --</option>
                    <option value="Homebor">Homebor</option>
                    <option value="iHomestay">iHomestay</option>
                    <!--<option value="3">Register as Accomodation Provider</option>-->
                  </select>

                </div>

              </div>
            </div>

            <div class="fr_group termins">

              <div class="form__group form__group__termins " id="group__termins">

                <div class="form__group__icon-input custom-checkbox">
                  <input type="checkbox" name="termins" class="custom-control-input" id="termins" autocomplete="off">
                  <label class="custom-control-label" for="termins">I Agree With <a href="privacy-policy"
                      class="btn-link" id="terms" target="_blank">Terms and Conditions</a></label>

                </div>

              </div>
            </div>


            <div class="not_robot__div mb-2">

              <div class="g-recaptcha" data-sitekey="6Lezyb0eAAAAAGpkYuR4gFdTOvcPIwKVOcEudhLT"></div>

            </div>
            <div class="div_btn_register" id="div_btn_register">
              <button type="submit" name="submit" id="btn_register" class="btn_register">Register</button>

            </div>

            <div class="message">
              <h6 id="message" class="form__group__input-error-message"></h6>
            </div>

          </form>

        </div>


      </div>

    </div>

  </main>

  <!--FOOTER ===============================================================-->
  <?php 
    include 'footer.php';
?>

  <!--end page-->



  <script src="assets/js/custom.js"></script>

  <script src="assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/header/js/jquery.sticky.js"></script>
  <script src="assets/js/mainHeader.js"></script>
  <script src="assets/js/validate_register.js?ver=1.2"></script>


</body>

</html>