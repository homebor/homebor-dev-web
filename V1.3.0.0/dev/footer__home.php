<?php
error_reporting(0);?>
         <!--MAIN FOOTER CONTENT
        =============================================================================================================-->
<footer id="ts-footer" style="margin-bottom:0;">
    <link rel="stylesheet" type="text/css" href="assets/css/footer.css?ver=1.0">

        <!--SECONDARY FOOTER CONTENT
            =========================================================================================================-->
        <section id="ts-footer-secondary" data-bg-pattern="assets/img/bg-pattern-dot.png" style="background-image: url('assets/img/bg-pattern-dot.png');">
            <div class="container">
                <!--Copyright-->
                <img id="white-logo" class="logo_f" src="assets/logos/white.png">
                
                <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
                <!--Social Icons-->
                <div class="ts-footer-nav">
                    <nav class="nav">
                        <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
                            <i class="fab fa-facebook-f" id="footer"></i>
                        </a>
                        <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
                            <i class="fab fa-twitter" id="footer"></i>
                        </a>
                        <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
                            <i class="fab fa-instagram" id="footer"></i>
                        </a>
                    </nav>
                </div>
                <!--end ts-footer-nav-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-secondary-->


    </footer>
    <!--end #ts-footer-->
