<?php
if ($_POST['homestay_id'] && $_POST['student_id'] && $_POST['room_color'] && $_POST['room_bed']) {
  require '../../xeon.php';

  session_start();
  error_reporting(0);

  $usuario = $_SESSION['username'];
  $ID_HOME = $_POST['homestay_id'];
  $ID_STUDENT = $_POST['student_id'];

  date_default_timezone_set("America/Toronto");
  $date = date('Y-m-d H:i:s');

  $queryAgentData = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario' ");
  $row_agent = $queryAgentData->fetch_assoc();

  $queryHomeData = $link->query("SELECT mail_h FROM pe_home WHERE id_home = '$ID_HOME' ");
  $row_home = $queryHomeData->fetch_assoc();

  $queryStudentData = $link->query("SELECT name_s, l_name_s, mail_s, firstd, lastd FROM pe_student WHERE id_student = '$ID_STUDENT' ");
  $row_student = $queryStudentData->fetch_assoc();

  $roomBed = null;
  $roomColor = $_POST['room_color'];

  if ($_POST['room_bed'] == 1) $roomBed = "A";
  if ($_POST['room_bed'] == 2) $roomBed = "B";
  if ($_POST['room_bed'] == 3) $roomBed = "C";

  if ($roomColor == "#232159") $bedRoom = 1;
  if ($roomColor == "#982A72") $bedRoom = 2;
  if ($roomColor == "#394893") $bedRoom = 3;
  if ($roomColor == "#A54483") $bedRoom = 4;
  if ($roomColor == "#5D418D") $bedRoom = 5;
  if ($roomColor == "#392B84") $bedRoom = 6;
  if ($roomColor == "#B15391") $bedRoom = 7;
  if ($roomColor == "#4F177D") $bedRoom = 8;

  // ? INSERT NOTIFICATION
  $queryNotification = $link->query("INSERT INTO notification 
  (`user_i`, 
  `user_i_l`, 
  `user_i_mail`, 
  `start`, 
  `end_`, 
  `bedrooms`, 
  `color`, 
  `user_r`, 
  `date_`, 
  `state`, 
  `confirmed`, 
  `title`, 
  `des`, 
  `report_s`, 
  `reserve_h`, 
  `status`, 
  `extend`) 
  VALUES ('$row_student[name_s]', 
  '$row_student[l_name_s]', 
  '$row_student[mail_s]', 
  '$row_student[firstd]', 
  '$row_student[lastd]', 
  '$bedRoom', 
  '$roomColor', 
  '$row_home[mail_h]', 
  '$date', 
  '0', 
  '0', 
  'Reservation Request', 
  '$roomBed', 
  'NULL', 
  '$row_home[mail_h]', 
  'NULL', 
  'NULL') ");


  // ? UPDATE STUDENT STATUS
  $queryStudent = $link->query("UPDATE pe_student SET status='Waiting Answer' WHERE id_student = '$ID_STUDENT' ");

  // ? INSERT WEB MASTER
  $queryWebmaster = $link->query("INSERT INTO webmaster 
  (`user`, 
  `activity`, 
  `dates`, 
  `edit_user`, 
  `id_m`, 
  `report_s`, 
  `reason`) 
  VALUES 
  ('$usuario', 
  'Send Request', 
  '$date', 
  '$row_student[mail_s]', 
  '$row_agent[id_m]', 
  'NULL', 
  NULL)
");

  if ($queryNotification == true && $queryStudent == true && $queryWebmaster == true) echo json_encode("ok");
}