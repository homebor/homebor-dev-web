<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$ID_STUDENT = $_POST['student_id'];

$queryStudent = $link->query("SELECT n_a FROM pe_student WHERE id_student = '$ID_STUDENT'");
$row_student = $queryStudent->fetch_assoc();

if ($row_student['n_a'] == "NULL" || $row_student['n_a'] == "0") echo json_encode(array('address' => "NULL")), exit;

$queryAcademy = $link->query("SELECT dir_a, city_a, state_a, p_code_a, name_a, photo_a FROM academy WHERE id_ac = '$row_student[n_a]' ");
$row_academy = $queryAcademy->fetch_assoc();

if ($row_academy) {
  $address = "$row_academy[dir_a], $row_academy[city_a], $row_academy[state_a], $row_academy[p_code_a]";
  echo json_encode(array(
    'address' =>  $address,
    'name' => $row_academy['name_a'],
    'photo' => "../$row_academy[photo_a]",
  ));
} else echo json_encode(array('address' => "NULL"));