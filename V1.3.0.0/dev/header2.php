<?php
error_reporting(0);?>
    <header id="ts-header" class="fixed-top">
        <link rel="stylesheet" type="text/css" href="assets/css/header.css">
        <nav id="ts-secondary-navigation" class="navbar p-0">
            <div class="container justify-content-end justify-content-sm-between">
                <div class="navbar-nav d-none d-sm-block">
                   <!-- FIRST HEADER WITH CONTACTS, EMAIL, SEARCH INPUT, CURENCY SELECT AND LANGUAGES SELECT-->
                   <!-- <span class="mr-4">
                            <i class="fa fa-phone-square mr-1"></i>
                            +1 123 456 789
                        </span>
                    <a href="#">
                        <i class="fa fa-envelope mr-1"></i>
                        hello@example.com
                    </a>
                </div>
                <div class="navbar-nav flex-row">
                    <input type="text" class="form-control p-2 border-left bg-transparent w-auto" placeholder="Search">
                    <select class="custom-select bg-transparent ts-text-small border-left" id="currency" name="currency">
                        <option value="1">GBP</option>
                        <option value="2">USD</option>
                        <option value="3">EUR</option>
                    </select>
                    <select class="custom-select bg-transparent ts-text-small border-left border-right" id="language" name="language">
                        <option value="1">EN</option>
                        <option value="2">FR</option>
                        <option value="3">DE</option>
                    </select>-->
                </div>
            </div>
        </nav>
        <!--PRIMARY NAVIGATION
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container">

                <!--Brand Logo-->
                <a class="navbar-brand" href="index.php">
                    <img src="assets/logos/page.png" alt="">
                </a>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav">
                        <!--Home (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="index.php" id="active">Home</a>
                        </li>
                        <!--end Home nav-item-->
                        <!--About Us (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#about" id="nav">About Us</a>
                        </li>
                        <!--end About Us nav-item-->

                        <!--Host (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#service" id="nav">Services</a>
                        </li>
                        <!--end Host nav-item-->


                        <!--HELP (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link mr-2" href="contact_us.php" id="nav">Contact Us</a>
                        </li>
                        <!--end HELP nav-item-->

                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="login.php" id="nav">Login</a>
                        </li>

                        <!--REGISTER (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="register.php" id="nav">Register</a>
                        </li>
                    </ul>
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->

    </header>
    <!--end Header-->