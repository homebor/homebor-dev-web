  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/ihomestay-header.css?ver=1.2.1">


  <nav
    class="col-12 col-xl-9 mx-auto d-flex align-items-center justify-content-around navbar navbar-expand-lg login-navbar"
    role="navigation">
    <a href="#" class="mt-5 m-lg-0 p-0 col-lg-2 d-none d-lg-block login-navbar-logo">
      <img src="assets/svg/ihomestay.svg" alt="homebor, ihomestay">
    </a>

    <ul class="p-0 pt-1 m-0 d-flex flex-column flex-lg-row align-items-center justify-content-center text-uppercase">
      <a href="https://ihomestaycanada.com/become-an-agent/" class="mt-4 mt-lg-0 px-2 login-link">Become an
        Agent</a>
      <a href="https://ihomestaycanada.com/host-a-student/" class="mt-4 mt-lg-0 px-2 login-link">Become a
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        Host
      </a>
      <a href="https://homebor.com/ihomestay-students-register" class="mt-4 mt-lg-0 px-2 login-link">
        Apply for accommodation
      </a>
      <a href="https://ihomestaycanada.com/contact-us/" class="mt-4 mt-lg-0 px-2 login-link">About Us</a>
      <a href="https://ihomestaycanada.com/contact-us/" class="mt-4 mt-lg-0 px-2 login-link">Payments</a>
    </ul>

    <a href="ihomestay_login" class="my-4 m-lg-0 p-0 pt-1 login-button">
      <svg viewBox="0 0 576 512" fill="#8102CEB8">
        <path
          d="M570.69,236.27,512,184.44V48a16,16,0,0,0-16-16H432a16,16,0,0,0-16,16V99.67L314.78,10.3C308.5,4.61,296.53,0,288,0s-20.46,4.61-26.74,10.3l-256,226A18.27,18.27,0,0,0,0,248.2a18.64,18.64,0,0,0,4.09,10.71L25.5,282.7a21.14,21.14,0,0,0,12,5.3,21.67,21.67,0,0,0,10.69-4.11l15.9-14V480a32,32,0,0,0,32,32H480a32,32,0,0,0,32-32V269.88l15.91,14A21.94,21.94,0,0,0,538.63,288a20.89,20.89,0,0,0,11.87-5.31l21.41-23.81A21.64,21.64,0,0,0,576,248.19,21,21,0,0,0,570.69,236.27ZM288,176a64,64,0,1,1-64,64A64,64,0,0,1,288,176ZM400,448H176a16,16,0,0,1-16-16,96,96,0,0,1,96-96h64a96,96,0,0,1,96,96A16,16,0,0,1,400,448Z" />
      </svg>
      LOGIN
    </a>

  </nav>

  <nav class="w-100 px-4 d-flex align-items-center justify-content-between d-lg-none bg-white" style="z-index: 999;">
    <a href="#" class="col-6 py-3">
      <img src="assets/icon/ihomestay-logo.png" width="100%" height="auto" alt="homebor, ihomestay">
    </a>

    <button class="d-lg-none icon-show-login-header">
      <img src="assets/icon/list.png" alt="menu, homebor">
    </button>
  </nav>
  <script src="assets/js/ihomestay-header.js?ver=1.2"></script>