<?php
include 'xeon.php';
error_reporting(0);

//TOTAL OF HOMESTAY
$sql = "SELECT * FROM `pe_home`"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

//TOTAL OF STUDENTS
$sql2 = "SELECT * FROM `pe_student`"; 
 
$connStatus2 = $link->query($sql2); 
 
$numberOfRows2 = mysqli_num_rows($connStatus2);

$a = 1;

$numberOfRows21 = $numberOfRows2 - $a;

//TOTAL OF Agents
$sql3 = "SELECT * FROM `users` WHERE usert='Admin' OR usert='Cordinator' OR usert='Agent' "; 
 
$connStatus3 = $link->query($sql3); 
 
$numberOfRows3 = mysqli_num_rows($connStatus3); 

//TOTAL OF Academies
$sql4 = "SELECT * FROM `academy`"; 
 
$connStatus4 = $link->query($sql4); 
 
$numberOfRows4 = mysqli_num_rows($connStatus4); 



$numberOfRows41 = $numberOfRows4 - $a;

//Agencies
$query="SELECT * FROM manager WHERE id_m != '0' ORDER BY id_m DESC LIMIT 1";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$sql5 = "SELECT * FROM `propertie_control` WHERE agency = '$row[id_m]'"; 
 
$connStatus5 = $link->query($sql5); 
 
$numberOfRows5 = mysqli_num_rows($connStatus5); 

//Agencies
$query2="SELECT * FROM manager WHERE id_m != '0' ORDER BY id_m DESC LIMIT 1,1";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$sql6 = "SELECT * FROM `propertie_control` WHERE agency = '$row[id_m]'"; 
 
$connStatus6 = $link->query($sql6); 
 
$numberOfRows6 = mysqli_num_rows($connStatus6); 

//Agencies
$query3="SELECT * FROM manager WHERE id_m != '0' ORDER BY id_m DESC LIMIT 2,1";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$sql7 = "SELECT * FROM `propertie_control` WHERE agency = '$row[id_m]'"; 
 
$connStatus7 = $link->query($sql7); 
 
$numberOfRows7 = mysqli_num_rows($connStatus7);

//Agencies
$query4="SELECT * FROM manager WHERE id_m != '0' ORDER BY id_m DESC LIMIT 3,1";
$resultado4=$link->query($query4);

$row4=$resultado4->fetch_assoc();

$sql8 = "SELECT * FROM `propertie_control` WHERE agency = '$row[id_m]'"; 
 
$connStatus8 = $link->query($sql8); 
 
$numberOfRows8 = mysqli_num_rows($connStatus8);

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ThemeStarz">

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/work_with_us.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Work With Us</title>

</head>

<body>

<div class="ts-page-wrapper" id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!--************ MAIN ***************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">

        <!--PAGE TITLE ******************************************************************************************-->
        <section id="page-title">
            <div class="container">
                <div class="ts-title">
                    <br>
                    <h1>Work With Us</h1>
                </div>
                <!--end ts-title-->
            </div>
            <!--end container-->
        </section>

        <!--DESCRIPTION *****************************************************************************************-->
        <section id="about-us-description">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <p class="h3">
                        We are reaching out to a small number of schools to offer our platform, at no cost, in exchange of only their feedback and ideas. 
                        </p>
                        <p class="mb-5">
                        It is extremely important for us to continue to build a product that add value to schools and family and creates the best matchmaking for students.
                        </p>
                        <a href="help.php?art=contact" class="btn btn-primary">Contact us</a>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4"></div>
                    <!--end col-md-4-->
                </div>
                <!--end row-->
                <hr class="my-5">
            </div>
            <!--end container-->
        </section>

                <!--TEAM
            =========================================================================================================-->
            <section id="about-us-team">
            <div class="container pb-5">
                <div class="row">
               <!--Person-->
          <div class="col-md-3">
              <div class="card ts-person ts-card">

                  <!--Image-->
                  <a href="#" class="card-img" data-bg-image="<?php echo "$row[photo]"?>">
                      <div class="ts-item__info-badge">
                          <span><?php echo "$numberOfRows5"?> Properties</span>
                      </div>
                  </a>

                  <!--Body-->
                  <div class="card-body">

                      <figure class="ts-item__info">
                          <h4><?php echo "$row[a_name]"?></h4>
                          <aside>
                              <i class="fa fa-map-marker mr-2"></i>
                              <?php echo "$row[city]"?>
                          </aside>
                      </figure>

                      <dl>
                          <dt>Phone</dt>
                          <dd><?php echo "$row[num]"?></dd>
                          <dt>Email</dt>
                          <dd><a href="#"><?php echo "$row[mail]"?></a></dd>
                      </dl>

                  </div>

              </div>
          </div>
          <!--end col-md-3-->

          <!--Person-->
          <div class="col-md-3">
              <div class="card ts-person ts-card">

                  <!--Image-->
                  <a href="#" class="card-img" data-bg-image="<?php echo "$row2[photo]"?>">
                      <div class="ts-item__info-badge">
                          <span><?php echo "$numberOfRows6"?> Properties</span>
                      </div>
                  </a>

                  <!--Body-->
                  <div class="card-body">

                      <figure class="ts-item__info">
                          <h4><?php echo "$row2[a_name]"?></h4>
                          <aside>
                              <i class="fa fa-map-marker mr-2"></i>
                              <?php echo "$row2[city]"?>
                          </aside>
                      </figure>

                      <dl>
                          <dt>Phone</dt>
                          <dd><?php echo "$row2[num]"?></dd>
                          <dt>Email</dt>
                          <dd><a href="#"><?php echo "$row2[mail]"?></a></dd>
                      </dl>

                  </div>

              </div>
          </div>
          <!--end col-md-3-->

          <!--Person-->
          <div class="col-md-3">
              <div class="card ts-person ts-card">

                  <!--Image-->
                  <a href="#" class="card-img" data-bg-image="<?php echo "$row3[photo]"?>">
                      <div class="ts-item__info-badge">
                          <span><?php echo "$numberOfRows7"?> Properties</span>
                      </div>
                  </a>

                  <!--Body-->
                  <div class="card-body">

                      <figure class="ts-item__info">
                          <h4><?php echo "$row3[a_name]"?></h4>
                          <aside>
                              <i class="fa fa-map-marker mr-2"></i>
                              <?php echo "$row3[city]"?>
                          </aside>
                      </figure>

                      <dl>
                          <dt>Phone</dt>
                          <dd><?php echo "$row3[num]"?></dd>
                          <dt>Email</dt>
                          <dd><a href="#"><?php echo "$row3[mail]"?></a></dd>
                      </dl>

                  </div>

              </div>
          </div>
          <!--end col-md-3-->

                    <!--Person-->
                    <div class="col-md-3">
              <div class="card ts-person ts-card">

                  <!--Image-->
                  <a href="#" class="card-img" data-bg-image="<?php echo "$row4[photo]"?>">
                      <div class="ts-item__info-badge">
                          <span><?php echo "$numberOfRows8"?> Properties</span>
                      </div>
                  </a>

                  <!--Body-->
                  <div class="card-body">

                      <figure class="ts-item__info">
                          <h4><?php echo "$row4[a_name]"?></h4>
                          <aside>
                              <i class="fa fa-map-marker mr-2"></i>
                              <?php echo "$row4[city]"?>
                          </aside>
                      </figure>

                      <dl>
                          <dt>Phone</dt>
                          <dd><?php echo "$row4[num]"?></dd>
                          <dt>Email</dt>
                          <dd><a href="#"><?php echo "$row4[mail]"?></a></dd>
                      </dl>

                  </div>

              </div>
          </div>
          <!--end col-md-3-->



                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

        <!--TESTIMONIALS ****************************************************************************************-->
        <section id="about-us-testimonials-carousel">
            <div class="bg-white text-center py-5">
                <div class="container">
                    <div class="offset-lg-2 col-lg-8">
                        <div class="owl-carousel" data-owl-items="1" data-owl-dots="1">

                            <div class="ts-slide">
                                <div class="ts-circle__sm" data-bg-image="assets/img/test1.jpeg"></div>
                                <h5 class="my-3">Minji.K from Korea</h5>
                                <p class="h5 font-weight-normal ts-text-color-light">
                                My Host was really friendly. I could get along with her and experience many things thanks to her. I felt like I was living in my house while I lived with her.
                                </p>
                            </div>
                            <!--end ts-slide-->

                            <div class="ts-slide">
                                <div class="ts-circle__sm" data-bg-image="assets/img/test2.jpeg"></div>
                                <h5 class="my-3">Matheus.J from Portugal</h5>
                                <p class="h5 font-weight-normal ts-text-color-light">
                                My stay at Rita and Michael's house has been great, cozy, the food very good, they have helped me in everything, we have talked during meals, the room very comfortable with Wifi and TV. It was my first time traveling alone and they made me feel at home.
                                </p>
                            </div>
                            <!--end ts-slide-->

                        </div>
                        <!--end owl-carousel-->
                    </div>
                    <!--end offset-lg-2-->
                </div>
                <!--end container-->
            </div>
            <!--end ts-block-->
        </section>


         <!--PRICING
            =========================================================================================================-->
        <section id="pricing">
            <div class="container">

                <!--Title-->
                <div class="ts-title text-center">
                    <h2>Affordable Prices</h2>
                </div>


                <div class="row no-gutters ts-cards-same-height">

                    <!--Price Box-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#dadada">
                                <h5 class="text-white" data-bg-color="#232159">Basic</h5>
                                <div class="ts-title">
                                    <h3 class="font-weight-normal">TBA</h3>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0 border-bottom-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 200 Properties</li>
                                    <li class="list-group-item">5 Agents Profile</li>
                                    <li class="list-group-item">100 Active Students Profiles</li>
                                    <li class="list-group-item">20 Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-outline-primary">Register Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                    <!--Price Box Promoted-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box ts-price-box__promoted">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#ED84C9">
                                <h5 class="text-white" data-bg-color="#982a72">Premium</h5>
                                <div class="ts-title text-white">
                                    <h3 class="font-weight-normal">
                                        TBA
                                    </h3>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 500 Properties</li>
                                    <li class="list-group-item">10 Agents Profiles</li>
                                    <li class="list-group-item">500 Active Students Profiles</li>
                                    <li class="list-group-item">50 Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-primary">Register Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                    <!--Price Box-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#dadada">
                                <h5 class="text-white" data-bg-color="#232159">Professional</h5>
                                <div class="ts-title">
                                    <h3 class="font-weight-normal">
                                        TBA
                                    </h3>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 1000 Properties</li>
                                    <li class="list-group-item">100 Agents Profile</li>
                                    <li class="list-group-item">1000 Active Students Profiles</li>
                                    <li class="list-group-item">100 / + Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-outline-primary">Register Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
      

        <!--PARTNERS ********************************************************************************************-->
        <section id="partners">
            <div class="ts-block py-4">
                <div class="container">
                    <!--block of logos-->
                    <div class="d-block d-md-flex justify-content-between align-items-center text-center ts-partners py-3">
                        <a href="#">
                            <img src="assets/img/logo-01.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-02.png" alt="">
                        </a>
                        <a href="index.php">
                            <img src="assets/logos/page.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-04.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-05.png" alt="">
                        </a>
                    </div>
                    <!--end logos-->
                </div>
                <!--end container-->
            </div>
        </section>

    </main>
    <!--end #ts-main-->

 <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/custom.js"></script>

</body>
</html>
