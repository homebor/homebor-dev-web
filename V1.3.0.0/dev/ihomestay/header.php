
<div class="container">
            <div class="row align-items-center position-relative">

                <div class="site-logo">
                    <a href="index.php" class="text-black"><img src="assets/logos/homebor.png" class="homebor__log" alt=""></a>
                </div>

                <div class="col-12">
                    <nav class="site-navigation text-right ml-auto " role="navigation">

                        <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                            <li><a href="index" class="nav-link "><p class="c-black" style="color: #000"> Home </p></a></li>
                            <li><a href="contact_us" class="nav-link"><p class="c-black" style="color: #000">About Us</p></a></li>
                            <li><a href="index#service" class="nav-link"><p class="c-black" style="color: #000"> Services </p></a></li>
                            <li><a href="index#ts-footer" class="nav-link"><p class="c-black" style="color: #000"> Contact Us </p></a></li>
                            <li><a href="help" class="nav-link"><p class="c-black" style="color: #000"> Help </p></a></li>

                            <!--<li class="has-children">
                                <a href="#about-section" class="nav-link c-black">About Us</a>
                                    
                                <ul class="dropdown arrow-top">
                                    <li><a href="#team-section" class="nav-link c-black">Team</a></li>
                                    <li><a href="#pricing-section" class="nav-link c-black">Pricing</a></li>
                                    <li><a href="#faq-section" class="nav-link c-black">FAQ</a></li>
                                    <li class="has-children">
                                        <a href="#">More Links</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Menu One</a></li>
                                            <li><a href="#">Menu Two</a></li>
                                            <li><a href="#">Menu Three</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>-->

                            
                            <li><a href="login" class="nav-link login btn_act"><p class="c-white" style="color: #fff"> Login </p></a></li>
                            <li><a href="register" class="nav-link register btn_act"><p class="c-white" style="color: #fff"> Register </p></a></li>
                        </ul>
                    </nav>

                </div>

                <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

            </div>
        </div>