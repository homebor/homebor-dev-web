<?php
// TODO WINYERSON
if ($_POST) {
  require '../../xeon.php';
  session_start();
  error_reporting(0);

  // TODO VARIABLES
  $homestayId = $_POST['id'];
  $usuario = $_SESSION['username'];

  // TODO VALIDACIONES
  if (!$link || !$homestayId || !$usuario) echo "Insuficientes parámetros", exit;

  // TODO CONSULTAS
  $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario' ");
  $row_student = $studentQuery->fetch_assoc();


  $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$homestayId' ");
  $row_homestay = $homestayQuery->fetch_assoc();


  $strNotification = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND title = 'Reservation Request'";
  $queryNotifications = $link->query($strNotification);
  $num_rows_notification = mysqli_num_rows($queryNotifications);

  if ($num_rows_notification === 3) echo json_encode("Limit"), exit;
  while ($value = mysqli_fetch_array($queryNotifications)) {
    if ($value['user_r'] === $row_homestay['mail_h']) echo json_encode("Required"), exit;
  }

  $jsonToSend = array(
    // * HOMESTAY DATA
    'homestayMail' => $row_homestay['mail_h'],
    // * STUDENT DATA
    'studentFirstDay' => $row_student['firstd'],
    'studentLastDay' => $row_student['lastd'],
  );

  echo json_encode($jsonToSend);
}
