<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';

define('Tag',chr(149));
define('Tag2',chr(155));

session_start();
$usuario = $_SESSION['username'];


if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM pe_student WHERE mail_s = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT pe_student.mail_s, events.*, pe_home.*, photo_home.* FROM events INNER JOIN pe_student ON pe_student.mail_s = '$id' AND pe_student.mail_s = events.mail_s INNER JOIN pe_home ON events.email = pe_home.mail_h INNER JOIN photo_home ON pe_home.id_home = photo_home.id_home";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$host = 'Im '.$row2['name_h'].' '.$row2['l_name_h'].' Im Homestay Host, i speak '.$row2['backl'].' and i have a few hobbies cook, movie';
$city = 'I really look forward to introducing my iHomestay guests to '.$row2['city'].' and the surrounding';

$plivingRoute = 'https://homebor.com/'.$row2['pliving'];
$parea1Route = 'https://homebor.com/'.$row2['parea1']; 
$phouseRoute = 'https://homebor.com/'.$row2['phome'];

if($row2['room_e'] == 'room1') {
    $proom1Route = 'https://homebor.com/'.$row2['proom1'];
    if($row2['proom1_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room2') {
    $proom1Route = 'https://homebor.com/'.$row2['proom2'];
    if($row2['proom2_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room3') {
    $proom1Route = 'https://homebor.com/'.$row2['proom3'];
    if($row2['proom3_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room4') {
    $proom1Route = 'https://homebor.com/'.$row2['proom4'];
    if($row2['proom4_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room5') {
    $proom1Route = 'https://homebor.com/'.$row2['proom5'];
    if($row2['proom5_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room6') {
    $proom1Route = 'https://homebor.com/'.$row2['proom6'];
    if($row2['proom6_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room7') {
    $proom1Route = 'https://homebor.com/'.$row2['proom7'];
    if($row2['proom7_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}
if($row2['room_e'] == 'room8') {
    $proom1Route = 'https://homebor.com/'.$row2['proom8'];
    if($row2['proom8_2'] != 'NULL'){
        $proom2Route = 'https://homebor.com/'.$row2['proom1_2'];
    } else {
        $proom2Route = 'https://homebor.com/'.$row2['pbath1'];
    }
}



$Address = 'Address: '.$row2['dir'].', '.$row2['city'].', '.$row2['state'].' '.$row2['p_code'];


if ($usuario != $row['mail_s']) {
	 header("location: ../index.php");
}
}


$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetTitle('Welcome to your homestay');

$pdf->Image('../../assets/img/piezas-ihomestay-triangulo-1.png',0,0,60,90);
$pdf->Image('../../assets/img/piezas-ihomestay-triangulo-2.png',150,0,60,105);

$pdf->Image('../../assets/img/ihomestay.png',0,0,50,10);

$pdf->SetFont('Arial','B',20);
$pdf->SetXY(10,10);
$pdf->Cell(0,0,'WELCOME TO YOUR',0,0,'C');

$pdf->SetFont('Arial','B',20);
$pdf->SetXY(10,20);
$pdf->Cell(0,0,'HOMESTAY',0,0,'C');

$pdf->Image('../../assets/img/piezas-ihomestay-rectangulo.png',30, 50,145,105);

$pdf->Image($proom1Route,40,55,130,90);

$pdf->SetFont('Arial','',16);
$pdf->SetXY(10,165);
$pdf->Cell(0,0,$Address,0,0,'C');

$pdf->SetFont('Arial','',16);
$pdf->SetXY(10,185);
$pdf->Cell(0,0,Tag.' Private Room',0,0,'L');

$pdf->SetFont('Arial','',16);
$pdf->SetXY(10,194);
$pdf->Cell(0,0,Tag.' Bathroom',0,0,'L');

$pdf->SetFont('Arial','',16);
$pdf->SetXY(10,201);
$pdf->Cell(0,0,Tag.' High Internet Speed',0,0,'L');

$pdf->SetFont('Arial','',16);
$pdf->SetXY(10,208);
$pdf->Cell(0,0,Tag.' Laundry',0,0,'L');

$pdf->Image($proom2Route,75,175,130,110);

$pdf->AddPage();

$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,10);
$pdf->Cell(0,0,'Hello/Bonjour!',0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,25);
$pdf->Cell(0,0,$host,0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,30);
$pdf->Cell(0,0,"watcher and love spend time with international students. The house is extremely clean and",0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,35);
$pdf->Cell(0,0,"welcoming. Every student is treated like a part of the family, so get ready to have dinners",0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,40);
$pdf->Cell(0,0,"together. They cannot wait to meet you.",0,0,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,50);
$pdf->Cell(0,0,"Accommodating, clean, cozy, 5+Star, spacious, tranquil, beautiful, award winning and good",0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,55);
$pdf->Cell(0,0,"cooking!",0,0,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,65);
$pdf->Cell(0,0,"I love entertaining my friends and neighbours and look forward to doing more of this as",0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,70);
$pdf->Cell(0,0,"Covid restrictions decrease.",0,0,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,80);
$pdf->Cell(0,0,$city,0,0,'L');
$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,85);
$pdf->Cell(0,0,"area and making their stay in Canada a memorable one.",0,0,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetXY(10,100);
$pdf->Cell(0,0,"Feel free to contact me before you arrive.",0,0,'L');



$pdf->Image($plivingRoute,5,110,100,70);
$pdf->Image($parea1Route,107,110,100,70);

$pdf->Image($phouseRoute,5,190,100,70);

$pdf->SetFont('Arial','B',18);
$pdf->SetXY(120,210);
$pdf->Cell(0,0,"Family Contact",0,0,'C');

$pdf->SetFont('Arial','',18);
$pdf->SetXY(130,225);
$pdf->Cell(0,0,$row2['num'],0,0,'L');

$pdf->SetFont('Arial','',16);
$pdf->SetXY(130,245);
$pdf->Cell(0,0,$row2['mail_h'],0,0,'L');

$pdf->Image('../../assets/img/phone.png',120,220,-1200);
$pdf->Image('../../assets/img/email.png',120,240,-1200);

$pdf->Output();
?>