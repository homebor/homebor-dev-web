<?php
require '../../xeon.php';
require '../../cript.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$resultado = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
$row = $resultado->fetch_assoc();

//TODO DATE TIME
date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');
$dateImage = date('YmdHisv');
$mail_s = $usuario;
$paths = '../../public_s/' . $usuario;

if (isset($_POST['update'])) {

  //TODO BASIC INFORMATION

  if (!empty($_FILES['profile_s']['name'])) {

    $profile_tmp = $_FILES['profile_s']['tmp_name'];
    $profileType = stristr($_FILES['profile_s']['type'], "/");
    $profileExtension = str_replace("/", ".", $profileType);
    $s_profileUrl = "public_s/" . $usuario . "/profilePhoto" . $dateImage . $profileExtension;
    $s_profile = "profilePhoto" . $dateImage . $profileExtension;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($profile_tmp, $paths . '/' . $s_profile);
  } else $s_profileUrl = $row['photo_s'];

  if (empty($_POST['name'])) $name = addslashes($row['name_s']);
  else $name =  addslashes($_POST['name']);

  if (empty($_POST['l_name'])) $l_name = addslashes($row['l_name_s']);
  else $l_name =  addslashes($_POST['l_name']);

  if (empty($_POST['mail_s'])) $mail_s1 =  $row['mail_s'];
  else $mail_s1 =  $_POST['mail_s'];

  if (empty($_POST['password'])) $password = $row['password'];
  else $password =  $_POST['password'];

  if (empty($_POST['arrive_g'])) $arrive_g =  $row['arrive_g'];
  else {
    $g_arrive = date_create($_POST['arrive_g']); //Se crea una fecha con el formato que recibes de la vista.
    $arrive_g = date_format($g_arrive, 'Y-m-d'); //Obtienes la fecha en el formato deseado.   
  }

  if (empty($_POST['destination_c_o']) && empty($_POST['destination_c'])) $destination_c =  $row['destination_c'];
  else if (!empty($_POST['destination_c_o'])) $destination_c =  addslashes($_POST['destination_c_o']);
  else $destination_c =  addslashes($_POST['destination_c']);


  //TODO PERSONAL STUDENT ADDRESS

  if (empty($_POST['country'])) $country = addslashes($row['country']);
  else $country =  addslashes($_POST['country']);

  if (empty($_POST['dir'])) $dir =  addslashes($row['dir_s']);
  else $dir =  addslashes($_POST['dir']);

  if (empty($_POST['city'])) $city =  addslashes($row['city_s']);
  else $city =  addslashes($_POST['city']);

  if (empty($_POST['state'])) $state = addslashes($row['state_s']);
  else $state =  addslashes($_POST['state']);

  if (empty($_POST['p_code'])) $p_code =  addslashes($row['p_code_s']);
  else $p_code = addslashes($_POST['p_code']);

  //TODO ACCOMMODATION REQUEST

  if (empty($_POST['firstd'])) $firstd = $row['firstd'];
  else {
    $f_firstd = date_create($_POST['firstd']); //Se crea una fecha con el formato que recibes de la vista.
    $firstd = date_format($f_firstd, 'Y-m-d'); //Obtienes la fecha en el formato deseado.    
  }

  if (empty($_POST['lastd'])) $lastd = $row['lastd'];
  else {
    $l_lastd = date_create($_POST['lastd']); //Se crea una fecha con el formato que recibes de la vista.
    $lastd = date_format($l_lastd, 'Y-m-d'); //Obtienes la fecha en el formato deseado.    
  }

  if (empty($_POST['n_airline'])) $n_airline =  addslashes($row['n_airline']);
  else $n_airline = addslashes($_POST['n_airline']);

  if (empty($_POST['n_flight'])) $n_flight =  addslashes($row['n_flight']);
  else $n_flight =  addslashes($_POST['n_flight']);

  if (empty($_POST['f_date'])) $f_date =  $row['departure_f'];
  else {
    $date_f = date_create($_POST['f_date']); //Se crea una fecha con el formato que recibes de la vista.
    $f_date = date_format($date_f, 'Y-m-d h:i A'); //Obtienes la fecha en el formato deseado.   
  }

  if (empty($_POST['h_date'])) $h_date =  $row['arrive_f'];
  else {
    $date_h = date_create($_POST['h_date']); //Se crea una fecha con el formato que recibes de la vista.
    $h_date = date_format($date_h, 'Y-m-d'); //Obtienes la fecha en el formato deseado.    
  }

  //TODO EMERGENCY CONTACT

  if (empty($_POST['cont_name'])) $cont_name = addslashes($row['cont_name']);
  else $cont_name =  addslashes($_POST['cont_name']);

  if (empty($_POST['cont_lname'])) $cont_lname = addslashes($row['cont_lname']);
  else $cont_lname =  addslashes($_POST['cont_lname']);

  if (empty($_POST['num_conts'])) $num_conts = addslashes($row['num_conts']);
  else $num_conts =  addslashes($_POST['num_conts']);

  if (empty($_POST['relationship'])) $s_relationship = addslashes($row['relationship']);
  else $s_relationship = addslashes($_POST['relationship']);

  //TODO PERSONAL INFORMATION

  if (empty($_POST["about_me"])) $about_me =  addslashes($row["about_me"]);
  else $about_me =  addslashes($_POST["about_me"]);

  if (empty($_POST['db'])) $db = $row['db_s'];
  else {
    $d_db = date_create($_POST['db']); //Se crea una fecha con el formato que recibes de la vista.
    $db = date_format($d_db, 'Y-m-d'); //Obtienes la fecha en el formato deseado.    
  }

  if (empty($_POST['gender'])) $gender = $row['gen_s'];
  else $gender =  $_POST['gender'];

  if (empty($_POST['num_s'])) $num_s = addslashes($row['num_s']);
  else $num_s =  addslashes($_POST['num_s']);

  if (empty($_POST['nationality'])) $nationality =  addslashes($row['nationality']);
  else $nationality =  addslashes($_POST['nationality']);

  if (empty($_POST['lang_s'])) $lang_s = addslashes($row['lang_s']);
  else $lang_s =  addslashes($_POST['lang_s']);

  if (empty($_POST['language_a'])) $language_a = addslashes($row['language_a']);
  else $language_a =  addslashes($_POST['language_a']);

  if (empty($_POST['pass'])) $pass = addslashes($row['pass']);
  else $pass =  addslashes($_POST['pass']);

  if (empty($_POST['exp_pass'])) $exp_pass =  $row['exp_pass'];
  else {
    $pass_exp = date_create($_POST['exp_pass']); //Se crea una fecha con el formato que recibes de la vista.
    $exp_pass = date_format($pass_exp, 'Y-m-d'); //Obtienes la fecha en el formato deseado.  
  }

  if (empty($_POST['bl'])) $bl = $row['db_visa'];
  else {
    $lb = date_create($_POST['bl']); //Se crea una fecha con el formato que recibes de la vista.
    $bl = date_format($lb, 'Y-m-d'); //Obtienes la fecha en el formato deseado.      
  }

  //TODO HEALTH INFORMATION

  if (empty($_POST['smoke_s'])) $smoke_s = $row['smoke_s'];
  else $smoke_s =  $_POST['smoke_s'];

  if (empty($_POST['drinks_alc'])) $drinks_alc =  $row['drinks_alc'];
  else $drinks_alc =  $_POST['drinks_alc'];

  if (empty($_POST['drugs'])) $drugs =  $row['drugs'];
  else $drugs =  $_POST['drugs'];

  if (empty($_POST['allergy_a_o']) && empty($_POST['allergy_a'])) $allergy_a = addslashes($row['allergy_a']);
  else if (!empty($_POST['allergy_a_o'])) $allergy_a =  addslashes($_POST['allergy_a_o']);
  else $allergy_a =  addslashes($_POST['allergy_a']);

  if (empty($_POST['allergy_m_o']) && empty($_POST['allergy_m'])) $allergy_m =  addslashes($row['allergy_m']);
  else if (!empty($_POST['allergy_m_o'])) $allergy_m =  addslashes($_POST['allergy_m_o']);
  else $allergy_m =  addslashes($_POST['allergy_m']);

  if (empty($_POST['healt_s'])) $healt_s =  addslashes($row['healt_s']);
  else $healt_s =  addslashes($_POST['healt_s']);

  if (empty($_POST['disease_o']) && empty($_POST['disease'])) $disease =  addslashes($row['disease']);
  else if (!empty($_POST['disease_o'])) $disease =  addslashes($_POST['disease_o']);
  else $disease =  addslashes($_POST['disease']);

  if (empty($_POST['treatment_o']) && empty($_POST['treatment'])) $treatment =  addslashes($row['treatment']);
  else if (!empty($_POST['treatment_o'])) $treatment =  addslashes($_POST['treatment_o']);
  else $treatment =  addslashes($_POST['treatment']);

  if (empty($_POST['treatment_p_o']) && empty($_POST['treatment_p'])) $treatment_p =  addslashes($row['treatment_p']);
  else if (!empty($_POST['treatment_p_o'])) $treatment_p =  addslashes($_POST['treatment_p_o']);
  else $treatment_p = addslashes($_POST['treatment_p']);

  if (empty($_POST['allergies_o']) && empty($_POST['allergies'])) $allergies =  addslashes($row['allergies']);
  else if (!empty($_POST['allergies_o'])) $allergies =  addslashes($_POST['allergies_o']);
  else $allergies =  addslashes($_POST['allergies']);

  if (empty($_POST['surgery_o']) && empty($_POST['surgery'])) $surgery =  addslashes($row['surgery']);
  else if (!empty($_POST['surgery_o'])) $surgery = addslashes($_POST['surgery_o']);
  else $surgery = addslashes($_POST['surgery']);

  //TODO PROFESIONAL INFORMATION

  if (empty($_POST['n_a'])) $n_a = $row['n_a'];
  else $n_a =  $_POST['n_a'];

  if (empty($_POST['english_l'])) $english_l =  $row['english_l'];
  else $english_l =  $_POST['english_l'];

  if (empty($_POST['type_s'])) $type_s = $row['type_s'];
  else $type_s =  $_POST['type_s'];

  if (empty($_POST['prog_selec'])) $prog_selec =  addslashes($row['prog_selec']);
  else $prog_selec =  addslashes($_POST['prog_selec']);

  if (empty($_POST['schedule'])) $schedule = $row['schedule'];
  else $schedule =  $_POST['schedule'];

  //TODO HOUSE PREFERENCE

  if (empty($_POST['smoker_l'])) $smoker_l =  $row['smoker_l'];
  else $smoker_l =  $_POST['smoker_l'];

  if (empty($_POST['children'])) $children =  $row['children'];
  else $children =  $_POST['children'];

  if (empty($_POST['teenagers'])) $teenagers = $row['teenagers'];
  else $teenagers =  $_POST['teenagers'];

  if (empty($_POST['pets'])) $pets = $row['pets'];
  else $pets =  $_POST['pets'];

  if (empty($_POST['diet'])) {
    $diet =  'NULL';
    $vegetarians = $row['vegetarians'];
    $halal =  $row['halal'];
    $kosher =  $row['kosher'];
    $lactose =  $row['lactose'];
    $gluten =  $row['gluten'];
    $pork =  $row['pork'];
    $none =  $row['none'];
  } elseif ($_POST['diet'] == 'Vegetarians') {
    $vegetarians = 'yes';
    $halal =  'no';
    $kosher =  'no';
    $lactose =  'no';
    $gluten =  'no';
    $pork =  'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'Halal') {
    $halal =  'yes';
    $vegetarians = 'no';
    $kosher =  'no';
    $lactose =  'no';
    $gluten =  'no';
    $pork =  'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'Kosher') {
    $kosher =  'yes';
    $halal =  'no';
    $vegetarians = 'no';
    $lactose =  'no';
    $gluten =  'no';
    $pork =  'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'Lactose') {
    $lactose =  'yes';
    $kosher =  'no';
    $halal =  'no';
    $vegetarians = 'no';
    $gluten =  'no';
    $pork =  'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'Gluten') {
    $gluten =  'yes';
    $lactose =  'no';
    $kosher =  'no';
    $halal =  'no';
    $vegetarians = 'no';
    $pork =  'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'Pork') {
    $pork =  'yes';
    $gluten =  'no';
    $lactose =  'no';
    $kosher =  'no';
    $halal =  'no';
    $vegetarians = 'no';
    $none =  'no';
  } elseif ($_POST['diet'] == 'None') {
    $none =  'yes';
    $pork =  'no';
    $gluten =  'no';
    $lactose =  'no';
    $kosher =  'no';
    $halal =  'no';
    $vegetarians = 'no';
  } else {
    $vegetarians = 'no';
    $halal =  'no';
    $kosher =  'no';
    $lactose =  'no';
    $gluten =  'no';
    $pork =  'no';
    $none =  'yes';
  }

  if (empty($_POST['a_price'])) $s_accommodationPrice = '0';
  else $s_accommodationPrice = $_POST['a_price'];

  if (empty($_POST['t_price'])) $s_transportPrice = '0';
  else $s_transportPrice = $_POST['t_price'];

  //TODO ATTACHED FILES

  if (!empty($_FILES['passport']['name'])) {

    if (!empty($row["pass_photo"]) || $row["pass_photo"] != 'NULL') $deletePhoto = unlink("../../" . $row["pass_photo"]);

    $passportImage_tmp = $_FILES['passport']['tmp_name'];
    $passportImageType = stristr($_FILES['passport']['type'], "/");
    $passportImageExtension = str_replace("/", ".", $passportImageType);
    $s_passportImageUrl = "public_s/" . $mail_s . "/passportPhoto" . $dateImage . $passportImageExtension;
    $s_passportImage = "passportPhoto" . $dateImage . $passportImageExtension;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($passportImage_tmp, $paths . '/' . $s_passportImage);
  } else $s_passportImageUrl = $row['pass_photo'];

  if (!empty($_FILES['visa']['name'])) {

    if (!empty($row["visa"]) || $row["visa"] != 'NULL') $deletePhoto = unlink("../../" . $row["visa"]);

    $visaImage_tmp = $_FILES['visa']['tmp_name'];
    $visaImageType = stristr($_FILES['visa']['type'], "/");
    $visaImageExtension = str_replace("/", ".", $visaImageType);
    $s_visaImageUrl = "public_s/" . $mail_s . "/visaPhoto" . $dateImage . $visaImageExtension;
    $s_visaImage = "visaPhoto" . $dateImage . $visaImageExtension;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($visaImage_tmp, $paths . '/' . $s_visaImage);
  } else $s_visaImageUrl = $row['visa'];

  if (!empty($_FILES['flight-image']['name'])) {

    if (!empty($row["flight_image"]) || $row["flight_image"] != 'NULL') $deletePhoto = unlink("../../" . $row["flight_image"]);

    $flightImage_tmp = $_FILES['flight-image']['tmp_name'];
    $flightImageType = stristr($_FILES['flight-image']['type'], "/");
    $flightImageExtension = str_replace("/", ".", $flightImageType);
    $s_flightImageUrl = "public_s/" . $mail_s . "/flightImage" . $dateImage . $flightImageExtension;
    $s_flightImage = "flightImage" . $dateImage . $flightImageExtension;

    if (!file_exists($paths)) mkdir($paths, 0777);
    move_uploaded_file($flightImage_tmp, $paths . '/' . $s_flightImage);
  } else $s_flightImageUrl = $row['flight_image'];

  if (!empty($_POST["signature-image"])) {
    if (!empty($row["signature_s"]) || $row["signature_s"] != 'NULL') $deletePhoto = unlink("../../" . $row["signature_s"]);

    if (!file_exists($paths)) mkdir($paths, 0777);
    $signature = $_POST["signature-image"];

    $folderPath = "../../public_s/" . $mail_s . "/";
    $image_parts = explode(";base64", $signature);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);
    $file = $folderPath . "signature" . $dateImage . ".png";
    file_put_contents($file, $image_base64);

    $signatureURL = "public_s/" . $mail_s . "/signature" . $dateImage . ".png";
  } else $signatureURL = $row['signature_s'];


  $id_m =  $row['id_m'];

  //TODO SAVE

  $query2 = "UPDATE pe_student SET photo_s='$s_profileUrl', name_s='$name', l_name_s='$l_name', arrive_g='$arrive_g', destination_c='$destination_c', country='$country', dir_s='$dir', city_s='$city', state_s='$state', p_code_s='$p_code', firstd='$firstd', lastd='$lastd', n_airline='$n_airline', n_flight='$n_flight', departure_f='$f_date', arrive_f='$h_date', cont_name='$cont_name', cont_lname='$cont_lname', num_conts='$num_conts', relationship = '$s_relationship', about_me='$about_me', db_s='$db', gen_s='$gender', num_s='$num_s', nationality='$nationality', lang_s='$lang_s', language_a='$language_a', passport='$pass', exp_pass='$exp_pass', db_visa='$bl', smoke_s='$smoke_s', drinks_alc='$drinks_alc', drugs='$drugs', allergy_a='$allergy_a', allergy_m='$allergy_m', healt_s='$healt_s', disease='$disease', treatment='$treatment', treatment_p='$treatment_p', allergies='$allergies', surgery='$surgery', n_a='$n_a', english_l='$english_l', type_s='$type_s', prog_selec='$prog_selec', schedule='$schedule', smoker_l='$smoker_l', children='$children', teenagers='$teenagers', pets='$pets',  vegetarians='$vegetarians', halal='$halal', kosher='$kosher', lactose='$lactose', gluten='$gluten', pork='$pork', none='$none', pass_photo='$s_passportImageUrl', visa='$s_visaImageUrl', pass_photo='$s_passportImageUrl', visa='$s_visaImageUrl', a_price='$s_accommodationPrice', t_price = '$s_transportPrice', relationship = '$s_relationship', signature_s = '$signatureURL', flight_image = '$s_flightImageUrl' WHERE mail_s='$usuario'";
  $resultado2 = $link->query($query2);

  $query3 = "INSERT INTO `webmaster`(`user`, `activity`, `dates`, `edit_user`, `id_m`) VALUES ('$usuario', 'Edit Student Profile', '$date', '$mail_s', '$id_m')";

  $resultado3 = $link->query($query3);

  if ($resultado2 == 1) {
    echo  "<script type='text/javascript'>window.top.location='student_info';</script>";
    exit;
  } else echo "Insertion Failed";
}