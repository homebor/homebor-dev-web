<?php
include '../xeon.php';
session_start();

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$id = $_GET['art_id'];

$query="SELECT * FROM pe_student WHERE id_student = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$row[mail_s]' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query3="SELECT * FROM academy WHERE name_a = '$row2[name_a]' ";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$aced = $link->query("SELECT * FROM notification WHERE user_i_mail = '$row[mail_s]' AND user_r = '$usuario' AND confirmed = '0' AND title = 'Reservation Request'");
$ac=$aced->fetch_assoc();

$usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$u=$usu->fetch_assoc();

$peh = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
$pe=$peh->fetch_assoc();



if ($row5['usert'] != 'homestay') {
    header("location: ../logout.php");   
}


?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/header-index.css">
  <link rel="stylesheet" href="assets/css/student-info-not.css">
  <link rel="stylesheet" href="assets/css/notification.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students Profile</title>


  <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>

<body style="background-color: #F3F8FC;">

  <header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
     =============================================================================================================-->
    <?php include 'header.php' ?>

  </header>
  <!--end Header-->


  <div class="container_register">

    <main class="card__register">

      <div class="row m-0 p-0">

        <div class="col-md-4 column-1">

          <div class="card">

            <h3 class="form__group-title__col-4">Basic Information</h3>

            <div class="information__content">

              <div class="form__group__div__photo">

                <img class="form__group__photo" id="preview" src="../<?php echo $row['photo_s'] ?>" alt="">

              </div>

              <!-- Group Name -->

              <?php if(empty($row['name_s']) || $row['name_s'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__name" id="group__name">
                <div class="form__group__icon-input">
                  <label for="name" class="form__label">First Name</label>

                  <label for="name" class="icon"><i class="icon icon__names"></i></label>
                  <p id="name" name="name" class="form__group-input names"><?php echo $row['name_s']?></p>

                </div>

              </div>

              <?php } ?>


              <!-- Group Last Name -->

              <?php if(empty($row['l_name_s']) || $row['l_name_s'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__l_name" id="group__l_name">
                <div class="form__group__icon-input">

                  <label for="l_name" class="form__label">Last Name</label>
                  <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                  <p id="l_name" name="l_name" class="form__group-input names"> <?php echo "$row[l_name_s]"?> </p>

                </div>
              </div>

              <?php } ?>

            </div>

          </div>


          <?php if($row['n_airline'] == 'NULL' && $row['n_flight'] == 'NULL' && $row['departure_f'] == 'NULL' && $row['n_flight'] == 'NULL' ){}else{ ?>


          <div class="card">


            <h3 class="form__group-title__col-4">Flight Information</h3>

            <div class="information__content">

              <!-- Group Booking Confirmation -->

              <?php if(empty($row['n_airline']) || $row['n_airline'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__n_airline" id="group__n_airline">
                <div class="form__group__icon-input">

                  <label for="n_airline" class="form__label">Booking Confirmation</label>

                  <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                  <p id="n_airline" name="n_airline" class="form__group-input n_airline">
                    <?php echo $row['n_airline']; ?> </p>

                </div>
              </div>

              <?php } ?>

              <!-- Group Booking Confirmation -->

              <?php if(empty($row['n_flight']) || $row['n_flight'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__n_flight" id="group__n_flight">
                <div class="form__group__icon-input">

                  <label for="n_flight" class="form__label">Landing Flight Number</label>

                  <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                  <p id="n_flight" name="n_flight" class="form__group-input n_flight"> <?php echo $row['n_flight']; ?>
                  </p>

                </div>
              </div>

              <?php } ?>

              <?php if(empty($row['departure_f']) || $row['departure_f'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group-f_date ">
                <div class="form__group__icon-input">
                  <label for="f_date" class="form__label">Flight Date</label>
                  <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                  <?php $roc1 = date_create($row['departure_f']); ?>

                  <p id="f_date" name="f_date" class="form__group-input f_date">
                    <?php echo date_format($roc1, 'jS F Y'); ?> </p>

                </div>
              </div>

              <?php } 
                                
                                if(empty($row['arrive_f']) || $row['arrive_f'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group-h_date">

                <div class="form__group__icon-input">

                  <label for="h_date" class="form__label">Arrival at the Homestay</label>
                  <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                  <?php $roc2 = date_create($row['arrive_f']); ?>

                  <p id="h_date" name="h_date" class="form__group-input h_date">
                    <?php echo date_format($roc2, 'jS F Y'); ?> </p>


                </div>
              </div>

              <?php } ?>

            </div>

          </div>

          <?php } 
                    
                    
                    if($row['cont_name'] == 'NULL' && $row['cont_lname'] == 'NULL' && $row['num_conts'] == 'NULL' && $row['cell_s'] == 'NULL' ){}else{
                    ?>



          <div class="card">

            <h3 class="form__group-title__col-4">Reservation Details</h3>

            <div class="information__content">


              <!-- Group Emergency Name -->

              <?php if(empty($ac['bedrooms']) || $ac['bedrooms'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__room" id="group__room">
                <div class="form__group__icon-input">

                  <label for="room" class="form__label">Room to occupy</label>

                  <label for="room" class="icon"><i class="icon icon__names"></i></label>

                  <p id="room" name="room" class="form__group-input room"> <?php echo $ac['bedrooms']; ?> </p>

                </div>
              </div>

              <?php } ?>

              <!-- Group Emergency Last Name -->

              <?php if(empty($ac['start']) || $ac['start'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__start_date" id="group__start_date">

                <div class="form__group__icon-input">

                  <label for="start_date" class="form__label">Arrive Date</label>

                  <label for="start_date" class="icon"><i class="icon icon__date"></i></label>

                  <?php $date_res = date_create($ac['start']); ?>

                  <p id="start_date" name="start_date" class="form__group-input start_date">
                    <?php echo date_format($date_res, 'jS F Y'); ?> </p>

                </div>
              </div>

              <?php } ?>


              <!-- Group Leave Date -->

              <?php if(empty($row['num_conts']) || $row['num_conts'] == 'NULL' ){}else{ ?>

              <div class="form__group form__group__num_conts" id="group__num_conts">

                <div class="form__group__icon-input">

                  <label for="num_conts" class="form__label">Leave Date</label>

                  <label for="cont_lname" class="icon"><i class="icon icon__date"></i></label>

                  <?php $date_res1 = date_create($ac['end_']); ?>

                  <p id="end_date" name="end_date" class="form__group-input end_date">
                    <?php echo date_format($date_res1, 'jS F Y'); ?> </p>

                </div>
              </div>

              <?php } ?>


              <form class="btns__options" action="addEventNot.php" method="post">
                <?php $sp = ' ' ?>

                <input type="hidden" name="title" value="<?php echo $ac['user_i'] . $sp . $ac['user_i_l']; ?>">

                <input type="hidden" name="start" value="<?php echo $ac['start']; ?>">

                <input type="hidden" name="end" value="<?php echo $ac['end_']; ?>">

                <input type="hidden" name="color" value="<?php echo $ac['color']; ?>">

                <input type="hidden" name="mail" value="<?php echo $ac['user_i_mail']; ?>">

                <input type="hidden" name="useri" value="<?php echo $u['name']; ?>">

                <input type="hidden" name="userii" value="<?php echo $u['l_name']; ?>">

                <input type="hidden" name="h_name" value="<?php echo $pe['h_name']; ?>">

                <button name="save" class="btn__op assignation__hom"> Confirm </button>

                <button name="delete" class="btn__op assignation__edit"> Reject </button>
              </form>

            </div>

          </div>

          <?php } ?>

        </div>



        <div class="col-md-8 column-2">

          <?php if($row['db_s'] == 'NULL' && $row['gen_s'] == 'NULL' && $row['nacionality'] == 'NULL' && $row['lang_s'] == 'NULL' ){}else{ ?>

          <div class="card">

            <h3 class="form__group-title__col-4">Personal Information</h3>

            <div class="information__content information__content-col-8">

              <div class="row form__group-basic-content m-0 p-0">

                <!-- Group Age -->

                <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL' ){}else{ 
                                    
                                        $from = new DateTime($row['db_s']);
                                        $to   = new DateTime('today');
                                        $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
                                        $sufix= " Years old";
                                    
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__age" id="group__age">

                  <div class="form__group__icon-input">

                    <label for="age" class="form__label">Age</label>

                    <label for="age" class="icon"><i class="icon icon__date"></i></label>

                    <p id="age" name="age" class="form__group-input age"> <?php echo $age . $sufix ?> </p>

                  </div>
                </div>

                <?php } ?>


                <!-- Group Gender -->

                <?php if(empty($row['gen_s']) || $row['gen_s'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__gender" id="group__gender">

                  <div class="form__group__icon-input">

                    <label for="gender" class="form__label">Gender</label>

                    <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                    <p id="gender" name="gender" class="form__group-input gender"> <?php echo $row['gen_s'] ?> </p>

                  </div>
                </div>

                <?php } ?>



                <!-- Group Background -->

                <?php if(empty($row['nacionality']) || $row['nacionality'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__nacionality" id="group__nacionality">
                  <div class="form__group__icon-input">

                    <label for="nacionality" class="form__label">Background</label>

                    <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>

                    <p id="nacionality" name="nacionality" class="form__group-input nacionality">
                      <?php echo $row['nacionality'] ?> </p>

                  </div>

                </div>

                <?php } ?>



                <!-- Group Origin Language -->

                <?php if(empty($row['lang_s']) || $row['lang_s'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__lang_s" id="group__lang_s">
                  <div class="form__group__icon-input">

                    <label for="lang_s" class="form__label">Origin Language</label>

                    <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>

                    <p id="lang_s" name="lang_s" class="form__group-input lang_s"> <?php echo $row['lang_s'] ?> </p>

                  </div>

                </div>

                <?php } ?>

              </div>

            </div>

          </div>

          <?php } 
                    
                    if($row['smoke_s'] == 'NULL' && $row['drinks_alc'] == 'NULL' && $row['drugs'] == 'NULL' && $row['disease'] == 'NULL' && $row['treatment'] == 'NULL' && $row['treatment_p'] == 'NULL' && $row['allergies'] == 'NULL' && $row['surgery'] == 'NULL'){}else{
                    
                    ?>

          <div class="card">

            <h3 class="form__group-title__col-4">Health Information</h3>

            <div class="information__content information__content-col-8">

              <div class="row form__group-basic-content m-0 p-0">

                <!-- Group Type Student -->

                <?php if(empty($row['smoke_s']) || $row['smoke_s'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Smoke?</label>

                    <label for="type_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['smoke_s'] ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Type Student -->

                <?php if(empty($row['drinks_alc']) || $row['drinks_alc'] == 'NULL' ){}else{ 
                                    if($row['drinks_alc'] == 'yes' ){
                                        $drinks = 'Yes';
                                    }elseif($row['drinks_alc'] == 'no'){
                                        $drinks = 'No';
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Drink Alcohol?</label>

                    <label for="type_s" class="icon"><i class="icon icon__drinks_alc"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $drinks ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Type Student -->

                <?php if(empty($row['drugs']) || $row['drugs'] == 'NULL' ){}else{ 
                                    if($row['drugs'] == 'yes' ){
                                        $drugs = 'Yes';
                                    }elseif($row['drugs'] == 'no'){
                                        $drugs = 'No';
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Use Drugs?</label>

                    <label for="type_s" class="icon"><i class="icon icon__drugs"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $drugs ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Smoker -->

                <?php if(empty($row['allergy_a']) || $row['allergy_a'] == 'NULL' ){}else{

                  $allergy = ucfirst($row['allergy_a']);
                ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Allergy to Animals</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $allergy ?> </p>
                  </div>


                </div>
                <?php } ?>

                <?php if(empty($row['allergy_m']) || $row['allergy_m'] == 'NULL' ){}else{
                                        $allergyM = ucfirst($row['allergy_m']);
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Dietary Restrictions</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__food"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $allergyM ?> </p>
                  </div>


                </div>
                <?php } ?>


                <!-- Group Type Student -->

                <?php if(empty($row['disease']) || $row['disease'] == 'NULL' ){}else{ 
                                    if($row['disease'] == 'no'){
                                        $disease = 'No';
                                    }else{
                                        $disease = $row['disease'];
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Some Disease?</label>

                    <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $disease ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Type Student -->

                <?php if(empty($row['treatment']) || $row['treatment'] == 'NULL' ){}else{ 
                                    if($row['treatment'] == 'no'){
                                        $treatment = 'No';
                                    }else{
                                        $treatment = $row['treatment'];
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Medical Treatment?</label>

                    <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $treatment ?> </p>

                  </div>

                </div>

                <?php } ?>


                <!-- Group Type Student -->

                <?php if(empty($row['treatment_p']) || $row['treatment_p'] == 'NULL' ){}else{
                                    if($row['treatment_p'] == 'no'){
                                        $treatment_p = 'No';
                                    }else{
                                        $treatment_p = $row['treatment_p'];
                                    }
                                    
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Psychological Treatment?</label>

                    <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $treatment_p ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Type Student -->

                <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL' ){}else{ 
                                    if($row['allergies'] == 'no'){
                                        $allergiesH = 'No';
                                    }else{
                                        $allergiesH = $row['allergies'];
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Have Allergies?</label>

                    <label for="type_s" class="icon"><i class="icon icon__allergies"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $allergiesH ?> </p>

                  </div>

                </div>

                <?php } ?>


                <?php if(empty($row['surgery']) || $row['surgery'] == 'NULL' ){}else{ 
                                    if($row['surgery'] == 'no'){
                                        $surgery = 'No';
                                    }else{
                                        $surgery = $row['allergies'];
                                    }
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Have you had Surgeries?</label>

                    <label for="type_s" class="icon"><i class="icon icon__allergies"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $surgery ?> </p>

                  </div>

                </div>

                <?php } ?>

              </div>

            </div>

          </div>

          <?php }if($row2['name_a'] == 'NULL' && $row['type_s'] == 'NULL' && $row['firstd'] == 'NULL' && $row['lastd'] == 'NULL' ){}else{
                    
                    ?>


          <div class="card">

            <h3 class="form__group-title__col-4">Professional Information</h3>

            <div class="information__content information__content-col-8">

              <div class="row form__group-basic-content m-0 p-0">


                <!-- Group Academy Preference -->

                <?php if(empty($row2['name_a']) || $row2['name_a'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                  <div class="form__group__icon-input">

                    <label for="n_a" class="form__label">School Name</label>

                    <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                    <p id="n_a" name="n_a" class="form__group-input n_a"> <?php echo $row2['name_a'] ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Academy Dir -->

                <?php if(empty($row2['dir_a']) || $row2['dir_a'] == 'NULL' && $row2['city_a'] == 'NULL'){}else{ ?>

                <div class="form__group col-sm-12 form__group__basic form__group__address_aca" id="group__address_aca">

                  <div class="form__group__icon-input">

                    <label for="address_aca" class="form__label">School Address</label>

                    <label for="address_aca" class="icon"><i class="icon icon__n_a"></i></label>

                    <p id="address_aca" name="address_aca" class="form__group-input address_aca">
                      <?php echo $row2['dir_a'] . ' - ' . $row2['city_a']?> </p>

                  </div>

                </div>

                <?php } ?>


                <!-- Group Type Student -->

                <?php if(empty($row['type_s']) || $row['type_s'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                  <div class="form__group__icon-input">

                    <label for="type_s" class="form__label">Type Student</label>

                    <label for="type_s" class="icon"><i class="icon icon__names"></i></label>

                    <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['type_s'] ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group Start Date of Stay -->

                <?php if(empty($row['firstd']) || $row['firstd'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__firstd" id="group__firstd">
                  <div class="form__group__icon-input">

                    <label for="firstd" class="form__label">Start Date of Stay</label>
                    <label for="firstd" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc5 = date_create($row['firstd']); ?>

                    <p id="firstd" name="firstd" class="form__group-input firstd">
                      <?php echo date_format($roc5, 'jS F Y'); ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group End Date of stay -->

                <?php if(empty($row['lastd']) || $row['lastd'] == 'NULL' ){}else{ ?>

                <div class="form__group col-sm-4 form__group__basic form__group__lastd" id="group__lastd">
                  <div class="form__group__icon-input">

                    <label for="lastd" class="form__label">End Date of Stay</label>
                    <label for="lastd" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc6 = date_create($row['lastd']); ?>

                    <p id="lastd" class="form__group-input lastd"> <?php echo date_format($roc6, 'jS F Y'); ?> </p>
                  </div>

                </div>

                <?php } ?>

              </div>

            </div>

          </div>

          <?php } 
                    
                    if($row['smoke_s'] == 'NULL' && $row['pets'] == 'NULL' && $row['food'] == 'NULL' && $row['allergies'] == 'NULL' && $row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{
                    
                    ?>


          <div class="card">

            <h3 class="form__group-title__col-4">House Preferences</h3>


            <h3 class="subtitle__section-card mt-3 mb-5">Can you Share With:</h3>

            <div class="information__content information__content-col-8">

              <div class="row form__group-basic-content m-0 p-0">

                <!-- Group Smoker -->

                <?php if(empty($row['smoker_l']) || $row['smoker_l'] == 'NULL' ){}else{
                                        if($row['smoker_l'] == 'yes' ){
                                            $smoker_l = 'Yes';
                                        }elseif($row['smoker_l'] == 'no'){
                                            $smoker_l = 'No';
                                        }
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Smokers?</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $smoker_l ?> </p>
                  </div>


                </div>
                <?php } ?>

                <?php if(empty($row['children']) || $row['children'] == 'NULL' ){}else{
                                        if($row['children'] == 'yes' ){
                                            $children = 'Yes';
                                        }elseif($row['children'] == 'no'){
                                            $children = 'No';
                                        }
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Children?</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__children"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $children ?> </p>
                  </div>


                </div>
                <?php } ?>


                <?php if(empty($row['teenagers']) || $row['teenagers'] == 'NULL' ){}else{
                                        if($row['teenagers'] == 'yes' ){
                                            $teenagers = 'Yes';
                                        }elseif($row['teenagers'] == 'no'){
                                            $teenagers = 'No';
                                        }
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Teenagers?</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__children"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $teenagers ?> </p>
                  </div>


                </div>
                <?php } ?>


                <?php if(empty($row['pets']) || $row['pets'] == 'NULL' ){}else{
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Pets?</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $row['pets'] ?> </p>
                  </div>


                </div>
                <?php } ?>


                <?php if(empty($row['food']) || $row['food'] == 'NULL' ){}else{
                                        ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Do you require a special diet?</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__food"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $row['food'] ?> </p>
                  </div>


                </div>
                <?php } ?>


                <?php if($row['food'] == "Yes"){ ?>

                <?php if($row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{ ?>

                <!-- Group Special -->
                <div class="form__group form__group__basic form__group__special-diet" id="group__special-diet">

                  <div class="form__group__div-special__diet">
                    <div class="form__group__icon-input">

                      <label for="" class="icon"><i class="icon icon__diet"></i></label>
                      <h4 class="title__special-diet">Special Diet</h4>

                    </div>


                    <div class="form__group__row-diet">

                      <div class="custom-control custom-checkbox">
                        <?php if($row['vegetarians'] == "yes"){ ?>

                        <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians"
                          name="vegetarians" value="yes" checked>

                        <?php } else{ ?>

                        <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians"
                          name="vegetarians" value="yes" disabled>

                        <?php } ?>
                        <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                      </div>

                      <div class="custom-control custom-checkbox">

                        <?php if($row['halal'] == "yes"){ ?>

                        <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" checked>

                        <?php }else{ ?>

                        <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes"
                          disabled>

                        <?php } ?>

                        <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                      </div>

                    </div>

                    <div class="form__group__row-diet">

                      <div class="custom-control custom-checkbox">

                        <?php if($row['kosher'] == "yes"){ ?>

                        <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes"
                          checked>

                        <?php }else{ ?>

                        <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes"
                          disabled>

                        <?php } ?>
                        <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                      </div>

                      <div class="custom-control custom-checkbox">

                        <?php if($row['lactose'] == "yes"){ ?>

                        <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes"
                          checked>

                        <?php }else{ ?>

                        <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes"
                          disabled>

                        <?php } ?>
                        <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                      </div>

                    </div>

                    <div class="form__group__row-diet">

                      <div class="custom-control custom-checkbox">

                        <?php if($row['gluten'] == "yes"){ ?>

                        <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes"
                          checked>

                        <?php }else{ ?>

                        <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes"
                          disabled>

                        <?php } ?>
                        <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                      </div>

                      <div class="custom-control custom-checkbox">
                        <?php if($row['pork'] == "yes"){ ?>
                        <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" checked>
                        <?php }else{ ?>

                        <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" disabled>

                        <?php } ?>
                        <label class="custom-control-label" for="pork">No Pork</label>
                      </div>

                    </div>

                  </div>

                </div>

                <?php }} ?>



              </div>

              <h3 class="subtitle__section-card mt-4 mb-5">Transport</h3>

              <div class="row form__group-basic-content m-0 p-0">

                <?php if(empty($row['pick_up']) || $row['pick_up'] == 'NULL' ){}else{ 
                                        
                                        if($row['pick_up'] == 'yes'){
                                            $pick_up = "Yes";
                                        }else{
                                            $pick_up = "No";
                                        }
                                        
                                    
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Pick Up Service</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__nacionality"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $pick_up ?> </p>
                  </div>


                </div>
                <?php } ?>


                <?php if(empty($row['drop_off']) || $row['drop_off'] == 'NULL' ){}else{ 
                                        if($row['drop_off'] == 'yes'){
                                            $drop_off = "Yes";
                                        }else{
                                            $drop_off = "No";
                                        }    
                                        
                                    ?>

                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                  <div class="form__group__icon-input">

                    <label for="smoke_s" class="form__label smoker">Drop Off Service</label>

                    <label for="smoke_s" class="icon"><i class="icon icon__nacionality"></i></label>

                    <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $drop_off ?> </p>
                  </div>


                </div>
                <?php } ?>

              </div>
            </div>

          </div>

          <?php } ?>

        </div>
      </div><br>


    </main>

  </div>


  <!--FOOTER ===============================================================-->
  <footer id="ts-footer">
    <?php 
    include 'footer.php';
?>
  </footer>
  <!--end page-->


  <script src="assets/js/main.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>

</body>

</html>