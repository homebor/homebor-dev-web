// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

const $_HOMESTAY = {
  // ? GET ID OF HOUSE TO EDIT
  ID_HOME: D.getElementById("homestay-id").value,
  // ? CURRENT USER
  USER: D.getElementById("current-user"),
  // ? TEMPLATE ROOM
  $reserveTemplate: D.getElementById("reserve-template").content.cloneNode(true),
  // ? TEMPLATE BED
  $bedTemplate: D.getElementById("bed-template").content.cloneNode(true),
  // ? ALL ROOMS
  $allReserves: D.getElementById("all-rooms"),
};

// ? DATA SAVED
const dataSaved = [];

// TODO FUNCIONES
export default class Rooms {
  constructor(HOMESTAY = $_HOMESTAY) {
    // ? USER
    this.user = HOMESTAY.USER;
    // ? HOMESTAY
    this.ID = HOMESTAY.ID_HOME;
    // ? HOMESTAY ROOMS
    this.$reserveTemplate = HOMESTAY.$reserveTemplate;
    this.$bedTemplate = HOMESTAY.$bedTemplate;
    this.$allReserves = HOMESTAY.$allReserves;
  }

  // ? REQUERIR DATOS DE LAS HABITACIONES E INSERTARLAS
  async homestayRooms(idHome) {
    try {
      // * OBTENIENDO DATOS
      const DATA = new FormData();
      DATA.set("homestay_id", idHome);

      const OPTIONS = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: DATA,
      };

      const REQUEST = await axios("../helpers/homestay/rooms_data.php", OPTIONS);
      const RESPONSE = await REQUEST.data;

      // ** FRAGMENTO HTML A INSERTAR
      const $fragment = D.createDocumentFragment();

      // * AGREGANDO RESERVAS EXISTENTES
      for (const key in RESPONSE) {
        // ** ¡DATA! **
        const ROOM = RESPONSE[key];

        // ** TEMPLATES
        const $newReserve = this.$reserveTemplate.firstElementChild.cloneNode(true);
        $newReserve.dataset.room = ROOM.roomTitle.name;
        const $newBed = this.$bedTemplate.cloneNode(true);

        // ** TITLE DEL DOCUMENTO
        D.getElementsByTagName("title")[0].textContent = "Rooms " + ROOM.homestayFullName;

        // ** RELLENANDO HEADER
        this.roomHeader(ROOM.roomTitle, $newReserve);

        // ** RELLENANDO PHOTO
        const image1 = ROOM.roomImage.image1;
        const image2 = ROOM.roomImage.image2;
        const image3 = ROOM.roomImage.image3;

        this.roomPhoto(ROOM, [image1, image2, image3], $newReserve);

        // ** RELLENANDO FEATURES
        this.roomFeatures(ROOM.roomFeatures, $newReserve);

        // ** RELLENAR DETALLES DE CAMAS
        this.roomBedsInfo(ROOM.roomReservesInfo, ROOM.roomTitle, $newBed, $newReserve);

        // ** CAMAS RESERVADAS ACTUALMENTE
        if (this.user.dataset.view === "rooms") this.roomCurrentReserves(ROOM.roomTitle, $newReserve);

        // ** AGREGAR RESERVA
        $fragment.appendChild($newReserve);
      }

      $fragment.querySelectorAll('[data-avalible="true"]').forEach((reserve) => reserve.remove());
      $fragment.querySelectorAll('[data-null="true"]').forEach((reserve) => reserve.remove());
      $fragment.querySelectorAll("#bed").forEach((bed, i) => {
        if (
          bed.querySelector("#bed-type").textContent === "Disabled" ||
          bed.querySelector("#bed-type").textContent === "NULL"
        ) {
          bed.remove();
        }
      });
      $fragment.querySelectorAll("#beds-container").forEach((beds) => {
        if (beds.querySelectorAll("#bed").length === 0) {
          beds.classList.add("justify-content-center");
          beds.querySelector("#students-img").classList.remove("d-none");
        }
      });

      // * AGREGAR TODAS LAS RESERVAS
      this.$allReserves.appendChild($fragment);
    } catch (error) {
      console.error("Ha ocurrido un error:", error);
    } finally {
      if (this.user.dataset.view === "rooms") return;
      if (D.querySelector("#total-rooms")) {
        D.querySelector("#total-rooms").value = D.querySelectorAll("#reserve").length;
      }
      if (D.querySelectorAll("#reserve").length === 8) {
        if (this.user.dataset.view === "edit-rooms") {
          D.querySelector("#add-new-room").remove();
        } else D.querySelector("#add-new-room").parentElement.remove();
      }
    }
  }

  /** // ? RELLENAR HEADER DE LA HABITACIÓN
   * @param {Object} roomTitle Nombre de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomHeader(roomTitle, reserveTemplate) {
    const $roomHeader = reserveTemplate.querySelector("#reserve-header");
    const $roomName = reserveTemplate.querySelector("#reserve-header h3");
    const $roomPrice = reserveTemplate.querySelector("#reserve-header p");
    const $homestayPrice = reserveTemplate.querySelector("#homestay-price");
    const $providerPrice = reserveTemplate.querySelector("#provider-price");

    $roomHeader.style.background = roomTitle.color;
    $roomName.textContent = roomTitle.name;
    if (this.user.value === "homestay") $roomPrice.textContent = `CAD$ ${roomTitle.priceHomestay}`;
    else $roomPrice.textContent = `CAD$ ${parseInt(roomTitle.priceHomestay) + parseInt(roomTitle.priceAgent)}`;

    if ($homestayPrice) $homestayPrice.value = roomTitle.priceHomestay;
    if ($providerPrice) $providerPrice.value = roomTitle.priceAgent;
  }

  /** // ? FILL IMAGES OF ROOM
   * @param {Object} roomData Datos de la habitacion
   * @param {Array} images Imagenes a insertar
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomPhoto(roomData, images, reserveTemplate) {
    const $fragment = D.createDocumentFragment();

    const $carouselContainer = D.createElement("div");
    $carouselContainer.id = roomData.roomTitle.name.replace(" ", "-");
    $carouselContainer.className = "w-100 carousel slide";
    $carouselContainer.dataset.ride = "carousel";

    const $indicators = D.createElement("ol");
    $indicators.className = "carousel-indicators";

    const $indicator = D.createElement("li");
    $indicator.dataset.target = `#${$carouselContainer.id}`;

    const $imagesContainer = D.createElement("div");
    $imagesContainer.className = "w-100 carousel-inner";

    const $btnPrevious = D.createElement("a");
    $btnPrevious.className = "carousel-control-prev";
    $btnPrevious.dataset.slide = "prev";
    $btnPrevious.setAttribute("role", "button");
    $btnPrevious.href = `#${$carouselContainer.id}`;

    const $btnNext = D.createElement("a");
    $btnNext.className = "carousel-control-next";
    $btnNext.dataset.slide = "next";
    $btnNext.setAttribute("role", "button");
    $btnNext.href = `#${$carouselContainer.id}`;

    images.forEach((image, i) => {
      $indicator.dataset.slideTo = i;
      $indicators.appendChild($indicator.cloneNode(true));
      $indicators.firstElementChild.className = "active";

      const $imageItem = D.createElement("div");
      $imageItem.className = "w-100 carousel-item";
      const $image = D.createElement("img");
      const $photoAdd = D.createElement("label");
      const $photoAddInput = D.createElement("input");

      $photoAdd.setAttribute("for", `${roomData.roomTitle.name} Image ${i + 1}`);
      $photoAdd.className = "photo-add";

      $photoAddInput.id = `${roomData.roomTitle.name} Image ${i + 1}`;
      !image || image === "NULL" || image === "null"
        ? ($photoAdd.innerHTML = `<img src="../assets/emptys/room-empty.png" width="100px" height="100px" alt="">`)
        : ($photoAdd.innerHTML = `<img src="../../${image}" alt="">`);
      $photoAddInput.value = "";
      $photoAddInput.hidden = true;
      $photoAddInput.type = "file";

      $imageItem.appendChild($photoAdd);
      $imageItem.appendChild($photoAddInput);

      $imagesContainer.appendChild($imageItem);
      $imagesContainer.firstElementChild.classList.add("active");

      $btnPrevious.innerHTML = `<span class="carousel-control-prev-icon mr-5" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>`;
      $btnNext.innerHTML = `<span class="carousel-control-next-icon ml-5" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>`;

      $carouselContainer.appendChild($indicators);
      $carouselContainer.appendChild($imagesContainer);
      $carouselContainer.appendChild($btnPrevious);
      $carouselContainer.appendChild($btnNext);

      $fragment.appendChild($carouselContainer);
    });

    if (reserveTemplate.querySelector("#reserve-photo").children[2]) {
      reserveTemplate.querySelector("#reserve-photo").children[2].remove();
      reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
    } else reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
  }

  /** // ? FILL FEATURES OF ROOM (TYPE ROOM, FOOD SERVICE)
   * @param {Object} features Caracteristicas de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomFeatures(features, reserveTemplate) {
    const $typeRoom = reserveTemplate.querySelector("#type-room");
    const $roomFoodService = reserveTemplate.querySelector("#food-service");

    if (this.user.dataset.view === "rooms") {
      $typeRoom.textContent = features.type;
      $roomFoodService.textContent = features.food;
    } else {
      $typeRoom.value = features.type;
      $roomFoodService.value = features.food;
    }
  }

  /** // ? FILL THE BEDS AND INSERT TO ROOM
   * @param {Object} roomBeds Informacion de las camas de la habitacion
   * @param {Object} roomTitle Nombre de la habitacion
   * @param {Element} bedTemplate Plantilla de las camas de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomBedsInfo(roomBeds, roomTitle, bedTemplate, reserveTemplate) {
    const ALL_BEDS = Object.values(roomBeds.allBeds) || [];
    dataSaved.push([roomTitle.name, roomBeds.allBedsReserved]);

    const $fragment = D.createDocumentFragment();
    const $bedsContainer = reserveTemplate.querySelector("#beds-container");
    if (ALL_BEDS && ALL_BEDS instanceof Array) {
      let bedsNull = 0;
      let bedsDisabled = 0;

      ALL_BEDS.forEach((bed) => {
        const ROOM = bed[0];
        const TYPE_ROOM = bed[4];
        let TYPE_BED;
        bed[1] === "Bunk-bed" || bed[1] === "Bunker" ? (TYPE_BED = "Bunk") : (TYPE_BED = bed[1]);

        let BED;
        if (bed[2] === "A") BED = "Bed 1";
        if (bed[2] === "B") BED = "Bed 2";
        if (bed[2] === "C") BED = "Bed 3";
        const STATUS = bed[3];

        if (bed[3] === "Disabled") bedsDisabled++;
        if (bed[3] === "NULL") bedsNull++;

        bedTemplate.querySelector("#bed-letter").textContent = BED;

        if (this.user.dataset.view === "rooms") bedTemplate.querySelector("#bed-type").textContent = TYPE_BED;

        if (this.user.dataset.view === "edit-rooms" || this.user.dataset.view === "edit-property") {
          bedTemplate.querySelector("#bed-type").name = BED;
          if (STATUS === "Available" || STATUS === "Disabled") {
            bedTemplate.querySelector("#bed").classList.add("added");
            // ** VERIFICAR SI HAY UNA TERCERA CAMA
            if (BED === "Bed 3") bedTemplate.querySelector("#bed").classList.add("more");
            // ** VERIFICAR SI ES AVAILABLE O DISABLED PARA DESHABILITAR SELECTS
            if (STATUS === "Available") bedTemplate.querySelector("#bed select").disabled = false;
            else bedTemplate.querySelector("#bed select").disabled = true;
            // ** DARLE VALUE A LAS CAMAS
            bedTemplate.querySelectorAll("#bed select option").forEach((option) => {
              if (option.value === TYPE_BED) option.setAttribute("selected", "");
              else option.removeAttribute("selected");
            });
          } else if (STATUS === "NULL") {
            bedTemplate.querySelector("#bed select").disabled = false;
            if (BED === "Bed 1") bedTemplate.querySelector("#bed").classList.add("added");
            else bedTemplate.querySelector("#bed").classList.remove("added");
            if (TYPE_ROOM === "Share") bedTemplate.querySelector("#bed").classList.add("added");
          }
        }

        let $clone = D.importNode(bedTemplate, true);

        $fragment.appendChild($clone);
      });

      if (this.user.dataset.view === "rooms" && (bedsDisabled === 3 || bedsNull === 3)) {
        $bedsContainer.parentElement.dataset.avalible = "true";
      }
      if (this.user.dataset.view === "edit-rooms" && bedsNull === 3) {
        $bedsContainer.parentElement.parentElement.dataset.null = "true";
      }
      if (this.user.dataset.view === "edit-property" && bedsNull === 3) {
        $bedsContainer.parentElement.parentElement.dataset.null = "true";
      }

      // * AGREGAR CAMA
      $bedsContainer.appendChild($fragment);
    }
  }

  // ? ROOM RESERVES LIST (VIEW ROOMS PROFILE HOMESTAY)
  roomReservesList(roomHeader, modal) {
    modal.classList.add("show");

    const $roomTitle = roomHeader.querySelector("h3").textContent;
    const $roomColor = roomHeader.style.background;

    modal.querySelector("h3").style.background = $roomColor;
    modal.querySelector("h3").textContent = $roomTitle;

    const RESERVE = [];
    dataSaved.forEach((reserve, i) => (reserve[1] && reserve[0] === $roomTitle ? RESERVE.push(reserve) : {}));

    if (RESERVE.length <= 0) return;

    if (RESERVE[0][0] === $roomTitle) {
      RESERVE[0][1].forEach((bedInfo) => {
        const name = bedInfo[0];
        const bed = () => {
          if (bedInfo[1] === "A") return "Bed 1";
          if (bedInfo[1] === "B") return "Bed 2";
          if (bedInfo[1] === "C") return "Bed 3";
        };
        const firstDay = bedInfo[2];
        const lastDay = bedInfo[3];
        const img = `<img src="../../${bedInfo[4]}" width="45px" height="45px" alt="profile" rel="noopener" style="pointer-events:none;">`;
        const studentId = bedInfo[5];

        const $tr = `
          <tr data-student-profile="student_info?art_id=${studentId}">
            <td>${img}</td>
            <td>${name}</td>
            <td>${bed()}</td>
            <td>${firstDay}</td>  
            <td>${lastDay}</td>
          </tr>
        `;
        modal.querySelector("table tbody").innerHTML += $tr;
        if (modal.querySelector("table tbody").children.length > 6) {
          modal.querySelector(".modal-content").classList.add("show-scroll");
        }
      });

      const allFirstDaysUnix = [];
      D.querySelectorAll("table tbody tr").forEach((tr) => {
        allFirstDaysUnix.push(new Date(tr.children[3].textContent).getTime());
      });

      const order = allFirstDaysUnix.sort();
      while (order.length > 0) {
        D.querySelectorAll("table tbody tr").forEach((tr) => {
          if (order[0] === new Date(tr.children[3].textContent).getTime()) {
            tr.parentElement.appendChild(tr);
            order.shift();
          }
        });
      }
    }
  }

  roomCurrentReserves(roomTitle, reserveTemplate) {
    const roomName = roomTitle.name;

    const RESERVE = [];
    dataSaved.forEach((reserve, i) => (reserve[1] && reserve[0] === roomName ? RESERVE.push(reserve) : {}));

    if (RESERVE.length > 0 && RESERVE[0][0] === roomName) {
      const allFirstDaysUnix = [];
      RESERVE[0][1].forEach((bedInfo) => allFirstDaysUnix.push(new Date(bedInfo[2]).getTime()));

      const traveled = [];
      const reservesData = new Map();
      const order = allFirstDaysUnix.sort();
      order.forEach((date) => {
        RESERVE[0][1].forEach((bedInfo) => {
          if (traveled.includes(bedInfo[1])) return;

          const dateUnix = new Date().getTime();
          const firstDayUnix = new Date(bedInfo[2]).getTime();
          const lastDayUnix = new Date(bedInfo[3]).getTime();

          if (date === firstDayUnix && dateUnix >= firstDayUnix && dateUnix <= lastDayUnix) {
            traveled.push(bedInfo[1]);
            reservesData.set(bedInfo[1], bedInfo);
          }
        });
      });

      if (reserveTemplate.querySelector("h3").textContent === roomName) {
        const renderStudent = (bed, bedContainer, data) => {
          bed.classList.replace("d-none", "d-flex");
          bedContainer.classList.replace("justify-content-center", "justify-content-between");
          bedContainer.querySelector("#student-img").src = "../../" + data[4];
          bedContainer.querySelector("#student-name").textContent = data[0];
          bedContainer.querySelector("#student-date").textContent = `${data[2]} - ${data[3]}`;
        };
        reserveTemplate.querySelectorAll("#beds-container .student-info").forEach((elem) => {
          const bed = elem.parentElement.querySelector("#bed-letter").textContent;
          if (bed === "Bed 1" && traveled.includes("A")) renderStudent(elem, elem.parentElement, reservesData.get("A"));
          if (bed === "Bed 2" && traveled.includes("B")) renderStudent(elem, elem.parentElement, reservesData.get("B"));
          if (bed === "Bed 3" && traveled.includes("C")) renderStudent(elem, elem.parentElement, reservesData.get("C"));
        });
      }
    }

    if (reserveTemplate.querySelectorAll("#beds-container .student-info.d-flex").length === 3) {
      reserveTemplate.querySelector(".status-detail").classList.add("unavailable");
    } else reserveTemplate.querySelector(".status-detail").classList.add("available");
  }

  /** // ? SAVE INFORMATION OF BEDROOMS
   * @param {Array} room Todas las habitacion a guardar
   * @param {Number|String} id ID de la casa a editar
   */
  async saveBedrooms(room, id) {
    const roomsSaved = {};

    room.forEach(async (info, i) => {
      try {
        const $roomNumber = i + 1;
        const $imageInput = info.querySelectorAll(".carousel-item input");
        const $selects = info.querySelectorAll("select");
        const $priceHomestay = info.querySelector("#homestay-price");
        const $priceAgent = info.querySelector("#provider-price");

        const roomData = new FormData();
        roomData.set("homestay_id", id);
        roomData.set("roomNumber", $roomNumber);

        if ($imageInput[0]) roomData.set("image1", $imageInput[0].files[0]);
        if ($imageInput[1]) roomData.set("image2", $imageInput[1].files[0]);
        if ($imageInput[2]) roomData.set("image3", $imageInput[2].files[0]);

        if ($selects && $selects[0].value) roomData.set("typeRoom", $selects[0].value);
        if ($selects && $selects[1].value) roomData.set("food", $selects[1].value);

        const verifyBed = (bed) => (bed === "Bunk" ? "Bunk-bed" : bed);
        if ($selects[2] && $selects[2].value) roomData.set("bed1", verifyBed($selects[2].value));
        if ($selects[3] && $selects[3].value) roomData.set("bed2", verifyBed($selects[3].value));
        if ($selects[4] && $selects[4].value) roomData.set("bed3", verifyBed($selects[4].value));

        // if ($priceAgent) {
        //   roomData.set("priceHomestay", $priceHomestay.value || 0);
        //   roomData.set("priceAgent", $priceAgent.value || 0);
        // } else {
        roomData.set("price", $priceHomestay.value || 0);
        // }

        const options = {
          method: "POST",
          headers: { "Content-type": "application/json; charset=utf-8" },
          data: roomData,
        };

        let fetchURL;
        if (this.user.dataset.view === "edit-rooms") fetchURL = "../helpers/homestay/edit_rooms.php";
        if (this.user.dataset.view === "edit-property") fetchURL = "../helpers/homestay/edit_rooms.php";
        const res = await axios(fetchURL, options);
        const data = await res.data;
        console.log(data);
        if (D.querySelector(`[data-room="${data[0]}"]`)) {
          roomsSaved[`Room ${$roomNumber}`] = false;
          D.querySelector(`[data-room="${data[0]}"]`).classList.add("required-fields");
        }

        // * ERROR EN LA PROGRAMACION, CLIENTE, RED O SERVIDOR
        if (data === "error") {
          _Messages.showMessage("An error occurred while saving the rooms.", 4, false, ".rooms-container");
        }

        // * ERRORES MANEJADOS
        if (data[1] === "no-type-room") D.querySelector(`[data-room="${data[0]}"] #type-room`).classList.add("invalid");
        if (data[1] === "no-shared-bed") {
          D.querySelectorAll(`[data-room="${data[0]}"] #bed`).forEach((bed, i) => {
            if (i == 2) return;
            bed.classList.add("no-bed");
            bed.dataset.noBed = "Select type bed";
          });
        }
        if (data[1] === "no-bed") D.querySelector(`[data-room="${data[0]}"] #bed`).classList.add("no-bed");
        if (data[1] === "price-homestay-invalid") {
          D.querySelector(`[data-room="${data[0]}"] #homestay-price`).classList.add("invalid");
        }
        if (data[1] === "price-agent-invalid") {
          D.querySelector(`[data-room="${data[0]}"] #provider-price`).classList.add("invalid");
        } // * MENSAJE DE GUARDADO SATISFACTORIO
        if (data.status === true) {
          roomsSaved[`Room ${$roomNumber}`] = true;
          D.querySelector(`[data-room="${data.room}"]`).classList.remove("required-fields");
          D.querySelector(`[data-room="${data.room}"]`).classList.add("saved");
          setTimeout(() => D.querySelector(`[data-room="${data.room}"]`).classList.remove("saved"), 4000);
        }
      } catch (error) {
        console.error("Ha ocurrido un error al guardar los cambios!:", error);
      } finally {
        if (i + 1 === D.querySelectorAll("[data-room]").length) {
          let allSaved = true;
          Object.values(roomsSaved).forEach((room) => (room === false ? (allSaved = false) : {}));

          if (allSaved) {
            _Messages.showMessage(`${D.querySelector("#house-name").value} has been successfully modified`, 2, 3);
            if (this.user.dataset.view === "edit-rooms") setTimeout(() => window.location.reload(), 2000);
            else if (this.user.dataset.view === "edit-property") setTimeout(() => window.location.reload(), 2000);
            else setTimeout(() => (window.location.href = "edit_propertie"), 2000);
          } else {
            _Messages.quitMessage();
            D.querySelector("#modal-dynamic").classList.add("show");

            D.querySelector("#modal-dynamic").innerHTML = `
              <div class="d-flex flex-column text-center justify-content-center save-changes">
                <h4>You have not finished configuring the rooms, do you want <br> 
                to save the changes without the rooms?</h4> 
                <div class="d-flex align-items-center justify-content-center">
                  <button id="save-changes-yes" disabled class="btn w-25 mx-2 enable-room">Yes</button>
                  <button id="save-changes-no" disabled class="btn w-25 mx-2 disable-room">No</button>
                </div>
              </div>
            `;

            setTimeout(() => {
              D.querySelector("#save-changes-yes").disabled = false;
              D.querySelector("#save-changes-no").disabled = false;
            }, 500);

            D.querySelector("#save-changes-no").addEventListener("click", (e) => {
              D.querySelector("#modal-dynamic").classList.remove("show");
              D.querySelector("#modal-dynamic").style.cssText = "";
              D.querySelector(`.required-fields`).scrollIntoView({ block: "start", behavior: "smooth" });
            });

            D.querySelector("#save-changes-yes").addEventListener("click", (e) => {
              D.querySelector("#modal-dynamic").classList.remove("show");

              _Messages.showMessage(`${D.querySelector("#house-name").value} has been successfully modified`, 2, 3);
              if (this.user.dataset.view === "edit-rooms") setTimeout(() => window.location.reload(), 2000);
              else if (this.user.dataset.view === "edit-property") setTimeout(() => window.location.reload(), 2000);
              else setTimeout(() => (window.location.href = "edit_propertie"), 2000);
            });
          }
        }
      }
    });
  }

  /** // ? ADD NEW ROOM
   * @param {String} reference Evento que ejecuta la funcion
   */
  addNewRoom(reference) {
    const $allRooms = D.getElementById("all-rooms");
    const $template = D.getElementById("reserve-template").content;

    let $lastRoomName;
    if ($allRooms.querySelector("#reserve-header")) {
      $lastRoomName = parseInt($allRooms.lastElementChild.querySelector("#reserve-header h3").textContent.slice(-1));
    } else $lastRoomName = 0;

    const $lastRoom = D.querySelector(`[data-room="Room ${$lastRoomName}"]`);

    const ADD_ROOM = () => {
      const ROOM_PREV = $lastRoomName + 1;
      let roomColor;

      if (ROOM_PREV > 0 && ROOM_PREV <= 8) {
        try {
          if (ROOM_PREV == 1) roomColor = "#232159";
          if (ROOM_PREV == 2) roomColor = "#982A72";
          if (ROOM_PREV == 3) roomColor = "#394893";
          if (ROOM_PREV == 4) roomColor = "#A54483";
          if (ROOM_PREV == 5) roomColor = "#5D418D";
          if (ROOM_PREV == 6) roomColor = "#392B84";
          if (ROOM_PREV == 7) roomColor = "#B15391";
          if (ROOM_PREV == 8) roomColor = "#4F177D";

          $template.querySelector("#reserve").dataset.room = "Room " + ROOM_PREV;
          $template.querySelector("#reserve-header").style.background = roomColor;
          $template.querySelector("#reserve-header h3").textContent = "Room " + ROOM_PREV;
          this.roomPhoto({ roomTitle: { name: "Room " + ROOM_PREV } }, [false, false, false], $template);

          const $bedHTML = `
          <div id="bed" class="my-lg-auto rounded d-flex justify-content-center bed-info-homestay">

              <div class="d-flex">
                <div class="w-100 h-50 d-flex align-items-center justify-content-center">
                  <img src="../assets/icon/cama 64.png" width="25px" height="25px" alt="">
                  <select name="Bed 1" id="bed-type" class="px-2" disabled="" required="">
                    <option value="NULL">Type bed</option>
                    <option value="Twin">Twin</option>
                    <option value="Bunk">Bunk</option>
                    <option value="Double">Double</option>
                  </select>
                </div>

                <div class="d-flex align-items-center justify-content-center">
                  <span id="bed-letter" class="text-white">Bed 1</span>
                </div>
              </div>

              <button id="add-bed" class="d-none" title="Add bed">+</button>
            </div>
          `;

          $template.querySelector("#beds-container").innerHTML = `${$bedHTML}${$bedHTML}${$bedHTML}`;

          $allRooms.appendChild(D.importNode($template, true));

          return;
        } catch (error) {
          console.error("Ha ocurrido un error: " + error);
        } finally {
          if (reference !== "toInput") {
            if (D.querySelector("#total-rooms")) {
              D.querySelector("#total-rooms").value = $allRooms.querySelectorAll("#reserve").length;
            }
            if (D.querySelectorAll("#reserve").length === 8) {
              if (this.user.dataset.view === "edit-rooms") {
                D.querySelector("#add-new-room").remove();
              } else D.querySelector("#add-new-room").parentElement.remove();
            }
          }
        }
      }
    };

    if (!$lastRoom) ADD_ROOM();
    else if (
      $lastRoom.querySelector("#type-room").value === "NULL" ||
      $lastRoom.querySelector("#bed-type").value === "NULL" ||
      (!$lastRoom.querySelector("#homestay-price").value && !$lastRoom.querySelector("#provider-price").value) ||
      ($lastRoom.querySelector("#homestay-price").value <= 0 && $lastRoom.querySelector("#provider-price").value <= 0)
    ) {
      _Messages.showMessage(
        "Please complete the information of the previous room to add more.",
        4,
        false,
        `[data-room="Room ${$lastRoomName}"]`
      );
    } else ADD_ROOM();
  }

  /** // ? VERIFY, ENABLE OR DISABLE ROOM
   * @param {Element} btn Boton que origino el evento
   * @param {String|Number} status Valor a colocar en el status de la habitacion
   */
  async statusRoom(btn, status) {
    // * Si existe btn y status habilitar o deshabilitar habitacion
    if (btn && status) {
      try {
        const roomNumber = btn.parentElement.parentElement.parentElement
          .querySelector("#reserve-header h3")
          .textContent.slice(-1);

        const roomData = new FormData();
        roomData.set("homestay_id", this.ID);
        roomData.set("roomNumber", roomNumber);
        roomData.set("roomStatus", status);

        const options = {
          method: "POST",
          headers: { "Content-type": "application/json; charset=utf-8" },
          data: roomData,
        };

        const res = await axios("../helpers/homestay/disable_room.php", options);
        const data = await res.data;

        this.statusRoom();
      } catch (error) {
        console.error("Ha ocurrido un error: " + error);
      }
    }
    // * Caso contrario verificar el status de las habitaciones
    else {
      const roomData = new FormData();
      roomData.set("homestay_id", this.ID);
      roomData.set("verify", true);

      const options = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("../helpers/homestay/disable_room.php", options);
      const data = await res.data;

      let i = 1;
      for (const key in data) {
        if (key.includes("date")) {
          const roomName = "Room " + i;
          const date1 = data[`date${i}`];
          const date2 = data[`date${i}_2`];
          const date3 = data[`date${i}_3`];
          const $allRooms = D.querySelectorAll(`[data-room]`);

          if (
            (date1 === "Disabled" || date1 === "NULL") &&
            (date2 === "Disabled" || date2 === "NULL") &&
            (date3 === "Disabled" || date3 === "NULL")
          ) {
            $allRooms.forEach((room) => {
              if (room.dataset.room === roomName) {
                room.querySelector("#disable-room").classList.add("d-none");
                room.querySelector("#enable-room").classList.remove("d-none");
              }
            });
          } else {
            $allRooms.forEach((room) => {
              if (room.dataset.room === roomName) {
                room.querySelector("#disable-room").classList.remove("d-none");
                room.querySelector("#enable-room").classList.add("d-none");
              }
            });
          }

          if (i === 8) break;
          else i++;
        }
      }
    }
  }

  // ! TODO STATIC
  /** // ? CHANGE TYPE OF ROOM AND MODIFY IN THIS REGARD
   * @param {Element} typeRoom Elemento de Type Room de la habitacion
   */
  static editBeds(typeRoom) {
    const $bedsContainer = typeRoom.parentElement.parentElement.parentElement.parentElement.parentElement;
    const selectValue = typeRoom.value;

    if (selectValue === "Single" || selectValue === "Executive") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        if (i === 1 || i === 2) {
          bed.classList.remove("added", "more");
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = true));
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = "NULL"));
        }
      });
    } else if (selectValue === "Share") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = "NULL"));
        if (i === 2) bed.classList.remove("more");
      });
    } else {
      $bedsContainer.querySelectorAll("#bed").forEach((bed) => bed.classList.remove("added"));
      $bedsContainer.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = "false"));
    }
  }

  /** // ? ADD THE BED BY GIVING THE MORE BUTTON (+)
   * @param {Element} btn Elemento select a reemplazar por el icono +
   */
  static addBed(btn) {
    const $bed = btn.parentElement;
    const $select = btn.parentElement.querySelector("select");

    $bed.classList.add("more");
    $select.disabled = false;
  }
}

/** // ? CONFIRM CHANGE OF ROOM STATUS
 * @param {String} modal Selector de elemente HTML del modal a mostrar
 * @param {String} message Mensaje a mostrar al usuario
 * @param {String|Number} status Valor a colocar en el status de la habitacion
 * @param {Element} btn Elemento que genero el evento
 */
const statusMessage = (modal, message, status, btn) => {
  let $html = `
      <div class="d-flex flex-column align-center justify-content-center confirm-status">
        <h4>Are you sure you want to ${message} the room?</h4>
        <div class="d-flex align-items-center justify-content-center">
          <button id="enable-room-yes" class="btn w-25 mx-2 enable-room">Yes</button>
          <button id="disable-room-no" class="btn w-25 mx-2 disable-room">No</button>
        </div>
      </div>
      `;

  dataSaved.forEach((data) => {
    const $currentRoom = btn.parentElement.parentElement.parentElement.parentElement.dataset.room;
    if (data[0] === $currentRoom) {
      if (data[1] && data[1].length > 0) {
        $html = `
          <div class="d-flex flex-column align-center justify-content-center confirm-status">
            <h4>There are active or pending reservations, you cannot disable this room</h4>
            <div class="d-flex align-items-center justify-content-center">
            <button id="disable-room-no" class="btn w-25 mx-2 bg-secondary text-white disable-room">Close</button>
            </div>
          </div>
          `;
      } else return;
    }
  });

  D.querySelector(modal).classList.add("show");
  D.querySelector(modal).innerHTML = $html;

  if (D.getElementById("enable-room-yes")) {
    D.getElementById("enable-room-yes").onclick = () => {
      new Rooms().statusRoom(btn, status);
      D.querySelector(modal).classList.remove("show");
    };
  }

  if (D.getElementById("disable-room-no")) {
    D.getElementById("disable-room-no").onclick = () => D.querySelector(modal).classList.remove("show");
  }
};

// TODO EVENTOS
// ! CLICK
D.addEventListener("click", (e) => {
  // ? AGREGAR CAMA
  if (e.target.matches("#add-bed")) Rooms.addBed(e.target);
  // ? EDITAR FOTO DE HABITACION
  if (e.target.id === "edit-photo-btn") e.target.parentElement.querySelector(".carousel-item.active input").click();
  // ? AÑADIR OTRA HABITACION
  if (e.target.id === "add-new-room") new Rooms().addNewRoom();
  // ? HABILITAR HABITACION
  if (e.target.id === "enable-room") statusMessage("#modal-dynamic", "enable", "0", e.target);
  // ? DESHABILITAR HABITACION
  if (e.target.id === "disable-room") statusMessage("#modal-dynamic", "disable", "-1", e.target);
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? EDIT BEDS
  if (e.target.matches("#type-room")) Rooms.editBeds(e.target);
  // ? SELECT OPTION WITH THE VALUE OF SELECT
  if (e.target.matches("#bed-type")) {
    e.target.querySelectorAll("option").forEach((option) => {
      option.value === e.target.value ? option.setAttribute("selected", "") : option.removeAttribute("selected");
    });
    e.target.parentElement.parentElement.parentElement.classList.remove("no-bed");
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
      "required-fields"
    );
  }
});
