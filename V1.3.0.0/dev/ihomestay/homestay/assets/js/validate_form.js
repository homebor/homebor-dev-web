/* ----------------------------------- FUNCTIONS OF THE FORMULARY --------------------------------------------- */

const D = document;

const $divSelectType = D.querySelectorAll(".type-room");

document.getElementById("num").onchange = function () {
  numberPhone();
};

function numberPhone() {
  let numberPhone = D.getElementById("num").value;

  D.getElementById("cell").value = numberPhone;
}

// Eye Function

let password = document.getElementById("pass");
let viewPassword = document.getElementById("btn__eye");
let click = false;

viewPassword.addEventListener("click", (e) => {
  if (!click) {
    password.type = "text";
    $("#btn__eye").removeClass("fa-eye").addClass("fa-eye-slash");
    click = true;
  } else if (click) {
    password.type = "password";
    $("#btn__eye").removeClass("fa-eye-slash").addClass("fa-eye");
    click = false;
  }
});

const form = document.querySelectorAll("#form button");

const BEDS = [];

/* const allButtons = {
  step_1Next: D.getElementById("next_step-1"),
  step_2Next: D.getElementById("next_step-2"),
  step_2Back: D.getElementById("back_step-1"),
  step_3Next: D.getElementById("next_step-4"),
  step_3Back: D.getElementById("back_step-2"),
  step_4Back: D.getElementById("back_step-3"),
};

const allCheckMeals = {
  private3Meals: D.getElementById("check1"),
  private2Meals: D.getElementById("check2"),
  shared3Meals: D.getElementById("check3"),
  shared2Meals: D.getElementById("check4"),
  onlyPrivate: D.getElementById("check5"),
  onlyShared: D.getElementById("check6"),
}; */

// TODO FETCHS REGISTER HOMESTAY

//* STEP 1

async function fetchStep1(form, btn) {
  const formStep = new FormData();
  formStep.set("h_name", form.h_name.value);
  formStep.set("num", form.num.value);
  formStep.set("room", form.room.value);
  formStep.set("m_city", form.m_city.value);
  formStep.set("mail", form.mail.value);
  formStep.set("pass", form.pass.value);
  formStep.set("pet", form.pet.value);
  formStep.set("pet_num", form.pet_num.value);
  if (form.dog.checked === true) formStep.set("dog", form.dog.value);
  else formStep.set("dog", "no");
  if (form.cat.checked === true) formStep.set("cat", form.cat.value);
  else formStep.set("cat", "no");
  if (form.other.checked === true) formStep.set("other", form.other.value);
  else formStep.set("other", "no");
  formStep.set("type_pet", form.type_pet.value);
  formStep.set("ag_pre", form.ag_pre.value);
  formStep.set("g_pre", form.g_pre.value);
  formStep.set("value_step", form.value_step.value);

  const DATA = { method: "POST", body: formStep };

  const jsonStep1 = await fetch("./action.php", DATA);
  const reusultStep1 = await jsonStep1.json();

  reusultStep1.forEach((response) => {
    if (response.register === "Registered") stepButtons(btn);
    if (response.register === "Error") alert("There was a problem, please try again");
  });
}

// * STEP 2

async function fetchStep2(form, btn) {
  const formStep = new FormData();
  formStep.set("dir", form.dir.value);
  formStep.set("city", form.city.value);
  formStep.set("state", form.state.value);
  formStep.set("p_code", form.p_code.value);
  formStep.set("y_service", form.y_service.value);
  formStep.set("num_mem", form.num_mem.value);
  formStep.set("backl", form.backl.value);

  if (form.vegetarians.checked === true) formStep.set("vegetarians", form.vegetarians.value);
  else formStep.set("vegetarians", "no");
  if (form.halal.checked === true) formStep.set("halal", form.halal.value);
  else formStep.set("halal", "no");
  if (form.kosher.checked === true) formStep.set("kosher", form.kosher.value);
  else formStep.set("kosher", "no");
  if (form.lactose.checked === true) formStep.set("lactose", form.lactose.value);
  else formStep.set("lactose", "no");
  if (form.gluten.checked === true) formStep.set("gluten", form.gluten.value);
  else formStep.set("gluten", "no");
  if (form.pork.checked === true) formStep.set("pork", form.pork.value);
  else formStep.set("pork", "no");
  if (form.none.checked === true) formStep.set("none", form.none.value);
  else formStep.set("none", "no");

  if (form.h_type.value !== "-- Select Option --") formStep.set("h_type", form.h_type.value);
  if (form.m_service.value !== "- Select -") formStep.set("m_service", form.m_service.value);

  // ? ROOMS

  if (form.bed_i) {
    formStep.set("bed-i", form.bed_i.files[0]);
    formStep.set("bed-i-ii", form.bed_i_ii.files[0]);
    formStep.set("bed-i-iii", form.bed_i_iii.files[0]);

    formStep.set("type1", form.type1.value);
    formStep.set("food1", form.food1.value);
    formStep.set("approx1", form.approx1.value);
    formStep.set("bed1_1", form.bed1_1.value);
    formStep.set("bed1_2", form.bed1_2.value);
    formStep.set("bed1_3", form.bed1_3.value);
  }
  if (form.bed_ii) {
    formStep.set("bed-ii", form.bed_ii.files[0]);
    formStep.set("bed-ii-ii", form.bed_ii_ii.files[0]);
    formStep.set("bed-ii-iii", form.bed_ii_iii.files[0]);

    formStep.set("type2", form.type2.value);
    formStep.set("food2", form.food2.value);
    formStep.set("approx2", form.approx2.value);
    formStep.set("bed2_1", form.bed2_1.value);
    formStep.set("bed2_2", form.bed2_2.value);
    formStep.set("bed2_3", form.bed2_3.value);
  }
  if (form.bed_iii) {
    formStep.set("bed-iii", form.bed_iii.files[0]);
    formStep.set("bed-iii-ii", form.bed_iii_ii.files[0]);
    formStep.set("bed-iii-iii", form.bed_iii_iii.files[0]);

    formStep.set("type3", form.type3.value);
    formStep.set("food3", form.food3.value);
    formStep.set("approx3", form.approx3.value);
    formStep.set("bed3_1", form.bed3_1.value);
    formStep.set("bed3_2", form.bed3_2.value);
    formStep.set("bed3_3", form.bed3_3.value);
  }
  if (form.bed_iv) {
    formStep.set("bed-iv", form.bed_iv.files[0]);
    formStep.set("bed-iv-ii", form.bed_iv_ii.files[0]);
    formStep.set("bed-iv-iii", form.bed_iv_iii.files[0]);

    formStep.set("type4", form.type4.value);
    formStep.set("food4", form.food4.value);
    formStep.set("approx4", form.approx4.value);
    formStep.set("bed4_1", form.bed4_1.value);
    formStep.set("bed4_2", form.bed4_2.value);
    formStep.set("bed4_3", form.bed4_3.value);
  }
  if (form.bed_v) {
    formStep.set("bed-v", form.bed_v.files[0]);
    formStep.set("bed-v-ii", form.bed_v_ii.files[0]);
    formStep.set("bed-v-iii", form.bed_v_iii.files[0]);

    formStep.set("type5", form.type5.value);
    formStep.set("food5", form.food5.value);
    formStep.set("approx5", form.approx5.value);
    formStep.set("bed5_1", form.bed5_1.value);
    formStep.set("bed5_2", form.bed5_2.value);
    formStep.set("bed5_3", form.bed5_3.value);
  }
  if (form.bed_vi) {
    formStep.set("bed-vi", form.bed_vi.files[0]);
    formStep.set("bed-vi-ii", form.bed_vi_ii.files[0]);
    formStep.set("bed-vi-iii", form.bed_vi_iii.files[0]);

    formStep.set("type6", form.type6.value);
    formStep.set("food6", form.food6.value);
    formStep.set("approx6", form.approx6.value);
    formStep.set("bed6_1", form.bed6_1.value);
    formStep.set("bed6_2", form.bed6_2.value);
    formStep.set("bed6_3", form.bed6_3.value);
  }
  if (form.bed_vii) {
    formStep.set("bed-vii", form.bed_vii.files[0]);
    formStep.set("bed-vii-ii", form.bed_vii_ii.files[0]);
    formStep.set("bed-vii-iii", form.bed_vii_iii.files[0]);

    formStep.set("type7", form.type7.value);
    formStep.set("food7", form.food7.value);
    formStep.set("approx7", form.approx7.value);
    formStep.set("bed7_1", form.bed7_1.value);
    formStep.set("bed7_2", form.bed7_2.value);
    formStep.set("bed7_3", form.bed7_3.value);
  }
  if (form.bed_viii) {
    formStep.set("bed-viii", form.bed_viii.files[0]);
    formStep.set("bed-viii-ii", form.bed_viii_ii.files[0]);
    formStep.set("bed-viii-iii", form.bed_viii_iii.files[0]);

    formStep.set("type8", form.type8.value);
    formStep.set("food8", form.food8.value);
    formStep.set("approx8", form.approx8.value);
    formStep.set("bed8_1", form.bed8_1.value);
    formStep.set("bed8_2", form.bed8_2.value);
    formStep.set("bed8_3", form.bed8_3.value);
  }

  formStep.set("value_step", form.value_step.value);

  const DATA = { method: "POST", body: formStep };

  const jsonStep2 = await fetch("./action.php", DATA);
  const resultStep2 = await jsonStep2.json();

  resultStep2.forEach((response) => {
    if (response.register === "Registered") {
      if (btn.classList.contains("btn_modal-close")) window.top.location = "index";
      else stepButtons(btn);
    }
    if (response.register === "Error") alert("There was a problem, please try again");
  });
}

// * STEP 3

async function fetchStep3(form, btn) {
  const formStep = new FormData();
  formStep.set("des", form.des.value);
  formStep.set("main", form.main.files[0]);
  formStep.set("lroom", form.lroom.files[0]);
  formStep.set("fp", form.fp.files[0]);
  formStep.set("area-i", form.area_i.files[0]);
  formStep.set("area-ii", form.area_ii.files[0]);
  formStep.set("area-iii", form.area_iii.files[0]);
  formStep.set("area-iv", form.area_iv.files[0]);
  formStep.set("bath-i", form.bath_i.files[0]);
  formStep.set("bath-ii", form.bath_ii.files[0]);
  formStep.set("bath-iii", form.bath_iii.files[0]);
  formStep.set("bath-iv", form.bath_iv.files[0]);
  formStep.set("value_step", form.value_step.value);

  const DATA = { method: "POST", body: formStep };

  const jsonStep3 = await fetch("./action.php", DATA);
  const resultStep3 = await jsonStep3.json();

  resultStep3.forEach((response) => {
    if (response.register === "Registered") stepButtons(btn);
    if (response.register === "Error") alert("There was a problem, please try again");
  });
}

//* STEP 4

async function fetchStep4(form, btn) {
  const formStep = new FormData();
  formStep.set("name_h", form.name_h.value);
  formStep.set("l_name_h", form.l_name_h.value);
  formStep.set("db", form.db.value);

  if (form.gender.value === "-- Select Gender --");
  else formStep.set("gender", form.gender.value);

  formStep.set("cell", form.cell.value);
  formStep.set("occupation_m", form.occupation_m.value);
  formStep.set("db_law", form.db_law.value);
  formStep.set("b_check_main", form.b_check_main.files[0]);
  formStep.set("value_step", form.value_step.value);

  // ? FAMILY MEMBER

  formStep.set("f_name1", form.f_name1.value);
  formStep.set("f_lname1", form.f_lname1.value);
  formStep.set("db1", form.db1.value);
  if (form.gender1.value === "-- Select Gender --");
  else formStep.set("gender1", form.gender1.value);
  if (form.re1.value === "-- Select Relation --");
  else formStep.set("re1", form.re1.value);
  formStep.set("occupation_f1", form.occupation_f1.value);
  formStep.set("db_lawf1", form.db_lawf1.value);
  formStep.set("b_check_mem1", form.b_check_mem1.files[0]);

  formStep.set("f_name2", form.f_name2.value);
  formStep.set("f_lname2", form.f_lname2.value);
  formStep.set("db2", form.db2.value);
  if (form.gender2.value === "-- Select Gender --");
  else formStep.set("gender2", form.gender2.value);
  if (form.re2.value === "-- Select Relation --");
  else formStep.set("re2", form.re2.value);
  formStep.set("occupation_f2", form.occupation_f2.value);
  formStep.set("db_lawf2", form.db_lawf2.value);
  formStep.set("b_check_mem2", form.b_check_mem2.files[0]);

  formStep.set("f_name3", form.f_name3.value);
  formStep.set("f_lname3", form.f_lname3.value);
  formStep.set("db3", form.db3.value);
  if (form.gender3.value === "-- Select Gender --");
  else formStep.set("gender3", form.gender3.value);
  if (form.re3.value === "-- Select Relation --");
  else formStep.set("re3", form.re3.value);
  formStep.set("occupation_f3", form.occupation_f3.value);
  formStep.set("db_lawf3", form.db_lawf3.value);
  formStep.set("b_check_mem3", form.b_check_mem3.files[0]);

  formStep.set("f_name4", form.f_name4.value);
  formStep.set("f_lname4", form.f_lname4.value);
  formStep.set("db4", form.db4.value);
  if (form.gender4.value === "-- Select Gender --");
  else formStep.set("gender4", form.gender4.value);
  if (form.re4.value === "-- Select Relation --");
  else formStep.set("re4", form.re4.value);
  formStep.set("occupation_f4", form.occupation_f4.value);
  formStep.set("db_lawf4", form.db_lawf4.value);
  formStep.set("b_check_mem4", form.b_check_mem4.files[0]);

  formStep.set("f_name5", form.f_name5.value);
  formStep.set("f_lname5", form.f_lname5.value);
  formStep.set("db5", form.db5.value);
  if (form.gender5.value === "-- Select Gender --");
  else formStep.set("gender5", form.gender5.value);
  if (form.re5.value === "-- Select Relation --");
  else formStep.set("re5", form.re5.value);
  formStep.set("occupation_f5", form.occupation_f5.value);
  formStep.set("db_lawf5", form.db_lawf5.value);
  formStep.set("b_check_mem5", form.b_check_mem5.files[0]);

  formStep.set("f_name6", form.f_name6.value);
  formStep.set("f_lname6", form.f_lname6.value);
  formStep.set("db6", form.db6.value);
  if (form.gender6.value === "-- Select Gender --");
  else formStep.set("gender6", form.gender6.value);
  if (form.re6.value === "-- Select Relation --");
  else formStep.set("re6", form.re6.value);
  formStep.set("occupation_f6", form.occupation_f6.value);
  formStep.set("db_lawf6", form.db_lawf6.value);
  formStep.set("b_check_mem6", form.b_check_mem6.files[0]);

  formStep.set("f_name7", form.f_name7.value);
  formStep.set("f_lname7", form.f_lname7.value);
  formStep.set("db7", form.db7.value);
  if (form.gender7.value === "-- Select Gender --");
  else formStep.set("gender7", form.gender7.value);
  if (form.re7.value === "-- Select Relation --");
  else formStep.set("re7", form.re7.value);
  formStep.set("occupation_f7", form.occupation_f7.value);
  formStep.set("db_lawf7", form.db_lawf7.value);
  formStep.set("b_check_mem7", form.b_check_mem7.files[0]);

  formStep.set("f_name8", form.f_name8.value);
  formStep.set("f_lname8", form.f_lname8.value);
  formStep.set("db8", form.db8.value);
  if (form.gender8.value === "-- Select Gender --");
  else formStep.set("gender8", form.gender8.value);
  if (form.re8.value === "-- Select Relation --");
  else formStep.set("re8", form.re8.value);
  formStep.set("occupation_f8", form.occupation_f8.value);
  formStep.set("db_lawf8", form.db_lawf8.value);
  formStep.set("b_check_mem8", form.b_check_mem8.files[0]);

  const DATA = { method: "POST", body: formStep };

  const jsonStep4 = await fetch("./action.php", DATA);
  const resultStep4 = await jsonStep4.json();

  resultStep4.forEach((response) => {
    if (response.register === "Registered") stepButtons(btn);
    if (response.register === "Error") alert("There was a problem, please try again");
  });
}

// * STEP 5

async function fetchStep5(form, btn) {
  const formStep = new FormData();

  formStep.set("backg", form.backg.value);

  if (form.misdemeanor.value === "Yes") formStep.set("misdemeanor", form.misdemeanor_spe.value);
  else if (form.misdemeanor.value === "-- Select Option --");
  else formStep.set("misdemeanor", form.misdemeanor.value);

  if (form.allergies.value === "Yes") formStep.set("allergies", form.allergies_spe.value);
  else if (form.allergies.value === "-- Select Option --");
  else formStep.set("allergies", form.allergies.value);

  if (form.medic_f.value === "Yes") formStep.set("medic_f", form.medic_f_spe.value);
  else if (form.medic_f.value === "-- Select Option --");
  else formStep.set("medic_f", form.medic_f.value);

  if (form.condition_m.value === "Yes") formStep.set("condition_m", form.condition_m_spe.value);
  else if (form.condition_m.value === "-- Select Option --");
  else formStep.set("condition_m", form.condition_m.value);

  if (form.health_f.value === "Yes") formStep.set("health_f", form.health_f_spe.value);
  else if (form.health_f.value === "-- Select Option --");
  else formStep.set("health_f", form.health_f.value);

  if (form.religion.value === "Yes") formStep.set("religion", form.religion_spe.value);
  else if (form.religion.value === "-- Select Option --");
  else formStep.set("religion", form.religion.value);

  if (form.c_background.value !== "-- Select Option --") formStep.set("c_background", form.c_background.value);

  if (form.smoke.value !== "-- Select Preference --") formStep.set("smoke", form.smoke.value);

  if (form.a_pre.value !== "-- Select Preference --") formStep.set("a_pre", form.a_pre.value);

  formStep.set("value_step", form.value_step.value);

  const DATA = { method: "POST", body: formStep };

  const jsonStep5 = await fetch("./action.php", DATA);
  const resultStep5 = await jsonStep5.json();

  resultStep5.forEach((response) => {
    if (response.register === "End Register") window.top.location = "index";
    if (response.register === "Error") alert("There was a problem, please try again");
  });
}

// TODO =====================================================

step = (form, btn) => {
  let idButton = btn.id;

  const registerForm = new FormData(form);

  const camposStep1 = {
    numStep1: campos.num,
    roomStep1: campos.room,
    m_cityStep1: campos.m_city,
    ag_preStep1: campos.ag_pre,
    g_preStep1: campos.g_pre,
    petStep1: campos.pet,
  };

  const camposStep2 = {
    cityStep2: campos.city,
    dirStep2: campos.dir,
    stateStep2: campos.state,
    p_codeStep2: campos.p_code,
  };

  if (btn.dataset.step === "1") {
    if (
      camposStep1.numStep1 === true &&
      camposStep1.roomStep1 === true &&
      camposStep1.m_cityStep1 === true &&
      camposStep1.ag_preStep1 === true &&
      camposStep1.g_preStep1 === true &&
      camposStep1.petStep1 === true
    )
      fetchStep1(form, btn);
    else {
      elseFunctionAjax();

      Object.values(camposStep1).forEach((campo) => {
        if (campo !== true) {
          const campoId = campo.id;

          document.getElementById("form__group-" + campoId).classList.add("input_false");
          document
            .getElementById("form__group-" + campoId)
            .querySelector(".message__input_error")
            .classList.add("message__input_error_active");
        }
      });
    }
  } else if (btn.dataset.step === "2") {
    fetchStep2(form, btn);
  } else if (btn.id === "btn__modal") {
    if (
      camposStep2.cityStep2 === true &&
      camposStep2.dirStep2 === true &&
      camposStep2.stateStep2 === true &&
      camposStep2.p_codeStep2 === true
    ) {
      const responseBed = "";

      if (form.type1 && form.type1.value === "NULL") {
        form.type1.style.border = "2px solid #cc0000";
      }
      if (form.bed1_1 && form.bed1_1.value === "NULL") {
        form.bed1_1.style.border = "2px solid #cc0000";
      }

      if (form.type2 && form.type2.value === "NULL") {
        form.type2.style.border = "2px solid #cc0000";
      }
      if (form.bed2_1 && form.bed2_1.value === "NULL") {
        form.bed2_1.style.border = "2px solid #cc0000";
      }

      if (form.type3 && form.type3.value === "NULL") {
        form.type3.style.border = "2px solid #cc0000";
      }
      if (form.bed3_1 && form.bed3_1.value === "NULL") {
        form.bed3_1.style.border = "2px solid #cc0000";
      }

      if (form.type4 && form.type4.value === "NULL") {
        form.type4.style.border = "2px solid #cc0000";
      }
      if (form.bed4_1 && form.bed4_1.value === "NULL") {
        form.bed4_1.style.border = "2px solid #cc0000";
      }

      if (form.type5 && form.type5.value === "NULL") {
        form.type5.style.border = "2px solid #cc0000";
      }
      if (form.bed5_1 && form.bed5_1.value === "NULL") {
        form.bed5_1.style.border = "2px solid #cc0000";
      }

      if (form.type6 && form.type6.value === "NULL") {
        form.type6.style.border = "2px solid #cc0000";
      }
      if (form.bed6_1 && form.bed6_1.value === "NULL") {
        form.bed6_1.style.border = "2px solid #cc0000";
      }

      if (form.type7 && form.type7.value === "NULL") {
        form.type7.style.border = "2px solid #cc0000";
      }
      if (form.bed7_1 && form.bed7_1.value === "NULL") {
        form.bed7_1.style.border = "2px solid #cc0000";
      }

      if (form.type8 && form.type8.value === "NULL") {
        form.type8.style.border = "2px solid #cc0000";
      }
      if (form.bed8_1 && form.bed8_1.value === "NULL") {
        form.bed8_1.style.border = "2px solid #cc0000";
      }

      if ((form.type1 && form.type1.value === "NULL") || (form.bed1_1 && form.bed1_1.value === "NULL")) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-1"]
        );
      } else if (
        (form.type1 && form.type1.value !== "NULL" && form.type2 && form.type2.value === "NULL") ||
        (form.type1 && form.type1.value !== "NULL" && form.bed2_1 && form.bed2_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-2"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.bed3_1 &&
          form.bed3_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-3"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.bed4_1 &&
          form.bed4_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-4"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.bed5_1 &&
          form.bed5_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-5"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.bed6_1 &&
          form.bed6_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-6"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.bed7_1 &&
          form.bed7_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-7"]
        );
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value !== "NULL" &&
          form.type8 &&
          form.type8.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value !== "NULL" &&
          form.bed8_1 &&
          form.bed8_1.value === "NULL")
      ) {
        anyError(
          "You haven't finished the configuration of your rooms, do you want to continue the process without that information?",
          [true, ".room-8"]
        );
      } else {
        let modal = D.getElementById("modal__kb");
        modal.classList.replace("d-none", "d-flex");
      }
      // ? QUIT MODAL DYNAMIC

      D.addEventListener("click", (e) => {
        if (e.target.matches("#yes")) {
          D.querySelector("#modal-dynamic").classList.remove("show");
          let modal = D.getElementById("modal__kb");
          modal.classList.replace("d-none", "d-flex");
        }
      });
    } else {
      elseFunctionAjax();

      Object.values(camposStep2).forEach((campo) => {
        if (campo !== true) {
          const campoId = campo.id;

          document.getElementById("form__group-" + campoId).classList.add("input_false");
          document
            .getElementById("form__group-" + campoId)
            .querySelector(".message__input_error")
            .classList.add("message__input_error_active");
        }
      });

      if (form.type1 && form.type1.value === "NULL") {
        form.type1.style.border = "2px solid #cc0000";
      }
      if (form.bed1_1 && form.bed1_1.value === "NULL") {
        form.bed1_1.style.border = "2px solid #cc0000";
      }

      if (form.type2 && form.type2.value === "NULL") {
        form.type2.style.border = "2px solid #cc0000";
      }
      if (form.bed2_1 && form.bed2_1.value === "NULL") {
        form.bed2_1.style.border = "2px solid #cc0000";
      }

      if (form.type3 && form.type3.value === "NULL") {
        form.type3.style.border = "2px solid #cc0000";
      }
      if (form.bed3_1 && form.bed3_1.value === "NULL") {
        form.bed3_1.style.border = "2px solid #cc0000";
      }

      if (form.type4 && form.type4.value === "NULL") {
        form.type4.style.border = "2px solid #cc0000";
      }
      if (form.bed4_1 && form.bed4_1.value === "NULL") {
        form.bed4_1.style.border = "2px solid #cc0000";
      }

      if (form.type5 && form.type5.value === "NULL") {
        form.type5.style.border = "2px solid #cc0000";
      }
      if (form.bed5_1 && form.bed5_1.value === "NULL") {
        form.bed5_1.style.border = "2px solid #cc0000";
      }

      if (form.type6 && form.type6.value === "NULL") {
        form.type6.style.border = "2px solid #cc0000";
      }
      if (form.bed6_1 && form.bed6_1.value === "NULL") {
        form.bed6_1.style.border = "2px solid #cc0000";
      }

      if (form.type7 && form.type7.value === "NULL") {
        form.type7.style.border = "2px solid #cc0000";
      }
      if (form.bed7_1 && form.bed7_1.value === "NULL") {
        form.bed7_1.style.border = "2px solid #cc0000";
      }

      if (form.type8 && form.type8.value === "NULL") {
        form.type8.style.border = "2px solid #cc0000";
      }
      if (form.bed8_1 && form.bed8_1.value === "NULL") {
        form.bed8_1.style.border = "2px solid #cc0000";
      }

      if ((form.type1 && form.type1.value === "NULL") || (form.bed1_1 && form.bed1_1.value === "NULL")) {
        form.type1.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 && form.type1.value !== "NULL" && form.type2 && form.type2.value === "NULL") ||
        (form.type1 && form.type1.value !== "NULL" && form.bed2_1 && form.bed2_1.value === "NULL")
      ) {
        form.type2.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.bed3_1 &&
          form.bed3_1.value === "NULL")
      ) {
        form.type3.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.bed4_1 &&
          form.bed4_1.value === "NULL")
      ) {
        form.type4.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.bed5_1 &&
          form.bed5_1.value === "NULL")
      ) {
        form.type5.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.bed6_1 &&
          form.bed6_1.value === "NULL")
      ) {
        form.type6.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.bed7_1 &&
          form.bed7_1.value === "NULL")
      ) {
        form.type7.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      } else if (
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value !== "NULL" &&
          form.type8 &&
          form.type8.value === "NULL") ||
        (form.type1 &&
          form.type1.value !== "NULL" &&
          form.type2 &&
          form.type2.value !== "NULL" &&
          form.type3 &&
          form.type3.value !== "NULL" &&
          form.type4 &&
          form.type4.value !== "NULL" &&
          form.type5 &&
          form.type5.value !== "NULL" &&
          form.type6 &&
          form.type6.value !== "NULL" &&
          form.type7 &&
          form.type7.value !== "NULL" &&
          form.bed8_1 &&
          form.bed8_1.value === "NULL")
      ) {
        form.type8.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      }
    }
  } else if (btn.dataset.step === "3") {
    let modal = D.getElementById("modal__kb");
    modal.classList.replace("d-flex", "d-none");

    fetchStep3(form, btn);
  } else if (btn.dataset.step === "4") {
    fetchStep4(form, btn);
  } else if (btn.dataset.step === "5") {
    stepButtons(btn);
  } else if (btn.id === "register") {
    fetchStep5(form, btn);
  }

  function elseFunctionAjax() {
    setTimeout(() => {
      $("#house_much").fadeIn("slow");

      setTimeout(() => {
        $("#house_much").fadeOut("slow");
      }, 20000 /* 5000 */);
    }, 100);

    $("#close").on("click", function close() {
      event.preventDefault();
      $("#house_much").fadeOut("slow");
    });
    document.getElementById("contentp").innerHTML = "Please fill in the required fields correctly.";
    document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
    document.getElementById("contentp").style.marginTop = "auto";
    document.getElementById("contentp").style.marginBottom = "auto";
    document.getElementById("contentp").style.color = "#000";
  }

  if (form.type1) {
    form.type1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed1_1) {
    form.bed1_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type2) {
    form.type2.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed2_1) {
    form.bed2_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type3) {
    form.type3.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed3_1) {
    form.bed3_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type4) {
    form.type4.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed4_1) {
    form.bed4_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type5) {
    form.type5.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed5_1) {
    form.bed5_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type6) {
    form.type6.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed6_1) {
    form.bed6_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type7) {
    form.type7.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed7_1) {
    form.bed7_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.type8) {
    form.type8.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
  if (form.bed8_1) {
    form.bed8_1.addEventListener("change", (e) => {
      e.target.style.border = "2px solid #ada8cd";
      e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
        "invalid"
      );
    });
  }
};

function stepButtons(btn2) {
  let element = btn2;
  let isButtonNext = element.classList.contains("step__button--next");
  let isButtonBack = element.classList.contains("step__button--back");

  if (isButtonNext || isButtonBack) {
    let currentStep = document.getElementById("step-" + element.dataset.step);
    let jumpStep = document.getElementById("step-" + element.dataset.to_step);

    currentStep.addEventListener("animationend", function callback() {
      currentStep.classList.remove("active");
      jumpStep.classList.add("active");
      if (isButtonNext) {
        currentStep.classList.add("to-left");
        progressOptions[element.dataset.to_step - 1].classList.add("active");
        progressOptions[element.dataset.to_step - 1].classList.add("color");
        progressOptions[element.dataset.step - 1].classList.remove("color");
      } else {
        jumpStep.classList.remove("to-left");
        progressOptions[element.dataset.step - 1].classList.remove("active");
        progressOptions[element.dataset.to_step - 1].classList.add("color");
        progressOptions[element.dataset.step - 1].classList.remove("color");
      }

      window.scroll(0, 0);

      currentStep.removeEventListener("animationend", callback);
    });

    currentStep.classList.add("inactive");
    jumpStep.classList.remove("inactive");
  }
}

// Functions

function closeModal() {
  let modal = D.getElementById("modal__kb");

  modal.classList.replace("d-flex", "d-none");
}

function bedroomInfo(idType, valueType) {
  const type = idType.slice(-1);
  const $parentBed = D.querySelector(`.beds-container-${type}`);
  $parentBed.classList.replace("d-none", "d-flex");

  if (valueType === "Single" || valueType === "Executive") {
    $parentBed.querySelector(`#bed-1`).classList.replace("d-none", "d-flex");
    $parentBed.querySelector(`#bed-1`).classList.add("added");
    $parentBed.querySelector("#bed-2").classList.replace("d-flex", "d-none");
    $parentBed.querySelector("#bed-2").classList.remove("added");
    $parentBed.querySelector("#bed-3").classList.replace("d-flex", "d-none");
    $parentBed.querySelector("#bed-3").classList.remove("added");
    $parentBed.querySelector(`#bed-type-1`).disabled = false;
  } else if (valueType === "Share") {
    $parentBed.querySelector(`#bed-1`).classList.replace("d-none", "d-flex");
    $parentBed.querySelector(`#bed-2`).classList.replace("d-none", "d-flex");
    $parentBed.querySelector(`#bed-3`).classList.replace("d-none", "d-flex");
    $parentBed.querySelector(`#bed-1`).classList.add("added");
    $parentBed.querySelector(`#bed-2`).classList.add("added");
    $parentBed.querySelector(`#bed-3`).classList.add("added");
    $parentBed.querySelector(`#bed-type-1`).disabled = false;
    $parentBed.querySelector(`#bed-type-2`).disabled = false;
    $parentBed.querySelector(`#bed-type-3`).disabled = false;
  }

  D.addEventListener("click", (e) => {
    if (e.target.matches(`#add-bed-${type}-3`)) {
      e.preventDefault();
      $parentBed.querySelector(`#bed-3`).classList.add("more");
    }
  });
}

function anyError(message, scroll = [false, ""]) {
  const $html = `
      <div class="d-flex flex-column align-center justify-content-center confirm-status text-center" style="width:55%">
        <h4 class="pt-2">${message}</h4>
        <div class="d-flex align-items-center justify-content-center pb-2">
        <button id="yes" class="btn w-25 mx-2 text-white disable-room" style="background-color: green">Yes</button>
        <button id="no" class="btn w-25 mx-2 text-white disable-room" style="background-color: red">No</button>
        </div>
      </div>`;
  D.querySelector("#modal-dynamic").innerHTML = $html;
  D.querySelector("#modal-dynamic").classList.add("show");

  if (D.getElementById("no")) {
    D.getElementById("no").onclick = () => {
      D.querySelector("#modal-dynamic").classList.remove("show");

      if (scroll[0]) {
        D.querySelector(scroll[1]).scrollIntoView({ block: "start", behavior: "smooth" });
        if (D.querySelector(scroll[1]).tagName !== "H1") D.querySelector(scroll[1]).classList.add("invalid");
      }
    };
  }
}

const $city = document.querySelector("#destination_c");

// Slide Options Function

const progressOptions = document.querySelectorAll(".progressbar__option");

D.addEventListener("click", (btn) => {
  if (btn.target.matches(".btn__next") || btn.target.matches(".step__button--back")) {
    btn.preventDefault();
    step(btn.target.parentNode.parentNode, btn.target);
  }
  if (btn.target.matches(".btn__modal") || btn.target.matches(".btn_modal-close")) {
    btn.preventDefault();
    step(btn.target.parentNode.parentNode.parentNode.parentNode.parentNode, btn.target);
  }
  if (btn.target.matches(".close") || btn.target.matches("#modal-dynamic") || btn.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }
});

D.addEventListener("change", (e) => {
  if (e.target.matches("#type-1")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-2")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-3")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-4")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-5")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-6")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-7")) bedroomInfo(e.target.id, e.target.value);
  if (e.target.matches("#type-8")) bedroomInfo(e.target.id, e.target.value);
});

/* $divSelectType.forEach((divSelect) => {
  console.log(divSelect);
}); */

// Validate Register

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^[0-9\s\(\)\+\-]{1,40}$/, // 2 a 14 numeros.
  rooms: /^[1-8]{1}$/, // 1 a 14 numeros.
  m_city: /(\W|^)(Toronto|Montreal|Ottawa|Quebec|Calgary|Vancouver|Victoria)(\W|$)/,
  ag_pre: /(\W|^)(Teenager|Adult|Any)(\W|$)/,
  g_pre: /(\W|^)(Male|Female|Any)(\W|$)/,
  pet: /(\W|^)(Yes|No)(\W|$)/,
  type1: /(\W|^)(Single|Share|Executive)(\W|$)/,
  bed1: /(\W|^)(Twin|Double|Bunker)(\W|$)/,
  date1: /(\W|^)(Avalible|Occupied|Disabled)(\W|$)/,
  food1: /(\W|^)(Yes|No)(\W|$)/,

  dir: /^[a-zA-Z0-9\_\-\s\°]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s\,]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^.{4,12}$/, // 4 a 12 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
};

const campos = {
  num: D.getElementById("num"),
  room: D.getElementById("room"),
  city: D.getElementById("city"),
  dir: D.getElementById("dir"),
  state: D.getElementById("state"),
  p_code: D.getElementById("p_code"),
  m_city: D.getElementById("m_city"),
  pet: D.getElementById("pet"),
  ag_pre: D.getElementById("ag_pre"),
  g_pre: D.getElementById("g_pre"),
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    case "num":
      validateField(expresiones.phone, e.target, "num");
      break;
    case "room":
      validateField(expresiones.rooms, e.target, "room");
      break;
    case "m_city":
      validateField(expresiones.m_city, e.target, "m_city");
      break;
    case "ag_pre":
      validateField(expresiones.ag_pre, e.target, "ag_pre");
      break;
    case "g_pre":
      validateField(expresiones.g_pre, e.target, "g_pre");
      break;
    case "pet":
      validateField(expresiones.pet, e.target, "pet");
      break;

    case "mail":
      validateField(expresiones.correo, e.target, "mail");
      break;
    case "dir":
      validateField(expresiones.dir, e.target, "dir");
      break;
    case "city":
      validateField(expresiones.dir, e.target, "city");
      break;
    case "state":
      validateField(expresiones.dir, e.target, "state");
      break;
    case "p_code":
      validateField(expresiones.dir, e.target, "p_code");
      break;

    case "p_code":
      validateField(expresiones.dir, e.target, "p_code");
      break;

    case "l_name_h":
      validateField(expresiones.name_h, e.target, "l_name_h");
      break;
    case "cell":
      validateField(expresiones.phone, e.target, "cell");
      break;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`form__group-${campo}`).classList.remove("input_false");
    document
      .querySelector(`.form__group-${campo} .message__input_error`)
      .classList.remove("message__input_error_active");
    campos[campo] = true;
  } else {
    document.getElementById(`form__group-${campo}`).classList.add("input_false");
    document.querySelector(`.form__group-${campo} .message__input_error`).classList.add("message__input_error_active");
    campos[campo] = input;
  }
};

D.addEventListener("keyup", (e) => {
  if (e.target.matches(".form__register input")) validateFormulary(e);
  if (e.target.matches(".form__register textarea")) validateFormulary(e);
});

D.addEventListener("change", (e) => {
  if (e.target.matches(".form__register select")) validateFormulary(e);
});

// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *
// *

// TODO HABITACIONES (MOMENTANEO)

var room_title = `<h2 class="title__group-section" id="title__bedrooms"> Bedrooms Information </h2>`;

var room1 = `
  
  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay room-1">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #232159">
          <h3 class="p-0 m-0 text-white">Room 1</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
        <div class="div-img-r w-100" align="center">

          <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
              <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">

              <div class="carousel-item active">

                <label for="file-xi" class="photo-add p-0" id="label-xi">
                  <img src="../assets/emptys/room-empty.png" alt="">
                </label>

                <label for="file-xi" class="add-photo-i py-auto" id="label-xi-i" title="Change Bedroom Photo"><p class="icon-edit fa fa-pencil-alt"></p></label>

                <img id="preview-xi" class="add-photo d-none" >

                <input type="file" name="bed_i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none;">


              </div>

              <div class="carousel-item">
                <label for="file-xi-ii" class="photo-add" id="label-xi-ii">
                  <img src="../assets/emptys/room-empty.png" alt="">
                </label>

                <label for="file-xi-ii" class="add-photo-i py-auto" id="label-xi-ii-i" title="Change Bedroom Photo"> 
                  <p class="icon-edit fa fa-pencil-alt"></p>
                </label>

                <img id="preview-xi-ii" class="add-photo d-none" >

                <input type="file" name="bed_i_ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">


              </div>

              <div class="carousel-item">

                <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                </label>
                
                <label for="file-xi-iii" class="add-photo-i py-auto" id="label-xi-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                <img id="preview-xi-iii" class="add-photo d-none" >

                <input type="file" name="bed_i_iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">


              </div>

            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next" style="z-index: 1">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>

          </div>

        </div><br>

        <!-- <button id="edit-photo-btn" title="Edit image">
          <img src="../assets/icon/edit." rel="noopener" alt="edit">
        </button> -->

        <img src="" id="single-image-room" class="d-none">

        <!-- // TODO CAROUSEL FOTOS -->
      </div>

      <!-- // TODO INFO ROOM -->
      <div class="d-flex justify-content-center mt-5">

        <!-- // TODO TYPE ROOM -->
        <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
          <div class="feature type-room">
            <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
            <select class="custom-select" name="type1" id="type-1" required>
              <option hidden selected value="NULL">Type room</option>
              <option value="Single">Single</option>
              <option value="Executive">Executive</option>
              <option value="Share">Share</option>
            </select>
          </div>
        </div>

        <!-- // TODO ROOM FOOD SERVICE -->
        <div class="col-auto p-0">
          <div class="feature food pl-0">
            <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
            <select class="custom-select" name="food1" id="food-service" required>
              <option hidden selected value="NULL">Food</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </div>
        </div>

      </div>

      <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
        <label class="font-weight-light">Weekly Price CAD$</label>
        <!-- <span class="rounded font-">CAD$</span> -->
        <div class="mb-2 form-group d-flex w-75 justify-content-center">
          <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx1" maxlength="10" style="width: 80%;">
        </div>
      </div>

    </div>

    <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-1" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed1_1" id="bed-type-1" class="px-2" disabled>
                <option selected hidden value="NULL" >Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed1_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed1_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-1-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>



`;

var room2 = `

    <!-- Room2 -->

    <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay room-2">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #982a72">
          <h3 class="p-0 m-0 text-white">Room 2</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
        <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                    
                      <label for="file-xii" class="photo-add" id="label-xii">
                        <img src="../assets/emptys/room-empty.png" alt="">
                      </label>
                      
                      <label for="file-xii" class="add-photo-i py-auto" id="label-xii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                      <img id="preview-xii" class="add-photo d-none" >

                      <input type="file" name="bed_ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">

                    </div>

                    <div class="carousel-item">
                      <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> 
                        <img src="../assets/emptys/room-empty.png" alt="">
                      </label>

                      <label for="file-xii-ii" class="add-photo-i py-auto" id="label-xii-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                      <img id="preview-xii-ii" class="add-photo d-none" >

                      <input type="file" name="bed_ii_ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">

                    </div>

                    <div class="carousel-item">
                      <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> 
                        <img src="../assets/emptys/room-empty.png" alt="">
                      </label>

                      <label for="file-xii-iii" class="add-photo-i py-auto" id="label-xii-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                      <img id="preview-xii-iii" class="add-photo d-none" >

                      <input type="file" name="bed_ii_iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">



                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div><br>

        <!-- <button id="edit-photo-btn" title="Edit image">
          <img src="../assets/icon/edit." rel="noopener" alt="edit">
        </button> -->

        <img src="" id="single-image-room" class="d-none">

        <!-- // TODO CAROUSEL FOTOS -->
      </div>

      <!-- // TODO INFO ROOM -->
      <div class="d-flex justify-content-center mt-5">

        <!-- // TODO TYPE ROOM -->
        <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
          <div class="feature type-room">
            <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
            <select class="custom-select" name="type2" id="type-2" required>
              <option hidden selected value="NULL">Type room</option>
              <option value="Single">Single</option>
              <option value="Executive">Executive</option>
              <option value="Share">Share</option>
            </select>
          </div>
        </div>

        <!-- // TODO ROOM FOOD SERVICE -->
        <div class="col-auto p-0">
          <div class="feature food pl-0">
            <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
            <select class="custom-select" name="food2" id="food-service" required>
              <option value="NULL" hidden selected>Food</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </div>
        </div>

      </div>

      <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
        <label class="font-weight-light">Weekly Price CAD$</label>
        <!-- <span class="rounded font-">CAD$</span> -->
        <div class="mb-2 form-group d-flex w-75 justify-content-center">
          <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx2" maxlength="10" style="width: 80%;">
        </div>
      </div>

    </div>

    <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-2" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-2" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed2_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed2_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed2_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-2-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room3 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay room-3">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #394893">
          <h3 class="p-0 m-0 text-white">Room 3</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                  <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">
                  <label for="file-xiii" class="photo-add" id="label-xiii">
                    <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                  
                  <label for="file-xiii" class="add-photo-i py-auto" id="label-xiii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiii" class="add-photo d-none" >

                  <input type="file" name="bed_iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">

                </div>

                <div class="carousel-item">
                  <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xiii-ii" class="add-photo-i py-auto" id="label-xiii-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiii-ii" class="add-photo d-none" >

                  <input type="file" name="bed_iii_ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >

                </div>

                <div class="carousel-item">
                  <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xiii-iii" class="add-photo-i py-auto" id="label-xiii-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiii-iii" class="add-photo d-none" >

                  <input type="file" name="bed_iii_iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

                            
          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type3" id="type-3" required>
                <option hidden selected value="NULL">Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food3" id="food-service" required>
                <option hidden selected value="NULL">Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx3" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-3" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-3" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed3_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed3_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed3_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-3-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room4 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #a54483">
          <h3 class="p-0 m-0 text-white">Room 4</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">
                  <label for="file-xiv" class="photo-add" id="label-xiv"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xiv" class="add-photo-i py-auto" id="label-xiv-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiv" class="add-photo d-none" >

                  <input type="file" name="bed_iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >

                </div>

                <div class="carousel-item">
                  <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xiv-ii" class="add-photo-i py-auto" id="label-xiv-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiv-ii" class="add-photo d-none" >

                  <input type="file" name="bed_iv_ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">

                </div>

                <div class="carousel-item">
                  <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii">
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xiv-iii" class="add-photo-i py-auto" id="label-xiv-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xiv-iii" class="add-photo d-none" >

                  <input type="file" name="bed_iv_iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

                        
          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type4" id="type-4" required>
                <option hidden selected value="NULL">Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food4" id="food-service" required>
                <option value="NULL" hidden selected>Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx4" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-4" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-4" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed4_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed4_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL" selected>Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed4_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-4-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room5 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #5d418d">
          <h3 class="p-0 m-0 text-white">Room 5</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
            </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <label for="file-xv" class="photo-add" id="label-xv">
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xv" class="add-photo-i py-auto" id="label-xv-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xv" class="add-photo d-none" >

                  <input type="file" name="bed_v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none">

                </div>

                <div class="carousel-item">

                  <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                  
                  <label for="file-xv-ii" class="add-photo-i py-auto" id="label-xv-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xv-ii" class="add-photo d-none" >

                  <input type="file" name="bed_v_ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none">

                </div>

                <div class="carousel-item">

                  <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                        
                  <label for="file-xv-iii" class="add-photo-i py-auto" id="label-xv-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xv-iii" class="add-photo d-none" >

                  <input type="file" name="bed_v_iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none">

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type5" id="type-5" required>
                <option hidden selected value="NULL">Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food5" id="food-service" required>
                <option value="NULL" hidden selected >Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx5" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-5" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-5" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed5_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed5_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed5_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-5-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room6 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #392b84">
          <h3 class="p-0 m-0 text-white">Room 6</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <label for="file-xvi" class="photo-add" id="label-xvi">
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                        
                  <label for="file-xvi" class="add-photo-i py-auto" id="label-xvi-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvi" class="add-photo d-none" >

                  <input type="file" name="bed_vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xvi-ii" class="photo-add" id="label-xvi-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                        
                  <label for="file-xvi-ii" class="add-photo-i py-auto" id="label-xvi-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvi-ii" class="add-photo d-none" >

                  <input type="file" name="bed_vi_ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xvi-iii" class="photo-add" id="label-xvi-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                        
                  <label for="file-xvi-iii" class="add-photo-i py-auto" id="label-xvi-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvi-iii" class="add-photo d-none" >

                  <input type="file" name="bed_vi_iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

                        
          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type6" id="type-6" required>
                <option hidden selected value="NULL">Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food6" id="food-service" required>
                <option value="NULL" hidden selected>Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx6" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-6" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-6" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed6_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed6_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed6_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-6-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room7 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #b15391">
          <h3 class="p-0 m-0 text-white">Room 7</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <label for="file-xvii" class="photo-add" id="label-xvii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                  
                  <label for="file-xvii" class="add-photo-i py-auto" id="label-xvii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvii" class="add-photo d-none" >

                  <input type="file" name="bed_vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xvii-ii" class="photo-add" id="label-xvii-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                            
                  <label for="file-xvii-ii" class="add-photo-i py-auto" id="label-xvii-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvii-ii" class="add-photo d-none" >

                  <input type="file" name="bed_vii_ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xvii-iii" class="photo-add" id="label-xvii-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                            
                  <label for="file-xvii-iii" class="add-photo-i py-auto" id="label-xvii-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xvii-iii" class="add-photo d-none" >

                  <input type="file" name="bed_vii_iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

                        
          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type7" id="type-7" required>
                <option value="NULL" hidden selected>Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food7" id="food-service" required>
                <option value="NULL" hidden selected>Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx7" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-7" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-7" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed7_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed7_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed7_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-7-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

var room8 = `

  <!-- // TODO ROOM CONTAINER -->
  <div id="reserve" class="d-flex flex-column justify-content-center align-items-center rounded  room-card-container-homestay">

    <div class="d-lg-flex py-4 px-3 align-items-center">

      <!-- // TODO ROOM CARD -->
      <div class="mx-4 pb-4 bg-white room-card">

        <!-- // TODO ROOM HEADER -->
        <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title" style="background-color: #4f177d">
          <h3 class="p-0 m-0 text-white">Room 8</h3>
        </div>

        <!-- // TODO ROOM PHOTO -->
        <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
          <div class="div-img-r w-100" align="center">

            <div id="carouselExampleIndicators-8" class="carousel slide" data-ride="carousel">

              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-8" data-slide-to="0" class="active"> </li>
                <li data-target="#carouselExampleIndicators-8" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-8" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <label for="file-xviii" class="photo-add" id="label-xviii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                    
                  <label for="file-xviii" class="add-photo-i py-auto" id="label-xviii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xviii" class="add-photo d-none" >

                  <input type="file" name="bed_viii" id="file-xviii" accept="image/*" onchange="previewImage_xviii();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xviii-ii" class="photo-add" id="label-xviii-ii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>
                        
                  <label for="file-xviii-ii" class="add-photo-i py-auto" id="label-xviii-ii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xviii-ii" class="add-photo d-none" >

                  <input type="file" name="bed_viii_ii" id="file-xviii-ii" accept="image/*" onchange="previewImage_xviii_ii();" style="display: none" >

                </div>

                <div class="carousel-item">

                  <label for="file-xviii-iii" class="photo-add" id="label-xviii-iii"> 
                  <img src="../assets/emptys/room-empty.png" alt="">
                  </label>

                  <label for="file-xviii-iii" class="add-photo-i py-auto" id="label-xviii-iii-i" title="Change Bedroom Photo"> <p class="icon-edit fa fa-pencil-alt"></p> </label>

                  <img id="preview-xviii-iii" class="add-photo d-none" >

                  <input type="file" name="bed_viii_iii" id="file-xviii-iii" accept="image/*" onchange="previewImage_xviii_iii();" style="display: none" >

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselExampleIndicators-8" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carouselExampleIndicators-8" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

                        
          </div><br>

          <!-- <button id="edit-photo-btn" title="Edit image">
            <img src="../assets/icon/edit." rel="noopener" alt="edit">
          </button> -->

          <img src="" id="single-image-room" class="d-none">

          <!-- // TODO CAROUSEL FOTOS -->
        </div>

        <!-- // TODO INFO ROOM -->
        <div class="d-flex justify-content-center mt-5">

          <!-- // TODO TYPE ROOM -->
          <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
            <div class="feature type-room">
              <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
              <select class="custom-select" name="type8" id="type-8" required>
                <option value="NULL" hidden selected>Type room</option>
                <option value="Single">Single</option>
                <option value="Executive">Executive</option>
                <option value="Share">Share</option>
              </select>
            </div>
          </div>

          <!-- // TODO ROOM FOOD SERVICE -->
          <div class="col-auto p-0">
            <div class="feature food pl-0">
              <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
              <select class="custom-select" name="food8" id="food-service" required>
                <option value="NULL" hidden selected>Food</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>

        </div>

        <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price" data-price-invalid="Price invalid">
          <label class="font-weight-light">Weekly Price CAD$</label>
          <!-- <span class="rounded font-">CAD$</span> -->
          <div class="mb-2 form-group d-flex w-75 justify-content-center">
            <input type="text" title="Homestay price" id="homestay-price" class="form-control" placeholder="Room Price" name="approx8" maxlength="10" style="width: 80%;">
          </div>
        </div>

      </div>

      <!-- // TODO BEDS CONTAINER -->
      <div id="beds-container-8" class="w-auto mx-lg-4 flex-column align-items-center room-beds-homestay edit mt-3 d-none beds-container-8" style="height: 480px;">
        <!-- // TODO BEDS -->
        <h3 class="font-weight-light text-muted">Beds</h3>

        <!-- // TODO BED -->
        <div id="bed-1" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed8_1" id="bed-type-1" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-2" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed8_2" id="bed-type-2" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed" class="d-none" title="Add bed">+</button>
        </div>

        <div id="bed-3" class="my-lg-auto rounded d-none justify-content-center bed-info-homestay" data-no-bed="Select type bed">

          <div class="d-flex">
            <div class="w-100 h-50 d-flex align-items-center justify-content-center">
              <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
              <select name="bed8_3" id="bed-type-3" class="px-2" disabled>
                <option value="NULL">Type bed</option>
                <option value="Twin">Twin</option>
                <option value="Bunk-bed">Bunk</option>
                <option value="Double">Double</option>
              </select>
            </div>

            <div class="d-flex align-items-center justify-content-center">
              <span id="bed-letter" class="text-white">Bed</span>
            </div>
          </div>

          <button id="add-bed-8-3" class="d-none" title="Add bed">+</button>
        </div>

        <!-- // TODO IMAGE ONLY ONE BED -->
        <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
      </div><br>

    </div>

  </div>

`;

function viewBed() {
  var total = "";

  if ($("#room").val() == "1") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;

    total += `
        </div>`;
  } else if ($("#room").val() == "2") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;

    total += `
        </div>`;
  } else if ($("#room").val() == "3") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;

    total += `
        </div>`;
  } else if ($("#room").val() == "4") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;
    total += room4;

    total += `
        </div>`;
  } else if ($("#room").val() == "5") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;
    total += room4;
    total += room5;

    total += `
        </div>`;
  } else if ($("#room").val() == "6") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;
    total += room4;
    total += room5;
    total += room6;

    total += `
        </div>`;
  } else if ($("#room").val() == "7") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;
    total += room4;
    total += room5;
    total += room6;
    total += room7;

    total += `
        </div>`;
  } else if ($("#room").val() == "8") {
    total =
      room_title +
      `

        <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;"> `;

    total += room1;
    total += room2;
    total += room3;
    total += room4;
    total += room5;
    total += room6;
    total += room7;
    total += room8;

    total += `
        </div>`;
  }

  $(".bedrooms").html(total);
}
