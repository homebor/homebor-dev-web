 <?php
  include_once('../xeon.php');
  include '../cript.php';
  session_start();
  error_reporting(0);

  $usuario = $_SESSION['username'];

  $query = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
  $row_homestay = $query->fetch_assoc();

  if (!$usuario) header("location: ../logout.php");

  ?>

 <!doctype html>
 <html lang="en">

 <head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="author" content="ThemeStarz">
   <meta http-equiv="X-UA-Compatible" content="ie=edge" />

   <!--// ? CSS -->
   <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.6">
   <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.5">
   <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.6">
   <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.6">
   <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.6">
   <link rel="stylesheet" href="../homestay/assets/css/carousel.css?ver=1.0.6">
   <link rel="stylesheet" href="assets/css/homedit.css?ver=1.0.6">
   <link rel="stylesheet" href="assets/css/homedit2.css?ver=1.0.6">
   <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.6">


   <!--// ? Mapbox Links-->
   <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js?ver=1.0.5'></script>
   <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css?ver=1.0.5' rel='stylesheet' />
   <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js?ver=1.0.5"></script>
   <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css?ver=1.0.5" rel="stylesheet" />

   <!--// ?  Favicons -->
   <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png?ver=1.0.5">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.5">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.5">
   <link rel="manifest" href="/site.webmanifest?ver=1.0.5">
   <link rel="mask-icon" href="/safari-pinned-tab.svg?ver=1.0.5" color="#5bbad5">
   <meta name="msapplication-TileColor" content="#da532c">
   <meta name="theme-color" content="#ffffff">

   <!--// ?  Daterangepicker -->
   <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js?ver=1.0.5"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js?ver=1.0.5"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js?ver=1.0.5">
   </script>
   <link rel="stylesheet" type="text/css"
     href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css?ver=1.0.5" />

   <title>Homebor - Edit Property </title>

 </head>

 <header id="ts-header" class="fixed-top" style="background-color: white;">
   <?php include 'header.php' ?>
 </header>

 <body>

   <!-- // TODO CONTAINER -->
   <main class="container">
     <!-- // TODO INPUTS DE UTILIDAD -->
     <input type="hidden" value="<?php echo $row_homestay['id_home'] ?>" id="homestay-id">
     <input type="hidden" data-view="edit-property" value="homestay" id="current-user">

     <!-- // TODO TITLE -->
     <div class="w-100 mb-5 py-3 border-bottom container__title">
       <h1 class="m-0 pl-lg-5 title__edit">Edit Propertie</h1>
     </div>

     <!-- // TODO BASIC INFORMATION -->
     <form id="basic-information" class="pb-4 bg-white shadow rounded" autocomplete="off" enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Basic Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">House Information</h3>

       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-around align-items-center">
         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_hname mr-2"></i>
             <label for="house-name" class="m-0 py-lg-0 py-2 text-muted font-weight-light">House Name *</label>
           </div>
           <input type="text" name="house-name" id="house-name" class="form-control"
             placeholder="e.g. John Smith Residence">
         </div>

         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_pnumber mr-2"></i>
             <label for="phone-number" class="m-0 py-lg-0 py-2 text-muted font-weight-light">Phone Number *</label>
           </div>
           <input type="text" name="phone-number" id="phone-number" class="form-control" placeholder="e.g. 55575846">
         </div>
       </div>

       <div
         class="col-10 mb-4 mb-lg-0 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-around align-items-center">
         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_hname mr-2"></i>
             <label class="m-0 py-lg-0 py-2 text-muted font-weight-light">Type of Residence *</label>
           </div>
           <select name="house-type" id="house-type" class="custom-select" style="width: 215px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="House"> House </option>
             <option value="Apartment"> Apartment </option>
             <option value="Condominium"> Condominium </option>
           </select>

           <!-- MENSAJE DE ERROR -->
           <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>
         </div>


         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_rooms mr-2"></i>
             <label for="total-rooms" class="m-0 py-lg-0 py-2 text-muted font-weight-light">Rooms in your House</label>
           </div>
           <input type="text" name="total-rooms" id="total-rooms" disabled class="form-control bg-light border-0"
             placeholder="e.g. 5">
         </div>
       </div>


       <h3 class="text-center text-lg-left title__group-section">Location</h3>

       <div class="d-flex flex-column flex-lg-row justify-content-center">
         <!-- // ? INPUTS -->
         <div class="col-lg-4 col-10 mx-auto form-group">
           <!-- // * MAIN CITY -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="main-city" class="m-0 text-muted font-weight-light">Main City</label>
             </div>

             <select name="main-city" id="main-city" class="custom-select">
               <option value="NULL" selected hidden>Select Option</option>
               <option value="Toronto">Toronto</option>
               <option value="Montreal">Montreal</option>
               <option value="Ottawa">Ottawa</option>
               <option value="Quebec">Quebec</option>
               <option value="Calgary">Calgary</option>
               <option value="Vancouver">Vancouver</option>
               <option value="Victoria">Victoria</option>
             </select>
           </div>

           <!-- // * ADDRESS -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="address" class="m-0 text-muted font-weight-light">Address *</label>
             </div>

             <input type="text" name="address" id="address" class="form-control" placeholder="Av, Street, etc.">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid Address </p>
           </div>

           <!-- // * CITY -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="city" class="m-0 text-muted font-weight-light">City *</label>
             </div>

             <input type="text" name="city" id="city" class="form-control" placeholder="e.g. Toronto">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid City </p>
           </div>

           <!-- // * STATE / PROVINCE -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="state" class="m-0 text-muted font-weight-light">State / Province *</label>
             </div>

             <input type="text" name="state" id="state" class="form-control" placeholder="e.g. Ontario">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid State </p>
           </div>

           <!-- // * POSTAL CODE -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="postal-code" class="m-0 text-muted font-weight-light">Postal Code *</label>
             </div>

             <input type="text" name="postal-code" id="postal-code" class="form-control"
               placeholder="No Special Characters">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid Postal Code </p>
           </div>
         </div>

         <!-- // ? MAP -->
         <div class="col-lg-6 col-10 mx-auto form-group">
           <div id='map' class="m-0 p-0"></div>
           <code hidden><?php echo $row_homestay['dir'] ?></code>
           <code hidden><?php echo $row_homestay['city'] ?></code>
           <code hidden><?php echo $row_homestay['state'] ?></code>
           <code hidden><?php echo $row_homestay['p_code'] ?></code>

           <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js?ver=1.0.5'></script>
         </div>
       </div>

     </form>


     <!-- // TODO HOUSE GALLERY -->
     <form id="house-gallery" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">House Gallery</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Photo Gallery</h3>

       <br><br>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="frontage-photo"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Frontage Photo</h4>

           <label for="change-photo-1" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/frontage-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-1" id="change-photo-1">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="living-room-photo"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Living Room Photo</h4>

           <label for="change-photo-2" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/living-room-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-2" id="change-photo-2">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="family-picture"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Family Picture</h4>

           <label for="change-photo-3" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/family-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-3" id="change-photo-3">
           </label>
         </div>
       </div>

       <br><br><br>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="kitchen" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Kitchen</h4>

           <label for="change-photo-4" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/kitchen-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-4" id="change-photo-4">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="dining-room"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Dining Room</h4>

           <label for="change-photo-5" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/dinning-room-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-5" id="change-photo-5">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br>
         </div>

         <!-- // * IMAGE -->
         <div class="mt-5 mt-lg-0 d-flex align-items-center justify-content-center"
           style="width: 250px; height: 180px;">
           <div id="house-area-3"
             class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
             <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
               House Area 3
               <span class="close text-white">X</span>
             </h4>

             <label for="change-photo-6" class="m-0 p-0 d-flex align-items-center justify-content-center">
               <img class="no-image" src="../assets/emptys/house-area-empty.png">
               <!-- // ** ADD PHOTO -->
               <input type="file" hidden name="change-photo-6" id="change-photo-6">
             </label>
           </div>

           <!-- REPLACEMENT -->
           <button id="add-area-3" class="m-0 add-photo-btn">+</button>
         </div>
       </div>


       <div id="house-area-4-container"
         class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-start">
         <!--  // * IMAGE  -->
         <div id="house-area-4"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
             House Area 4
             <span class="close text-white">X</span>
           </h4>

           <label for="change-photo-7" class="m-0 p-0 d-flex align-items-center justify-content-center ">
             <img class="no-image" src="../assets/emptys/house-area-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-7" id="change-photo-7">
           </label>

         </div>
         <!-- REPLACEMENT -->
         <button id="add-area-4" class="add-photo-btn d-none">+</button>
       </div>

       <div class="d-none d-lg-block">
         <br><br><br><br>
       </div>
       <div class="d-lg-none">
         <br><br><br>
       </div>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="bathroom-1" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Bathroom Photo 1</h4>

           <label for="change-photo-8" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-8" id="change-photo-8">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="bathroom-2" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Bathroom Photo 2</h4>

           <label for="change-photo-9" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <img class="no-image" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-9" id="change-photo-9">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div class="d-flex align-items-center justify-content-center" style="width: 250px; height: 180px;">
           <div id="bathroom-3"
             class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
             <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
               Bathroom Photo 3
               <span class="close text-white">X</span>
             </h4>

             <label for="change-photo-10" class="m-0 p-0 d-flex align-items-center justify-content-center">
               <img class="no-image" src="../assets/emptys/bathroom-empty.png">
               <!-- // ** ADD PHOTO -->
               <input type="file" hidden name="change-photo-10" id="change-photo-10">
             </label>
           </div>

           <!-- REPLACEMENT -->
           <button id="add-bathroom-3" class="m-0 add-photo-btn">+</button>
         </div>

       </div>

       <div class="d-lg-none">
         <br><br><br><br>
       </div>

       <div id="bathroom-4-container"
         class="col-10 mx-auto mb-4 d-flex flex-column flex-lg-row align-items-center justify-content-start">
         <!--  // * IMAGE  -->
         <div id="bathroom-4"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
             Bathroom Photo 4
             <span class="close text-white">X</span>
           </h4>

           <label for="change-photo-11" class="m-0 p-0 d-flex align-items-center justify-content-center ">
             <img class="no-image" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden name="change-photo-11" id="change-photo-11">
           </label>

         </div>
         <!-- REPLACEMENT -->
         <button id="add-bathroom-4" class="add-photo-btn d-none">+</button>
       </div>

     </form>


     <!-- // TODO ADDITIONAL INFORMATION -->
     <form id="additional-information" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Additional Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Description of your House</h3>

       <div class="w-75 p-0 mx-auto d-flex form-group">
         <i class="icon_description "></i>
         <textarea rows="3" maxlength="255" class="mx-auto form-control" name="house-description" id="house-description"
           placeholder="Describe your house using few words, no special characters."></textarea>
       </div>

       <br><br>

       <h3 class="text-center text-lg-left title__group-section">Preferences</h3>

       <!-- // * ACADEMY -->
       <div class="col-11 col-lg-10 my-4 mx-auto d-flex justify-content-between align-items-center">
         <div class="w-100 p-0 mx-auto d-flex flex-column form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_p_academy mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">School Preference</label>
           </div>
           <select class="custom-select" id="academy" name="academy">
             <option value="NULL" hidden selected>Select School</option>
             <!-- INSERTAR TODAS LAS ACADEMIAS -->
           </select>
         </div>
       </div>


       <!-- // * GENDER / AGE / STATUS -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center ">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_gender mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Gender Preference</label>
           </div>
           <select class="custom-select" id="preferences-gender" name="preferences-gender" style="width: 290px;">
             <option value="NULL" selected hidden>Select Gender</option>
             <option value="Male">Male</option>
             <option value="Female">Female</option>
             <option value="Any">Any</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_age mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Age Preference</label>
           </div>
           <select class="custom-select" id="preferences-age" name="preferences-age" style="width: 290px;">
             <option value="NULL" selected hidden>Select Age</option>
             <option value="Teenager">Teenager</option>
             <option value="Adult">Adult</option>
             <option value="Any">Any</option>
           </select>
         </div>


         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_smokers mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Smokers Politics</label>
           </div>
           <select class="custom-select" id="smokers-politics" name="smokers-politics" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Outside-OK">Outside-OK</option>
             <option value="Inside-OK">Inside-OK</option>
             <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>
           </select>
         </div>
       </div>


       <!-- // * SMOKERS / MEALS SERVICE / HOMESTAY QUESTION 1 -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_meals "></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Meals Service</label>
           </div>
           <select class="custom-select" id="meals-service" name="meals-service" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group" style="width: 290px;">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_years mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Since when have you been a
               Homestay?</label>
           </div>
           <input type="date" class="form-control" id="total-years" name="total-years">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Do you have pets?</label>
           </div>
           <select class="custom-select" id="pets" name="pets" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>


       <!-- // * PETS -->
       <div
         class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center pet-response">
         <div class="form-group total-pets-and-type">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label for="total-pets" class="m-0 py-2 py-lg-0 text-muted font-weight-light">How many pets?</label>
           </div>
           <input type="number" class="form-control" id="total-pets" name="total-pets" max="10"
             placeholder="Pets Number" style="width: 290px;">
         </div>


         <div
           class="form-group type-of-pet d-flex flex-lg-column align-items-center align-items-lg-start justify-content-between"
           style="width: 290px;">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Kind of Pet</label>
           </div>

           <!-- // ** KINDS OF PET -->
           <div class="w-auto p-2 m-0 d-flex align-items-center justify-content-center form-control">
             <!-- // ** DOG -->
             <input type="checkbox" id="dog" name="dog">
             <label for="dog" class="m-0 py-2 py-lg-0 mr-2 px-1 text-center text-muted font-weight-light">Dog</label>

             <!-- // ** CAT -->
             <input type="checkbox" id="cat" name="cat">
             <label for="cat" class="m-0 py-2 py-lg-0 mr-2 px-1 text-center text-muted font-weight-light">Cat</label>

             <!-- // ** OTHER -->
             <input type="checkbox" id="other" name="other">
             <label for="other" class="m-0 py-2 py-lg-0 px-1 text-center text-muted font-weight-light">Other</label>
           </div>
         </div>


         <div class="form-group specify-type-pet">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label for="specify-pet" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify</label>
           </div>
           <input type="text" class="form-control" id="specify-pet" name="specify-pet" placeholder="Specify"
             style="width: 290px;">
         </div>
       </div>


       <h3 class="text-center text-lg-left title__group-section">Special Diet</h3>

       <!-- // * SPECIAL DIET -->
       <div
         class="col-10 col-lg-6 py-4 pt-5 my-4 mx-auto d-flex flex-column justify-content-between align-items-center rounded special-diet">
         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="vegetarians" name="vegetarians" value="yes">
           <label for="vegetarians" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Vegetarians</label>
           <input type="checkbox" id="halal" name="halal" value="yes">
           <label for="halal" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">Halal (Muslims)</label>
         </div>

         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="kosher" name="kosher" value="yes">
           <label for="kosher" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Kosher (Jews)</label>
           <input type="checkbox" id="lactose" name="lactose" value="yes">
           <label for="lactose" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">Lactose
             Intolerant</label>
         </div>

         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="gluten" name="gluten" value="yes">
           <label for="gluten" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Gluten Free
             Diet</label>
           <input type="checkbox" id="pork" name="pork" value="no">
           <label for="pork" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">No Pork</label>
         </div>


       </div>
     </form>


     <!-- // TODO FAMILY INFORMATION -->
     <form id="family-information" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Family Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Any Member of your Family</h3>

       <!-- // * QUESTION 1 / QUESTION 2 / QUESTION 3 -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies2 mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Have Allergies?</label>
           </div>
           <select class="custom-select" id="allergies" name="allergies" style="width: 260px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Take any Medication?</label>
           </div>
           <select class="custom-select" id="take-medication" name="take-medication" style="width: 260px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies2 mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Have Health Problems?</label>
           </div>
           <select class="custom-select" id="health-problems" name="health-problems" style="width: 260px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>

       <!-- // * RESPONSE 1 / RESPONSE 2 / RESPONSE 3 -->
       <div id="responses-any-members"
         class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center responses">
         <div class="form-group">
           <label for="specify-allergy" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Allergy</label>
           <input type="text" name="specify-allergy" id="specify-allergy" class="form-control" style="width: 260px;">
         </div>

         <div class="form-group">
           <label for="specify-medication" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Medication</label>
           <input type="text" name="specify-medication" id="specify-medication" class="form-control"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <label for="specify-problems" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Problems</label>
           <input type="text" name="specify-problems" id="specify-problems" class="form-control" style="width: 260px;">
         </div>

         <p class="message__input_error"> Write a valid Name </p>
       </div>


       <h3 class="mt-4 text-center text-lg-left title__group-section">Main Contact Information</h3>


       <!-- // * NAME CONTACT / LAST NAME / DATE OF BIRTH -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_mname mr-2"></i>
             <label for="name-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Name Main Contact *</label>
           </div>
           <input type="text" class="form-control" id="name-contact" name="name-contact" placeholder="e.g. John"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_mname mr-2"></i>
             <label for="last-name-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Last Name
               *</label>
           </div>
           <input type="text" class="form-control" id="last-name-contact" name="last-name-contact"
             placeholder="e.g. Smith" style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_db mr-1"></i>
             <label for="date-birth-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Date of Birth
               *</label>
           </div>
           <input type="date" class="form-control" id="date-birth-contact" name="date-birth-contact"
             placeholder="MM-DD-YYYY" style="width: 260px;">
         </div>
       </div>


       <!-- // * GENDER / PHONE NUMBER / OCCUPATION -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_gender2 mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Gender *</label>
           </div>
           <select class="custom-select" id="gender-contact" name="gender-contact" style="width: 260px;">
             <option value="NULL" hidden selected>Select Gender</option>
             <option value="Male">Male</option>
             <option value="Female">Female</option>
             <option value="Private">Private</option>
           </select>
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_pnumber2 mr-2"></i>
             <label for="phone-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Phone Number</label>
           </div>
           <input type="text" class="form-control" id="phone-contact" name="phone-contact" placeholder="e.g. 55578994"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="occupation-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Occupation</label>
           </div>
           <input type="text" class="form-control" id="occupation-contact" name="occupation-contact"
             placeholder="e.g. Lawyer" style="width: 260px;">
         </div>
       </div>


       <!-- // * DATE OF BACKGROUND CHECK -->
       <div class="col-10 my-lg-4 mx-auto d-flex justify-content-center align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_db mr-1"></i>
             <label for="date-background-check" class="m-0 py-2 py-lg-0 text-muted font-weight-light">
               Date of Background Check
             </label>
           </div>
           <input type="date" class="form-control" id="date-background-check" name="date-background-check"
             placeholder="MM-DD-YYYY" style="width: 260px;">
         </div>
       </div>


       <!-- // * PDF -->
       <div class="col-12 col-lg-10 mb-5 mx-auto d-flex flex-column justify-content-center align-items-center">
         <div class="col-12 col-lg-auto my-0 d-flex justify-content-between form-group">
           <div class="w-100 py-1 d-flex align-items-center">
             <i class="icon_bcheck mr-1"></i>
             <label for="pdf-file"
               class="w-100 h-auto form-control text-muted d-flex align-items-center justify-content-between font-weight-light"
               style="cursor:pointer">
               Background Check (Only PDF File)
               <span class="ml-3 py-1 px-4 btn btn-dark text-white ">Upload File</span>
             </label>
           </div>

           <input type="file" class="p-0 p-1 m-0 form-control" name="pdf-file" id="pdf-file" maxlength="1" accept="pdf"
             hidden>
         </div>

         <iframe id="iframe-pdf" class="embed-responsive-item my-2" src=""></iframe>
       </div>



       <h3 class="text-center text-lg-left title__group-section">Family Preferences</h3>


       <!-- // * NUMBER MEMBERS / BACKGROUND / BACKGROUND LANGUAGE -->
       <div class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_family mr-1"></i>
             <label for="number-members" class="w-100 m-0 text-muted font-weight-light">Number Members</label>
           </div>

           <input type="number" class="form-control" id="number-members" name="number-members" min="0" max="10"
             placeholder="Only Numbers" style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="background" class="w-100 m-0 text-muted font-weight-light">Background</label>
           </div>

           <input type="text" class="form-control" id="background" name="background" placeholder="e.g. Canadian"
             style="width: 290px;">
         </div>

         <div class="form-group ">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="background-language" class="w-100 m-0 text-muted font-weight-light">Background Language</label>
           </div>

           <input type="text" class="form-control" id="background-language" name="background-language"
             placeholder="e.g. English" style="width: 290px;">
         </div>
       </div>


       <!-- // * FAMILY QUESTION 1 / FAMILY QUESTION 2 / FAMILY QUESTION 3 -->
       <div class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="religion" class="w-100 m-0 text-muted font-weight-light">
               Do you belong to a religion?
             </label>
           </div>

           <select class="custom-select" name="religion" id="religion" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="condition" class="w-100 m-0 text-muted font-weight-light">
               Any Physical or Mental condition?
             </label>
           </div>

           <select class="custom-select" name="condition" id="condition" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="misdemeanor" class="w-100 m-0 text-muted font-weight-light">
               Have they committed a misdemeanor?
             </label>
           </div>

           <select class="custom-select" name="misdemeanor" id="misdemeanor" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>

       <!-- // * FAMILY RESPONSE 1 / FAMILY RESPONSE 2 / FAMILY RESPONSE 3 -->
       <div id="family-responses"
         class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center responses">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-religion" class="w-100 m-0 text-muted font-weight-light">Which Religion?</label>
           </div>

           <input type="text" class="form-control" id="specify-religion" name="specify-religion" min="0" max="10"
             style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-condition" class="w-100 m-0 text-muted font-weight-light">Which Condition?</label>
           </div>

           <input type="text" class="form-control" id="specify-condition" name="specify-condition"
             style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-misdemeanor" class="w-100 m-0 text-muted font-weight-light">
               Specify misdemeanor?
             </label>
           </div>

           <input type="text" class="form-control" id="specify-misdemeanor" name="specify-misdemeanor"
             style="width: 290px;">
         </div>
       </div>


       <!-- // * FAMILY FINAL QUESTION -->
       <div class="col-10 my-3 my-lg-5 mx-auto d-flex justify-content-center align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label class="w-100 m-0 text-muted font-weight-light">Do you give us your consent
               to go to the authorities and check your criminal background check?</label>
           </div>

           <select name="family-final-question" id="family-final-question" class="custom-select" style="width: 100%;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes"> Yes </option>
             <option value="No"> No </option>
           </select>
         </div>
       </div>



       <!-- // TODO ADD FAMILY MEMBERS -->
       <h3 class="text-center text-lg-left title__group-section">Family Members</h3>

       <div id="family-members"></div>

       <template id="family-member-template">
         <section class="family-member">
           <!-- // * NAME CONTACT / LAST NAME / DATE OF BIRTH -->
           <div
             class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_mname mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Name</label>
               </div>
               <input type="text" class="form-control" data-name placeholder="e.g. Melissa" style="width: 260px;">
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_mname mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Last Name</label>
               </div>
               <input type="text" class="form-control" data-last-name placeholder="e.g. Smith" style="width: 260px;">
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_db mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Date of Birth</label>
               </div>
               <input type="date" class="form-control" data-date-birth placeholder="MM-DD-YYYY" style="width: 260px;">
             </div>
           </div>


           <!-- // * GENDER / RELATION / OCCUPATION -->
           <div
             class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
             <div class="form-group">
               <div class="d-flex align-items-center">
                 <i class="icon_gender2 mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Gender</label>
               </div>
               <select class="custom-select" data-gender style="width: 260px;">
                 <option value="NULL" hidden selected>Select Gender</option>
                 <option value="Male">Male</option>
                 <option value="Female">Female</option>
                 <option value="Private">Private</option>
               </select>
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_family mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Relation</label>
               </div>
               <select class="custom-select" data-relation style="width: 260px;">
                 <option value="NULL" hidden selected>Select Relation</option>
                 <option value="Dad">Dad</option>
                 <option value="Mom">Mom</option>
                 <option value="Son">Son</option>
                 <option value="Daughter">Daughter</option>
                 <option value="Grandparents">Grandparents</option>
                 <option value="Others">Others</option>
               </select>

             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_check mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Occupation</label>
               </div>
               <input type="text" class="form-control" data-occupation placeholder="e.g. Lawyer" style="width: 260px;">

             </div>
           </div>


           <!-- // * DATE OF BACKGROUND CHECK / BACKGROUND CHECK -->
           <div class="col-10 my-lg-4 mx-auto d-flex justify-content-around align-items-center">
             <div class="form-group">
               <div class="py-1 mb-3 d-flex flex-column flex-lg-row align-items-center justify-content-center">
                 <div class="d-flex align-content-center w-auto" style="width: 260px;">
                   <i class="icon_db mr-1"></i>
                   <label class="m-0 mr-2 text-muted font-weight-light">Date of Background Check</label>
                 </div>
                 <input type="date" class="form-control" data-date-background-check placeholder="MM-DD-YYYY"
                   style="width: 260px;">
               </div>

               <iframe id="iframe-pdf" data-member-pdf class="embed-responsive-item my-2" src=""></iframe>

               <div class="py-1 d-flex align-items-center">
                 <i class="icon_bcheck mr-1"></i>
                 <label class="w-100 m-0 text-muted font-weight-light">Background Check</label>
               </div>
               <input type="file" class="w-100 p-0 p-1 m-0 form-control" data-background-check maxlength="1"
                 accept="pdf">
             </div>
           </div>


         </section>
         <br>
         <hr>
         <br><br>
       </template>

       <div class="col-10 my-5 mb-0 mx-auto d-flex justify-content-center align-items-center">
         <button id="add-member" type="button" class="mx-auto btn btn-add-family-member">+ Add Family Member</button>
       </div>

     </form>

     <!-- // TODO ULTIMATE BUTTONS -->
     <div class="w-100 d-flex align-items-center justify-content-end rounded border-0 form__group-save "
       style="transform: translateY(-6rem); padding: 2rem 0 !important;">
       <button id="submit" class="btn btn-primary px-5 py-3 text-white border-0 ts-btn-arrow btn-lg"
         style="background-color: #232159;">
         <i class="fa fa-save mr-2"></i> Save Changes
       </button>
     </div>


     <!-- // TODO MODAL DYNAMIC -->
     <div id="modal-dynamic" class="modal-dynamic">

       <section class="modal-content">
         <span class="close">X</span>
         <h3>Title</h3>


         <div class="modal-empty">
           <img src="../assets/img/homestay_home.png" alt="empty" rel="noopener">
         </div>


         <table class="modal-table">
           <thead>
             <tr>
               <th>Image</th>
               <th>Name</th>
               <th>Bed</th>
               <th>Start</th>
               <th>End</th>
             </tr>
           </thead>
           <tbody>
           </tbody>
         </table>

       </section>

     </div>
   </main>


   <!--// TODO FOOTER ===============================================================-->
   <?php include 'footer.php'; ?>

   <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.5"></script>
   <script src="../assets/js/popper.min.js?ver=1.0.5"></script>
   <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.5"></script>
   <script src="../assets/js/leaflet.js?ver=1.0.5"></script>
   <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.5"></script>
   <script src="../assets/js/map-leaflet.js?ver=1.0.5"></script>
   <script src="../manager/assets/js/rooms.js?ver=1.0.5"></script>
   <script src="../manager/assets/js/galery-homestay.js?ver=1.0.5"></script>
   <!--// * Date Input Safari-->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js?ver=1.0.5"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js?ver=1.0.5"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js?ver=1.0.5"></script>
   <script src="assets/js/date_safari.js?ver=1.0.5"></script>


   <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.5"></script>
   <script src="assets/js/edit_property.js?ver=1.0.5" type="module"></script>

 </body>

 </html>