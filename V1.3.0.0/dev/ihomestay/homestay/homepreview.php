<?php
session_start();
include_once('../xeon.php');
error_reporting(0);

// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];


$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2 = $query2->fetch_assoc();

$_SESSION['mail_h'] = $row['mail_h']; 

$query3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();

$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
$row6=$query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM propertie_control WHERE id_home = '$id'");
$row7=$query7->fetch_assoc();

$query9 = $link->query("SELECT * FROM academy");
$row9=$query9->fetch_assoc();

$query10 = $link->query("SELECT * FROM academy INNER JOIN pe_home ON pe_home.id_home = '$id' AND academy.id_ac = pe_home.a_pre");
$row10=$query10->fetch_assoc();



    if ($row5['usert'] == 'Admin') {
        $way = '../master/index.php';
    } elseif ($row5['usert'] == 'Cordinator') {
       $way = '../master/cordinator.php';
        echo '<style type="text/css"> a#admin{display:none;} </style>';
    }

}

if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
     header("location: ../logout.php");   
    }
}
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../homestay/assets/css/carousel.css">
    <link rel="stylesheet" href="../homestay/assets/css/galery.css">
    <link rel="stylesheet" href="assets/css/homepreview.css">

    <link rel="stylesheet" href="../assets/datepicker/css/rome.css">
    <link rel="stylesheet" href="../assets/datepicker/css/style.css">
     
    <!-- Mapbox Link -->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
    <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Preview</title>

</head>
<style type="text/css">
                                    
                                </style>

<body style="background-color: #F3F8FC;">

<header id="ts-header" class="fixed-top" style="background-color: white;">

<?php include '../master/header.php' ?>

</header>

<div class="container__title">
<h1 class="title__register">Homestay Information </h1>

</div>

<form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">
<main class="card-main">
<div class="form__group-house_in form__group">

    <div class="form__target-header">
        <h3 class="title_target"> Basic Information </h3>
    </div>

    <h2 class="title__group-section">House Information</h2>

    <div class="row form__group-row px-4 house_info__div">

        <?php if($row['name_h'] == 'NULL'){}else{ ?>

            <div class="form__group-h_name col-lg-4" id="form__group-h_name">
                <div class="form__group-hinf">
                    <label for="h_name" class="label__name house_info">House Name</label>
                    <label for="h_name" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>
                    <p id="h_name group__h_name" class="form__group-input hname"> <?php echo $row['h_name'] ?> </p>
                </div>
                <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>
            </div>

        <?php } 
        
        if($row['num'] == 'NULL'){}else{
        
        ?>

            <div class="form__group-num col-lg-4" id="form__group-num">
                <div class="form__group-hinf">
                    <label for="p_num" class="label__name house_info">Phone Number</label>
                    <label for="p_num" class="l_icon" title="Phone Number"> <i class="icon icon_pnumber"></i> </label>
                    <p id="p_num group__p_num" class="form__group-input pnumber"> <?php echo $row['num'] ?> </p>
                </div>

                <p class="message__input_error"> Only numbers. Min 6 characters </p>
            </div>

        <?php } 
        
        if($row['room'] == 'NULL'){}else{
        
        ?>

            <div class="form__group-room col-lg-4 " id="form__group-room">
                <div class="form__group-hinf">
                    <label for="room" class="label__name hi_room">Rooms in your House</label>
                    <label for="room" class="l_icon" title="Rooms in your House"> <i class="icon icon_rooms"></i> </label>
                    <p class="form__group-input rooms" > <?php echo $row['room'] ?> </p>
                </div>

                <p class="message__input_error"> Only numbers. Max 8 rooms </p>
            </div>

        <?php } 
        
        if($row['mail_h'] == 'NULL'){}else{

        ?>

            <div class="form__group-mail col-lg-4" id="form__group-mail">
                <div class="form__group-hinf">
                    <label for="mail" class="label__name house_info">Homestay Mail</label>
                    <label for="mail" class="l_icon" title="Mail"> <i class="icon icon_mail"></i> </label>
                    <p id="pass group__mail" class="form__group-input mail"> <?php echo $row['mail_h'] ?> </p>
                </div>

                <p class="message__input_error"> Invalid Email </p>
            </div>

        <?php } ?>
        

    </div>


    <h2 class="title__group-section">Location</h2>

    <div class="row form__group-row px-4">

        <div class="form__group-adress col-sm-6">
        
            <?php if($row['dir'] == 'NULL'){}else{ ?>

                <div class="form__group-location col-sm-12 form__group-dir" id="form__group-dir">
                    <div class="form__group-hinf loc">
                        <label for="dir" class="label__name location">Address</label>
                        <label for="dir" class="l_icon" title="Avenue, street, boulevard, etc."> <i class="icon icon_location"></i> </label>
                        <p class="form__group-input address" placeholder="Av, Street, etc."> <?php echo $row['dir'] ?> </p>

                    </div>
                </div>

            <?php } if($row['city'] == 'NULL'){}else{ ?>

                <div class="form__group-location col-sm-12 form__group-city" id="form__group-city">

                    <div class="form__group-hinf loc">
                        <label for="city" class="label__name location">City</label>
                        <label for="city" class="l_icon" title="City"> <i class="icon icon_location"></i> </label>
                        <p class="form__group-input city"> <?php echo $row['city'] ?> </p>
                    </div>

                </div>

            <?php } if($row['state'] == 'NULL'){}else{ ?>

                <div class="form__group-location col-sm-12 form__group-state" id="form__group-state">

                    <div class="form__group-hinf loc">
                        <label for="state" class="label__name location">State / Province</label>
                        <label for="state" class="l_icon" title="State / Province"> <i class="icon icon_location2"></i> </label>
                        <p class="form__group-input state"> <?php echo $row['state'] ?> </p>
                    </div>
                </div>

            <?php } if($row['p_code'] == 'NULL'){}else{ ?>

                <div class="form__group-location col-sm-12 form__group-pcode" id="form__group-p_code">

                    <div class="form__group-hinf loc">

                        <label for="p_code" class="label__name location">Postal Code</label>
                        <label for="p_code" class="l_icon" title="Postal Code"> <i class="icon icon_location2"></i> </label>
                        <p class="form__group-input pcode" > <?php echo $row['p_code'] ?> </p>

                    </div>

                </div>

            <?php } ?>

        </div>

        <div class="form__group-map col-sm-6">
            <div id='map' style="transform: translateY(12.5%);"></div>
        </div>

        <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>

                        <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                        <script>
                            mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                            console.log(mapboxgl.accessToken);
                            var client = new MapboxClient(mapboxgl.accessToken);
                            console.log(client);

                            
                            var address ="<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                            var test = client.geocodeForward(address, function(err, data, res) {
                            // data is the geocoding result as parsed JSON
                            // res is the http response, including: status, headers and entity properties

                            console.log(res);
                            console.log(res.url);
                            console.log(data);

                            var coordinates = data.features[0].center;

                            var map = new mapboxgl.Map({
                                container: 'map',
                                style: 'mapbox://styles/mapbox/streets-v10',
                                center: coordinates,
                                zoom: 16
                            });
                            var ei = document.createElement('div');
                            ei.id = 'house';

                            new mapboxgl.Marker(ei)
                            .setLngLat(coordinates)
                            .addTo(map);


                            });
                        </script>
    
    </div><br>

</div>

</main>

















<main class="card-main">
<div class="form__group-house_in form__group">

    <div class="form__target-header">
        <h3 class="title_target"> House Details </h3>
    </div>

    <h2 class="title__group-section">Photo Gallery</h2>


    <div class="row" style="margin: 0; margin-bottom: 3rem; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

        <?php if($row['phome'] == 'NULL'){}else{ ?>

            <div class="tarjet">
                
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Frontage Photo </h4>

                <div class="div-img-r" align="center">
                    <div class="carousel-item active">
                        <img src="<?php echo '../'.$row['phome'].''?>" class="add-photo" >
                    </div>
                </div>

            </div>

        <?php } if($row4['pliving'] == 'NULL'){}else{ ?>

            <!-- Living Room Photo -->
            <div class="tarjet" >

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Living Room Photo </h4>

                <div class="div-img-r" align="center">
                    <div class="carousel-item active">
                        <img src="<?php echo '../'.$row4['pliving'].''?>" class="add-photo">
                    </div>
                </div>

            </div>

        <?php } if($row4['fp'] == 'NULL'){}else{ ?>

            <div class="tarjet">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; "> Family Picture </h4>

                <div class="div-img-r" align="center">
                    <div class="carousel-item active">
                        <img src="<?php echo '../'.$row4['fp'].''?>" class="add-photo">
                    </div>
                </div>
                        
            </div>
        
        <?php } if($row4['parea1'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Kitchen </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['parea1'].''?>" class="add-photo">

                </div>
        

            </div>
        
        <?php } if($row4['parea2'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Dining Room </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['parea2'].''?>" class="add-photo">

                </div>
        

            </div>

        <?php } if($row4['parea3'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Area 3 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['parea3'].''?>" class="add-photo">

                </div>
        

            </div>
        
        <?php } if($row4['parea4'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Area 4 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['parea4'].''?>" class="add-photo">

                </div>
        

            </div>


        <?php } if($row4['pbath1'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Bathroom Photo 1 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['pbath1'].''?>" class="add-photo">

                </div>
        

            </div>

        <?php } if($row4['pbath2'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Bathroom Photo 2 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['pbath2'].''?>" class="add-photo">

                </div>
        

            </div>

        <?php } if($row4['pbath3'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Bathroom Photo 3 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['pbath3'].''?>" class="add-photo">

                </div>
        

            </div>

        <?php } if($row4['pbath4'] == 'NULL'){}else{ ?>

            <div class="tarjet" align="center">

                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Bathroom Photo 3 </h4>

                <div class="carousel-item active">

                    <img src="<?php echo '../'.$row4['pbath4'].''?>" class="add-photo">

                </div>
        

            </div>

        <?php } ?>
                
    </div>


    <h2 class="title__group-section">Bedrooms</h2>

    <div class="row" style="margin: 0; margin-bottom: 3rem; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

        <?php if ($row2['date1'] == 'Disabled' OR $row2['date1'] == 'NULL') {}else{ ?>

            <!-- Room 1 -->
            <div class="tarjet tarjet-rooms " id="room1">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>

                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom1_2'] == 'NULL' OR $row4['proom1_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom1'].''?>" alt="Room1" class="img-room">



                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">

                            <ol class="carousel-indicators">
                                <?php if ($row4['proom1'] == 'NULL' OR $row4['proom1'] == '') {}else{ ?>
                                    <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                <?php } if ($row4['proom1_2'] == 'NULL' OR $row4['proom1_2'] == '') {}else{ ?>
                                    <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                <?php }if ($row4['proom1_3'] == 'NULL' OR $row4['proom1_3'] == '') {}else{ ?>
                                    <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                <?php } ?>
                            </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom1'] == 'NULL' OR $row4['proom1'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom1'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom1_2'] == 'NULL' OR $row4['proom1_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom1_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom1_3'] == 'NULL' OR $row4['proom1_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom1_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>
                    <?php } ?>

                </div><br>


                <div class="row init">

                    <div class="details in-d">

                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type1'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed1'] ?> </p>

                    </div>

                </div>


                <div class="row init">

                    <div class="details in-d">

                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date1'] ?> </p>

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food1'] ?> </p>

                    </div>

                
                </div><br>


                <div class="row init init_price justify-content-center">
                                                
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                    <div style="display: flex;">
                        <input type="text" class="form-control pl-2 pr-0 aprox_label" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox1'] + $row2['aprox_a1']; ?></p>
                    </div>

                </div><br>

            </div>



        
        <?php }  if ($row2['date2'] == 'Disabled' OR $row2['date2'] == 'NULL') {}else{ ?>




            <!-- Room 2 -->

            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>

                <div class="div-img-r" align="center">

                <?php 
                    if ($row4['proom2_2'] == 'NULL' OR $row4['proom2_2'] == '') {
                ?>

                    <img src="<?php echo '../'.$row4['proom2'].''?>" alt="Room1" class="img-room">

                <?php 
                    }else{
                ?>

                    <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom2'] == 'NULL' OR $row4['proom2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom2_2'] == 'NULL' OR $row4['proom2_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                            <?php } if ($row4['proom2_3'] == 'NULL' OR $row4['proom2_3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                            <?php } ?>
                        </ol>

                        <div class="carousel-inner">

                            <?php if ($row4['proom2'] == 'NULL' OR $row4['proom2'] == '') {}else{ ?>

                                <div class="carousel-item active">

                                    <img src="<?php echo '../'.$row4['proom2'].''?>" class="add-photo" >

                                </div>

                            <?php } if ($row4['proom2_2'] == 'NULL' OR $row4['proom2_2'] == '') {}else{ ?>

                                <div class="carousel-item">

                                    <img src="<?php echo '../'.$row4['proom2_2'].''?>" class="add-photo" >

                                </div>

                            <?php } if ($row4['proom2_3'] == 'NULL' OR $row4['proom2_3'] == '') {}else{ ?>

                                <div class="carousel-item">

                                    <img src="<?php echo '../'.$row4['proom2_3'].''?>" class="add-photo" >

                                </div>

                            <?php } ?>

                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>

                    </div>

                <?php } ?>

                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type2'] ?> </p>  

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed2'] ?> </p>  

                    </div>

                </div>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date2'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food2'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">
                                                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                    <div style="display: flex;">
                                                
                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox2'] + $row2['aprox_a2']; ?></p>

                    </div>


                </div><br>
                    

            </div>



        <?php } if ($row2['date3'] == 'Disabled' OR $row2['date3'] == 'NULL') {}else{ ?>


            <!-- Room 3 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom3_2'] == 'NULL' OR $row4['proom3_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom3'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom3'] == 'NULL' OR $row4['proom3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom3_2'] == 'NULL' OR $row4['proom3_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                            <?php } if ($row4['proom3_3'] == 'NULL' OR $row4['proom3_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom3'] == 'NULL' OR $row4['proom3'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom3'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom3_2'] == 'NULL' OR $row4['proom3_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom3_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom3_3'] == 'NULL' OR $row4['proom3_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom3_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type3'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed3'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date3'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food3'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">

                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox3'] + $row2['aprox_a3']; ?></p>

                    </div>

                </div><br>
                    

            </div>

        <?php } if ($row2['date4'] == 'Disabled' OR $row2['date4'] == 'NULL') {}else{ ?>


            <!-- Room 4 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom4_2'] == 'NULL' OR $row4['proom4_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom4'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom4'] == 'NULL' OR $row4['proom4'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom4_2'] == 'NULL' OR $row4['proom4_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                            <?php } if ($row4['proom4_3'] == 'NULL' OR $row4['proom4_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom4'] == 'NULL' OR $row4['proom4'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom4'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom4_2'] == 'NULL' OR $row4['proom4_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom4_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom4_3'] == 'NULL' OR $row4['proom4_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom4_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type4'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed4'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date4'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food4'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">

                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox4'] + $row2['aprox_a4']; ?></p>

                    </div>

                </div><br>
                    

            </div>


        <?php } if ($row2['date5'] == 'Disabled' OR $row2['date5'] == 'NULL') {}else{ ?>


            <!-- Room 5 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom5_2'] == 'NULL' OR $row4['proom5_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom5'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom5'] == 'NULL' OR $row4['proom5'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom5_2'] == 'NULL' OR $row4['proom5_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                            <?php } if ($row4['proom5_3'] == 'NULL' OR $row4['proom5_3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom5'] == 'NULL' OR $row4['proom5'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom5'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom5_2'] == 'NULL' OR $row4['proom5_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom5_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom5_3'] == 'NULL' OR $row4['proom5_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom5_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type5'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed5'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date5'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food5'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">

                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox5'] + $row2['aprox_a5']; ?></p>

                    </div>

                </div><br>
                    

            </div>

        <?php } if ($row2['date6'] == 'Disabled' OR $row2['date6'] == 'NULL') {}else{ ?>


            <!-- Room 6 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 6 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom6_2'] == 'NULL' OR $row4['proom6_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom6'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom6'] == 'NULL' OR $row4['proom6'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom6_2'] == 'NULL' OR $row4['proom6_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                            <?php } if ($row4['proom6_3'] == 'NULL' OR $row4['proom6_3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom6'] == 'NULL' OR $row4['proom6'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom6'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom6_2'] == 'NULL' OR $row4['proom6_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom6_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom6_3'] == 'NULL' OR $row4['proom6_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom6_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type6'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed6'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date6'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food6'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">

                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox6'] + $row2['aprox_a6']; ?></p>

                    </div>

                </div><br>
                    

            </div>

        <?php } if ($row2['date7'] == 'Disabled' OR $row2['date7'] == 'NULL') {}else{ ?>


            <!-- Room 7 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 7 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom7_2'] == 'NULL' OR $row4['proom7_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom7'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom7'] == 'NULL' OR $row4['proom7'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom7_2'] == 'NULL' OR $row4['proom7_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>
                            <?php } if ($row4['proom7_3'] == 'NULL' OR $row4['proom7_3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom7'] == 'NULL' OR $row4['proom7'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom7'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom7_2'] == 'NULL' OR $row4['proom7_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom7_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom7_3'] == 'NULL' OR $row4['proom7_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom7_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type7'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed7'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date7'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food7'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">

                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox7'] + $row2['aprox_a7']; ?></p>

                    </div>

                </div><br>
                    

            </div>

            
        <?php } if ($row2['date8'] == 'Disabled' OR $row2['date8'] == 'NULL') {}else{ ?>


            <!-- Room 8 -->
            <div class="tarjet tarjet-rooms">
            
                <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; padding-top: .7rem; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 8 </h4>


                <div class="div-img-r" align="center">

                    <?php 
                        if ($row4['proom8_2'] == 'NULL' OR $row4['proom8_2'] == '') {
                    ?>

                        <img src="<?php echo '../'.$row4['proom8'].''?>" alt="Room1" class="img-room">

                    <?php 
                        }else{
                    ?>

                        <div id="carouselExampleIndicators-8" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <?php if ($row4['proom8'] == 'NULL' OR $row4['proom8'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-8" data-slide-to="0" class="active"> </li>
                            <?php } if ($row4['proom8_2'] == 'NULL' OR $row4['proom8_2'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-8" data-slide-to="1"></li>
                            <?php } if ($row4['proom8_3'] == 'NULL' OR $row4['proom8_3'] == '') {}else{ ?>
                                <li data-target="#carouselExampleIndicators-8" data-slide-to="2"></li> 
                            <?php } ?>
                        </ol>

                            <div class="carousel-inner">

                                <?php if ($row4['proom8'] == 'NULL' OR $row4['proom8'] == '') {}else{ ?>

                                    <div class="carousel-item active">

                                        <img src="<?php echo '../'.$row4['proom8'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom8_2'] == 'NULL' OR $row4['proom8_2'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom8_2'].''?>" class="add-photo" >

                                    </div>

                                <?php } if ($row4['proom8_3'] == 'NULL' OR $row4['proom8_3'] == '') {}else{ ?>

                                    <div class="carousel-item">

                                        <img src="<?php echo '../'.$row4['proom8_3'].''?>" class="add-photo" >

                                    </div>

                                <?php } ?>

                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators-8" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="carousel-control-next" href="#carouselExampleIndicators-8" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>

                    <?php } ?>
                                        
                </div><br>


                <div class="row init">

                    <div class="details in-d">
                                        
                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">

                        <p class="tex-bed"> <?php echo $row2['type8'] ?> </p>  

                    </div>

                    <div class="details">
                                            
                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">

                        <p class="tex-bed"> <?php echo $row2['bed8'] ?> </p>  

                    </div>

                </div>


                <div class="row init">
                                    
                    <div class="details in-d">
                            
                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">

                        <p class="tex-bed"> <?php echo $row2['date8'] ?> </p>

                    </div>

                    <div class="details">
                                        
                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">

                        <p class="tex-bed"> <?php echo $row2['food8'] ?> </p>

                    </div>

                </div><br>

                <div class="row init init_price justify-content-center">
                    
                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price </p>
                            
                    <div style="display: flex;">

                        <input type="text" class="form-control aprox_label pl-2 pr-0" readonly value="CAD$" style="width:40%;">
                        <p class="form-control" id="number"> <?php echo $row2['aprox8'] + $row2['aprox_a8']; ?></p>

                    </div>

                </div><br>
                    

            </div>

        <?php } ?>

    </div>

</div>

</main>


<main class="card-main">

<div class="form__group-house_in form__group">

<div class="form__target-header">
    <h3 class="title_target"> Additional Information </h3>
</div>

<?php if(empty($row['des']) || $row['des'] == 'NULL'){}else{ ?>

    <h2 class="title__group-section">Description of the House</h2>

    <div class="form__group-description_div">

        <div class="form__group-description">

            <label for="address" class="l_icon" title="Description"> <i class="icon icon_description"></i> </label>
            <p class="form__group-input description"><?php echo $row['des'] ?></p>
            
        </div>


    </div>

<?php } ?>

<h2 class="title__group-section">Preferences</h2>

<div class="row form__group-preferences form__group-preferences1">

    <?php if(empty($row10['name_a']) || $row10['name_a'] == 'NULL'){}else{ ?>
            
        <div class="form__group-academy col-xl-5 p-0">
            <label for="a_pre" class="label__name preference">Academy Preference</label>
            <label for="a_pre" class="l_icon" title="Academy Preference"> <i class="icon icon_p_academy"></i> </label>
            <p class="form__group-input academy"><?php echo $row10['name_a'] ?></p>
            
        </div>

    <?php } if(empty($row['g_pre']) || $row['g_pre'] == 'NULL'){}else{ ?>

        <div class="form__group-gender col-xl-3 p-0">
            <label for="g_pre" class="label__name preference">Gender Preference</label>
            <label for="g_pre" class="l_icon" title="Gender Preference"> <i class="icon icon_gender"></i> </label>

            <p class="form__group-input gender"><?php echo $row['g_pre'] ?></p>
        </div>

    <?php } if(empty($row['ag_pre']) || $row['ag_pre'] == 'NULL'){}else{ ?>

        <div class="form__group-age col-xl-2 p-0">
            <label for="ag_pre" class="label__name preference">Age Preference</label>
            <label for="ag_pre" class="l_icon" title="Age Preference"> <i class="icon icon_age"></i> </label>

            <p class="form__group-input age"><?php echo $row['ag_pre'] ?></p>
        </div>

    <?php } if(empty($row['status']) || $row['status'] == 'NULL'){}else{ ?>
                    
        <div class="form__group-status form__group col-xl-3 p-0">
            <label for="status" class="label__name preference">House Status</label>
            <label for="status" class="l_icon" title="House Status"> <i class="icon icon_status"></i> </label>

            <p class="form__group-input status"><?php echo $row['status'] ?></p>
        </div>

    <?php } if(empty($row['smoke']) || $row['smoke'] == 'NULL'){}else{ ?>

        <div class="form__group-smokers form__group col-xl-3 p-0">
            <label for="smoke" class="label__name preference">Smokers Politics</label>
            <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_smokers"></i> </label>

            <p class="form__group-input smoke"><?php echo $row['smoke'] ?></p>
        </div>

    <?php } if(empty($row['m_service']) || $row['m_service'] == 'NULL'){}else{ ?>
        
        <div class="form__group-smokers form__group col-xl-3 p-0">
            <label for="smoke" class="label__name preference">Meals Service</label>
            <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_meals"></i> </label>

            <p class="form__group-input smoke"><?php echo $row['m_service'] ?></p>
        </div>

    <?php } if(empty($row['y_service']) || $row['y_service'] == 'NULL'){}else{ ?>
        
        <div class="form__group-smokers form__group col-xl-4 p-0">
            <label for="smoke" class="label__name preference">Since when have you been a Homestay?</label>
            <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_years"></i> </label>

            <p class="form__group-input smoke"><?php echo $row['y_service'] ?></p>
        </div>

    <?php } ?>

</div>


<div class="row form__group-preferences">
    
    <div class="form__group-diet col-lg-5 p-0">
        <h2 class="title__group-preference"><i class="icon icon-diet"></i> Special Diet</h2>

        <div class="form__group-diet-in">
        
            <div class="custom-control custom-checkbox">
            <?php if(empty($row['vegetarians']) || $row['vegetarians'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" checked>
            <?php } ?>
                <label class="custom-control-label" for="vegetarians">Vegetarians</label>
            </div>

            <div class="custom-control custom-checkbox">
            <?php if(empty($row['halal']) || $row['halal'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="halal" name="halal" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="halal" name="halal" checked>
            <?php } ?>
                <label class="custom-control-label" for="halal">Halal (Muslims)</label>
            </div>

        </div>

        <div class="form__group-diet-in">

            <div class="custom-control custom-checkbox">
            <?php if(empty($row['kosher']) || $row['kosher'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" checked>
            <?php } ?>
                <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
            </div>

            <div class="custom-control custom-checkbox">
            <?php if(empty($row['lactose']) || $row['lactose'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" checked>
            <?php } ?>
                <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
            </div>

        </div>

        <div class="form__group-diet-in">

            <div class="custom-control custom-checkbox">
            <?php if(empty($row['gluten']) || $row['gluten'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" checked>
            <?php } ?>
                <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
            </div>

            <div class="custom-control custom-checkbox">
            <?php if(empty($row['pork']) || $row['pork'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="pork" name="pork" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="pork" name="pork" checked>
            <?php } ?>
                <label class="custom-control-label" for="pork">No Pork</label>
            </div>
        
        </div>

        <div class="form__group-diet-in">

            <div class="custom-control custom-checkbox-none custom-checkbox">
            <?php if(empty($row['none']) || $row['none'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="none" name="none" disabled>
            <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="none" name="none" checked>
            <?php } ?>
                <label class="custom-control-label" for="none">None</label>
            </div>

        </div>

    </div>

    <div class="form__group-pets col-lg-5 p-0">
        <h2 class="title__group-preference"><i class="icon icon-pets"></i> Pets</h2>

        <div class="form__group-pets-in">
            
            <?php if(empty($row['pet'])){}else if($row['pet'] == 'NULL'){ ?>

                <div class="form__group-ypets">
                    <label for="pet" class="label__name pets_label" >Pets</label>
                    <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>

                    <p class="form__group-input ypets">No</p>
                </div>

            <?php }else{ ?>

                <div class="form__group-ypets">
                    <label for="pet" class="label__name pets_label" >Pets</label>
                    <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>

                    <p class="form__group-input ypets"><?php echo $row['pet'] ?></p>
                </div>

            <?php } if($row['pet_num'] == '0'){ ?>

                <div class="form__group-npets">
                    <label for="pet" class="label__name pets_label" >Number of Pets</label>
                    <label for="npets" class="l_icon" title="Pets Number"> <i class="icon icon_pets"></i> </label>
                    <p class="form__group-input npets">0</p>
                </div>

            <?php }else{?>
        
                <div class="form__group-npets">
                    <label for="pet" class="label__name pets_label" >Number of Pets</label>
                    <label for="npets" class="l_icon" title="Pets Number"> <i class="icon icon_pets"></i> </label>
                    <p class="form__group-input npets"><?php echo $row['pet_num'] ?></p>
                </div>

            <?php } ?>

        </div>

        <div class="form__group-pets-in">
            <div class="custom-control custom-checkbox custom-checkbox-pets">
            <?php if(empty($row['dog']) || $row['dog'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input custom-checkbox-dogs" id="dog" name="dog" disabled>
            <?php } else{ ?>
                <input type="checkbox" class="custom-control-input custom-checkbox-dogs" id="dog" name="dog" checked>
            <?php } ?>
                <label class="custom-control-label" for="dog">Dogs</label>
            </div>
                             
            <div class="custom-control custom-checkbox custom-checkbox-pets">
            <?php if(empty($row['cat']) || $row['cat'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="cat" name="cat" disabled>
            <?php } else{ ?>
                <input type="checkbox" class="custom-control-input" id="cat" name="cat" checked>
            <?php } ?>

                <label class="custom-control-label" for="cat">Cats</label>
            </div>
        </div>

        <div class="form__group-pets-in">
            <div class="custom-control custom-checkbox custom-checkbox-pets">
            <?php if(empty($row['other']) || $row['other'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="other" name="other" disabled>
            <?php } else{ ?>
                <input type="checkbox" class="custom-control-input" id="other" name="other" checked>
            <?php } ?>
                <label class="custom-control-label" for="other">Others</label>
            </div>

            <div class="form__group-others">
            <?php if(empty($row['type_pet'])){}else if( $row['type_pet'] == 'NULL'){ ?>
                <p class="form__group-input others">No</p>
            <?php }else{ ?>
                <p class="form__group-input others"><?php echo $row['type_pet'] ?></p>
            <?php } ?>
            </div>
            
                
        </div>

    </div>

</div>

</main>



<main class="card-main">

<div class="form__group-house_in form__group">
    <div class="form__target-header">
        <h3 class="title_target"> Family Information </h3>
    </div>

    <h2 class="title__group-section">Main Contact Information</h2>

    <div class="row form__group-row margin-t">

        <?php if(empty($row['name_h']) || $row['name_h'] == 'NULL'){}else{ ?>

            <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-name_h">
                
                <label for="name_h" class="l_icon" title="Name Main Contact"> <i class="icon icon_mname"></i> </label>
                <label for="name_h" class="label__name label__family" >Name Main Contact</label>
                <p class="form__group-input mname"><?php echo $row['name_h']; ?></p>

            </div>

        <?php } if(empty($row['l_name_h']) || $row['l_name_h'] == 'NULL'){}else{ ?>
        
            <div class="form__group-lmname form__group-hinf2 col-md-4" id="form__group-l_name_h">
                <label for="l_name_h" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_mname"></i> </label>
                <label for="l_name_h" class="label__name label__family" >Last Name</label>
                <p class="form__group-input mname"><?php echo $row['l_name_h']; ?></p>

            </div>

        <?php } if(empty($row['db']) || $row['db'] == 'NULL'){}else{ ?>

            <div class="form__group-lmname form__group-hinf2 col-md-4">
                <label for="db" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_db"></i> </label>
                <label for="db" class="label__name label__family" >Date of Birth</label>
                <p class="form__group-input dateb"><?php echo $row['db']; ?></p>
            </div>

        <?php } if(empty($row['db']) || $row['db'] == 'NULL'){}else{ ?>

    
            <div class="form__group-gender form__group-hinf2 col-md-4">
                <label for="gender" class="label__name label__family" >Gender</label>
                <label for="gender" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <p class="form__group-input mgender"><?php echo $row['gender']; ?></p>
            </div>

        <?php } if(empty($row['cell']) || $row['cell'] == 'NULL'){}else{ ?>

            <div class="form__group-db-check form__group-hinf2 col-md-4" id="form__group-cell">

                <label for="cell" class="label__name label__family" >Alternative Phone</label>
                <label for="cell" class="l_icon" title="Alternative Phone"> <i class="icon icon_pnumber"></i> </label>
                <p class="form__group-input alter_phone" ><?php echo $row['cell']; ?></p>

            </div>

        <?php } if(empty($row['db_law']) || $row['db_law'] == 'NULL'){}else{ ?>

            <div class="form__group-db-check form__group-hinf2 col-md-4">
                <label for="db_law" class="l_icon" title="Date of Background Check"> <i class="icon icon_db"></i> </label>
                <label for="db_law" class="label__name label__family" >Date of Background Check</label>
                <p class="form__group-input db_law"><?php echo $row['db_law']; ?></p>
            </div>

        <?php } if(empty($row['law']) || $row['law'] == 'NULL'){}else{ ?>


            <div class="form__group-b-check form__group-hinf2 col-md-7">
                <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                <iframe class="embed-responsive-item" src="../'<?php echo $row['law']?>'"></iframe>
            </div>

        <?php } ?> 

    </div>

    <?php if($row['num_mem'] == '0' && $row['backg'] == 'NULL' && $row['backl'] == 'NULL'){}else{ ?>

        <h2 class="title__group-section">Family</h2>

        <div class="row form__group-row margin-t">

            <?php if(empty($row['num_mem']) || $row['num_mem'] == '0'){}else{ ?>

                <div class="form__group-nmembers form__group-hinf2 col-md-4">
                    
                    <label for="num_mem" class="label__name label__family" >Number Members</label>
                    <label for="num_mem" class="l_icon" title="Number Members"> <i class="icon icon_family"></i> </label>
                    <p class="form__group-input nmembers" id="num_mem"> <?php echo $row['num_mem']; ?> </p>

                </div>

            <?php } if(empty($row['backg']) || $row['backg'] == 'NULL'){}else{ ?>

                <div class="form__group-background form__group-hinf2 col-md-4">
                    
                    <label for="backg" class="label__name label__family" >Background</label>
                    <label for="backg" class="l_icon" title="Background"> <i class="icon icon_bcheck"></i> </label>
                    <p class="form__group-input background" id="backg"> <?php echo $row['backg']; ?> </p>

                </div>

            <?php } if(empty($row['backl']) || $row['backl'] == 'NULL'){}else{ ?>

                <div class="form__group-background_l form__group-hinf2 col-md-4">

                    <label for="backl" class="label__name label__family" >Background Language</label>
                    <label for="backl" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>
                    <p class="form__group-input background_l" id="backg"> <?php echo $row['backl']; ?> </p>

                </div>

            <?php } ?>

        </div>

    <?php } ?>

    
    <div class="form__group-row2">

        <?php if($row3['f_name1'] == "NULL" AND $row3['f_lname1'] == "NULL" AND $row3['db1'] == "NULL" AND $row3['gender1'] == "NULL" AND $row3['db_lawf1'] == "NULL" AND $row3['lawf1'] == "NULL" AND $row3['re1'] == "NULL"){}else{ ?>

            <div id="family1" class="panel-collapse">

                <h2 class="title__group-section">Member 1</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name1']) || $row3['f_name1'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name1" class="label__name label__family" >Name</label>
                            <label for="f_name1" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name1" id="f_name1" ><?php echo $row3['f_name1'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname1']) || $row3['f_lname1'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname1" class="label__name label__family" >Last Name</label>
                            <label for="f_lname1" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname1" id="f_lname1" ><?php echo $row3['f_lname1'] ?></p>

                        </div>


                    <?php } if(empty($row3['db1']) || $row3['db1'] == 'NULL'){}else{ ?>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db1" class="label__name label__family" >Date of Birth</label>
                            <label for="db1" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db1"><?php echo $row3['db1'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender1']) || $row3['gender1'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                            <label for="gender1" class="label__name label__family" >Gender</label>
                            <label for="gender1" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender1" id="gender1"><?php echo $row3['gender1'] ?></p>

                        </div>

                    <?php } if(empty($row3['re1']) || $row3['re1'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="relation1" class="label__name label__family" >Relation</label>
                            <label for="relation1" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation1" id="relation1"><?php echo $row3['re1'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf1']) || $row3['db_lawf1'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf1" class="label__name label__family" >Relation</label>
                            <label for="db_lawf1" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation1" id="db_lawf1"><?php echo $row3['db_lawf1'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf1']) || $row3['lawf1'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf1" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf1']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br><br>
        
        <?php } if($row3['f_name2'] == "NULL" AND $row3['f_lname2'] == "NULL" AND $row3['db2'] == "NULL" AND $row3['gender2'] == "NULL" AND $row3['db_lawf2'] == "NULL" AND $row3['lawf2'] == "NULL" AND $row3['re2'] == "NULL"){}else{ ?>

            <div id="family2" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 2</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name2']) || $row3['f_name2'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name2" class="label__name label__family" >Name</label>
                            <label for="f_name2" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name2" id="f_name2" ><?php echo $row3['f_name2'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname2']) || $row3['f_lname2'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname2" class="label__name label__family" >Last Name</label>
                            <label for="f_lname2" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname2" id="f_lname2" ><?php echo $row3['f_lname2'] ?></p>

                        </div>


                    <?php } if(empty($row3['db2']) || $row3['db2'] == 'NULL'){}else{ ?>

                        <div class="form__group-db2 form__group-hinf3 col-lg-4">

                            <label for="db2" class="label__name label__family" >Date of Birth</label>
                            <label for="db2" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db2"><?php echo $row3['db2'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender2']) || $row3['gender2'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender2 form__group-hinf3 col-lg-4">

                            <label for="gender2" class="label__name label__family" >Gender</label>
                            <label for="gender2" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender2" id="gender2"><?php echo $row3['gender2'] ?></p>

                        </div>

                    <?php } if(empty($row3['re2']) || $row3['re2'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation2 form__group-hinf3 col-lg-4">

                            <label for="relation2" class="label__name label__family" >Relation</label>
                            <label for="relation2" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation2" id="relation2"><?php echo $row3['re2'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf2']) || $row3['db_lawf2'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law2 form__group-hinf3 col-lg-4">

                            <label for="db_lawf2" class="label__name label__family" >Relation</label>
                            <label for="db_lawf2" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation2" id="db_lawf2"><?php echo $row3['db_lawf2'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf2']) || $row3['lawf2'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law2 form__group-hinf3 col-lg-4">

                            <label for="db_lawf2" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf2']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br>
        
        <?php } if($row3['f_name3'] == "NULL" AND $row3['f_lname3'] == "NULL" AND $row3['db3'] == "NULL" AND $row3['gender3'] == "NULL" AND $row3['db_lawf3'] == "NULL" AND $row3['lawf3'] == "NULL" AND $row3['re3'] == "NULL"){}else{ ?>

            <div id="family3" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 3</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name3']) || $row3['f_name3'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name3" class="label__name label__family" >Name</label>
                            <label for="f_name3" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name3" id="f_name3" ><?php echo $row3['f_name3'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname3']) || $row3['f_lname3'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname3" class="label__name label__family" >Last Name</label>
                            <label for="f_lname3" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname3" id="f_lname3" ><?php echo $row3['f_lname3'] ?></p>

                        </div>


                    <?php } if(empty($row3['db3']) || $row3['db3'] == 'NULL'){}else{ ?>

                        <div class="form__group-db3 form__group-hinf3 col-lg-4">

                            <label for="db3" class="label__name label__family" >Date of Birth</label>
                            <label for="db3" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db3"><?php echo $row3['db3'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender3']) || $row3['gender3'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender3 form__group-hinf3 col-lg-4">

                            <label for="gender3" class="label__name label__family" >Gender</label>
                            <label for="gender3" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender3" id="gender3"><?php echo $row3['gender3'] ?></p>

                        </div>

                    <?php } if(empty($row3['re3']) || $row3['re3'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation3 form__group-hinf3 col-lg-4">

                            <label for="relation3" class="label__name label__family" >Relation</label>
                            <label for="relation3" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation3" id="relation3"><?php echo $row3['re3'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf3']) || $row3['db_lawf3'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law3 form__group-hinf3 col-lg-4">

                            <label for="db_lawf3" class="label__name label__family" >Relation</label>
                            <label for="db_lawf3" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation3" id="db_lawf3"><?php echo $row3['db_lawf3'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf3']) || $row3['lawf3'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law3 form__group-hinf3 col-lg-4">

                            <label for="db_lawf3" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf3']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br>
        
        <?php } if($row3['f_name4'] == "NULL" AND $row3['f_lname4'] == "NULL" AND $row3['db4'] == "NULL" AND $row3['gender4'] == "NULL" AND $row3['db_lawf4'] == "NULL" AND $row3['lawf4'] == "NULL" AND $row3['re4'] == "NULL"){}else{ ?>

            <div id="family4" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 4</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name4']) || $row3['f_name4'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name4" class="label__name label__family" >Name</label>
                            <label for="f_name4" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name4" id="f_name4" ><?php echo $row3['f_name4'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname4']) || $row3['f_lname4'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname4" class="label__name label__family" >Last Name</label>
                            <label for="f_lname4" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname4" id="f_lname4" ><?php echo $row3['f_lname4'] ?></p>

                        </div>


                    <?php } if(empty($row3['db4']) || $row3['db4'] == 'NULL'){}else{ ?>

                        <div class="form__group-db4 form__group-hinf3 col-lg-4">

                            <label for="db4" class="label__name label__family" >Date of Birth</label>
                            <label for="db4" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db4"><?php echo $row3['db4'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender4']) || $row3['gender4'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender4 form__group-hinf3 col-lg-4">

                            <label for="gender4" class="label__name label__family" >Gender</label>
                            <label for="gender4" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender4" id="gender4"><?php echo $row3['gender4'] ?></p>

                        </div>

                    <?php } if(empty($row3['re4']) || $row3['re4'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation4 form__group-hinf3 col-lg-4">

                            <label for="relation4" class="label__name label__family" >Relation</label>
                            <label for="relation4" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation4" id="relation4"><?php echo $row3['re4'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf4']) || $row3['db_lawf4'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law4 form__group-hinf3 col-lg-4">

                            <label for="db_lawf4" class="label__name label__family" >Relation</label>
                            <label for="db_lawf4" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation4" id="db_lawf4"><?php echo $row3['db_lawf4'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf4']) || $row3['lawf4'] == 'NULL'){}else{ ?>

                        <div class="form__group-4 form__group-hinf3 col-lg-4">

                            <label for="db_lawf4" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf4']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br>
        
        <?php } if($row3['f_name5'] == "NULL" AND $row3['f_lname5'] == "NULL" AND $row3['db5'] == "NULL" AND $row3['gender5'] == "NULL" AND $row3['db_lawf5'] == "NULL" AND $row3['lawf5'] == "NULL" AND $row3['re5'] == "NULL"){}else{ ?>

            <div id="family5" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 5</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name5']) || $row3['f_name5'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name5" class="label__name label__family" >Name</label>
                            <label for="f_name5" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name5" id="f_name5" ><?php echo $row3['f_name5'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname5']) || $row3['f_lname5'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname5" class="label__name label__family" >Last Name</label>
                            <label for="f_lname5" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname5" id="f_lname5" ><?php echo $row3['f_lname5'] ?></p>

                        </div>


                    <?php } if(empty($row3['db5']) || $row3['db5'] == 'NULL'){}else{ ?>

                        <div class="form__group-db5 form__group-hinf3 col-lg-4">

                            <label for="db5" class="label__name label__family" >Date of Birth</label>
                            <label for="db5" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db5"><?php echo $row3['db5'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender5']) || $row3['gender5'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender5 form__group-hinf3 col-lg-4">

                            <label for="gender5" class="label__name label__family" >Gender</label>
                            <label for="gender5" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender5" id="gender5"><?php echo $row3['gender5'] ?></p>

                        </div>

                    <?php } if(empty($row3['re5']) || $row3['re5'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation5 form__group-hinf3 col-lg-4">

                            <label for="relation5" class="label__name label__family" >Relation</label>
                            <label for="relation5" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation5" id="relation5"><?php echo $row3['re5'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf5']) || $row3['db_lawf5'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law5 form__group-hinf3 col-lg-4">

                            <label for="db_lawf5" class="label__name label__family" >Relation</label>
                            <label for="db_lawf5" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation5" id="db_lawf5"><?php echo $row3['db_lawf5'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf5']) || $row3['lawf5'] == 'NULL'){}else{ ?>

                        <div class="form__group-5 form__group-hinf3 col-lg-4">

                            <label for="db_lawf5" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf5']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br>
        
        <?php } if($row3['f_name6'] == "NULL" AND $row3['f_lname6'] == "NULL" AND $row3['db6'] == "NULL" AND $row3['gender6'] == "NULL" AND $row3['db_lawf6'] == "NULL" AND $row3['lawf6'] == "NULL" AND $row3['re6'] == "NULL"){}else{ ?>

            <div id="family6" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 6</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name6']) || $row3['f_name6'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name6" class="label__name label__family" >Name</label>
                            <label for="f_name6" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name6" id="f_name6" ><?php echo $row3['f_name6'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname6']) || $row3['f_lname6'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname6" class="label__name label__family" >Last Name</label>
                            <label for="f_lname6" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname6" id="f_lname6" ><?php echo $row3['f_lname6'] ?></p>

                        </div>


                    <?php } if(empty($row3['db6']) || $row3['db6'] == 'NULL'){}else{ ?>

                        <div class="form__group-db6 form__group-hinf3 col-lg-4">

                            <label for="db6" class="label__name label__family" >Date of Birth</label>
                            <label for="db6" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db6"><?php echo $row3['db6'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender6']) || $row3['gender6'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender6 form__group-hinf3 col-lg-4">

                            <label for="gender6" class="label__name label__family" >Gender</label>
                            <label for="gender6" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender6" id="gender6"><?php echo $row3['gender6'] ?></p>

                        </div>

                    <?php } if(empty($row3['re6']) || $row3['re6'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation6 form__group-hinf3 col-lg-4">

                            <label for="relation6" class="label__name label__family" >Relation</label>
                            <label for="relation6" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation6" id="relation6"><?php echo $row3['re6'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf6']) || $row3['db_lawf6'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law6 form__group-hinf3 col-lg-4">

                            <label for="db_lawf6" class="label__name label__family" >Relation</label>
                            <label for="db_lawf6" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation6" id="db_lawf6"><?php echo $row3['db_lawf6'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf6']) || $row3['lawf6'] == 'NULL'){}else{ ?>

                        <div class="form__group-6 form__group-hinf3 col-lg-4">

                            <label for="db_lawf6" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf6']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div><br>
        
        <?php } if($row3['f_name7'] == "NULL" AND $row3['f_lname7'] == "NULL" AND $row3['db7'] == "NULL" AND $row3['gender7'] == "NULL" AND $row3['db_lawf7'] == "NULL" AND $row3['lawf7'] == "NULL" AND $row3['re7'] == "NULL"){}else{ ?>

            <div id="family7" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 7</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name7']) || $row3['f_name7'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name7" class="label__name label__family" >Name</label>
                            <label for="f_name7" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name7" id="f_name7" ><?php echo $row3['f_name7'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname7']) || $row3['f_lname7'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname7" class="label__name label__family" >Last Name</label>
                            <label for="f_lname7" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname7" id="f_lname7" ><?php echo $row3['f_lname7'] ?></p>

                        </div>


                    <?php } if(empty($row3['db7']) || $row3['db7'] == 'NULL'){}else{ ?>

                        <div class="form__group-db7 form__group-hinf3 col-lg-4">

                            <label for="db7" class="label__name label__family" >Date of Birth</label>
                            <label for="db7" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db7"><?php echo $row3['db7'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender7']) || $row3['gender7'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender7 form__group-hinf3 col-lg-4">

                            <label for="gender7" class="label__name label__family" >Gender</label>
                            <label for="gender7" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender7" id="gender7"><?php echo $row3['gender7'] ?></p>

                        </div>

                    <?php } if(empty($row3['re7']) || $row3['re7'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation7 form__group-hinf3 col-lg-4">

                            <label for="relation7" class="label__name label__family" >Relation</label>
                            <label for="relation7" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation7" id="relation7"><?php echo $row3['re7'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf7']) || $row3['db_lawf7'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law7 form__group-hinf3 col-lg-4">

                            <label for="db_lawf7" class="label__name label__family" >Relation</label>
                            <label for="db_lawf7" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation7" id="db_lawf7"><?php echo $row3['db_lawf7'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf7']) || $row3['lawf7'] == 'NULL'){}else{ ?>

                        <div class="form__group-7 form__group-hinf3 col-lg-4">

                            <label for="db_lawf7" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf7']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div>
        
        <?php } if($row3['f_name8'] == "NULL" AND $row3['f_lname8'] == "NULL" AND $row3['db8'] == "NULL" AND $row3['gender8'] == "NULL" AND $row3['db_lawf8'] == "NULL" AND $row3['lawf8'] == "NULL" AND $row3['re8'] == "NULL"){}else{ ?>

            <div id="family8" class="panel-collapse mt-4">

                <h2 class="title__group-section">Member 8</h2>

                <div class="row family__members justify-content-center">

                    <?php if(empty($row3['f_name8']) || $row3['f_name8'] == 'NULL'){}else{ ?>

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name8" class="label__name label__family" >Name</label>
                            <label for="f_name8" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input name8" id="f_name8" ><?php echo $row3['f_name8'] ?></p>

                        </div>

                    <?php } if(empty($row3['f_lname8']) || $row3['f_lname8'] == 'NULL'){}else{ ?>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname8" class="label__name label__family" >Last Name</label>
                            <label for="f_lname8" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <p class="form__group-input lname8" id="f_lname8" ><?php echo $row3['f_lname8'] ?></p>

                        </div>


                    <?php } if(empty($row3['db8']) || $row3['db8'] == 'NULL'){}else{ ?>

                        <div class="form__group-db8 form__group-hinf3 col-lg-4">

                            <label for="db8" class="label__name label__family" >Date of Birth</label>
                            <label for="db8" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input dbirth1" id="db8"><?php echo $row3['db8'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['gender8']) || $row3['gender8'] == 'NULL'){}else{ ?>

                        <div class="form__group-gender8 form__group-hinf3 col-lg-4">

                            <label for="gender8" class="label__name label__family" >Gender</label>
                            <label for="gender8" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <p class="form__group-input gender8" id="gender8"><?php echo $row3['gender8'] ?></p>

                        </div>

                    <?php } if(empty($row3['re8']) || $row3['re8'] == 'NULL'){}else{ ?>

                        <div class="form__group-relation8 form__group-hinf3 col-lg-4">

                            <label for="relation8" class="label__name label__family" >Relation</label>
                            <label for="relation8" class="l_icon" title="Gender"> <i class="icon icon_family"></i> </label>
                            <p class="form__group-input relation8" id="relation8"><?php echo $row3['re8'] ?></p>

                        </div>
                    
                    <?php } if(empty($row3['db_lawf8']) || $row3['db_lawf8'] == 'NULL'){}else{ ?>

                        <div class="form__group-db_law8 form__group-hinf3 col-lg-4">

                            <label for="db_lawf8" class="label__name label__family" >Relation</label>
                            <label for="db_lawf8" class="l_icon" title="Gender"> <i class="icon icon_db"></i> </label>
                            <p class="form__group-input relation8" id="db_lawf8"><?php echo $row3['db_lawf8'] ?></p>

                        </div>

                    <?php } if(empty($row3['lawf8']) || $row3['lawf8'] == 'NULL'){}else{ ?>

                        <div class="form__group-8 form__group-hinf3 col-lg-4">

                            <label for="db_lawf8" class="l_icon" title="Gender"> <i class="icon icon_bcheck"></i> </label>
                            <iframe class="embed-responsive-item" src="../'<?php echo $row3['lawf8']?>'"></iframe>

                        </div>

                    <?php } ?>

                </div>

            </div>
        
        <?php } ?>


    </div>
    

</div>
</main>
</form>

<footer id="ts-footer">

<?php include '../master/footer.php' ?>
</footer>

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="../manager/assets/js/rooms.js"></script>
<script src="../manager/assets/js/galery-homestay.js"></script>
<script src="../manager/assets/js/rooms_register.js"></script>

</body>
</html>
