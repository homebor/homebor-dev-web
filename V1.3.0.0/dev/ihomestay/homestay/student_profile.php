<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM academy");
$row6=$query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario'");
$row7=$query7->fetch_assoc();

$sql = "SELECT * FROM `pe_student` WHERE id_m = '$row7[id_m]'"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus);

 if ($row5['usert'] != 'Manager') {
         header("location: ../logout.php");   
    }

if ($row7['plan'] == 'No Plan') {
         header("location: ../manager/work_with_us.php");   
    }

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_profile.css">


    
    <!-- REFERENCE OF MAPBOX API -->
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
        <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
    
        <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js"></script>
        <link
        rel="stylesheet"
        href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
        type="text/css"
        />
        <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
        <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Register</title>



    <script src="../assets/js/jquery-3.3.1.min.js"></script>
    

</head>
<body>

<header id="ts-header" class="fixed-top" style="background: white;">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
       <?php include 'header.php' ?>

    </header>
    <!--end Header-->


<div class="container_register">

    <main class="card__register">
        <form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">

            <div class="row m-0 p-0">

                <div class="col-md-4 column-1" id="div__flex">

                    <div class="card column1">

                        <h3 class="form__group-title__col-4">Basic Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">
                                <img class="form__group__photo" id="preview" src="" alt="">

                                <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">


                                <label for="file" class="photo-add" id="label-i"> <p class="form__group-l_title"> Add Profile Photo </p> </label>

                                <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;" title="Change Frontage Photo"></label>
                            </div>

                            <div class="info mt-5"></div>

                            

                                <!-- Group Name -->

                                <div class="form__group form__group__name" id="group__name">
                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">First Name</label>

                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                    
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__l_name" id="group__l_name">
                                    <div class="form__group__icon-input">

                                        <label for="l_name" class="form__label">Last Name</label>

                                        <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="e.g. Smith">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                                
                            <!-- Group Mail -->

                            <div class="form__group form__group__mail_s" id="group__mail_s">
                                <div class="form__group__icon-input">

                                    <label for="mail_s" class="form__label">Student Mail</label>

                                    <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>
                                    <input type="text" id="mail_s" name="mail_s" class="form__group-input mail" placeholder="e.g. jsmith@gmail.com">

                                </div>
                                <p class="form__group__input-error">It is not a valid Email</p>
                            </div>
                            
                            <!-- Group Password -->

                            <div class="form__group form__group__password" id="group__password">
                                <div class="form__group__icon-input">

                                    <label for="password" class="form__label">Password</label>

                                    <label for="password" class="icon"><i class="icon icon__password"></i></label>
                                    <input type="password" id="password" name="password" class="form__group-input password" placeholder="min 8 Characters">
                                    

                                </div>
                                <p class="form__group__input-error">Numbers, Uppercase, Lowercase, and Special Characters</p>
                            </div>

                            <!-- Arrive Date -->
                            <div class="form__group form__group-arrive_g">

                                <div class="form__group__icon-input">

                                    <label for="arrive_g" class="form__label">Arrive Date</label>
                                    <label for="arrive_g" class="icon"><i class="icon icon__date"></i></label>
                                    <input type="date" id="arrive_g" name="arrive_g" class="form__group-input h_date" placeholder="Arrival Date at the Homestay">

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>
                        
                        </div>

                    </div>


                    <div class="card column2">


                        <h3 class="form__group-title__col-4">Personal Student Address</h3>

                        <div class="information__content">

                            <!-- Group Origin City -->
                            <div class="form__group form__group__basic form__group__country" id="group__country">
                                <div class="form__group__icon-input">

                                    <label for="country" class="form__label">Country</label>

                                    <label for="country" class="icon"><i class="icon icon__dir"></i></label>
                                    <input type="text" id="country" name="country" class="form__group-input country" placeholder="e.g. Mexico">

                                </div>
                                <p class="form__group__input-error">Enter a valid Date</p>
                            </div>


                            <!-- Group Address -->
                            <div class="form__group form__group__dir" id="group__dir">
                                    <div class="form__group__icon-input">

                                        <label for="dir" class="form__label">Address </label>

                                        <label for="dir" class="icon"><i class="icon icon__dir"></i></label>
                                            
                                        <input type="text" id="dir" name="dir" class="form__group-input dir" placeholder="Av, Street, etc.">
                                            
                                    </div>

                                    <p class="form__group__input-error">Please enter a valid Address</p>
                                </div>
                                    
                                <!-- Group City -->
                                <div class="form__group form__group__city" id="group__city">
                                    <div class="form__group__icon-input">

                                        <label for="city" class="form__label">City </label>

                                        <label for="city" class="icon"><i class="icon icon__dir"></i></label>
                                            
                                        <input type="text" id="city" name="city" class="form__group-input city2" placeholder="e.g. Monterrey">

                                    </div>

                                    <p class="form__group__input-error">Please enter a valid City</p>
                                </div>
                                    
                                <!-- Group State -->
                                <div class="form__group form__group__state" id="group__state">
                                    <div class="form__group__icon-input">

                                        <label for="state" class="form__label"> Province / State </label>

                                        <label for="state" class="icon"><i class="icon icon__dir"></i></label>

                                        <input type="text" id="state" name="state" class="form__group-input state" placeholder="e.g. Yucatán">
                                            
                                    </div>

                                    <p class="form__group__input-error">Please enter a valid State</p>
                                </div>
                                    
                                <!-- Group Postal Code -->
                                <div class="form__group form__group__p_code" id="group__p_code">
                                    <div class="form__group__icon-input">

                                        <label for="p_code" class="form__label">Postal Code </label>

                                        <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>

                                        <input type="text" id="p_code" name="p_code" class="form__group-input p_code" placeholder="e.g. 145187">

                                    </div>

                                    <p class="form__group__input-error">Please enter a valid Postal Code</p>
                                </div>

                        </div>

                    </div>

                    <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
    
                <script>
                mapboxgl.accessToken = 'pk.eyJ1Ijoiam9zZXhlb24iLCJhIjoiY2szaXpqdXBpMDB1bTNpczR1cGt0ajgzYSJ9.Dd0-oK7-s-VU2RZxac1VRg';
                var map = new mapboxgl.Map({

                    container: 'map', // container id
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [-79.347015, 43.651070], // starting position
                    zoom: 11 // starting zoom
                });
                
                // Add geolocate control to the map.
                map.addControl(
                new mapboxgl.GeolocateControl({
                    positionOptions: {
                        enableHighAccuracy: true
                    },
                    trackUserLocation: true
                    })
                );

                

                </script>

                    <div class="card column3">


                        <h3 class="form__group-title__col-4">Accommodation Request</h3>

                        <div class="information__content">

                            <!-- Group Start Date of Stay -->
                            <div class="form__group form__group__firstd" id="group__firstd">
                                <div class="form__group__icon-input">
                                        
                                    <label for="firstd" class="form__label">Start Date of Stay</label>
                                    <label for="firstd" class="icon"><i class="icon icon__date"></i></label>
                                    <input type="date" id="firstd" name="firstd" class="form__group-input firstd" placeholder="Start Date of Stay">

                                </div>
                                <p class="form__group__input-error">Enter a valid Date</p>
                            </div>

                            <!-- Group End Date of stay -->
                            <div class="form__group form__group__lastd" id="group__lastd">
                                <div class="form__group__icon-input">
                                        
                                    <label for="lastd" class="form__label">End Date of Stay</label>
                                    <label for="lastd" class="icon"><i class="icon icon__date"></i></label>
                                    <input type="date" id="lastd" name="lastd" class="form__group-input lastd" placeholder="End Date of stay">
                                </div>
                                <p class="form__group__input-error">Enter a valid Date</p>
                            </div>

                            <input type="date" id="firstd2" hidden>
                            <input type="date" id="lastd2" hidden>

                            <p id="calcWeeks" class="calc__weeks"></p>

                        </div>

                    </div>
                    
                    <div class="card column4">


                        <h3 class="form__group-title__col-4">Flight Information</h3>

                        <div class="information__content">
                        
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_airline" id="group__n_airline">
                                <div class="form__group__icon-input">

                                    <label for="n_airline" class="form__label">Booking Confirmation</label>

                                    <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>
                                    <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline" placeholder="e.g. Air Canada (Canadá) AC">
                                    <p class="form__group__input-error">Minimum 4 Characters</p>

                                </div>
                            </div>
                            
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_flight" id="group__n_flight">
                                <div class="form__group__icon-input">

                                    <label for="n_flight" class="form__label">Landing Flight Number</label>

                                    <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>
                                    <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight" placeholder="e.g. B-9820">

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>


                                <div class="form__group form__group-f_date ">
                                    <div class="form__group__icon-input">

                                        <label for="f_date" class="form__label">Flight Date</label>
                                        <label for="f_date" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="f_date" name="f_date" class="form__group-input f_date" placeholder="Flight Date">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <div class="form__group form__group-h_date">

                                    <div class="form__group__icon-input">

                                        <label for="h_date" class="form__label">Arrival at the Homestay</label>
                                        <label for="h_date" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="h_date" name="h_date" class="form__group-input h_date" placeholder="Arrival Date at the Homestay">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>

                            <!-- Group Destination City  -->
                            <div class="form__group form__group__basic form__group__destination_c" id="group__destination_c">

                                <div class="form__group__icon-input">

                                    <label for="destination_c" class="form__label smoker"> Destination City </label>

                                    <label for="destination_c" class="icon"><i class="icon icon__city"></i></label>

                                    <select class="custom-select form__group-input smoke_s" id="destination_c" name="destination_c" onchange="otherCity()">
                                        <option hidden="option" selected disabled>Select Option</option>
                                        <option value="Toronto">Toronto</option>
                                        <option value="Montreal">Montreal</option>
                                        <option value="Ottawa">Ottawa</option>
                                        <option value="Quebec">Quebec</option>
                                        <option value="Calgary">Calgary</option>
                                        <option value="Vancouver">Vancouver</option>
                                        <option value="Victoria">Victoria</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <p class="form__group__input-error">Enter a valid Date</p>

                                </div>

                                <div class="form__group form__group__basic form__group__other_c d-none" id="group__other_c">



                                </div>


                                <script>
                                const $city = document.querySelector("#destination_c");

                                function otherCity() {
                                const citiI = $city.selectedIndex;
                                if(citiI === -1) return; // Esto es cuando no hay elementos
                                const optionCity = $city.options[citiI];

                                if(optionCity.value == 'Other'){

                                    document.getElementById('group__other_c').classList.remove('d-none');

                                    $('#group__other_c').html(`
                                        <label class="label__other__city">Specify the Other City</label>
                                        <input class="input__other_city" type="text" name="destination_c">`    
                                    );

                                }else{
                                    document.getElementById('group__other_c').classList.add('d-none');
                                }

                                };

                                </script>

                        </div>

                    </div>



                    <div class="card column5">

                        <h3 class="form__group-title__col-4">Emergency Contact</h3>

                        <div class="information__content">


                                <!-- Group Emergency Name -->
                                <div class="form__group form__group__cont_name" id="group__cont_name">
                                    <div class="form__group__icon-input">

                                        <label for="cont_name" class="form__label">Contact Name</label>

                                        <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name" placeholder="e.g. Joy">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Emergency Last Name -->
                                <div class="form__group form__group__cont_lname" id="group__cont_lname">

                                    <div class="form__group__icon-input">

                                        <label for="cont_lname" class="form__label">Contact Last Name</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname" placeholder="e.g. Smith">

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                                <!-- Group Emergency phone -->
                                <div class="form__group form__group__num_conts" id="group__num_conts">

                                    <div class="form__group__icon-input">

                                        <label for="num_conts" class="form__label">Emergency Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>
                                        <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts" placeholder="Emergency Number">
                                    </div>
                                        <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Alternative Phone -->
                                <div class="form__group form__group__cell_s" id="group__cell_s">
                                    <div class="form__group__icon-input">

                                        <label for="cell_s" class="form__label">Alternative Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>
                                        <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s" placeholder="Alternative Number">
                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                        </div>

                    </div>

                </div>


                <input type="text" class="form-control" id="id_m" name="id_m" value="<?php echo $row7['id_m'];?>" hidden>

                <input type="text" class="form-control" id="plan" name="plan" value="<?php echo $row7['plan'];?>" hidden>

                <input type="text" class="form-control" id="agency" name="agency" value="<?php echo $row7['a_name'];?>"hidden>

                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $numberOfRows;?>" hidden>







                <div class="col-md-8 column-2" id="div__flex">

                    <div class="card column6">

                        <h3 class="form__group-title__col-4">Personal Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Date of Birth -->
                                <div class="form__group col-lg-4 form__group__db" id="group__db">
                                    <div class="form__group__icon-input">

                                        <label for="db" class="form__label">Date of Birth</label>
                                        <label for="db" class="icon"><i class="icon icon__date"></i></label>
                                        <input type="date" id="db" name="db" class="form__group-input db" placeholder="Date of Birth">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Gender -->
                                <div class="form__group col-lg-4 form__group__basic form__group__gender" id="group__gender">

                                    <div class="form__group__icon-input">

                                        <label for="gender" class="form__label">Gender</label>

                                        <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                                        <select class="custom-select form__group-input gender" id="gender" name="gender">
                                            <option hidden="option" selected disabled> Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Any">Any</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a Gender</p>
                                </div>

                                <!-- Group Number -->

                                <div class="form__group col-lg-4 form__group__num_s" id="group__num_s">

                                    <div class="form__group__icon-input">

                                        <label for="num_s" class="form__label">Phone Number</label>

                                        <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>
                                        <input type="num" id="num_s" name="num_s" class="form__group-input num_s" placeholder="Personal Number">

                                    </div>
                                    <p class="form__group__input-error">Enter a Valid Number</p>
                                </div>
                                
                                <!-- Group Origin City -->
                                <div class="form__group col-lg-4 form__group__basic form__group__nationality" id="group__nationality">
                                    <div class="form__group__icon-input">

                                        <label for="nationality" class="form__label">Background</label>

                                        <label for="nationality" class="icon"><i class="icon icon__city"></i></label>
                                        <input type="text" id="nationality" name="nationality" class="form__group-input country" placeholder="e.g. Mexican">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Origin Language -->
                                <div class="form__group col-lg-4 form__group__basic form__group__lang_s" id="group__lang_s">
                                    <div class="form__group__icon-input">

                                        <label for="lang_s" class="form__label">Origin Language</label>

                                        <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s" placeholder="e.g. English">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Another Language -->
                                <div class="form__group col-lg-4 form__group__basic form__group__language_a" id="group__language_a">
                                    <div class="form__group__icon-input">

                                        <label for="language_a" class="form__label">Another Language</label>

                                        <label for="language_a" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <input type="text" id="language_a" name="language_a" class="form__group-input nacionality" placeholder="e.g. Spanish">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Passport -->
                                <div class="form__group col-lg-4 form__group__basic form__group__pass" id="group__pass">
                                    <div class="form__group__icon-input">

                                        <label for="pass" class="form__label visa">Passport</label>

                                        <label for="pass" class="icon"><i class="icon icon__pass"></i></label>
                                        <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Passport -->
                                <div class="form__group col-lg-4 form__group__basic form__group__exp_pass" id="group__exp_pass">
                                    <div class="form__group__icon-input">

                                        <label for="exp_pass" class="form__label visa">Passport Expiration Date</label>

                                        <label for="exp_pass" class="icon"><i class="icon icon__bl"></i></label>
                                        <input type="date" id="exp_pass" name="exp_pass" class="form__group-input pass" placeholder="Passport">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Visa Expiration Date -->
                                <div class="form__group col-lg-4 form__group__basic form__group__bl" id="group__bl">
                                    <div class="form__group__icon-input">

                                        <label for="bl" class="form__label visa">Visa Expiration Date</label>
                                        <label for="bl" class="icon"><i class="icon icon__bl"></i></label>
                                        <input type="date" id="bl" name="bl" class="form__group-input bl" placeholder="Visa Expiration Date">
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <div class="form__group col-lg-6 form__group__basic form__group__visa" id="group__visa">
                                    <div class="form__group__icon-input">

                                        <label for="visa" class="form__label visa">Visa</label>
                                        <label for="visa" class="icon"><i class="icon icon__visa"></i></label>
                                        <input type="file" id="visa" name="visa" class="form__group-input visa" placeholder="Visa" maxlength="1" accept="pdf">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>


                    <div class="card column7">

                        <h3 class="form__group-title__col-4">Health Information</h3>

                        <!-- <h3 class="subtitle__section-card">Health Information</h3> -->

                        <div class="information__content information__content-col-8 pb-2 pt-4">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Smoker -->
                                <div class="form__group col-lg-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                                    <div class="form__group__icon-input">

                                        <label for="smoke_s" class="form__label smoker">Do you smoke?</label>

                                        <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="smoke_s" name="smoke_s">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                                <!-- Group Alcoholic Beverages -->
                                <div class="form__group col-lg-4 form__group__basic form__group__drinks_alc" id="group__drinks_alc">

                                    <div class="form__group__icon-input">

                                        <label for="drinks_alc" class="form__label smoker">You Drink Alcohol?</label>

                                        <label for="drinks_alc" class="icon"><i class="icon icon__drinks_alc"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="drinks_alc" name="drinks_alc">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                                <!-- Group Drugs -->
                                <div class="form__group col-lg-4 form__group__basic form__group__drugs" id="group__drugs">

                                    <div class="form__group__icon-input">

                                        <label for="drugs" class="form__label smoker">Have you used any drugs?</label>

                                        <label for="drugs" class="icon"><i class="icon icon__drugs"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="drugs" name="drugs">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>

                                <!-- Group Allergy to Animals -->
                                <div class="form__group col-lg-5 form__group__basic form__group__allergy_a" id="group__allergy_a">

                                    <div class="form__group__icon-input">

                                        <label for="allergy_a" class="form__label pets_l">Allergy to Animals</label>

                                        <label for="allergy_a" class="icon"><i class="icon icon__pets"></i></label>

                                        <select class="custom-select form__group-input pets" id="allergy_a" name="allergy_a" onchange="allergyAnimals()">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>


                                    <div class="form__group__icon-input d-none" id="specify_allergy_a">

                                    </div>


                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                
                                
                                <!-- Group Dietary Restrictions -->
                                <div class="form__group col-lg-5 form__group__basic form__group__allergy_m" id="group__allergy_m">

                                    <div class="form__group__icon-input">

                                        <label for="allergy_m" class="form__label pets_l">Dietary Restrictions</label>

                                        <label for="allergy_m" class="icon"><i class="icon icon__food"></i></label>

                                        <select class="custom-select form__group-input pets" id="allergy_m" name="allergy_m" onchange="allergyMeals()">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>


                                    <div class="form__group__icon-input d-none" id="specify_allergy_m">

                                    </div>


                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <script>
                                    const $allergyA = document.querySelector("#allergy_a");

                                    function allergyAnimals() {
                                        const allergyA = $allergyA.selectedIndex;
                                        if(allergyA === -1) return; // Esto es cuando no hay elementos
                                        const optionAallergy = $allergyA.options[allergyA];

                                        if(optionAallergy.value == 'yes'){

                                            document.getElementById('specify_allergy_a').classList.remove('d-none');

                                            $('#specify_allergy_a').html(`
                                                <label class="label__specify2">Specify</label>
                                                <input class="input__specify" type="text" name="allergy_a">`    
                                            );

                                        }else{
                                            document.getElementById('specify_allergy_a').classList.add('d-none');
                                        }

                                    };
                                    
                                    
                                    // Allergy Meals

                                    const $allergyM = document.querySelector("#allergy_m");

                                    function allergyMeals() {
                                        const allergyM = $allergyM.selectedIndex;
                                        if(allergyM === -1) return; // Esto es cuando no hay elementos
                                        const optionMallergy = $allergyM.options[allergyM];

                                        if(optionMallergy.value == 'yes'){

                                            document.getElementById('specify_allergy_m').classList.remove('d-none');

                                            $('#specify_allergy_m').html(`
                                                <label class="label__specify2">Specify</label>
                                                <input class="input__specify" type="text" name="allergy_m">`    
                                            );

                                        }else{
                                            document.getElementById('specify_allergy_m').classList.add('d-none');
                                        }

                                    };

                                </script>
                                
                            
                            </div>

                        </div>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Current Heal Information -->
                                <div class="form__group col-sm-12 form__group__basic form__group__preference form__group__healt_s" id="group__healt_s">

                                    <div class="form__group__icon-input">

                                        <label for="healt_s" class="form__label allergies_l">What is your state of health at the moment?</label>

                                        <label for="healt_s" class="icon"><i class="icon icon__healt_s"></i></label>

                                        <textarea class="form__group-input allergies" id="healt_s" rows="3" name="healt_s" placeholder="Write using few words."></textarea>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Disease -->
                                <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__disease" id="group__disease">

                                        <label for="disease" class="allergies_l">Do you suffer from any disease?</label>

                                        <div class="check__div">
                                            
                                            <div class="check__yes">
                                                <input type="radio" id="yes_disease" class="input_check"> 
                                                <label class="label__check" for="yes">Yes</label>
                                            </div>

                                            <div class="check__no">
                                                <input type="radio" name="disease" id="no_disease" class="input_check" value="no"> 
                                                <label class="label__check" for="no">No</label>
                                            </div>
                                        
                                        </div> 


                                        <div class="textarea__div-desease" id="textarea__div-desease">
                                            <label for="text__disease" class="label__specify">Specify</label>

                                            <textarea name="disease" class="col-sm-11 ml-auto mr-auto form__group-input healt" id="text__disease" rows="3" placeholder="Write using few words."></textarea>
                                        </div>

                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                
                                <!-- Group Treatment -->
                                <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__treatment" id="group__treatment">

                                    <label for="treatment" class="allergies_l">Are you under treatment or taking any medication?</label>

                                    <div class="check__div">
                                        
                                        <div class="check__yes">
                                            <input type="radio" id="yes_treatment" class="input_check"> 
                                            <label class="label__check" for="yes">Yes</label>
                                        </div>

                                        <div class="check__no">
                                            <input type="radio" name="treatment" id="no_treatment" class="input_check" value="no"> 
                                            <label class="label__check" for="no">No</label>
                                        </div>

                                    </div>

                                    <div class="textarea__div-desease" id="textarea__div-treatment">
                                        <label for="text__treatment" class="label__specify">Specify</label>

                                        <textarea name="treatment" class="col-sm-11 ml-auto mr-auto form__group-input healt" id="text__treatment" rows="3" placeholder="Write using few words."></textarea>
                                    </div>

                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                
                                <!-- Group Psychological Treatment -->
                                <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__treatment_p" id="group__treatment_p">

                                    <label for="treatment_p" class="allergies_l text-center">Are you or were you undergoing psychological or psychiatric treatment?</label>

                                    <div class="check__div">
                                        
                                        <div class="check__yes">
                                            <input type="radio" id="yes_treatment_p" class="input_check"> 
                                            <label class="label__check" for="yes">Yes</label>
                                        </div>

                                        <div class="check__no">
                                            <input type="radio" name="treatment_p" id="no_treatment_p" class="input_check" value="no"> 
                                            <label class="label__check" for="no">No</label>
                                        </div>

                                    </div>

                                    <div class="textarea__div-desease" id="textarea__div-treatment_p">
                                        <label for="text__treatment_p" class="label__specify">Specify</label>

                                        <textarea name="treatment_p" class="col-sm-11 ml-auto mr-auto form__group-input healt" id="text__treatment_p" rows="3" placeholder="Write using few words."></textarea>
                                    </div>

                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                
                                <!-- Group Allergies -->
                                <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__allergies" id="group__allergies">

                                    <label for="allergies" class="allergies_l text-center">Do you suffer from any allergies?</label>

                                    <div class="check__div">
                                        
                                        <div class="check__yes">
                                            <input type="radio" id="yes_allergies" class="input_check"> 
                                            <label class="label__check" for="yes">Yes</label>
                                        </div>

                                        <div class="check__no">
                                            <input type="radio" name="allergies" id="no_allergies" class="input_check" value="no"> 
                                            <label class="label__check" for="no">No</label>
                                        </div>

                                    </div>

                                    <div class="textarea__div-desease" id="textarea__div-allergies">
                                        <label for="text__allergies" class="label__specify">Specify</label>

                                        <textarea name="allergies" class="col-sm-11 ml-auto mr-auto form__group-input healt" id="text__allergies" rows="3" placeholder="Write using few words."></textarea>
                                    </div>

                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                
                                <!-- Group Surgery -->
                                <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__surgery mb-0" id="group__surgery">

                                    <label for="surgery" class="allergies_l text-center">Have you had any surgery in the 12 months prior to your trip?</label>

                                    <div class="check__div">
                                        
                                        <div class="check__yes">
                                            <input type="radio" id="yes_surgery" class="input_check"> 
                                            <label class="label__check" for="yes">Yes</label>
                                        </div>

                                        <div class="check__no">
                                            <input type="radio" name="surgery" id="no_surgery" class="input_check" value="no"> 
                                            <label class="label__check" for="no">No</label>
                                        </div>

                                    </div>

                                    <div class="textarea__div-desease" id="textarea__div-surgery">
                                        <label for="text__surgery" class="label__specify">Specify</label>

                                        <textarea name="surgery" class="col-sm-11 ml-auto mr-auto form__group-input healt" id="text__surgery" rows="3" placeholder="Write using few words."></textarea>
                                    </div>

                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <script>

                                    // Disease

                                    var yes_d = document.querySelector('#yes_disease'); 
                                    var no_d = document.querySelector('#no_disease'); 

                                    yes_d.addEventListener("click", () => {

                                        no_d.checked = false;
                                        yes_d.checked = true;

                                        document.getElementById('textarea__div-desease').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_d.addEventListener("click", () => {

                                        no_d.checked = true;
                                        yes_d.checked = false;

                                        document.getElementById('textarea__div-desease').classList.remove('textarea__div-desease-active');

                                    });

                                    // Treatment
                                    
                                    var yes_t = document.querySelector('#yes_treatment'); 
                                    var no_t = document.querySelector('#no_treatment'); 

                                    yes_t.addEventListener("click", () => {

                                        no_t.checked = false;
                                        yes_t.checked = true;

                                        document.getElementById('textarea__div-treatment').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_t.addEventListener("click", () => {

                                        no_t.checked = true;
                                        yes_t.checked = false;

                                        document.getElementById('textarea__div-treatment').classList.remove('textarea__div-desease-active');

                                    });


                                    // Psychological Treatment
                                    
                                    var yes_tp = document.querySelector('#yes_treatment_p'); 
                                    var no_tp = document.querySelector('#no_treatment_p'); 

                                    yes_tp.addEventListener("click", () => {

                                        no_tp.checked = false;
                                        yes_tp.checked = true;

                                        document.getElementById('textarea__div-treatment_p').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_tp.addEventListener("click", () => {

                                        no_tp.checked = true;
                                        yes_tp.checked = false;

                                        document.getElementById('textarea__div-treatment_p').classList.remove('textarea__div-desease-active');

                                    });
                                    
                                    // Psychological Treatment
                                    
                                    var yes_tp = document.querySelector('#yes_treatment_p'); 
                                    var no_tp = document.querySelector('#no_treatment_p'); 

                                    yes_tp.addEventListener("click", () => {

                                        no_tp.checked = false;
                                        yes_tp.checked = true;

                                        document.getElementById('textarea__div-treatment_p').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_tp.addEventListener("click", () => {

                                        no_tp.checked = true;
                                        yes_tp.checked = false;

                                        document.getElementById('textarea__div-treatment_p').classList.remove('textarea__div-desease-active');

                                    });
                                    
                                    // Allergies
                                    
                                    var yes_a = document.querySelector('#yes_allergies'); 
                                    var no_a = document.querySelector('#no_allergies'); 

                                    yes_a.addEventListener("click", () => {

                                        no_a.checked = false;
                                        yes_a.checked = true;

                                        document.getElementById('textarea__div-allergies').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_a.addEventListener("click", () => {

                                        no_a.checked = true;
                                        yes_a.checked = false;

                                        document.getElementById('textarea__div-allergies').classList.remove('textarea__div-desease-active');

                                    });
                                    
                                    // Surgery
                                    
                                    var yes_s = document.querySelector('#yes_surgery'); 
                                    var no_s = document.querySelector('#no_surgery'); 

                                    yes_s.addEventListener("click", () => {

                                        no_s.checked = false;
                                        yes_s.checked = true;

                                        document.getElementById('textarea__div-surgery').classList.add('textarea__div-desease-active');

                                    });
                                    
                                    no_s.addEventListener("click", () => {

                                        no_s.checked = true;
                                        yes_s.checked = false;

                                        document.getElementById('textarea__div-surgery').classList.remove('textarea__div-desease-active');

                                    });

                                </script>
                            </div>

                        </div>
                        
                        

                    </div>


                    <div class="card column8">

                        <h3 class="form__group-title__col-4">Professional Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">


                                <!-- Group Academy -->
                                <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                                    <div class="form__group__icon-input">

                                        <label for="n_a" class="form__label">Academy</label>

                                        <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                                        <select class="custom-select form__group-input n_a" id="n_a" name="n_a" onchange="otherAcademy()">   
                                            <?php    
                                                while ( $row6 = $query6->fetch_array() )    
                                                    {
                                            ?>
                                                    <option hidden="option" selected disabled>Select Academy</option>
                                                    <option value=" <?php echo $row6['id_ac'] ?> " >
                                                        <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                                                    </option>
                                            <?php }  ?>
                                                    <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                    <div class="form__group form__group__basic form__group__other_a d-none" id="group__other_a">



                                    </div>


                                    <script>
                                    const $academy = document.querySelector("#n_a");

                                    function otherAcademy() {
                                    const academyI = $academy.selectedIndex;
                                    if(academyI === -1) return; // Esto es cuando no hay elementos
                                    const optionAcademy = $academy.options[academyI];

                                    if(optionAcademy.value == 'Other'){

                                        document.getElementById('group__other_a').classList.remove('d-none');

                                        $('#group__other_a').html(`

                                        <div class="row mt-3 mb-0">
                                            <div class="form__group col-sm-6 form__group__basic form__group__n_a" id="group__n_a">
                                                <label class="label__other__city">Academy Name</label>
                                                <input class="input__other_city" type="text" name="destination_c">
                                                
                                            </div>   
                                            <div class="form__group col-sm-6 form__group__basic form__group__n_a" id="group__n_a">
                                                <label class="label__other__city">Academy Address</label>
                                                <input class="input__other_city" type="text" name="destination_c">
                                                
                                            </div>
                                        </div>
                                        `    
                                        );

                                    }else{
                                        document.getElementById('group__other_a').classList.add('d-none');
                                    }

                                    };

                                    </script>
                                </div>

                                <!-- Group Background 
                                <div class="form__group col-sm-4 form__group__basic form__group__nacionality" id="group__nacionality">
                                    <div class="form__group__icon-input">

                                        <label for="nacionality" class="form__label">Background</label>

                                        <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <input type="text" id="nacionality" name="nacionality" class="form__group-input nacionality" placeholder="e.g. Canadian">

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div> -->

                                <!-- Group English Level -->
                                <div class="form__group col-lg-4 form__group__basic form__group__english_l" id="group__english_l">

                                    <div class="form__group__icon-input">

                                        <label for="english_l" class="form__label smoker">Current English level</label>

                                        <label for="english_l" class="icon"><i class="icon icon__city"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="english_l" name="english_l">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Beginner">Beginner</option>
                                            <option value="Intermediate">Intermediate</option>
                                            <option value="Advanced">Advanced</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                                <!-- Group Type Student -->
                                <div class="form__group col-lg-4 form__group__basic form__group__type_s" id="group__type_s">
                                    <div class="form__group__icon-input">

                                        <label for="type_s" class="form__label">Type Student</label>

                                        <label for="type_s" class="icon"><i class="icon icon__names"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="type_s" name="type_s" onchange="otherProgram()">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Ingles">Ingles</option>
                                            <option value="High School">High School</option>
                                            <option value="College">College</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Program to Study in Canada -->
                                <div class="form__group col-lg-4 form__group__basic form__group__prog_selec d-none" id="group__prog_selec">
                                    
                                </div>


                                    <script>
                                    const $program = document.querySelector("#type_s");

                                    function otherProgram() {
                                    const programI = $program.selectedIndex;
                                    if(programI === -1) return; // Esto es cuando no hay elementos
                                    const optionProgram = $program.options[programI];

                                    if(optionProgram.value == 'College'){

                                        document.getElementById('group__prog_selec').classList.remove('d-none');

                                        $('#group__prog_selec').html(`

                                        <div class="form__group__icon-input">

                                            <label for="prog_selec" class="form__label">Program to Study in Canada</label>

                                            <label for="prog_selec" class="icon"><i class="icon icon__prog_selec"></i></label>
                                            <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s" placeholder="e.g. College">

                                        </div>
                                        <p class="form__group__input-error">Enter a valid Date</p>
                                        `    
                                        );

                                    }else{
                                        document.getElementById('group__prog_selec').classList.add('d-none');
                                    }

                                    };

                                    </script>
                                
                                


                                <!-- Group Schedule  -->
                                <div class="form__group col-lg-4 form__group__basic form__group__schedule" id="group__schedule">

                                    <div class="form__group__icon-input">

                                        <label for="schedule" class="form__label smoker"> Schedule </label>

                                        <label for="schedule" class="icon"><i class="icon icon__city"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="schedule" name="schedule">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Day">Day</option>
                                            <option value="Evening">Evening</option>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                            
                            </div>

                        </div>

                    </div>





                    <div class="card column9">

                        <h3 class="form__group-title__col-4">House Preferences</h3>

                        <h3 class="subtitle__section-card mb-5">Can you Share With:</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">


                                <!-- Group Living with Smoker -->
                                <div class="form__group col-sm-4 form__group__basic form__group__smoker_l" id="group__smoker_l">

                                    <div class="form__group__icon-input">

                                        <label for="smoker_l" class="form__label pets_l">Smokers?</label>

                                        <label for="smoker_l" class="icon"><i class="icon icon__smoke_s"></i></label>

                                        <select class="custom-select form__group-input pets" id="smoker_l" name="smoker_l">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Can you live with Children? -->
                                <div class="form__group col-sm-4 form__group__basic form__group__children" id="group__children">

                                    <div class="form__group__icon-input">

                                        <label for="children" class="form__label pets_l">Children?</label>

                                        <label for="children" class="icon"><i class="icon icon__children"></i></label>

                                        <select class="custom-select form__group-input pets" id="children" name="children">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Can you live with Teenagers? -->
                                <div class="form__group col-sm-4 form__group__basic form__group__teenagers" id="group__teenagers">

                                    <div class="form__group__icon-input">

                                        <label for="teenagers" class="form__label pets_l">Teenagers?</label>

                                        <label for="teenagers" class="icon"><i class="icon icon__children"></i></label>

                                        <select class="custom-select form__group-input pets" id="teenagers" name="teenagers">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Pets -->
                                <div class="form__group col-sm-5 form__group__basic form__group__pets" id="group__pets">

                                    <div class="form__group__icon-input">

                                        <label for="pets" class="form__label pets_l">Pets?</label>

                                        <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                                        <select class="custom-select form__group-input pets" id="pets" name="pets">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group End Date of stay -->
                                <div class="form__group col-sm-5 form__group__lodging_type" id="group__lodging_type">
                                    <div class="form__group__icon-input">
                                            
                                        <label for="lodging_type" class="form__label">Type of Accommodation</label>
                                        <label for="lodging_type" class="icon"><i class="icon icon__lodging"></i></label>

                                        <select class="custom-select form__group-input gender" id="lodging_type" name="lodging_type" onchange="mostrar()">
                                            <option hidden="option" selected disabled> Select Lodging</option>
                                            <option value="Single1">Single (1)</option>
                                            <option value="Share1">Share (1)</option>
                                            <option value="Single2">Single (2)</option>
                                            <option value="Share2">Share (2)</option>
                                        </select>
                                        
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                    <p id="details__lodging" class="d-none"></p>
                                </div>


                                <script>
                                    const $select = document.querySelector("#lodging_type");

                                    function mostrar() {
                                        const indice = $select.selectedIndex;
                                        if(indice === -1) return; // Esto es cuando no hay elementos
                                        const opcionSeleccionada = $select.options[indice];

                                        if(opcionSeleccionada.value == 'Select Lodging'){

                                            document.getElementById('details__lodging').classList.add('d-none');

                                        }else if(opcionSeleccionada.value == 'Share2'){

                                            document.getElementById('details__lodging').classList.remove('d-none');
                                            document.getElementById('details__lodging').classList.add('detail__lodging');

                                            document.getElementById('group__food').classList.add('d-none');

                                            $('#details__lodging').html('Without Food Service <b>175 CAD</b> Weekly.');

                                        }else if(opcionSeleccionada.value == 'Single2'){

                                            document.getElementById('details__lodging').classList.remove('d-none');
                                            document.getElementById('details__lodging').classList.add('detail__lodging');

                                            document.getElementById('group__food').classList.add('d-none');

                                            $('#details__lodging').html('Without Food Service <b>375 CAD</b> Weekly.');
                                        }else if(opcionSeleccionada.value == 'Single1'){

                                            document.getElementById('details__lodging').classList.remove('d-none');
                                            document.getElementById('details__lodging').classList.add('detail__lodging');
                                            
                                            document.getElementById('group__food').classList.remove('d-none');

                                            $('#details__lodging').html('2 meals included (breakfast and dinner) <b>375 CAD</b> Weekly.');
                                        }else if(opcionSeleccionada.value == 'Share1'){

                                            document.getElementById('details__lodging').classList.remove('d-none');
                                            document.getElementById('details__lodging').classList.add('detail__lodging');

                                            document.getElementById('group__food').classList.remove('d-none');

                                            $('#details__lodging').html('2 meals included (breakfast and dinner) <b>250 CAD</b> Weekly.');
                                        }

                                    };

                                </script>
                                
                                <!-- Group Food -->
                                <div class="form__group col-sm-6 form__group__basic form__group__food d-none" id="group__food">

                                    <div class="form__group__icon-input">

                                        <label for="pets" class="form__label pets_l">Do you require a special diet?</label>

                                        <label for="food" class="icon"><i class="icon icon__food"></i></label>

                                        <select class="custom-select form__group-input food" id="food" name="food" onchange="specialDiet()">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <!-- Group Special -->
                                <div class="form__group form__group__basic form__group__special-diet d-none" id="group__special-diet">
                                
                                    

                                </div>

                                <script>
                                    // Special Diet

                                    const $specialD = document.querySelector("#food");

                                    function specialDiet() {
                                        const specialD = $specialD.selectedIndex;
                                        if(specialD === -1) return; // Esto es cuando no hay elementos
                                        const optionSdiet = $specialD.options[specialD];

                                        if(optionSdiet.value == 'Yes'){

                                            document.getElementById('group__special-diet').classList.remove('d-none');

                                            $('#group__special-diet').html(`
                                            
                                            <div class="form__group__div-special__diet">
                                                <div class="form__group__icon-input">

                                                    <label for="" class="icon"><i class="icon icon__diet"></i></label>
                                                    <h4 class="title__special-diet">Special Diet</h4>

                                                </div>
                                            

                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes">
                                                        <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">
                                                        <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                                                    </div>

                                                </div>

                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">
                                                        <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes">
                                                        <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                                                    </div>

                                                </div>
                                                
                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">
                                                        <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes">
                                                        <label class="custom-control-label" for="pork">No Pork</label>
                                                    </div>

                                                </div>
                                                
                                                <div class="form__group__row-diet">
                                                    
                                                    <div class="custom-control custom-checkbox-none custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">
                                                        <label class="custom-control-label" for="none">None</label>
                                                    </div>

                                                </div>

                                            </div>
                                            
                                            `    
                                            );

                                        }else{
                                            document.getElementById('group__special-diet').classList.add('d-none');
                                        }

                                    };

                                </script>

                            </div>

                        </div>


                        <h3 class="subtitle__section-card mb-5">Transport</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Pick-Up Service -->
                                <div class="form__group col-sm-5 form__group__basic form__group__pick_up" id="group__pick_up">

                                    <div class="form__group__icon-input">

                                        <label for="pick_up" class="form__label pets_l">Pick Up Service</label>

                                        <label for="pick_up" class="icon"><i class="icon icon__city"></i></label>

                                        <select class="custom-select form__group-input pets" id="pick_up" name="pick_up">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Drop Off Service -->
                                <div class="form__group col-sm-5 form__group__basic form__group__drop_off" id="group__drop_off">

                                    <div class="form__group__icon-input">

                                        <label for="drop_off" class="form__label pets_l">Drop off service</label>

                                        <label for="drop_off" class="icon"><i class="icon icon__city"></i></label>

                                        <select class="custom-select form__group-input pets" id="drop_off" name="drop_off">
                                            <option hidden="option" selected disabled>Select Option</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div><br>

            <div class="form__message" id="form__message">
            </div>

            <div class="form__group__btn-send" align="center">
                <button type="submit" class="form__btn">Register Student</button>
                <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>
            </div>

            

            
        </form>
    </main>

</div>


    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


<?php include 'footer.php' ?>

<script>
                                function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }
                            </script>


<script>
    document.getElementById("arrive_g").onchange = function(){fields()};

    function fields(){

        var arrive = document.getElementById("arrive_g").value;

        document.getElementById("firstd").value = arrive;
        document.getElementById("firstd2").value = arrive;
        document.getElementById("h_date").value = arrive;

    }
    
    document.getElementById("lastd").onchange = function(){calWeek()};

    function calWeek(){

        var firstd = document.getElementById("firstd").value;
        var lastd2 = document.getElementById("lastd").value;

        document.getElementById("firstd2").value = firstd;
        document.getElementById("lastd2").value = lastd2;

        //$('#calcWeeks').html(firstd);

        const date1 = new Date(firstd);
        const date2 = new Date(lastd2);
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        const diffWeeksD = diffDays / 7;

        if(diffWeeksD % 1 == 0){

            const diffWeeks = diffDays / 7; 

            $('#calcWeeks').html(diffWeeks  + " weeks accommodation");

        }else{

            const diffWeeks = Math.floor(diffDays / 7); 
            const diffDays2 = Math.floor(diffDays - diffWeeks * 7); 

            $('#calcWeeks').html(diffWeeks + " weeks and " + diffDays2 + " days accommodation");

        }

    }


    /* function myFunction(x) {
    if (x.matches) { // If media query matches
        document.getElementById('div__flex').classList.remove('col-md-4');
        document.getElementById('div__flex').classList.remove('col-md-8');
    } else {}
    }

    var x = window.matchMedia("(max-width: 767px)")
    myFunction(x) // Call listener function at run time
    x.addListener(myFunction) // Attach listener function on state changes 
    */
    
    
</script>

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/validate_form.js"></script>


<!--Date Input Safari-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script src="assets/js/date-safari.js"></script>


</body>
</html>