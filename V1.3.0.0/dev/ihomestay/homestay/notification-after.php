<link rel="stylesheet" href="assets/css/notification.css">

<style>
.img-stu {
  width: 2.5em;
  height: auto;
}
</style>

<?php

    $noti = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND confirmed = '0' ORDER BY id_not DESC");
    $n = mysqli_num_rows($noti);

    $peh = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
    $pe=$peh->fetch_assoc();

    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');

?>

<li class="dropdown " style="margin-top: 1%; list-style: none;">
  <a class="after" data-toggle="dropdown" href="#" style="color: #982A72; font-size: 14px;">
    <img src="assets/icon/notification 64.png" alt="" width="32" height="32">
    <span class="badge" id="bfha"> <?php echo $n; ?></span></a>

  <input type="hidden" id="mail" value="<?php echo $usuario ?>">

  <?php
        if ($n != 0 ) {
    ?>

  <style>
  .badge {
    display: inline;
  }
  </style>

  <?php
        }else{
    ?>

  <style>
  .badge {
    display: none;
  }
  </style>

  <?php
        }
    ?>

  <!-- List (1st level) -->
  <ul class="dropdown-menu d-m-after">

    <div class="container__notification">

      <form action="delete_noti.php" method="POST" class="m-0 p-0">

        <input type="hidden" id="mail_h" name="mail_h" value="<?php echo $usuario ?>">

        <p class="title__noti_div">You have <?php echo $n; ?> Notification <button type="submit"
            class="button_delete_all" onclick="deleteAll()" name="delete_all" title="Delete All notification">Delete
            all</button></p>

      </form>

      <?php 
                    if ($n == 0) {
                ?>

      <style>
      .title__noti_div {
        display: none;
      }

      ul.dropdown-menu {
        height: 3em;
        padding: 0;
      }

      .menu {
        overflow: hidden;
        display: none;
      }

      .title_not-i {
        color: #232159;
      }

      .scrollbar {
        display: none
      }
      </style>


      <?php
                    echo "<li class='title_not-i'><center><b> You don't have notification request </b></center></li>";
                }


                ?>

      <div class="content__noti">

        <div class="scrollbar" id="style-1">

          <?php 

                        while ($n = mysqli_fetch_array($noti)) {
                            $di = $n['id_not'];

                            $pe_s =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[user_i_mail]'");
                            $data = $pe_s->fetch_assoc();

                            $start_date = new DateTime($n['date_']);
                            $since_start = $start_date->diff(new DateTime($date));

                                        

                            if($since_start->m > '1'){

                                $datec = date_create($n['date_']);

                                $time_send = date_format($datec, 'jS M \of\ Y');

                            } elseif($since_start->m == '1'){

                                $time_send = $since_start->m.' month ago<br>';
                                                
                            }else{

                                if($since_start->d > '1'){

                                    if($since_start->d > '7'){

                                        $time_send = floor(($since_start->format("%a") / 7)) . " weeks ago";

                                    }else{

                                        $time_send = $since_start->d.' days ago<br>';
                                                    
                                    }

                                }elseif($since_start->d == '1'){

                                    $time_send = $since_start->d.' day ago<br>';

                                }else{

                                    if($since_start->h > '1'){

                                        $time_send = $since_start->h.' hours ago<br>';
                                                    
                                    } elseif($since_start->h == '1'){

                                        $time_send = $since_start->h.' hour ago<br>';

                                    }else{

                                        if($since_start->i > '1'){

                                            $time_send = $since_start->i.' minutes ago<br>';

                                        } elseif($since_start->i == '1'){

                                            $time_send = $since_start->i.' minute ago<br>';

                                        }else{
                                                    
                                            if($since_start->s != '0'){
                                                                
                                                $time_send = 'Just a moment';
                                                            
                                            }
                                                
                                        }
                                            
                                    }
                                        
                                }
                                    
                            }

                            if ($n['title'] == 'Extend Reservation') { ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info_not.php?art_id=<?php echo $data['id_student'] ?>" class="notification_panel new"
              id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info_not.php?art_id=<?php echo $data['id_student'] ?>" class="notification_panel view"
                id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$data['photo_s'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>
                  <p class="text_wview"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> wants extend your
                    stay in the <b>Room <?php echo $n['bedrooms']; ?></b>.</p>

                  <div class="date_reserve">
                    <div class="m-0 p-0 date_res">
                      <p class="text_wview" style="opacity: 0;"><b> Arrive: </b> <?php echo $n['start']; ?></p>
                      <p class="text_wview time"><b> Leave: </b> <?php echo $n['end_']; ?></p>
                    </div>

                    <div class="btns_acept" align="right">
                      <form action="extend_reserve.php" method="post">

                        <?php $sp = ' ' ?>


                        <input type="hidden" name="end" value="<?php echo $n['end_']; ?>">


                        <input type="hidden" name="mail" value="<?php echo $n['user_i_mail']; ?>">

                        <button class="btn btn_confirm">Confirm</button>
                        <button class="btn btn_reject">Reject</button>

                      </form>
                    </div>
                  </div>



                  <?php }else{ ?>

                  <p class="font_view"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> wants extend your
                    stay in the <b>Room <?php echo $n['bedrooms']; ?></b>.</p>

                  <div class="date_reserve">
                    <div class="m-0 p-0 date_res">
                      <p class="font_view" style="opacity: 0;"><b> Arrive: </b> <?php echo $n['start']; ?></p>
                      <p class="font_view time"><b> Leave: </b> <?php echo $n['end_']; ?></p>
                    </div>

                    <div class="btns_acept" align="right">
                      <form action="extend_reserve.php" method="post">

                        <?php $sp = ' ' ?>


                        <input type="hidden" name="end" value="<?php echo $n['end_']; ?>">


                        <input type="hidden" name="mail" value="<?php echo $n['user_i_mail']; ?>">

                        <button class="btn btn_confirm view_confirm">Confirm</button>
                        <button class="btn btn_reject view_reject">Reject</button>

                      </form>
                    </div>
                  </div>

                  <?php } ?>
                </div>

              </a>
          </div>

          <?php
    
                                }elseif ($n['title'] == 'Reservation Request') { ?>

          <div class="div_panel_noti">

            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info_not.php?art_id=<?php echo $data['id_student'] ?>" class="notification_panel new"
              id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info_not.php?art_id=<?php echo $data['id_student'] ?>" class="notification_panel view"
                id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$data['photo_s'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>
                  <p class="text_wview"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> wants to reserve
                    <b>Room <?php echo $n['bedrooms']; ?></b>.
                  </p>

                  <div class="date_reserve">
                    <div class="m-0 p-0 date_res">
                      <p class="text_wview"><b> Arrive: </b> <?php echo $n['start']; ?></p>
                      <p class="text_wview time2"><b> Leave: </b> <?php echo $n['end_']; ?></p>
                    </div>

                    <div class="btns_acept" align="right">
                      <form action="addEventNot.php" method="post">
                        <?php $sp = ' ' ?>

                        <input type="hidden" name="title" value="<?php echo $n['user_i'] . $sp . $n['user_i_l']; ?>">

                        <input type="hidden" name="start" value="<?php echo $n['start']; ?>">

                        <input type="hidden" name="end" value="<?php echo $n['end_']; ?>">

                        <input type="hidden" name="color" value="<?php echo $n['color']; ?>">

                        <input type="hidden" name="mail" value="<?php echo $n['user_i_mail']; ?>">

                        <input type="hidden" name="useri" value="<?php echo $pe['name_h']; ?>">

                        <input type="hidden" name="userii" value="<?php echo $pe['l_name_h']; ?>">

                        <input type="hidden" name="h_name" value="<?php echo $pe['h_name']; ?>">

                        <input type="hidden" name="bedroom" value="<?php echo $n['bedrooms']; ?>">

                        <input type="hidden" name="id_not" value="<?php echo $n['id_not']; ?>">

                        <button class="btn btn_confirm" name="save">Confirm</button>
                        <button class="btn btn_reject" name="delete">Reject</button>

                      </form>
                    </div>
                  </div>



                  <?php }else{ ?>
                  <p class="font_view"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> wants to reserve
                    <b>Room <?php echo $n['bedrooms']; ?></b>.
                  </p>

                  <div class="date_reserve">
                    <div class="m-0 p-0 date_res">
                      <p class="font_view"><b> Arrive: </b> <?php echo $n['start']; ?></p>
                      <p class="font_view time2"><b> Leave: </b> <?php echo $n['end_']; ?></p>
                    </div>

                    <div class="btns_acept" align="right">
                      <form action="addEventNot.php" method="post">
                        <?php $sp = ' ' ?>

                        <input type="hidden" name="title" value="<?php echo $n['user_i'] . $sp . $n['user_i_l']; ?>">

                        <input type="hidden" name="start" value="<?php echo $n['start']; ?>">

                        <input type="hidden" name="end" value="<?php echo $n['end_']; ?>">

                        <input type="hidden" name="color" value="<?php echo $n['color']; ?>">

                        <input type="hidden" name="mail" value="<?php echo $n['user_i_mail']; ?>">

                        <input type="hidden" name="useri" value="<?php echo $pe['name_h']; ?>">

                        <input type="hidden" name="userii" value="<?php echo $pe['l_name_h']; ?>">

                        <input type="hidden" name="h_name" value="<?php echo $pe['h_name']; ?>">

                        <input type="hidden" name="bedroom" value="<?php echo $n['bedrooms']; ?>">

                        <input type="hidden" name="id_not" value="<?php echo $n['id_not']; ?>">

                        <button class="btn btn_confirm" name="save">Confirm</button>
                        <button class="btn btn_reject" name="delete">Reject</button>

                      </form>
                    </div>
                  </div>

                  <?php } ?>
                </div>

              </a>
          </div>


          <?php
    
                                }elseif ($n['title'] == 'Cancel Report' && $n['user_i_mail'] == 'sebi@homebor.com') { 
    
                                    $pe_s2 =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[report_s]'");
                                    $data2 = $pe_s2->fetch_assoc();
    
                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info_not.php?art_id=<?php echo $data2['id_student'] ?>" class="notification_panel new"
              id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info_not.php?art_id=<?php echo $data2['id_student'] ?>" class="notification_panel view"
                id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../images/icon.png"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> has canceled the
                    report you made to <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s']; ?></b>. </p>

                  <?php }else{ ?>
                  <p class="font_view"><b><?php echo $n['user_i'] ?> <?php echo $n['user_i_l'];?></b> has canceled the
                    report you made to <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s']; ?></b>.</p>
                  <?php } ?>
                </div>

              </a>
          </div>

          <?php }elseif ($n['status'] == 'Reply') { 
                                
                                $ag =  $link->query("SELECT * FROM manager WHERE mail = '$n[user_i_mail]'");
                                $ag2 = $ag->fetch_assoc(); 
    
                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="reports.php" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="reports.php" class="notification_panel view" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$ag2['photo'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview"><b><?php echo $ag2['a_name'] ?> </b> has responded to your report. Click to see
                    details. </p>

                  <?php }else{ ?>
                  <p class="font_view"><b><?php echo $ag2['a_name'] ?> </b> has responded to your report. Click to see
                    details.</p>
                  <?php } ?>
                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Delete Reserve') {  
                                
                                $ag =  $link->query("SELECT * FROM manager WHERE mail = '$n[user_i_mail]'");
                                $ag2 = $ag->fetch_assoc();
    
                                $pe_s2 =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[report_s]'");
                                $data2 = $pe_s2->fetch_assoc();
                                
                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="reports.php" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="reports.php" class="notification_panel view" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$ag2['photo'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview">The student <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b>
                    reservation was removed from his home. </p>

                  <?php }else{ ?>
                  <p class="font_view">The student <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b>
                    reservation was removed from his home.</p>
                  <?php } ?>
                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Keep Reserve') { 
                                
                                $ag =  $link->query("SELECT * FROM manager WHERE mail = '$n[user_i_mail]'");
                                $ag2 = $ag->fetch_assoc();
    
                                $pe_s2 =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[report_s]'");
                                $data2 = $pe_s2->fetch_assoc();
                                
                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="reports.php" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="reports.php" class="notification_panel view" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$ag2['photo'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview">The <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b> student
                    reservation will continue active at home. </p>

                  <?php }else{ ?>
                  <p class="font_view">The <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b> student
                    reservation will continue active at home.</p>
                  <?php } ?>
                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Student Arrival 3w' || $n['title'] == 'Student Arrival 15d' || $n['title'] == 'Student Arrival 3d') { 
    
                                    $ag =  $link->query("SELECT * FROM manager WHERE mail = '$n[user_i_mail]'");
                                    $ag2 = $ag->fetch_assoc();
    
                                    $pe_s2 =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[reserve_h]'");
                                    $data2 = $pe_s2->fetch_assoc();
    
                                    $roc1 = date_create($n['start']);
                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info_not.php?art_id=<?php echo $data2['id_student'] ?>" class="notification_panel new"
              id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info_not.php?art_id=<?php echo $data2['id_student'] ?>" class="notification_panel view"
                id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$ag2['photo'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview"><b><?php echo $ag2['a_name'] ?></b> reminds you that
                    <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b>, will arrive on
                    <?php echo date_format($roc1, 'jS F '); ?>.
                  </p>

                  <?php }else{ ?>
                  <p class="font_view"><b><?php echo $ag2['a_name'] ?></b> reminds you that
                    <b><?php echo $data2['name_s'] . ' ' . $data2['l_name_s'] ?></b>, will arrive on
                    <?php echo date_format($roc1, 'jS F '); ?>.
                  </p>
                  <?php } ?>
                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Status Changed') { 

                                $pay =  $link->query("SELECT * FROM payments WHERE i_mail = '$usuario' AND reserve_s = '$n[des]'");
                                $r_pay = $pay->fetch_assoc();

                                $stu =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[des]'");
                                $r_stu = $stu->fetch_assoc();

                                $man =  $link->query("SELECT * FROM manager WHERE mail = '$n[user_i_mail]'");
                                $r_man = $man->fetch_assoc();

                                ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="homestay_payments" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="homestay_payments" class="notification_panel new" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$r_man['photo'].'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){

                                            if($r_pay['status_p'] == 'Paid' || $r_pay['status_p'] == 'Budgeted'){ ?>

                  <p class="text_wview"><?php echo $r_man['a_name'] ?> has changed the status of
                    <?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?> bill. See the details.</p>

                  <?php }else if($r_pay['status_p'] == 'Payable'){ ?>

                  <p class="text_wview"><?php echo $r_man['a_name'] ?> reminds you that you have not paid
                    <?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?> bill. Click to process payment</p>

                  <?php }}else{

                                            if($r_pay['status_p'] == 'Paid' || $r_pay['status_p'] == 'Budgeted'){ ?>

                  <p class="font_view"><?php echo $r_man['a_name'] ?> has changed the status of
                    <?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?> bill. See the details.</p>

                  <?php }else if($r_pay['status_p'] == 'Payable'){ ?>

                  <p class="font_view"><?php echo $r_man['a_name'] ?> reminds you that you have not paid
                    <?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?> bill.</p>

                  <?php }} ?>

                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Reservation Provider') { 
                                
            $queryStudent =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[user_i_mail]'");
            $rowStudent = $queryStudent->fetch_assoc();

            if($n['user_i_l'] != 'NULL'){
              $m_name_m = $n['user_i'] . $n['user_i_l'];
            }else{
              $m_name_m = $n['user_i'];
            }
                                
            $s_photo_s = $rowStudent['photo_s'];
            $s_id = $rowStudent['id_student'];
            $s_fullnames = $rowStudent['name_s'] . ' ' . $rowStudent['l_name_s'];

          ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info?art_id=<?php echo $s_id ?>" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info?art_id=<?php echo $s_id ?>" class="notification_panel new" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$s_photo_s.'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview"><b><?php echo $m_name_m ?></b> has assigned <b><?php echo $s_fullnames ?></b> to
                    your room
                    <?php echo $n['bedrooms'] ?>. click to see his information.</p>

                  <?php }else{ ?>

                  <p class="font_view"><b><?php echo $m_name_m ?></b> has assigned <b><?php echo $s_fullnames ?></b> to
                    your room
                    <?php echo $n['bedrooms'] ?>. click to see his information.</p>

                  <?php } ?>

                </div>

              </a>
          </div>

          <?php }elseif ($n['title'] == 'Reservation Rejected Provider') { 
                                
            $queryStudent =  $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[user_i_mail]'");
            $rowStudent = $queryStudent->fetch_assoc();
                                
            if($n['user_i_l'] != 'NULL'){
              $m_name_m = $n['user_i'] . $n['user_i_l'];
            }else{
              $m_name_m = $n['user_i'];
            }
            
            $s_photo_s = $rowStudent['photo_s'];
            $s_id = $rowStudent['id_student'];
            $s_name = $rowStudent['name_s'];

          ?>

          <div class="div_panel_noti">
            <form action="delete_noti.php" method="POST">

              <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

              <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one"
                title="Delete Notification"></button>

            </form>

            <?php if($n['state'] == '0'){ ?>

            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

            <a href="student_info?art_id=<?php echo $s_id ?>" class="notification_panel new" id="cancel_report">

              <?php }else{ ?>

              <p class="ban m-0"><?php echo $time_send ?></p>

              <a href="student_info?art_id=<?php echo $s_id ?>" class="notification_panel new" id="cancel_report">

                <?php } ?>

                <div class="col-2 p-0">
                  <?php echo'<img class="photo" src="../'.$s_photo_s.'"/>'; ?>
                </div>

                <div class="col-10 p-0 pr-3">
                  <?php if($n['state'] == '0'){ ?>

                  <p class="text_wview"><b><?php echo $m_name_m ?></b> He has rejected <b><?php echo $s_name."'s" ?></b>
                    request at your house.</p>

                  <?php }else{ ?>

                  <p class="font_view"><b><b><?php echo $m_name_m ?></b> He has rejected
                      <b><?php echo $s_name."'s" ?></b> request at your house.</p>

                  <?php } ?>

                </div>

              </a>
          </div>

          <?php }} ?>


          <div class="force-overflow"></div>

        </div>



      </div>

    </div>

  </ul>
</li>

<script>
function deleteOne() {

  let delete_one = $('#id_not').val();

  $.ajax({
    url: 'delete_noti.php',
    type: 'POST',
    data: {
      delete_one
    },
    success: function(hola) {

      location.reload();

    }
  });

}

function deleteAll() {

  let mail_h = $('#mail_h').val();

  $.ajax({
    url: 'delete_noti.php',
    type: 'POST',
    data: {
      mail_h
    },
    success: function(hola) {

      location.reload();

    }
  });

}
</script>