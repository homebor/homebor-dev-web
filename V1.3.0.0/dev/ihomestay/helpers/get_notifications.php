<?php
require '../../xeon.php';

session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');

if ($usuario) {
  $usersQuery = $link->query("SELECT usert FROM users WHERE mail = '$usuario' ");
  $row_user = $usersQuery->fetch_assoc();

  if (strtolower($row_user['usert']) == "agent") {
    // TODO NOTIFICATIONS AGENT
    // ? AGENT QUERY
    $agentsQuery = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario' ");
    $row_agent = $agentsQuery->fetch_assoc();

    // ? MANAGER QUERY     
    $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
    $row_manager = $managerQuery->fetch_assoc();

    // ? NOTIFICATIONS QUERY
    $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' ORDER BY id_not DESC");
    $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' AND confirmed = '0' AND state = '0' ");
    $total_new_notifications = mysqli_num_rows($newNotifications);


    while ($notification = mysqli_fetch_array($queryNotifications)) {
      // ? REGISTRO
      // * STUDENT REGISTER
      if ($notification['title'] == "Change Student") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_s = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $queryAgent2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * CHANGE STUDENT
      if ($notification['title'] == "Change Student") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_s = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $queryAgent2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * CHANGE STUDENT
      if ($notification['title'] == "Change Student") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_s = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $queryAgent2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // ? SEGUIMIENTO
      // * CHANGE STUDENT
      if ($notification['title'] == "Change Student") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_s = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $queryAgent2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * CANCEL RESERVATION
      if ($notification['title'] == "Cancel Reservation") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        if ($notification['status'] == "Reply") {
          $description = "<b>$row_homestay[h_name]</b> wants to remove the <b>$row_student[name_s] $row_student[l_name_s]</b> reservation. <small>Click for details</small>";
        } else $description = "<b>$row_homestay[h_name]</b> has responded to your report. <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = "$row_homestay[phome]";
        $notification['description'] = $description;
        $notification['userImage1'] = "$row_student[photo_s]";
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * REPORT SITUATION
      if ($notification['title'] == "Report Situation") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        if ($notification['status'] == "Reply") {
          $description = "<b>$row_homestay[h_name]</b> has responded to your report. <small>Click for details</small>";
        } else $description = "<b>$row_homestay[h_name]</b> has reported to <b>$row_student[name_s] $row_student[l_name_s].</b> <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = "$row_homestay[phome]";
        $notification['description'] = $description;
        $notification['userImage1'] = $row_student['photo_s'];
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * FLIGHT DATA CONFIRMATION
      if ($notification['title'] == "Flight Data Confirmation") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[user_i_mail]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$row_student[name_s] $row_student[l_name_s]</b> has updated his flight information. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $row_student['photo_s'];
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * DELETE RESERVE
      if ($notification['title'] == "Delete Reserve") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $agentsQuery2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$name</b> has deleted <b>$row_student[name_s] $row_student[l_name_s]</b> reservation in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>  <small>Click for details</small>";

        // Keep = (nombre coordinador) has kept the reserve of (nombre estudiante) in (nombre casa)


        $notification['url'] = "reports";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * KEEP RESERVE
      if ($notification['title'] == "Keep Reserve") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $agentsQuery2->fetch_assoc();
        if ($row_agent2) $image = $row_agent2['photo'];
        else {
          $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
          $row_manager = $managerQuery->fetch_assoc();
          $image = $row_manager['photo'];
        }

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$row_agent2[name] $row_agent2[l_name]</b> has kept the reserve of <b>$row_student[name_s] $row_student[l_name_s]</b> in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>  <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }

      $allNotifications[] = $notification;
    }


    // ? DATA TO SEND
    $jsonToSend = array(
      'notifications' => $allNotifications,
      'newNotifications' => $total_new_notifications,
    );
  } else if (strtolower($row_user['usert']) === "manager") {
    /* //TODO NOTIFICATIONS MANAGER */
    // ? MANAGER QUERY
    $managersQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
    $row_manager = $managersQuery->fetch_assoc();

    // ? NOTIFICATIONS QUERY
    $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' ORDER BY id_not DESC");
    $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' AND confirmed = '0' AND state = '0' ");
    $total_new_notifications = mysqli_num_rows($newNotifications);


    while ($notification = mysqli_fetch_array($queryNotifications)) {
      // * CHANGE STUDENT
      if ($notification['title'] == "Change Student") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_s = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $queryAgent2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $image = $row_manager['photo'];
          $name = "$row_manager[name] $row_manager[l_name]";
        }

        $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * CANCEL RESERVATION
      if ($notification['title'] == "Cancel Reservation") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        if ($notification['status'] == "Reply") {
          $description = "<b>$row_homestay[h_name]</b> wants to remove the <b>$row_student[name_s] $row_student[l_name_s]</b> reservation. <small>Click for details</small>";
        } else $description = "<b>$row_homestay[h_name]</b> has responded to your report. <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = "$row_homestay[phome]";
        $notification['description'] = $description;
        $notification['userImage1'] = "$row_student[photo_s]";
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * REPORT SITUATION
      if ($notification['title'] == "Report Situation") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        if ($notification['status'] == "Reply") {
          $description = "<b>$row_homestay[h_name]</b> has responded to your report. <small>Click for details</small>";
        } else $description = "<b>$row_homestay[h_name]</b> has reported to <b>$row_student[name_s] $row_student[l_name_s].</b> <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = "$row_homestay[phome]";
        $notification['description'] = $description;
        $notification['userImage1'] = $row_student['photo_s'];
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * FLIGHT DATA CONFIRMATION
      if ($notification['title'] == "Flight Data Confirmation") {
        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[user_i_mail]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$row_student[name_s] $row_student[l_name_s]</b> has updated his flight information. <small>Click for details</small>";

        $notification['url'] = "student_info?art_id=$row_student[id_student]";
        $notification['issuerImage'] = $row_student['photo_s'];
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * DELETE RESERVE
      if ($notification['title'] == "Delete Reserve") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $agentsQuery2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managersQuery2 = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
          $row_manager2 = $managersQuery2->fetch_assoc();
          $image = $row_manager2['photo'];
          $name = "$row_manager2[name] $row_manager2[l_name]";
        }

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$name</b> has deleted <b>$row_student[name_s] $row_student[l_name_s]</b> reservation in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>  <small>Click for details</small>";


        $notification['url'] = "reports";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }
      // *
      // *
      // *
      // *
      // *
      // * KEEP RESERVE
      if ($notification['title'] == "Keep Reserve") {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
        $row_homestay = $homestayQuery->fetch_assoc();

        $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
        $row_agent2 = $agentsQuery2->fetch_assoc();
        if ($row_agent2) {
          $image = $row_agent2['photo'];
          $name = "$row_agent2[name] $row_agent2[l_name]";
        } else {
          $managersQuery2 = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
          $row_manager2 = $managersQuery2->fetch_assoc();
          $image = $row_manager2['photo'];
          $name = "$row_manager2[name] $row_manager2[l_name]";
        }

        $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
        $row_student = $studentQuery->fetch_assoc();

        $description = "<b>$name</b> has kept the reserve of <b>$row_student[name_s] $row_student[l_name_s]</b> in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>  <small>Click for details</small>";

        $notification['url'] = "reports";
        $notification['issuerImage'] = $image;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = null;
      }

      $allNotifications[] = $notification;
    }


    // ? DATA TO SEND
    $jsonToSend = array(
      'notifications' => $allNotifications,
      'newNotifications' => $total_new_notifications,
    );
  } else if (strtolower($row_user['usert']) === "homestay") {
    // ? HOMESTAY QUERY
    $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$usuario' ");
    $row_homestay = $homestayQuery->fetch_assoc();

    // ? NOTIFICATIONS QUERY
    $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_homestay[mail_h]' AND state < '2' ORDER BY date_ DESC");
    $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_homestay[mail_h]' AND state = '0' ");
    $total_new_notifications = mysqli_num_rows($newNotifications);

    // ? ADDITIONAL INFORMATION


    while ($notification = mysqli_fetch_array($queryNotifications)) {

      if ($notification['title'] == 'Reservation Provider' || $notification['title'] == 'Reservation Rejected Provider') {

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
        $row_student = $studentQuery->fetch_assoc();

        if ($notification['user_i_l'] != 'NULL') $m_name = $notification['user_i'] . $notification['user_i_l'];
        else $m_name = $notification['user_i'];

        if ($row_student['name_s'] != 'NULL') $s_fullnames = $row_student['name_s'] . ' ' . $row_student['l_name_s'];
        else $s_fullnames = $notification['name_s'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $issuerImage = 'assets/emptys/profile-student-empty.png';
        else $issuerImage = $row_student['photo_s'];

        if ($notification['title'] == 'Reservation Provider') {
          $description = '<b>' . $m_name . '</b> has assigned <b>' . $s_fullnames . '<b> to your room ' . $notification['bedrooms'] . '.</b> Click to see his information.';
        } else if ($notification['title'] == 'Reservation Rejected Provider') {
          $description = "<b>" . $m_name . "</b> has rejected <b>" . $row_student['name_s'] . "'s</b> request at your house.";
        }

        $url = 'student_info?art_id=' . $row_student['id_student'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = false;
      }

      if ($notification['title'] == 'Reservation Request') {

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
        $row_student = $studentQuery->fetch_assoc();

        if ($row_student['name_s'] != 'NULL') $s_fullnames = $row_student['name_s'] . ' ' . $row_student['l_name_s'];
        else $s_fullnames = $notification['name_s'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $issuerImage = 'assets/emptys/profile-student-empty.png';
        else $issuerImage = $row_student['photo_s'];

        $description = '<b>' . $s_fullnames . '</b> wants to reserve <b>Room ' . $notification['bedrooms'] . '</b>.';

        $url = 'student_info_not?art_id=' . $row_student['id_student'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = false;
        $notification['userImage2'] = false;
        $notification['start'] = $notification['start'];
        $notification['end'] = $notification['end_'];
        if ($notification['confirmed'] == '1') {
          if ($notification['status'] !== 'Rejected') $notification['confirm'] = true;
          else $notification['confirm'] = false;
        }
      }

      if ($notification['title'] == 'Delete Reserve' || $notification['title'] == 'Keep Reserve') {

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[report_s]'");
        $row_student = $studentQuery->fetch_assoc();

        $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $userImage1 = 'assets/emptys/profile-student-empty.png';
        else $userImage1 = $row_student['photo_s'];

        if ($notification['title'] == 'Delete Reserve') {
          $description = 'The Student <b>' . $row_student['name_s'] . ' ' . $row['l_name_s'] . '</b> reservation was removed from your home.';
        } else if ($notification['title'] == 'Keep Reserve') {
          $description = "The reservation of <b>" . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . "</b> will continue to be active in your house.";
        }

        $url = 'reports';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = $userImage1;
        $notification['userImage2'] = false;
      }

      if ($notification['title'] == 'Reservation Change Before' || $notification['title'] == 'Reservation Change After') {

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[reserve_h]'");
        $row_student = $studentQuery->fetch_assoc();

        $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $userImage1 = 'assets/emptys/profile-student-empty.png';
        else $userImage1 = $row_student['photo_s'];

        if ($notification['title'] == 'Reservation Change Before') {
          $description = '<b>' . $row_manager['a_name'] . '</b> has changed your reservation of <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b>';
        } else if ($notification['title'] == 'Reservation Change After') {
          $description = '<b>' . $row_manager['a_name'] . '</b> has changed the reservation from <b>' . $row_student['name_s'] . ' ' . $row['l_name_s'] . '</b> to your home.';
        }

        $url = 'student_info?art_id=' . $row_student['id_student'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = $userImage1;
      }

      if (
        $notification['title'] == 'Student Arrival 3w' || $notification['title'] == 'Student Arrival 15d' ||
        $notification['title'] == 'Student Arrival 3d'
      ) {

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[reserve_h]'");
        $row_student = $studentQuery->fetch_assoc();

        $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $userImage1 = 'assets/emptys/profile-student-empty.png';
        else $userImage1 = $row_student['photo_s'];

        $startEvent = date_create($notification['start']);
        $start = date_format($startEvent, 'jS F, Y ');

        $description = '<b>' . $row_manager['a_name'] . '</b> reminds you that <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b>, will arrive on <b>' . $start . '</b>';

        $url = 'student_info?art_id=' . $row_student['id_student'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = $userImage1;
      }

      if ($notification['status'] == 'Reply') {

        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $userImage1 = 'assets/emptys/profile-student-empty.png';
        else $userImage1 = $row_student['photo_s'];

        $description = '<b>' . $row_manager['a_name'] . '</b>, has responded to your report. Click to see details.';

        $url = 'reports';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;

      }

      if ($notification['title'] == 'Status Changed') {

        $paymentsQuery = $link->query("SELECT status_p FROM payments WHERE i_mail = '$usuario' AND reserve_s = '$notification[des]'");
        $row_payments = $paymentsQuery->fetch_assoc();

        $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[des]'");
        $row_student = $studentQuery->fetch_assoc();

        $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
          $userImage1 = 'assets/emptys/profile-student-empty.png';
        else $userImage1 = $row_student['photo_s'];

        if ($row_payments['status_p'] == 'Paid' || $row_payments['status_p'] == 'Budgeted') {
          $description = '<b>' . $row_manager['a_name'] . '</b>, has responded to your report. Click to see details.';
        } else if ($row_payments['status_p'] == 'Payable') {
          $description = '<b>' . $row_manager['a_name'] . '</b> reminds you that you have not paid <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b> bill. Click to process payment';
        }

        $url = 'homestay_payments';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = $userImage1;
      }

      if ($notification['title'] == 'Extend Reservation') {
      } // TODO QUERYS EXTEND RESERVATION

      $allNotifications[] = $notification;
    }


    // ? DATA TO SEND
    $jsonToSend = array(
      'notifications' => $allNotifications,
      'newNotifications' => $total_new_notifications,
    );
  } else if (strtolower($row_user['usert']) === "student") {
    // ? STUDENT QUERY
    $studentsQuery = $link->query("SELECT id_student, name_s, l_name_s, photo_s, mail_s FROM pe_student WHERE mail_s = '$usuario' ");
    $row_student = $studentsQuery->fetch_assoc();

    // ? NOTIFICATIONS QUERY
    $queryNotifications = $link->query("SELECT * FROM noti_student WHERE user_r = '$row_student[mail_s]' AND state < '2' ORDER BY date_ DESC");
    $newNotifications = $link->query("SELECT * FROM noti_student WHERE user_r = '$row_student[mail_s]' AND confirmed = '0'
    AND state = '0' ");
    $total_new_notifications = mysqli_num_rows($newNotifications);


    while ($notification = mysqli_fetch_array($queryNotifications)) {

      if ($notification['des'] == 'Rejected') {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        $description = 'Sorry <b>' . $row_student['name_s'] . '! ' . $notification['h_name'] . ' </b>was rechazed your request';

        $url = 'detail.php?art_id=' . $row_homestay['id_home'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Reservation Change') {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        $description = '<b>' . $row_manager['a_name'] . '</b> has changed your stay to <b>' . $row_homestay['h_name'] . '</b>';

        $url = 'detail.php?art_id=' . $row_homestay['id_home'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Student Confirmed') {
        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        $description = '<b>' . $notification['h_name'] . '</b> has accepted your reservation request.';

        $url = 'detail.php?art_id=' . $row_homestay['id_home'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Delete Reserve' || $notification['des'] === 'Keep Reserve') {
        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        if ($notification['des'] === 'Delete Reserve') {
          $description = 'Sorry <b>' . $row_student['name_s'] . '</b>! <b> Your reservation has been removed from ' . $notification['h_name'];
        } else if ($notification['des'] === 'Keep Reserve') {
          $description = 'Your reservation at <b>' . $notification['h_name'] . '</b> is still active.';
        }

        $url = '#';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Flight Information') {
        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        $description = $row_student['name_s'] . ', fill in your details! Click here before reserving a room to enter the flight data';

        $url = 'edit_student';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Student Confirmed Provider' || $notification['des'] == 'Student Rejected Provider') {
        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[change_house]'");
        $row_manager = $managerQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        if ($notification['des'] == 'Student Confirmed Provider') {
          $description = '<b>' . $row_manager['a_name'] . '</b> has assigned your reservation to ' . $row_homestay['h_name'] . ' Click for details';
        } else if ($notification['des'] == 'Student Rejected Provider') {
          $description = $row_manager['a_name'] . '</b> has rejected your request in ' . $notification['h_name'];
        }

        $url = 'detail?art_id=' . $row_homestay['id_home'];

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Reassigned') {
        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        $description = $row_student['name_s'] . ', your provider has assigned you new houses, check the details of them.';

        $url = 'index';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Cancel Reservation') {
        $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
        $row_manager = $managerQuery->fetch_assoc();

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
          $issuerImage = 'assets/icon/home 64.png';
        else $issuerImage = $row_manager['photo'];

        if (empty($row_homestay['phome']) || $row_homestay['phome'] === 'NULL')
          $userImage1 = 'assets/emptys/frontage-empty.png';
        else $userImage1 = $row_homestay['phome'];

        $description = '<b>' . $row_manager['a_name'] . '</b>, has started a report. click see the information.';

        $url = '#';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        $notification['userImage1'] = $userImage1;
      }

      if ($notification['des'] == 'Today' || $notification['des'] == '1 Day' || $notification['des'] == '2 Days' || $notification['des'] == '3 Days' || $notification['des'] == '4 Days' || $notification['des'] == '5 Days') {

        $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
        $row_homestay = $homestayQuery->fetch_assoc();

        if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
          $issuerImage = 'assets/emptys/frontage-empty.png';
        else $issuerImage = $row_homestay['phome'];

        if ($notification['des'] == 'Today') $expirationDay = 'Today';
        if ($notification['des'] == '1 Day') $expirationDay = 'Tomorrow';
        if ($notification['des'] == '2 Days') $expirationDay = '2 Days';
        if ($notification['des'] == '3 Days') $expirationDay = '3 Days';
        if ($notification['des'] == '4 Days') $expirationDay = '4 Days';
        if ($notification['des'] == '5 Days') $expirationDay = '5 Days';

        $description = 'The reservation at <b>' . $notification['h_name'] . '</b> will expire ' . $expirationDay . '. Click here to extend session.';

        $url = '#';

        $notification['url'] = $url;
        $notification['issuerImage'] = $issuerImage;
        $notification['description'] = $description;
        // $notification['userImage1'] = $userImage1;
      }

      $allNotifications[] = $notification;
    }


    // ? DATA TO SEND
    $jsonToSend = array(
      'notifications' => $allNotifications,
      'newNotifications' => $total_new_notifications,
    );
  }


  echo json_encode($jsonToSend), exit;
}
