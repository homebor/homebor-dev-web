<?php
// TODO WINYERSON
if ($_POST['homestay_id']) {
  require '../../../xeon.php';

  session_start();
  error_reporting(0);

  // TODO VARIABLES
  $id = $_POST['homestay_id'];
  $usuario = $_SESSION['username'];

  // TODO VALIDACIONES
  if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

  // TODO CONSULTAS
  $homestayQuery = $link->query("SELECT id_home FROM pe_home WHERE id_home = '$id' ");
  $row_homestay = $homestayQuery->fetch_assoc();

  $queryManager_IJ = $link->query("SELECT * FROM manager INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and manager.id_m = pe_student.id_m");
  $row_manager = $queryManager_IJ->fetch_assoc();

  $queryAgents_IJ = $link->query("SELECT * FROM agents INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and agents.id_ag = pe_student.id_ag");
  $row_agent = $queryAgents_IJ->fetch_assoc();

  $queryStudent = $link->query("SELECT id_m, id_ag, name_s, l_name_s, mail_s FROM pe_student WHERE mail_s = '$usuario'");
  $row_student = $queryStudent->fetch_assoc();


  if ($row_student['id_m'] == '0' and $row_student['id_ag'] == '0') {
    $jsonToSend = array(
      'case' => 1,
    );
  } else if ($row_student['id_m'] != '0' and $row_student['id_ag'] == '0') {
    $jsonToSend = array(
      'case' => 2,
      'agentHref' => "agent_info.php?art_id=" . $row_manager['id_m'],
      'agentPhoto' => "../../" . $row_manager['photo'],
      'agencyName' => $row_manager['a_name'],
      'agentMail' => $row_manager['mail'],
      'agentNum' => $row_manager['num'],
      'agentType' => 'Manager',
    );
  } else {
    $jsonToSend = array(
      'case' => 3,
      'agentHref' => "agent_info.php?art_id=" . $row_agent['id_ag'],
      'agentPhoto' => "../../" . $row_agent['photo'],
      'agencyName' => $row_agent['agency'],
      'agentFullname' => $row_agent['name'] . " " . $row_agent['l_name'],
      'agentMail' => $row_agent['a_mail'],
      'agentNum' => $row_agent['num'],
      'agentType' => 'Coordinator',
    );
  }

  $jsonToSend += array(
    'studentFullname' => $row_student['name_s'] . " " . $row_student['l_name_s'],
    'studentMail' => $row_student['mail_s'],
    'studentMessage' => '#' . $row_homestay['id_home'],
  );

  echo json_encode($jsonToSend);
}
