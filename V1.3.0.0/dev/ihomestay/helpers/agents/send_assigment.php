<?php
require '../../../xeon.php';

session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$ID_STUDENT = $_POST['student_id'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');

if ($usuario && $ID_STUDENT) {
  $HOUSE1 = $_POST['house1'];
  $HOUSE2 = $_POST['house2'];
  $HOUSE3 = $_POST['house3'];
  $HOUSE4 = $_POST['house4'];
  $HOUSE5 = $_POST['house5'];


  $queryUsers = $link->query("SELECT usert FROM users WHERE mail = '$usuario' ");
  $row_user = $queryUsers->fetch_assoc();

  $queryStudents = $link->query("SELECT mail_s FROM pe_student WHERE id_student = '$ID_STUDENT' ");
  $row_student = $queryStudents->fetch_assoc();

  if (strtolower($row_user['usert']) == "manager") {
    $queryManagers = $link->query("SELECT name, l_name FROM manager WHERE mail = '$usuario' ");
    $row_manager = $queryManagers->fetch_assoc();

    $queryNotification = $link->query("INSERT INTO `noti_student`(`h_name`, `user_i`, `user_i_l`, `user_i_mail`, `user_r`, `date_`, `state`, `confirmed`, `des`) VALUES ('NULL', '$row_manager[name]', '$row_manager[l_name]', '$usuario', '$row_student[mail_s]', '$date', '0', '0', 'Reassigned')");
  } else if (strtolower($row_user['usert']) == "agent") {
    $queryAgents = $link->query("SELECT name, l_name FROM agents WHERE a_mail = '$usuario' ");
    $row_agent = $queryAgents->fetch_assoc();

    $queryNotification = $link->query("INSERT INTO `noti_student`(`h_name`, `user_i`, `user_i_l`, `user_i_mail`, `user_r`, `date_`, `state`, `confirmed`, `des`) VALUES ('NULL', '$row_agent[name]', '$row_agent[l_name]', '$usuario', '$row_student[mail_s]', '$date', '0', '0', 'Reassigned')");
  }

  $queryStudent = $link->query("UPDATE pe_student SET 
status='Student for Select House',
house1='$HOUSE1',
house2='$HOUSE2',
house3='$HOUSE3',
house4='$HOUSE4',
house5='$HOUSE5' WHERE id_student = '$ID_STUDENT' ");

  echo json_encode($queryStudent);
}
