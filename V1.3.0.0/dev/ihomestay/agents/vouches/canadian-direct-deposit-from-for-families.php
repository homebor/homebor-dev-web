<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';
session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM pe_home WHERE id_home = '$id'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

if ($usuario = $row['a_mail']) {
   
} else {
     header("location: ../index.php");
}

}

$username = $row2['name_h'].' '.$row2['l_name_h'];
$address = $row2['dir'].', '.$row2['city'];

$orgDate = $row2['db'];
$newDate = date("d-m-Y", strtotime($orgDate));

 
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetTitle('Direct Deposit');

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',10,20,-300);


$pdf->SetFont('Arial','B',16);
$pdf->SetXY(0,70);
$pdf->Cell(0,0,"Canadian Direct Deposit Set-Up",0,0,'C');

//table
$pdf->SetFont('Arial','B',12);
$pdf->SetXY(10,80);
$width_cell=array(80,0,0,0);
$pdf->SetFillColor(217,217,217);
$pdf->SetTextColor(0,0,0); // Background color of header 
// Header starts /// 
$pdf->Cell($width_cell[1],10,'PERSONAL INFORMATION',1,0,'L',true); // First header column 
$pdf->Cell($width_cell[1],10,'',1,0,'C',true);  // Second header column
$pdf->Cell($width_cell[1],10,'',1,0,'C',true); // Third header column 
$pdf->Cell($width_cell[1],10,'',1,1,'C',true); // Fourth header column
//// header is over ///////

$pdf->SetFont('Arial','',11);
$pdf->SetTextColor(0,0,0);

// First row of data
$pdf->SetTextColor(0,0,0); 
$pdf->Cell($width_cell[0],10,'Name',1,0,'L', true); // First column of row 1 
$pdf->Cell($width_cell[1],10,$username,1,0,'C',false); // Second column of row 1 
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 1 
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // Fourth column of row 1 
//  First row of data is over 

//  Third row of data
$pdf->Cell($width_cell[0],10,'Address',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,$address,1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Phone',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,$row2['num'],1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Email',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,$row2['mail_h'],1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Date of Birth (DD/MM/YYYY)',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,$newDate,1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

// Header starts /// 
$pdf->Cell($width_cell[1],10,'',1,0,'L',true); // First header column 
$pdf->Cell($width_cell[1],10,'',1,0,'C',true);  // Second header column
$pdf->Cell($width_cell[1],10,'',1,0,'C',true); // Third header column 
$pdf->Cell($width_cell[1],10,'',1,1,'C',true); // Fourth header column
//// header is over ///////

// First row of data 
$pdf->Cell($width_cell[0],10,'Account Holder Name',1,0,'L', true); // First column of row 1 
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 1 
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 1 
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // Fourth column of row 1 
//  First row of data is over 

//  Third row of data
$pdf->Cell($width_cell[0],10,'Address',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Bank Name',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Branch Transit (5 digits)',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,' ',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Bank or Financial Institution (3 Digits)',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Account (Up to 12 Digits)',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[1],10,'',0,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,'',0,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[1],10,'',0,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[1],10,'',0,1,'C',false); // fourth column of row 3

$pdf->Cell($width_cell[0],10,'Email E transfer',1,0,'L',true); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3



$pdf->Output();
?>