<?php

require '../../xeon.php';

session_start();
$usuario = $_SESSION['username'];


if (isset($_GET['art_id'])) {
  $id = $_GET['art_id'];

  $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
  $row = $homestayQuery->fetch_assoc();

  $query3 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
  $row3 = $query3->fetch_assoc();


  $query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
  $row4 = $query4->fetch_assoc();

  $query5 = $link->query("SELECT * FROM manager INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and manager.id_m = pe_student.id_m");
  $row5 = $query5->fetch_assoc();

  $query20 = $link->query("SELECT * FROM manager WHERE id_m = $row[id_m]");
  $man = $query20->fetch_assoc();

  $query6 = $link->query("SELECT * FROM agents INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and agents.id_ag = pe_student.id_ag");
  $row6 = $query6->fetch_assoc();

  $query7 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
  $row7 = $query7->fetch_assoc();

  $query91 = $link->query("SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a");
  $row91 = $query91->fetch_assoc();

  $query10 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
  $row10 = $query10->fetch_assoc();
}

?>

<!doctype html>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.8">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.8">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.8">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.8">
  <link rel="stylesheet" href="../assets/css/header.css?ver=1.0.8">
  <link rel="stylesheet" href="assets/css/detail.css?ver=1.0.8">
  <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.8">

  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.8"></script>

  <!-- Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js?ver=1.0.8"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />


  <title>Homebor - Detail Homestay</title>

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
</head>

<!--// TODO HEADER -->
<?php require 'header.php'; ?>

<body style="background: #eee;">
  <div class="ts-page-wrapper ts-homepage" id="page-top">


    <!-- // TODO MAIN -->
    <main id="ts-main">
      <input type="hidden" id="homestay-id" value="<?php echo $id ?>">


      <!--// TODO PAGE TITLE ======================================================-->
      <section id="page-title">
        <div class="container mt-5 pt-5">

          <div class="d-block d-sm-flex justify-content-between">

            <!--Title-->
            <div class="ts-title mb-0">
              <h1 id="homestay-name"></h1>
              <h5 id="homestay-address" class="ts-opacity__90"></h5>
            </div>

          </div>

        </div>
      </section>

      <!--GALLERY CAROUSEL ================================-->
      <section id="gallery-carousel">

        <div id="carousel-container" class="owl-carousel ts-gallery-carousel ts-gallery-carousel__multi"
          data-owl-dots="1" data-owl-items="3" data-owl-center="1" data-owl-loop="1">

          <!--SLIDE FRONTAGE-->
          <div class="slide">
            <?php if(!empty($row['phome']) && $row['phome'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row['phome']; ?> ">
            </div>
            <?php } else{ ?>
            <div class="ts-image " data-bg-image="../assets/emptys/frontage-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE FAMILY PICTURE-->
          <div class="slide">
            <?php if(!empty($row4['fp']) && $row4['fp'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['fp'] ?>">
            </div>
            <?php } else { ?>
            <div class="ts-image" data-bg-image="../assets/emptys/family-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE LIVING ROOM-->
          <div class="slide">
            <?php if(!empty($row4['pliving']) && $row4['pliving'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['pliving'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/living-room-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE AREA 1-->
          <div class="slide">
            <?php if(!empty($row4['parea1']) && $row4['parea1'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['parea1'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/kitchen-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE AREA 2-->
          <div class="slide">
            <?php if(!empty($row4['parea2']) && $row4['parea2'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['parea2'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/dinning-room-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE AREA 3-->
          <div class="slide">
            <?php if(!empty($row4['parea3']) && $row4['parea3'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['parea3'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/living-room-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE AREA 4-->
          <div class="slide">
            <?php if(!empty($row4['parea4']) && $row4['parea4'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['parea4'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/living-room-empty.png">
            </div>
            <?php } ?>
          </div>

          <!--SLIDE BATHROOM 1-->
          <div class="slide">
            <?php if(!empty($row4['pbath1']) && $row4['pbath1'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['pbath1'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/bathroom-empty.png">
            </div>

            <?php } ?>
          </div>

          <!--SLIDE BATHROOM 2-->
          <div class="slide">
            <?php if(!empty($row4['pbath2']) && $row4['pbath2'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['pbath2'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/bathroom-empty.png">
            </div>

            <?php } ?>
          </div>

          <!--SLIDE BATHROOM 3-->
          <div class="slide">
            <?php if(!empty($row4['pbath3']) && $row4['pbath3'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['pbath3'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/bathroom-empty.png">
            </div>

            <?php } ?>
          </div>

          <!--SLIDE BATHROOM 4-->
          <div class="slide">
            <?php if(!empty($row4['pbath4']) && $row4['pbath4'] != 'NULL'){ ?>
            <div class="ts-image" data-bg-image="<?php echo '../../' . $row4['pbath4'] ?>">
            </div>
            <?php }else{ ?>
            <div class="ts-image" data-bg-image="../assets/emptys/bathroom-empty.png">
            </div>

            <?php } ?>
          </div>





        </div>

      </section>

      <section id="description-house" class="d-flex justify-content-center w-100">
        <div class="m-0 p-0 col-10 col-sm-9 col-md-6 col-lg-6">
          <h3>Description of the House</h3>
          <article class="card p-3">
            <p id="description-house-content" class="m-0"></p>
          </article>
        </div>
      </section>

      <!-- // TODO SECTION QUICK INFO -->

      <section id="quick-info-section">

        <div class="row d-flex justify-content-center px-3">
          <div class="col-11 col-sm-10 col-md-7 col-lg-3">

            <!-- // TODO OWNER DETAILS -->

            <h3>Owner Details</h3>

            <article class="card p-3">
              <dl class="ts-description-list__line mb-0">
                <dt>Email:</dt>
                <dd id="email-home"></dd>

                <dt>Names:</dt>
                <dd id="fullname-home"></dd>

                <dt>Date of Birth:</dt>
                <dd id="db-property-home"></dd>

                <dt>Gender:</dt>
                <dd id="gender-home"></dd>

                <dt>Phone Number:</dt>
                <dd id="pnumber-home"></dd>

                <dt>Occupation:</dt>
                <dd id="occupation-home"></dd>

                <dt>Background Check:</dt>
                <dd id="db-check-home"></dd>


              </dl>
              <div class="d-flex justify-content-center mt-4 mb-2">
                <a href="homedit?art_id=<?php echo $row['id_home'] ?>" class="btn btn__edit-homestay">Edit
                  Homestay</a>
              </div>
            </article>

          </div>


          <div class="col-11 col-sm-10 col-md-7 col-lg-7">

            <!-- // TODO QUICK INFO -->

            <section id="quick-info">
              <h3>Quick Info</h3>

              <div class="ts-quick-info ts-box">

                <!-- // ! SECTION 1 -->

                <div class="row no-gutters quick__info">

                  <!-- // ? BEDROOMS -->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick" data-bg-image="../assets/img/habitacion.png">
                      <h6>Bedrooms</h6>
                      <figure id="room-home"></figure>
                    </div>
                  </div>

                  <!-- // ? EXPERICNE YEARS -->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick" data-bg-image="../assets/img/certificacion.png">
                      <h6>Experience as a Homestay</h6>

                      <figure id="y-experience-home"></figure>
                    </div>
                  </div>

                  <!-- // ?  BACKGORUND LANGUAGE -->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick">
                      <h6>Background Language</h6>
                      <figure id="backl-home"></figure>
                    </div>
                    <img class="language__quick icon__quick-info">
                  </div>

                  <!-- // ? PETS -->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick" data-bg-image="../assets/img/mascotas.png">
                      <h6>Pets</h6>
                      <figure id="pet-home"></figure>
                    </div>
                  </div>


                </div>

                <!-- // ! SECTION 2 -->

                <div class="row no-gutters">

                  <!-- // ? AGE PREFERENCE-->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick">
                      <h6>Age Preference</h6>
                      <figure id="a-preference-home"></figure>
                    </div>
                    <img class="agepre__quick icon__quick-info">
                  </div>

                  <!--// ? GENDER PREFERENCE-->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick" data-bg-image="../assets/img/genero.png">
                      <h6>Gender Preference</h6>
                      <figure id="genpre-home"></figure>
                    </div>
                  </div>

                  <!--// ? FOOD SERVICE-->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick">
                      <h6>Food Service</h6>
                      <figure id="foodservice-home"></figure>
                    </div>
                    <img class="mservice__quick icon__quick-info">
                  </div>

                  <!--// ? SPECIAL DIET-->
                  <div class="col-sm-3">
                    <div class="ts-quick-info__item bb_quick">
                      <h6>Special Diet</h6>
                      <figure id="diet-home"></figure>
                    </div>
                    <img class="speial-diet__quick icon__quick-info">
                  </div>

                </div>

              </div>

            </section>

            <!-- // TODO <================================================================================> -->


          </div>
        </div>

      </section>

      <!-- // TODO SECTION LOCATION -->

      <section id="location-section">

        <div class="row d-flex justify-content-center px-3">
          <div class="col-11 col-sm-10 col-md-7 col-lg-3">

            <!-- // TODO LOCATION -->

            <h3>Location</h3>

            <article class="card p-3" id="location">

              <dl class="ts-description-list__line mb-0">

                <dt><i class="fa fa-home mr-2"></i>Address:</dt>
                <dd class="border-bottom pb-2" id="address-home"></dd>

                <dt><i class="fa fa-building mr-2"></i>City:</dt>
                <dd class="border-bottom pb-2" id="city-home"></dd>

                <dt><i class="fa fa-map-marker mr-2"></i>State:</dt>
                <dd class="border-bottom pb-2" id="state-home"></dd>

                <dt><i class="fa fa-map-marker mr-2"></i>Postal Code:</dt>
                <dd id="pcode-home"></dd>

              </dl>

            </article>

          </div>
          <div class="col-11 col-sm-10 col-md-7 col-lg-7">

            <!-- // TODO MAP -->

            <h3>Map</h3>

            <section id="map-location">

              <div id='map' style="height: 200px;"></div>

              <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
              <script>
              mapboxgl.accessToken =
                'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
              var client = new MapboxClient(mapboxgl.accessToken);


              var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
              var test = client.geocodeForward(address, function(err, data, res) {
                // data is the geocoding result as parsed JSON
                // res is the http response, including: status, headers and entity properties


                var coordinates = data.features[0].center;

                var map = new mapboxgl.Map({
                  container: 'map',
                  style: 'mapbox://styles/mapbox/streets-v10',
                  center: coordinates,
                  zoom: 14
                });

                var ea = document.createElement('div');
                ea.id = 'house';

                new mapboxgl.Marker(ea)
                  .setLngLat(coordinates)
                  .addTo(map);


              });
              </script>

            </section>


            <!-- //TODO <=========================================================================> -->

          </div>
      </section>

      <!-- // TODO SECTION ROOMS INFO -->

      <section id="location-section">

        <div class="row d-flex justify-content-center px-3">
          <div class="col-11 col-sm-10 col-md-7 col-lg-3">
            <!-- // TODO PENDING RESERVATION  -->

            <h3 id="h-pending-reservation">Pending Reservations</h3>

            <article id="article-pending-reservation" data-id-noti="">

              <form action="#" id="pendig-reservations-parent" class="mb-2">

                <template id="pending-reservations">

                  <div id="reservation-pending" class="ts-box active__div mb-3">

                    <h5 id="title__reservation-pending" class="title__reservation"></h5>

                    <a href="#" class="link__profile" id="link__profile-pending">
                      <p id="name__student-pending" class="name__student text_reservation"></p>
                    </a>

                    <div id="div__active__res-pending" class="mb-2">

                      <div id="date__reservation-pending" class="date__reservation w-100">

                        <div id="div__img-pending" class="d-flex justify-content-center div__img p-0 col-md-4">

                          <a href="#"><img src="#" class="img" alt="" id="student-img-pending"></a>

                        </div>

                        <div id="arrive_date-pending" class="arrive_date p-0 col-md-4">
                          <p class="title__date text_reservation" id="arrive-title-pending"></p>
                          <p class="date__content text_reservation" id="arrive-content-pending"></p>
                        </div>

                        <div id="leave_date-pending" class="leave_date p-0 col-md-4">
                          <p class="title__date text_reservation" id="leave-title-pending"></p>
                          <p class="date__content text_reservation" id="leave-content-pending"></p>
                        </div>

                      </div>


                    </div>

                    // TODO INPUTS TO SUBMIT

                    <input type="hidden" name="id_student" id="id_student">
                    <input type="hidden" name="id_not" id="id_not">

                    <div class="buttons__confirm d-flex justify-content-center mt-3" id="div__submit-reservation">

                      <button type="submit" name="cancel-reservation" id="btn__cancel"
                        class="btn btn__cancel-reservation mr-3"></button>
                      <button type="submit" name="confirm-reservation" id="btn__confirm"
                        class="btn btn__confirm-reservation"></button>

                      <p class="text__result-reservation d-none"></p>
                    </div>

                  </div>

                </template>

              </form>

            </article>


            <!-- // TODO ACTIVE RESERVATION -->

            <h3 id="a-active-reservation">Active Reservations</h3>

            <article id="active-reservations-parent">

              <template id="active-reservations">

                <div id="reservation-active" class="ts-box active__div mb-3">

                  <h5 id="title__reservation-active" class="title__reservation"></h5>

                  <a href="#" class="link__profile" id="link__profile-active">
                    <p class="name__student text_reservation" id="name__student-active">
                    </p>
                  </a>

                  <div class="" id="div__active__res-active">

                    <div class="date__reservation" id="date__reservation-active">

                      <div class="d-flex justify-content-center div__img col-md-4 p-0" id="div__img-active">

                        <img src="" class="img" alt="" id="student-img-active">

                      </div>

                      <div id="arrive_date-active" class="arrive_date col-md-4 p-0">
                        <p class="title__date text_reservation" id="arrive-title-active"></p>
                        <p class="date__content text_reservation" id="arrive-content-active"></p>
                      </div>

                      <div id="leave_date-active" class="leave_date col-md-4 p-0">
                        <p class="title__date text_reservation" id="leave-title-active"></p>
                        <p class="date__content text_reservation" id="leave-content-active"></p>
                      </div>

                    </div>

                    <div class="d-flex justify-content-center mt-3 buttons__students" id="div-button-download-pdf">
                      <a href="#" id="link-button-pdf" class="buttons__link link__button-pdf" target="_blank"><span
                          id="span-arrow-download" class="fa fa-arrow-right arrow-pdf arrow__download"></span></a>
                    </div>

                    <div class="d-flex justify-content-center buttons__students" id="div-button-open-report">
                      <a href="#" id="button-open-report" class="buttons__link link__button-report"><span
                          id="span-arrow-download" class="fa fa-arrow-right arrow-report arrow__download"></span></a>
                    </div>


                  </div>

                </div>

              </template>

            </article><br>


            <!-- //TODO <==========================================> -->

            <article id="actions">

              <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top" title="Print">
                <i class="fa fa-print"></i>
              </a>

            </article><br>

            <article id="buttons__section" class="buttons__section">

              <a href="../agents/vouches/Welcomeyourhouse?art_id=<?php echo $row['mail_h'] ?>"
                class="btn btn__pdf btn__brochure-for-students" id="btn__canadian-direct" target="_blank">Download
                Brochure for
                Students</a>
              <a href="../agents/vouches/homestay-resident-suitability-declaration?art_id=<?php echo $id ?>"
                class="btn btn__pdf btn__homestay-declaration" id="btn__homestay-declaration" target="_blank">Homestay
                Resident
                Suitability
                Declaration</a>
              <a href="../agents/vouches/homestay-provider-agreement?art_id=<?php echo $id ?>"
                class="btn btn__pdf btn__homestay-agreement" id="btn__homestay-agreement" target="_blank">Homestay
                Provider
                Agreement 2021-2023</a>
              <a href="../agents/vouches/canadian-direct-deposit-from-for-families?art_id=<?php echo $id ?>"
                class="btn btn__pdf btn__canadian-direct" id="btn__canadian-direct" target="_blank">Canadian Direct
                Deposit
                Set-Up</a>

            </article>
          </div>
          <div class="col-11 col-sm-10 col-md-7 col-lg-7">
            <!-- //TODO BEDROOMS INFO -->

            <h3 id="h-bedroom-information">Bedrooms Information</h3>

            <div class="rooms-list-agents rooms-list-quick-agents mx-auto w-100" id="parent-room-choose">

              <template id="template-bedrooms">

                <div id="reserve" class="py-4 px-2 rounded room-card-container-agents room-card-container-quick-agents"
                  style="height: auto !important; width: max-content">
                  <!-- // TODO ROOM CARD -->
                  <div class=" mx-auto pb-5 bg-white room-card">

                    <!-- // TODO ROOM HEADER -->
                    <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title">
                      <h3 class="p-0 m-0 text-white">Room 1</h3>
                      <p class="m-0 p-0 text-white">CAD$ 14</p>
                    </div>

                    <!-- // TODO ROOM PHOTO -->
                    <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo">

                      <img src="" id="single-image-room" class="d-none">

                      <!-- // TODO CAROUSEL FOTOS -->
                    </div>

                    <!-- // TODO INFO ROOM -->
                    <div class="d-flex mb-4">

                      <!-- // TODO TYPE ROOM -->
                      <div class="col-6 p-0">
                        <div class="feature">
                          <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
                          <span id="type-room" class="px-2">Type</span>
                        </div>
                      </div>

                      <!-- // TODO ROOM FOOD SERVICE -->
                      <div class="col-6 p-0">
                        <div class="feature">
                          <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
                          <span id="food-service" class="px-2">Food Service</span>
                        </div>
                      </div>

                    </div>

                    <!-- // TODO INFO PRICE ROOM -->

                    <div class="d-flex flex-column align-items-center mb-4">
                      <span>Weekly Price CAD$</span>

                      <div class="mt-2 d-flex align-items-center">
                        <span data-price-agent class="mx-2 p-2 price-room">100</span>
                        <span data-price-homestay class="mx-2 p-2 price-room">100</span>
                      </div>
                    </div>

                    <!-- // TODO ROOM RESERVE BUTTON -->
                    <div class="m-0 d-flex align-items-center justify-content-center btn-reserve" id="">
                      <button type="button" id="reserve-btn" class="btn px-3 text-light" disabled data-room="">Reserve
                        Now</button>
                      <div id="div-btn-reserve" class="d-none"></div>
                    </div>

                    <span class="status-detail"></span>
                  </div>

                  <!-- // TODO BEDS CONTAINER -->
                  <div id="beds-container"
                    class="d-flex mx-lg-2 flex-column align-items-center h-auto room-beds-quick-agents mt-3">
                    <!-- // TODO BEDS -->
                    <h3 class="font-weight-light text-muted">Beds</h3>

                    <!-- // TODO IMAGE ONLY ONE BED -->
                    <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none"
                      alt="students">
                  </div>


                </div>

              </template>

            </div>

            <template id="bed-template">
              <!-- // TODO BED -->
              <label for="select-bed" id="bed" class="my-2 rounded d-flex bed-info-agents">

                <div class="d-flex">
                  <div class="w-100 h-50 d-flex align-items-center justify-content-center">
                    <img src="../homestay/icon/cama 64.png" width="25px" height="25px" alt="">
                    <span id="bed-type" class="py-4 px-2 text-dark">Type</span>
                  </div>

                  <div class="d-flex align-items-center justify-content-center">
                    <span id="bed-letter" class="text-white">Bed</span>
                  </div>
                </div>

                <div class="d-flex align-items-center justify-content-center student-info py-3">
                  <img id="student-img" src="../assets/img/student_home.png" rel="noopener" alt="profile">
                </div>
                <a href="#" class="student-details" title="See profile">
                  <span id="student-name" class="d-flex align-items-center justify-content-center">Name Student
                    Large</span>
                  <small id="student-date" class="d-flex align-items-center justify-content-center"
                    title="Arrive - Leaving">
                    06/15/2022 - 08/24/2022
                  </small>
                </a>

                <input id="select-bed" type="checkbox" name="check-bed" hidden>
              </label>


            </template>


          </div>
        </div>
      </section>

      <!-- // TODO SECTION ADDITIONAL INFORMATION -->

      <section id="additional-section">
        <div class="row d-flex justify-content-center px-3">
          <div class="col-11 col-sm-10 col-md-7 col-lg-4">

            <!-- // TODO ADDITIONAL INFORMATION -->
            <h3 id="additional-information-title"> Additional Information </h3>

            <!-- // TODO CONTENT ADDITIONAL INFORMATION -->

            <article class="card p-3">
              <dl class="ts-description-list__line mb-0">

                <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                  <dt class="" for="#">Background:</dt>
                  <dd class="pt-2" id="background-home"></dd>

                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                  <dt class="" for="#">Religion:</dt>
                  <dd class="pt-2" id="religion-home"></dd>

                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                  <dt class="" for="#">Misdemeanor:</dt>
                  <dd class="pt-2" id="misdemeanor-home"></dd>

                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                  <dt class="" for="#">School:</dt>
                  <dd class="pt-2" id="academy-home"></dd>

                </div>

                <div class="d-flex justify-content-between align-items-center pl-2">

                  <dt class="col-9 p-0" for="#">Do you authorize to see your criminal Backgorund Check?:</dt>
                  <dd class="pt-2" id="backgroundCheck-home"></dd>

                </div>


              </dl>
            </article>

          </div>
          <div class=" col-11 col-sm-10 col-md-7 col-lg-5">

            <!-- // TODO HEALTH INFORMATION -->

            <h3 id="health-information-title">Health Information</h3>

            <section class="card p-3">
              <dl class="ts-description-list__line mb-0">

                <dt class="pl-2">Smoker Politics:</dt>
                <dd class="border-bottom pb-2" id="smoker-politics-home"></dd>

                <dt class="pl-2">Allergies:</dt>
                <dd class="border-bottom pb-2" id="allergies-home"></dd>

                <dt class="pl-2">Take a medication:</dt>
                <dd class="border-bottom pb-2" id="medication-home"></dd>

                <dt class="pl-2">Physical or Mental Condition:</dt>
                <dd class="border-bottom pb-2" id="pmcondition-home"></dd>

                <dt class="pl-2">Health Problems:</dt>
                <dd class="border-bottom pb-2" id="health-problems-home"></dd>

              </dl>
            </section>

          </div>
        </div>
      </section>

      <section class="row d-flex justify-content-center px-3">
        <div class="col-11 col-sm-11 col-md-11 col-lg-9">

          <h3 id="family-members-title"> Family Members</h3>

          <section id="family-members-parent">
            <template id="template-member-family">

              <article class="ts-box p-4">
                <h4 class="pl-2 mt-0 mx-0 border-bottom mb-3 pb-2" id="member-name">dawdawdawdawd</h4>

                <div class="row d-flex justify-content-center">

                  <article class="col-md-6" id="article-member-family">

                    <dl class="ts-description-list__line mb-0">

                      <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                        <dt for="#">Date of Birth:</dt>
                        <dd class="pt-2" id="db-member"></dd>

                      </div>

                      <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                        <dt>Gender:</dt>
                        <dd class="pt-2" id="gender"></dd>

                      </div>

                      <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                        <dt>Relation:</dt>
                        <dd class="pt-2" id="relation"></dd>

                      </div>

                      <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                        <dt>Occupation:</dt>
                        <dd class="pt-2" id="occupation"></dd>

                      </div>

                      <div class="d-flex justify-content-between align-items-center border-bottom pl-2">

                        <dt>Background Check:</dt>
                        <dd class="pt-2" id="backgroundCheck"></dd>

                      </div>

                    </dl>

                  </article>

                  <article class="col-md-6 mb-2  d-flex justify-content-center align-items-center"
                    id="div-background-check">

                    <div class="p-0 m-0">

                      <label for="#" class="label_background">Backgorund Check</label>

                      <iframe class="w-100" src="" frameborder="0" id="pdf-member-check"></iframe>

                    </div>

                    <h5 class="d-none" id="no-background">No Bakground Check</h5>

                  </article>

                </div>
              </article>

            </template>

          </section>

        </div>


      </section>

      <section id="section-logs">
        <div class="col-md-10 p-4 mx-auto m-0">
          <h3 id="family-members-title" class="ml-5 pl-2"> Activity Log </h3>
          <div class="ts-box col-md-11 mx-auto table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th class="text-center align-middle col-2">Creator User</th>
                  <th class="text-center align-middle col-3">Title Activity</th>
                  <th class="text-center align-middle col-3">Date</th>
                  <th class="text-center align-middle col-2">Receiving User</th>
                  <th class="text-center align-middle col-2">Student Report</th>
                  <!-- <th class="text-center align-middle">Reason</th> -->
                </tr>
              </thead>
              <tbody class="table-bordered" id="tbody-logs">
                <template id="tbody-log-template">
                  <tr>
                    <td class="text-center align-middle" id="creator-user">Ihomestay</td>
                    <td class="text-center align-middle" id="title-log">Edit User</td>
                    <td class="text-center align-middle" id="date-log">12-20-2022 15:20:00</td>
                    <td class="text-center align-middle" id="receiving-user">Smith House</td>
                    <td class="text-center align-middle" id="student-report">Woody Sherriff</td>
                    <!-- <td class="text-center align-middle" id="reason-log">Woody Sherriff</td> -->
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
        </div>

      </section>

    </main>

    <!-- --------------------- //TODO  MODAL ADD REPORT -------------------------- -->

    <div class="modal__reserve w-100 h-100 position-fixed d-none align-items-center justify-content-center"
      id="modal-add-report">
      <form class="justify-content-center align-items-center modal__div modal__div__report" id="form-send-report"
        action="#" enctype="multipart/form-data">
        <div class=" modal__header modal__header_report d-flex justify-content-between">
          <h3 class="title__modal-report" id="title__modal-report">New Report</h3>
          <button class="btn__close-modal-report" id="btn__close-modal-report">X</button>
        </div>

        <div class="modal__body px-3 py-4">
          <div class="div__reports div__report-title">
            <label for="#" class="label__style">Title Report :</label>
            <select name="title_report" class="custom-select select__title" id="report-title">
              <option hidden="option" disabled selected>-- Select Title --</option>
              <option value="Report Situation">Report Situation</option>
              <option value="Cancel Reservation">Cancel Reservation</option>
            </select>
          </div>

          <hr><br>

          <div class="div__reports div__student-fullname">
            <label for="#" class="label__style">Student to Report :</label>
            <input type="text" class="name_student-report" id="student-fullname" name="name_student-report" readonly>
          </div>

          <hr><br>

          <div class="div__reports div__report-content d-flex justify-content-between align-items-center">
            <div class="col-md-4">
              <div class="img-report-container w-100">
                <input type="file" name="report_image" id="report_image" class="d-none" data-image-upload="true"
                  accept="image/*">
                <label for="report_image" class="div__image_container" id="add_image_report">
                  <img class="image-container" src="../assets/emptys/room-empty.png" alt="">
                </label>
                <!-- <div class="carousel-item active p-0 w-auto" style="position: relative">

                  <img id="preview" class="add-photo">

                  <input type="file" name="report_image" id="file" accept="image/*" onchange="previewImage();"
                    style="display: none">


                  <label for="file" class="photo-add d-flex justify-content-center align-items-center" id="label-i"
                    style="width: 100% !important">
                    <p class="text-center p-0 m-0"> Add Report Image </p>
                  </label>

                  <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none"
                    title="Change Frontage Photo"></label>

                </div> -->
              </div>
            </div>

            <div class="col-md-8">
              <textarea name="description_report" id="" rows="6" class="textarea__report"
                placeholder="Write the content of the report. no special characters."></textarea>
            </div>
          </div>

          <input type="hidden" id="student-id" name="id_student">
        </div>

        <div class="modal__footer modal__footer_report d-flex justify-content-end">
          <button class="btn btn__send-report" type="submit" id="send-report">Send Report</button>
        </div>
      </form>
    </div>




    <!-- --------------------- //TODO MODAL ADD NEW RESERVATION -------------------------- -->

    <div class="modal__reserve w-100 h-100 position-fixed d-none align-items-center justify-content-center"
      id="modal__reservation">
      <article class="justify-content-center align-items-center modal__div modal__div__reservation">
        <form action="#" id="form__reservation">
          <div class="modal__header d-flex justify-content-between">
            <h4 class="title__modal-header">Choose the Student</h4>
            <button type="button" id="btn__close-modal" class="btn__close-modal">X</button>
          </div>

          <div class="modal__body">
            <article id="student_list" class="student__list">

              <!-- FIELD SEARCH STUDENTS -->

              <div class="div__search-input">
                <label for="search__input" class="fa fa-search label__search"></label>
                <input type="text" id="search__input" class="search__input" placeholder="Type your Search">
              </div>

              <!-- STUDENTS LIST RESERVATION -->

              <ul id="list__student" class="ul__list-students">
                <li
                  class="label__select-student header__list-students d-flex justify-content-evenly align-items-center pb-0">
                  <div class="h-0 text-center col-2 col-sm-2 col-md-2 col-lg-2 p-0"
                    style="width: 60px; max-width: 60px;">
                    <p class="title__list-student">Photo</p>
                  </div>
                  <div class="text-center col-5 col-sm-5 col-md-5 col-lg-5 p-0">
                    <p class="title__list-student">Names</p>
                  </div>
                  <div class="text-center col-4 col-sm-4 col-md-4 col-lg-4 p-0">
                    <p class="title__list-student">Date Arrive / Date Leave</p>
                  </div>
                  <div class="col-2 col-sm-2 col-md-2 col-lg-2 p-0"></div>
                </li>
                <template id="student__list-ul">
                  <li class="students__details">
                    <label for="" id="label__select-student"
                      class="label__select-student d-flex justify-content-evenly align-items-center">
                      <div class="div_img_student col-2 col-sm-2 col-md-2 col-lg-2 p-0">
                        <img src="" alt="" class="img__student">
                      </div>
                      <div class="div_names_students col-5 col-sm-5 col-md-5 col-lg-5 p-0">
                        <p class="names__students"></p>
                      </div>
                      <div class="div_dates_students text-center col-4 col-sm-4 col-md-4 col-lg-4 p-0">
                        <p class="dates__students" id="date-arrive"></p>
                        <p class="dates__students" id="date-leave"></p>
                      </div>
                      <div class="div_checkbox text-center col-2 col-sm-2 col-md-2 col-lg-2 p-0 ">
                        <input type="checkbox" name="" id="">
                      </div>
                    </label>
                  </li>
                </template>
              </ul>
              <div class="d-none div__no_student justify-content-center align-items-center" id="div__no_student">
                <img class="img__no_student" src="../assets/emptys/calendar-empty-no-border.png" alt="">
                <h3 class="text__no_student">There are no Students for the Dates available for this bed.</h3>
              </div>

              <ul id="list__student-search" class="d-none">
              </ul>

              <input type="hidden" id="id__student-reservation" name="id_student">
              <input type="hidden" value="<?php echo $row['id_home'] ?>" name="id_h">
              <input type="hidden" value="" id="button-room" name="id_room">

            </article>
          </div>

          <div class="modal__footer d-flex justify-content-end" id="modal__footer_resevation">
            <div id="btn__success">
              <button type="submit" id="btn__submit-reservation" class="btn btn__add-reservation">Add
                Reservation</button>
            </div>
          </div>
        </form>
      </article>
    </div>


    <div id="house_much" style="display: none;">
      <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
      <div class="content_popup">

        <div class="content">
          <div class="col-2">
            <img class="img-stu" src="../images/icon.png" />
          </div>
          <div class="col-10" id="contentp">

          </div>
        </div>
      </div>

    </div>
    <div id="house_ass" style="display: none;">
      <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
      <div class="content_popup">

        <div class="content">
          <div class="col-2">
            <img class="img-stu" src="../images/icon.png" />
          </div>
          <div class="col-9" id="assh">

          </div>
        </div>
      </div>

    </div>

    <script>
    function previewImage() {
      var file = document.getElementById("file").files;

      if (file.length > 0) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
          document.getElementById("preview").setAttribute("src", event.target.result);
        };

        fileReader.readAsDataURL(file[0]);

        var label_1 = document.getElementById("label-i");
        var label_1_1 = document.getElementById("label-i-i");

        label_1.classList.replace('d-flex', 'd-none');
        label_1_1.style.display = 'inline';

      }

    }
    </script>

    <!-- // TODO FOOTER ===============================================================-->
    <?php require 'footer.php'; ?>


  </div>

  <!-- // TODO SCRIPTS -->
  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.8"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.8"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.8"></script>
  <script src="../assets/js/jquery.magnific-popup.min.js?ver=1.0.8"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.8"></script>
  <script src="../assets/js/custom.js?ver=1.0.8"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.8"></script>
  <script src="assets/js/details.js?ver=1.0.8"></script>
  <script src="../assets/js/owl.carousel.min.js?ver=1.0.8"></script>
  <script>
  $('.owl-carousel').owlCarousel({
    loop: true,
    center: true,
    autoWidth: true,
  })
  </script>

</body>

</html>