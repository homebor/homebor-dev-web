<?php
    require '../../xeon.php';
    session_start();
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    if ($row['usert'] != 'Agent') {
        
      header("location: ../logout.php");   

 } 

    $query2="SELECT * FROM manager WHERE mail = '$usuario'";
    $resultado2=$link->query($query2);

    $row2=$resultado2->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/simplebar.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/list_delete_user.css?ver=1.1.1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Administartor Panel</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
    integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
</head>

<body id="body">

  <!--HEADER ===============================================================-->
  <?php 
    include 'header.php';
?><br><br>
  <main id="ts-main">

    <!--PAGE TITLE
            =========================================================================================================-->


    <section id="page-title">
      <div class="container">
        <div class="row py-4">
          <div class="col-lg-10">
            <div class="">
              <br>
              <h1 style="color: #232159">Users to Delete </h1>
              <!-- Echo username -->
              <?php
        $usuario = $_SESSION['username']; 
    ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <hr>
    <div class="container" style="max-width: 1300px !important">
      <br />
      <div class="card">
        <div class="card-body">
          <div class="table-responsive" id="dynamic_content">
          </div>
        </div>
      </div>
    </div>



    </section>

</body>
</main>
</body>


<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="assets/js/list_delete_user.js?ver=1.1.1"></script>

</body>

</html>