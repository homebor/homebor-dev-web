// TODO IMPORTACIONES
import MAP from "./map_assigment.js?ver=1.0.26";
import studentData from "./student_data.js?ver=1.0.26";
import homestayData from "./houses_data.js?ver=1.0.26";
import { defaultFilters } from "./houses_data.js?ver=1.0.26";
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_STUDENT = searchParams.get("art_id");
// ? INSTANCIAS
const _Messages = new Messages();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await studentData();
  await homestayData();
  await defaultFilters();

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }

  // ? BACK TO PANEL
  if (e.target.matches(".back_panel")) window.location.href = "directory_students";

  // ? SEND THE ADDRESS OF HOUSES SELECTED TO THE MAP
  if (e.target.id === "next-step") {
    const allDataHouses = {};
    document.querySelectorAll("#all-houses-selected > div").forEach((house, i) => {
      allDataHouses[`house${i + 1}`] = {
        id: house.querySelector("[data-house-profile]").dataset.houseProfile,
        name: house.querySelector("label").textContent,
        address: house.querySelector("[data-house-direction]").textContent,
        photo: house.querySelector("img").src,
      };
    });

    MAP(allDataHouses);
  }
});

// ! KEYUP
D.addEventListener("keyup", (e) => {});

// ! CHANGE
D.addEventListener("change", (e) => {});

// ! ERROR
D.querySelector("#student").addEventListener("error", (e) => {
  e.target.src = "../assets/emptys/profile-student-empty.png";
});
// D.querySelector("[data-house-image]").addEventListener("error", (e) => {
//   e.target.src = "../assets/emptys/frontage-empty.png";
// });
