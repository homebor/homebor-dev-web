"use strict";
// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const SearchParams = new URLSearchParams(W.location.search);
const $_HOMESTAY = {
  // ? OBTENER EL PARAMETRO art_id DE LA URL
  ID_HOME: SearchParams.get("art_id"),
  // ? TEMPLATE ROOM
  $reserveTemplate: D.getElementById("reserve-template").content.cloneNode(true),
  // ? TEMPLATE BED
  $bedTemplate: D.getElementById("bed-template").content.cloneNode(true),
  // ? ALL ROOMS
  $allReserves: D.getElementById("all-rooms"),
};
// ? INSTANCIAS
const _Messages = new Messages();

// ? DATA SAVED
const dataSaved = [];

// TODO FUNCIONES
export default class Rooms {
  constructor(HOMESTAY = $_HOMESTAY) {
    // ? HOMESTAY
    this.ID = HOMESTAY.ID_HOME;
    // ? HOMESTAY ROOMS
    this.$reserveTemplate = HOMESTAY.$reserveTemplate;
    this.$bedTemplate = HOMESTAY.$bedTemplate;
    this.$allReserves = HOMESTAY.$allReserves;
  }
  // ? REQUERIR DATOS DE LAS HABITACIONES E INSERTARLAS
  async homestayRooms() {
    try {
      // * OBTENIENDO DATOS
      const DATA = new FormData();
      DATA.set("homestay_id", this.ID);

      const OPTIONS = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: DATA,
      };
      const REQUEST = await axios("../helpers/homestay/rooms_data.php", OPTIONS);
      const RESPONSE = await REQUEST.data;

      // ** FRAGMENTO HTML A INSERTAR
      const $fragment = D.createDocumentFragment();

      // * AGREGANDO RESERVAS EXISTENTES
      for (const key in RESPONSE) {
        // ** ¡DATA! **
        const ROOM = RESPONSE[key];

        // ! VALIDAR A QUE SI TODOS LOS DATE ESTAN EN DISABLED NO MOSTRAR
        let valid = true;

        for (const key in ROOM) {
          if (key !== "roomFeatures") continue;
          if (
            !ROOM[key].type ||
            ROOM[key].type === "Disabled" ||
            ROOM[key].type === "NULL" ||
            ROOM[key].type === "null"
          )
            valid = false;
        }

        if (!valid) continue;

        // ** TEMPLATES
        const $newReserve = this.$reserveTemplate.firstElementChild.cloneNode(true);
        $newReserve.dataset.room = ROOM.roomTitle.name;
        const $newBed = this.$bedTemplate.cloneNode(true);

        // ** TITLE DEL DOCUMENTO
        D.getElementsByTagName("title")[0].textContent = "Rooms " + ROOM.homestayFullName;

        // ** RELLENANDO HEADER
        this.roomHeader(ROOM.roomTitle, $newReserve);

        // ** RELLENANDO PHOTO
        const image1 = ROOM.roomImage.image1;
        const image2 = ROOM.roomImage.image2;
        const image3 = ROOM.roomImage.image3;

        this.roomPhoto(ROOM, [image1, image2, image3], $newReserve);

        // ** RELLENANDO FEATURES
        this.roomFeatures(ROOM.roomFeatures, $newReserve);

        // ** RELLENAR DETALLES DE CAMAS
        this.roomBedsInfo(ROOM.roomReservesInfo, ROOM.roomTitle, $newBed, $newReserve);

        // ** AGREGAR RESERVA
        $fragment.appendChild($newReserve);
      }

      // * AGREGAR TODAS LAS RESERVAS
      this.$allReserves.appendChild($fragment);
    } catch (error) {
      console.error("Ha ocurrido un error:", error);
    } finally {
      D.querySelector("#total-rooms").value = D.querySelectorAll("#reserve").length;
      if (D.querySelectorAll("#reserve").length === 8) {
        D.querySelector("#add-new-room").parentElement.remove();
      }
    }
  }

  /** // ? RELLENAR HEADER DE LA HABITACIÓN
   * @param {Object} roomTitle Nombre de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomHeader(roomTitle, reserveTemplate) {
    const $roomHeader = reserveTemplate.querySelector("#reserve-header");
    const $roomName = reserveTemplate.querySelector("#reserve-header h3");
    const $roomPrice = reserveTemplate.querySelector("#reserve-header p");
    const $homestayPrice = reserveTemplate.querySelector("#homestay-price");
    const $providerPrice = reserveTemplate.querySelector("#provider-price");

    $roomHeader.style.background = roomTitle.color;
    $roomName.textContent = roomTitle.name;
    $roomPrice.textContent = `CAD$ ${parseInt(roomTitle.priceHomestay) + parseInt(roomTitle.priceAgent)}`;

    $homestayPrice.value = roomTitle.priceHomestay;
    $providerPrice.value = roomTitle.priceAgent;
  }

  /** // ? FILL IMAGES OF ROOM
   * @param {Object} roomData Datos de la habitacion
   * @param {Array} images Imagenes a insertar
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomPhoto(roomData, images, reserveTemplate) {
    const $fragment = D.createDocumentFragment();

    const $carouselContainer = D.createElement("div");
    $carouselContainer.id = roomData.roomTitle.name.replace(" ", "-");
    $carouselContainer.className = "w-100 carousel slide";
    $carouselContainer.dataset.ride = "carousel";

    const $indicators = D.createElement("ol");
    $indicators.className = "carousel-indicators";

    const $indicator = D.createElement("li");
    $indicator.dataset.target = `#${$carouselContainer.id}`;

    const $imagesContainer = D.createElement("div");
    $imagesContainer.className = "w-100 carousel-inner";

    const $btnPrevious = D.createElement("a");
    $btnPrevious.className = "carousel-control-prev";
    $btnPrevious.dataset.slide = "prev";
    $btnPrevious.setAttribute("role", "button");
    $btnPrevious.href = `#${$carouselContainer.id}`;

    const $btnNext = D.createElement("a");
    $btnNext.className = "carousel-control-next";
    $btnNext.dataset.slide = "next";
    $btnNext.setAttribute("role", "button");
    $btnNext.href = `#${$carouselContainer.id}`;

    images.forEach((image, i) => {
      $indicator.dataset.slideTo = i;
      $indicators.appendChild($indicator.cloneNode(true));
      $indicators.firstElementChild.className = "active";

      const $imageItem = D.createElement("div");
      $imageItem.className = "w-100 carousel-item";
      const $image = D.createElement("img");
      const $photoAdd = D.createElement("label");
      const $photoAddInput = D.createElement("input");

      $photoAdd.setAttribute("for", `${roomData.roomTitle.name} Image ${i + 1}`);
      $photoAdd.className = "photo-add";

      $photoAddInput.id = `${roomData.roomTitle.name} Image ${i + 1}`;
      !image || image === "NULL" || image === "null"
        ? ($photoAdd.innerHTML = `<img src="../assets/emptys/room-empty.png" width="100px" height="100px" alt="">`)
        : ($photoAdd.innerHTML = `<img src="../../${image}" alt="">`);
      $photoAddInput.value = "";
      $photoAddInput.hidden = true;
      $photoAddInput.type = "file";

      $imageItem.appendChild($photoAdd);
      $imageItem.appendChild($photoAddInput);

      $imagesContainer.appendChild($imageItem);
      $imagesContainer.firstElementChild.classList.add("active");

      $btnPrevious.innerHTML = `<span class="carousel-control-prev-icon mr-5" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>`;
      $btnNext.innerHTML = `<span class="carousel-control-next-icon ml-5" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>`;

      $carouselContainer.appendChild($indicators);
      $carouselContainer.appendChild($imagesContainer);
      $carouselContainer.appendChild($btnPrevious);
      $carouselContainer.appendChild($btnNext);

      $fragment.appendChild($carouselContainer);
    });

    if (reserveTemplate.querySelector("#reserve-photo").children[2]) {
      reserveTemplate.querySelector("#reserve-photo").children[2].remove();
      reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
    } else reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
  }

  /** // ? FILL FEATURES OF ROOM (TYPE ROOM, FOOD SERVICE)
   * @param {Object} features Caracteristicas de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomFeatures(features, reserveTemplate) {
    const $typeRoom = reserveTemplate.querySelector("#type-room");
    const $roomFoodService = reserveTemplate.querySelector("#food-service");

    $typeRoom.value = features.type;
    $roomFoodService.value = features.food;
  }

  /** // ? FILL THE BEDS AND INSERT TO ROOM
   * @param {Object} roomBeds Informacion de las camas de la habitacion
   * @param {Object} roomTitle Nombre de la habitacion
   * @param {Element} bedTemplate Plantilla de las camas de la habitacion
   * @param {Element} reserveTemplate Plantilla de la habitacion
   */
  roomBedsInfo(roomBeds, roomTitle, bedTemplate, reserveTemplate) {
    const ALL_BEDS = Object.values(roomBeds.allBeds) || [];
    dataSaved.push([roomTitle.name, roomBeds.allBedsReserved]);

    const $fragment = D.createDocumentFragment();
    const $bedsContainer = reserveTemplate.querySelector("#beds-container");

    if (ALL_BEDS && ALL_BEDS instanceof Array) {
      ALL_BEDS.forEach((bed) => {
        const ROOM = bed[0];
        const TYPE_ROOM = bed[4];
        let TYPE_BED;
        bed[1] === "Bunk-bed" || bed[1] === "Bunker" ? (TYPE_BED = "Bunk") : (TYPE_BED = bed[1]);

        let BED;
        if (bed[2] === "A") BED = "Bed 1";
        if (bed[2] === "B") BED = "Bed 2";
        if (bed[2] === "C") BED = "Bed 3";
        const STATUS = bed[3];

        bedTemplate.querySelector("#bed-type").name = BED;
        bedTemplate.querySelector("#bed-letter").textContent = BED;

        const statusBed = (classBed, selectStatus, bedAdditional = false) => {
          bedTemplate.querySelector("#bed").classList.add(classBed);
          bedTemplate.querySelector("#bed select").disabled = selectStatus;

          bedTemplate.querySelectorAll(`#bed select option`).forEach((option) => {
            if (option.value === TYPE_BED) option.setAttribute("selected", "");
            else option.removeAttribute("selected");

            if (option.value === "") option.setAttribute("selected", "");
          });

          if (bedAdditional && BED === "Bed 3") {
            bedTemplate.querySelector("#bed").classList.add("more");
            bedTemplate.querySelector("#bed button").classList.add("d-none");
          }
        };

        const disableBed = () => {
          if (TYPE_ROOM === "Single" || TYPE_ROOM === "Executive") {
            bedTemplate.querySelector("#bed").classList.remove("added");
            bedTemplate.querySelector("#bed select").disabled = true;
            const $options = bedTemplate.querySelectorAll(`#bed select option`);
            $options.forEach((option) => (option.value === "" ? option.setAttribute("selected", "") : {}));
          } else if (TYPE_ROOM === "Share") {
            bedTemplate.querySelectorAll(`#bed select option`).forEach((option) => {
              option.removeAttribute("selected");
              if (option.value === "") option.setAttribute("selected", "");
            });
          }
        };

        if (TYPE_ROOM === "Single" || TYPE_ROOM === "Executive") {
          STATUS && STATUS !== "Disabled" && STATUS !== "NULL" ? statusBed("added", false) : disableBed();
        }

        if (TYPE_ROOM === "Share") {
          STATUS && STATUS !== "Disabled" && STATUS !== "NULL" ? statusBed("added", false, true) : disableBed();
        }

        let $clone = D.importNode(bedTemplate, true);

        $fragment.appendChild($clone);
      });

      // * AGREGAR CAMA
      $bedsContainer.appendChild($fragment);
    }
  }

  /** // ? SAVE INFORMATION OF BEDROOMS
   * @param {Array} room Todas las habitacion a guardar
   * @param {Number|String} id ID de la casa a editar
   */
  saveBedrooms(room, id) {
    room.forEach(async (info, i) => {
      try {
        const $roomNumber = i + 1;
        const $images = info.querySelectorAll(".carousel-item img");
        const $imageInput = info.querySelectorAll(".carousel-item input");
        const $selects = info.querySelectorAll("select");
        const $priceHomestay = info.querySelector("#homestay-price");
        const $priceAgent = info.querySelector("#provider-price");

        const images = [];
        const readImages = () => $imageInput.forEach((input) => images.push(input.files[0]));
        await readImages();

        const roomData = new FormData();
        roomData.set("homestay_id", id);
        roomData.set("roomNumber", $roomNumber);

        if ($images[0]) roomData.set("image1", images[0]);
        if ($images[1]) roomData.set("image2", images[1]);
        if ($images[2]) roomData.set("image3", images[2]);

        if ($selects && $selects[0].value) roomData.set("typeRoom", $selects[0].value);
        if ($selects && $selects[1].value) roomData.set("food", $selects[1].value);

        if ($selects[2] && $selects[2].value) roomData.set("bed1", $selects[2].value);
        if ($selects[3] && $selects[3].value) roomData.set("bed2", $selects[3].value);
        if ($selects[4] && $selects[4].value) roomData.set("bed3", $selects[4].value);

        if ($priceHomestay && $priceHomestay.value) roomData.set("priceHomestay", $priceHomestay.value);
        if ($priceAgent && $priceAgent.value) roomData.set("priceAgent", $priceAgent.value);

        const options = {
          method: "POST",
          headers: { "Content-type": "application/json; charset=utf-8" },
          data: roomData,
        };

        const res = await axios("../helpers/homestay/edit_rooms.php", options);
        const data = await res.data;

        if (D.querySelector(`[data-room="${data[0]}"]`)) {
          D.querySelector("#modal-dynamic").classList.add("show");
          D.querySelector("#modal-dynamic").style.opacity = "0";

          D.querySelector(".loading-saving").style.opacity = "0";
          D.querySelector("#modal-dynamic").style.opacity = "1";

          setTimeout(() => {
            D.querySelector(".loading-saving").style.opacity = "1";
            D.querySelector(".loading-saving").classList.add("d-none");
          }, 500);

          D.querySelector(`[data-room="${data[0]}"]`).classList.add("required-fields");

          D.querySelector("#modal-dynamic").innerHTML = `
          <div class="d-flex flex-column text-center justify-content-center save-changes">
            <h4>You haven't finished the configuration of your rooms, do you want <br> to continue the process without that information?</h4>
            <div class="d-flex align-items-center justify-content-center">
              <button id="save-changes-yes" disabled class="btn w-25 mx-2 enable-room">Yes</button>
              <button id="save-changes-no" disabled class="btn w-25 mx-2 disable-room">No</button>
            </div>
          </div>
          `;

          D.querySelector("#save-changes-no").addEventListener("click", (e) => {
            D.querySelector("#modal-dynamic").classList.remove("show");
            D.querySelector("#modal-dynamic").style.cssText = "";
            D.querySelector(`[data-room="${data[0]}"]`).scrollIntoView({ block: "start", behavior: "smooth" });
          });

          D.querySelector("#save-changes-yes").addEventListener("click", (e) => {
            D.querySelector(".loading-saving").classList.remove("d-none");
            D.querySelector("#modal-dynamic").classList.remove("show");

            D.querySelector("[data-house-name]").dataset.houseName = D.querySelector("#house-name").value + " ";
            D.querySelector(".loading-content").classList.add("d-none");
            D.querySelector(".loaded-content").classList.remove("d-none");
            setTimeout(() => (window.location.href = "edit_propertie"), 1000);
          });
        }

        // * ERROR EN LA PROGRAMACION, CLIENTE, RED O SERVIDOR
        if (data === "error") {
          _Messages.showMessage("An error occurred while saving the rooms.", 4, false, ".rooms-container");
        }

        if (data[1] === "no-type-room")
          // * ERRORES MANEJADOS
          D.querySelector(`[data-room="${data[0]}"] #type-room`).classList.add("invalid");
        else if (data[1] === "no-shared-bed") {
          D.querySelectorAll(`[data-room="${data[0]}"] #bed`).forEach((bed, i) => {
            if (i == 2) return;
            bed.classList.add("no-bed");
            bed.dataset.noBed = "Select type bed";
          });
        } else if (data[1] === "no-bed") D.querySelector(`[data-room="${data[0]}"] #bed`).classList.add("no-bed");
        else if (data[1] === "price-homestay-invalid") {
          D.querySelector(`[data-room="${data[0]}"] #homestay-price`).classList.add("invalid");
        } else if (data[1] === "price-agent-invalid") {
          D.querySelector(`[data-room="${data[0]}"] #provider-price`).classList.add("invalid");
        } // * MENSAJE DE GUARDADO SATISFACTORIO
        else if (data.status === true) {
          D.querySelector(`[data-room="${data.room}"]`).classList.add("saved");
          setTimeout(() => D.querySelector(`[data-room="${data.room}"]`).classList.remove("saved"), 4000);
        }
      } catch (error) {
        console.error("Ha ocurrido un error al guardar los cambios!:", error);
      }
    });
  }

  /** // ? ADD NEW ROOM
   * @param {String} reference Evento que ejecuta la funcion
   */
  addNewRoom(reference) {
    const $allRooms = D.getElementById("all-rooms");
    const $template = D.getElementById("reserve-template").content;

    let $lastRoomName;
    if ($allRooms.querySelector("#reserve-header")) {
      $lastRoomName = parseInt($allRooms.lastElementChild.querySelector("#reserve-header h3").textContent.slice(-1));
    } else $lastRoomName = 0;

    const $lastRoom = D.querySelector(`[data-room="Room ${$lastRoomName}"]`);

    const ADD_ROOM = () => {
      const ROOM_PREV = $lastRoomName + 1;
      let roomColor;

      if (ROOM_PREV > 0 && ROOM_PREV <= 8) {
        try {
          if (ROOM_PREV == 1) roomColor = "#232159";
          if (ROOM_PREV == 2) roomColor = "#982A72";
          if (ROOM_PREV == 3) roomColor = "#394893";
          if (ROOM_PREV == 4) roomColor = "#A54483";
          if (ROOM_PREV == 5) roomColor = "#5D418D";
          if (ROOM_PREV == 6) roomColor = "#392B84";
          if (ROOM_PREV == 7) roomColor = "#B15391";
          if (ROOM_PREV == 8) roomColor = "#4F177D";

          $template.querySelector("#reserve").dataset.room = "Room " + ROOM_PREV;
          $template.querySelector("#reserve-header").style.background = roomColor;
          $template.querySelector("#reserve-header h3").textContent = "Room " + ROOM_PREV;
          this.roomPhoto({ roomTitle: { name: "Room " + ROOM_PREV } }, [false, false, false], $template);

          const $bedHTML = `
            <div id="bed" class="mb-4 my-lg-auto rounded d-flex justify-content-center bed-info-homestay"
            data-no-bed="Select type bed">

            <div class="my-4 d-flex">
              <div class="w-100 h-50 d-flex align-items-center justify-content-center">
                <img src="../assets/icon/cama 64.png" width="25px" height="25px" alt="">
                <select name="bed-type" id="bed-type" class="px-2" disabled required>
                  <option value="NULL">Type bed</option>
                  <option value="Twin">Twin</option>
                  <option value="Bunk">Bunk</option>
                  <option value="Double">Double</option>
                </select>
              </div>

              <div class="d-flex align-items-center justify-content-center">
                <span id="bed-letter" class="text-white">Bed</span>
              </div>
            </div>

            <button id="add-bed" class="my-3 d-none" title="Add bed">+</button>
          </div>
          `;

          $template.querySelector("#beds-container").innerHTML = `${$bedHTML}${$bedHTML}${$bedHTML}`;

          $allRooms.appendChild(D.importNode($template, true));

          return;
        } catch (error) {
          console.error("Ha ocurrido un error: " + error);
        } finally {
          if (reference !== "toInput") {
            D.querySelector("#total-rooms").value = $allRooms.querySelectorAll("#reserve").length;
            if ($allRooms.querySelectorAll("#reserve").length === 8) {
              D.querySelector("#add-new-room").parentElement.remove();
            }
          }
        }
      }
    };

    if (!$lastRoom) ADD_ROOM();
    else if (
      $lastRoom.querySelector("#type-room").value === "NULL" ||
      $lastRoom.querySelector("#bed-type").value === "NULL" ||
      (!$lastRoom.querySelector("#homestay-price").value && !$lastRoom.querySelector("#provider-price").value) ||
      ($lastRoom.querySelector("#homestay-price").value <= 0 && $lastRoom.querySelector("#provider-price").value <= 0)
    ) {
      _Messages.showMessage(
        "Please complete the information of the previous room to add more.",
        4,
        false,
        `[data-room="Room ${$lastRoomName}"]`
      );
    } else ADD_ROOM();
  }

  /** // ? VERIFY, ENABLE OR DISABLE ROOM
   * @param {Element} btn Boton que origino el evento
   * @param {String|Number} status Valor a colocar en el status de la habitacion
   */
  async statusRoom(btn, status) {
    // * Si existe btn y status habilitar o deshabilitar habitacion
    if (btn && status) {
      try {
        const roomNumber = btn.parentElement.parentElement.parentElement
          .querySelector("#reserve-header h3")
          .textContent.slice(-1);

        const roomData = new FormData();
        roomData.set("homestay_id", this.ID);
        roomData.set("roomNumber", roomNumber);
        roomData.set("roomStatus", status);

        const options = {
          method: "POST",
          headers: { "Content-type": "application/json; charset=utf-8" },
          data: roomData,
        };

        const res = await axios("../helpers/homestay/disable_room.php", options);
        const data = await res.data;

        this.statusRoom();
      } catch (error) {
        console.error("Ha ocurrido un error: " + error);
      }
    }
    // * Caso contrario verificar el status de las habitaciones
    else {
      const roomData = new FormData();
      roomData.set("homestay_id", this.ID);
      roomData.set("verify", true);

      const options = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("../helpers/homestay/disable_room.php", options);
      const data = await res.data;

      let i = 1;
      for (const key in data) {
        if (key.includes("date")) {
          const roomName = "Room " + i;
          const date1 = data[`date${i}`];
          const date2 = data[`date${i}_2`];
          const date3 = data[`date${i}_3`];
          const $allRooms = D.querySelectorAll(`[data-room]`);

          if (
            (date1 === "Disabled" || date1 === "NULL") &&
            (date2 === "Disabled" || date2 === "NULL") &&
            (date3 === "Disabled" || date3 === "NULL")
          ) {
            $allRooms.forEach((room) => {
              if (room.dataset.room === roomName) {
                room.querySelector("#disable-room").classList.add("d-none");
                room.querySelector("#enable-room").classList.remove("d-none");
              }
            });
          } else {
            $allRooms.forEach((room) => {
              if (room.dataset.room === roomName) {
                room.querySelector("#disable-room").classList.remove("d-none");
                room.querySelector("#enable-room").classList.add("d-none");
              }
            });
          }

          if (i === 8) break;
          else i++;
        }
      }
    }
  }

  // ! TODO STATIC
  /** // ? CHANGE TYPE OF ROOM AND MODIFY IN THIS REGARD
   * @param {Element} typeRoom Elemento de Type Room de la habitacion
   */
  static editBeds(typeRoom) {
    const $bedsContainer = typeRoom.parentElement.parentElement.parentElement.parentElement.parentElement;
    const selectValue = typeRoom.value;

    if (selectValue === "Single" || selectValue === "Executive") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        if (i === 1 || i === 2) {
          bed.classList.remove("added", "more");
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = true));
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = "NULL"));
        }
      });
    } else if (selectValue === "Share") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = "NULL"));
        if (i === 2) bed.classList.remove("more");
      });
    } else {
      $bedsContainer.querySelectorAll("#bed").forEach((bed) => bed.classList.remove("added"));
      $bedsContainer.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = "false"));
    }
  }

  /** // ? ADD THE BED BY GIVING THE MORE BUTTON (+)
   * @param {Element} btn Elemento select a reemplazar por el icono +
   */
  static addBed(btn) {
    const $bed = btn.parentElement;
    const $select = btn.parentElement.querySelector("select");

    $bed.classList.add("more");
    $select.disabled = false;
  }
}

/** // ? CONFIRM CHANGE OF ROOM STATUS
 * @param {String} modal Selector de elemente HTML del modal a mostrar
 * @param {String} message Mensaje a mostrar al usuario
 * @param {String|Number} status Valor a colocar en el status de la habitacion
 * @param {Element} btn Elemento que genero el evento
 */
const statusMessage = (modal, message, status, btn) => {
  let $html = `
      <div class="d-flex flex-column align-center justify-content-center confirm-status">
        <h4>Are you sure you want to ${message} the room?</h4>
        <div class="d-flex align-items-center justify-content-center">
          <button id="enable-room-yes" class="btn w-25 mx-2 enable-room">Yes</button>
          <button id="disable-room-no" class="btn w-25 mx-2 disable-room">No</button>
        </div>
      </div>
      `;

  dataSaved.forEach((data) => {
    const $currentRoom = btn.parentElement.parentElement.parentElement.parentElement.dataset.room;
    if (data[0] === $currentRoom) {
      if (data[1] && data[1].length > 0) {
        $html = `
          <div class="d-flex flex-column align-center justify-content-center confirm-status">
            <h4>There are active or pending reservations, you cannot disable this room</h4>
            <div class="d-flex align-items-center justify-content-center">
            <button id="disable-room-no" class="btn w-25 mx-2 bg-secondary text-white disable-room">Close</button>
            </div>
          </div>
          `;
      } else return;
    }
  });

  D.querySelector(modal).classList.add("show");
  D.querySelector(modal).innerHTML = $html;

  if (D.getElementById("enable-room-yes")) {
    D.getElementById("enable-room-yes").onclick = () => {
      new Rooms().statusRoom(btn, status);
      D.querySelector(modal).classList.remove("show");
    };
  }

  if (D.getElementById("disable-room-no")) {
    D.getElementById("disable-room-no").onclick = () => D.querySelector(modal).classList.remove("show");
  }
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", (e) => {
  _Messages.quitMessage();
});
// ! CLICK
D.addEventListener("click", (e) => {
  // ? AGREGAR CAMA
  if (e.target.matches("#add-bed")) Rooms.addBed(e.target);
  // ? EDITAR FOTO DE HABITACION
  if (e.target.id === "edit-photo-btn") e.target.parentElement.querySelector(".carousel-item.active input").click();
  // ? AÑADIR OTRA HABITACION
  if (e.target.id === "add-new-room") new Rooms().addNewRoom();
  // ? HABILITAR HABITACION
  if (e.target.id === "enable-room") statusMessage("#modal-dynamic", "enable", "0", e.target);
  // ? DESHABILITAR HABITACION
  if (e.target.id === "disable-room") statusMessage("#modal-dynamic", "disable", "-1", e.target);
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? EDIT BEDS
  if (e.target.matches("#type-room")) Rooms.editBeds(e.target);
  // ? SELECT OPTION WITH THE VALUE OF SELECT
  if (e.target.matches("#bed-type")) {
    e.target.querySelectorAll("option").forEach((option) => {
      option.value === e.target.value ? option.setAttribute("selected", "") : option.removeAttribute("selected");
    });
    e.target.parentElement.parentElement.parentElement.classList.remove("no-bed");
    e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove(
      "required-fields"
    );
  }
});
