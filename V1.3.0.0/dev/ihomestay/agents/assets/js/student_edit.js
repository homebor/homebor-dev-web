const D = document;
const W = window;

const $nearToSchool = D.querySelector("#accomm-near").value;
const $accomUrgent = D.querySelector("#accomm-urgent").value;
const $summerFee = D.querySelector("#summer-fee").value;
const $minorFee = D.querySelector("#minor-fee").value;
const $accomGuardianship = D.querySelector("#accomm-guardianship").value;

let totalPrice = 0;
let priceNearSchool = 0;
let priceAccomUrgent = 0;
let priceSummerFee = 0;
let priceMinorFee = 0;
let priceAccomGuardianship = 0;

// TODO NEAR TO SCHOOL
if ($nearToSchool === "yes") {
  priceNearSchool = 35 * 5 * 4;
  D.querySelector("#container-price-a-near").classList.remove("d-none");
} else {
  D.querySelector("#container-price-a-near").classList.add("d-none");
  priceNearSchool = 0;
}

// TODO ACCOMMODATION URGENT
if ($accomUrgent === "yes") {
  priceAccomUrgent = 150;
  D.querySelector("#container-price-a-urgent").classList.remove("d-none");
} else {
  priceAccomUrgent = 0;
  D.querySelector("#container-price-a-urgent").classList.add("d-none");
}

// TODO SUMMER FEE
if ($summerFee === "yes") {
  priceSummerFee = 45 * 4;
  D.querySelector("#container-price-summer-fee").classList.remove("d-none");
} else {
  priceSummerFee = 0;
  D.querySelector("#container-price-summer-fee").classList.add("d-none");
}

// TODO MINOR FEE
if ($minorFee === "yes") {
  priceMinorFee = 75 * 4;
  D.querySelector("#container-price-minor-fee").classList.remove("d-none");
} else {
  priceMinorFee = 0;
  D.querySelector("#container-price-minor-fee").classList.add("d-none");
}

// TODO GUARDIANSHIP
if ($accomGuardianship === "yes") {
  priceAccomGuardianship = 550;
  D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
} else {
  priceAccomGuardianship = 0;
  D.querySelector("#container-price-a-guardianship").classList.add("d-none");
}

const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;
let finishedSignature = false;

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
/* $btnDescargar.onclick = () => {
  const enlace = document.createElement("a");
  // El título
  enlace.download = "Firma.png";
  // Convertir la imagen a Base64 y ponerlo en el enlace
  enlace.href = $canvas.toDataURL();
  // Hacer click en él
  enlace.click();
}; */

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});

["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
    finishedSignature = true;
  });
});

D.addEventListener("click", (e) => {
  if (e.target.matches(".form__btn") && finishedSignature == true) {
    const enlace = document.createElement("a");
    // Convertir la imagen a Base64 y ponerlo en el enlace
    enlace.href = $canvas.toDataURL();

    document.querySelector("#signature-canvas").value = enlace.href;
  }
});

D.addEventListener("DOMContentLoaded", (e) => {
  if (D.querySelector("#transport_total-db").value === "" || D.querySelector("#total_weckly").value === "") {
    if (D.querySelector("#transport_total-db").value === "") D.querySelector("#transport_total-db").value = 0;
    if (D.querySelector("#total_weckly").value === "") D.querySelector("#total_weckly").value = 0;
    if (D.querySelector("#booking_fee").value === "") D.querySelector("#booking_fee").value = 250;
    else console.log("no");

    D.querySelector("#total__price").value = 0;
    D.querySelector("#total__price-invoice").value = 0;
  } else {
    let totalAccomodation = parseFloat(D.querySelector("#total_weckly").value) * 4;
    D.querySelector("#total_weckly-db").value = totalAccomodation;
    D.querySelector("#total__price").value =
      parseFloat(D.querySelector("#booking_fee").value) +
      parseFloat(totalAccomodation) +
      priceNearSchool +
      priceAccomUrgent +
      priceSummerFee +
      priceMinorFee +
      priceAccomGuardianship +
      parseFloat(D.querySelector("#transport_total-db").value);
    D.querySelector("#total__price-invoice").value =
      parseFloat(D.querySelector("#booking_fee").value) +
      parseFloat(totalAccomodation) +
      priceNearSchool +
      priceAccomUrgent +
      priceSummerFee +
      priceMinorFee +
      priceAccomGuardianship +
      parseFloat(D.querySelector("#transport_total-db").value);
  }
});

D.addEventListener("change", (e) => {
  if (
    e.target.matches("#meal_p") ||
    e.target.matches("#lodging_type") ||
    e.target.matches("#pick_up") ||
    e.target.matches("#drop_off") ||
    e.target.matches("#accomm-near") ||
    e.target.matches("#accomm-urgent") ||
    e.target.matches("#accomm-guardianship") ||
    e.target.matches("#summer-fee") ||
    e.target.matches("#minor-fee")
  ) {
    let accommodation = 0;
    let transportTotal = 0;
    const mealValue = D.querySelector("#meal_p").value;
    const accommodationValue = D.querySelector("#lodging_type").value;

    if (accommodationValue === "Share") {
      if (mealValue === "Only Room") accommodation = 900 / 4;
      else if (mealValue === "2 Meals") accommodation = 1100 / 4;
      else if (mealValue === "3 Meals") accommodation = 1200 / 4;
    } else if (accommodationValue === "Executive") {
      if (mealValue === "Only Room") accommodation = 1450 / 4;
      else if (mealValue === "2 Meals") accommodation = 1800 / 4;
      else if (mealValue === "3 Meals") accommodation = 1900 / 4;
    } else {
      if (mealValue === "Only Room") accommodation = 1050 / 4;
      else if (mealValue === "2 Meals") accommodation = 1400 / 4;
      else if (mealValue === "3 Meals") accommodation = 1500 / 4;
    }

    if (mealValue === "Only Room") {
      D.querySelector("#food").value = "No";
      if (D.querySelector("#group__diet").matches(".d-block"))
        D.querySelector("#group__diet").classList.replace("d-block", "d-none");
      else D.querySelector("#group__diet").classList.add("d-none");
    } else if (mealValue === "2 Meals" || mealValue === "3 Meals") {
      D.querySelector("#group__diet").classList.replace("d-none", "d-block");
      D.querySelector("#food").value = "Yes";
    }
    /* D.querySelector("#total_weckly").value = "$" + accommodation + " CAD"; */
    D.querySelector("#total_weckly").value = accommodation;

    let pickUp = D.querySelector("#pick_up").value;
    let dropOff = D.querySelector("#drop_off").value;

    if (pickUp === "yes") pickUp = 200;
    else pickUp = 0;

    if (dropOff === "yes") dropOff = 100;
    else dropOff = 0;

    const $nearToSchool = D.querySelector("#accomm-near").value;
    const $accomUrgent = D.querySelector("#accomm-urgent").value;
    const $summerFee = D.querySelector("#summer-fee").value;
    const $minorFee = D.querySelector("#minor-fee").value;
    const $accomGuardianship = D.querySelector("#accomm-guardianship").value;

    let totalPrice = 0;
    let priceNearSchool = 0;
    let priceAccomUrgent = 0;
    let priceSummerFee = 0;
    let priceMinorFee = 0;
    let priceAccomGuardianship = 0;

    // TODO NEAR TO SCHOOL
    if ($nearToSchool === "yes") {
      priceNearSchool = 35 * 5 * 4;
      D.querySelector("#container-price-a-near").classList.remove("d-none");
    } else {
      D.querySelector("#container-price-a-near").classList.add("d-none");
      priceNearSchool = 0;
    }

    // TODO ACCOMMODATION URGENT
    if ($accomUrgent === "yes") {
      priceAccomUrgent = 150;
      D.querySelector("#container-price-a-urgent").classList.remove("d-none");
    } else {
      priceAccomUrgent = 0;
      D.querySelector("#container-price-a-urgent").classList.add("d-none");
    }

    // TODO SUMMER FEE
    if ($summerFee === "yes") {
      priceSummerFee = 45 * 4;
      D.querySelector("#container-price-summer-fee").classList.remove("d-none");
    } else {
      priceSummerFee = 0;
      D.querySelector("#container-price-summer-fee").classList.add("d-none");
    }

    // TODO MINOR FEE
    if ($minorFee === "yes") {
      priceMinorFee = 75 * 4;
      D.querySelector("#container-price-minor-fee").classList.remove("d-none");
    } else {
      priceMinorFee = 0;
      D.querySelector("#container-price-minor-fee").classList.add("d-none");
    }

    // TODO GUARDIANSHIP
    if ($accomGuardianship === "yes") {
      priceAccomGuardianship = 550;
      D.querySelector("#container-price-a-guardianship").classList.remove("d-none");
    } else {
      priceAccomGuardianship = 0;
      D.querySelector("#container-price-a-guardianship").classList.add("d-none");
    }

    transportTotal = parseFloat(pickUp) + parseFloat(dropOff);
    /* D.querySelector("#transport_total").value = transportTotal; */
    D.querySelector("#transport_total-db").value = transportTotal;
    D.querySelector(".price__transport").textContent = transportTotal;
    let bookingFee = D.querySelector("#booking_fee").value;
    let totalAccommodation = accommodation * 4;

    if (totalAccommodation == 0)
      totalPrice =
        parseFloat(transportTotal) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);
    else if (transportTotal == 0)
      totalPrice =
        parseFloat(totalAccommodation) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);
    else
      totalPrice =
        parseFloat(totalAccommodation) +
        parseFloat(transportTotal) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);

    D.querySelector(".price__b-fee").textContent = bookingFee;
    D.querySelector("#price__accommodation").textContent = totalAccommodation;
    D.querySelector("#total_weckly-db").value = totalAccommodation;
    D.querySelector("#total__price").value = totalPrice;
    D.querySelector("#total__price-invoice").value = totalPrice;
  }
});
D.addEventListener("click", (e) => {
  if (e.target.matches("#btn_invoice")) D.querySelector("#table_invoice").classList.toggle("d-none");
});
