$(document).ready(function () {
  load_data(1);

  function load_data(page, query = "") {
    $.ajax({
      url: "fetch_delete_user.php",
      method: "POST",
      data: { page: page, query: query },
      success: function (data) {
        $("#dynamic_content").html(data);
      },
    });
  }

  $(document).on("click", ".page-link", function () {
    var page = $(this).data("page_number");
    var query = $("#search_box").val();
    load_data(page, query);
  });
});

((D, W) => {
  async function deleteUser(btn, idHome, idNot) {
    const formDeleteUser = new FormData();
    formDeleteUser.set("id_home", idHome);
    formDeleteUser.set("id_not", idNot);
    const DATA = { method: "POST", body: formDeleteUser };

    const jsonDeleteUser = await fetch("./delete_user.php", DATA);
    const resultDeteteUser = await jsonDeleteUser.json();

    if (resultDeteteUser === "true") {
      btn.disabled = true;
      btn.style.cursor = "context-menu";
      btn.textContent = "Deleted";
    } else {
      alert("A problem has occurred, please try again.");
    }

    D.getElementById("btn_").classList.remove("d-none");
    D.getElementById("div__btn_delete").classList.remove("spin");
  }

  D.addEventListener("click", (e) => {
    if (e.target.matches("#btn_")) {
      D.getElementById("btn_").classList.add("d-none");
      D.getElementById("div__btn_delete").classList.add("spin");
      deleteUser(e.target, e.target.dataset.idHome, e.target.dataset.idNot);
    }
  });
})(document, window);
