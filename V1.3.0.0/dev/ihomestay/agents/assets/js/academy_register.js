// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  _Messages.quitMessage();
});

D.addEventListener("submit", (e) => {
  e.preventDefault();

  _Messages.showMessage("The school has been successfully registered", 2, 2);
  setTimeout(() => e.target.submit(), 2000);
});
