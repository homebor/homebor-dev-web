((D, W) => {
  const $aLinkViweProfileHomestay = D.querySelector("#a-view-profile-home");

  if ($aLinkViweProfileHomestay) {
    $aLinkViweProfileHomestay.addEventListener("mouseover", (e) => {
      D.querySelector(".arrow__view").classList.replace("arrow__view", "arrow__view-active");
    });

    $aLinkViweProfileHomestay.addEventListener("mouseout", (e) => {
      D.querySelector(".arrow__view-active").classList.replace("arrow__view-active", "arrow__view");
    });
  }

  D.addEventListener("click", (e) => {
    if (e.target.matches("#btn_invoice")) D.querySelector("#table_invoice").classList.toggle("d-none");
  });
})(document, window);
