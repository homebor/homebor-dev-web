// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_HOME = searchParams.get("art_id");

// TODO FUNCIONES
export default class BasicInformation {
  constructor() {}
  // ? OBTENER DE LA BASE DE DATOS LA INFORMACION BASICA
  async getBasicInformation() {
    try {
      const formData = new FormData();
      formData.set("homestay_id", ID_HOME);

      const OPTIONS = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("homedit_data.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";
      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];
      // console.log(data);

      await this.renderBasicInformation("houseInformation", data.basicInformation.houseInformation);
      await this.renderBasicInformation("location", data.basicInformation.locationInformation);
    } catch (error) {
      console.error(error);
    }
  }

  /** // ? FUNCION QUE RENDERIZA Y RELLENA LOS CAMPOS CON LA INFORMACION OBTENIDA
   * @param {String} title Detalles de los campos a rellenar
   * @param {Object|Array} data Datos a insertar
   */
  async renderBasicInformation(title, data) {
    if (title === "houseInformation") {
      D.getElementById("house-name").value = data.name;
      D.getElementById("phone-number").value = data.phone;
      D.getElementById("total-rooms").value = D.querySelectorAll("#reserve").length;
      D.getElementById("house-type").value = data.typeResidence;
      D.getElementById("house-mail").value = data.mail;
    }
    if (title === "location") {
      D.getElementById("main-city").value = data.mainCity;
      D.getElementById("address").value = data.address;
      D.getElementById("city").value = data.city;
      D.getElementById("state").value = data.state_province;
      D.getElementById("postal-code").value = data.postalCode;
    }
  }

  /** // ? SAVE BASIC INFORMATION
   * @param {Elements} inputs Todos los campos input de Basic Information a guardar
   * @param {Elements} selects Todos los campos select de Basic Information a guardar
   */
  async saveBasicInformation(inputs, selects) {
    try {
      const $nameContact = D.querySelector("#name-contact").value.trim();
      const $lastnameContact = D.querySelector("#last-name-contact").value.trim();

      const roomData = new FormData();
      roomData.set("basicInformation", true);
      roomData.set("homestay_id", ID_HOME);
      roomData.set("houseType", selects[0].value);
      roomData.set("mainCity", selects[1].value);
      roomData.set("houseName", `${$lastnameContact.toUpperCase()}, ${$nameContact}`);
      roomData.set("phoneNumber", inputs[1].value);
      roomData.set("totalRooms", inputs[2].value);
      roomData.set("houseMail", inputs[3].value);
      roomData.set("address", inputs[4].value);
      roomData.set("city", inputs[5].value);
      roomData.set("state", inputs[6].value);
      roomData.set("postalCode", inputs[7].value);
      const options = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("edit-admin-propertie.php", options);
      const data = await res.data;
    } catch (error) {
      console.error("Error en Basic Information", error);
    }
  }
}
