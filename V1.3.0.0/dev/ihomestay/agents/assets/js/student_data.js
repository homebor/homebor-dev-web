// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_STUDENT = searchParams.get("art_id");

// ? INSTANCIAS

// TODO FUNCIONES
// ? FUNCION QUE OBTIENE DE LA BASE DE DATOS LA INFORMACION ADICIONAL
export default async function getStudentData() {
  try {
    const formData = new FormData();
    formData.set("student_id", ID_STUDENT);
    formData.set("section", "student");

    const OPTIONS = {
      method: "POST",
      headers: { "Content-type": "application/json; charset=utf-8" },
      data: formData,
    };

    const req = await axios("../helpers/agents/edit_assigment.php", OPTIONS);
    const data = await req.data;

    if (!data) throw "No se ha recibido datos";
    if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

    await renderStudentData(data);
  } catch (error) {
    console.error(error);
  }
}

/** // ? FUNCION QUE RENDERIZA Y RELLENA LOS CAMPOS CON LA INFORMACION OBTENIDA
 * @param {String} title Detalles de los campos a rellenar
 * @param {Object|Array} data Datos a insertar
 */
async function renderStudentData(data) {
  const studentAge = (datebirth) => {
    const currentDate = new Date();
    const dateBirth = new Date(datebirth);
    let age = currentDate.getFullYear() - dateBirth.getFullYear();
    const month = currentDate.getMonth() - dateBirth.getMonth();

    if (month < 0 || (month === 0 && currentDate.getDate() < dateBirth.getDate())) age--;

    return `${age} Years`;
  };
  const weekStay = (firstday, lastday) => {
    let firstDate = firstday.split("-");
    let lastDate = lastday.split("-");
    let firstDateResult = Date.UTC(firstDate[0], firstDate[1] - 1, firstDate[2]);
    let lastDateResult = Date.UTC(lastDate[0], lastDate[1] - 1, lastDate[2]);
    const CALCULATION = lastDateResult - firstDateResult;
    const DAYS = Math.floor(CALCULATION / (1000 * 60 * 60 * 24));

    if (DAYS % 7 === 0) {
      D.querySelector("#stay").dataset.stay = "Weeks of Stay";
      return `${DAYS / 7} Weeks`;
    } else {
      D.querySelector("#stay").dataset.stay = "Days of Stay";
      return `${DAYS} Days`;
    }
  };

  D.querySelector("#student").src = "../../" + data.student.image;
  D.querySelector("#fullname-student").textContent = `${data.student.name} ${data.student.lastname}`;
  if (!data.student.nationality || data.student.nationality === "NULL") {
    D.querySelector("#nationality-student").textContent = " Empty";
  } else D.querySelector("#nationality-student").textContent = ` ${data.student.nationality}`;
  if (
    !data.student.academy ||
    data.student.academy === "NULL" ||
    !data.student.addressAcademy ||
    data.student.addressAcademy === "NULL" ||
    !data.student.cityAcademy ||
    data.student.cityAcademy === "NULL" ||
    !data.student.stateAcademy ||
    data.student.stateAcademy === "NULL"
  ) {
    D.querySelector("#academy-student").textContent = "Empty";
  } else {
    D.querySelector(
      "#academy-student"
    ).textContent = ` ${data.student.academy}, ${data.student.addressAcademy} ${data.student.cityAcademy} ${data.student.stateAcademy}`;
  }
  D.querySelector("#firstday-student").textContent = ` ${data.student.firstDay}`;
  D.querySelector("#lastday-student").textContent = ` ${data.student.lastDay}`;
  const studentDates = `${data.student.firstDay.replaceAll("-", "/")} - ${data.student.lastDay.replaceAll("-", "/")}`;
  D.querySelector("#search-date").value = studentDates;
  D.querySelector(".cancelBtn").classList.remove("btn-default");
  D.querySelector(".applyBtn").classList.remove("btn-primary");
  D.querySelector("#gender-student").textContent = ` ${data.student.gender}`;
  D.querySelector("#datebirth-student").textContent = ` ${data.student.dateBirth}`;
  D.querySelector("#student-age").textContent = studentAge(data.student.dateBirth);
  D.querySelector("#stay").textContent = weekStay(data.student.firstDay, data.student.lastDay);

  const $fragment = D.createDocumentFragment();
  data.student.more.forEach((arr) => {
    const searchFilter = (text) => {
      const filters = [
        "accommodation",
        "smoke",
        "pets",
        "food",
        "vegetarian",
        "halal",
        "kosher",
        "lactose",
        "gluten",
        "pork",
      ];
      let filterFound;
      filters.some((filter) => {
        if (text.includes(filter)) {
          filterFound = filter;
          return true;
        }
      });
      return filterFound;
    };

    if (
      arr[0] === "Vegetarian" ||
      arr[0] === "Halal" ||
      arr[0] === "Kosher" ||
      arr[0] === "Lactose" ||
      arr[0] === "Gluten" ||
      arr[0] === "Pork"
    ) {
      if (arr[1].toLowerCase() === "yes") {
        const $dl = D.createElement("dl");
        $dl.innerHTML = `<dt>${arr[0]}</dt>`;
        $dl.innerHTML += `<dd data-filter="${searchFilter(arr[0].toLowerCase())}" class="text-capitalize">${
          arr[1]
        }</dd>`;

        $fragment.appendChild($dl);
      }
    } else {
      if (arr[0] && arr[0] !== "NULL" && arr[1] && arr[1] !== "NULL") {
        const $dl = D.createElement("dl");
        $dl.className = "m-0 mx-auto p-0";
        $dl.innerHTML = `<dt class="text-center">${arr[0]}</dt>`;
        $dl.innerHTML += `<dd data-filter="${searchFilter(arr[0].toLowerCase())}" class="text-center text-capitalize">${
          arr[1]
        }</dd>`;

        $fragment.appendChild($dl);
      }
    }
  });

  D.querySelector(".ts-description-lists > div").appendChild($fragment);
}
