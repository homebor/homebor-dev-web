// TODO IMPORTANCIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES
class AgencyInformation {
  constructor() {
    this.allStudentsGetted = { total: [], unregistered: [] };
    this.allHomestaysGetted = { total: [], uncertified: [] };

    this.pages = { students: [], homestays: [] };

    this.studentsDrag = { isDown: false, startX: undefined, scrollLeft: undefined };
    this.homestaysDrag = { isDown: false, startX: undefined, scrollLeft: undefined };

    this.studentTemplate = D.getElementById("student-template").content;
    this.homestayTemplate = D.getElementById("homestay-template").content;
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? GET ALL STUDENTS OF THE AGENCY
  async getStudents() {
    return new Promise(async (resolve, reject) => {
      try {
        const getAllStudents = await axios("../helpers/students/get_all_students.php");
        const allStudents = await getAllStudents.data;
        this.allStudentsGetted.total = allStudents;
        this.allStudentsGetted.unregistered = allStudents.filter((student) => {
          return student.status.toLowerCase().includes("confirm registration") ? student : false;
        });
        // * CREATE PAGES
        for (let i = 0; i < this.allStudentsGetted.unregistered.length; i += 20) {
          this.pages.students.push(this.allStudentsGetted.unregistered.slice(i, i + 20));
        }
        resolve(true);
      } catch (error) {}
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? RENDER STUDENTS OF THE AGENCY
  async renderStudents(students) {
    return new Promise((resolve, reject) => {
      const $container = D.getElementById("all-students");

      if (!students || !students instanceof Array) {
        $container.style.cursor = "default";
        $container.innerHTML = `<span class="mt-3 mx-4 mx-lg-auto students-empty">You have no pending unregistered students</span>`;
        resolve(true);
      } else {
        const $fragment = D.createDocumentFragment();

        $container.innerHTML = "<space></space>";
        students.forEach((student) => {
          if (!student.status.toLowerCase().includes("confirm registration")) return;
          const $newStudent = D.importNode(this.studentTemplate, true);
          $newStudent.querySelector("[data-student-image]").src = "../../" + student.photo_s;
          $newStudent.querySelector("[data-student-name]").textContent = `${student.name_s} ${student.l_name_s}`;
          if (student.nationality === "NULL") student.nationality = "Empty";
          $newStudent.querySelector("[data-student-nationality]").textContent = student.nationality;
          if (student.firstd === "NULL") student.firstd = "Empty";
          $newStudent.querySelector("[data-student-firstdate]").textContent = student.firstd;
          if (student.lastd === "NULL") student.lastd = "Empty";
          $newStudent.querySelector("[data-student-lastdate]").textContent = student.lastd;
          if (student.academy === "NULL") student.academy = "Empty";
          $newStudent.querySelector("[data-student-school]").textContent = student.academy;
          $newStudent.querySelector("[data-student-profile]").href = `student_info?art_id=${student.id_student}`;
          $fragment.appendChild($newStudent);
        });
        $container.appendChild($fragment);
        $container.innerHTML += "<space></space>";

        resolve(true);
      }
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? GET ALL HOMESTAYS OF THE AGENCY
  async getHomestays() {
    return new Promise(async (resolve, reject) => {
      try {
        const getAllHomestays = await axios("../helpers/homestay/get_all_homestays.php");
        const allHomestays = await getAllHomestays.data;
        this.allHomestaysGetted.total = allHomestays;
        this.allHomestaysGetted.uncertified = allHomestays.filter((home) => {
          return home.certified.toLowerCase().includes("no") ? home : false;
        });
        // * CREATE PAGES
        for (let i = 0; i < this.allHomestaysGetted.uncertified.length; i += 20) {
          this.pages.homestays.push(this.allHomestaysGetted.uncertified.slice(i, i + 20));
        }
        resolve(true);
      } catch (error) {}
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? RENDER HOMESTAYS OF THE AGENCY
  async renderHomestays(homestays) {
    return new Promise((resolve, reject) => {
      const $container = D.getElementById("all-homestays");

      if (!homestays || !homestays instanceof Array) {
        $container.style.cursor = "default";
        $container.innerHTML = `<span class="mt-3 mx-4 mx-lg-auto homestays-empty">You have no pending unregistered homestays</span>`;
        resolve(true);
      } else {
        const $fragment = D.createDocumentFragment();

        $container.innerHTML = "<space></space>";
        homestays.forEach((homestay) => {
          if (homestay.certified.trim().toLowerCase().includes("yes")) return;
          const $newHomestay = D.importNode(this.homestayTemplate, true);
          $newHomestay.querySelector("[data-homestay-image]").src = "../../" + homestay.phome;
          $newHomestay.querySelector("[data-homestay-name]").textContent = `${homestay.name_h} ${homestay.l_name_h}`;
          if (homestay.dir === "NULL") homestay.dir = "Empty";
          if (homestay.city === "NULL") homestay.city = "Empty";
          $newHomestay.querySelector("[data-homestay-address]").textContent = `${homestay.dir} ${homestay.city}`;
          $newHomestay.querySelector("[data-homestay-bedrooms]").textContent = homestay.room;
          if (homestay.pet === "NULL") homestay.pet = "Empty";
          $newHomestay.querySelector("[data-homestay-pets]").textContent = homestay.pet;
          if (homestay.g_pre === "NULL") homestay.g_pre = "Empty";
          $newHomestay.querySelector("[data-homestay-gender]").textContent = homestay.g_pre;
          if (homestay.ag_pre === "NULL") homestay.ag_pre = "Empty";
          $newHomestay.querySelector("[data-homestay-age]").textContent = homestay.ag_pre;
          $newHomestay.querySelector("[data-homestay-profile]").href = `detail?art_id=${homestay.id_home}`;
          $fragment.appendChild($newHomestay);
        });
        $container.appendChild($fragment);
        $container.innerHTML += "<space></space>";

        resolve(true);
      }
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? RENDER STATISTICS OF THE AGENCY
  async renderStatistics() {
    return new Promise((resolve, reject) => {
      try {
        const studentsStatistics = {
          totalStudents: this.allStudentsGetted.total.length,
          confirmRegister: 0,
          searchHomestay: 0,
          selectHouse: 0,
          acceptStudents: 0,
          activeStudents: 0,
        };
        console.info("STUDENT STATISTICS");
        this.allStudentsGetted.total.forEach((student) => {
          console.log("----", student.name_s, student.status);
          if (student.status.toLowerCase().includes("confirm registration")) studentsStatistics.confirmRegister += 1;
          if (student.status.toLowerCase().includes("search for homestay")) studentsStatistics.searchHomestay += 1;
          if (student.status.toLowerCase().includes("student for select house")) studentsStatistics.selectHouse += 1;
          if (student.status.toLowerCase().includes("house for accept student")) studentsStatistics.acceptStudents += 1;
          if (student.status.toLowerCase().includes("homestay found")) studentsStatistics.activeStudents += 1;
        });

        D.querySelector("[data-total-students]").textContent = studentsStatistics.totalStudents;
        D.querySelector("[data-students-for-confirm]").textContent = studentsStatistics.confirmRegister;
        D.querySelector("[data-students-in-search]").textContent = studentsStatistics.searchHomestay;
        D.querySelector("[data-students-for-select]").textContent = studentsStatistics.selectHouse;
        D.querySelector("[data-homestay-for-accept]").textContent = studentsStatistics.acceptStudents;
        D.querySelector("[data-active-students]").textContent = studentsStatistics.activeStudents;

        D.querySelector("[data-total-homestay]").textContent = this.allHomestaysGetted.total.length;

        resolve(true);
      } catch (error) {}
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? DRAG SCROLL
  dragScroll(event, container) {
    const scrollContainer = (dragData) => {
      if (!dragData.isDown) return;
      event.preventDefault();
      const x = event.pageX - container.offsetLeft;
      const scrolling = (x - dragData.startX) * 1.5;
      container.scrollLeft = dragData.scrollLeft - scrolling;
    };
    if (event.type === "mousemove") {
      if (container.id === "all-students") scrollContainer(this.studentsDrag);
      if (container.id === "all-homestays") scrollContainer(this.homestaysDrag);
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? SCROLL INFINITY
  scrollInfinity(container) {
    const currentScroll = container.scrollLeft;
    const totalScroll = container.scrollWidth - container.offsetWidth;
    const percentageScroll = Math.round((currentScroll / totalScroll) * 100);

    if (
      container.id === "all-homestays" &&
      percentageScroll >= 80 &&
      this.pages.homestays[Number(container.dataset.homestayPage) + 1] &&
      this.pages.homestays[Number(container.dataset.homestayPage) + 1].length
    ) {
      container.dataset.homestayPage = Number(container.dataset.homestayPage) + 1;

      this.renderHomestays(this.pages.homestays[container.dataset.homestayPage]);
    } else if (
      container.id === "all-students" &&
      percentageScroll >= 80 &&
      this.pages.students[Number(container.dataset.studentPage) + 1] &&
      this.pages.students[Number(container.dataset.studentPage) + 1].length
    ) {
      container.dataset.studentPage = Number(container.dataset.studentPage) + 1;

      this.renderStudents(this.pages.students[container.dataset.studentPage]);
    }
  }
}

// TODO INSTANCIA
const INSTANCE = new AgencyInformation();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await INSTANCE.getStudents();
  await INSTANCE.getHomestays();
  await INSTANCE.renderStatistics();
  if (INSTANCE.allHomestaysGetted.length > INSTANCE.allStudentsGetted.length) {
    INSTANCE.renderStudents(INSTANCE.pages.students[0]);
    await INSTANCE.renderHomestays(INSTANCE.pages.homestays[0]);
  } else {
    INSTANCE.renderHomestays(INSTANCE.pages.homestays[0]);
    await INSTANCE.renderStudents(INSTANCE.pages.students[0]);
  }

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {});

// ! KEYUP
D.addEventListener("keyup", (e) => {});

// ! SCROLL
D.addEventListener(
  "scroll",
  (e) => {
    if (e.target.id && e.target.id.includes("all-")) INSTANCE.scrollInfinity(e.target, e);
  },
  true
);

// ! MOUSE DOWN
const defineDragOptions = (e, dataDrag, container) => {
  dataDrag.isDown = true;
  dataDrag.startX = e.pageX - container.offsetLeft;
  dataDrag.scrollLeft = container.scrollLeft;
  INSTANCE.dragScroll(e, container);
};
D.querySelector("#all-students").addEventListener("mousedown", (e) => {
  defineDragOptions(e, INSTANCE.studentsDrag, D.querySelector("#all-students"));
});
D.querySelector("#all-homestays").addEventListener("mousedown", (e) => {
  defineDragOptions(e, INSTANCE.homestaysDrag, D.querySelector("#all-homestays"));
});

// ! MOUSE LEAVE
D.querySelector("#all-students").addEventListener("mouseleave", (e) => (INSTANCE.studentsDrag.isDown = false));
D.querySelector("#all-homestays").addEventListener("mouseleave", (e) => (INSTANCE.homestaysDrag.isDown = false));

// ! MOUSE UP
D.querySelector("#all-students").addEventListener("mouseup", (e) => (INSTANCE.studentsDrag.isDown = false));
D.querySelector("#all-homestays").addEventListener("mouseup", (e) => (INSTANCE.homestaysDrag.isDown = false));

// ! MOUSE MOVE
D.querySelector("#all-students").addEventListener("mousemove", (e) => {
  INSTANCE.dragScroll(e, D.querySelector("#all-students"));
});
D.querySelector("#all-homestays").addEventListener("mousemove", (e) =>
  INSTANCE.dragScroll(e, D.querySelector("#all-homestays"))
);

// ! ERROR
D.addEventListener(
  "error",
  (e) => {
    if (e.target.matches(".student-image")) e.target.src = "../assets/emptys/profile-student-empty.png";
    if (e.target.matches(".homestay-image")) e.target.src = "../assets/emptys/frontage-empty.png";
  },
  true
);
