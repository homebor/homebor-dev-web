// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_HOME = searchParams.get("art_id");
// ? TEMPLATE DE MIEMBRO DE FAMILIA
const $template = D.getElementById("family-member-template").content;

// TODO FUNCIONES
export default class FamilyInformation {
  constructor() {}
  // ? OBTENER DE LA BASE DE DATOS LA INFORMACION DE FAMILIA
  async getFamilyInformation() {
    try {
      const formData = new FormData();
      formData.set("homestay_id", ID_HOME);

      const OPTIONS = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("homedit_data.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";
      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

      await this.renderFamilyInformation("anyMembers", data.familyInformation.anyMembers);
      await this.renderFamilyInformation("contact", data.familyInformation.contact);
      await this.renderFamilyInformation("preferences", data.familyInformation.preferences);
      await this.addFamilyMembers(data.familyInformation.additionalMembers);
    } catch (error) {
      console.error(error);
    }
  }
  /** // ? FUNCION QUE RENDERIZA Y RELLENA LOS CAMPOS CON LA INFORMACION OBTENIDA
   * @param {String} title Detalles de los campos a rellenar
   * @param {Object|Array} data Datos a insertar
   */
  async renderFamilyInformation(title, data) {
    if (title === "anyMembers") {
      if (
        data.allergies !== "No" &&
        data.allergies !== "NULL" &&
        data.medication !== "No" &&
        data.medication !== "NULL" &&
        data.health !== "No" &&
        data.health !== "NULL"
      ) {
        D.querySelector("#responses-any-members").classList.add("show");
      }

      if (data.allergies !== "NULL" && data.allergies !== "No") {
        D.querySelector("#responses-any-members").classList.add("response-1");
        D.querySelector("#specify-allergy").value = data.allergies;
        D.querySelector("#allergies").value = "Yes";
      } else D.querySelector("#allergies").value = data.allergies;

      if (data.medication !== "NULL" && data.medication !== "No") {
        D.querySelector("#responses-any-members").classList.add("response-2");
        D.querySelector("#specify-medication").value = data.medication;
        D.querySelector("#take-medication").value = "Yes";
      } else D.querySelector("#take-medication").value = data.medication;

      if (data.health !== "NULL" && data.health !== "No") {
        D.querySelector("#responses-any-members").classList.add("response-3");
        D.querySelector("#specify-problems").value = data.health;
        D.querySelector("#health-problems").value = "Yes";
      } else D.querySelector("#health-problems").value = data.health;
    }
    if (title === "contact") {
      D.querySelector("#name-contact").value = data.name;
      D.querySelector("#last-name-contact").value = data.lastName;
      D.querySelector("#date-birth-contact").value = data.dateBirth;
      D.querySelector("#gender-contact").value = data.gender;
      D.querySelector("#phone-contact").value = data.phone;
      D.querySelector("#occupation-contact").value = data.occupation;
      D.querySelector("#date-background-check").value = data.backgroundDate;
      D.querySelector("iframe").src = "../../" + data.pdf;
    }

    if (title === "preferences") {
      D.querySelector("#number-members").value = data.totalMembers;
      D.querySelector("#background").value = data.background;
      D.querySelector("#background-language").value = data.backgroundLanguage;

      if (data.religion === "No" && data.conditionMental === "No" && data.misdemeanor === "No")
        await FamilyInformation.allShowResponses("#family-responses");

      if (data.religion !== "No") {
        D.querySelector("#religion").value = "Yes";
        D.querySelector("#specify-religion").value = data.religion;
      } else D.querySelector("#religion").value = "No";
      if (data.conditionMental !== "No") {
        D.querySelector("#condition").value = "Yes";
        D.querySelector("#specify-condition").value = data.conditionMental;
      } else D.querySelector("#condition").value = "No";
      if (data.misdemeanor !== "No") {
        D.querySelector("#misdemeanor").value = "Yes";
        D.querySelector("#specify-misdemeanor").value = data.misdemeanor;
      } else D.querySelector("#misdemeanor").value = "No";

      D.querySelector("#family-final-question").value = data.certifiedBackground;
    }
  }

  /** // ? FUNCION QUE RENDERIZA LOS MIEMBROS DE FAMILIA
   * @param {Object} data Objeto con todos los datos de cada miembro de familia
   */
  async addFamilyMembers(data) {
    const allMembers = [[], [], [], [], [], [], [], []];

    Object.values(data).forEach((data, i) => {
      if (i > 0 && i < 9) allMembers[0].push(data);
      if (i > 8 && i < 17) allMembers[1].push(data);
      if (i > 16 && i < 25) allMembers[2].push(data);
      if (i > 24 && i < 33) allMembers[3].push(data);
      if (i > 32 && i < 41) allMembers[4].push(data);
      if (i > 40 && i < 49) allMembers[5].push(data);
      if (i > 48 && i < 57) allMembers[6].push(data);
      if (i > 56 && i < 65) allMembers[7].push(data);
    });

    allMembers.forEach((member, i) => {
      if (
        member[0] === "NULL" &&
        member[1] === "NULL" &&
        member[2] === "NULL" &&
        member[3] === "NULL" &&
        member[4] === "NULL" &&
        member[5] === "NULL" &&
        member[6] === "NULL" &&
        member[7] === "NULL"
      )
        return;

      if (member[0]) $template.querySelector("[data-name]").value = member[0];
      if (member[1]) $template.querySelector("[data-last-name]").value = member[1];
      if (member[2]) $template.querySelector("[data-date-birth]").value = member[2];

      if (member[3]) {
        $template.querySelectorAll(`[data-gender] option`).forEach((option) => {
          option.value !== "NULL" ? option.removeAttribute("selected") : {};
        });
        $template.querySelector(`[data-gender] option[value="${member[3]}"]`).setAttribute("selected", "");
      }
      if (member[4]) $template.querySelector("[data-occupation]").value = member[4];
      if (member[5]) $template.querySelector("[data-date-background-check]").value = member[5];
      if (member[6]) $template.querySelector("[data-member-pdf]").src = "../../" + member[6];
      if (member[7]) {
        $template.querySelectorAll(`[data-relation] option`).forEach((option) => {
          option.value !== "NULL" ? option.removeAttribute("selected") : {};
          $template.querySelector(`[data-relation] option[value="${member[7]}"]`).setAttribute("selected", "");
        });
      }
      const $clone = D.importNode($template, true);
      D.getElementById("family-members").appendChild($clone);
    });

    // * REMOVE BUTTON
    if (D.querySelectorAll("#family-members .family-member").length === 8) {
      D.querySelector(".btn-add-family-member").parentElement.remove();
    }
  }

  // ! SAVE FAMILY INFORMATION AND FAMILY MEMBERS
  async saveFamilyInformation(inputs, selects) {
    try {
      const setResponse = (value1, value2) => {
        if (value1 && value1 !== "NULL") {
          if (value1 === "Yes") return value2;
          else return "No";
        } else return "NULL";
      };

      const roomData = new FormData();
      roomData.set("familyInformation", true);
      roomData.set("homestay_id", ID_HOME);
      // * ANY MEMBER OF YOUR FAMILY
      roomData.set("allergies", setResponse(selects[0].value, inputs[0].value));
      roomData.set("medications", setResponse(selects[1].value, inputs[1].value));
      roomData.set("healthProblems", setResponse(selects[2].value, inputs[2].value));
      // * MAIN CONTACT
      roomData.set("nameContact", inputs[3].value);
      roomData.set("lastNameContact", inputs[4].value);
      roomData.set("dateBirthContact", inputs[5].value);
      roomData.set("genderContact", selects[3].value);
      roomData.set("phoneContact", inputs[6].value);
      roomData.set("occupationContact", inputs[7].value);
      roomData.set("dateBackgroundCheckContact", inputs[8].value);
      inputs[9].files[0]
        ? roomData.set("backgroundCheck", inputs[9].files[0])
        : roomData.set("backgroundCheck", "NULL");
      // * FAMILY PREFERENCES
      roomData.set("totalMembers", inputs[10].value);
      roomData.set("background", inputs[11].value);
      roomData.set("backgroundLanguage", inputs[12].value);
      roomData.set("religion", setResponse(selects[4].value, inputs[13].value));
      roomData.set("condition", setResponse(selects[5].value, inputs[14].value));
      roomData.set("misdemeanor", setResponse(selects[6].value, inputs[15].value));
      roomData.set("finalQuestion", selects[7].value);

      const options = {
        method: "POST",
        headers: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("./edit-admin-propertie.php", options);
      const data = await res.data;

      if (data.status !== "ok") return;

      // * FAMILY MEMBERS
      D.querySelectorAll(".family-member").forEach(async (elem, i) => {
        if (
          !elem.querySelector("[data-name]").value &&
          !elem.querySelector("[data-last-name]").value &&
          !elem.querySelector("[data-date-birth]").value &&
          elem.querySelector("[data-gender]").value === "NULL" &&
          elem.querySelector("[data-relation]").value === "NULL" &&
          !elem.querySelector("[data-occupation]").value
        ) {
          return;
        }

        const familyMembers = new FormData();
        familyMembers.set("familyMembers", true);
        familyMembers.set("homestay_id", ID_HOME);
        familyMembers.set(`member${i + 1}_name`, elem.querySelector("[data-name]").value || "Empty");
        familyMembers.set(`member${i + 1}_lastName`, elem.querySelector("[data-last-name]").value || "Empty");
        familyMembers.set(`member${i + 1}_dateBirth`, elem.querySelector("[data-date-birth]").value || "NULL");
        familyMembers.set(`member${i + 1}_gender`, elem.querySelector("[data-gender]").value || "NULL");
        familyMembers.set(`member${i + 1}_relation`, elem.querySelector("[data-relation]").value || "NULL");
        familyMembers.set(`member${i + 1}_occupation`, elem.querySelector("[data-occupation]").value || "Empty");
        familyMembers.set(
          `member${i + 1}_dateBackground`,
          elem.querySelector("[data-date-background-check]").value || "NULL"
        );
        familyMembers.set(`member${i + 1}_backgroundCheck`, elem.querySelector("[data-background-check]").files[0]);

        const familyMembersOptions = {
          method: "POST",
          headers: { "Content-type": "application/json; charset=utf-8" },
          data: familyMembers,
        };

        const sendFamilyMembers = await axios("./edit-admin-propertie.php", familyMembersOptions);
        const resultFamilyMembers = await sendFamilyMembers.data;
        // console.log("Family Members Enviado!", resultFamilyMembers);
      });
    } catch (error) {
      console.error("Error en Family Information", error);
    }
  }

  /** // ? FUNCION QUE MUESTRA LAS RESPUESTAS DE ALGUNOS INPUT SELECT
   * @param {Element} container Elemento padre de las respuestas a mostrar
   * @param {Number} response Respuesta a mostrar
   * @param {String} action Accion de remover clase "remove" o agregar "add"
   */
  static async allShowResponses(container, response = undefined, action = "remove") {
    const $elem = D.querySelector(container);
    const className = `response-${response}`;
    $elem.classList.add("show");
    if (response === 1) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);
    else if (response === 2) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);
    else if (response === 3) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);

    if (
      !$elem.classList.contains("response-1") &&
      !$elem.classList.contains("response-2") &&
      !$elem.classList.contains("response-3")
    )
      $elem.classList.remove("show");
  }

  /** // ? UTILIY RESPONSES
   * @param {String} value Valor del input select de pregunta
   * @param {Number} response Numero de la respuesta a mostrar (Normalmente 3 preguntas de IZQ a DER)
   * @param {String} container Elemento padre de las respuestas
   * @param {String} action Add para mostrar la respuesta, null para ocultarla
   */
  static responseControl(value, response, container, action = "add") {
    value === "Yes"
      ? FamilyInformation.allShowResponses(container, response, action)
      : FamilyInformation.allShowResponses(container, response);
  }
}

/** // ? UTILITY ADD FAMILY MEMBER
 * @param {Element} e Elemento presionado
 */
const addFamilyMember = (e) => {
  e.preventDefault();

  if (D.querySelectorAll("#family-responses section").length >= 8) return;

  $template.querySelectorAll("input").forEach((input) => (input.value = ""));
  $template.querySelectorAll("select option").forEach((option) => {
    option.value === "NULL" ? option.setAttribute("selected", "") : option.removeAttribute("selected");
  });
  $template.querySelector("iframe").src = "";
  const $clone = D.importNode($template, true);
  D.getElementById("family-members").appendChild($clone);
  // * REMOVE BUTTON
  if (D.querySelectorAll("#family-members .family-member").length === 8) e.target.parentElement.remove();
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", (e) => {});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? ADD MEMBER FAMILY
  if (e.target.matches("#add-member")) addFamilyMember(e);
});

// ! CHANGE
D.addEventListener("change", (e) => {
  if (e.target.id === "religion") FamilyInformation.responseControl(e.target.value, 1, "#family-responses");
  if (e.target.id === "condition") FamilyInformation.responseControl(e.target.value, 2, "#family-responses");
  if (e.target.id === "misdemeanor") FamilyInformation.responseControl(e.target.value, 3, "#family-responses");

  if (e.target.id === "allergies") FamilyInformation.responseControl(e.target.value, 1, "#responses-any-members");
  if (e.target.id === "take-medication") FamilyInformation.responseControl(e.target.value, 2, "#responses-any-members");
  if (e.target.id === "health-problems") FamilyInformation.responseControl(e.target.value, 3, "#responses-any-members");
});
