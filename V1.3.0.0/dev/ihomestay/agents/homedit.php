 <?php
  // ! DEJAR
  include_once('../../xeon.php');
  include '../../cript.php';
  session_start();
  error_reporting(0);

  // ! DEJAR
  $usuario = $_SESSION['username'];

  // ! DEJAR
  if (isset($_GET['art_id'])) {
    $id = $_GET['art_id'];

    // ! DEJAR
    $query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row = $query->fetch_assoc();

    // ! DEJAR
    $query6 = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
    $row6 = $query6->fetch_assoc();

    // ! DEJAR
    $query8 = $link->query("SELECT * FROM manager WHERE id_m = '$row6[id_m]' ");
    $row8 = $query8->fetch_assoc();
  }

  ?>

 <!doctype html>
 <html lang="en">

 <head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="author" content="ThemeStarz">
   <meta http-equiv="X-UA-Compatible" content="ie=edge" />

   <!--// ? CSS -->
   <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.36">
   <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.36">
   <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.36">
   <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.36">
   <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.36">
   <link rel="stylesheet" href="../homestay/assets/css/carousel.css?ver=1.0.36">
   <link rel="stylesheet" href="assets/css/homedit.css?ver=1.0.36">
   <link rel="stylesheet" href="assets/css/homedit2.css?ver=1.0.36">
   <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.36">


   <!--// ? Mapbox Links-->
   <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js?ver=1.0.36'></script>
   <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css?ver=1.0.36' rel='stylesheet' />
   <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js?ver=1.0.36"></script>
   <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css?ver=1.0.36" rel="stylesheet" />

   <!--// ?  Favicons -->
   <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png?ver=1.0.36">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.36">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png?ver=1.0.36">
   <link rel="manifest" href="/site.webmanifest?ver=1.0.36">
   <link rel="mask-icon" href="/safari-pinned-tab.svg?ver=1.0.36" color="#5bbad5">
   <meta name="msapplication-TileColor" content="#da532c">
   <meta name="theme-color" content="#ffffff">

   <!--// ?  Daterangepicker -->
   <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js?ver=1.0.36"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js?ver=1.0.36"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js?ver=1.0.36">
   </script>
   <link rel="stylesheet" type="text/css"
     href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css?ver=1.0.36" />

   <title>Homebor - Edit Property</title>

 </head>

 <header id="ts-header" class="fixed-top" style="background-color: white;">
   <?php include 'header.php' ?>
 </header>

 <body style="overflow-y: hidden;">
   <!-- // TODO LOADING SAVING -->
   <div class="loading-saving">
     <div class="px-5 loading-content">
       <h4>Please wait...</h4>
       <svg width="50" height="50" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#fff">
         <g fill="none" fill-rule="evenodd">
           <g transform="translate(1 1)" stroke-width="2">
             <circle stroke-opacity=".5" cx="18" cy="18" r="18" />
             <path d="M36 18c0-9.94-8.06-18-18-18">
               <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s"
                 repeatCount="indefinite" />
             </path>
           </g>
         </g>
       </svg>
     </div>

     <div class="px-5 loaded-content d-none">
       <h4 data-house-name="">has been successfully modified</h4>
       <svg width="50" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 496.158 496.158"
         style="enable-background:new 0 0 496.158 496.158;" xml:space="preserve">
         <path style="fill:#222;"
           d="M496.158,248.085c0-137.021-111.07-248.082-248.076-248.082C111.07,0.003,0,111.063,0,248.085 c0,137.002,111.07,248.07,248.082,248.07C385.088,496.155,496.158,385.087,496.158,248.085z" />
         <path style="fill:#FFFFFF;"
           d="M384.673,164.968c-5.84-15.059-17.74-12.682-30.635-10.127c-7.701,1.605-41.953,11.631-96.148,68.777c-22.49,23.717-37.326,42.625-47.094,57.045c-5.967-7.326-12.803-15.164-19.982-22.346c-22.078-22.072-46.699-37.23-47.734-37.867c-10.332-6.316-23.82-3.066-30.154,7.258c-6.326,10.324-3.086,23.834,7.23,30.174c0.211,0.133,21.354,13.205,39.619,31.475c18.627,18.629,35.504,43.822,35.67,44.066c4.109,6.178,11.008,9.783,18.266,9.783c1.246,0,2.504-0.105,3.756-0.322c8.566-1.488,15.447-7.893,17.545-16.332c0.053-0.203,8.756-24.256,54.73-72.727c37.029-39.053,61.723-51.465,70.279-54.908c0.082-0.014,0.141-0.02,0.252-0.043c-0.041,0.01,0.277-0.137,0.793-0.369c1.469-0.551,2.256-0.762,2.301-0.773c-0.422,0.105-0.641,0.131-0.641,0.131l-0.014-0.076c3.959-1.727,11.371-4.916,11.533-4.984C385.405,188.218,389.034,176.214,384.673,164.968z" />
       </svg>
     </div>

   </div>

   <!-- // TODO CONTAINER -->
   <main class="container">

     <!-- // TODO TITLE -->
     <div class="w-100 mb-5 py-3 border-bottom container__title">
       <h1 class="m-0 pl-lg-5 title__edit">Edit Propertie</h1>
     </div>

     <!-- // TODO BASIC INFORMATION -->
     <form id="basic-information" class="pb-4 bg-white shadow rounded" autocomplete="off" enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Basic Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">House Information</h3>

       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-around align-items-center">
         <div class="form-group d-none flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_hname mr-2"></i>
             <label for="house-name" class="m-0 text-muted font-weight-light">House Name *</label>
           </div>
           <input type="text" name="house-name" id="house-name" class="form-control"
             placeholder="e.g. John Smith Residence">
         </div>

         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_pnumber mr-2"></i>
             <label for="phone-number" class="m-0 text-muted font-weight-light">Phone Number *</label>
           </div>
           <input type="text" name="phone-number" id="phone-number" class="form-control" placeholder="e.g. 55575846">
         </div>

         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_rooms mr-2"></i>
             <label for="total-rooms" class="m-0 text-muted font-weight-light">Rooms in your House for Students
               *</label>
           </div>
           <input type="text" name="total-rooms" id="total-rooms" disabled class="form-control bg-light border-0"
             placeholder="e.g. 5">
         </div>
       </div>

       <div
         class="col-10 mb-4 mb-lg-0 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-around align-items-center">
         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_hname mr-2"></i>
             <label class="m-0 text-muted font-weight-light">Type of Residence *</label>
           </div>
           <select name="house-type" id="house-type" class="custom-select" style="width: 215px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="House"> House </option>
             <option value="Apartment"> Apartment </option>
             <option value="Condominium"> Condominium </option>
           </select>

           <!-- MENSAJE DE ERROR -->
           <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>
         </div>

         <div class="form-group d-flex flex-column">
           <div class="d-flex align-items-center">
             <i class="icon_mail mr-2"></i>
             <label for="house-mail" class="m-0 text-muted font-weight-light">Homestay Mail</label>
           </div>
           <input type="mail" name="house-mail" id="house-mail" disabled class="form-control bg-light border-0"
             placeholder="emaple@example.com">

           <!-- MENSAJE DE ERROR -->
           <p class="message__input_error"> Enter a Valid Email </p>
         </div>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Location</h3>

       <div class="d-flex flex-column flex-lg-row justify-content-center">
         <div class="col-lg-4 col-10 mx-auto form-group">
           <!-- // * MAIN CITY -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="main-city" class="m-0 text-muted font-weight-light">Main City</label>
             </div>

             <select name="main-city" id="main-city" class="custom-select">
               <option value="NULL" selected hidden>Select Option</option>
               <option value="Toronto">Toronto</option>
               <option value="Montreal">Montreal</option>
               <option value="Ottawa">Ottawa</option>
               <option value="Quebec">Quebec</option>
               <option value="Calgary">Calgary</option>
               <option value="Vancouver">Vancouver</option>
               <option value="Victoria">Victoria</option>
             </select>
           </div>

           <!-- // * ADDRESS -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="address" class="m-0 text-muted font-weight-light">Address *</label>
             </div>

             <input type="text" name="address" id="address" class="form-control" placeholder="Av, Street, etc.">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid Address </p>
           </div>

           <!-- // * CITY -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="city" class="m-0 text-muted font-weight-light">City *</label>
             </div>

             <input type="text" name="city" id="city" class="form-control" placeholder="e.g. Toronto">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid City </p>
           </div>

           <!-- // * STATE / PROVINCE -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="state" class="m-0 text-muted font-weight-light">State / Province *</label>
             </div>

             <input type="text" name="state" id="state" class="form-control" placeholder="e.g. Ontario">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid State </p>
           </div>

           <!-- // * POSTAL CODE -->
           <div class="form-group d-flex flex-column">
             <div class="d-flex align-items-center">
               <i class="icon_location mr-2"></i>
               <label for="postal-code" class="m-0 text-muted font-weight-light">Postal Code *</label>
             </div>

             <input type="text" name="postal-code" id="postal-code" class="form-control"
               placeholder="No Special Characters">

             <!-- MENSAJE DE ERROR -->
             <p class="message__input_error location_error"> Write a valid Postal Code </p>
           </div>
         </div>

         <!-- // ? MAP -->
         <div class="col-lg-6 col-10 mx-auto form-group">
           <div id='map' class="m-0 p-0"></div>
           <code hidden><?php echo $row['dir'] ?></code>
           <code hidden><?php echo $row['city'] ?></code>
           <code hidden><?php echo $row['state'] ?></code>
           <code hidden><?php echo $row['p_code'] ?></code>

           <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js?ver=1.0.36'></script>
         </div>
       </div>

     </form>


     <!-- // TODO HOUSE GALLERY -->
     <form id="house-gallery" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">House Gallery</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Photo Gallery</h3>

       <br><br>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="frontage-photo"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Frontage Photo</h4>

           <label for="change-photo-1" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/frontage-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-1" id="change-photo-1">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="living-room-photo"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Living Room Photo</h4>

           <label for="change-photo-2" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/living-room-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-2" id="change-photo-2">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="family-picture"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Family Picture</h4>

           <label for="change-photo-3" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/family-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-3" id="change-photo-3">
           </label>
         </div>
       </div>

       <br><br><br>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="kitchen" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Kitchen</h4>

           <label for="change-photo-4" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/kitchen-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-4" id="change-photo-4">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="dining-room"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Dining Room</h4>

           <label for="change-photo-5" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/dinning-room-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-5" id="change-photo-5">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br>
         </div>

         <!-- // * IMAGE -->
         <div class="mt-5 mt-lg-0 d-flex align-items-center justify-content-center"
           style="width: 250px; height: 180px;">
           <div id="house-area-3"
             class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
             <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
               House Area 3
               <span class="close text-white">X</span>
             </h4>

             <label for="change-photo-6" class="m-0 p-0 d-flex align-items-center justify-content-center">
               <img class="no-image" src="../assets/emptys/house-area-empty.png">
               <!-- // ** ADD PHOTO -->
               <input type="file" hidden data-gallery-photo name="change-photo-6" id="change-photo-6">
             </label>
           </div>

           <!-- REPLACEMENT -->
           <button id="add-area-3" class="m-0 add-photo-btn">+</button>
         </div>
       </div>


       <div id="house-area-4-container"
         class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-start">
         <!--  // * IMAGE  -->
         <div id="house-area-4"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
             House Area 4
             <span class="close text-white">X</span>
           </h4>

           <label for="change-photo-7" class="m-0 p-0 d-flex align-items-center justify-content-center ">
             <img class="no-image" src="../assets/emptys/house-area-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-7" id="change-photo-7">
           </label>

         </div>
         <!-- REPLACEMENT -->
         <button id="add-area-4" class="add-photo-btn d-none">+</button>
       </div>

       <div class="d-none d-lg-block">
         <br><br><br><br>
       </div>
       <div class="d-lg-none">
         <br><br><br>
       </div>

       <div class="col-10 mx-auto d-flex flex-column flex-lg-row align-items-center justify-content-around">
         <!-- // * IMAGE -->
         <div id="bathroom-1" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Bathroom Photo 1</h4>

           <label for="change-photo-8" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <i class="fa fa-pencil-alt"></i>
             <img class="gallery-photo" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-8" id="change-photo-8">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div id="bathroom-2" class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">Bathroom Photo 2</h4>

           <label for="change-photo-9" class="m-0 p-0 d-flex align-items-center justify-content-center">
             <img class="no-image" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-9" id="change-photo-9">
           </label>
         </div>

         <div class="d-lg-none">
           <br><br><br><br>
         </div>

         <!-- // * IMAGE -->
         <div class="d-flex align-items-center justify-content-center" style="width: 250px; height: 180px;">
           <div id="bathroom-3"
             class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
             <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
               Bathroom Photo 3
               <span class="close text-white">X</span>
             </h4>

             <label for="change-photo-10" class="m-0 p-0 d-flex align-items-center justify-content-center">
               <img class="no-image" src="../assets/emptys/bathroom-empty.png">
               <!-- // ** ADD PHOTO -->
               <input type="file" hidden data-gallery-photo name="change-photo-10" id="change-photo-10">
             </label>
           </div>

           <!-- REPLACEMENT -->
           <button id="add-bathroom-3" class="m-0 add-photo-btn">+</button>
         </div>

       </div>

       <div class="d-lg-none">
         <br><br><br><br>
       </div>

       <div id="bathroom-4-container"
         class="col-10 mx-auto mb-4 d-flex flex-column flex-lg-row align-items-center justify-content-start">
         <!--  // * IMAGE  -->
         <div id="bathroom-4"
           class="m-0 p-0 d-flex flex-column justify-content-between shadow gallery-photo-container hide">
           <h4 class="w-100 m-0 p-2 text-white text-center text-capitalize font-weight-bold">
             Bathroom Photo 4
             <span class="close text-white">X</span>
           </h4>

           <label for="change-photo-11" class="m-0 p-0 d-flex align-items-center justify-content-center ">
             <img class="no-image" src="../assets/emptys/bathroom-empty.png">
             <!-- // ** ADD PHOTO -->
             <input type="file" hidden data-gallery-photo name="change-photo-11" id="change-photo-11">
           </label>

         </div>
         <!-- REPLACEMENT -->
         <button id="add-bathroom-4" class="add-photo-btn d-none">+</button>
       </div>

     </form>

     <!-- // TODO ALL ROOMS CONTAINER -->
     <section class="m-0 p-0 container rooms-container">
       <h3 class="mt-5 text-center text-lg-left title__group-section">Bedrooms</h3>
       <div id="all-rooms" class="container rooms-list-homestay homedit mt-5">

         <template id="reserve-template">

           <!-- // TODO ROOM CONTAINER -->
           <div id="reserve"
             class="d-flex flex-column justify-content-center align-items-center rounded room-card-container-homestay">

             <div class="d-lg-flex py-4 px-3 align-items-center">

               <!-- // TODO ROOM CARD -->
               <div class="mx-4 pb-4 bg-white room-card">

                 <!-- // TODO ROOM HEADER -->
                 <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title">
                   <h3 class="p-0 m-0 text-white">Room #</h3>
                   <p class="m-0 p-0 text-white">CAD$ 0</p>
                 </div>

                 <!-- // TODO ROOM PHOTO -->
                 <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo edit">
                   <button id="delete-photo-btn" title="Delete image">
                     <svg fill="#fff" style="opacity: 0.7;" viewBox="0 0 24 24" width="26px" height="26px">
                       <path
                         d="M 10 2 L 9 3 L 4 3 L 4 5 L 5 5 L 5 20 C 5 20.522222 5.1913289 21.05461 5.5683594 21.431641 C 5.9453899 21.808671 6.4777778 22 7 22 L 17 22 C 17.522222 22 18.05461 21.808671 18.431641 21.431641 C 18.808671 21.05461 19 20.522222 19 20 L 19 5 L 20 5 L 20 3 L 15 3 L 14 2 L 10 2 z M 7 5 L 17 5 L 17 20 L 7 20 L 7 5 z M 9 7 L 9 18 L 11 18 L 11 7 L 9 7 z M 13 7 L 13 18 L 15 18 L 15 7 L 13 7 z" />
                     </svg>
                   </button>

                   <button id="edit-photo-btn" title="Edit image">
                     <img src="../assets/icon/edit.png" rel="noopener" alt="edit">
                   </button>

                   <img src="" id="single-image-room" class="d-none">

                   <!-- // TODO CAROUSEL FOTOS -->
                 </div>

                 <!-- // TODO INFO ROOM -->
                 <div class="d-flex justify-content-center mt-5">

                   <!-- // TODO TYPE ROOM -->
                   <div class="col-auto mr-3 p-0" data-no-type-room="Select type room">
                     <div class="feature type-room">
                       <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
                       <select class="custom-select" name="type-room" id="type-room" required>
                         <option value="NULL" hidden selected>Type room</option>
                         <option value="Single">Single</option>
                         <option value="Executive">Executive</option>
                         <option value="Share">Share</option>
                       </select>
                     </div>
                   </div>

                   <!-- // TODO ROOM FOOD SERVICE -->
                   <div class="col-auto p-0">
                     <div class="feature food">
                       <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
                       <select class="custom-select" name="food-service" id="food-service" required>
                         <option value="NULL" hidden selected>Food</option>
                         <option value="Yes">Yes</option>
                         <option value="No">No</option>
                       </select>
                     </div>
                   </div>

                 </div>

                 <div class="m-0 mt-4 p-0 d-flex flex-column align-items-center justify-content-center edit-price"
                   data-price-invalid="Price invalid">
                   <label class="font-weight-light">Weekly Price CAD$</label>
                   <div class="mb-2 form-group d-flex w-75">
                     <input type="text" title="Homestay price" id="homestay-price" class="mr-3 form-control"
                       placeholder="Homestay" maxlength="10">
                     <input type="text" title="Provider price" id="provider-price" class="form-control"
                       placeholder="Provider" maxlength="10">
                   </div>
                 </div>

                 <div class="d-flex align-items-center justify-content-center">
                   <button id="disable-room" class="w-auto mt-3 py-1 mx-auto btn disable-room">Disable room</button>
                   <button id="enable-room" class="w-auto mt-3 py-1 mx-auto btn enable-room d-none">Enable room</button>
                 </div>

               </div>

               <!-- // TODO BEDS CONTAINER -->
               <div id="beds-container"
                 class="w-auto mx-lg-4 mt-5 mt-lg-0 d-flex flex-column align-items-center room-beds-homestay edit">
                 <!-- // TODO BEDS -->
                 <h3 class="font-weight-light text-muted">Beds</h3>

                 <br class="d-d-lg-none">

                 <!-- // TODO IMAGE ONLY ONE BED -->
                 <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none"
                   alt="students">
               </div>

             </div>

           </div>

         </template>

       </div>

       <template id="bed-template">
         <!-- // TODO BED -->
         <div id="bed" class="mb-4 my-lg-auto rounded d-flex justify-content-center bed-info-homestay"
           data-no-bed="Select type bed">

           <div class="my-4 d-flex">
             <div class="w-100 h-50 d-flex align-items-center justify-content-center">
               <img src="../assets/icon/cama 64.png" width="25px" height="25px" alt="">
               <select name="bed-type" id="bed-type" class="px-2" disabled required>
                 <option value="NULL">Type bed</option>
                 <option value="Twin">Twin</option>
                 <option value="Bunk">Bunk</option>
                 <option value="Double">Double</option>
               </select>
             </div>

             <div class="d-flex align-items-center justify-content-center">
               <span id="bed-letter" class="text-white">Bed</span>
             </div>
           </div>

           <button id="add-bed" class="my-3 d-none" title="Add bed">+</button>
         </div>

       </template>

       <div class="d-flex align-items-center justify-content-center">
         <button id="add-new-room" class="mx-auto btn add-new-room">Add new room</button>
       </div>



     </section>


     <!-- // TODO ADDITIONAL INFORMATION -->
     <form id="additional-information" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Additional Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Description of your House</h3>

       <div class="w-75 p-0 mx-auto d-flex form-group">
         <i class="icon_description "></i>
         <textarea rows="3" maxlength="255" class="mx-auto form-control" name="house-description" id="house-description"
           placeholder="Describe your house using few words, no special characters."></textarea>
       </div>

       <br><br>

       <h3 class="text-center text-lg-left title__group-section">Preferences</h3>

       <!-- // * ACADEMY -->
       <div class="col-11 col-lg-10 my-4 mx-auto d-flex justify-content-between align-items-center">
         <div class="w-100 p-0 mx-auto d-flex flex-column form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_p_academy mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">School Preference</label>
           </div>
           <select class="custom-select" id="academy" name="academy">
             <option value="NULL" hidden selected>Select School</option>
             <!-- INSERTAR TODAS LAS ACADEMIAS -->
           </select>
         </div>
       </div>


       <!-- // * GENDER / AGE / STATUS -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center ">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_gender mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Gender Preference</label>
           </div>
           <select class="custom-select" id="preferences-gender" name="preferences-gender" style="width: 290px;">
             <option value="NULL" selected hidden>Select Gender</option>
             <option value="Male">Male</option>
             <option value="Female">Female</option>
             <option value="Any">Any</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_age mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Age Preference</label>
           </div>
           <select class="custom-select" id="preferences-age" name="preferences-age" style="width: 290px;">
             <option value="NULL" selected hidden>Select Age</option>
             <option value="Teenager">Teenager</option>
             <option value="Adult">Adult</option>
             <option value="Any">Any</option>
           </select>
         </div>


         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_smokers mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Smokers Politics</label>
           </div>
           <select class="custom-select" id="smokers-politics" name="smokers-politics" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Outside-OK">Outside-OK</option>
             <option value="Inside-OK">Inside-OK</option>
             <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>
           </select>
         </div>
       </div>


       <!-- // * SMOKERS / MEALS SERVICE / HOMESTAY QUESTION 1 -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_meals "></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Meals Service</label>
           </div>
           <select class="custom-select" id="meals-service" name="meals-service" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group" style="width: 290px;">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_years mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Since when have you been a
               Homestay?</label>
           </div>
           <input type="date" class="form-control" id="total-years" name="total-years">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Do you have pets?</label>
           </div>
           <select class="custom-select" id="pets" name="pets" style="width: 290px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>


       <!-- // * PETS -->
       <div
         class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center pet-response">
         <div class="form-group total-pets-and-type">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label for="total-pets" class="m-0 py-2 py-lg-0 text-muted font-weight-light">How many pets?</label>
           </div>
           <input type="number" class="form-control" id="total-pets" name="total-pets" max="10"
             placeholder="Pets Number" style="width: 290px;">
         </div>


         <div
           class="form-group type-of-pet d-flex flex-lg-column align-items-center align-items-lg-start justify-content-between"
           style="width: 290px;">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Kind of Pet</label>
           </div>

           <!-- // ** KINDS OF PET -->
           <div class="w-auto p-2 m-0 d-flex align-items-center justify-content-center form-control">
             <!-- // ** DOG -->
             <input type="checkbox" id="dog" name="dog">
             <label for="dog" class="m-0 py-2 py-lg-0 mr-2 px-1 text-center text-muted font-weight-light">Dog</label>

             <!-- // ** CAT -->
             <input type="checkbox" id="cat" name="cat">
             <label for="cat" class="m-0 py-2 py-lg-0 mr-2 px-1 text-center text-muted font-weight-light">Cat</label>

             <!-- // ** OTHER -->
             <input type="checkbox" id="other" name="other">
             <label for="other" class="m-0 py-2 py-lg-0 px-1 text-center text-muted font-weight-light">Other</label>
           </div>
         </div>


         <div class="form-group specify-type-pet">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_pets mr-1"></i>
             <label for="specify-pet" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify</label>
           </div>
           <input type="text" class="form-control" id="specify-pet" name="specify-pet" placeholder="Specify"
             style="width: 290px;">
         </div>
       </div>


       <h3 class="text-center text-lg-left title__group-section">Special Diet</h3>

       <!-- // * SPECIAL DIET -->
       <div
         class="col-10 col-lg-6 py-4 pt-5 my-4 mx-auto d-flex flex-column justify-content-between align-items-center rounded special-diet">
         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="vegetarians" name="vegetarians" value="yes">
           <label for="vegetarians" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Vegetarians</label>
           <input type="checkbox" id="halal" name="halal" value="yes">
           <label for="halal" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">Halal (Muslims)</label>
         </div>

         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="kosher" name="kosher" value="yes">
           <label for="kosher" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Kosher (Jews)</label>
           <input type="checkbox" id="lactose" name="lactose" value="yes">
           <label for="lactose" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">Lactose
             Intolerant</label>
         </div>

         <div class="col-12 col-lg-8 mb-3 d-flex align-items-center justify-content-center">
           <input type="checkbox" id="gluten" name="gluten" value="yes">
           <label for="gluten" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted mr-3">Gluten Free
             Diet</label>
           <input type="checkbox" id="pork" name="pork" value="no">
           <label for="pork" class="m-0 py-2 py-lg-0 p-0 font-weight-light text-muted">No Pork</label>
         </div>


       </div>
     </form>


     <!-- // TODO FAMILY INFORMATION -->
     <form id="family-information" class="pb-4 mt-5 bg-white shadow rounded" autocomplete="off"
       enctype="multipart/form-data">
       <div class="form__target-header">
         <legend class="title_target">Family Information</legend>
       </div>

       <h3 class="text-center text-lg-left title__group-section">Any Member of your Family</h3>

       <!-- // * QUESTION 1 / QUESTION 2 / QUESTION 3 -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies2 mr-2"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Have Allergies?</label>
           </div>
           <select class="custom-select" id="allergies" name="allergies" style="width: 260px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Take any Medication?</label>
           </div>
           <select class="custom-select" id="take-medication" name="take-medication" style="width: 260px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_allergies2 mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Have Health Problems?</label>
           </div>
           <select class="custom-select" id="health-problems" name="health-problems" style="width: 260px;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>

       <!-- // * RESPONSE 1 / RESPONSE 2 / RESPONSE 3 -->
       <div id="responses-any-members"
         class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center responses">
         <div class="form-group">
           <label for="specify-allergy" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Allergy</label>
           <input type="text" name="specify-allergy" id="specify-allergy" class="form-control" style="width: 260px;">
         </div>

         <div class="form-group">
           <label for="specify-medication" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Medication</label>
           <input type="text" name="specify-medication" id="specify-medication" class="form-control"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <label for="specify-problems" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Specify the
             Problems</label>
           <input type="text" name="specify-problems" id="specify-problems" class="form-control" style="width: 260px;">
         </div>

         <p class="message__input_error"> Write a valid Name </p>
       </div>


       <h3 class="mt-4 text-center text-lg-left title__group-section">Main Contact Information</h3>


       <!-- // * NAME CONTACT / LAST NAME / DATE OF BIRTH -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_mname mr-2"></i>
             <label for="name-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Name Main Contact *</label>
           </div>
           <input type="text" class="form-control" id="name-contact" name="name-contact" placeholder="e.g. John"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_mname mr-2"></i>
             <label for="last-name-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Last Name
               *</label>
           </div>
           <input type="text" class="form-control" id="last-name-contact" name="last-name-contact"
             placeholder="e.g. Smith" style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_db mr-1"></i>
             <label for="date-birth-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Date of Birth
               *</label>
           </div>
           <input type="date" class="form-control" id="date-birth-contact" name="date-birth-contact"
             placeholder="MM-DD-YYYY" style="width: 260px;">
         </div>
       </div>


       <!-- // * GENDER / PHONE NUMBER / OCCUPATION -->
       <div class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_gender2 mr-1"></i>
             <label class="m-0 py-2 py-lg-0 text-muted font-weight-light">Gender *</label>
           </div>
           <select class="custom-select" id="gender-contact" name="gender-contact" style="width: 260px;">
             <option value="NULL" hidden selected>Select Gender</option>
             <option value="Male">Male</option>
             <option value="Female">Female</option>
             <option value="Private">Private</option>
           </select>
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_pnumber2 mr-2"></i>
             <label for="phone-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Phone Number</label>
           </div>
           <input type="text" class="form-control" id="phone-contact" name="phone-contact" placeholder="e.g. 55578994"
             style="width: 260px;">
         </div>

         <div class="form-group">
           <div class=" d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="occupation-contact" class="m-0 py-2 py-lg-0 text-muted font-weight-light">Occupation</label>
           </div>
           <input type="text" class="form-control" id="occupation-contact" name="occupation-contact"
             placeholder="e.g. Lawyer" style="width: 260px;">
         </div>
       </div>


       <!-- // * DATE OF BACKGROUND CHECK -->
       <div class="col-10 my-lg-4 mx-auto d-flex justify-content-center align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_db mr-1"></i>
             <label for="date-background-check" class="m-0 py-2 py-lg-0 text-muted font-weight-light">
               Date of Background Check
             </label>
           </div>
           <input type="date" class="form-control" id="date-background-check" name="date-background-check"
             placeholder="MM-DD-YYYY" style="width: 260px;">
         </div>
       </div>


       <!-- // * PDF -->
       <div class="col-12 col-lg-10 mb-5 mx-auto d-flex flex-column justify-content-center align-items-center">
         <div class="col-12 col-lg-auto my-0 d-flex justify-content-between form-group">
           <div class="w-100 py-1 d-flex align-items-center">
             <i class="icon_bcheck mr-1"></i>
             <label for="pdf-file"
               class="w-100 h-auto form-control text-muted d-flex align-items-center justify-content-between font-weight-light"
               style="cursor:pointer">
               Background Check (Only PDF File)
               <span class="ml-3 py-1 px-4 btn btn-dark text-white ">Upload File</span>
             </label>
           </div>

           <input type="file" class="p-0 p-1 m-0 form-control" name="pdf-file" id="pdf-file" maxlength="1" accept="pdf"
             hidden>
         </div>

         <iframe id="iframe-pdf" class="embed-responsive-item my-2" src=""></iframe>
       </div>



       <h3 class="text-center text-lg-left title__group-section">Family Preferences</h3>


       <!-- // * NUMBER MEMBERS / BACKGROUND / BACKGROUND LANGUAGE -->
       <div class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_family mr-1"></i>
             <label for="number-members" class="w-100 m-0 text-muted font-weight-light">Number Members</label>
           </div>

           <input type="number" class="form-control" id="number-members" name="number-members" min="0" max="10"
             placeholder="Only Numbers" style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="background" class="w-100 m-0 text-muted font-weight-light">Background</label>
           </div>

           <input type="text" class="form-control" id="background" name="background" placeholder="e.g. Canadian"
             style="width: 290px;">
         </div>

         <div class="form-group ">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="background-language" class="w-100 m-0 text-muted font-weight-light">Background Language</label>
           </div>

           <input type="text" class="form-control" id="background-language" name="background-language"
             placeholder="e.g. English" style="width: 290px;">
         </div>
       </div>


       <!-- // * FAMILY QUESTION 1 / FAMILY QUESTION 2 / FAMILY QUESTION 3 -->
       <div class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="religion" class="w-100 m-0 text-muted font-weight-light">
               Do you belong to a religion?
             </label>
           </div>

           <select class="custom-select" name="religion" id="religion" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="condition" class="w-100 m-0 text-muted font-weight-light">
               Any Physical or Mental condition?
             </label>
           </div>

           <select class="custom-select" name="condition" id="condition" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label for="misdemeanor" class="w-100 m-0 text-muted font-weight-light">
               Have they committed a misdemeanor?
             </label>
           </div>

           <select class="custom-select" name="misdemeanor" id="misdemeanor" style="width: 290px;">
             <option value="NULL" hidden selected>Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
           </select>
         </div>
       </div>

       <!-- // * FAMILY RESPONSE 1 / FAMILY RESPONSE 2 / FAMILY RESPONSE 3 -->
       <div id="family-responses"
         class="col-10 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center responses">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-religion" class="w-100 m-0 text-muted font-weight-light">Which Religion?</label>
           </div>

           <input type="text" class="form-control" id="specify-religion" name="specify-religion" min="0" max="10"
             style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-condition" class="w-100 m-0 text-muted font-weight-light">Which Condition?</label>
           </div>

           <input type="text" class="form-control" id="specify-condition" name="specify-condition"
             style="width: 290px;">
         </div>

         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <label for="specify-misdemeanor" class="w-100 m-0 text-muted font-weight-light">
               Specify misdemeanor?
             </label>
           </div>

           <input type="text" class="form-control" id="specify-misdemeanor" name="specify-misdemeanor"
             style="width: 290px;">
         </div>
       </div>


       <!-- // * FAMILY FINAL QUESTION -->
       <div class="col-10 my-3 my-lg-5 mx-auto d-flex justify-content-center align-items-center">
         <div class="form-group">
           <div class="py-1 d-flex align-items-center">
             <i class="icon_check mr-1"></i>
             <label class="w-100 m-0 text-muted font-weight-light">Do you give us your consent
               to go to the authorities and check your criminal background check?</label>
           </div>

           <select name="family-final-question" id="family-final-question" class="custom-select" style="width: 100%;">
             <option value="NULL" selected hidden>Select Option</option>
             <option value="Yes"> Yes </option>
             <option value="No"> No </option>
           </select>
         </div>
       </div>



       <!-- // TODO ADD FAMILY MEMBERS -->
       <h3 class="text-center text-lg-left title__group-section">Family Members</h3>

       <div id="family-members"></div>

       <template id="family-member-template">
         <section class="family-member">
           <!-- // * NAME CONTACT / LAST NAME / DATE OF BIRTH -->
           <div
             class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_mname mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Name</label>
               </div>
               <input type="text" class="form-control" data-name placeholder="e.g. Melissa" style="width: 260px;">
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_mname mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Last Name</label>
               </div>
               <input type="text" class="form-control" data-last-name placeholder="e.g. Smith" style="width: 260px;">
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_db mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Date of Birth</label>
               </div>
               <input type="date" class="form-control" data-date-birth placeholder="MM-DD-YYYY" style="width: 260px;">
             </div>
           </div>


           <!-- // * GENDER / RELATION / OCCUPATION -->
           <div
             class="col-10 my-lg-4 mx-auto d-flex flex-column flex-lg-row justify-content-between align-items-center">
             <div class="form-group">
               <div class="d-flex align-items-center">
                 <i class="icon_gender2 mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Gender</label>
               </div>
               <select class="custom-select" data-gender style="width: 260px;">
                 <option value="NULL" hidden selected>Select Gender</option>
                 <option value="Male">Male</option>
                 <option value="Female">Female</option>
                 <option value="Private">Private</option>
               </select>
             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_family mr-2"></i>
                 <label class="m-0 text-muted font-weight-light">Relation</label>
               </div>
               <select class="custom-select" data-relation style="width: 260px;">
                 <option value="NULL" hidden selected>Select Relation</option>
                 <option value="Dad">Dad</option>
                 <option value="Mom">Mom</option>
                 <option value="Son">Son</option>
                 <option value="Daughter">Daughter</option>
                 <option value="Grandparents">Grandparents</option>
                 <option value="Others">Others</option>
               </select>

             </div>

             <div class="form-group">
               <div class=" d-flex align-items-center">
                 <i class="icon_check mr-1"></i>
                 <label class="m-0 text-muted font-weight-light">Occupation</label>
               </div>
               <input type="text" class="form-control" data-occupation placeholder="e.g. Lawyer" style="width: 260px;">

             </div>
           </div>


           <!-- // * DATE OF BACKGROUND CHECK / BACKGROUND CHECK -->
           <div class="col-10 my-lg-4 mx-auto d-flex justify-content-around align-items-center">
             <div class="form-group">
               <div class="py-1 mb-3 d-flex flex-column flex-lg-row align-items-center justify-content-center">
                 <div class="d-flex align-content-center w-auto" style="width: 260px;">
                   <i class="icon_db mr-1"></i>
                   <label class="m-0 mr-2 text-muted font-weight-light">Date of Background Check</label>
                 </div>
                 <input type="date" class="form-control" data-date-background-check placeholder="MM-DD-YYYY"
                   style="width: 260px;">
               </div>

               <iframe id="iframe-pdf" data-member-pdf class="embed-responsive-item my-2" src=""></iframe>

               <div class="py-1 d-flex align-items-center">
                 <i class="icon_bcheck mr-1"></i>
                 <label class="w-100 m-0 text-muted font-weight-light">Background Check</label>
               </div>
               <input type="file" class="w-100 p-0 p-1 m-0 form-control" data-background-check maxlength="1"
                 accept="pdf">
             </div>
           </div>


         </section>
         <br>
         <hr>
         <br><br>
       </template>

       <div class="col-10 my-5 mb-0 mx-auto d-flex justify-content-center align-items-center">
         <button id="add-member" type="button" class="mx-auto btn btn-add-family-member">+ Add Family Member</button>
       </div>

     </form>

     <!-- // TODO ULTIMATE BUTTONS -->
     <div class="w-100 d-flex flex-column py-5 px-3 shadow bg-white rounded border-0 form__group-save"
       style="transform: translateY(-5rem);">
       <div class="w-100 mx-auto px-5 d-flex flex-column justify-content-end reason" data-type="decertify">
         <h3 class="my-2 text-center text-lg-left border-bottom reason-title">Reason for Decertification</h3>
         <br>
         <textarea id="reason" class="mx-auto form-control" cols="60" rows="5"
           placeholder="We have decided not to work with the following house for the following reasons (...)"></textarea>
         <br>
         <button id="send-reason" class="mx-auto py-2 px-2 btn btn-lg btn-secondary">Send Reason</button>
       </div>

       <div class="w-100 p-0 m-0 d-flex flex-column flex-lg-row justify-content-around final-buttons">
         <button id="discard" class="btn btn-primary text-white border-0 ts-btn-arrow btn-lg"
           style="background-color: #4f177d;">
           <i class="fa fa-times mr-1"></i> Discard Changes
         </button>
         <br class="d-lg-none">
         <button id="disabled" class="btn btn-primary text-white border-0 ts-btn-arrow btn-lg "
           style="background-color: #394893;">
           <i class="fa fa-times mr-1"></i> Disabled House
         </button>
         <br class="d-lg-none">
         <button id="decertify" class="btn btn-primary text-white border-0 ts-btn-arrow btn-lg "
           style="background-color: #982a72;">
           <i class="fa fa-times mr-1"></i> Descertify House
         </button>
         <br class="d-lg-none">
         <button id="certified" class="btn btn-primary text-white border-0 ts-btn-arrow btn-lg "
           style="background-color: #5D418D;">
           <i class="fa fa-save mr-1"></i> Certified House
         </button>
         <br class="d-lg-none">
         <button id="submit" class="btn btn-primary text-white border-0 ts-btn-arrow btn-lg"
           style="background-color: #232159;">
           <i class="fa fa-save mr-1"></i> Save Changes
         </button>
       </div>
     </div>


     <!-- // TODO MODAL DYNAMIC -->
     <div id="modal-dynamic" class="modal-dynamic">

       <section class="modal-content">
         <span class="close">X</span>
         <h3>Title</h3>


         <div class="modal-empty">
           <img src="../assets/img/homestay_home.png" alt="empty" rel="noopener">
         </div>


         <table class="modal-table">
           <thead>
             <tr>
               <th>Image</th>
               <th>Name</th>
               <th>Bed</th>
               <th>Start</th>
               <th>End</th>
             </tr>
           </thead>
           <tbody>
           </tbody>
         </table>

       </section>

     </div>
   </main>


   <?php

    $payments = $link->query("SELECT * FROM payments WHERE i_mail = '$row[mail_h]' AND r_mail = '$row8[mail]' OR i_mail = '$row8[mail]' AND r_mail = '$row[mail_h]'");
    $payments_rows = mysqli_num_rows($payments);

    if ($payments_rows > 0) { ?>

   <section class="card-main" id="payments1">


     <div class="form__group-house_in form__group">
       <div class="form__target-header">
         <h3 class="title_target"> Invoice History </h3>
       </div>


       <table class="table table-hover table-striped">

         <thead>
           <tr>
             <th class="text-center align-middle">N° Invoice</th>
             <th class="text-center align-middle">Date</th>
             <th class="text-center align-middle">Price</th>
             <th class="text-center align-middle">Student</th>
             <th class="text-center align-middle">Status</th>
             <th class="text-center align-middle"></th>
           </tr>
         </thead>

         <tbody>
           <?php while ($pay = mysqli_fetch_array($payments)) {

                $id_p = strlen($pay['id_p']);

                $student = $link->query("SELECT * FROM pe_student WHERE mail_s = '$pay[reserve_s]'");
                $r_stu = $student->fetch_assoc();

              ?>
           <tr>
             <form action="payment_save.php" method="POST">

               <th class="text-center align-middle text_table1">
                 <input type="hidden" name="id_p" value="<?php echo $pay['id_p'] ?>" readonly>

                 <?php if ($id_p == '1')  ?> #0000<?php echo $pay['id_p'] ?>
                 <?php if ($id_p == '2')  ?> #000<?php echo $pay['id_p'] ?>
                 <?php if ($id_p == '3')  ?> #00<?php echo $pay['id_p'] ?>
                 <?php if ($id_p == '4')  ?> #0<?php echo $pay['id_p'] ?>
                 <?php if ($id_p == '5')  ?> #<?php echo $pay['id_p'] ?>
               </th>

               <th class="text-center align-middle text_table"><?php echo substr($pay['date_p'], 0, 10); ?></th>
               <th class="text-center align-middle text_table"><?php echo $pay['price_p'] ?></th>
               <th class="text-center align-middle text_table"><a class="link_stu"
                   href="student_info?art_id=<?= $r_stu['id_student'] ?>"><?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?>
               </th>
               <th class="text-center align-middle text_table">
                 <select class="form-control custom-select" name="status_p" id="">
                   <?php if ($pay['status_p'] == 'Payable') { ?>

                   <option value="Payable" selected> Payable </option>
                   <option value="Budgeted"> Budgeted </option>
                   <option value="Paid"> Paid </option>


                   <?php } else if ($pay['status_p'] == 'Budgeted') { ?>

                   <option value="Budgeted" selected> Budgeted </option>
                   <option value="Payable"> Payable </option>
                   <option value="Paid"> Paid </option>

                   <?php } else if ($pay['status_p'] == 'Paid') { ?>

                   <option value="Paid" selected> Paid </option>
                   <option value="Payable"> Payable </option>
                   <option value="Budgeted"> Budgeted </option>

                   <?php } ?>
                 </select>
               </th>
               <th class="text-center align-middle text_table d-block">
                 <button type="submit" class="btn btn_change first">Save Status</button>
                 <a href="#" class="btn btn_change second">View Invoice</button>
               </th>
             </form>
           </tr>

           <?php } ?>

         </tbody>

       </table>

     </div>

   </section>

   <?php } ?>




   <!--// TODO FOOTER ===============================================================-->
   <?php include 'footer.php'; ?>

   <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.36"></script>
   <script src="../assets/js/popper.min.js?ver=1.0.36"></script>
   <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.36"></script>
   <script src="../assets/js/leaflet.js?ver=1.0.36"></script>
   <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.36"></script>
   <script src="../assets/js/map-leaflet.js?ver=1.0.36"></script>
   <script src="../manager/assets/js/rooms.js?ver=1.0.36"></script>
   <script src="../manager/assets/js/galery-homestay.js?ver=1.0.36"></script>
   <!--// * Date Input Safari-->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js?ver=1.0.36"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js?ver=1.0.36"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js?ver=1.0.36"></script>
   <script src="assets/js/date-safari.js?ver=1.0.36"></script>


   <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.36"></script>
   <script src="assets/js/homedit.js?ver=1.0.36" type="module"></script>

 </body>

 </html>