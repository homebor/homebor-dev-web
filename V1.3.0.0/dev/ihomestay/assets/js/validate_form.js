const formulary = document.getElementById('form_contact');

const inputs = document.querySelectorAll('#form_contact input');
const textarea = document.querySelectorAll('#form_contact textarea');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^\d{1,2}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
    message: /^[a-zA-ZÀ-ÿ0-9\s\W_.+-]{1,500}$/ // Description message.
}

const campos = {
    name: false,
    email: false,
    message: false
}


const validateFormulary = (e) =>{
    switch(e.target.name){
        case "name":
            validateField(expresiones.nombre, e.target, 'name');
        break;

        case "email":
            validateField(expresiones.correo, e.target, 'email');
        break;
        
        case "message":
            validateField(expresiones.message, e.target, 'message');
        break;

    }
}

const validateField = (expresion, input, campo) => {
    if(expresion.test(input.value)){
        document.getElementById(`group__${campo}`).classList.remove('form__group-incorrect');
        document.getElementById(`group__${campo}`).classList.add('form__group-correct');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.remove('form__group__input-error-active');
        campos[campo] = true;
    } else{
        document.getElementById(`group__${campo}`).classList.add('form__group-incorrect');
        document.getElementById(`group__${campo}`).classList.remove('form__group-correct');
        document.querySelector(`#group__${campo} .form__group__input-error`).classList.add('form__group__input-error-active');
        campos[campo] = false;
    }
}

inputs.forEach((input) =>{
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
});

textarea.forEach((textarea) =>{
    textarea.addEventListener('keyup', validateFormulary);
    textarea.addEventListener('blur', validateFormulary);
});


formulary.addEventListener('submit', (e) =>{
    e.preventDefault();

    document.getElementById('btn_').classList.add('btn_none');
    document.getElementById('btn__success').classList.add('spin');

    if(campos.name && campos.email && campos.message){

        $.ajax({
            url: 'contact_email.php',
            type: 'POST',
            data: $('#form_contact').serialize(),
            success: function(response) {

                formulary.reset();

                document.getElementById('btn_').classList.remove('btn_none');
                document.getElementById('btn__success').classList.remove('spin');

                document.getElementById('form__message-sucess').classList.add('form__message-sucess-active');

                setTimeout(() =>{
                    document.getElementById('form__message-sucess').classList.remove('form__message-sucess-active');
                }, 5000);

                document.querySelectorAll('.form__group-correct').forEach((border) => {
                    border.classList.remove('form__group-correct');
                });



            }
        });

       

    } else{

        document.getElementById('btn_').classList.remove('btn_none');
        document.getElementById('btn__success').classList.remove('spin');

        document.getElementById('form__message').classList.add('form__message-active');

        setTimeout(() =>{
            document.getElementById('form__message').classList.remove('form__message-active');
        }, 5000);


        
    }
})