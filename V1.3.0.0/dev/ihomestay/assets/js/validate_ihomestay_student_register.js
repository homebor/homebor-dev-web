const D = document;
const W = window;

const form = document.querySelector(".form__register");

// Slide Options Function

const progressOptions = document.querySelectorAll(".progressbar__option");

// validate dields

const inputs = document.querySelectorAll("#form__register input");

// Eye Function

let password = document.getElementById("password");
let viewPassword = document.getElementById("btn__eye");
let click = false;

viewPassword.addEventListener("click", (e) => {
  if (!click) {
    password.type = "text";
    $("#btn__eye").removeClass("fa-eye").addClass("fa-eye-slash");
    click = true;
  } else if (click) {
    password.type = "password";
    $("#btn__eye").removeClass("fa-eye-slash").addClass("fa-eye");
    click = false;
  }
});

// TODO CANVAS

D.querySelector("#btn__register").disabled = true;

const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;
let finishedSignature = false;

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);

  finishedSignature = false;
  D.querySelector("#btn__register").disabled = true;
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
/* $btnDescargar.onclick = () => {}; */

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});
["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
  });
});
["mouseup", "mousedown"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    finishedSignature = true;
    D.querySelector("#btn__register").disabled = false;
  });
});

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^[0-9\s\(\)\+\-]{1,40}$/, // 2 a 14 numeros.
  rooms: /^\d{1,2}$/, // 1 a 14 numeros.
  dir: /^[a-zA-Z0-9\_\-\s\°\S]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s\S]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/, // 8 a 15 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
};

const campos = {
  country: false,
  num_s: false,
  nationality: false,
  pass: false,
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    // Optional Fields
    case "name":
      validateField(expresiones.nombre, e.target, "name");
      break;

    case "l_name":
      validateField(expresiones.nombre, e.target, "l_name");
      break;

    case "password":
      validateField(expresiones.password, e.target, "password");
      break;

    /*case "firstd":
            validateField(expresiones.date, e.target, 'firstd');
        break;*/

    case "dir":
      validateField(expresiones.dir, e.target, "dir");
      break;

    case "city":
      validateField(expresiones.city, e.target, "city");
      break;

    case "state":
      validateField(expresiones.state, e.target, "state");
      break;

    case "p_code":
      validateField(expresiones.p_code, e.target, "p_code");
      break;

    // Required Fields

    case "country":
      validateField(expresiones.dir, e.target, "country");
      break;

    case "num_s":
      validateField(expresiones.phone, e.target, "num_s");
      break;

    case "nationality":
      validateField(expresiones.name, e.target, "nationality");
      break;

    case "pass":
      validateField(expresiones.name, e.target, "pass");
      break;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`group__${campo}`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  } else {
    document.getElementById(`group__${campo}`).classList.add("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.add("form__group__input-error-active");
    campos[campo] = false;
  }
};

inputs.forEach((input) => {
  input.addEventListener("keyup", validateFormulary);
  input.addEventListener("blur", validateFormulary);
});

//! NEW FUNCTIONS TO REGISTER

// ? VARIABLES

// ? FUNCTIONS

function stepButtons(btn) {
  let element = btn;
  let isButtonNext = element.classList.contains("step__button--next");
  let isButtonBack = element.classList.contains("step__button--back");

  if (isButtonNext || isButtonBack) {
    let currentStep = document.getElementById("step-" + element.dataset.step);
    let jumpStep = document.getElementById("step-" + element.dataset.to_step);

    currentStep.addEventListener("animationend", function callback() {
      currentStep.classList.remove("active");
      jumpStep.classList.add("active");
      if (isButtonNext) {
        currentStep.classList.add("to-left");
        progressOptions[element.dataset.to_step - 1].classList.add("active");
        progressOptions[element.dataset.to_step - 1].classList.add("color");
        progressOptions[element.dataset.step - 1].classList.remove("color");
      } else {
        jumpStep.classList.remove("to-left");
        progressOptions[element.dataset.step - 1].classList.remove("active");
        progressOptions[element.dataset.to_step - 1].classList.add("color");
        progressOptions[element.dataset.step - 1].classList.remove("color");
      }

      currentStep.removeEventListener("animationend", callback);
    });
    btn.parentElement.parentElement.parentElement.parentElement.parentElement.scrollIntoView({
      block: "start",
      behavior: "smooth",
    });

    currentStep.classList.add("inactive");
    jumpStep.classList.remove("inactive");
  }
}

async function stepSave(btnSubmit, form) {
  if (btnSubmit.id === "btn__register") {
    if (finishedSignature == true) {
      const enlace = document.createElement("a");
      // Convertir la imagen a Base64 y ponerlo en el enlace
      enlace.href = $canvas.toDataURL();

      document.querySelector("#signature-canvas").value = enlace.href;
    } else {
      setTimeout(() => {
        $("#house_much").fadeIn("slow");

        setTimeout(() => {
          $("#house_much").fadeOut("slow");
        }, 5000);
      }, 100);

      $("#close").on("click", function close() {
        event.preventDefault();
        $("#house_much").fadeOut("slow");
      });
      document.getElementById("contentp").innerHTML = "Please sign to agree to the terms and conditions.";
      document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
      document.getElementById("contentp").style.marginTop = "auto";
      document.getElementById("contentp").style.marginBottom = "auto";
      document.getElementById("contentp").style.color = "#000";
    }
  }

  const formRegister = new FormData(form);
  const DATA = { method: "POST", body: formRegister };
  const jsonSaveRegister = await fetch("./ihomestay_student_agency_action.php", DATA);
  const resultRegister = await jsonSaveRegister.json();

  if (resultRegister.response === "registered complete") {
    if (btnSubmit.id === "btn-back-5") stepButtons(btnSubmit);
    else W.top.location = "student/student_info";
  } else stepButtons(btnSubmit);
}

// ? EVENTS

D.addEventListener("DOMContentLoaded", (e) => {
  if (D.querySelector("#transport_total-db").value === "") D.querySelector("#transport_total-db").value = 0;
  if (D.querySelector("#total_weckly").value === "") D.querySelector("#total_weckly").value = 0;
  if (D.querySelector("#total__price").value === "") D.querySelector("#total__price").value = 0;
});

D.addEventListener("click", (e) => {
  if (e.target.matches(".step__button--back") || e.target.matches(".step__button--next"))
    stepSave(e.target, e.target.parentElement.parentElement);
});

D.addEventListener("change", (e) => {
  if (
    e.target.matches("#meal_p") ||
    e.target.matches("#lodging_type") ||
    e.target.matches("#pick_up") ||
    e.target.matches("#drop_off") ||
    e.target.matches("#accomm-near") ||
    e.target.matches("#accomm-urgent") ||
    e.target.matches("#accomm-guardianship") ||
    e.target.matches("#summer-fee") ||
    e.target.matches("#minor-fee")
  ) {
    let accommodation = 0;
    let transportTotal = 0;
    const mealValue = D.querySelector("#meal_p").value;
    const accommodationValue = D.querySelector("#lodging_type").value;

    if (accommodationValue === "Share") {
      if (mealValue === "Only Room") accommodation = 900 / 4;
      else if (mealValue === "2 Meals") accommodation = 1100 / 4;
      else if (mealValue === "3 Meals") accommodation = 1200 / 4;
    } else if (accommodationValue === "Executive") {
      if (mealValue === "Only Room") accommodation = 1450 / 4;
      else if (mealValue === "2 Meals") accommodation = 1800 / 4;
      else if (mealValue === "3 Meals") accommodation = 1900 / 4;
    } else {
      if (mealValue === "Only Room") accommodation = 1050 / 4;
      else if (mealValue === "2 Meals") accommodation = 1400 / 4;
      else if (mealValue === "3 Meals") accommodation = 1500 / 4;
    }

    if (mealValue === "Only Room") {
      D.querySelector("#food").value = "No";
      D.querySelector("#group__diet").classList.replace("d-block", "d-none");
    } else if (mealValue === "2 Meals" || mealValue === "3 Meals") {
      D.querySelector("#group__diet").classList.replace("d-none", "d-block");
      D.querySelector("#food").value = "Yes";
    }
    /* D.querySelector("#total_weckly").value = "$" + accommodation + " CAD"; */
    D.querySelector("#total_weckly").value = accommodation;

    let pickUp = D.querySelector("#pick_up").value;
    let dropOff = D.querySelector("#drop_off").value;

    if (pickUp === "yes") pickUp = 200;
    else pickUp = 0;

    if (dropOff === "yes") dropOff = 100;
    else dropOff = 0;

    transportTotal = parseFloat(pickUp) + parseFloat(dropOff);
    /* D.querySelector("#transport_total").value = transportTotal; */
    D.querySelector("#transport_total-db").value = transportTotal;
    const $nearToSchool = D.querySelector("#accomm-near").value;
    const $accomUrgent = D.querySelector("#accomm-urgent").value;
    const $summerFee = D.querySelector("#summer-fee").value;
    const $minorFee = D.querySelector("#minor-fee").value;
    /* const $accomGuardianship = D.querySelector("#accomm-guardianship").value; */
    let priceNearSchool = 0;
    let priceAccomUrgent = 0;
    let priceSummerFee = 0;
    let priceMinorFee = 0;
    let priceAccomGuardianship = 0;
    let totalPrice = 0;
    let bookingFee = 250;
    let totalAccommodation = 0;

    if (accommodation !== 0) totalAccommodation = accommodation * 4;
    else totalAccommodation = 0;

    // TODO NEAR TO SCHOOL
    if ($nearToSchool === "yes") priceNearSchool = 35 * 7 * 4;
    else priceNearSchool = 0;

    // TODO ACCOMMODATION URGENT
    if ($accomUrgent === "yes") priceAccomUrgent = 150;
    else priceAccomUrgent = 0;

    // TODO SUMMER FEE
    if ($summerFee === "yes") priceSummerFee = 45 * 4;
    else priceSummerFee = 0;

    // TODO MINOR FEE
    if ($minorFee === "yes") priceMinorFee = 75 * 4;
    else priceMinorFee = 0;

    // TODO GUARDIANSHIP
    /* if ($accomGuardianship === "yes") priceAccomGuardianship = 550;
    else priceAccomGuardianship = 0; */

    totalPrice =
      totalAccommodation + transportTotal + priceNearSchool + priceAccomUrgent + priceSummerFee + priceMinorFee;

    /* if (totalAccommodation != 0 || transportTotal != 0)
      totalPrice = parseFloat(totalAccommodation) + parseFloat(transportTotal) + parseFloat(bookingFee);
    else if (totalAccommodation == 0) totalPrice = parseFloat(transportTotal) + parseFloat(bookingFee);
    else if (transportTotal == 0) totalPrice = parseFloat(totalAccommodation) + parseFloat(bookingFee);
 */
    D.querySelector("#total_weckly-db").value = totalAccommodation;
    D.querySelector("#total__price").value = totalPrice;
  }
});

/* const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento"),
  $idStudent = document.querySelector("#id_student").value;

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
$btnDescargar.onclick = () => {
  const enlace = document.createElement("a");
  // El título
  enlace.download = "signature-" + $idStudent + ".png";
  // Convertir la imagen a Base64 y ponerlo en el enlace
  enlace.href = $canvas.toDataURL();
  // Hacer click en él
  enlace.click();
};

window.obtenerImagen = () => {
  return $canvas.toDataURL();
};

$btnGenerarDocumento.onclick = (e) => {
  e.preventDefault();
  window.open("studentinfo.php");
};

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});
["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
  });
}); */
