<?php
include 'xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

//TOTAL OF HOMESTAY
$sql = "SELECT * FROM `pe_home`"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

//TOTAL OF STUDENTS
$sql2 = "SELECT * FROM `pe_student`"; 
 
$connStatus2 = $link->query($sql2); 
 
$numberOfRows2 = mysqli_num_rows($connStatus2);

//TOTAL OF Agents
$sql3 = "SELECT * FROM `users` WHERE usert='Admin' OR usert='Cordinator' "; 
 
$connStatus3 = $link->query($sql3); 
 
$numberOfRows3 = mysqli_num_rows($connStatus3); 

//TOTAL OF Academies
$sql4 = "SELECT * FROM `academy`"; 
 
$connStatus4 = $link->query($sql4); 
 
$numberOfRows4 = mysqli_num_rows($connStatus4);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Map Toronto</title>
     <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <!--CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/leaflet.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/mapacademy.css">

    <link
      rel="stylesheet"
      href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
      crossorigin=""
    />
    <script
      src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
      integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
      crossorigin=""
    ></script>

     <!-- Mapbox Link -->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

</head>
<body>
<!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <!-- HERO MAP
    =================================================================================================================-->
    <section id="ts-hero" class=" mb-0">

        <!--Fullscreen mode-->
        <div class="ts-full-screen ts-has-horizontal-results w-1001 d-flex1 flex-column1" id="full">

            <!-- MAP
            =========================================================================================================-->
            <div class="ts-map ts-shadow__sm" id="ts-map">

               <!--Map-->

                            <!--Map-->
                           
                                <?php
                                include 'mapboxacademy_toronto.php';
                                ?>
                                

        </div>
    </div>
        <!--end full-screen-->

    </section>
    <!--end ts-hero-->


    <!--end Header-->

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <!--Background Color-->  

     <main id="ts-main">

        <!--PAGE TITLE
            =========================================================================================================-->

             <!--LATEST LISTINGS
        =============================================================================================================-->
        <section id="featured-properties" class="ts-block pt-5" >
            <div class="container py-4">
                <!--Title-->
                <div class="ts-title">
                    <h2>Ranked Top</h2>
                </div>
                <!--Row-->
                <div class="row">
        
     <?php 
        error_reporting(0);
        $sql = "SELECT * FROM `pe_home` WHERE certified = 'yes' ORDER BY id_home DESC LIMIT 4"; //get article id too
        $result = $link->query($sql); //it makes the query
    echo "<tbody>";
      while($rows = $result->fetch_assoc()) {
          echo '<tr> 
    <!--Item 2-->
                    <div class="col-sm-6 col-md-3">
                        <div class="card ts-item ts-card">
                            <!--Ribbon-->
                            <div class="ts-ribbon" id="ribbon-ts">
                                <i class="fa fa-star"></i>
                            </div>
                            <!--Card Image-->
                            <a href="detail.php?art_id='.$rows['id_home'].'" class="card-img ts-item__image" data-bg-image="'.$rows['phome'].'">
                                <figure class="ts-item__info">
                                    <h4>'.$rows['h_name'].'</h4>
                                    <aside>
                                        <i class="fa fa-map-marker mr-2"></i>
                                        '.$rows['city'].'
                                    </aside>
                                </figure>
                                <div class="ts-item__info-badge">'.$rows['status'].'</div>
                            </a>

                            <!--Card Body-->
                            <a href="detail.php?art_id='.$rows['id_home'].'">
                            <div class="card-body ts-item__body">
                                <div class="ts-description-lists">
                                    <dl>
                                        <dt>Bedrooms</dt>
                                        <dd>'.$rows['room'].'</dd>
                                    </dl>
                                    <dl>
                                        <dt>Gender</dt>
                                        <dd>'.$rows['g_pre'].'</dd>
                                    </dl>
                                    <dl>
                                        <dt>Pets</dt>
                                        <dd>'.$rows['pet'].'</dd>
                                    </dl>
                                    <dl>
                                        <dt>Age</dt>
                                        <dd>'.$rows['ag_pre'].'</dd>
                                    </dl>

                                </div>
                            </div>
                            </a>

                            <!--Card Footer-->
                            <a href="detail.php?art_id='.$rows['id_home'].'" class="card-footer">
                                <span class="ts-btn-arrow">Detail</span>
                            </a>

                        </div>
                        <!--end ts-item ts-card-->
                    </div>
                    <!--end Item col-md-4--></tr> ';
     }  

    ?>

                    </div>
                    <!--end Item col-md-4-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

        <!-- Services ICONS
        =============================================================================================================-->
        <section class="ts-block bg-white" data-bg-pattern="assets/img/bg-pattern-dot.png">
            <div class="container py-4">
                <div class="row">
                    <!--Home Information-->
                    <div class="col-sm- col-md-4" id="home-service">
                        <div class="ts-feature">
                            <a href="about_us.php#homestay"><figure class="ts-feature__icon p-2">
                                    <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                               <a href="about_us.php#homestay"> <img src="images/homestay.png" id="size"></a>
                            </figure></a>
                            <a id="homestay2" href="about_us.php#homestay"><h4>Homestay</h4></a>
                            <p id="homestay2">Home Sweet Home.</p>
                        </div>
                    </div>
                    <!--Students-->
                    <div id="student2" class="col-sm-6 col-md-4">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                    <a href="about_us.php#student"><span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span></a>
                                <a href="about_us.php#student"><img src="images/student.png" id="size"></a>
                            </figure>
                            <a href="about_us.php#student"><h4 id="stu2">Students</h4></a>
                            <p id="stu2">Meet New Friends, Culture, Languages.</p>
                        </div>
                    </div>
                    <!--Academies-->
                    <div id="academy2" class="col-sm-6 col-md-3">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                    <a href="work_with_us.php"><span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span></a>
                                <a href="work_with_us.php"><img src="images/agency.png" id="size"></a>
                            </figure>
                            <a href="work_with_us.php"><h4 id="aca2">Agency</h4></a>
                            <p id="aca2">Learn New Languages.</p>
                        </div>
                    </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-block-->
        <br>
        <br>

       <!--PRICING
            =========================================================================================================-->
        <section id="pricing">
            <div class="container">

                <!--Title-->
                <div class="ts-title text-center">
                    <h2>Affordable Prices</h2>
                </div>


                <div class="row no-gutters ts-cards-same-height">

                    <!--Price Box-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#dadada">
                                <h5 class="text-white" data-bg-color="#232159">Basic</h5>
                                <div class="ts-title">
                                    <h3 class="font-weight-normal">Free</h3>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0 border-bottom-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 200 Properties</li>
                                    <li class="list-group-item">5 Agents Profile</li>
                                    <li class="list-group-item">100 Active Students Profiles</li>
                                    <li class="list-group-item">20 Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-outline-primary">Select Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                    <!--Price Box Promoted-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box ts-price-box__promoted">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#ED84C9">
                                <h5 class="text-white" data-bg-color="#982a72">Premium</h5>
                                <div class="ts-title text-white">
                                    <h3 class="font-weight-normal">
                                        <sup>$</sup>9,99
                                    </h3>
                                    <small class="ts-opacity__50">per month</small>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 500 Properties</li>
                                    <li class="list-group-item">10 Agents Profiles</li>
                                    <li class="list-group-item">500 Active Students Profiles</li>
                                    <li class="list-group-item">50 Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-primary">Select Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                    <!--Price Box-->
                    <div class="col-sm-4 col-lg-4">
                        <div class="card text-center ts-price-box">

                            <!--Header-->
                            <div class="card-header" data-bg-color="#dadada">
                                <h5 class="text-white" data-bg-color="#232159">Professional</h5>
                                <div class="ts-title">
                                    <h3 class="font-weight-normal">
                                        <sup>$</sup>19,99
                                    </h3>
                                    <small class="ts-opacity__50">Per Month</small>
                                </div>
                            </div>

                            <!--Body-->
                            <div class="card-body p-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Register Up To 1000 Properties</li>
                                    <li class="list-group-item">100 Agents Profile</li>
                                    <li class="list-group-item">1000 Active Students Profiles</li>
                                    <li class="list-group-item">100 / + Properties Certified by Homebor</li>
                                </ul>
                            </div>

                            <!--Footer-->
                            <div class="card-footer bg-transparent pt-0 border-0">
                                <a href="register.php" class="btn btn-outline-primary">Select Now</a>
                            </div>

                        </div>
                    </div>
                    <!--end price-box-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>


       

      

        <!--PARTNERS ********************************************************************************************-->
        <section id="partners">
            <div class="ts-block py-4">
                <div class="container">
                    <!--block of logos-->
                    <div class="d-block d-md-flex justify-content-between align-items-center text-center ts-partners py-3">
                        <a href="#">
                            <img src="assets/img/logo-01.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-02.png" alt="">
                        </a>
                        <a href="index.php">
                            <img src="assets/logos/page.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-04.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-05.png" alt="">
                        </a>
                    </div>
                    <!--end logos-->
                </div>
                <!--end container-->
            </div>
        </section>


        

    </main>
    <!--end #ts-main-->
<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/leaflet.js"></script>
<script src="assets/js/jQuery.MultiFile.min.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/map-leaflet.js"></script>

<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/sly.min.js"></script>
<script src="assets/js/dragscroll.js"></script>
<script src="assets/js/jquery.scrollbar.min.js"></script>
<script src="assets/js/leaflet.js"></script>
<script src="assets/js/leaflet.markercluster.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/map-leaflet.js"></script>
</body>
</html>