<?php
session_start();
include_once('../../xeon.php');
error_reporting(0);

// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];


$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2 = $query2->fetch_assoc();

$query3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();

$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
$row6=$query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM propertie_control WHERE id_home = '$id'");
$row7=$query7->fetch_assoc();

$query9 = $link->query("SELECT * FROM academy");
$row9=$query9->fetch_assoc();

$query10 = $link->query("SELECT * FROM academy INNER JOIN pe_home ON pe_home.id_home = '$id' AND academy.id_ac = pe_home.a_pre");
$row10=$query10->fetch_assoc();

$query11 = $link->query("SELECT * FROM users WHERE mail = '$row[mail_h]' ");
$row11=$query11->fetch_assoc();

if ($row6['id_m'] != $row7['agency'] ) {
           header("location: index.php");
        }

        
    }
    if ($row5['usert'] != 'Manager') {
        header("location: ../logout.php");   
    }
?>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../homestay/assets/css/carousel.css">
  <link rel="stylesheet" href="../homestay/assets/css/galery.css">
  <link rel="stylesheet" href="assets/css/propertie_preview.css">
  <link rel="stylesheet" href="assets/css/notification.css">

  <link rel="stylesheet" href="../assets/datepicker/css/rome.css">
  <link rel="stylesheet" href="../assets/datepicker/css/style.css">

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <!-- Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>


  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Preview</title>

</head>

<body style="background-color: #F3F8FC;">
  <!-- WRAPPER - FONDO
    =================================================================================================================-->

  <!--*********************************************************************************************************-->
  <!--HEADER **************************************************************************************************-->
  <!--*********************************************************************************************************-->
  <header id="ts-header" class="fixed-top" style="background-color: white;">

    <?php include 'header.php' ?>

  </header>

  <div class="container__title">
    <h1 class="title__register">Homestay Information </h1>

  </div>

  <form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">
    <?php if($row['h_name'] == 'NULL' && $row['num'] == 'NULL' && $row['room'] == 'NULL' && $row['h_type'] == 'NULL' && $row11['mail'] == 'NULL' && $row['m_city'] == 'NULL' && $row['dir'] == 'NULL' && $row['city'] == 'NULL' && $row['state'] == 'NULL' && $row['p_code'] == 'NULL'){}else{ ?>

    <main class="card-main">
      <div class="form__group-house_in form__group">

        <div class="form__target-header">
          <h3 class="title_target"> Basic Information </h3>
        </div>

        <?php if($row['h_name'] == 'NULL' && $row['num'] == 'NULL' && $row['room'] == 'NULL' && $row['h_type'] == 'NULL' && $row11['mail'] == 'NULL'){}else{ ?>

        <h2 class="title__group-section">House Information</h2>

        <div class="row justify-content-center form__group__row_box">

          <?php if(empty($row['h_name']) || $row['h_name'] == 'NULL'){}else{ ?>

          <div class="form__group form__group-h_name col-lg-4" id="form__group-h_name">

            <div class="form__group-hinf">
              <label for="h_name" class="label__name house_info">House Name *</label>
              <label for="h_name" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>

              <p class="form__group-input"> <?php echo $row['h_name'] ?> </p>

            </div>

          </div>

          <?php } 

        if(empty($row['num']) || $row['num'] == 'NULL'){}else{
        
        ?>

          <div class="form__group form__group-num col-lg-4" id="form__group-num">

            <div class="form__group-hinf">
              <label for="p_num" class="label__name house_info">Phone Number</label>
              <label for="p_num" class="l_icon" title="Phone Number"> <i class="icon icon_pnumber"></i> </label>

              <p class="form__group-input"> <?php echo $row['num'] ?> </p>

            </div>

          </div>

          <?php } 

        if(empty($row['room']) || $row['room'] == 'NULL'){}else{
        
        ?>

          <div class="form__group form__group-room col-lg-4" id="form__group-room">

            <div class="form__group-hinf">
              <label for="room" class="label__name hi_room">Rooms in your House</label>
              <label for="room" class="l_icon" title="Rooms in your House"> <i class="icon icon_rooms"></i> </label>

              <p class="form__group-input"> <?php echo $row['room'] ?> </p>

            </div>

          </div>

          <?php } 

        if(empty($row['h_type']) || $row['h_type'] == 'NULL'){}else{
        
        ?>

          <div class="form__group form__group-h_type col-lg-4" id="form__group-h_type">

            <div class="form__group-hinf">
              <label for="h_type" class="label__name house_info">Type of Residence</label>
              <label for="h_type" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>

              <p class="form__group-input"> <?php echo $row['h_type'] ?> </p>

            </div>

          </div>

          <?php } 

        if(empty($row11['mail']) || $row11['mail'] == 'NULL'){}else{

        ?>

          <div class="form__group form__group-mail col-lg-4" id="form__group-mail">

            <div class="form__group-hinf">
              <label for="mail" class="label__name hi_room">Homestay Mail</label>
              <label for="mail" class="l_icon" title="Mail"> <i class="icon icon_mail"></i> </label>

              <p class="form__group-input"> <?php echo $row11['mail'] ?> </p>

            </div>

          </div>

          <?php } ?>

        </div>


        <?php } if($row['m_city'] == 'NULL' && $row['dir'] == 'NULL' && $row['city'] == 'NULL' && $row['state'] == 'NULL' && $row['p_code'] == 'NULL'){}else{ ?>

        <h2 class="title__group-section">Location</h2>

        <div class="row form__location px-4 end_div">


          <div class="form__group-adress col-lg-6">

            <?php

                if(empty($row['m_city']) || $row['m_city'] == 'NULL'){}else{

            ?>

            <div class="form__group-location col-sm-12 form__group-m_city mb-4">
              <label for="m_city" class="label__name location">Main City</label>
              <label for="m_city" class="l_icon" title="Avenue, street, boulevard, etc."> <i
                  class="icon icon_location"></i> </label>

              <input type="text" name="dir" id="dir" class="form__group-input address"
                value="<?php echo $row['m_city'];?>" readonly>

            </div>

            <?php } 

                if(empty($row['dir']) || $row['dir'] == 'NULL'){}else{
            
            ?>

            <div class="form__group-location col-sm-12 form__group-dir" id="form__group-dir">
              <label for="dir" class="label__name location">Address </label>
              <label for="dir" class="l_icon" title="Avenue, street, boulevard, etc."> <i
                  class="icon icon_location"></i> </label>

              <input type="text" name="dir" id="dir" class="form__group-input address" value="<?php echo $row['dir'];?>"
                readonly>

            </div>

            <?php } 

                if(empty($row['city']) || $row['city'] == 'NULL'){}else{
            
            ?>

            <div class="form__group-location col-sm-12 form__group-city" id="form__group-city">
              <label for="city" class="label__name location">City</label>
              <label for="city" class="l_icon" title="City"> <i class="icon icon_location"></i> </label>

              <input type="text" name="city" id="city" class="form__group-input city" value="<?php echo $row['city'];?>"
                readonly>

            </div>

            <?php } 

                if(empty($row['state']) || $row['state'] == 'NULL'){}else{

            ?>
            <div class="form__group-location col-sm-12 form__group-state" id="form__group-state">
              <label for="state" class="label__name location">State / Province</label>
              <label for="state" class="l_icon" title="State / Province"> <i class="icon icon_location"></i> </label>

              <input type="text" name="state" id="state" class="form__group-input state"
                value="<?php echo $row['state'];?>" readonly>

            </div>

            <?php } 

                if(empty($row['p_code']) || $row['p_code'] == 'NULL'){}else{
            
            ?>

            <div class="form__group-location col-sm-12 form__group-pcode" id="form__group-p_code">
              <label for="p_code" class="label__name location">Postal Code</label>
              <label for="p_code" class="l_icon" title="Postal Code"> <i class="icon icon_location"></i> </label>

              <input type="text" name="p_code" id="p_code" class="form__group-input pcode"
                value="<?php echo $row['p_code'];?>" readonly>

            </div>

            <?php } ?>

          </div>

          <div class="form__group-map col-sm-6">
            <div id='map' style="transform: translateY(12.5%);"></div>
          </div>

          <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>

          <script>
          mapboxgl.accessToken =
            'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
          console.log(mapboxgl.accessToken);
          var client = new MapboxClient(mapboxgl.accessToken);
          console.log(client);


          var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
          var test = client.geocodeForward(address, function(err, data, res) {
            // data is the geocoding result as parsed JSON
            // res is the http response, including: status, headers and entity properties

            console.log(res);
            console.log(res.url);
            console.log(data);

            var coordinates = data.features[0].center;

            var map = new mapboxgl.Map({
              container: 'map',
              style: 'mapbox://styles/mapbox/streets-v10',
              center: coordinates,
              zoom: 14
            });
            new mapboxgl.Marker()
              .setLngLat(coordinates)
              .addTo(map);


          });
          </script>

        </div><br>

        <?php } ?>

      </div>

    </main>


    <?php } if($row['phome'] == 'NULL' && $row4['pliving'] == 'NULL' && $row4['fp'] == 'NULL' && $row4['parea1'] == 'NULL' && $row4['parea2'] == 'NULL' && $row4['parea3'] == 'NULL' && $row4['parea4'] == 'NULL' && $row4['pbath1'] == 'NULL' && $row4['pbath2'] == 'NULL' && $row4['pbath3'] == 'NULL' && $row4['pbath4'] == 'NULL' && $row2['date1'] == 'NULL' && $row2['date2'] == 'NULL' && $row2['date3'] == 'NULL' && $row2['date4'] == 'NULL' && $row2['date5'] == 'NULL' && $row2['date6'] == 'NULL' && $row2['date7'] == 'NULL' && $row2['date8'] == 'NULL'){}else{ ?>

    <main class="card-main">
      <div class="form__group-house_in form__group">

        <div class="form__target-header">
          <h3 class="title_target"> House Details </h3>
        </div>

        <h2 class="title__group-section">Photo Gallery</h2>


        <div class="row"
          style="margin: 0; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

          <?php 

            if(empty($row['phome']) || $row['phome'] == 'NULL'){}else{
            
        ?>

          <div class="tarjet">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
              Frontage Photo </h4>


            <div class="div-img-r" align="center">


              <div class="carousel-item active">

                <img src="<?php echo '../../'.$row['phome'].''?>" class="add-photo">

              </div>

            </div>

          </div>

          <?php } 

            if(empty($row4['pliving']) || $row4['pliving'] == 'NULL'){}else{
        
        ?>

          <!-- Living Room Photo -->
          <div class="tarjet">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
              Living Room Photo </h4>

            <div class="div-img-r" align="center">

              <div class="carousel-item active">

                <img id="preview-ii" class="add-photo" src="<?php echo '../../'.$row4['pliving'].''?>">

              </div>

            </div>

          </div>

          <?php } 

            if(empty($row4['fp']) || $row4['fp'] == 'NULL'){}else{
        
        ?>


          <div class="tarjet">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; ">
              Family Picture </h4>

            <div class="div-img-r" align="center">


              <div class="carousel-item active">

                <img id="preview-fp" class="add-photo" src="<?php echo '../../'.$row4['fp'].''?>">

              </div>

            </div>


          </div>

          <?php } ?>


        </div>




        <!-- Area Photos -->

        <div class="row div-area"
          style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

          <?php 

            if(empty($row4['parea1']) || $row4['parea1'] == 'NULL'){}else{
        
        ?>

          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              Kitchen </h4>

            <div class="carousel-item active">

              <img id="preview-iii" class="add-photo" src="<?php echo '../../'.$row4['parea1'].''?>">

            </div>

          </div>

          <?php } 

            if(empty($row4['parea2']) || $row4['parea2'] == 'NULL'){}else{
        
        ?>


          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              Dining Room </h4>



            <div class="carousel-item active">

              <img id="preview-iv" class="add-photo" src="<?php echo '../../'.$row4['parea2'].''?>">

            </div>


          </div>

          <?php } 

            if(empty($row4['parea3']) || $row4['parea3'] == 'NULL'){}else{
        
        ?>

          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              House Area 3 </h4>

            <div class="carousel-item active">

              <img id="preview-v" class="add-photo" src="<?php echo '../../'.$row4['parea3'].''?>">

            </div>

          </div>

          <?php } 

            if(empty($row4['parea4']) || $row4['parea4'] == 'NULL'){}else{

        ?>

          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              House Area 4 </h4>

            <div class="carousel-item active">

              <img id="preview-vi" class="add-photo" src="<?php echo '../../'.$row4['parea4'].''?>">

            </div>

          </div>


          <?php } ?>


        </div>



        <!-- Bathroom Photos -->

        <div class="row gallery-end"
          style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;">

          <?php 

            if(empty($row4['pbath1']) || $row4['pbath1'] == 'NULL'){}else{

        ?>


          <div class="tarjet">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
              Bathroom Photo 1 </h4>

            <div class="div-img-r" align="center">

              <div class="carousel-item active">

                <img id="preview-vii" class="add-photo" src="<?php echo '../../'.$row4['pbath1'].''?>">

              </div>

            </div>

          </div>

          <?php } 
        
            if(empty($row4['pbath2']) || $row4['pbath2'] == 'NULL'){}else{

        ?>


          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              Bathroom 2 </h4>

            <div class="carousel-item active">

              <img id="preview-viii" class="add-photo" src="<?php echo '../../'.$row4['pbath2'].''?>">

            </div>

          </div>

          <?php } 

            if(empty($row4['pbath3']) || $row4['pbath3'] == 'NULL'){}else{

        ?>

          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              Bathroom 3 </h4>

            <div class="carousel-item active">

              <img id="preview-ix" class="add-photo" src="<?php echo '../../'.$row4['pbath3'].''?>">

            </div>

          </div>


          <?php } if(empty($row4['pbath4']) || $row4['pbath4'] == 'NULL'){}else{ ?>

          <div class="tarjet" align="center">

            <h4 class="title-room"
              style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
              Bathroom 3 </h4>

            <div class="carousel-item active">

              <img id="preview-x" class="add-photo" src="<?php echo '../../'.$row4['pbath4'].''?>">

            </div>

          </div>

          <?php } ?>

        </div>

        <div class="m-0 p-0 bedrooms">

          <h2 class="title__group-section" id="title__bedrooms"> Bedrooms Information </h2>

          <div class="row"
            style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;">

            <?php if($row4['proom1'] == 'NULL' && $row4['proom1_2'] == 'NULL' && $row4['proom1_3'] == 'NULL' && $row2['type1'] == 'NULL' && $row2['bed1'] == 'NULL' && $row2['date1'] == 'NULL' && $row2['food1'] == 'NULL' && $row2['aprox1'] == '0' && $row2['aprox_a1'] == '0'){}else{ ?>

            <!-- Room 1 -->
            <div class="tarjet tarjet-rooms " id="room1">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 1 </h4>

              <?php if($row4['proom1'] == 'NULL' && $row4['proom1_2'] == 'NULL' && $row4['proom1_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom1']) || $row4['proom1'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom1_2']) || $row4['proom1_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom1_3']) || $row4['proom1_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>

                  <div class="carousel-inner">

                    <?php if(empty($row4['proom1']) || $row4['proom1'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xi" class="add-photo " src="../../<?php echo $row4['proom1'] ?>">

                    </div>

                    <?php } if(empty($row4['proom1_2']) || $row4['proom1_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xi-ii" class="add-photo " src="../../<?php echo $row4['proom1_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom1_3']) || $row4['proom1_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xi-iii" class="add-photo" src="../../<?php echo $row4['proom1_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom1_2'] == 'NULL' && $row4['proom1_3'] == 'NULL' || $row4['proom1'] == 'NULL' && $row4['proom1_3'] == 'NULL' || $row4['proom1_2'] == 'NULL' && $row4['proom1'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>

              </div>

              <?php } ?>

              <br>

              <?php if($row2['type1'] == 'NULL' && $row2['bed1'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type1'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type1'] ?></p>

                </div>

                <?php } if($row2['bed1'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed1'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 
                
                if($row2['date1'] == 'NULL' && $row2['food1'] == 'NULL'){}else{
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date1'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date1'] ?></p>

                </div>

                <?php } if($row2['food1'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food1'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                
                    if($row2['aprox1'] == 'NULL' && $row2['aprox_a1'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox1'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();"
                        name="approx1" maxlength="15" value="<?php echo $row2['aprox1'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a1'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();"
                        name="approx_a1" maxlength="15" value="<?php echo $row2['aprox_a1'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>


            </div>

            <?php } 

            if($row4['proom2'] == 'NULL' && $row4['proom2_2'] == 'NULL' && $row4['proom2_3'] == 'NULL' && $row2['type2'] == 'NULL' && $row2['bed2'] == 'NULL' && $row2['date2'] == 'NULL' && $row2['food2'] == 'NULL' && $row2['aprox2'] == '0' && $row2['aprox_a2'] == '0'){}else{

            ?>

            <!-- Room2 -->

            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 2 </h4>

              <?php if($row4['proom2'] == 'NULL' && $row4['proom2_2'] == 'NULL' && $row4['proom2_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom2']) || $row4['proom2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom2_2']) || $row4['proom2_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom2_3']) || $row4['proom2_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>

                  <div class="carousel-inner">

                    <?php if(empty($row4['proom2']) || $row4['proom2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xii" class="add-photo " src="../../<?php echo $row4['proom2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom2_2']) || $row4['proom2_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xii-ii" class="add-photo " src="../../<?php echo $row4['proom2_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom2_3']) || $row4['proom2_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xii-iii" class="add-photo " src="../../<?php echo $row4['proom2_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom2_2'] == 'NULL' && $row4['proom2_3'] == 'NULL' || $row4['proom2'] == 'NULL' && $row4['proom2_3'] == 'NULL' || $row4['proom2_2'] == 'NULL' && $row4['proom2'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>

              </div>

              <?php } ?>

              <br>

              <?php if($row2['type2'] == 'NULL' && $row2['bed2'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type2'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type2'] ?></p>

                </div>


                <?php } if($row2['bed2'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed2'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                if($row2['date2'] == 'NULL' && $row2['food2'] == 'NULL'){}else{
                
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date2'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date2'] ?></p>

                </div>

                <?php } if($row2['food2'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food2'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                
                    if($row2['aprox2'] == 'NULL' && $row2['aprox_a2'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox2'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();"
                        name="approx2" maxlength="15" value="<?php echo $row2['aprox2'] ?>" readonly>

                    </div>

                    <?php } if($row2['aprox_a2'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();"
                        name="approx_a2" maxlength="15" value="<?php echo $row2['aprox_a2'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>


            </div>

            <?php } 

            if($row4['proom3'] == 'NULL' && $row4['proom3_2'] == 'NULL' && $row4['proom3_3'] == 'NULL' && $row2['type3'] == 'NULL' && $row2['bed3'] == 'NULL' && $row2['date3'] == 'NULL' && $row2['food3'] == 'NULL' && $row2['aprox3'] == '0' && $row2['aprox_a3'] == '0'){}else{

            ?>

            <!-- Room 3 -->


            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 3 </h4>

              <?php if($row4['proom3'] == 'NULL' && $row4['proom3_2'] == 'NULL' && $row4['proom3_3'] == 'NULL'){}else{ ?>


              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom3']) || $row4['proom3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom3_2']) || $row4['proom3_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom3_3']) || $row4['proom3_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom3']) || $row4['proom3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xiii" class="add-photo " src="../../<?php echo $row4['proom3'] ?>">

                    </div>

                    <?php } if(empty($row4['proom3_2']) || $row4['proom3_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xiii-ii" class="add-photo " src="../../<?php echo $row4['proom3_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom3_3']) || $row4['proom3_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xiii-iii" class="add-photo " src="../../<?php echo $row4['proom3_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom3_2'] == 'NULL' && $row4['proom3_3'] == 'NULL' || $row4['proom3'] == 'NULL' && $row4['proom3_3'] == 'NULL' || $row4['proom3_2'] == 'NULL' && $row4['proom3'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>


              </div>

              <?php } ?>

              <br>

              <?php if($row2['type3'] == 'NULL' && $row2['bed3'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type3'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type3'] ?></p>

                </div>

                <?php } if($row2['bed3'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed3'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date3'] == 'NULL' && $row2['food3'] == 'NULL'){}else{
                    
                ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date3'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date3'] ?></p>

                </div>

                <?php } if($row2['food3'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food3'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox3'] == 'NULL' && $row2['aprox_a3'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox3'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();"
                        name="approx3" maxlength="15" value="<?php echo $row2['aprox3'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a3'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();"
                        name="approx_a3" maxlength="15" value="<?php echo $row2['aprox_a3'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>

            </div>

            <?php } 

            if($row4['proom4'] == 'NULL' && $row4['proom4_2'] == 'NULL' && $row4['proom4_3'] == 'NULL' && $row2['type4'] == 'NULL' && $row2['bed4'] == 'NULL' && $row2['date4'] == 'NULL' && $row2['food4'] == 'NULL' && $row2['aprox4'] == '0' && $row2['aprox_a4'] == '0'){}else{

            ?>


            <!-- Room 4 -->
            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 4 </h4>

              <?php if($row4['proom4'] == 'NULL' && $row4['proom4_2'] == 'NULL' && $row4['proom4_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom4']) || $row4['proom4'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom4_2']) || $row4['proom4_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom4_3']) || $row4['proom4_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom4']) || $row4['proom4'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xiv" class="add-photo " src="../../<?php echo $row4['proom4'] ?>">

                    </div>

                    <?php } if(empty($row4['proom4_2']) || $row4['proom4_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xiv-ii" class="add-photo " src="../../<?php echo $row4['proom4_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom4_3']) || $row4['proom4_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xiv-iii" class="add-photo " src="../../<?php echo $row4['proom4_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom4_2'] == 'NULL' && $row4['proom4_3'] == 'NULL' || $row4['proom4'] == 'NULL' && $row4['proom4_3'] == 'NULL' || $row4['proom4_2'] == 'NULL' && $row4['proom4'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>


              </div>

              <?php } ?>
              <br>

              <?php if($row2['type4'] == 'NULL' && $row2['bed4'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type4'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type4'] ?></p>

                </div>

                <?php } if($row2['bed4'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed4'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date4'] == 'NULL' && $row2['food4'] == 'NULL'){}else{
                    
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date4'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date4'] ?></p>

                </div>

                <?php } if($row2['food4'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food4'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox4'] == 'NULL' && $row2['aprox_a4'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">
                    <?php if($row2['aprox4'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();"
                        name="approx4" maxlength="15" value="<?php echo $row2['aprox4'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a4'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();"
                        name="approx_a4" maxlength="15" value="<?php echo $row2['aprox_a4'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>
            </div>

            <?php } 

            if($row4['proom5'] == 'NULL' && $row4['proom5_2'] == 'NULL' && $row4['proom5_3'] == 'NULL' && $row2['type5'] == 'NULL' && $row2['bed5'] == 'NULL' && $row2['date5'] == 'NULL' && $row2['food5'] == 'NULL' && $row2['aprox5'] == '0' && $row2['aprox_a5'] == '0'){}else{

            ?>

            <!-- Room 5 -->

            <div class="tarjet tarjet-rooms ">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 5 </h4>

              <?php if($row4['proom5'] == 'NULL' && $row4['proom5_2'] == 'NULL' && $row4['proom5_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom5']) || $row4['proom5'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom5_2']) || $row4['proom5_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom5_3']) || $row4['proom5_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom5']) || $row4['proom5'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xv" class="add-photo " src="../../<?php echo $row4['proom5'] ?>">

                    </div>

                    <?php } if(empty($row4['proom5_2']) || $row4['proom5_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xv-ii" class="add-photo " src="../../<?php echo $row4['proom5_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom5_3']) || $row4['proom5_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xv-iii" class="add-photo " src="../../<?php echo $row4['proom5_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom5_2'] == 'NULL' && $row4['proom5_3'] == 'NULL' || $row4['proom5'] == 'NULL' && $row4['proom5_3'] == 'NULL' || $row4['proom5_2'] == 'NULL' && $row4['proom5'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>

              </div>

              <?php } ?>

              <br>

              <?php if($row2['type5'] == 'NULL' && $row2['bed5'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type5'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type5'] ?></p>

                </div>

                <?php } if(['bed5'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed5'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date5'] == 'NULL' && $row2['food5'] == 'NULL'){}else{
                    
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date5'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date5'] ?></p>

                </div>

                <?php } if($row2['food5'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food5'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox5'] == 'NULL' && $row2['aprox_a5'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox5'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();"
                        name="approx5" maxlength="15" value="<?php echo $row2['aprox5'] ?>" readonly>

                    </div>

                    <?php } if($row2['aprox_a5'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();"
                        name="approx_a5" maxlength="15" value="<?php echo $row2['aprox_a5'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>
            </div>



            <?php } 

            if($row4['proom6'] == 'NULL' && $row4['proom6_2'] == 'NULL' && $row4['proom6_3'] == 'NULL' && $row2['type6'] == 'NULL' && $row2['bed6'] == 'NULL' && $row2['date6'] == 'NULL' && $row2['food6'] == 'NULL' && $row2['aprox6'] == '0' && $row2['aprox_a6'] == '0'){}else{

            ?>



            <!-- Romm 6 -->

            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 6 </h4>

              <?php if($row4['proom6'] == 'NULL' && $row4['proom6_2'] == 'NULL' && $row4['proom6_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom6']) || $row4['proom6'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom6_2']) || $row4['proom6_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom6_3']) || $row4['proom6_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom6']) || $row4['proom6'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xvi" class="add-photo " src="../../<?php echo $row4['proom6'] ?>">

                    </div>

                    <?php } if(empty($row4['proom6_2']) || $row4['proom6_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xvi-ii" class="add-photo " src="../../<?php echo $row4['proom6_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom6_3']) || $row4['proom6_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xvi-iii" class="add-photo " src="../../<?php echo $row4['proom6_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom6_2'] == 'NULL' && $row4['proom6_3'] == 'NULL' || $row4['proom6'] == 'NULL' && $row4['proom6_3'] == 'NULL' || $row4['proom6_2'] == 'NULL' && $row4['proom6'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>


              </div>

              <?php } ?>
              <br>


              <?php if($row2['type6'] == 'NULL' && $row2['bed6'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type6'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type6'] ?></p>

                </div>

                <?php } if($row2['bed6'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed6'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date6'] == 'NULL' && $row2['food6'] == 'NULL'){}else{
                    
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date6'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date6'] ?></p>

                </div>

                <?php } if($row2['food6'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food6'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox6'] == 'NULL' && $row2['aprox_a6'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox6'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();"
                        name="approx6" maxlength="15" value="<?php echo $row2['aprox6'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a6'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();"
                        name="approx_a6" maxlength="15" value="<?php echo $row2['aprox_a6'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>
            </div>

            <?php } 

            if($row4['proom7'] == 'NULL' && $row4['proom7_2'] == 'NULL' && $row4['proom7_3'] == 'NULL' && $row2['type7'] == 'NULL' && $row2['bed7'] == 'NULL' && $row2['date7'] == 'NULL' && $row2['food7'] == 'NULL' && $row2['aprox7'] == '0' && $row2['aprox_a7'] == '0'){}else{

            ?>


            <!-- Room 7 -->

            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 7 </h4>

              <?php if($row4['proom7'] == 'NULL' && $row4['proom7_2'] == 'NULL' && $row4['proom7_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom7']) || $row4['proom7'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom7_2']) || $row4['proom7_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom7_3']) || $row4['proom7_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom7']) || $row4['proom7'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xvii" class="add-photo " src="../../<?php echo $row4['proom7'] ?>">

                    </div>

                    <?php } if(empty($row4['proom7_2']) || $row4['proom7_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xvii-ii" class="add-photo " src="../../<?php echo $row4['proom7_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom7_3']) || $row4['proom7_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xvii-iii" class="add-photo " src="../../<?php echo $row4['proom7_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom7_2'] == 'NULL' && $row4['proom7_3'] == 'NULL' || $row4['proom7'] == 'NULL' && $row4['proom7_3'] == 'NULL' || $row4['proom7_2'] == 'NULL' && $row4['proom7'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>


              </div>
              <?php } ?>
              <br>

              <?php if($row2['type7'] == 'NULL' && $row2['bed7'] == 'NULL'){}else{ ?>

              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type7'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type7'] ?></p>

                </div>

                <?php } if($row2['bed7'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed7'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date7'] == 'NULL' && $row2['food7'] == 'NULL'){}else{
                    
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date7'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date7'] ?></p>

                </div>

                <?php } if($row2['food7'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food7'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox7'] == 'NULL' && $row2['aprox_a7'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox7'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();"
                        name="approx7" maxlength="15" value="<?php echo $row2['aprox7'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a7'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();"
                        name="approx_a7" maxlength="15" value="<?php echo $row2['aprox_a7'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>

            </div>

            <?php } 

            if($row4['proom8'] == 'NULL' && $row4['proom8_2'] == 'NULL' && $row4['proom8_3'] == 'NULL' && $row2['type8'] == 'NULL' && $row2['bed8'] == 'NULL' && $row2['date8'] == 'NULL' && $row2['food8'] == 'NULL' && $row2['aprox8'] == '0' && $row2['aprox_a8'] == '0'){}else{

            ?>


            <!-- Room 8 -->

            <div class="tarjet tarjet-rooms">

              <h4 class="title-room"
                style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
                Room 8 </h4>

              <?php if($row4['proom8'] == 'NULL' && $row4['proom8_2'] == 'NULL' && $row4['proom8_3'] == 'NULL'){}else{ ?>

              <div class="div-img-r" align="center">

                <div id="carouselExampleIndicators-8" class="carousel slide" data-ride="carousel">

                  <ol class="carousel-indicators">
                    <?php if(empty($row4['proom8']) || $row4['proom8'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-8" data-slide-to="0" class="active"> </li>

                    <?php } if(empty($row4['proom8_2']) || $row4['proom8_2'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-8" data-slide-to="1"></li>

                    <?php } if(empty($row4['proom8_3']) || $row4['proom8_3'] == 'NULL'){}else{ ?>

                    <li data-target="#carouselExampleIndicators-8" data-slide-to="2"></li>

                    <?php } ?>
                  </ol>


                  <div class="carousel-inner">

                    <?php if(empty($row4['proom8']) || $row4['proom8'] == 'NULL'){}else{ ?>

                    <div class="carousel-item active">

                      <img id="preview-xviii" class="add-photo " src="../../<?php echo $row4['proom8'] ?>">

                    </div>

                    <?php } if(empty($row4['proom8_2']) || $row4['proom8_2'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xviii-ii" class="add-photo " src="../../<?php echo $row4['proom8_2'] ?>">

                    </div>

                    <?php } if(empty($row4['proom8_3']) || $row4['proom8_3'] == 'NULL'){}else{ ?>

                    <div class="carousel-item">

                      <img id="preview-xviii-iii" class="add-photo " src="../../<?php echo $row4['proom8_3'] ?>">

                    </div>

                    <?php } ?>

                  </div>

                  <?php if($row4['proom8_2'] == 'NULL' && $row4['proom8_3'] == 'NULL' || $row4['proom8'] == 'NULL' && $row4['proom8_3'] == 'NULL' || $row4['proom8_2'] == 'NULL' && $row4['proom8'] == 'NULL'){}else{ ?>

                  <a class="carousel-control-prev" href="#carouselExampleIndicators-8" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="carousel-control-next" href="#carouselExampleIndicators-8" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                  <?php } ?>

                </div>


              </div>
              <?php } ?>


              <br>

              <?php if($row2['type7'] == 'NULL' && $row2['bed7'] == 'NULL'){}else{ ?>
              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['type8'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt=""
                      title="Type Accomodation">
                  </div>

                  <p class="text__room"><?php echo $row2['type8'] ?></p>

                </div>

                <?php } if($row2['bed8'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                  </div>

                  <p class="text__room"><?php echo $row2['bed8'] ?></p>

                </div>

                <?php } ?>

              </div><br>

              <?php } 

                    if($row2['date8'] == 'NULL' && $row2['food8'] == 'NULL'){}else{
                    
                ?>


              <div class="row init ml-auto" style="width: 80%">

                <?php if($row2['date8'] == 'NULL'){}else{ ?>

                <div class="details in-d">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                  </div>

                  <p class="text__room"><?php echo $row2['date8'] ?></p>

                </div>

                <?php } if($row2['food8'] == 'NULL'){}else{ ?>

                <div class="details">

                  <div class="icon-type">
                    <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                  </div>

                  <p class="text__room"><?php echo $row2['food8'] ?></p>

                </div>

                <?php } ?>

              </div>

              <?php } 
                    
                    if($row2['aprox8'] == 'NULL' && $row2['aprox_a8'] == 'NULL'){}else{
                ?>

              <div class="row init ml-auto mr-auto" style="width: 90%; margin-top: 10%;">

                <div class="container">

                  <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD
                    $ </p>

                  <div style="display: flex;">

                    <?php if($row2['aprox8'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>

                      <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction8();"
                        name="approx8" maxlength="15" value="<?php echo $row2['aprox8'] ?>" readonly>


                    </div>

                    <?php } if($row2['aprox_a8'] == 'NULL'){}else{ ?>

                    <div class="col-lg-6">

                      <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>

                      <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction8_2();"
                        name="approx_a8" maxlength="15" value="<?php echo $row2['aprox_a8'] ?>" readonly>

                    </div>

                    <?php } ?>

                  </div>

                </div>

              </div>

              <?php } ?>

              <br>
            </div>

            <?php } ?>

          </div>

        </div>

    </main>

    <?php } 

if($row['des'] == 'NULL' && $row['a_pre'] == 'NULL' && $row['g_pre'] == 'NULL' && $row['ag_pre'] == 'NULL' && $row['status'] == 'NULL' && $row['smoke'] == 'NULL' && $row['m_service'] == 'NULL' && $row['y_service'] == 'NULL' && $row['vegetarians'] == 'no' && $row['halal'] == 'no' && $row['kosher'] == 'no' && $row['none'] == 'no' && $row['pet'] == 'NULL' && $row['pet_num'] == '0' && $row['dog'] == 'no' && $row['cat'] == 'no' && $row['other'] == 'no' && $row['type_pet'] == 'NULL'){}else{

?>



    <main class="card-main">

      <div class="form__group-house_in form__group">

        <div class="form__target-header">
          <h3 class="title_target"> Additional Information </h3>
        </div>

        <?php if(empty($row['des']) || $row['des'] == 'NULL'){}else{ ?>

        <h2 class="title__group-section">Description of your House</h2>

        <div class="form__group-description_div">

          <div class="form__group-description">

            <label for="address" class="l_icon" title="Description"> <i class="icon icon_description"></i> </label>

            <p rows="3" maxlength="255" class="form__group-input description" name="des" id="des"
              placeholder="Description: Describe your house using few words, no special characters.">
              <?php echo $row['des'] ?> </p>

          </div>


        </div>

        <?php } 
    
     if($row['a_pre'] == 'NULL' && $row['g_pre'] == 'NULL' && $row['ag_pre'] == 'NULL' && $row['status'] == 'NULL' && $row['smoke'] == 'NULL' && $row['m_service'] == 'NULL' && $row['y_service'] == 'NULL'){}else{

    ?>

        <h2 class="title__group-section">Preferences</h2>

        <div class="row form__group-preferences form__group-preferences1">

          <?php if(empty($row['a_pre']) || $row['a_pre'] == 'NULL'){}else{ ?>

          <div class="form__group-academy col-xl-12 mb-3">
            <label for="a_pre" class="label__name preference">Academy Preference</label>
            <label for="a_pre" class="l_icon" title="Academy Preference"> <i class="icon icon_p_academy"></i> </label>

            <input type="text" class="form__group-input academy" readonly
              value="<?php echo $row10['name_a'] . ', ' . $row10['dir_a'] ?>">

          </div>

          <?php } 

        if(empty($row['g_pre']) || $row['g_pre'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-gender col-xl-4">
            <label for="g_pre" class="label__name preference">Gender Preference</label>
            <label for="g_pre" class="l_icon" title="Gender Preference"> <i class="icon icon_gender"></i> </label>

            <input type="text" class="form__group-input gender" readonly value="<?php echo $row['g_pre']  ?>">

          </div>

          <?php } 

        if(empty($row['ag_pre']) || $row['ag_pre'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-age col-xl-4">
            <label for="ag_pre" class="label__name preference">Age Preference</label>
            <label for="ag_pre" class="l_icon" title="Age Preference"> <i class="icon icon_age"></i> </label>

            <input type="text" class="form__group-input gender" readonly value="<?php echo $row['ag_pre']  ?>">

          </div>

          <?php } 

        if(empty($row['status']) || $row['status'] == 'NULL'){}else{
    
    ?>


          <div class="form__group-status form__group col-xl-4">
            <label for="status" class="label__name preference">House Status</label>
            <label for="status" class="l_icon" title="House Status"> <i class="icon icon_status"></i> </label>

            <input type="text" class="form__group-input gender" readonly value="<?php echo $row['status']  ?>">

          </div>

          <?php } 

        if(empty($row['smoke']) || $row['smoke'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-smokers form__group col-xl-4">
            <label for="smoke" class="label__name preference">Smokers Politics</label>
            <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_smokers"></i> </label>

            <input type="text" class="form__group-input gender" readonly value="<?php echo $row['smoke']  ?>">

          </div>

          <?php } 

        if(empty($row['m_service']) || $row['m_service'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-smokers form__group col-xl-4">
            <label for="m_service" class="label__name preference">Meals Service</label>
            <label for="m_service" class="l_icon" title="Smokers Politics"> <i class="icon icon_meals"></i> </label>

            <input type="text" class="form__group-input gender" readonly value="<?php echo $row['m_service']  ?>">
          </div>

          <?php } 

        if(empty($row['y_service']) || $row['y_service'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-smokers form__group col-xl-4">
            <label for="y_service" class="label__name preference">Since when have you been a Homestay?</label>
            <label for="y_service" class="l_icon" title="Smokers Politics"> <i class="icon icon_years"></i> </label>

            <input type="date" class="form__group-input y_hom" id="y_service" name="y_service"
              value="<?php echo $row['y_service'] ?>" readonly>

          </div>

          <?php } ?>

        </div>

        <?php } if($row['vegetarians'] == 'no' && $row['kosher'] == 'no' && $row['halal'] == 'no' && $row['lactose'] == 'no' && $row['gluten'] == 'no' && $row['pork'] == 'no' && $row['pet'] == 'NULL' && $row['pet_num'] == '0' && $row['dog'] == 'no' && $row['cat'] == 'no' && $row['other'] == 'no' && $row['type_pet'] == 'NULL'){}else{ ?>


        <div class="row form__group-preferences">

          <div class="form__group-diet col-lg-5 p-0">
            <h2 class="title__group-preference"><i class="icon icon-diet"></i> Special Diet</h2>

            <div class="form__group-diet-in">

              <div class="custom-control custom-checkbox">

                <?php if(empty($row['vegetarians']) || $row['vegetarians'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" value="yes"
                  disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" value="yes"
                  checked disabled>
                <?php } ?>
                <label class="custom-control-label" for="vegetarians">Vegetarians</label>
              </div>

              <div class="custom-control custom-checkbox">
                <?php if(empty($row['halal']) || $row['halal'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" checked
                  disabled>
                <?php } ?>
                <label class="custom-control-label" for="halal">Halal (Muslims)</label>
              </div>

            </div>

            <div class="form__group-diet-in">

              <div class="custom-control custom-checkbox">
                <?php if(empty($row['kosher']) || $row['kosher'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" checked
                  disabled>
                <?php } ?>
                <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
              </div>

              <div class="custom-control custom-checkbox">
                <?php if(empty($row['lactose']) || $row['lactose'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" checked
                  disabled>
                <?php } ?>
                <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
              </div>

            </div>

            <div class="form__group-diet-in">

              <div class="custom-control custom-checkbox">
                <?php if(empty($row['gluten']) || $row['gluten'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" checked
                  disabled>
                <?php } ?>
                <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
              </div>

              <div class="custom-control custom-checkbox">
                <?php if(empty($row['pork']) || $row['pork'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="no" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="no" checked disabled>
                <?php } ?>
                <label class="custom-control-label" for="pork">No Pork</label>
              </div>

            </div>

            <div class="form__group-diet-in">

              <div class="custom-control custom-checkbox-none custom-checkbox">
                <?php if(empty($row['none']) || $row['none'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" checked disabled>
                <?php } ?>
                <label class="custom-control-label" for="none">None</label>
              </div>

            </div>

          </div>

          <div class="form__group-pets col-lg-5 p-0">
            <h2 class="title__group-preference"><i class="icon icon-pets"></i> Pets</h2>

            <div class="form__group-pets-in">

              <?php if(empty($row['pet']) || $row['pet'] == 'NULL'){}else{ ?>

              <div class="form__group-ypets">
                <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>

                <input type="text" class="form__group-input ypets" id="pet" name="pet" value="<?php echo $row['pet'] ?>"
                  readonly>


              </div>

              <?php } 
            
                if(empty($row['pet_num']) || $row['pet_num'] == 'NULL'){}else{

            ?>


              <div class="form__group-npets">
                <label for="npets" class="l_icon" title="Pets Number"> <i class="icon icon_pets"></i> </label>

                <input type="number" class="form__group-input npets" id="npets" name="pet_num" min="1" max="10"
                  placeholder="Pets Number" value="<?php echo $row['pet_num'] ?>">

              </div>

              <?php } ?>

            </div>

            <div class="form__group-pets-in">
              <div class="custom-control custom-checkbox custom-checkbox-pets">
                <?php if(empty($row['dog']) || $row['dog'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input custom-checkbox-dogs" id="dog" name="dog" value="yes"
                  disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input custom-checkbox-dogs" id="dog" name="dog" value="yes"
                  checked disabled>
                <?php } ?>
                <label class="custom-control-label" for="dog">Dogs</label>
              </div>

              <div class="custom-control custom-checkbox custom-checkbox-pets">
                <?php if(empty($row['cat']) || $row['cat'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="cat" name="cat" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="cat" name="cat" value="yes" checked disabled>
                <?php } ?>
                <label class="custom-control-label" for="cat">Cats</label>
              </div>
            </div>

            <div class="form__group-pets-in">
              <div class="custom-control custom-checkbox custom-checkbox-pets">
                <?php if(empty($row['other']) || $row['other'] == 'no'){ ?>
                <input type="checkbox" class="custom-control-input" id="other" name="other" value="yes" disabled>
                <?php }else{ ?>
                <input type="checkbox" class="custom-control-input" id="other" name="other" value="yes" checked
                  disabled>
                <?php } ?>
                <label class="custom-control-label" for="other">Others</label>
              </div>

              <?php if(empty($row['type_pet']) || $row['type_pet'] == 'NULL'){ ?>

              <div class="form__group-others"></div>

              <?php }else{ ?>
              <div class="form__group-others">

                <input type="text" class="form__group-input others" id="type_pet" name="type_pet"
                  placeholder="'Others' Specified" value="<?php echo $row['type_pet'] ?>" readonly>

              </div>
              <?php } ?>


            </div>

          </div>

        </div>

        <?php } ?>

    </main>

    <?php } ?>


    <main class="card-main">

      <div class="form__group-house_in form__group">
        <div class="form__target-header">
          <h3 class="title_target"> Family Information </h3>
        </div>

        <?php if($row['allergies'] == 'NULL' && $row['medic_f'] == 'NULL' && $row['health_f'] == 'NULL'){}else{ ?>

        <h2 class="title__group-section">Any Member of your Family:</h2>

        <div class="row form__group-row margin-t">

          <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL'){}else{ ?>

          <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-allergies">

            <label for="allergies" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i>
            </label>
            <label for="allergies" class="label__name label__family">Have Allergies?</label>

            <input type="text" class="form__group-input" value="<?php echo $row['allergies'] ?>">

          </div>

          <?php } 

        if(empty($row['medic_f']) || $row['medic_f'] == 'NULL'){}else{ 
    
    ?>


          <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-medic_f">

            <label for="medic_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies"></i> </label>
            <label for="medic_f" class="label__name label__family">Take any Medication?</label>

            <input type="text" class="form__group-input" value="<?php echo $row['allergies'] ?>">
          </div>

          <?php } 

        if(empty($row['health_f']) || $row['health_f'] == 'NULL'){}else{ 
    
    ?>

          <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-health_f">

            <label for="health_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i>
            </label>
            <label for="health_f" class="label__name label__family">Have Health Problems?</label>

            <input type="text" class="form__group-input" value="<?php echo $row['medic_f'] ?>">

          </div>

          <?php } ?>

        </div>

        <?php } 

    if($row['name_h'] == 'NULL' && $row['l_name_h'] == 'NULL' && $row['db'] == 'NULL' && $row['gender'] == 'NULL' && $row['cell'] == 'NULL' && $row['occupation_m'] == 'NULL' && $row['law'] == 'NULL'){}else{
    
    ?>



        <h2 class="title__group-section">Main Contact Information</h2>

        <div class="row form__group-row margin-t">

          <?php if(empty($row['name_h']) || $row['name_h'] == 'NULL'){}else{ ?>

          <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-name_h">

            <label for="name_h" class="l_icon" title="Name Main Contact"> <i class="icon icon_mname"></i> </label>
            <label for="name_h" class="label__name label__family">Name Main Contact </label>

            <input type="text" class="form__group-input mname" id="name_h" name="name_h" placeholder="e.g. John"
              value="<?php echo $row['name_h'] ?>" readonly>

          </div>

          <?php } 
    
    if(empty($row['l_name_h']) || $row['l_name_h'] == 'NULL'){}else{

    ?>

          <div class="form__group-lmname form__group-hinf2 col-md-4" id="form__group-l_name_h">
            <label for="l_name_h" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_mname"></i>
            </label>
            <label for="l_name_h" class="label__name label__family">Last Name </label>

            <input type="text" class="form__group-input lmname" id="l_name_h" name="l_name_h" placeholder="e.g. Smith"
              value="<?php echo $row['l_name_h'] ?>" readonly>

          </div>

          <?php } 

    if(empty($row['db']) || $row['db'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-lmname form__group-hinf2 col-md-4">
            <label for="db" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_db"></i> </label>
            <label for="db" class="label__name label__family">Date of Birth </label>

            <input type="date" class="form__group-input dateb" id="db" name="db" placeholder="MM-DD-YYYY"
              value="<?php echo $row['db'] ?>" readonly>
          </div>

          <?php } 

    if(empty($row['gender']) || $row['gender'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-gender form__group-hinf2 col-md-4">
            <label for="gender" class="label__name label__family">Gender </label>
            <label for="gender" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

            <input type="text" class="form__group-input dateb" id="db" name="db" placeholder="MM-DD-YYYY"
              value="<?php echo $row['gender'] ?>" readonly>

          </div>

          <?php } 

    if(empty($row['cell']) || $row['cell'] == 'NULL'){}else{
    
    ?>
          <div class="form__group-db-check form__group-hinf2 col-md-4" id="form__group-cell">

            <label for="cell" class="label__name label__family">Phone Number</label>
            <label for="cell" class="l_icon" title="Alternative Phone"> <i class="icon icon_pnumber2"></i> </label>

            <input type="text" class="form__group-input alter_phone" id="cell" name="cell" placeholder="e.g. 55578994"
              value="<?php echo $row['cell'] ?>" readolny>

          </div>

          <?php } 

    if(empty($row['occupation_m']) || $row['occupation_m'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-occupation_m form__group-hinf2 col-md-4">
            <label for="occupation_m" class="l_icon" title="Date of Background Check"> <i class="icon icon_check"></i>
            </label>
            <label for="occupation_m" class="label__name label__family">Occupation</label>

            <input type="text" class="form__group-input db_law" id="occupation_m" name="occupation_m"
              placeholder="e.g. Lawyer" value="<?php echo $row['occupation_m'] ?>" readonly>

          </div>

          <?php } ?>

        </div>

        <?php if(empty($row['law']) || $row['law'] == 'NULL'){}else{ ?>

        <div class="row form__group-row margin-t">
          <div class="form__group-b-check col-md-7">

            <center><iframe class="embed-responsive-item mt-5 mb-0" src="../../<?php echo $row['law'] ?>"></iframe>
            </center>

          </div>

        </div>
        <?php }} 

    if($row['num_mem'] == '0' && $row['backg'] == 'NULL' && $row['backl'] == 'NULL' && $row['religion'] == 'NULL' && $row['condition_m'] == 'NULL' && $row['misdemeanor'] == 'NULL' && $row['c_background'] == 'NULL'){}else{
    
    ?>


        <h2 class="title__group-section">Family Preferences</h2>

        <div class="row form__group-row margin-t">

          <?php if(empty($row['num_mem']) || $row['num_mem'] == '0'){}else{ ?>

          <div class="form__group-nmembers form__group-hinf2 col-md-4">

            <label for="num_mem" class="label__name label__family">Number Members</label>
            <label for="num_mem" class="l_icon" title="Number Members"> <i class="icon icon_family"></i> </label>

            <input type="number" class="form__group-input nmembers" id="num_mem" name="num_mem" min="0" max="10"
              placeholder="Only Numbers" value="<?php echo $row['num_mem'] ?>" readonly>


          </div>
          <?php } 

    if(empty($row['backg']) || $row['backg'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-background form__group-hinf2 col-md-4">

            <label for="backg" class="label__name label__family">Background</label>
            <label for="backg" class="l_icon" title="Background"> <i class="icon icon_check"></i> </label>

            <input type="text" class="form__group-input background" id="backg" name="backg" placeholder="e.g. Canadian"
              value="<?php echo $row['backg'] ?>" readonly>


          </div>
          <?php } 

    if(empty($row['backl']) || $row['backl'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-background_l form__group-hinf2 col-md-4">

            <label for="backl" class="label__name label__family">Background Language</label>
            <label for="backl" class="l_icon" title="Background Language"> <i class="icon icon_check"></i> </label>

            <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English"
              value="<?php echo $row['backl'] ?>" readonly>

          </div>

          <?php } 
    
    if(empty($row['religion']) || $row['religion'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-religion form__group-hinf2 col-md-4">

            <label for="religion" class="label__name label__family">Do you belong to a religion?</label>
            <label for="religion" class="l_icon" title="Background Language"> <i class="icon icon_check"></i> </label>

            <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English"
              value="<?php echo $row['religion'] ?>" readonly>


          </div>

          <?php } 

    if(empty($row['condition_m']) || $row['condition_m'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-condition_m form__group-hinf2 col-md-4">

            <label for="condition_m" class="label__name label__family">Any Physical or Mental condition?</label>
            <label for="condition_m" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
            </label>

            <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English"
              value="<?php echo $row['condition_m'] ?>" readonly>

          </div>

          <?php } 

    if(empty($row['misdemeanor']) || $row['misdemeanor'] == 'NULL'){}else{
    
    ?>

          <div class="form__group-misdemeanor form__group-hinf2 col-md-4">

            <label for="misdemeanor" class="label__name label__family">Have they committed a misdemeanor?</label>
            <label for="misdemeanor" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
            </label>

            <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English"
              value="<?php echo $row['misdemeanor'] ?>" readonly>

          </div>

          <?php } 

    if(empty($row['c_background']) || $row['c_background'] == 'NULL'){}else{
    
    ?>


          <div class="form__group-c_background form__group-hinf2 col-xl-10">

            <label for="c_background" class="label__name">Do you give us your consent to go to the authorities and check
              your criminal background check?</label>
            <label for="c_background" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
            </label>

            <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English"
              value="<?php echo $row['c_background'] ?>" readonly>

          </div>

          <?php } ?>

        </div>

        <?php } ?>


        <div class="form__group-row2">

          <?php if($row3['f_name1'] == 'NULL' && $row3['f_lname1'] == 'NULL' && $row3['db1'] == 'NULL' && $row3['gender1'] == 'NULL' && $row3['re1'] == 'NULL' && $row3['db_lawf1'] == 'NULL' && $row3['lawf1'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 1</h2>

          <div id="family1" class="panel-collapse">

            <div class="row member1">

              <?php 
                if(empty($row3['f_name1']) || $row3['f_name1'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name1" class="label__name label__family">Name</label>
                <label for="f_name1" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name1" id="f_name1" name="f_name1"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name1'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname1']) || $row3['f_lname1'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname1" class="label__name label__family">Last Name</label>
                <label for="f_lname1" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname1" id="f_lname1" name="f_lname1"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname1'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db1']) || $row3['db1'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db1" class="label__name label__family">Date of Birth</label>
                <label for="db1" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db1" name="db1" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db1'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender1']) || $row3['gender1'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender1" class="label__name label__family">Gender</label>
                <label for="gender1" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db1" name="db1"
                  value="<?php echo $row3['gender1'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re1']) || $row3['re1'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re1" class="label__name label__family">Relation</label>
                <label for="re1" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db1" name="db1"
                  value="<?php echo $row3['re1'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f1']) || $row3['occupation_f1'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f1" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f1" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f1" name="occupation_f1"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f1'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf1']) || $row3['db_lawf1'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf1" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf1" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf1" name="db_lawf1"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf1'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf1']) || $row3['lawf1'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf1'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>




          <?php } if($row3['f_name2'] == 'NULL' && $row3['f_lname2'] == 'NULL' && $row3['db2'] == 'NULL' && $row3['gender2'] == 'NULL' && $row3['re2'] == 'NULL' && $row3['db_lawf2'] == 'NULL' && $row3['lawf2'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 2</h2>

          <div id="family2" class="panel-collapse">

            <div class="row member1">

              <?php 
                if(empty($row3['f_name2']) || $row3['f_name2'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name2" class="label__name label__family">Name</label>
                <label for="f_name2" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name2" name="f_name2"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name2'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname2']) || $row3['f_lname2'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname2" class="label__name label__family">Last Name</label>
                <label for="f_lname2" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname2" name="f_lname2"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname2'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db2']) || $row3['db2'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db2" class="label__name label__family">Date of Birth</label>
                <label for="db2" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db2" name="db2" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db2'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender2']) || $row3['gender2'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender2" class="label__name label__family">Gender</label>
                <label for="gender2" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db2" name="db2"
                  value="<?php echo $row3['gender2'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re2']) || $row3['re2'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re2" class="label__name label__family">Relation</label>
                <label for="re2" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db2" name="db2"
                  value="<?php echo $row3['re2'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f2']) || $row3['occupation_f2'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f2" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f2" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f2" name="occupation_f2"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f2'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf2']) || $row3['db_lawf2'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf2" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf2" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf2" name="db_lawf2"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf2'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf2']) || $row3['lawf2'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf2'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>

          <?php }if($row3['f_name3'] == 'NULL' && $row3['f_lname3'] == 'NULL' && $row3['db3'] == 'NULL' && $row3['gender3'] == 'NULL' && $row3['re3'] == 'NULL' && $row3['db_lawf3'] == 'NULL' && $row3['lawf3'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 3</h2>

          <div id="family3" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name3']) || $row3['f_name3'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name3" class="label__name label__family">Name</label>
                <label for="f_name3" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name3" name="f_name3"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name3'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname3']) || $row3['f_lname3'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname3" class="label__name label__family">Last Name</label>
                <label for="f_lname3" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname3" name="f_lname3"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname3'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db3']) || $row3['db3'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db3" class="label__name label__family">Date of Birth</label>
                <label for="db3" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db3" name="db3" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db3'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender3']) || $row3['gender3'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender3" class="label__name label__family">Gender</label>
                <label for="gender3" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender3'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re3']) || $row3['re3'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re3" class="label__name label__family">Relation</label>
                <label for="re3" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re3'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f3']) || $row3['occupation_f3'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f3" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f3" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f3" name="occupation_f3"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f3'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf3']) || $row3['db_lawf3'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf3" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf3" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf3" name="db_lawf3"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf3'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf3']) || $row3['lawf3'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf3'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>

          <?php } if($row3['f_name4'] == 'NULL' && $row3['f_lname4'] == 'NULL' && $row3['db4'] == 'NULL' && $row3['gender4'] == 'NULL' && $row3['re4'] == 'NULL' && $row3['db_lawf4'] == 'NULL' && $row3['lawf4'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 4</h2>

          <div id="family4" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name4']) || $row3['f_name4'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name4" class="label__name label__family">Name</label>
                <label for="f_name4" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name4" name="f_name4"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name4'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname4']) || $row3['f_lname4'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname4" class="label__name label__family">Last Name</label>
                <label for="f_lname4" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname4" name="f_lname4"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname4'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db4']) || $row3['db4'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db4" class="label__name label__family">Date of Birth</label>
                <label for="db4" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db4" name="db4" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db4'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender4']) || $row3['gender4'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender4" class="label__name label__family">Gender</label>
                <label for="gender4" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender4'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re4']) || $row3['re4'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re4" class="label__name label__family">Relation</label>
                <label for="re4" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re4'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f4']) || $row3['occupation_f4'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f4" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f4" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f4" name="occupation_f4"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f4'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf4']) || $row3['db_lawf4'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf4" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf4" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf4" name="db_lawf4"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf4'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf4']) || $row3['lawf4'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf4'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>


          <?php } if($row3['f_name5'] == 'NULL' && $row3['f_lname5'] == 'NULL' && $row3['db5'] == 'NULL' && $row3['gender5'] == 'NULL' && $row3['re5'] == 'NULL' && $row3['db_lawf5'] == 'NULL' && $row3['lawf5'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 5</h2>

          <div id="family5" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name5']) || $row3['f_name5'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name5" class="label__name label__family">Name</label>
                <label for="f_name5" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name5" name="f_name5"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name5'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname5']) || $row3['f_lname5'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname5" class="label__name label__family">Last Name</label>
                <label for="f_lname5" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname5" name="f_lname5"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname5'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db5']) || $row3['db5'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db5" class="label__name label__family">Date of Birth</label>
                <label for="db5" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db5" name="db5" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db5'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender5']) || $row3['gender5'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender5" class="label__name label__family">Gender</label>
                <label for="gender5" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender5'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re5']) || $row3['re5'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re5" class="label__name label__family">Relation</label>
                <label for="re5" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re5'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f5']) || $row3['occupation_f5'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f5" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f5" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f5" name="occupation_f5"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f5'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf5']) || $row3['db_lawf5'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf5" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf5" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf5" name="db_lawf5"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf5'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf5']) || $row3['lawf5'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf5'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>

          <?php } if($row3['f_name6'] == 'NULL' && $row3['f_lname6'] == 'NULL' && $row3['db6'] == 'NULL' && $row3['gender6'] == 'NULL' && $row3['re6'] == 'NULL' && $row3['db_lawf6'] == 'NULL' && $row3['lawf6'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 6</h2>

          <div id="family6" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name6']) || $row3['f_name6'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name6" class="label__name label__family">Name</label>
                <label for="f_name6" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name6" name="f_name6"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name6'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname6']) || $row3['f_lname6'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname6" class="label__name label__family">Last Name</label>
                <label for="f_lname6" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname6" name="f_lname6"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname6'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db6']) || $row3['db6'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db6" class="label__name label__family">Date of Birth</label>
                <label for="db6" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db6" name="db6" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db6'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender6']) || $row3['gender6'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender6" class="label__name label__family">Gender</label>
                <label for="gender6" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender6'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re6']) || $row3['re6'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re6" class="label__name label__family">Relation</label>
                <label for="re6" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re6'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f6']) || $row3['occupation_f6'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f6" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f6" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f6" name="occupation_f6"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f6'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf6']) || $row3['db_lawf6'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf6" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf6" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf6" name="db_lawf6"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf6'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf6']) || $row3['lawf6'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf6'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>

          <?php } if($row3['f_name7'] == 'NULL' && $row3['f_lname7'] == 'NULL' && $row3['db7'] == 'NULL' && $row3['gender7'] == 'NULL' && $row3['re7'] == 'NULL' && $row3['db_lawf7'] == 'NULL' && $row3['lawf7'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 7</h2>

          <div id="family7" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name7']) || $row3['f_name7'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name7" class="label__name label__family">Name</label>
                <label for="f_name7" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name7" name="f_name7"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname7']) || $row3['f_lname7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname7" class="label__name label__family">Last Name</label>
                <label for="f_lname7" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname7" name="f_lname7"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db7']) || $row3['db7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db7" class="label__name label__family">Date of Birth</label>
                <label for="db7" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db7" name="db7" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender7']) || $row3['gender7'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender7" class="label__name label__family">Gender</label>
                <label for="gender7" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re7']) || $row3['re7'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re7" class="label__name label__family">Relation</label>
                <label for="re7" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f7']) || $row3['occupation_f7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f7" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f7" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f7" name="occupation_f7"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f7'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf7']) || $row3['db_lawf7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf7" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf7" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf7" name="db_lawf7"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf7']) || $row3['lawf7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf7'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>

          </div>

          <!-- Family Member 8 -->

          <?php } if($row3['f_name8'] == 'NULL' && $row3['f_lname8'] == 'NULL' && $row3['db8'] == 'NULL' && $row3['gender8'] == 'NULL' && $row3['re8'] == 'NULL' && $row3['db_lawf8'] == 'NULL' && $row3['lawf8'] == 'NULL'){}else{ ?>

          <h2 class="title__group-section member_f">Member 8</h2>

          <div id="family8" class="panel-collapse">

            <div class="row member1">
              <?php 
                if(empty($row3['f_name7']) || $row3['f_name7'] == 'NULL'){}else{
                ?>

              <div class="form__group-fname form__group-hinf3 col-lg-4">

                <label for="f_name7" class="label__name label__family">Name</label>
                <label for="f_name7" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input name2" id="f_name7" name="f_name7"
                  placeholder="e.g. Melissa" value="<?php echo $row3['f_name7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['f_lname7']) || $row3['f_lname7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-flname form__group-hinf3 col-lg-4">

                <label for="f_lname7" class="label__name label__family">Last Name</label>
                <label for="f_lname7" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                <input type="text" class="form__group-input lname2" id="f_lname7" name="f_lname7"
                  placeholder="e.g. Smith" value="<?php echo $row3['f_lname7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['db7']) || $row3['db7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db1 form__group-hinf3 col-lg-4">

                <label for="db7" class="label__name label__family">Date of Birth</label>
                <label for="db7" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                <input type="date" class="form__group-input dbirth1" id="db7" name="db7" placeholder="MM-DD-YYYY"
                  value="<?php echo $row3['db7'] ?>" readonly>


              </div>
              <?php } 

                if(empty($row3['gender7']) || $row3['gender7'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                <label for="gender7" class="label__name label__family">Gender</label>
                <label for="gender7" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['gender7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['re7']) || $row3['re7'] == 'NULL'){}else{
                
                ?>


              <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                <label for="re7" class="label__name label__family">Relation</label>
                <label for="re7" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>

                <input type="text" class="form__group-input dbirth1" id="db3" name="db3"
                  value="<?php echo $row3['re7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['occupation_f7']) || $row3['occupation_f7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                <label for="occupation_f7" class="l_icon" title="Date of Background Check"> <i
                    class="icon icon_check"></i> </label>
                <label for="occupation_f7" class="label__name label__family">Occupation</label>

                <input type="text" class="form__group-input db_law" id="occupation_f7" name="occupation_f7"
                  placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f7'] ?>" readonly>

              </div>
              <?php } 

                if(empty($row3['db_lawf7']) || $row3['db_lawf7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                <label for="db_lawf7" class="label__name label__family">Date Background Check</label>
                <label for="db_lawf7" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                </label>

                <input type="date" class="form__group-input relation1" id="db_lawf7" name="db_lawf7"
                  placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf7'] ?>" readonly>

              </div>

              <?php } 

                if(empty($row3['lawf7']) || $row3['lawf7'] == 'NULL'){}else{
                
                ?>

              <div class="form__group-b-check col-lg-7">
                <center><iframe class="embed-responsive-item mt-5 mb-0"
                    src="../../<?php echo $row3['lawf7'] ?>"></iframe></center>
              </div>

              <?php } ?>
            </div>
          </div>

          <?php } ?>

        </div>


      </div>
    </main>



  </form>

  <footer id="ts-footer">

    <?php include 'footer.php' ?>
  </footer>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/rooms.js"></script>
  <script src="assets/js/galery-homestay.js"></script>
  <script src="assets/js/rooms_register.js"></script>

</body>

</html>