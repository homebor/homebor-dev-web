 <?php
session_start();
include_once('../../xeon.php');
include '../../cript.php';
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2 = $query2->fetch_assoc();

$query3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();

$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
$row6=$query6->fetch_assoc();

$query9 = $link->query("SELECT * FROM academy");
$row9=$query9->fetch_assoc();

$query10 = $link->query("SELECT * FROM academy INNER JOIN pe_home ON pe_home.id_home = '$id' AND academy.id_ac = pe_home.a_pre");
$row10=$query10->fetch_assoc();

$query11 = $link->query("SELECT * FROM users WHERE mail = '$row[mail_h]' ");
$row11=$query11->fetch_assoc();

if ($row['id_m'] != $row6['id_m'] ) {
    header("location: index.php");
       }

 
}
if ($row5['usert'] != 'Manager') {
    header("location: ../logout.php");   
}
?>
 <!doctype html>
 <html lang="en">

 <head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="author" content="ThemeStarz">
   <meta http-equiv="X-UA-Compatible" content="ie=edge" />

   <!--CSS -->
   <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
   <link rel="stylesheet" href="../assets/css/leaflet.css">
   <link rel="stylesheet" href="../assets/css/style.css">
   <link rel="stylesheet" href="../homestay/assets/css/carousel.css">
   <link rel="stylesheet" href="../homestay/assets/css/galery.css">
   <link rel="stylesheet" href="assets/css/homedit1.css">

   <link rel="stylesheet" href="../assets/datepicker/css/rome.css">
   <link rel="stylesheet" href="../assets/datepicker/css/style.css">


   <!--Mapbox Links -->
   <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
   <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
   <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
   <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />



   <!-- Favicons -->
   <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
   <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
   <link rel="manifest" href="/site.webmanifest">
   <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
   <meta name="msapplication-TileColor" content="#da532c">
   <meta name="theme-color" content="#ffffff">

   <!-- Daterangepicker 
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />-->

   <title>Homebor - Edit Property</title>

   <script src="../assets/js/jquery-3.3.1.min.js"></script>



 </head>

 <body style="background-color: #F3F8FC;">

   <!-- WRAPPER - FONDO
    =================================================================================================================-->

   <!--*********************************************************************************************************-->
   <!--HEADER **************************************************************************************************-->
   <!--*********************************************************************************************************-->
   <header id="ts-header" class="fixed-top" style="background-color: white;">

     <?php include 'header.php' ?>

   </header>

   <div class="container__title">
     <h1 class="title__register">Edit Propertie </h1>

   </div>

   <form id="form" class="ts-form" action="edit-admin-propertie.php" method="post" autocomplete="off"
     enctype="multipart/form-data">
     <main class="card-main">
       <div class="form__group-house_in form__group">

         <div class="form__target-header">
           <h3 class="title_target"> Basic Information </h3>
         </div>

         <h2 class="title__group-section">House Information</h2>

         <div class="row justify-content-center form__group__row_box">

           <div class="form__group form__group-h_name col-lg-4" id="form__group-h_name">

             <div class="form__group-hinf">
               <label for="h_name" class="label__name house_info">House Name </label>
               <label for="h_name" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>

               <?php if(empty($row['h_name']) || $row['h_name'] == 'NULL'){ ?>

               <input type="text" name="h_name" id="h_name group__h_name" class="form__group-input hname"
                 placeholder="e.g. John Smith Residence">

               <?php } else{ ?>

               <input type="text" name="h_name" id="h_name group__h_name" class="form__group-input hname"
                 placeholder="e.g. John Smith Residence" value="<?php echo $row['h_name'] ?>">

               <?php } ?>

             </div>

             <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>

           </div>

           <div class="form__group form__group-num col-lg-4" id="form__group-num">

             <div class="form__group-hinf">
               <label for="p_num" class="label__name house_info">Phone Number </label>
               <label for="p_num" class="l_icon" title="Phone Number"> <i class="icon icon_pnumber"></i> </label>

               <?php if(empty($row['num']) || $row['num'] == 'NULL'){ ?>

               <input type="text" name="num" id="p_num group__p_num" class="form__group-input pnumber"
                 placeholder="e.g. 55575846">

               <?php }else{ ?>

               <input type="text" name="num" id="p_num group__p_num" class="form__group-input pnumber"
                 placeholder="e.g. 55575846" value="<?php echo $row['num'] ?>">

               <?php } ?>

             </div>

             <p class="message__input_error"> Only numbers. Min 6 characters </p>

           </div>

           <div class="form__group form__group-room col-lg-4" id="form__group-room">

             <div class="form__group-hinf">
               <label for="room" class="label__name hi_room">Rooms in your House </label>
               <label for="room" class="l_icon" title="Rooms in your House"> <i class="icon icon_rooms"></i> </label>

               <?php if(empty($row['room']) || $row['room'] == 'NULL'){ ?>

               <input type="text" name="room" id="room" class="form__group-input rooms" placeholder="e.g. 5"
                 onKeyDown="viewBed()" onKeyUp="viewBed()" maxlength="1">

               <?php }else{ ?>

               <input type="text" name="room" id="room" class="form__group-input rooms" placeholder="e.g. 5"
                 onKeyDown="viewBed()" onKeyUp="viewBed()" maxlength="1" value="<?php echo $row['room'] ?>">

               <?php } ?>
             </div>

             <p class="message__input_error"> Only numbers. Max 8 rooms </p>

           </div>

           <div class="form__group form__group-h_type col-lg-4" id="form__group-h_type">

             <div class="form__group-hinf">
               <label for="h_type" class="label__name house_info">Type of Residence</label>
               <label for="h_type" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>

               <select name="h_type" id="h_type group__h_type" class="custom-select form__group-input hname">

                 <?php if(empty($row['h_type']) || $row['h_type'] == 'NULL'){ ?>

                 <option selected hidden="option" disabled>-- Select Option --</option>
                 <option value="House"> House </option>
                 <option value="Apartment"> Apartment </option>
                 <option value="Condominium"> Condominium </option>

                 <?php }elseif(empty($row['h_type']) || $row['h_type'] == 'House'){ ?>

                 <option value="House" selected> House </option>
                 <option value="Apartment"> Apartment </option>
                 <option value="Condominium"> Condominium </option>

                 <?php }elseif(empty($row['h_type']) || $row['h_type'] == 'Apartment'){ ?>

                 <option value="Apartment" selected> Apartment </option>
                 <option value="House"> House </option>
                 <option value="Condominium"> Condominium </option>

                 <?php }elseif(empty($row['h_type']) || $row['h_type'] == 'Condominium'){ ?>

                 <option value="Condominium" selected> Condominium </option>
                 <option value="Apartment"> Apartment </option>
                 <option value="House"> House </option>

                 <?php } ?>


               </select>

             </div>
             <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>

           </div>

           <div class="form__group form__group-mail col-lg-4" id="form__group-mail">

             <div class="form__group-hinf">
               <label for="mail" class="label__name hi_room">Homestay Mail </label>
               <label for="mail" class="l_icon" title="Mail"> <i class="icon icon_mail"></i> </label>

               <?php if(empty($row11['mail']) || $row11['mail'] == 'NULL'){ ?>

               <input type="text" name="mail" id="pass group__mail" class="form__group-input mail"
                 placeholder="emaple@example.com">

               <?php }else{ ?>

               <input type="text" name="mail" id="pass group__mail" class="form__group-input mail"
                 placeholder="emaple@example.com" value="<?php echo $row11['mail'] ?>" readonly>

               <?php } ?>
             </div>

             <p class="message__input_error"> Enter a Valid Email </p>

           </div>

           <div class="form__group form__group-pass col-lg-4" id="form__group-pass">

             <div class="form__group-hinf">
               <label for="pass" class="label__name hi_room">Password </label>
               <label for="pass" class="l_icon" title="Phone Number"> <i class="icon icon_pass"></i> </label>

               <?php if(empty($row11['psw']) || $row11['psw'] == 'NULL'){ ?>

               <input type="password" name="pass" id="pass group__pass" class="form__group-input pass"
                 placeholder="Enter Password">

               <?php } else{ 

                    $psw = SED::decryption($row11['psw']);

                ?>

               <input type="password" name="pass" id="pass group__pass" class="form__group-input pass"
                 value="<?php echo $psw ?>" readonly>

               <?php } ?>
             </div>

             <p class="message__input_error"> Min 4 characters </p>

           </div>

         </div>




         <h2 class="title__group-section">Location</h2>

         <div class="row form__location px-4 end_div">

           <div class="form__group-adress col-lg-6">

             <div class="form__group-location col-sm-12 form__group-m_city mb-4">
               <label for="m_city" class="label__name location">Main City</label>
               <label for="m_city" class="l_icon" title="Avenue, street, boulevard, etc."> <i
                   class="icon icon_location"></i> </label>
               <select name="m_city" id="m_city" class="custom-select form__group-input address">

                 <?php if(empty($row['m_city']) || $row['m_city'] == 'NULL'){ ?>

                 <option hidden="option" selected disabled>Select Option</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Toronto'){ ?>

                 <option value="Toronto" selected>Toronto</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Montreal'){ ?>

                 <option value="Montreal" selected>Montreal</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Ottawa'){ ?>

                 <option value="Ottawa" selected>Ottawa</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Quebec'){ ?>

                 <option value="Quebec" selected>Quebec</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Calgary'){ ?>

                 <option value="Calgary" selected>Calgary</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'Vancouver'){ ?>

                 <option value="Vancouver" selected>Vancouver</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Toronto">Toronto</option>
                 <option value="Victoria">Victoria</option>

                 <?php }else if(empty($row['m_city']) || $row['m_city'] == 'NULL'){ ?>

                 <option value="Victoria" selected>Victoria</option>
                 <option value="Vancouver">Vancouver</option>
                 <option value="Calgary">Calgary</option>
                 <option value="Quebec">Quebec</option>
                 <option value="Ottawa">Ottawa</option>
                 <option value="Montreal">Montreal</option>
                 <option value="Toronto">Toronto</option>

                 <?php } ?>

               </select>
             </div>

             <div class="form__group-location col-sm-12 form__group-dir" id="form__group-dir">
               <label for="dir" class="label__name location">Address </label>
               <label for="dir" class="l_icon" title="Avenue, street, boulevard, etc."> <i
                   class="icon icon_location"></i> </label>

               <?php if(empty($row['dir']) || $row['dir'] == 'NULL'){ ?>

               <input type="text" name="dir" id="dir" class="form__group-input address" placeholder="Av, Street, etc.">

               <?php } else{ ?>

               <input type="text" name="dir" id="dir" class="form__group-input address"
                 value="<?php echo $row['dir'];?>">

               <?php } ?>

               <p class="message__input_error location_error"> Write a valid Address </p>
             </div>

             <div class="form__group-location col-sm-12 form__group-city" id="form__group-city">
               <label for="city" class="label__name location">City </label>
               <label for="city" class="l_icon" title="City"> <i class="icon icon_location"></i> </label>

               <?php if(empty($row['city']) || $row['city'] == 'NULL'){ ?>

               <input type="text" name="city" id="city" class="form__group-input city" placeholder="e.g. Toronto">

               <?php } else{ ?>

               <input type="text" name="city" id="city" class="form__group-input city"
                 value="<?php echo $row['city'];?>">

               <?php } ?>

               <p class="message__input_error location_error"> Write a valid City </p>
             </div>


             <div class="form__group-location col-sm-12 form__group-state" id="form__group-state">
               <label for="state" class="label__name location">State / Province </label>
               <label for="state" class="l_icon" title="State / Province"> <i class="icon icon_location"></i> </label>

               <?php if(empty($row['state']) || $row['state'] == 'NULL'){ ?>

               <input type="text" name="state" id="state" class="form__group-input state" placeholder="e.g. Ontario">

               <?php } else{ ?>

               <input type="text" name="state" id="state" class="form__group-input state"
                 value="<?php echo $row['state'];?>">

               <?php } ?>

               <p class="message__input_error location_error"> Write a valid State </p>
             </div>

             <div class="form__group-location col-sm-12 form__group-pcode" id="form__group-p_code">
               <label for="p_code" class="label__name location">Postal Code </label>
               <label for="p_code" class="l_icon" title="Postal Code"> <i class="icon icon_location"></i> </label>

               <?php if(empty($row['p_code']) || $row['p_code'] == 'NULL'){ ?>

               <input type="text" name="p_code" id="p_code" class="form__group-input pcode"
                 placeholder="No Special Characters">

               <?php } else{ ?>

               <input type="text" name="p_code" id="p_code" class="form__group-input pcode"
                 value="<?php echo $row['p_code'];?>">

               <?php } ?>

               <p class="message__input_error location_error"> Write a valid Postal Code </p>
             </div>

           </div>

           <div class="form__group-map col-sm-6">
             <div id='map' style="transform: translateY(12.5%);"></div>
           </div>

           <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
           <script>
           mapboxgl.accessToken =
             'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
           console.log(mapboxgl.accessToken);
           var client = new MapboxClient(mapboxgl.accessToken);
           console.log(client);


           var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
           var test = client.geocodeForward(address, function(err, data, res) {
             // data is the geocoding result as parsed JSON
             // res is the http response, including: status, headers and entity properties

             console.log(res);
             console.log(res.url);
             console.log(data);

             var coordinates = data.features[0].center;

             var map = new mapboxgl.Map({
               container: 'map',
               style: 'mapbox://styles/mapbox/streets-v10',
               center: coordinates,
               zoom: 14
             });
             new mapboxgl.Marker()
               .setLngLat(coordinates)
               .addTo(map);


           });
           </script>

         </div><br>

       </div>

     </main>



     <main class="card-main">
       <div class="form__group-house_in form__group">

         <div class="form__target-header">
           <h3 class="title_target"> House Details </h3>
         </div>

         <h2 class="title__group-section">Photo Gallery</h2>


         <div class="row"
           style="margin: 0; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">


           <div class="tarjet">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
               Frontage Photo </h4>


             <div class="div-img-r" align="center">


               <div class="carousel-item active">

                 <?php if ($row['phome'] == 'NULL') { ?>

                 <label for="file" class="photo-add" id="label-i">
                   <p class="form__group-l_title"> + </p>
                 </label>

                 <img id="preview" class="add-photo d-none">

                 <input type="file" name="main" id="file" accept="image/*" onchange="previewImage();"
                   style="display: none">

                 <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                   title="Change Frontage Photo"></label>

                 <?php } else{ ?>

                 <img id="preview" class="add-photo" src="<?php echo '../'.$row['phome'].''?>">

                 <input type="file" name="main" id="file" accept="image/*" onchange="previewImage();"
                   style="display: none">

                 <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i"
                   title="Change Frontage Photo"></label>

                 <?php } ?>

               </div>

             </div>

           </div>




           <!-- Living Room Photo -->
           <div class="tarjet">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
               Living Room Photo </h4>

             <div class="div-img-r" align="center">

               <div class="carousel-item active">

                 <?php if ($row4['pliving'] == 'NULL') { ?>

                 <label for="file-ii" class="photo-add" id="label-ii">
                   <p class="form__group-l_title"> + </p>
                 </label>

                 <img id="preview-ii" class="add-photo d-none">

                 <input type="file" name="lroom" id="file-ii" accept="image/*" onchange="previewImage_ii();"
                   style="display: none">

                 <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-ii-i" style="display: none"
                   title="Change Living Room"> </label>

                 <?php }else { ?>

                 <img id="preview-ii" class="add-photo" src="<?php echo '../'.$row4['pliving'].''?>">

                 <input type="file" name="lroom" id="file-ii" accept="image/*" onchange="previewImage_ii();"
                   style="display: none">

                 <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-ii-i" title="Change Living Room">
                 </label>

                 <?php } ?>

               </div>

             </div>

           </div>



           <div class="tarjet">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; ">
               Family Picture </h4>

             <div class="div-img-r" align="center">


               <div class="carousel-item active">

                 <?php if ($row4['fp'] == 'NULL') { ?>

                 <input type="file" name="fp" id="family" accept="image/*" onchange="previewImagefp();"
                   style="display: none">

                 <label for="family" class="photo-add" id="fp">
                   <p class="form__group-l_title"> + </p>
                 </label>

                 <img id="preview-fp" class="add-photo d-none">


                 <label for="family" class="add-photo-i fa fa-pencil-alt" id="fp-i" style="display: none;"
                   title="Change Frontage Photo"></label>

                 <?php }else { ?>

                 <input type="file" name="fp" id="family" accept="image/*" onchange="previewImagefp();"
                   style="display: none">

                 <img id="preview-fp" class="add-photo" src="<?php echo '../'.$row4['fp'].''?>">

                 <label for="family" class="add-photo-i fa fa-pencil-alt" id="fp-i"
                   title="Change Frontage Photo"></label>

                 <?php } ?>

               </div>

             </div>


           </div>


         </div>




         <!-- Area Photos -->

         <div class="row div-area"
           style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               Kitchen </h4>

             <div class="carousel-item active">

               <?php if ($row4['parea1'] == 'NULL') { ?>

               <label for="file-iii" class="photo-add" id="label-iii">
                 <p class="form__group-l_title"> + </p>
               </label>
               <img id="preview-iii" class="add-photo d-none">

               <input type="file" name="area-i" id="file-iii" accept="image/*" onchange="previewImage_iii();"
                 style="display: none">


               <label for="file-iii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i" style="display: none"
                 title="Change Kitchen Photo"></label>

               <?php }else { ?>

               <img id="preview-iii" class="add-photo" src="<?php echo '../'.$row4['parea1'].''?>">

               <input type="file" name="area-i" id="file-iii" accept="image/*" onchange="previewImage_iii();"
                 style="display: none">

               <label for="file-iii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i"
                 title="Change Kitchen Photo"></label>

               <?php } ?>


             </div>


           </div>


           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               Dining Room </h4>



             <div class="carousel-item active">

               <?php if ($row4['parea2'] == 'NULL') { ?>

               <label for="file-iv" class="photo-add" id="label-iv">
                 <p class="form__group-l_title"> + </p>
               </label>

               <img id="preview-iv" class="add-photo" style="margin-top: 5%; margin-bottom: 2%;">

               <input type="file" name="area-ii" id="file-iv" accept="image/*" onchange="previewImage_iv();"
                 style="display: none">

               <label for="file-iv" class="add-photo-i fa fa-pencil-alt" id="label-iv-i" style="display: none"
                 title="Change Dining Room Photo"> </label>

               <?php }else { ?>

               <img id="preview-iv" class="add-photo" src="<?php echo '../'.$row4['parea2'].''?>">

               <input type="file" name="area-ii" id="file-iv" accept="image/*" onchange="previewImage_iv();"
                 style="display: none">

               <label for="file-iv" class="add-photo-i fa fa-pencil-alt" id="label-iv-i"
                 title="Change Dining Room Photo"> </label>

               <?php } ?>

             </div>


           </div>


           <?php if ($row4['parea3'] == 'NULL') { ?>

           <div class="tarjet3 area-3 areas">

             <center><label for="collapse-floor-1"><button class="add-room iii" id="iii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-1" aria-expanded="false"
                   aria-controls="collapse-floor-1" onclick="mostrarBoton_1()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Another Area of the House">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-1"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> House Area 3 <button class="fa fa-window-close iii" id="iii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-1" aria-expanded="false"
                   aria-controls="collapse-floor-1" onclick="hidBoton_1()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;">
                 </button></h3>

               <div class="div-img-r" align="center">


                 <div class="carousel-item active">

                   <label for="file-v" class="photo-add" id="label-v">
                     <p class="form__group-l_title"> + </p>
                   </label>
                   <img id="preview-v" class="add-photo d-none">

                   <input type="file" name="area-iii" id="file-v" accept="image/*" onchange="previewImage_v();"
                     style="display: none">


                   <label for="file-v" class="add-photo-i fa fa-pencil-alt" id="label-v-i" style="display: none"
                     title="Change House Area Photo"> </label>


                 </div>



               </div>

             </div>

           </div>


           <div class="wrap-div area-iii" id="area-iii" style="display: none">

             <center><label for="collapse-floor-2"><button class="add-room iii" id="iv" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false"
                   aria-controls="collapse-floor-2" onclick="mostrarBoton_2()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px; margin-top: 2%;"
                   title="Add Another Area of the House">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-2"
               style="width: 100%; background: white; padding-top: 2%; margin-left: 3%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> House Area 4 <button class="fa fa-window-close iii" id="iv-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false"
                   aria-controls="collapse-floor-2" onclick="hidBoton_2()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">


                 <div class="carousel-item active">

                   <label for="file-vi" class="photo-add" id="label-vi">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-vi" class="add-photo d-none">

                   <input type="file" name="area-iv" id="file-vi" accept="image/*" onchange="previewImage_vi();"
                     style="display: none">

                   <label for="file-vi" class="add-photo-i fa fa-pencil-alt" id="label-vi-i" style="display: none"
                     title="Change House Area Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <?php }else { ?>

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               House Area 3 </h4>

             <div class="carousel-item active">

               <img id="preview-v" class="add-photo" src="<?php echo '../'.$row4['parea3'].''?>">

               <input type="file" name="area-iii" id="file-v" accept="image/*" onchange="previewImage_v();"
                 style="display: none">

               <label for="file-v" class="add-photo-i fa fa-pencil-alt" id="label-v-i" title="Change House Area Photo">
               </label>

             </div>

           </div>

           <?php if ($row4['parea4'] == 'NULL') { ?>

           <div class="wrap-div area-iii" id="area-iii">

             <center><label for="collapse-floor-2"><button class="add-room iii" id="iv" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false"
                   aria-controls="collapse-floor-2" onclick="mostrarBoton_2()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px; margin-top: 2%;"
                   title="Add Another Area of the House">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-2"
               style="width: 100%; background: white; padding-top: 2%; margin-left: 3%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> House Area 4 <button class="fa fa-window-close iii" id="iv-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false"
                   aria-controls="collapse-floor-2" onclick="hidBoton_2()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">


                 <div class="carousel-item active">
                   <label for="file-vi" class="photo-add" id="label-vi">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-vi" class="add-photo d-none">

                   <input type="file" name="area-iv" id="file-vi" accept="image/*" onchange="previewImage_vi();"
                     style="display: none">

                   <label for="file-vi" class="add-photo-i fa fa-pencil-alt" id="label-vi-i" style="display: none"
                     title="Change House Area Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <?php }else{ ?>

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               House Area 4 </h4>

             <div class="carousel-item active">

               <img id="preview-vi" class="add-photo" src="<?php echo '../'.$row4['parea4'].''?>">

               <input type="file" name="area-iv" id="file-vi" accept="image/*" onchange="previewImage_vi();"
                 style="display: none">

               <label for="file-vi" class="add-photo-i fa fa-pencil-alt" id="label-vi-i"
                 title="Change House Area Photo"> </label>

             </div>

           </div>


           <?php } ?>

           <?php } ?>

         </div>



         <!-- Bathroom Photos -->

         <div class="row gallery-end"
           style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;">


           <div class="tarjet">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;">
               Bathroom Photo 1 </h4>

             <div class="div-img-r" align="center">

               <div class="carousel-item active">

                 <?php if ($row4['pbath1'] == 'NULL') { ?>

                 <label for="file-vii" class="photo-add" id="label-vii">
                   <p class="form__group-l_title"> + </p>
                 </label>

                 <img id="preview-vii" class="add-photo d-none">

                 <input type="file" name="bath-i" id="file-vii" accept="image/*" onchange="previewImage_vii();"
                   style="display: none">

                 <label for="file-vii" class="add-photo-i fa fa-pencil-alt" id="label-vii-i" style="display: none"
                   title="Change Bathroom Photo"> </label>

                 <?php }else { ?>

                 <img id="preview-vii" class="add-photo" src="<?php echo '../'.$row4['pbath1'].''?>">

                 <input type="file" name="bath-i" id="file-vii" accept="image/*" onchange="previewImage_vii();"
                   style="display: none">

                 <label for="file-vii" class="add-photo-i fa fa-pencil-alt" id="label-vii-i"
                   title="Change Bathroom Photo"> </label>

                 <?php } ?>

               </div>

             </div>

           </div>


           <?php if ($row4['pbath2'] == 'NULL') { ?>

           <div class="wrap-div bath-ii">

             <center><label for="collapse-floor-4"><button class="add-room iii" id="vi" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-4" aria-expanded="false"
                   aria-controls="collapse-floor-4" onclick="mostrarBoton_4()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Another Bathroom">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-4"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 2 <button class="fa fa-window-close iii" id="vi-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-4" aria-expanded="false"
                   aria-controls="collapse-floor-4" onclick="hidBoton_4()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">


                 <div class="carousel-item active">

                   <label for="file-viii" class="photo-add" id="label-viii">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-viii" class="add-photo d-none">

                   <input type="file" name="bath-ii" id="file-viii" accept="image/*" onchange="previewImage_viii();"
                     style="display: none">

                   <label for="file-viii" class="add-photo-i fa fa-pencil-alt" id="label-viii-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <div class="wrap-div bath-3" id="bath-iii" style="display: none">

             <center><label for="collapse-floor-5"><button class="add-room iii" id="vii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false"
                   aria-controls="collapse-floor-5" onclick="mostrarBoton_5()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Another Bathroom">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-5"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 3 <button class="fa fa-window-close iii" id="vii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false"
                   aria-controls="collapse-floor-5" onclick="hidBoton_5()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">

                 <div class="carousel-item active">

                   <label for="file-ix" class="photo-add" id="label-ix">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-ix" class="add-photo d-none">

                   <input type="file" name="bath-iii" id="file-ix" accept="image/*" onchange="previewImage_ix();"
                     style="display: none">


                   <label for="file-ix" class="add-photo-i fa fa-pencil-alt" id="label-ix-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>
           </div>

           <div class="wrap-div bath-iv" id="bath-iv" style="display: none">

             <center><label for="collapse-floor-6"><button class="add-room iii" id="viii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="mostrarBoton_6()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Bathroom Photo 4">

                   +

                 </button></label></center>

             <div class="tarjet2 collapse" id="collapse-floor-6"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 4 <button class="fa fa-window-close iii" id="viii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="hidBoton_6()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">

                 <div class="carousel-item active">

                   <label for="file-x" class="photo-add" id="label-x">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-x" class="add-photo d-none">

                   <input type="file" name="bath-iv" id="file-x" accept="image/*" onchange="previewImage_x();"
                     style="display: none">


                   <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <?php }else { ?>

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               Bathroom 2 </h4>

             <div class="carousel-item active">

               <img id="preview-viii" class="add-photo" src="<?php echo '../'.$row4['pbath2'].''?>">

               <input type="file" name="bath-ii" id="file-viii" accept="image/*" onchange="previewImage_viii();"
                 style="display: none">

               <label for="file-viii" class="add-photo-i fa fa-pencil-alt" id="label-viii-i"
                 title="Change Bathroom Photo"> </label>

             </div>

           </div>

           <?php if ($row4['pbath3'] == 'NULL') { ?>

           <div class="wrap-div bath-3" id="bath-iii">

             <center><label for="collapse-floor-5"><button class="add-room iii" id="vii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false"
                   aria-controls="collapse-floor-5" onclick="mostrarBoton_5()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Another Bathroom">

                   +

                 </button></label></center>

             <div class="tarjet collapse" id="collapse-floor-5"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 3 <button class="fa fa-window-close iii" id="vii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false"
                   aria-controls="collapse-floor-5" onclick="hidBoton_5()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">

                 <div class="carousel-item active">

                   <label for="file-ix" class="photo-add" id="label-ix">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-ix" class="add-photo d-none">

                   <input type="file" name="bath-iii" id="file-ix" accept="image/*" onchange="previewImage_ix();"
                     style="display: none">


                   <label for="file-ix" class="add-photo-i fa fa-pencil-alt" id="label-ix-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <div class="wrap-div bath-iv" id="bath-iv" style="display: none">

             <center><label for="collapse-floor-6"><button class="add-room iii" id="viii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="mostrarBoton_6()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Bathroom Photo 4">

                   +

                 </button></label></center>

             <div class="tarjet2 collapse" id="collapse-floor-6"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 4 <button class="fa fa-window-close iii" id="viii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="hidBoton_6()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">

                 <div class="carousel-item active">

                   <label for="file-x" class="photo-add" id="label-x">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-x" class="add-photo d-none">

                   <input type="file" name="bath-iv" id="file-x" accept="image/*" onchange="previewImage_x();"
                     style="display: none">


                   <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <?php }else { ?>

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               Bathroom 3 </h4>

             <div class="carousel-item active">

               <img id="preview-ix" class="add-photo" src="<?php echo '../'.$row4['pbath3'].''?>">

               <input type="file" name="bath-iii" id="file-ix" accept="image/*" onchange="previewImage_ix();"
                 style="display: none">

               <label for="file-ix" class="add-photo-i fa fa-pencil-alt" id="label-ix-i" title="Change Bathroom Photo">
               </label>

             </div>

           </div>


           <?php if ($row4['pbath4'] == 'NULL') { ?>

           <div class="wrap-div bath-iv" id="bath-iv">

             <center><label for="collapse-floor-6"><button class="add-room iii" id="viii" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="mostrarBoton_6()"
                   style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;"
                   title="Add Bathroom Photo 4">

                   +

                 </button></label></center>

             <div class="tarjet2 collapse" id="collapse-floor-6"
               style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

               <h3 class="title-room"> Bathroom 4 <button class="fa fa-window-close iii" id="viii-i" type="button"
                   data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false"
                   aria-controls="collapse-floor-6" onclick="hidBoton_6()"
                   style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; color: #4F177D; float: right; margin-right: -.5em;"></button>
               </h3>

               <div class="div-img-r" align="center">

                 <div class="carousel-item active">

                   <label for="file-x" class="photo-add" id="label-x">
                     <p class="form__group-l_title"> + </p>
                   </label>

                   <img id="preview-x" class="add-photo d-none">

                   <input type="file" name="bath-iv" id="file-x" accept="image/*" onchange="previewImage_x();"
                     style="display: none">


                   <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" style="display: none"
                     title="Change Bathroom Photo"> </label>

                 </div>

               </div>

             </div>

           </div>

           <?php }else { ?>

           <div class="tarjet" align="center">

             <h4 class="title-room"
               style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;">
               Bathroom 4 </h4>

             <div class="carousel-item active">

               <img id="preview-x" class="add-photo" src="<?php echo '../'.$row4['pbath4'].''?>">

               <input type="file" name="bath-iv" id="file-x" accept="image/*" onchange="previewImage_x();"
                 style="display: none">

               <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" title="Change Bathroom Photo">
               </label>

             </div>

           </div>

           <?php } ?>

           <?php } ?>

           <?php } ?>




         </div>



         <div class="m-0 p-0 bedrooms">





         </div>






     </main>



     <main class="card-main">

       <div class="form__group-house_in form__group">

         <div class="form__target-header">
           <h3 class="title_target"> Additional Information </h3>
         </div>

         <h2 class="title__group-section">Description of your House</h2>

         <div class="form__group-description_div">

           <div class="form__group-description">

             <label for="address" class="l_icon" title="Description"> <i class="icon icon_description"></i> </label>

             <?php if(empty($row['des']) || $row['des'] == 'NULL'){ ?>

             <textarea rows="3" maxlength="255" class="form__group-input description" name="des" id="des"
               placeholder="Description: Describe your house using few words, no special characters."></textarea>

             <?php }else{ ?>

             <textarea rows="3" maxlength="255" class="form__group-input description" name="des" id="des"
               placeholder="Description: Describe your house using few words, no special characters."> <?php echo $row['des'] ?> </textarea>

             <?php } ?>

           </div>


         </div>

         <h2 class="title__group-section">Preferences</h2>

         <div class="row form__group-preferences form__group-preferences1">

           <div class="form__group-academy col-xl-12 mb-3">
             <label for="a_pre" class="label__name preference">Academy Preference</label>
             <label for="a_pre" class="l_icon" title="Academy Preference"> <i class="icon icon_p_academy"></i> </label>
             <select class="custom-select form__group-input academy" id="a_pre" name="a_pre">
               <?php    
                if($row['a_pre'] == 'NULL'){

                    while ( $row9 = $query9->fetch_array() ){
            ?>
               <option hidden="option">-- Select Academy --</option>

               <option value=" <?php echo $row9['id_ac'] ?> ">
                 <?php echo $row9['name_a']; ?><?php echo ', '?><?php echo $row9['dir_a']; ?>
               </option>

               <?php } ?>

               <?php }else{ ?>
               <option value="<?php echo $row10['a_pre'] ?>"><?php echo $row10['name_a'].', '.$row10['dir_a']?></option>

               <?php

                    while ( $row9 = $query9->fetch_array() ){
                    ?>
               <option hidden="option">-- Select Academy --</option>

               <option value=" <?php echo $row9['id_ac'] ?> ">
                 <?php echo $row9['name_a']; ?><?php echo ', '?><?php echo $row9['dir_a']; ?>
               </option>

               <?php } ?>

               <?php } ?>


             </select>
           </div>

           <div class="form__group-gender col-xl-4">
             <label for="g_pre" class="label__name preference">Gender Preference</label>
             <label for="g_pre" class="l_icon" title="Gender Preference"> <i class="icon icon_gender"></i> </label>
             <select class="custom-select form__group-input gender" id="g_pre" name="g_pre">
               <?php if(empty($row['g_pre']) || $row['g_pre'] == 'NULL'){ ?>

               <option hidden="option" selected disabled hidden="option">-- Select Gender --</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
               <option value="Any">Any</option>

               <?php } else if($row['g_pre'] == 'Male'){ ?>

               <option value="Male" selected>Male</option>
               <option value="Female">Female</option>
               <option value="Any">Any</option>

               <?php } else if($row['g_pre'] == 'Female'){ ?>

               <option value="Female" selected>Female</option>
               <option value="Male">Male</option>
               <option value="Any">Any</option>

               <?php } else if($row['g_pre'] == 'Any'){ ?>

               <option value="Any" selected>Any</option>
               <option value="Female">Female</option>
               <option value="Male">Male</option>

               <?php } ?>
             </select>
           </div>

           <div class="form__group-age col-xl-4">
             <label for="ag_pre" class="label__name preference">Age Preference</label>
             <label for="ag_pre" class="l_icon" title="Age Preference"> <i class="icon icon_age"></i> </label>
             <select class="custom-select form__group-input age" id="ag_pre" name="ag_pre">
               <?php if(empty($row['ag_pre']) || $row['ag_pre'] == 'NULL'){ ?>

               <option hidden="option" selected disabled hidden="option">-- Select Age --</option>
               <option value="Teenager">Teenager</option>
               <option value="Adult">Adult</option>
               <option value="Any">Any</option>

               <?php } else if($row['ag_pre'] == 'Teenager'){ ?>

               <option value="Teenager" selected>Teenager</option>
               <option value="Adult">Adult</option>
               <option value="Any">Any</option>

               <?php } else if($row['ag_pre'] == 'Adult'){ ?>

               <option value="Adult" selected>Adult</option>
               <option value="Teenager">Teenager</option>
               <option value="Any">Any</option>

               <?php } else if($row['ag_pre'] == 'Any'){ ?>

               <option value="Any" selected>Any</option>
               <option value="Adult">Adult</option>
               <option value="Teenager">Teenager</option>

               <?php } ?>
             </select>
           </div>



           <div class="form__group-status form__group col-xl-4">
             <label for="status" class="label__name preference">House Status</label>
             <label for="status" class="l_icon" title="House Status"> <i class="icon icon_status"></i> </label>
             <select class="custom-select form__group-input status" id="status" name="status">
               <?php if(empty($row['status']) || $row['status'] == 'NULL'){ ?>

               <option hidden="option" selected disabled hidden="option">-- Select Status --</option>
               <option value="Avalible">Avalible</option>
               <option value="Occupied">Occupied</option>

               <?php }else if($row['status'] == 'Avalible'){ ?>

               <option value="Avalible" selected>Avalible</option>
               <option value="Occupied">Occupied</option>

               <?php }else if($row['status'] == 'Occupied'){ ?>

               <option value="Occupied" selected>Occupied</option>
               <option value="Avalible">Avalible</option>

               <?php } ?>

             </select>
           </div>

           <div class="form__group-smokers form__group col-xl-4">
             <label for="smoke" class="label__name preference">Smokers Politics</label>
             <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_smokers"></i> </label>
             <select class="custom-select form__group-input smoke" id="smoke" name="smoke">
               <?php if(empty($row['smoke']) || $row['smoke'] == 'NULL'){ ?>

               <option hidden="option" selected disabled hidden="option">-- Select Preference --</option>
               <option value="Outside-OK">Outside-OK</option>
               <option value="Inside-OK">Inside-OK</option>
               <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>

               <?php }else if($row['smoke'] == 'Outside-OK'){ ?>

               <option value="Outside-OK" selected>Outside-OK</option>
               <option value="Inside-OK">Inside-OK</option>
               <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>

               <?php }else if($row['smoke'] == 'Inside-OK'){ ?>

               <option value="Inside-OK" selected>Inside-OK</option>
               <option value="Outside-OK">Outside-OK</option>
               <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>

               <?php }else if($row['smoke'] == 'Strictly Non-Smoking'){ ?>

               <option value="Strictly Non-Smoking" selected>Strictly Non-Smoking</option>
               <option value="Inside-OK">Inside-OK</option>
               <option value="Outside-OK">Outside-OK</option>

               <?php } ?>
             </select>
           </div>

           <div class="form__group-smokers form__group col-xl-4">
             <label for="m_service" class="label__name preference">Meals Service</label>
             <label for="m_service" class="l_icon" title="Smokers Politics"> <i class="icon icon_meals"></i> </label>
             <select class="custom-select form__group-input smoke" id="m_service" name="m_service">
               <?php if(empty($row['m_service']) || $row['m_service'] == 'NULL'){ ?>

               <option selected disabled hidden="option">- Select -</option>
               <option value="Yes">Yes</option>
               <option value="No">No</option>

               <?php }else if($row['m_service'] == 'Yes'){ ?>

               <option value="Yes" selected>Yes</option>
               <option value="No">No</option>

               <?php }else if($row['m_service'] == 'No'){ ?>

               <option value="No" selected>No</option>
               <option value="Yes">Yes</option>

               <?php } ?>
             </select>
           </div>

           <div class="form__group-smokers form__group col-xl-4">
             <label for="y_service" class="label__name preference">Since when have you been a Homestay?</label>
             <label for="y_service" class="l_icon" title="Smokers Politics"> <i class="icon icon_years"></i> </label>

             <?php if(empty($row['y_service']) || $row['y_service'] == 'NULL'){ ?>

             <input type="date" class="form__group-input y_hom" id="y_service" name="y_service">

             <?php }else{ ?>

             <input type="date" class="form__group-input y_hom" id="y_service" name="y_service"
               value="<?php echo $row['y_service'] ?>">

             <?php } ?>

           </div>

           <div class="form__group form__group-ypets col-xl-4">
             <label for="y_service" class="label__name preference">Do you have pets?</label>
             <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>
             <select class="custom-select form__group-input ypets" id="pet" name="pet" onchange="yesPets()">
               <?php if(empty($row['pet']) || $row['pet'] == 'NULL'){ ?>
               <option hidden="option" selected disabled> -- Select Option -- </option>
               <option value="Yes">Yes</option>
               <option value="No">No</option>
               <?php } else if(empty($row['pet']) || $row['pet'] == 'Yes'){ ?>

               <option value="Yes" selected>Yes</option>
               <option value="No">No</option>

               <?php } else if(empty($row['pet']) || $row['pet'] == 'No'){ ?>

               <option value="No" selected>No</option>
               <option value="Yes">Yes</option>

               <?php } ?>
             </select>
           </div>

           <div class="form__group form__group-ypets col-xl-4 d-none" id="num__pets">
             <label for="y_service" class="label__name preference">How many pets?</label>
             <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>
             <input type="number" class="form__group-input npets" id="npets" name="" min="1" max="10"
               placeholder="Pets Number">
           </div>

           <div class="form__group form__group-ypets col-xl-4 d-none" id="kind__pets">
             <label for="y_service" class="label__name preference">Kind of Pet</label>

             <div class="type__pets">
               <div class="pet_dog">
                 <?php if(empty($row['dog']) || $row['dog'] == 'no'){ ?>

                 <input type="checkbox" id="dog" name="dog" value="yes">

                 <?php }else{ ?>

                 <input type="checkbox" id="dog" name="dog" value="yes" checked>

                 <?php } ?>
                 <label for="">Dog</label>
               </div>

               <div class="pet_cat">
                 <?php if(empty($row['cat']) || $row['cat'] == 'no'){ ?>

                 <input type="checkbox" id="cat" name="cat" value="yes">

                 <?php }else{ ?>

                 <input type="checkbox" id="cat" name="cat" value="yes" checked>

                 <?php } ?>
                 <label for="">Cat</label>
               </div>

               <div class="pet_other">
                 <?php if(empty($row['other']) || $row['other'] == 'no'){ ?>

                 <input type="checkbox" id="other" name="other" value="yes">


                 <?php }else{ ?>

                 <input type="checkbox" id="cat" name="cat" value="yes" checked>

                 <?php } ?>
                 <label for="">Other</label>
               </div>
             </div>

           </div>


           <div class="form__group form__group-ypets col-xl-4" id="specify_pets">

             <label for="y_service" class="label__name preference">Specify</label>
             <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>

             <?php if(empty($row['other']) || $row['other'] == 'no'){ ?>
             <input type="text" class="form__group-input specify_pets" id="type_pet" name="type_pet"
               placeholder="Specify">
             <?php }else{ ?>
             <input type="text" class="form__group-input specify_pets" id="type_pet" name="type_pet"
               placeholder="Specify" value="<?php echo $row['other'] ?>">

             <?php } ?>


           </div>




           <script>
           const $pets = document.querySelector("#pet");

           function yesPets() {

             const petsI = $pets.selectedIndex;
             if (petsI === -1) return; // Esto es cuando no hay elementos
             const optionPets = $pets.options[petsI];

             if (optionPets.value == 'Yes') {

               document.getElementById('num__pets').classList.remove('d-none');
               document.getElementById('kind__pets').classList.remove('d-none');

             } else {
               document.getElementById('num__pets').classList.add('d-none');
               document.getElementById('kind__pets').classList.add('d-none');

             }

           };

           yesPets();


           // obtener campos ocultar div
           var checkbox = $("#other");
           var hidden = $("#specify_pets");
           //var populate = $("#populate");

           hidden.hide();
           checkbox.change(function() {
             if (checkbox.is(':checked')) {
               //hidden.show();
               $("#specify_pets").fadeIn("200")
             } else {
               //hidden.hide();
               $("#specify_pets").fadeOut("100")
               $("#type_pet").val(""); // limpia los valores de lols input al ser ocultado
               $('#specify_pets').prop('checked', false); // limpia los valores de checkbox al ser ocultado

             }
           });
           </script>

         </div>




         <div class="row justify-content-center form__group-preferences">

           <div class="form__group-diet col-lg-5 p-0">
             <h2 class="title__group-preference"><i class="icon icon-diet"></i> Special Diet</h2>

             <div class="form__group-diet-in">

               <div class="custom-control custom-checkbox">

                 <?php if(empty($row['vegetarians']) || $row['vegetarians'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" value="yes"
                   checked>
                 <?php } ?>
                 <label class="custom-control-label" for="vegetarians">Vegetarians</label>
               </div>

               <div class="custom-control custom-checkbox">
                 <?php if(empty($row['halal']) || $row['halal'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="halal">Halal (Muslims)</label>
               </div>

             </div>

             <div class="form__group-diet-in">

               <div class="custom-control custom-checkbox">
                 <?php if(empty($row['kosher']) || $row['kosher'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
               </div>

               <div class="custom-control custom-checkbox">
                 <?php if(empty($row['lactose']) || $row['lactose'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
               </div>

             </div>

             <div class="form__group-diet-in">

               <div class="custom-control custom-checkbox">
                 <?php if(empty($row['gluten']) || $row['gluten'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
               </div>

               <div class="custom-control custom-checkbox">
                 <?php if(empty($row['pork']) || $row['pork'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="no">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="no" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="pork">No Pork</label>
               </div>

             </div>

             <div class="form__group-diet-in">

               <div class="custom-control custom-checkbox-none custom-checkbox">
                 <?php if(empty($row['none']) || $row['none'] == 'no'){ ?>
                 <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">
                 <?php }else{ ?>
                 <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" checked>
                 <?php } ?>
                 <label class="custom-control-label" for="none">None</label>
               </div>

             </div>

           </div>


         </div>

     </main>


     <main class="card-main">

       <div class="form__group-house_in form__group">
         <div class="form__target-header">
           <h3 class="title_target"> Family Information </h3>
         </div>

         <h2 class="title__group-section">Any Member of your Family:</h2>

         <div class="row form__group-row margin-t">

           <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-allergies">

             <label for="allergies" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i>
             </label>
             <label for="allergies" class="label__name label__family">Have Allergies?</label>
             <select class="custom-select form__group-input" id="allergies" name="allergies" onchange="otherAllergy()">

               <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL'){ ?>

               <option hidden="option" selected disabled>-- Select Option --</option>
               <option value="Yes">Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['allergies']) || $row['allergies'] == 'Yes'){ ?>

               <option value="Yes" selected>Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['allergies']) || $row['allergies'] == 'No'){ ?>

               <option value="No" selected>No</option>
               <option value="Yes">Yes</option>

               <?php } ?>

             </select>

             <p class="message__input_error"> Write a valid Name </p>

           </div>

           <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__allergies">

           </div>


           <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-medic_f">

             <label for="medic_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies"></i> </label>
             <label for="medic_f" class="label__name label__family">Take any Medication?</label>
             <select class="custom-select form__group-input" id="medic_f" name="medic_f" onchange="otherMedication()">

               <?php if(empty($row['medic_f']) || $row['medic_f'] == 'NULL'){ ?>

               <option hidden="option" selected disabled>-- Select Option --</option>
               <option value="Yes">Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['medic_f']) || $row['medic_f'] == 'Yes'){ ?>

               <option value="Yes" selected>Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['medic_f']) || $row['medic_f'] == 'No'){ ?>

               <option value="No" selected>No</option>
               <option value="Yes">Yes</option>

               <?php } ?>
             </select>

             <p class="message__input_error"> Write a valid Name </p>
           </div>

           <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__medic_f">

           </div>

           <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-health_f">

             <label for="health_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i>
             </label>
             <label for="health_f" class="label__name label__family">Have Health Problems?</label>
             <select class="custom-select form__group-input" id="health_f" name="health_f" onchange="otherHealth()">

               <?php if(empty($row['medic_f']) || $row['medic_f'] == 'NULL'){ ?>

               <option hidden="option" selected disabled>-- Select Option --</option>
               <option value="Yes">Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['medic_f']) || $row['medic_f'] == 'Yes'){ ?>

               <option value="Yes" selected>Yes</option>
               <option value="No">No</option>

               <?php }else if(empty($row['medic_f']) || $row['medic_f'] == 'No'){ ?>

               <option value="No" selected>No</option>
               <option value="Yes">Yes</option>

               <?php } ?>
             </select>

             <p class="message__input_error"> Write a valid Name </p>
           </div>

           <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__health_f">

           </div>

         </div>




         <script>
         const $allergy = document.querySelector("#allergies");

         function otherAllergy() {

           const allergyI = $allergy.selectedIndex;

           if (allergyI === -1) return; // Esto es cuando no hay elementos

           const optionAllergy = $allergy.options[allergyI];

           if (optionAllergy.value == 'Yes') {

             document.getElementById('group__allergies').classList.remove('d-none');

             $('#group__allergies').html(`
    
                                <label class="label__other__city">Specify the Allergy</label>
                                <input class="input__other_city" type="text" name="allergies"> 
    
                            `);

           } else {
             document.getElementById('group__allergies').classList.add('d-none');
           }

         }

         const $medication = document.querySelector("#medic_f");

         function otherMedication() {

           const medicationI = $medication.selectedIndex;

           if (medicationI === -1) return; // Esto es cuando no hay elementos

           const optionMedication = $medication.options[medicationI];

           if (optionMedication.value == 'Yes') {

             document.getElementById('group__medic_f').classList.remove('d-none');

             $('#group__medic_f').html(`
    
                                <label class="label__other__city">Specify the Medication</label>
                                <input class="input__other_city" type="text" name="medic_f"> 
    
                            `);

           } else {
             document.getElementById('group__medic_f').classList.add('d-none');
           }

         }

         const $health = document.querySelector("#health_f");

         function otherHealth() {

           const healthI = $health.selectedIndex;

           if (healthI === -1) return; // Esto es cuando no hay elementos

           const optionHealth = $health.options[healthI];

           if (optionHealth.value == 'Yes') {

             document.getElementById('group__health_f').classList.remove('d-none');

             $('#group__health_f').html(`
    
                                <label class="label__other__city">Specify the Problems</label>
                                <input class="input__other_city" type="text" name="health_f"> 
    
                            `);

           } else {
             document.getElementById('group__health_f').classList.add('d-none');
           }

         }
         </script>

         <h2 class="title__group-section">Main Contact Information</h2>

         <div class="row form__group-row margin-t">

           <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-name_h">

             <label for="name_h" class="l_icon" title="Name Main Contact"> <i class="icon icon_mname"></i> </label>
             <label for="name_h" class="label__name label__family">Name Main Contact </label>

             <?php if(empty($row['name_h']) || $row['name_h'] == 'NULL'){ ?>
             <input type="text" class="form__group-input mname" id="name_h" name="name_h" placeholder="e.g. John">
             <?php }else{ ?>
             <input type="text" class="form__group-input mname" id="name_h" name="name_h" placeholder="e.g. John"
               value="<?php echo $row['name_h'] ?>">
             <?php } ?>

             <p class="message__input_error"> Write a valid Name </p>
           </div>

           <div class="form__group-lmname form__group-hinf2 col-md-4" id="form__group-l_name_h">
             <label for="l_name_h" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_mname"></i>
             </label>
             <label for="l_name_h" class="label__name label__family">Last Name </label>
             <?php if(empty($row['l_name_h']) || $row['l_name_h'] == 'NULL'){ ?>
             <input type="text" class="form__group-input lmname" id="l_name_h" name="l_name_h" placeholder="e.g. Smith">
             <?php }else{ ?>
             <input type="text" class="form__group-input lmname" id="l_name_h" name="l_name_h" placeholder="e.g. Smith"
               value="<?php echo $row['l_name_h'] ?>">
             <?php } ?>

             <p class="message__input_error"> Write a valid Last Name </p>
           </div>

           <div class="form__group-lmname form__group-hinf2 col-md-4">
             <label for="db" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_db"></i> </label>
             <label for="db" class="label__name label__family">Date of Birth </label>

             <?php if(empty($row['db']) || $row['db'] == 'NULL'){ ?>
             <input type="date" class="form__group-input dateb" id="db" name="db" placeholder="MM-DD-YYYY">
             <?php }else{ ?>
             <input type="date" class="form__group-input dateb" id="db" name="db" placeholder="MM-DD-YYYY"
               value="<?php echo $row['db'] ?>">
             <?php } ?>
           </div>




           <div class="form__group-gender form__group-hinf2 col-md-4">
             <label for="gender" class="label__name label__family">Gender </label>
             <label for="gender" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
             <select class="custom-select form__group-input mgender" id="gender" name="gender">
               <?php if(empty($row['gender']) || $row['gender'] == 'NULL'){ ?>

               <option hidden="option" selected disabled>-- Select Gender --</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
               <option value="Private">Private</option>

               <?php }else if($row['gender'] == 'Male'){ ?>

               <option value="Male" selected>Male</option>
               <option value="Female">Female</option>
               <option value="Private">Private</option>

               <?php }else if($row['gender'] == 'Female'){ ?>

               <option value="Female" selected>Female</option>
               <option value="Male">Male</option>
               <option value="Private">Private</option>

               <?php }else if($row['gender'] == 'Private'){ ?>

               <option value="Private" selected>Private</option>
               <option value="Female">Female</option>
               <option value="Male">Male</option>

               <?php } ?>
             </select>
           </div>

           <div class="form__group-db-check form__group-hinf2 col-md-4" id="form__group-cell">

             <label for="cell" class="label__name label__family">Phone Number</label>
             <label for="cell" class="l_icon" title="Alternative Phone"> <i class="icon icon_pnumber2"></i> </label>
             <?php if(empty($row['cell']) || $row['cell'] == 'NULL'){ ?>
             <input type="text" class="form__group-input alter_phone" id="cell" name="cell" placeholder="e.g. 55578994">
             <?php }else{ ?>
             <input type="text" class="form__group-input alter_phone" id="cell" name="cell" placeholder="e.g. 55578994"
               value="<?php echo $row['cell'] ?>">
             <?php } ?>

             <p class="message__input_error"> Only numbers. Min 6 characters </p>

           </div>

           <div class="form__group-occupation_m form__group-hinf2 col-md-4">
             <label for="occupation_m" class="l_icon" title="Date of Background Check"> <i class="icon icon_check"></i>
             </label>
             <label for="occupation_m" class="label__name label__family">Occupation</label>

             <?php if(empty($row['occupation_m']) || $row['occupation_m'] == 'NULL'){ ?>

             <input type="text" class="form__group-input db_law" id="occupation_m" name="occupation_m"
               placeholder="e.g. Lawyer">

             <?php }else{ ?>

             <input type="text" class="form__group-input db_law" id="occupation_m" name="occupation_m"
               placeholder="e.g. Lawyer" value="<?php echo $row['occupation_m'] ?>">

             <?php } ?>
           </div>

           <div class="form__group-db-check form__group-hinf2 col-md-4">
             <label for="db_law" class="l_icon" title="Date of Background Check"> <i class="icon icon_db"></i> </label>
             <label for="db_law" class="label__name label__family">Date of Background Check</label>

             <?php if(empty($row['db_law']) || $row['db_law'] == 'NULL'){ ?>
             <input type="date" class="form__group-input db_law" id="db_law" name="db_law" placeholder="MM-DD-YYYY">
             <?php }else{ ?>
             <input type="date" class="form__group-input db_law" id="db_law" name="db_law" placeholder="MM-DD-YYYY"
               value="<?php echo $row['db_law'] ?>">
             <?php } ?>
           </div>

         </div>

         <div class="row form__group-row margin-t">
           <div class="form__group-b-check col-md-7">

             <?php if(empty($row['law']) || $row['law'] == 'NULL'){}else{ ?>
             <center><iframe class="embed-responsive-item mt-5 mb-0" src="../../<?php echo $row['law'] ?>"></iframe>
             </center>
             <?php } ?>
             <div class=" form__group-hinf2 mt-3">

               <label for="db_law" class="label__name">Background Check</label>
               <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i>
               </label>

               <input type="file" class="form__group-input db_law" name="image[]" id="profile-img" maxlength="1"
                 accept="pdf" style="width: 100%;">
             </div>
           </div>

         </div>


         <h2 class="title__group-section">Family Preferences</h2>

         <div class="row form__group-row margin-t">

           <div class="form__group-nmembers form__group-hinf2 col-md-4">

             <label for="num_mem" class="label__name label__family">Number Members</label>
             <label for="num_mem" class="l_icon" title="Number Members"> <i class="icon icon_family"></i> </label>

             <?php if(empty($row['num_mem']) || $row['num_mem'] == '0'){ ?>

             <input type="number" class="form__group-input nmembers" id="num_mem" name="num_mem" min="0" max="10"
               placeholder="Only Numbers">

             <?php }else{ ?>

             <input type="number" class="form__group-input nmembers" id="num_mem" name="num_mem" min="0" max="10"
               placeholder="Only Numbers" value="<?php echo $row['num_mem'] ?>">

             <?php } ?>

           </div>

           <div class="form__group-background form__group-hinf2 col-md-4">

             <label for="backg" class="label__name label__family">Background</label>
             <label for="backg" class="l_icon" title="Background"> <i class="icon icon_check"></i> </label>

             <?php if(empty($row['backg']) || $row['backg'] == 'NULL'){ ?>

             <input type="text" class="form__group-input background" id="backg" name="backg"
               placeholder="e.g. Canadian">

             <?php }else{ ?>

             <input type="text" class="form__group-input background" id="backg" name="backg" placeholder="e.g. Canadian"
               value="<?php echo $row['backg'] ?>">

             <?php } ?>

           </div>

           <div class="form__group-background_l form__group-hinf2 col-md-4">

             <label for="backl" class="label__name label__family">Background Language</label>
             <label for="backl" class="l_icon" title="Background Language"> <i class="icon icon_check"></i> </label>

             <?php if(empty($row['backl']) || $row['backl'] == 'NULL'){ ?>

             <input type="text" class="form__group-input background_l" id="backl" name="backl"
               placeholder="e.g. English">

             <?php }else{ ?>

             <input type="text" class="form__group-input background_l" id="backl" name="backl"
               placeholder="e.g. English" value="<?php echo $row['backl'] ?>">

             <?php } ?>

           </div>

           <div class="form__group-religion form__group-hinf2 col-md-4">

             <label for="religion" class="label__name label__family">Do you belong to a religion?</label>
             <label for="religion" class="l_icon" title="Background Language"> <i class="icon icon_check"></i> </label>

             <select name="religion" id="religion" class="custom-select form__group-input background_l"
               onchange="otherReligion()">

               <?php if(empty($row['religion']) || $row['religion'] == 'NULL'){ ?>

               <option hidden="option" selected disabled> -- Select Option -- </option>
               <option value="Yes"> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['religion']) || $row['religion'] == 'Yes'){ ?>

               <option value="Yes" selected> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['religion']) || $row['religion'] == 'No'){ ?>

               <option value="No" selected> No </option>
               <option value="Yes"> Yes </option>

               <?php } ?>

             </select>

           </div>

           <div class="form__group-religion form__group-hinf2 col-md-4 d-none" id="group__religion">

           </div>

           <div class="form__group-condition_m form__group-hinf2 col-md-4">

             <label for="condition_m" class="label__name label__family">Any Physical or Mental condition?</label>
             <label for="condition_m" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
             </label>

             <select name="condition_m" id="condition_m" class="custom-select form__group-input background_l"
               onchange="otherCondition()">

               <?php if(empty($row['condition_m']) || $row['condition_m'] == 'NULL'){ ?>

               <option hidden="option" selected disabled> -- Select Option -- </option>
               <option value="Yes"> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['condition_m']) || $row['condition_m'] == 'Yes'){ ?>

               <option value="Yes" selected> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['condition_m']) || $row['condition_m'] == 'No'){ ?>

               <option value="No" selected> No </option>
               <option value="Yes"> Yes </option>

               <?php } ?>

             </select>

           </div>

           <div class="form__group-condition form__group-hinf2 col-md-4 d-none" id="group__condition_m">

           </div>

           <div class="form__group-misdemeanor form__group-hinf2 col-md-4">

             <label for="misdemeanor" class="label__name label__family">Have they committed a misdemeanor?</label>
             <label for="misdemeanor" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
             </label>

             <select name="misdemeanor" id="misdemeanor" class="custom-select form__group-input background_l"
               onchange="otherMisdemeanor()">

               <?php if(empty($row['misdemeanor']) || $row['misdemeanor'] == 'NULL'){ ?>

               <option hidden="option" selected disabled> -- Select Option -- </option>
               <option value="Yes"> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['misdemeanor']) || $row['misdemeanor'] == 'Yes'){ ?>

               <option value="Yes" selected> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['misdemeanor']) || $row['misdemeanor'] == 'No'){ ?>

               <option value="No" selected> No </option>
               <option value="Yes"> Yes </option>

               <?php } ?>

             </select>

           </div>

           <div class="form__group-misdemeanor form__group-hinf2 col-md-4 d-none" id="group__misdemeanor">

           </div>

           <script>
           const $misdemeanor = document.querySelector("#misdemeanor");

           function otherMisdemeanor() {

             const misdemeanorI = $misdemeanor.selectedIndex;

             if (misdemeanorI === -1) return; // Esto es cuando no hay elementos

             const optionMisdemeanor = $misdemeanor.options[misdemeanorI];

             if (optionMisdemeanor.value == 'Yes') {

               document.getElementById('group__misdemeanor').classList.remove('d-none');

               $('#group__misdemeanor').html(`

                                    <label class="label__other__city">Specify?</label>
                                    <input class="input__other_city" type="text" name="misdemeanor"> 

                                `);

             } else {
               document.getElementById('group__misdemeanor').classList.add('d-none');
             }

           }

           const $religion = document.querySelector("#religion");

           function otherReligion() {

             const religionI = $religion.selectedIndex;

             if (religionI === -1) return; // Esto es cuando no hay elementos

             const optionReligion = $religion.options[religionI];

             if (optionReligion.value == 'Yes') {

               document.getElementById('group__religion').classList.remove('d-none');

               $('#group__religion').html(`

                                    <label class="label__other__city">Which Religion?</label>
                                    <input class="input__other_city" type="text" name="religion"> 

                                `);

             } else {
               document.getElementById('group__religion').classList.add('d-none');
             }

           }


           const $condition = document.querySelector("#condition_m");

           function otherCondition() {

             const conditionI = $condition.selectedIndex;

             if (conditionI === -1) return; // Esto es cuando no hay elementos

             const optionCondition = $condition.options[conditionI];

             if (optionCondition.value == 'Yes') {

               document.getElementById('group__condition_m').classList.remove('d-none');

               $('#group__condition_m').html(`

                                    <label class="label__other__city">Which Condition?</label>
                                    <input class="input__other_city" type="text" name="condition_m"> 

                                `);

             } else {
               document.getElementById('group__condition_m').classList.add('d-none');
             }

           }
           </script>


           <div class="form__group-c_background form__group-hinf2 col-xl-10">

             <label for="c_background" class="label__name">Do you give us your consent to go to the authorities and
               check your criminal background check?</label>
             <label for="c_background" class="l_icon" title="Background Language"> <i class="icon icon_check"></i>
             </label>

             <select name="c_background" id="c_background" class="custom-select form__group-input background_l">

               <?php if(empty($row['c_background']) || $row['c_background'] == 'NULL'){ ?>

               <option hidden="option" selected disabled> -- Select Option -- </option>
               <option value="Yes"> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['c_background']) || $row['c_background'] == 'Yes'){ ?>

               <option value="Yes" selected> Yes </option>
               <option value="No"> No </option>

               <?php }else if(empty($row['c_background']) || $row['c_background'] == 'No'){ ?>

               <option value="No" selected> No </option>
               <option value="Yes"> Yes </option>

               <?php } ?>

             </select>

           </div>

         </div>


         <div class="form__group-row2">

           <?php if($row3['f_name1'] == 'NULL' && $row3['f_lname1'] == 'NULL' && $row3['db1'] == 'NULL' && $row3['gender1'] == 'NULL' && $row3['re1'] == 'NULL' && $row3['db_lawf1'] == 'NULL' && $row3['lawf1'] == 'NULL'){ ?>

           <button type="button" data-toggle="collapse" class="add__family" data-target="#family1"
             aria-expanded="false"> Add Family Member </button>

           <div id="family1" class="panel-collapse collapse">

             <?php }else{ ?>

             <h2 class="title__group-section member_f">Member 1</h2>

             <div id="family1" class="panel-collapse">

               <?php } ?>

               <div class="row member1">

                 <div class="form__group-fname form__group-hinf3 col-lg-4">

                   <label for="f_name1" class="label__name label__family">Name</label>
                   <label for="f_name1" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                   <?php if(empty($row3['f_name1']) || $row3['f_name1'] == 'NULL'){ ?>

                   <input type="text" class="form__group-input name1" id="f_name1" name="f_name1"
                     placeholder="e.g. Melissa">

                   <?php }else{ ?>

                   <input type="text" class="form__group-input name1" id="f_name1" name="f_name1"
                     placeholder="e.g. Melissa" value="<?php echo $row3['f_name1'] ?>">

                   <?php } ?>

                 </div>

                 <div class="form__group-flname form__group-hinf3 col-lg-4">

                   <label for="f_lname1" class="label__name label__family">Last Name</label>
                   <label for="f_lname1" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                   <?php if(empty($row3['f_lname1']) || $row3['f_lname1'] == 'NULL'){ ?>

                   <input type="text" class="form__group-input lname1" id="f_lname1" name="f_lname1"
                     placeholder="e.g. Smith">

                   <?php }else{ ?>

                   <input type="text" class="form__group-input lname1" id="f_lname1" name="f_lname1"
                     placeholder="e.g. Smith" value="<?php echo $row3['f_lname1'] ?>">

                   <?php } ?>

                 </div>

                 <div class="form__group-db1 form__group-hinf3 col-lg-4">

                   <label for="db1" class="label__name label__family">Date of Birth</label>
                   <label for="db1" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                   <?php if(empty($row3['db1']) || $row3['db1'] == 'NULL'){ ?>

                   <input type="date" class="form__group-input dbirth1" id="db1" name="db1" placeholder="MM-DD-YYYY">

                   <?php }else{ ?>

                   <input type="date" class="form__group-input dbirth1" id="db1" name="db1" placeholder="MM-DD-YYYY"
                     value="<?php echo $row3['db1'] ?>">

                   <?php } ?>

                 </div>


                 <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                   <label for="gender1" class="label__name label__family">Gender</label>
                   <label for="gender1" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                   <select class="form__group-input gender1" id="gender1" name="gender1">

                     <?php if(empty($row3['gender1']) || $row3['gender1'] == 'NULL'){ ?>

                     <option hidden="option" selected disabled>-- Select Gender --</option>
                     <option value="Male">Male</option>
                     <option value="Female">Female</option>
                     <option value="Private">Private</option>

                     <?php }else if($row3['gender1'] == 'Male'){ ?>

                     <option value="Male" selected>Male</option>
                     <option value="Female">Female</option>
                     <option value="Private">Private</option>

                     <?php }else if($row3['gender1'] == 'Female'){ ?>

                     <option value="Female" selected>Female</option>
                     <option value="Male">Male</option>
                     <option value="Private">Private</option>

                     <?php }else if($row3['gender1'] == 'Private'){ ?>

                     <option value="Private" selected>Private</option>
                     <option value="Female">Female</option>
                     <option value="Male">Male</option>

                     <?php } ?>
                   </select>

                 </div>

                 <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                   <label for="re1" class="label__name label__family">Relation</label>
                   <label for="re1" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                   <select class="custom-select form__group-input relation1" id="re1" name="re1">

                     <?php if(empty($row3['re1']) || $row3['re1'] == 'NULL'){ ?>

                     <option hidden="option" selected disabled>-- Select Relation --</option>
                     <option value="Dad">Dad</option>
                     <option value="Mom">Mom</option>
                     <option value="Son">Son</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Dad'){ ?>

                     <option value="Dad" selected>Dad</option>
                     <option value="Mom">Mom</option>
                     <option value="Son">Son</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Mom'){ ?>

                     <option value="Mom" selected>Mom</option>
                     <option value="Dad">Dad</option>
                     <option value="Son">Son</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Son'){ ?>

                     <option value="Son" selected>Son</option>
                     <option value="Mom">Mom</option>
                     <option value="Dad">Dad</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Daughter'){ ?>

                     <option value="Daughter" selected>Daughter</option>
                     <option value="Son">Son</option>
                     <option value="Mom">Mom</option>
                     <option value="Dad">Dad</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Grandparents'){ ?>

                     <option value="Grandparents" selected>Grandparents</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Son">Son</option>
                     <option value="Mom">Mom</option>
                     <option value="Dad">Dad</option>
                     <option value="Other">Others</option>

                     <?php } else if($row3['re1'] == 'Other'){ ?>

                     <option value="Other" selected>Others</option>
                     <option value="Grandparents">Grandparents</option>
                     <option value="Daughter">Daughter</option>
                     <option value="Son">Son</option>
                     <option value="Mom">Mom</option>
                     <option value="Dad">Dad</option>

                     <?php } ?>
                   </select>

                 </div>

                 <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                   <label for="occupation_f1" class="l_icon" title="Date of Background Check"> <i
                       class="icon icon_check"></i> </label>
                   <label for="occupation_f1" class="label__name label__family">Occupation</label>

                   <?php if(empty($row3['occupation_f1']) || $row3['occupation_f1'] == 'NULL'){ ?>

                   <input type="text" class="form__group-input db_law" id="occupation_f1" name="occupation_f1"
                     placeholder="e.g. Lawyer">

                   <?php }else{ ?>

                   <input type="text" class="form__group-input db_law" id="occupation_f1" name="occupation_f1"
                     placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f1'] ?>">

                   <?php } ?>
                 </div>

                 <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                   <label for="db_lawf1" class="label__name label__family">Date Background Check</label>
                   <label for="db_lawf1" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                   </label>

                   <?php if(empty($row3['db_lawf1']) || $row3['db_lawf1'] == 'NULL'){ ?>

                   <input type="date" class="form__group-input relation1" id="db_lawf1" name="db_lawf1"
                     placeholder="MM-DD-YYYY">

                   <?php }else{ ?>

                   <input type="date" class="form__group-input relation1" id="db_lawf1" name="db_lawf1"
                     placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf1'] ?>">

                   <?php } ?>

                 </div>

                 <div class="form__group-b-check col-lg-7">
                   <?php if(empty($row3['lawf1']) || $row3['lawf1'] == 'NULL'){}else{ ?>
                   <center><iframe class="embed-responsive-item mt-5 mb-0"
                       src="../../<?php echo $row3['lawf1'] ?>"></iframe></center>
                   <?php } ?>
                   <div class=" form__group-hinf3">
                     <label for="profile-img" class="label__name label__family">Background Check</label>
                     <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i>
                     </label>
                     <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1"
                       accept="pdf" style="width: 100%;">
                   </div>
                 </div>
               </div>

               <?php if($row3['f_name2'] == 'NULL' && $row3['f_lname2'] == 'NULL' && $row3['db2'] == 'NULL' && $row3['gender2'] == 'NULL' && $row3['re2'] == 'NULL' && $row3['db_lawf2'] == 'NULL' && $row3['lawf2'] == 'NULL'){ ?>

               <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family2"
                 aria-expanded="false"> Add Family Member </button>

               <?php }else{} ?>

             </div>




             <?php if($row3['f_name2'] == 'NULL' && $row3['f_lname2'] == 'NULL' && $row3['db2'] == 'NULL' && $row3['gender2'] == 'NULL' && $row3['re2'] == 'NULL' && $row3['db_lawf2'] == 'NULL' && $row3['lawf2'] == 'NULL'){ ?>

             <div id="family2" class="panel-collapse collapse">

               <?php }else{ ?>
               <br>

               <h2 class="title__group-section member_f">Member 2</h2>

               <div id="family2" class="panel-collapse">

                 <?php } ?>

                 <div class="row member1">

                   <div class="form__group-fname form__group-hinf3 col-lg-4">
                     <label for="f_name2" class="label__name label__family">Name</label>
                     <label for="f_name2" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                     <?php if(empty($row3['f_name2']) || $row3['f_name2'] == 'NULL'){ ?>

                     <input type="text" class="form__group-input name2" id="f_name2" name="f_name2"
                       placeholder="e.g. Melissa">

                     <?php }else{ ?>

                     <input type="text" class="form__group-input name2" id="f_name2" name="f_name2"
                       placeholder="e.g. Melissa" value="<?php echo $row3['f_name2'] ?>">

                     <?php } ?>

                   </div>

                   <div class="form__group-flname form__group-hinf3 col-lg-4">

                     <label for="f_lname2" class="label__name label__family">Last Name</label>
                     <label for="f_lname2" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                     <?php if(empty($row3['f_lname2']) || $row3['f_lname2'] == 'NULL'){ ?>

                     <input type="text" class="form__group-input lname2" id="f_lname2" name="f_lname2"
                       placeholder="e.g. Smith">

                     <?php }else{ ?>

                     <input type="text" class="form__group-input lname2" id="f_lname2" name="f_lname2"
                       placeholder="e.g. Smith" value="<?php echo $row3['f_lname2'] ?>">

                     <?php } ?>

                   </div>

                   <div class="form__group-db1 form__group-hinf3 col-lg-4">

                     <label for="db2" class="label__name label__family">Date of Birth</label>
                     <label for="db2" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                     <?php if(empty($row3['db2']) || $row3['db2'] == 'NULL'){ ?>

                     <input type="date" class="form__group-input db2" id="db2" name="db2" placeholder="MM-DD-YYYY">

                     <?php }else{ ?>

                     <input type="date" class="form__group-input db2" id="db2" name="db2" placeholder="MM-DD-YYYY"
                       value="<?php echo $row3['db2'] ?>">

                     <?php } ?>

                   </div>


                   <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                     <label for="gender2" class="label__name label__family">Gender</label>
                     <label for="gender2" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                     <select class="custom-select form__group-input gender2" id="gender2" name="gender2">
                       <?php if(empty($row3['gender2']) || $row3['gender2'] == 'NULL'){ ?>

                       <option hidden="option" selected disabled>-- Select Gender --</option>
                       <option value="Male">Male</option>
                       <option value="Female">Female</option>
                       <option value="Private">Private</option>

                       <?php }else if($row3['gender2'] == 'Male'){ ?>

                       <option value="Male" selected>Male</option>
                       <option value="Female">Female</option>
                       <option value="Private">Private</option>

                       <?php }else if($row3['gender2'] == 'Female'){ ?>

                       <option value="Female" selected>Female</option>
                       <option value="Male">Male</option>
                       <option value="Private">Private</option>

                       <?php }else if($row3['gender2'] == 'Private'){ ?>

                       <option value="Private" selected>Private</option>
                       <option value="Female">Female</option>
                       <option value="Male">Male</option>

                       <?php } ?>
                     </select>
                   </div>

                   <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                     <label for="re2" class="label__name label__family">Relation</label>
                     <label for="re2" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                     <select class="custom-select form__group-input relation2" id="re2" name="re2">
                       <?php if(empty($row3['re2']) || $row3['re2'] == 'NULL'){ ?>

                       <option hidden="option" selected disabled>-- Select Relation --</option>
                       <option value="Dad">Dad</option>
                       <option value="Mom">Mom</option>
                       <option value="Son">Son</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Dad'){ ?>

                       <option value="Dad" selected>Dad</option>
                       <option value="Mom">Mom</option>
                       <option value="Son">Son</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Mom'){ ?>

                       <option value="Mom" selected>Mom</option>
                       <option value="Dad">Dad</option>
                       <option value="Son">Son</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Son'){ ?>

                       <option value="Son" selected>Son</option>
                       <option value="Mom">Mom</option>
                       <option value="Dad">Dad</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Daughter'){ ?>

                       <option value="Daughter" selected>Daughter</option>
                       <option value="Son">Son</option>
                       <option value="Mom">Mom</option>
                       <option value="Dad">Dad</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Grandparents'){ ?>

                       <option value="Grandparents" selected>Grandparents</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Son">Son</option>
                       <option value="Mom">Mom</option>
                       <option value="Dad">Dad</option>
                       <option value="Other">Others</option>

                       <?php } else if($row3['re2'] == 'Other'){ ?>

                       <option value="Other" selected>Others</option>
                       <option value="Grandparents">Grandparents</option>
                       <option value="Daughter">Daughter</option>
                       <option value="Son">Son</option>
                       <option value="Mom">Mom</option>
                       <option value="Dad">Dad</option>

                       <?php } ?>
                     </select>

                   </div>


                   <div class="form__group-occupation_f2 form__group-hinf2 col-md-4">
                     <label for="occupation_f2" class="l_icon" title="Date of Background Check"> <i
                         class="icon icon_check"></i> </label>
                     <label for="occupation_f2" class="label__name label__family">Occupation</label>

                     <?php if(empty($row3['occupation_f2']) || $row3['occupation_f2'] == 'NULL'){ ?>

                     <input type="text" class="form__group-input db_law" id="occupation_f2" name="occupation_f2"
                       placeholder="e.g. Lawyer">

                     <?php }else{ ?>

                     <input type="text" class="form__group-input db_law" id="occupation_f2" name="occupation_f2"
                       placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f2'] ?>">

                     <?php } ?>

                   </div>

                   <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                     <label for="db_lawf2" class="label__name label__family">Date Background Check</label>
                     <label for="db_lawf2" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                     </label>

                     <?php if(empty($row3['db_lawf2']) || $row3['db_lawf2'] == 'NULL'){ ?>

                     <input type="date" class="form__group-input db_lawf2" id="db_lawf2" name="db_lawf2"
                       placeholder="MM-DD-YYYY">

                     <?php }else{ ?>

                     <input type="date" class="form__group-input db_lawf2" id="db_lawf2" name="db_lawf2"
                       placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf2'] ?>">

                     <?php } ?>

                   </div>

                   <div class="form__group-b-check col-lg-7">
                     <?php if(empty($row3['lawf2']) || $row3['lawf2'] == 'NULL'){}else{ ?>
                     <center><iframe class="embed-responsive-item mt-5 mb-0"
                         src="../../<?php echo $row3['lawf2'] ?>"></iframe></center>
                     <?php } ?>
                     <div class="form__group-hinf3">
                       <label for="profile-img" class="label__name label__family">Background Check</label>
                       <label for="profile-img" class="l_icon" title="Background Check"> <i
                           class="icon icon_bcheck"></i> </label>

                       <input type="file" class="form__group-input lawf2" name="image[]" id="profile-img" maxlength="1"
                         accept="pdf" style="width: 100%;">
                     </div>
                   </div>
                 </div>

                 <?php if($row3['f_name3'] == 'NULL' && $row3['f_lname3'] == 'NULL' && $row3['db3'] == 'NULL' && $row3['gender3'] == 'NULL' && $row3['re3'] == 'NULL' && $row3['db_lawf3'] == 'NULL' && $row3['lawf3'] == 'NULL'){ ?>

                 <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family3"
                   aria-expanded="false"> Add Family Member </button>

                 <?php }else{} ?>

               </div>

               <?php if($row3['f_name3'] == 'NULL' && $row3['f_lname3'] == 'NULL' && $row3['db3'] == 'NULL' && $row3['gender3'] == 'NULL' && $row3['re3'] == 'NULL' && $row3['db_lawf3'] == 'NULL' && $row3['lawf3'] == 'NULL'){ ?>

               <div id="family3" class="panel-collapse collapse">

                 <?php }else{ ?>
                 <br>

                 <h2 class="title__group-section member_f">Member 3</h2>

                 <div id="family3" class="panel-collapse">

                   <?php } ?>



                   <div class="row member1">
                     <div class="form__group-fname form__group-hinf3 col-lg-4">

                       <label for="f_name3" class="label__name label__family">Name</label>
                       <label for="f_name3" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                       <?php if(empty($row3['f_name3']) || $row3['f_name3'] == 'NULL'){ ?>

                       <input type="text" class="form__group-input name3" id="f_name3" name="f_name3"
                         placeholder="e.g. Melissa">

                       <?php }else{ ?>

                       <input type="text" class="form__group-input name3" id="f_name3" name="f_name3"
                         placeholder="e.g. Melissa" value="<?php echo $row3['f_name3'] ?>">

                       <?php } ?>

                     </div>

                     <div class="form__group-flname form__group-hinf3 col-lg-4">

                       <label for="f_lname3" class="label__name label__family">Last Name</label>
                       <label for="f_lname3" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>

                       <?php if(empty($row3['f_lname3']) || $row3['f_lname3'] == 'NULL'){ ?>

                       <input type="text" class="form__group-input lname3" id="f_lname3" name="f_lname3"
                         placeholder="e.g. Smith">

                       <?php }else{ ?>

                       <input type="text" class="form__group-input lname3" id="f_lname3" name="f_lname3"
                         placeholder="e.g. Smith" value="<?php echo $row3['f_lname3'] ?>">

                       <?php } ?>

                     </div>

                     <div class="form__group-db1 form__group-hinf3 col-lg-4">

                       <label for="db3" class="label__name label__family">Date of Birth</label>
                       <label for="db3" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                       <?php if(empty($row3['db3']) || $row3['db3'] == 'NULL'){ ?>

                       <input type="date" class="form__group-input db3" id="db3" name="db3" placeholder="MM-DD-YYYY">

                       <?php }else{ ?>

                       <input type="date" class="form__group-input db3" id="db3" name="db3" placeholder="MM-DD-YYYY"
                         value="<?php echo $row3['db3'] ?>">

                       <?php } ?>

                     </div>

                     <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                       <label for="gender3" class="label__name label__family">Gender</label>
                       <label for="gender3" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                       <select class="custom-select form__group-input gender3" id="gender3" name="gender3">
                         <?php if(empty($row3['gender3']) || $row3['gender3'] == 'NULL'){ ?>

                         <option hidden="option" selected disabled>-- Select Gender --</option>
                         <option value="Male">Male</option>
                         <option value="Female">Female</option>
                         <option value="Private">Private</option>

                         <?php }else if($row3['gender3'] == 'Male'){ ?>

                         <option value="Male" selected>Male</option>
                         <option value="Female">Female</option>
                         <option value="Private">Private</option>

                         <?php }else if($row3['gender3'] == 'Female'){ ?>

                         <option value="Female" selected>Female</option>
                         <option value="Male">Male</option>
                         <option value="Private">Private</option>

                         <?php }else if($row3['gender3'] == 'Private'){ ?>

                         <option value="Private" selected>Private</option>
                         <option value="Female">Female</option>
                         <option value="Male">Male</option>

                         <?php } ?>
                       </select>
                     </div>

                     <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                       <label for="re3" class="label__name label__family">Relation</label>
                       <label for="re3" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                       <select class="custom-select form__group-input relation3" id="re3" name="re3">
                         <?php if(empty($row3['re3']) || $row3['re3'] == 'NULL'){ ?>

                         <option hidden="option" selected disabled>-- Select Relation --</option>
                         <option value="Dad">Dad</option>
                         <option value="Mom">Mom</option>
                         <option value="Son">Son</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Dad'){ ?>

                         <option value="Dad" selected>Dad</option>
                         <option value="Mom">Mom</option>
                         <option value="Son">Son</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Mom'){ ?>

                         <option value="Mom" selected>Mom</option>
                         <option value="Dad">Dad</option>
                         <option value="Son">Son</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Son'){ ?>

                         <option value="Son" selected>Son</option>
                         <option value="Mom">Mom</option>
                         <option value="Dad">Dad</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Daughter'){ ?>

                         <option value="Daughter" selected>Daughter</option>
                         <option value="Son">Son</option>
                         <option value="Mom">Mom</option>
                         <option value="Dad">Dad</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Grandparents'){ ?>

                         <option value="Grandparents" selected>Grandparents</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Son">Son</option>
                         <option value="Mom">Mom</option>
                         <option value="Dad">Dad</option>
                         <option value="Other">Others</option>

                         <?php } else if($row3['re3'] == 'Other'){ ?>

                         <option value="Other" selected>Others</option>
                         <option value="Grandparents">Grandparents</option>
                         <option value="Daughter">Daughter</option>
                         <option value="Son">Son</option>
                         <option value="Mom">Mom</option>
                         <option value="Dad">Dad</option>

                         <?php } ?>
                       </select>
                     </div>

                     <div class="form__group-occupation_f3 form__group-hinf2 col-md-4">
                       <label for="occupation_f3" class="l_icon" title="Date of Background Check"> <i
                           class="icon icon_check"></i> </label>
                       <label for="occupation_f3" class="label__name label__family">Occupation</label>

                       <?php if(empty($row3['occupation_f3']) || $row3['occupation_f3'] == 'NULL'){ ?>

                       <input type="text" class="form__group-input db_law" id="occupation_f3" name="occupation_f3"
                         placeholder="e.g. Lawyer">

                       <?php }else{ ?>

                       <input type="text" class="form__group-input db_law" id="occupation_f3" name="occupation_f3"
                         placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f3'] ?>">

                       <?php } ?>

                     </div>


                     <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                       <label for="db_lawf3" class="label__name label__family">Date Background Check</label>
                       <label for="db_lawf3" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i>
                       </label>

                       <?php if(empty($row3['db_lawf3']) || $row3['db_lawf3'] == 'NULL'){ ?>

                       <input type="date" class="form__group-input db_lawf3" id="db_lawf3" name="db_lawf3"
                         placeholder="MM-DD-YYYY">

                       <?php }else{ ?>

                       <input type="date" class="form__group-input db_lawf3" id="db_lawf3" name="db_lawf3"
                         placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf3'] ?>">

                       <?php } ?>

                     </div>

                     <div class="form__group-b-check col-lg-7">
                       <?php if(empty($row3['lawf3']) || $row3['lawf3'] == 'NULL'){}else{ ?>
                       <center><iframe class="embed-responsive-item mt-5 mb-0"
                           src="../../<?php echo $row3['lawf3'] ?>"></iframe></center>
                       <?php } ?>
                       <div class=" form__group-hinf3 ">
                         <label for="profile-img" class="label__name label__family">Background Check</label>
                         <label for="profile-img" class="l_icon" title="Background Check"> <i
                             class="icon icon_bcheck"></i> </label>
                         <input type="file" class="form__group-input lawf3" name="image[]" id="profile-img"
                           maxlength="1" accept="pdf" style="width: 100%;">
                       </div>
                     </div>
                   </div>

                   <?php if($row3['f_name4'] == 'NULL' && $row3['f_lname4'] == 'NULL' && $row3['db4'] == 'NULL' && $row3['gender4'] == 'NULL' && $row3['re4'] == 'NULL' && $row3['db_lawf4'] == 'NULL' && $row3['lawf4'] == 'NULL'){ ?>

                   <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family4"
                     aria-expanded="false"> Add Family Member </button>

                   <?php }else{} ?>

                 </div>

                 <?php if($row3['f_name4'] == 'NULL' && $row3['f_lname4'] == 'NULL' && $row3['db4'] == 'NULL' && $row3['gender4'] == 'NULL' && $row3['re4'] == 'NULL' && $row3['db_lawf4'] == 'NULL' && $row3['lawf4'] == 'NULL'){ ?>

                 <div id="family4" class="panel-collapse collapse">

                   <?php }else{ ?>
                   <br>

                   <h2 class="title__group-section member_f">Member 4</h2>

                   <div id="family4" class="panel-collapse">

                     <?php } ?>

                     <div class="row member1">
                       <div class="form__group-fname form__group-hinf3 col-lg-4">

                         <label for="f_name4" class="label__name label__family">Name</label>
                         <label for="f_name4" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                         <?php if(empty($row3['f_name4']) || $row3['f_name4'] == 'NULL'){ ?>

                         <input type="text" class="form__group-input name4" id="f_name4" name="f_name4"
                           placeholder="e.g. Melissa">

                         <?php }else{ ?>

                         <input type="text" class="form__group-input name4" id="f_name4" name="f_name4"
                           placeholder="e.g. Melissa" value="<?php echo $row3['f_name4'] ?>">

                         <?php } ?>

                       </div>

                       <div class="form__group-flname form__group-hinf3 col-lg-4">

                         <label for="f_lname4" class="label__name label__family">Last Name</label>
                         <label for="f_lname4" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i>
                         </label>

                         <?php if(empty($row3['f_lname4']) || $row3['f_lname4'] == 'NULL'){ ?>

                         <input type="text" class="form__group-input lname4" id="f_lname4" name="f_lname4"
                           placeholder="e.g. Smith">

                         <?php }else{ ?>

                         <input type="text" class="form__group-input lname4" id="f_lname4" name="f_lname4"
                           placeholder="e.g. Smith" value="<?php echo $row3['f_lname4'] ?>">

                         <?php } ?>

                       </div>

                       <div class="form__group-db1 form__group-hinf3 col-lg-4">

                         <label for="db4" class="label__name label__family">Date of Birth</label>
                         <label for="db4" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                         <?php if(empty($row3['db4']) || $row3['db4'] == 'NULL'){ ?>

                         <input type="date" class="form__group-input db4" id="db4" name="db4" placeholder="MM-DD-YYYY">

                         <?php }else{ ?>

                         <input type="date" class="form__group-input db4" id="db4" name="db4" placeholder="MM-DD-YYYY"
                           value="<?php echo $row3['db4'] ?>">

                         <?php } ?>

                       </div>

                       <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                         <label for="gender4" class="label__name label__family">Gender</label>
                         <label for="gender4" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                         <select class="custom-select form__group-input gender4" id="gender4" name="gender4">
                           <?php if(empty($row3['gender4']) || $row3['gender4'] == 'NULL'){ ?>

                           <option hidden="option" selected disabled>-- Select Gender --</option>
                           <option value="Male">Male</option>
                           <option value="Female">Female</option>
                           <option value="Private">Private</option>

                           <?php }else if($row3['gender4'] == 'Male'){ ?>

                           <option value="Male" selected>Male</option>
                           <option value="Female">Female</option>
                           <option value="Private">Private</option>

                           <?php }else if($row3['gender4'] == 'Female'){ ?>

                           <option value="Female" selected>Female</option>
                           <option value="Male">Male</option>
                           <option value="Private">Private</option>

                           <?php }else if($row3['gender4'] == 'Private'){ ?>

                           <option value="Private" selected>Private</option>
                           <option value="Female">Female</option>
                           <option value="Male">Male</option>

                           <?php } ?>
                         </select>
                       </div>

                       <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                         <label for="re4" class="label__name label__family">Relation</label>
                         <label for="re4" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                         <select class="custom-select form__group-input relation4" id="re4" name="re4">
                           <?php if(empty($row3['re4']) || $row3['re4'] == 'NULL'){ ?>

                           <option hidden="option" selected disabled>-- Select Relation --</option>
                           <option value="Dad">Dad</option>
                           <option value="Mom">Mom</option>
                           <option value="Son">Son</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Dad'){ ?>

                           <option value="Dad" selected>Dad</option>
                           <option value="Mom">Mom</option>
                           <option value="Son">Son</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Mom'){ ?>

                           <option value="Mom" selected>Mom</option>
                           <option value="Dad">Dad</option>
                           <option value="Son">Son</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Son'){ ?>

                           <option value="Son" selected>Son</option>
                           <option value="Mom">Mom</option>
                           <option value="Dad">Dad</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Daughter'){ ?>

                           <option value="Daughter" selected>Daughter</option>
                           <option value="Son">Son</option>
                           <option value="Mom">Mom</option>
                           <option value="Dad">Dad</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Grandparents'){ ?>

                           <option value="Grandparents" selected>Grandparents</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Son">Son</option>
                           <option value="Mom">Mom</option>
                           <option value="Dad">Dad</option>
                           <option value="Other">Others</option>

                           <?php } else if($row3['re4'] == 'Other'){ ?>

                           <option value="Other" selected>Others</option>
                           <option value="Grandparents">Grandparents</option>
                           <option value="Daughter">Daughter</option>
                           <option value="Son">Son</option>
                           <option value="Mom">Mom</option>
                           <option value="Dad">Dad</option>

                           <?php } ?>
                         </select>
                       </div>

                       <div class="form__group-occupation_f4 form__group-hinf2 col-md-4">
                         <label for="occupation_f4" class="l_icon" title="Date of Background Check"> <i
                             class="icon icon_check"></i> </label>
                         <label for="occupation_f4" class="label__name label__family">Occupation</label>

                         <?php if(empty($row3['occupation_f4']) || $row3['occupation_f4'] == 'NULL'){ ?>

                         <input type="text" class="form__group-input db_law" id="occupation_f4" name="occupation_f4"
                           placeholder="e.g. Lawyer">

                         <?php }else{ ?>

                         <input type="text" class="form__group-input db_law" id="occupation_f4" name="occupation_f4"
                           placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f4'] ?>">

                         <?php } ?>

                       </div>

                       <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                         <label for="db_lawf4" class="label__name label__family">Date Background Check</label>
                         <label for="db_lawf4" class="l_icon" title="Date Background Check"> <i
                             class="icon icon_db"></i> </label>

                         <?php if(empty($row3['db_lawf4']) || $row3['db_lawf4'] == 'NULL'){ ?>

                         <input type="date" class="form__group-input db_lawf4" id="db_lawf4" name="db_lawf4"
                           placeholder="MM-DD-YYYY">

                         <?php }else{ ?>

                         <input type="date" class="form__group-input db_lawf4" id="db_lawf4" name="db_lawf4"
                           placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf4'] ?>">

                         <?php } ?>

                       </div>

                       <div class="form__group-b-check col-lg-7">

                         <?php if(empty($row3['lawf4']) || $row3['lawf4'] == 'NULL'){}else{ ?>
                         <center><iframe class="embed-responsive-item mt-5 mb-0"
                             src="../../<?php echo $row3['lawf4'] ?>"></iframe></center>
                         <?php } ?>
                         <div class=" form__group-hinf3 ">
                           <label for="profile-img" class="label__name label__family">Background Check</label>
                           <label for="profile-img" class="l_icon" title="Background Check"> <i
                               class="icon icon_bcheck"></i> </label>
                           <input type="file" class="form__group-input lawf4" name="image[]" id="profile-img"
                             maxlength="1" accept="pdf" style="width: 100%;">
                         </div>

                       </div>
                     </div>


                     <?php if($row3['f_name5'] == 'NULL' && $row3['f_lname5'] == 'NULL' && $row3['db5'] == 'NULL' && $row3['gender5'] == 'NULL' && $row3['re5'] == 'NULL' && $row3['db_lawf5'] == 'NULL' && $row3['lawf5'] == 'NULL'){ ?>

                     <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family5"
                       aria-expanded="false"> Add Family Member </button>

                     <?php }else{} ?>

                   </div>


                   <?php if($row3['f_name5'] == 'NULL' && $row3['f_lname5'] == 'NULL' && $row3['db5'] == 'NULL' && $row3['gender5'] == 'NULL' && $row3['re5'] == 'NULL' && $row3['db_lawf5'] == 'NULL' && $row3['lawf5'] == 'NULL'){ ?>

                   <div id="family5" class="panel-collapse collapse">

                     <?php }else{ ?>
                     <br>

                     <h2 class="title__group-section member_f">Member 5</h2>

                     <div id="family5" class="panel-collapse">

                       <?php } ?>


                       <div class="row member1">
                         <div class="form__group-fname form__group-hinf3 col-lg-4">

                           <label for="f_name5" class="label__name label__family">Name</label>
                           <label for="f_name5" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                           <?php if(empty($row3['f_name5']) || $row3['f_name5'] == 'NULL'){ ?>

                           <input type="text" class="form__group-input name5" id="f_name5" name="f_name5"
                             placeholder="e.g. Melissa">

                           <?php }else{ ?>

                           <input type="text" class="form__group-input name5" id="f_name5" name="f_name5"
                             placeholder="e.g. Melissa" value="<?php echo $row3['f_name5'] ?>">

                           <?php } ?>

                         </div>

                         <div class="form__group-flname form__group-hinf3 col-lg-4">

                           <label for="f_lname5" class="label__name label__family">Last Name</label>
                           <label for="f_lname5" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i>
                           </label>

                           <?php if(empty($row3['f_lname5']) || $row3['f_lname5'] == 'NULL'){ ?>

                           <input type="text" class="form__group-input lname5" id="f_lname5" name="f_lname5"
                             placeholder="e.g. Smith">

                           <?php }else{ ?>

                           <input type="text" class="form__group-input lname5" id="f_lname5" name="f_lname5"
                             placeholder="e.g. Smith" value="<?php echo $row3['f_lname5'] ?>">

                           <?php } ?>

                         </div>

                         <div class="form__group-db1 form__group-hinf3 col-lg-4">

                           <label for="db5" class="label__name label__family">Date of Birth</label>
                           <label for="db5" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>

                           <?php if(empty($row3['db5']) || $row3['db5'] == 'NULL'){ ?>

                           <input type="date" class="form__group-input db5" id="db5" name="db5"
                             placeholder="MM-DD-YYYY">

                           <?php }else{ ?>

                           <input type="date" class="form__group-input db5" id="db5" name="db5" placeholder="MM-DD-YYYY"
                             value="<?php echo $row3['db5'] ?>">

                           <?php } ?>

                         </div>

                         <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                           <label for="gender5" class="label__name label__family">Gender</label>
                           <label for="gender5" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i>
                           </label>
                           <select class="custom-select form__group-input gender5" id="gender5" name="gender5">
                             <?php if(empty($row3['gender5']) || $row3['gender5'] == 'NULL'){ ?>

                             <option hidden="option" selected disabled>-- Select Gender --</option>
                             <option value="Male">Male</option>
                             <option value="Female">Female</option>
                             <option value="Private">Private</option>

                             <?php }else if($row3['gender5'] == 'Male'){ ?>

                             <option value="Male" selected>Male</option>
                             <option value="Female">Female</option>
                             <option value="Private">Private</option>

                             <?php }else if($row3['gender5'] == 'Female'){ ?>

                             <option value="Female" selected>Female</option>
                             <option value="Male">Male</option>
                             <option value="Private">Private</option>

                             <?php }else if($row3['gender5'] == 'Private'){ ?>

                             <option value="Private" selected>Private</option>
                             <option value="Female">Female</option>
                             <option value="Male">Male</option>

                             <?php } ?>
                           </select>
                         </div>

                         <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                           <label for="re5" class="label__name label__family">Relation</label>
                           <label for="re5" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                           <select class="custom-select form__group-input re5" id="re5" name="re5">
                             <?php if(empty($row3['re5']) || $row3['re5'] == 'NULL'){ ?>

                             <option hidden="option" selected disabled>-- Select Relation --</option>
                             <option value="Dad">Dad</option>
                             <option value="Mom">Mom</option>
                             <option value="Son">Son</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Dad'){ ?>

                             <option value="Dad" selected>Dad</option>
                             <option value="Mom">Mom</option>
                             <option value="Son">Son</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Mom'){ ?>

                             <option value="Mom" selected>Mom</option>
                             <option value="Dad">Dad</option>
                             <option value="Son">Son</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Son'){ ?>

                             <option value="Son" selected>Son</option>
                             <option value="Mom">Mom</option>
                             <option value="Dad">Dad</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Daughter'){ ?>

                             <option value="Daughter" selected>Daughter</option>
                             <option value="Son">Son</option>
                             <option value="Mom">Mom</option>
                             <option value="Dad">Dad</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Grandparents'){ ?>

                             <option value="Grandparents" selected>Grandparents</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Son">Son</option>
                             <option value="Mom">Mom</option>
                             <option value="Dad">Dad</option>
                             <option value="Other">Others</option>

                             <?php } else if($row3['re5'] == 'Other'){ ?>

                             <option value="Other" selected>Others</option>
                             <option value="Grandparents">Grandparents</option>
                             <option value="Daughter">Daughter</option>
                             <option value="Son">Son</option>
                             <option value="Mom">Mom</option>
                             <option value="Dad">Dad</option>

                             <?php } ?>
                           </select>
                         </div>

                         <div class="form__group-occupation_f5 form__group-hinf2 col-md-4">
                           <label for="occupation_f5" class="l_icon" title="Date of Background Check"> <i
                               class="icon icon_check"></i> </label>
                           <label for="occupation_f5" class="label__name label__family">Occupation</label>

                           <?php if(empty($row3['occupation_f5']) || $row3['occupation_f5'] == 'NULL'){ ?>

                           <input type="text" class="form__group-input db_law" id="occupation_f5" name="occupation_f5"
                             placeholder="e.g. Lawyer">

                           <?php }else{ ?>

                           <input type="text" class="form__group-input db_law" id="occupation_f5" name="occupation_f5"
                             placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f5'] ?>">

                           <?php } ?>
                         </div>

                         <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                           <label for="db_lawf5" class="label__name label__family">Date Background Check</label>
                           <label for="db_lawf5" class="l_icon" title="Date Background Check"> <i
                               class="icon icon_db"></i> </label>

                           <?php if(empty($row3['db_lawf5']) || $row3['db_lawf5'] == 'NULL'){ ?>

                           <input type="date" class="form__group-input db_lawf5" id="db_lawf5" name="db_lawf5"
                             placeholder="MM-DD-YYYY">

                           <?php }else{ ?>

                           <input type="date" class="form__group-input db_lawf5" id="db_lawf5" name="db_lawf5"
                             placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf5'] ?>">

                           <?php } ?>

                         </div>

                         <div class="form__group-b-check col-lg-7">

                           <?php if(empty($row3['lawf5']) || $row3['lawf5'] == 'NULL'){}else{ ?>
                           <center><iframe class="embed-responsive-item mt-5 mb-0"
                               src="../../<?php echo $row3['lawf5'] ?>"></iframe></center>
                           <?php } ?>
                           <div class=" form__group-hinf3 ">
                             <label for="profile-img" class="label__name label__family">Background Check</label>
                             <label for="profile-img" class="l_icon" title="Background Check"> <i
                                 class="icon icon_bcheck"></i> </label>
                             <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1"
                               accept="pdf" style="width: 100%;">
                           </div>
                         </div>
                       </div>

                       <?php if($row3['f_name6'] == 'NULL' && $row3['f_lname6'] == 'NULL' && $row3['db6'] == 'NULL' && $row3['gender6'] == 'NULL' && $row3['re6'] == 'NULL' && $row3['db_lawf6'] == 'NULL' && $row3['lawf6'] == 'NULL'){ ?>

                       <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family6"
                         aria-expanded="false"> Add Family Member </button>

                       <?php }else{} ?>

                     </div>

                     <?php if($row3['f_name6'] == 'NULL' && $row3['f_lname6'] == 'NULL' && $row3['db6'] == 'NULL' && $row3['gender6'] == 'NULL' && $row3['re6'] == 'NULL' && $row3['db_lawf6'] == 'NULL' && $row3['lawf6'] == 'NULL'){ ?>

                     <div id="family6" class="panel-collapse collapse">

                       <?php }else{ ?>
                       <br>

                       <h2 class="title__group-section member_f">Member 6</h2>

                       <div id="family6" class="panel-collapse">

                         <?php } ?>


                         <div class="row member1">
                           <div class="form__group-fname form__group-hinf3 col-lg-4">

                             <label for="f_name6" class="label__name label__family">Name</label>
                             <label for="f_name6" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>

                             <?php if(empty($row3['f_name6']) || $row3['f_name6'] == 'NULL'){ ?>

                             <input type="text" class="form__group-input name6" id="f_name6" name="f_name6"
                               placeholder="e.g. Melissa">

                             <?php }else{ ?>

                             <input type="text" class="form__group-input name6" id="f_name6" name="f_name6"
                               placeholder="e.g. Melissa" value="<?php echo $row3['f_name6'] ?>">

                             <?php } ?>

                           </div>

                           <div class="form__group-flname form__group-hinf3 col-lg-4">

                             <label for="f_lname6" class="label__name label__family">Last Name</label>
                             <label for="f_lname6" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i>
                             </label>

                             <?php if(empty($row3['f_lname6']) || $row3['f_lname6'] == 'NULL'){ ?>

                             <input type="text" class="form__group-input lname6" id="f_lname6" name="f_lname6"
                               placeholder="e.g. Smith">

                             <?php }else{ ?>

                             <input type="text" class="form__group-input lname6" id="f_lname6" name="f_lname6"
                               placeholder="e.g. Smith" value="<?php echo $row3['f_lname6'] ?>">

                             <?php } ?>

                           </div>

                           <div class="form__group-db1 form__group-hinf3 col-lg-4">

                             <label for="db6" class="label__name label__family">Date of Birth</label>
                             <label for="db6" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i>
                             </label>

                             <?php if(empty($row3['db6']) || $row3['db6'] == 'NULL'){ ?>

                             <input type="date" class="form__group-input db6" id="db6" name="db6"
                               placeholder="MM-DD-YYYY">

                             <?php }else{ ?>

                             <input type="date" class="form__group-input db6" id="db6" name="db6"
                               placeholder="MM-DD-YYYY" value="<?php echo $row3['db6'] ?>">

                             <?php } ?>

                           </div>


                           <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                             <label for="gender6" class="label__name label__family">Gender</label>
                             <label for="gender6" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i>
                             </label>
                             <select class="custom-select form__group-input gender6" id="gender6" name="gender6">
                               <?php if(empty($row3['gender6']) || $row3['gender6'] == 'NULL'){ ?>

                               <option hidden="option" selected disabled>-- Select Gender --</option>
                               <option value="Male">Male</option>
                               <option value="Female">Female</option>
                               <option value="Private">Private</option>

                               <?php }else if($row3['gender6'] == 'Male'){ ?>

                               <option value="Male" selected>Male</option>
                               <option value="Female">Female</option>
                               <option value="Private">Private</option>

                               <?php }else if($row3['gender6'] == 'Female'){ ?>

                               <option value="Female" selected>Female</option>
                               <option value="Male">Male</option>
                               <option value="Private">Private</option>

                               <?php }else if($row3['gender6'] == 'Private'){ ?>

                               <option value="Private" selected>Private</option>
                               <option value="Female">Female</option>
                               <option value="Male">Male</option>

                               <?php } ?>
                             </select>
                           </div>

                           <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                             <label for="re6" class="label__name label__family">Relation</label>
                             <label for="re6" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                             <select class="custom-select form__group-input relation6" id="re6" name="re6">
                               <?php if(empty($row3['re6']) || $row3['re6'] == 'NULL'){ ?>

                               <option hidden="option" selected disabled>-- Select Relation --</option>
                               <option value="Dad">Dad</option>
                               <option value="Mom">Mom</option>
                               <option value="Son">Son</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Dad'){ ?>

                               <option value="Dad" selected>Dad</option>
                               <option value="Mom">Mom</option>
                               <option value="Son">Son</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Mom'){ ?>

                               <option value="Mom" selected>Mom</option>
                               <option value="Dad">Dad</option>
                               <option value="Son">Son</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Son'){ ?>

                               <option value="Son" selected>Son</option>
                               <option value="Mom">Mom</option>
                               <option value="Dad">Dad</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Daughter'){ ?>

                               <option value="Daughter" selected>Daughter</option>
                               <option value="Son">Son</option>
                               <option value="Mom">Mom</option>
                               <option value="Dad">Dad</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Grandparents'){ ?>

                               <option value="Grandparents" selected>Grandparents</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Son">Son</option>
                               <option value="Mom">Mom</option>
                               <option value="Dad">Dad</option>
                               <option value="Other">Others</option>

                               <?php } else if($row3['re6'] == 'Other'){ ?>

                               <option value="Other" selected>Others</option>
                               <option value="Grandparents">Grandparents</option>
                               <option value="Daughter">Daughter</option>
                               <option value="Son">Son</option>
                               <option value="Mom">Mom</option>
                               <option value="Dad">Dad</option>

                               <?php } ?>
                             </select>
                           </div>

                           <div class="form__group-occupation_f6 form__group-hinf2 col-md-4">
                             <label for="occupation_f6" class="l_icon" title="Date of Background Check"> <i
                                 class="icon icon_check"></i> </label>
                             <label for="occupation_f6" class="label__name label__family">Occupation</label>

                             <?php if(empty($row3['occupation_f6']) || $row3['occupation_f6'] == 'NULL'){ ?>

                             <input type="text" class="form__group-input db_law" id="occupation_f6" name="occupation_f6"
                               placeholder="e.g. Lawyer">

                             <?php }else{ ?>

                             <input type="text" class="form__group-input db_law" id="occupation_f6" name="occupation_f6"
                               placeholder="e.g. Lawyer" value="<?php echo $row3['occupation_f6'] ?>">

                             <?php } ?>
                           </div>

                           <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                             <label for="db_lawf6" class="label__name label__family">Date Background Check</label>
                             <label for="db_lawf6" class="l_icon" title="Date Background Check"> <i
                                 class="icon icon_db"></i> </label>

                             <?php if(empty($row3['db_lawf6']) || $row3['db_lawf6'] == 'NULL'){ ?>

                             <input type="date" class="form__group-input db_lawf6" id="db_lawf6" name="db_lawf6"
                               placeholder="MM-DD-YYYY">

                             <?php }else{ ?>

                             <input type="date" class="form__group-input db_lawf6" id="db_lawf6" name="db_lawf6"
                               placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf6'] ?>">

                             <?php } ?>

                           </div>

                           <div class="form__group-b-check col-lg-7">

                             <?php if(empty($row3['lawf6']) || $row3['lawf6'] == 'NULL'){}else{ ?>
                             <center><iframe class="embed-responsive-item mt-5 mb-0"
                                 src="../../<?php echo $row3['lawf6'] ?>"></iframe></center>
                             <?php } ?>
                             <div class=" form__group-hinf3 ">
                               <label for="profile-img" class="label__name label__family">Background Check</label>
                               <label for="profile-img" class="l_icon" title="Background Check"> <i
                                   class="icon icon_bcheck"></i> </label>
                               <input type="file" class="form__group-input" name="image[]" id="profile-img"
                                 maxlength="1" accept="pdf" style="width: 100%;">
                             </div>

                           </div>
                         </div>

                         <?php if($row3['f_name7'] == 'NULL' && $row3['f_lname7'] == 'NULL' && $row3['db7'] == 'NULL' && $row3['gender7'] == 'NULL' && $row3['re7'] == 'NULL' && $row3['db_lawf7'] == 'NULL' && $row3['lawf7'] == 'NULL'){ ?>

                         <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family7"
                           aria-expanded="false"> Add Family Member </button>

                         <?php }else{} ?>

                       </div>

                       <?php if($row3['f_name7'] == 'NULL' && $row3['f_lname7'] == 'NULL' && $row3['db7'] == 'NULL' && $row3['gender7'] == 'NULL' && $row3['re7'] == 'NULL' && $row3['db_lawf7'] == 'NULL' && $row3['lawf7'] == 'NULL'){ ?>

                       <div id="family7" class="panel-collapse collapse">

                         <?php }else{ ?>
                         <br>

                         <h2 class="title__group-section member_f">Member 7</h2>

                         <div id="family7" class="panel-collapse">

                           <?php } ?>


                           <div class="row member1">
                             <div class="form__group-fname form__group-hinf3 col-lg-4">

                               <label for="f_name7" class="label__name label__family">Name</label>
                               <label for="f_name7" class="l_icon" title="Name"> <i class="icon icon_mname"></i>
                               </label>

                               <?php if(empty($row3['f_name7']) || $row3['f_name7'] == 'NULL'){ ?>

                               <input type="text" class="form__group-input name7" id="f_name7" name="f_name7"
                                 placeholder="e.g. Melissa">

                               <?php }else{ ?>

                               <input type="text" class="form__group-input name7" id="f_name7" name="f_name7"
                                 placeholder="e.g. Melissa" value="<?php echo $row3['f_name7'] ?>">

                               <?php } ?>

                             </div>

                             <div class="form__group-flname form__group-hinf3 col-lg-4">

                               <label for="f_lname7" class="label__name label__family">Last Name</label>
                               <label for="f_lname7" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i>
                               </label>

                               <?php if(empty($row3['f_lname7']) || $row3['f_lname7'] == 'NULL'){ ?>

                               <input type="text" class="form__group-input lname7" id="f_lname7" name="f_lname7"
                                 placeholder="e.g. Smith">

                               <?php }else{ ?>

                               <input type="text" class="form__group-input lname7" id="f_lname7" name="f_lname7"
                                 placeholder="e.g. Smith" value="<?php echo $row3['f_lname7'] ?>">

                               <?php } ?>

                             </div>

                             <div class="form__group-db1 form__group-hinf3 col-lg-4">

                               <label for="db7" class="label__name label__family">Date of Birth</label>
                               <label for="db7" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i>
                               </label>

                               <?php if(empty($row3['db7']) || $row3['db7'] == 'NULL'){ ?>

                               <input type="date" class="form__group-input db7" id="db7" name="db7"
                                 placeholder="MM-DD-YYYY">

                               <?php }else{ ?>

                               <input type="date" class="form__group-input db7" id="db7" name="db7"
                                 placeholder="MM-DD-YYYY" value="<?php echo $row3['db7'] ?>">

                               <?php } ?>

                             </div>

                             <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                               <label for="gender7" class="label__name label__family">Gender</label>
                               <label for="gender7" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i>
                               </label>
                               <select class="custom-select form__group-input gender7" id="gender7" name="gender7">
                                 <?php if(empty($row3['gender7']) || $row3['gender7'] == 'NULL'){ ?>

                                 <option hidden="option" selected disabled>-- Select Gender --</option>
                                 <option value="Male">Male</option>
                                 <option value="Female">Female</option>
                                 <option value="Private">Private</option>

                                 <?php }else if($row3['gender7'] == 'Male'){ ?>

                                 <option value="Male" selected>Male</option>
                                 <option value="Female">Female</option>
                                 <option value="Private">Private</option>

                                 <?php }else if($row3['gender7'] == 'Female'){ ?>

                                 <option value="Female" selected>Female</option>
                                 <option value="Male">Male</option>
                                 <option value="Private">Private</option>

                                 <?php }else if($row3['gender7'] == 'Private'){ ?>

                                 <option value="Private" selected>Private</option>
                                 <option value="Female">Female</option>
                                 <option value="Male">Male</option>

                                 <?php } ?>
                               </select>
                             </div>

                             <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                               <label for="re7" class="label__name label__family">Relation</label>
                               <label for="re7" class="l_icon" title="Relation"> <i class="icon icon_family"></i>
                               </label>
                               <select class="custom-select form__group-input relation7" id="re7" name="re7">
                                 <?php if(empty($row3['re7']) || $row3['re7'] == 'NULL'){ ?>

                                 <option hidden="option" selected disabled>-- Select Relation --</option>
                                 <option value="Dad">Dad</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Son">Son</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Dad'){ ?>

                                 <option value="Dad" selected>Dad</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Son">Son</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Mom'){ ?>

                                 <option value="Mom" selected>Mom</option>
                                 <option value="Dad">Dad</option>
                                 <option value="Son">Son</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Son'){ ?>

                                 <option value="Son" selected>Son</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Dad">Dad</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Daughter'){ ?>

                                 <option value="Daughter" selected>Daughter</option>
                                 <option value="Son">Son</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Dad">Dad</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Grandparents'){ ?>

                                 <option value="Grandparents" selected>Grandparents</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Son">Son</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Dad">Dad</option>
                                 <option value="Other">Others</option>

                                 <?php } else if($row3['re7'] == 'Other'){ ?>

                                 <option value="Other" selected>Others</option>
                                 <option value="Grandparents">Grandparents</option>
                                 <option value="Daughter">Daughter</option>
                                 <option value="Son">Son</option>
                                 <option value="Mom">Mom</option>
                                 <option value="Dad">Dad</option>

                                 <?php } ?>
                               </select>
                             </div>

                             <div class="form__group-occupation_f7 form__group-hinf2 col-md-4">
                               <label for="occupation_f7" class="l_icon" title="Date of Background Check"> <i
                                   class="icon icon_check"></i> </label>
                               <label for="occupation_f7" class="label__name label__family">Occupation</label>

                               <?php if(empty($row3['occupation_f7']) || $row3['occupation_f7'] == 'NULL'){ ?>

                               <input type="text" class="form__group-input db_law" id="occupation_f7"
                                 name="occupation_f7" placeholder="e.g. Lawyer">

                               <?php }else{ ?>

                               <input type="text" class="form__group-input db_law" id="occupation_f7"
                                 name="occupation_f7" placeholder="e.g. Lawyer"
                                 value="<?php echo $row3['occupation_f7'] ?>">

                               <?php } ?>
                             </div>

                             <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                               <label for="db_lawf7" class="label__name label__family">Date Background Check</label>
                               <label for="db_lawf7" class="l_icon" title="Date Background Check"> <i
                                   class="icon icon_db"></i> </label>

                               <?php if(empty($row3['db_lawf7']) || $row3['db_lawf7'] == 'NULL'){ ?>

                               <input type="date" class="form__group-input db_lawf7" id="db_lawf7" name="db_lawf7"
                                 placeholder="MM-DD-YYYY">

                               <?php }else{ ?>

                               <input type="date" class="form__group-input db_lawf7" id="db_lawf7" name="db_lawf7"
                                 placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf7'] ?>">

                               <?php } ?>

                             </div>

                             <div class="form__group-b-check col-lg-7">

                               <?php if(empty($row3['lawf7']) || $row3['lawf7'] == 'NULL'){}else{ ?>
                               <center><iframe class="embed-responsive-item mt-5 mb-0"
                                   src="../../<?php echo $row3['lawf7'] ?>"></iframe></center>
                               <?php } ?>
                               <div class=" form__group-hinf3 ">
                                 <label for="profile-img" class="label__name label__family">Background Check</label>
                                 <label for="profile-img" class="l_icon" title="Background Check"> <i
                                     class="icon icon_bcheck"></i> </label>
                                 <input type="file" class="form__group-input" name="image[]" id="profile-img"
                                   maxlength="1" accept="pdf" style="width: 100%;">
                               </div>

                             </div>
                           </div>

                           <?php if($row3['f_name8'] == 'NULL' && $row3['f_lname8'] == 'NULL' && $row3['db8'] == 'NULL' && $row3['gender8'] == 'NULL' && $row3['re8'] == 'NULL' && $row3['db_lawf8'] == 'NULL' && $row3['lawf8'] == 'NULL'){ ?>

                           <button type="button" data-toggle="collapse" class="add__family family2"
                             data-target="#family8" aria-expanded="false"> Add Family Member </button>

                           <?php }else{} ?>

                         </div>

                         <!-- Family Member 8 -->

                         <?php if($row3['f_name8'] == 'NULL' && $row3['f_lname8'] == 'NULL' && $row3['db8'] == 'NULL' && $row3['gender8'] == 'NULL' && $row3['re8'] == 'NULL' && $row3['db_lawf8'] == 'NULL' && $row3['lawf8'] == 'NULL'){ ?>

                         <div id="family8" class="panel-collapse collapse">

                           <?php }else{ ?>
                           <br>

                           <h2 class="title__group-section member_f">Member 8</h2>

                           <div id="family8" class="panel-collapse">

                             <?php } ?>

                             <div class="row member1">
                               <div class="form__group-fname form__group-hinf3 col-lg-4">

                                 <label for="f_name8" class="label__name label__family">Name</label>
                                 <label for="f_name8" class="l_icon" title="Name"> <i class="icon icon_mname"></i>
                                 </label>

                                 <?php if(empty($row3['f_name8']) || $row3['f_name8'] == 'NULL'){ ?>

                                 <input type="text" class="form__group-input name8" id="f_name8" name="f_name8"
                                   placeholder="e.g. Melissa">

                                 <?php }else{ ?>

                                 <input type="text" class="form__group-input name8" id="f_name8" name="f_name8"
                                   placeholder="e.g. Melissa" value="<?php echo $row3['f_name8'] ?>">

                                 <?php } ?>

                               </div>

                               <div class="form__group-flname form__group-hinf3 col-lg-4">

                                 <label for="f_lname8" class="label__name label__family">Last Name</label>
                                 <label for="f_lname8" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i>
                                 </label>

                                 <?php if(empty($row3['f_lname8']) || $row3['f_lname8'] == 'NULL'){ ?>

                                 <input type="text" class="form__group-input lname8" id="f_lname8" name="f_lname8"
                                   placeholder="e.g. Smith">

                                 <?php }else{ ?>

                                 <input type="text" class="form__group-input lname8" id="f_lname8" name="f_lname8"
                                   placeholder="e.g. Smith" value="<?php echo $row3['f_lname8'] ?>">

                                 <?php } ?>

                               </div>

                               <div class="form__group-db1 form__group-hinf3 col-lg-4">

                                 <label for="db8" class="label__name label__family">Date of Birth</label>
                                 <label for="db8" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i>
                                 </label>

                                 <?php if(empty($row3['db8']) || $row3['db8'] == 'NULL'){ ?>

                                 <input type="date" class="form__group-input db8" id="db8" name="db8"
                                   placeholder="MM-DD-YYYY">

                                 <?php }else{ ?>

                                 <input type="date" class="form__group-input db8" id="db8" name="db8"
                                   placeholder="MM-DD-YYYY" value="<?php echo $row3['db8'] ?>">

                                 <?php } ?>

                               </div>

                               <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                                 <label for="gender8" class="label__name label__family">Gender</label>
                                 <label for="gender8" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i>
                                 </label>
                                 <select class="custom-select form__group-input gender8" id="gender8" name="gender8">
                                   <?php if(empty($row3['gender8']) || $row3['gender8'] == 'NULL'){ ?>

                                   <option hidden="option" selected disabled>-- Select Gender --</option>
                                   <option value="Male">Male</option>
                                   <option value="Female">Female</option>
                                   <option value="Private">Private</option>

                                   <?php }else if($row3['gender8'] == 'Male'){ ?>

                                   <option value="Male" selected>Male</option>
                                   <option value="Female">Female</option>
                                   <option value="Private">Private</option>

                                   <?php }else if($row3['gender8'] == 'Female'){ ?>

                                   <option value="Female" selected>Female</option>
                                   <option value="Male">Male</option>
                                   <option value="Private">Private</option>

                                   <?php }else if($row3['gender8'] == 'Private'){ ?>

                                   <option value="Private" selected>Private</option>
                                   <option value="Female">Female</option>
                                   <option value="Male">Male</option>

                                   <?php } ?>
                                 </select>
                               </div>

                               <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                                 <label for="re8" class="label__name label__family">Relation</label>
                                 <label for="re8" class="l_icon" title="Relation"> <i class="icon icon_family"></i>
                                 </label>
                                 <select class="custom-select form__group-input re8" id="re8" name="re8">
                                   <?php if(empty($row3['re8']) || $row3['re8'] == 'NULL'){ ?>

                                   <option hidden="option" selected disabled>-- Select Relation --</option>
                                   <option value="Dad">Dad</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Son">Son</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Dad'){ ?>

                                   <option value="Dad" selected>Dad</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Son">Son</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Mom'){ ?>

                                   <option value="Mom" selected>Mom</option>
                                   <option value="Dad">Dad</option>
                                   <option value="Son">Son</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Son'){ ?>

                                   <option value="Son" selected>Son</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Dad">Dad</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Daughter'){ ?>

                                   <option value="Daughter" selected>Daughter</option>
                                   <option value="Son">Son</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Dad">Dad</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Grandparents'){ ?>

                                   <option value="Grandparents" selected>Grandparents</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Son">Son</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Dad">Dad</option>
                                   <option value="Other">Others</option>

                                   <?php } else if($row3['re8'] == 'Other'){ ?>

                                   <option value="Other" selected>Others</option>
                                   <option value="Grandparents">Grandparents</option>
                                   <option value="Daughter">Daughter</option>
                                   <option value="Son">Son</option>
                                   <option value="Mom">Mom</option>
                                   <option value="Dad">Dad</option>

                                   <?php } ?>
                                 </select>
                               </div>

                               <div class="form__group-occupation_f8 form__group-hinf2 col-md-4">
                                 <label for="occupation_f8" class="l_icon" title="Date of Background Check"> <i
                                     class="icon icon_check"></i> </label>
                                 <label for="occupation_f8" class="label__name label__family">Occupation</label>

                                 <?php if(empty($row3['occupation_f8']) || $row3['occupation_f8'] == 'NULL'){ ?>

                                 <input type="text" class="form__group-input db_law" id="occupation_f8"
                                   name="occupation_f8" placeholder="e.g. Lawyer">

                                 <?php }else{ ?>

                                 <input type="text" class="form__group-input db_law" id="occupation_f8"
                                   name="occupation_f8" placeholder="e.g. Lawyer"
                                   value="<?php echo $row3['occupation_f8'] ?>">

                                 <?php } ?>


                               </div>

                               <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                                 <label for="db_lawf8" class="label__name label__family">Date Background Check</label>
                                 <label for="db_lawf8" class="l_icon" title="Date Background Check"> <i
                                     class="icon icon_db"></i> </label>

                                 <?php if(empty($row3['db_lawf8']) || $row3['db_lawf8'] == 'NULL'){ ?>

                                 <input type="date" class="form__group-input db_lawf8" id="db_lawf8" name="db_lawf8"
                                   placeholder="MM-DD-YYYY">

                                 <?php }else{ ?>

                                 <input type="date" class="form__group-input db_lawf8" id="db_lawf8" name="db_lawf8"
                                   placeholder="MM-DD-YYYY" value="<?php echo $row3['db_lawf8'] ?>">

                                 <?php } ?>

                               </div>

                               <div class="form__group-b-check col-lg-7">

                                 <?php if(empty($row3['lawf8']) || $row3['lawf8'] == 'NULL'){}else{ ?>
                                 <center><iframe class="embed-responsive-item mt-5 mb-0"
                                     src="../../<?php echo $row3['lawf8'] ?>"></iframe></center>
                                 <?php } ?>
                                 <div class=" form__group-hinf3 ">
                                   <label for="profile-img" class="label__name label__family">Background Check</label>
                                   <label for="profile-img" class="l_icon" title="Background Check"> <i
                                       class="icon icon_bcheck"></i> </label>
                                   <input type="file" class="form__group-input" name="image[]" id="profile-img"
                                     maxlength="1" accept="pdf" style="width: 100%;">
                                 </div>

                               </div>
                             </div>
                           </div>

                         </div>

                         <input type="text" class="form-control" id="mailpass" name="mailpass"
                           value="<?php echo $row['mail_h'] ?>" hidden>
                         <input type="text" class="form-control" id="idpass" name="idpass"
                           value="<?php echo $row['id_home'] ?>" hidden>
                         <input type="text" class="form-control" id="id_m" name="id_m" required
                           value="<?php echo "$row6[id_m]";?>" hidden>




                         <div class="form__group-save">
                           <button id="discard" name="discard" class="btn btn-primary ts-btn-arrow btn-lg"
                             style="color: white; background-color: #4f177d; border: 2px solid #4f177d;">
                             <i class="fa fa-times mr-2"></i> Discard Changes
                           </button>
                           <button id="delete" name="delete" class="btn btn-primary ts-btn-arrow btn-lg "
                             style="color: white; background-color: #982a72; border: 2px solid #982a72;">
                             <i class="fa fa-save mr-2"></i> Advance Options
                           </button>
                           <button id="certified" name="certified" class="btn btn-primary ts-btn-arrow btn-lg "
                             style="color: white; background-color: #5D418D; border: 2px solid #5D418D;">
                             <i class="fa fa-times mr-2"></i> Certified House
                           </button>
                           <button id="submit" name="update" class="btn btn-primary ts-btn-arrow btn-lg"
                             style="color: white; background-color: #232159; border: 2px solid #232159;">
                             <i class="fa fa-save mr-2"></i> Save Changes
                           </button>
                         </div>




                       </div>
     </main>

   </form>


   <input type="hidden" id="mail_hom" value="<?php echo $row['mail_h']?>">
   <input type="hidden" id="mail_user" value="<?php echo $usuario ?>">

   <?php 
    
    $payments = $link->query("SELECT * FROM payments WHERE i_mail = '$row[mail_h]' AND r_mail = '$usuario' OR i_mail = '$usuario' AND r_mail = '$row[mail_h]'");
    $rp=mysqli_num_rows($payments);

    if($rp == '0'){}else{

    ?>

   <main class="card-main" id="payments1">

     <div class="form__group-house_in form__group">
       <div class="form__target-header">
         <h3 class="title_target"> Invoice History </h3>
       </div>


       <table class="table table-hover table-striped">

         <thead>
           <tr>
             <th class="text-center align-middle">N° Invoice</th>
             <th class="text-center align-middle">Date</th>
             <th class="text-center align-middle">Price</th>
             <th class="text-center align-middle">Student</th>
             <th class="text-center align-middle">Status</th>
             <th class="text-center align-middle"></th>
           </tr>
         </thead>

         <tbody>
           <?php while($pay = mysqli_fetch_array($payments)){ 
                        
                        $id_p = strlen($pay['id_p']);

                        $student = $link->query("SELECT * FROM pe_student WHERE mail_s = '$pay[reserve_s]'");
                        $r_stu=$student->fetch_assoc();

                    ?>
           <tr>
             <form action="payment_save.php" method="POST">

               <th class="text-center align-middle text_table1">
                 <input type="hidden" name="id_p" value="<?php echo $pay['id_p'] ?>" readonly>

                 <?php if($id_p == '1'){ ?>

                 #0000<?php echo $pay['id_p'] ?>

                 <?php }else if($id_p == '2'){ ?>

                 #000<?php echo $pay['id_p'] ?>

                 <?php }else if($id_p == '3'){ ?>

                 #00<?php echo $pay['id_p'] ?>

                 <?php }else if($id_p == '4'){ ?>

                 #0<?php echo $pay['id_p'] ?>

                 <?php }else if($id_p == '5'){ ?>

                 #<?php echo $pay['id_p'] ?>

                 <?php } ?>
               </th>

               <th class="text-center align-middle text_table"><?php echo substr($pay['date_p'], 0, 10); ?></th>
               <th class="text-center align-middle text_table"><?php echo $pay['price_p'] ?></th>
               <th class="text-center align-middle text_table"><a class="link_stu"
                   href="student_info?art_id=<?= $r_stu['id_student'] ?>"><?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?>
               </th>
               <th class="text-center align-middle text_table">
                 <select class="form-control custom-select" name="status_p" id="">

                   <?php 

                                        if($pay['status_p'] == 'Payable'){ ?>

                   <option value="Payable" selected> Payable </option>
                   <option value="Budgeted"> Budgeted </option>
                   <option value="Paid"> Paid </option>


                   <?php }else if($pay['status_p'] == 'Budgeted'){ ?>

                   <option value="Budgeted" selected> Budgeted </option>
                   <option value="Payable"> Payable </option>
                   <option value="Paid"> Paid </option>

                   <?php }else if($pay['status_p'] == 'Paid'){ ?>

                   <option value="Paid" selected> Paid </option>
                   <option value="Payable"> Payable </option>
                   <option value="Budgeted"> Budgeted </option>

                   <?php } ?>
                 </select>
               </th>
               <th class="text-center align-middle text_table d-block">
                 <button type="submit" class="btn btn_change first">Save Status</button>
                 <a href="#" class="btn btn_change second">View Invoice</button>

               </th>
             </form>
           </tr>





           <?php } ?>

         </tbody>

       </table>



     </div>

   </main>

   <?php } ?>






   <div class="form__message">
     <div class="message_error aprobe" id="aprobe">
       <p class="error-m"> Register correctly</p>
     </div>

     <div class="message_error error" id="error_r">
       <p class="error-m">Please fill in the required fields correctly</p>
     </div>
   </div>

   <footer id="ts-footer">

     <?php include 'footer.php' ?>
   </footer>

   <script>
   $('#year').daterangepicker({
     locale: {
       format: 'YYYY-MM-DD'
     },
     singleDatePicker: true,
     showDropdowns: true,
     minYear: 1990,
     autoApply: true,
     maxYear: parseInt(moment().format('YYYY'), 10)
   });
   </script>





   <script src="../assets/js/jquery-3.3.1.min.js"></script>
   <script src="../assets/js/popper.min.js"></script>
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/leaflet.js"></script>
   <script src="../assets/js/jQuery.MultiFile.min.js"></script>
   <script src="../assets/js/custom.js"></script>
   <script src="../assets/js/map-leaflet.js"></script>
   <script src="assets/js/rooms.js"></script>
   <script src="assets/js/galery-homestay.js"></script>
   <script src="assets/js/rooms_edit.js"></script>
   <script src="assets/js/payments.js"></script>
   <!--Date Input Safari-->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
   <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
   <script src="assets/js/date-safari.js"></script>

   <script>
   function changePayment() {
     $("#view1").modal("show");
   }
   </script>


 </body>

 </html>