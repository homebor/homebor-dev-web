<?php
include '../../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
$row6=$query6->fetch_assoc();

$sql = "SELECT * FROM `agents` WHERE id_m = '$row6[id_m]'"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 


 if ($row5['usert'] != 'Manager') {
         header("location: ../logout.php");   
    }

if ($row6['plan'] == 'No Plan') {
         header("location: ../manager/work_with_us.php");   
    }

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/agent_profile.css">

  <!-- REFERENCE OF MAPBOX API -->
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js">
  </script>
  <link rel="stylesheet"
    href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
    type="text/css" />
  <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Coordinator Profile</title>


</head>

<body>

  <?php include 'header.php' ?>

  <div class="container_register">

    <main class="card__register">
      <form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">

        <h1 class="title__register">Register Your Coordinator <p class="required">Required Files *</p>
        </h1>


        <div class="row m-0 p-0 center">

          <div class="col-md-4 column-1">

            <div class="card">

              <h3 class="form__group-title__col-4">Basic Information</h3>

              <div class="information__content">

                <div class="form__group__div__photo">
                  <img class="form__group__photo d-none" id="preview" src="" alt="">

                  <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_c"
                    id="file" maxlength="1" accept="jpg|png|jpeg|pdf">


                  <label for="file" class="photo-add" id="label-i">
                    <p class="form__group-l_title"> Add Profile Photo </p>
                  </label>

                  <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                    title="Change Frontage Photo"></label>
                </div>

                <div class="info"></div>



                <!-- Group Name -->

                <div class="form__group form__group__name" id="group__name">
                  <div class="form__group__icon-input">
                    <label for="name" class="form__label">Name *</label>

                    <label for="name" class="icon"><i class="icon icon__names"></i></label>
                    <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>

                </div>

                <!-- Group Last Name -->

                <div class="form__group form__group__l_name" id="group__l_name">
                  <div class="form__group__icon-input">

                    <label for="l_name" class="form__label">Last Name *</label>

                    <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                    <input type="text" id="l_name" name="l_name" class="form__group-input names"
                      placeholder="e.g. Smith">

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>
                </div>


              </div>

            </div>



          </div>



          <div class="col-md-8 ">

            <div class="card column-2">

              <h3 class="form__group-title__col-4">Coordinator Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Coordinator Mail* -->
                  <div class="form__group col-md-4 form__group__basic form__group__a_mail" id="group__a_mail">
                    <div class="form__group__icon-input">

                      <label for="a_mail" class="form__label">Mail *</label>

                      <label for="a_mail" class="icon"><i class="icon icon__a_name"></i></label>
                      <input type="text" id="a_mail" name="a_mail" class="form__group-input a_mail"
                        placeholder="e.g. johnsmith@gmail.com">

                    </div>
                    <p class="form__group__input-error">Enter a valid email</p>
                  </div>

                  <!-- Coordinator Password -->
                  <div class="form__group col-md-4 form__group__basic form__group__pass" id="group__pass">
                    <div class="form__group__icon-input">

                      <label for="pass" class="form__label">Password *</label>

                      <label for="pass" class="icon"><i class="icon icon__password"></i></label>
                      <input type="password" id="pass" name="pass" class="form__group-input password"
                        placeholder="Enter your password">

                    </div>
                    <p class="form__group__input-error">Min. 4 characters</p>
                  </div>

                  <!-- Group Coordinator Contact -->
                  <div class="form__group col-md-4 form__group__basic form__group__num" id="group__num">
                    <div class="form__group__icon-input">

                      <label for="num" class="form__label">Contact Number</label>

                      <label for="num" class="icon"><i class="icon icon__num"></i></label>
                      <input type="text" id="num" name="num" class="form__group-input num" placeholder="e.g. 5557891">

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Coordinator Alternative Contact -->
                  <div class="form__group col-md-4 form__group__basic form__group__num2" id="group__num2">
                    <div class="form__group__icon-input">

                      <label for="num2" class="form__label">Alternative Contact</label>

                      <label for="num2" class="icon"><i class="icon icon__num"></i></label>
                      <input type="text" id="num2" name="num2" class="form__group-input num2"
                        placeholder="e.g. 5557891"></input>

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Years of Experience -->
                  <div class="form__group col-md-4 form__group__basic form__group__experience" id="group__experience">
                    <div class="form__group__icon-input">

                      <label for="experience" class="form__label">Years of Experience</label>

                      <label for="experience" class="icon"><i class="icon icon__exp"></i></label>
                      <input type="text" id="experience" name="experience" class="form__group-input experience"
                        placeholder="Only numbers"></input>

                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>


                  <!-- Group Language Spoken -->
                  <div class="form__group col-md-4 form__group__basic form__group__language" id="group__language">
                    <div class="form__group__icon-input">

                      <label for="language" class="form__label">Language Spoken</label>

                      <label for="language" class="icon"><i class="icon icon__city"></i></label>
                      <input type="text" id="language" name="language" class="form__group-input language"
                        placeholder="e.g. Spanish, English, etc."></input>

                    </div>
                    <p class="form__group__input-error">Write a valid Language</p>
                  </div>

                  <input type="hidden" class="form-control" id="agency" name="agency"
                    value="<?php echo "$row6[a_name]";?>">


                  <!-- Group Specialization -->
                  <div class="form__group col-md-4 form__group__basic form__group__specialization"
                    id="group__specialization">
                    <div class="form__group__icon-input">

                      <label for="specialization" class="form__label">Specialization</label>

                      <label for="specialization" class="icon"><i class="icon icon__spe"></i></label>
                      <select class="custom-select form__group-input specialization" id="specialization"
                        name="specialization">
                        <option hidden="option">-- Select Option -- </option>
                        <option value="homestay">Homestay</option>
                        <option value="students">Students</option>
                        <option value="both">Both</option>
                      </select>

                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>

                </div>

              </div>

            </div>

            <!-- Agency ID -->
            <input type="text" class="form-control" id="id_m" name="id_m" value="<?php echo "$row6[id_m]";?>" hidden>

            <input type="text" class="form-control" id="id_user" name="id_user" value="<?php echo "$row5[mail]";?>"
              hidden>

            <input type="text" class="form-control" id="plan" name="plan" value="<?php echo "$row6[plan]";?>" hidden>

            <input type="text" class="form-control" id="limit" name="limit" value="<?php echo "$numberOfRows";?>"
              hidden>




          </div>

          <div class="col-md-8">

            <div class="card">

              <h3 class="form__group-title__col-4">Location</h3>

              <div class="row pl-4 pr-4">

                <div class="col-md-6 p-0">

                  <div class="information__content">

                    <!-- Group Address -->
                    <div class="form__group form__group__dir" id="group__dir">
                      <div class="form__group__icon-input">

                        <label for="dir" class="form__label">Address </label>

                        <label for="dir" class="icon"><i class="icon icon__dir"></i></label>

                        <?php if(empty($row6['dir'] || $row6['dir'] == 'NULL')){ ?>

                        <input type="text" id="dir" name="dir" class="form__group-input dir"
                          placeholder="Av, Street, etc.">

                        <?php }else{ ?>

                        <input type="text" id="dir" name="dir" class="form__group-input dir"
                          value="<?php echo $row6['dir'] ?>">

                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Please enter a valid Address</p>
                    </div>

                    <!-- Group City -->
                    <div class="form__group form__group__city" id="group__city">
                      <div class="form__group__icon-input">

                        <label for="city" class="form__label">City </label>

                        <label for="city" class="icon"><i class="icon icon__dir"></i></label>

                        <?php if(empty($row6['city'] || $row6['city'] == 'NULL')){ ?>

                        <input type="text" id="city" name="city" class="form__group-input city2"
                          placeholder="e.g. Toronto">

                        <?php }else{ ?>

                        <input type="text" id="city" name="city" class="form__group-input city2"
                          value="<?php echo $row6['city'] ?>">

                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Please enter a valid City</p>
                    </div>

                    <!-- Group State -->
                    <div class="form__group form__group__state" id="group__state">
                      <div class="form__group__icon-input">

                        <label for="state" class="form__label">State </label>

                        <label for="state" class="icon"><i class="icon icon__dir"></i></label>

                        <?php if(empty($row6['state'] || $row6['state'] == 'NULL')){ ?>

                        <input type="text" id="state" name="state" class="form__group-input state"
                          placeholder="e.g. Ontario">

                        <?php }else{ ?>

                        <input type="text" id="state" name="state" class="form__group-input state"
                          value="<?php echo $row6['state'] ?>">

                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Please enter a valid State</p>
                    </div>

                    <!-- Group Postal Code -->
                    <div class="form__group form__group__p_code" id="group__p_code">
                      <div class="form__group__icon-input">

                        <label for="p_code" class="form__label">Postal Code </label>

                        <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>

                        <?php if(empty($row6['p_code'] || $row6['p_code'] == 'NULL')){ ?>

                        <input type="text" id="p_code" name="p_code" class="form__group-input p_code"
                          placeholder="e.g. 145187">

                        <?php }else{ ?>

                        <input type="text" id="p_code" name="p_code" class="form__group-input p_code"
                          value="<?php echo $row6['p_code'] ?>">

                        <?php } ?>

                      </div>
                      <p class="form__group__input-error">Please enter a valid Postal Code</p>
                    </div>

                  </div>

                  <div class="form-group mb-0">
                    <br>
                    <p id="latitude" hidden="p"></p>
                    <input class="form-control" name="lati" id="lati" value="" hidden="input">
                    <p id="longitude" hidden="p"></p>
                    <input class="form-control" name="longi" id="longi" value="" hidden="input">
                  </div>

                </div>

                <div class="col-md-6 p-0">

                  <div id="map"></div>

                </div>

              </div>

            </div>





          </div>

        </div><br>

        <div class="form__message" id="form__message">
          <p>llene los campos correctamente</p>
        </div>

        <div class="form__group__btn-send" align="center" id="btn__success">
          <button type="submit" class="form__btn" id="btn_">Register Coordinator</button>
        </div>

        <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>




      </form>
    </main>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>
  <div id="house_ass" style="display: none;">
    <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-9" id="assh">

        </div>
      </div>
    </div>

  </div>


  <?php include 'footer.php' ?>

  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
        document.getElementById("preview").classList.remove("d-none");
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    } else {
      document.getElementById("preview").classList.add("d-none");
    }

  }
  </script>

  <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
  <script>
  mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
  console.log(mapboxgl.accessToken);
  var client = new MapboxClient(mapboxgl.accessToken);
  console.log(client);


  var address = "<?php echo "$row6[dir], $row6[city], $row6[state] $row6[p_code]"; ?>"
  var test = client.geocodeForward(address, function(err, data, res) {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties

    console.log(res);
    console.log(res.url);
    console.log(data);

    var coordinates = data.features[0].center;

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v10',
      center: coordinates,
      zoom: 14
    });
    new mapboxgl.Marker()
      .setLngLat(coordinates)
      .addTo(map);


  });
  </script>
  <!--end col-md-6-->

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/validate__register__coord.js"></script>

</body>

</html>