// TODO IMPORTACIONES
import Notifications from "../../../controllers/notifications.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? NOTIFICATION CONTAINER
const $notificationList = D.getElementById("notification-list");

// TODO FUNCIONES Y/O CLASES
class ManagerNotifications extends Notifications {
  constructor() {
    super();
  }

  // ? GESTIONAR NOTIFICACIONES
  async manageNotifications() {
    const allNotifications = await super.getNotifications();

    if (allNotifications.notifications) {
      allNotifications.notifications.forEach((notification) => {
        this.insertNotification(notification, false);
      });
    }
    this.evaluateNewNotifications(allNotifications.newNotifications);
  }

  // ! MOSTRAR PANEL DE NOTIFICACIONES
  showPanel() {
    if (D.querySelector(".new-notifications").textContent == 0) {
      D.querySelectorAll(".new-notifications").forEach((elem) => elem.classList.remove("show"));
    }
    const bellBounding = this.bellBtn.getBoundingClientRect();

    if (this.bellBtn.dataset.show === "false") {
      this.bellBtn.dataset.show = true;

      let totalNotifications = $notificationList.querySelectorAll(".notification-item").length;
      if (totalNotifications >= 0 && totalNotifications <= 1) {
        $notificationList.dataset.totalNotifications = `You have ${totalNotifications} notification`;
        if (totalNotifications === 0) {
          if (!$notificationList.querySelector(".no-notifications")) {
            $notificationList.innerHTML += `<p class="no-notifications">You don't have any notification</p>`;
          }
        }
      } else $notificationList.dataset.totalNotifications = `You have ${totalNotifications} notifications`;

      if (W.innerWidth < 768) {
        D.body.style.overflowY = "hidden";
        $notificationList.style.left = "0";
      } else $notificationList.style.left = bellBounding.left - 400 + "px";

      this.showElement(true, $notificationList);
    } else {
      D.body.style.overflowY = "scroll";
      this.bellBtn.dataset.show = false;
      this.showElement(false, $notificationList);
    }
  }
}

// TODO INSTANCIA
const INSTANCE = new ManagerNotifications();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await INSTANCE.manageNotifications();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // * MOSTRAR PANEL DE NOTIFICACIONES
  if (e.target.id === "bell-btn") INSTANCE.showPanel();
  if (e.target.id === "mobile-bell-btn" || e.target.matches(".close-notifications")) INSTANCE.showPanel();
  // * OCULTAR PANEL DE NOTIFICACIONES
  if (
    !e.target.id.includes("bell-btn") &&
    !e.target.id.includes("notification-list") &&
    !e.target.matches("#notification-list *") &&
    $notificationList.classList.contains("d-flex")
  ) {
    INSTANCE.showElement(false, $notificationList);
    INSTANCE.bellBtn.dataset.show = "false";
  }
  // * VER NOTIFICACION
  if (e.target.matches(".notification-item")) INSTANCE.markAsRead(e.target);
});

// ! SCROLL
D.addEventListener("scroll", (e) => {
  // * OCULTAR PANEL DE NOTIFICACIONES
  if (D.querySelector("#notification-list").classList.contains("show")) INSTANCE.showPanel();
});

// ! RESIZE
W.addEventListener("resize", (e) => {
  // * OCULTAR PANEL DE NOTIFICACIONES
  if ($notificationList.classList.contains("d-flex")) {
    INSTANCE.showElement(false, $notificationList);
    INSTANCE.bellBtn.dataset.show = "false";
  }
});

// ! ERROR
