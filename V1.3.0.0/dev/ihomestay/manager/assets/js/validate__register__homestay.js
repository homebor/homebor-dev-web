const formulary = document.getElementById('form');

const inputs = document.querySelectorAll('#form input');

const expresiones = {
    // Basic Information

    name: /^[a-zA-Z0-9\_\-\s]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    phone: /^\d{2,20}$/, // 2 a 14 numeros.
    rooms: /^[1-8]{1}$/, // 1 a 14 numeros.
    dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
    city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
    p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

    // Family Information
    name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,


    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    password: /^.{4,12}$/, // 4 a 12 digitos.
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}

const campos = {
    h_name: false,
    num: false,
    room: false,
    mail:false,
    city: false,
    dir: false,
    p_code: false,
    name_h: false,
    l_name_h: false,
    cell: false
}

const validateFormulary = (e) => {
    switch (e.target.name) {
        case "h_name":
            validateField(expresiones.name, e.target, 'h_name');
            break;
        case "num":
            validateField(expresiones.phone, e.target, 'num');
            break;
        case "room":
            validateField(expresiones.rooms, e.target, 'room');
            break;

        case "mail":
            validateField(expresiones.correo, e.target, 'mail');
        break;




        case "dir":
            validateField(expresiones.dir, e.target, 'dir');
            break;
        case "city":
            validateField(expresiones.dir, e.target, 'city');
            break;
        case "state":
            validateField(expresiones.dir, e.target, 'state');
            break;
        case "p_code":
            validateField(expresiones.dir, e.target, 'p_code');
            break;

        case "name_h":
            validateField(expresiones.name_h, e.target, 'name_h');
            break;
        case "l_name_h":
            validateField(expresiones.name_h, e.target, 'l_name_h');
            break;
        case "cell":
            validateField(expresiones.phone, e.target, 'cell');
            break;
    }
}

const validateField = (expresion, input, campo) => {

    if (expresion.test(input.value)) {
        document.getElementById(`form__group-${campo}`).classList.remove('input_false');
        document.querySelector(`.form__group-${campo} .message__input_error`).classList.remove('message__input_error_active');
        campos[campo] = true;

    } else {
        document.getElementById(`form__group-${campo}`).classList.add('input_false');
        document.querySelector(`.form__group-${campo} .message__input_error`).classList.add('message__input_error_active');
        campos[campo] = false;
    }

}

inputs.forEach((input) => {
    input.addEventListener('keyup', validateFormulary);
    input.addEventListener('blur', validateFormulary);
});

formulary.addEventListener('submit', (e) => {
    e.preventDefault();

    if (campos.h_name && campos.num && campos.room) {

        var registerForm = new FormData($('#form')[0]);

        $.ajax({
            url: 'action_propertie.php',
            type: 'POST',
            data: registerForm,
            contentType: false,
            processData: false,
            success: function(response) {

                let register = JSON.parse(response);

                register.forEach(reg => {
                    if(reg.register == 'Registered'){

                        window.top.location = 'edit_propertie';

                    }else if(reg.register == 'Mail Exist'){

                        setTimeout(() => {
                            $("#house_much").fadeIn("slow");
                    
                            setTimeout(() => {
                                $("#house_much").fadeOut("slow");
                    
                            }, 5000)
                        }, 100);
                    
                        $('#close').on("click", function close(){
                            event.preventDefault();
                            $("#house_much").fadeOut("slow");
                        });
                        document.getElementById("contentp").innerHTML = "There is already a student with that email. Enter another email.";
                        document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
                        document.getElementById("contentp").style.marginTop  = "auto";
                        document.getElementById("contentp").style.marginBottom  = "auto";
                        document.getElementById("contentp").style.color = "#000";
                    
                    }else if(reg.register == 'Plan'){

                        setTimeout(() => {
                            $("#house_much").fadeIn("slow");
                    
                            setTimeout(() => {
                                $("#house_much").fadeOut("slow");
                    
                            }, 5000)
                        }, 100);
                    
                        $('#close').on("click", function close(){
                            event.preventDefault();
                            $("#house_much").fadeOut("slow");
                        });
                        document.getElementById("contentp").innerHTML = "Exceed the limit of accepted students. Change your plan to continue.";
                        document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
                        document.getElementById("contentp").style.marginTop  = "auto";
                        document.getElementById("contentp").style.marginBottom  = "auto";
                        document.getElementById("contentp").style.color = "#000";

                    }

                });

                

            }
        });

        formulary.reset();

    } else {
        setTimeout(() => {
            $("#house_much").fadeIn("slow");
    
            setTimeout(() => {
                $("#house_much").fadeOut("slow");
    
            }, 5000)
        }, 100);
    
        $('#close').on("click", function close(){
            event.preventDefault();
            $("#house_much").fadeOut("slow");
        });
        document.getElementById("contentp").innerHTML = "Please fill in the required fields correctly.";
        document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
        document.getElementById("contentp").style.marginTop  = "auto";
        document.getElementById("contentp").style.marginBottom  = "auto";
        document.getElementById("contentp").style.color = "#000";

    }
});