var room_title = `<h2 class="title__group-section" id="title__bedrooms"> Bedrooms Information </h2>`;

let divrooms = `

    <div class="row" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;">

`;
function viewBed() {
  if ($("#room").val() == "1") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
        
                                            <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>

                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
        
                                            <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>

                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
        
                                            <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>

                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          total = room_title + divrooms + firstr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "2") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>


                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          total = room_title + divrooms + firstr + secondr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "3") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number"  name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            
                            <br>
                                            
                    
                        </div>

                        `;

          total = room_title + divrooms + firstr + secondr + thirdr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "4") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";
        let fourthr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>

                        `;

          fourthr += `
                        
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom4 == "NULL") {
            fourthr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiv" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv" class="add-photo" src="../${room.proom4}">
                        
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_2 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiv-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-ii" class="add-photo" src="../${room.proom4_2}">
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_3 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>
                    
                                            <img id="preview-xiv-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom4_3}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fourthr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                                            
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Single") {
            fourthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Share") {
            fourthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Executive") {
            fourthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            fourthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            fourthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            fourthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                            
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Avalible") {
            fourthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Occupied") {
            fourthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Disabled") {
            fourthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food4 == "NULL") {
            fourthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "Yes") {
            fourthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "No") {
            fourthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">
                                                    `;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15" value="${room.aprox4}">
                                                    `;
          }
          fourthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">`;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15" value="${room.aprox_a4}">`;
          }

          fourthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>
                        
                        `;

          total = room_title + divrooms + firstr + secondr + thirdr + fourthr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "5") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";
        let fourthr = "";
        let fifthr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>

                        `;

          fourthr += `
                        
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom4 == "NULL") {
            fourthr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiv" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv" class="add-photo" src="../${room.proom4}">
                        
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_2 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiv-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xiv-ii" class="add-photo" src="../${room.proom4_2}">
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_3 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>
                    
                                            <img id="preview-xiv-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom4_3}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fourthr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                                            
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Single") {
            fourthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Share") {
            fourthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Executive") {
            fourthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Twin") {
            fourthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Double") {
            fourthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Bunker") {
            fourthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                            
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Avalible") {
            fourthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Occupied") {
            fourthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Disabled") {
            fourthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food4 == "NULL") {
            fourthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "Yes") {
            fourthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "No") {
            fourthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">
                                                    `;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15" value="${room.aprox4}">
                                                    `;
          }
          fourthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">`;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15" value="${room.aprox_a4}">`;
          }

          fourthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>
                        
                        `;

          fifthr += `
                        
                        <div class="tarjet tarjet-rooms ">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
                                </ol>

                                    <div class="carousel-inner">`;

          if (room.proom5 == "NULL") {
            fifthr += `
                                        
                                        <div class="carousel-item active">

                                        <label for="file-xv" class="photo-add" id="label-xv"> Bedroom Photo 1</label>
                                            <img id="preview-xv" class="add-photo d-none" >

                                            <input type="file" name="bed-v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none" 
                                                    >


                                            <label for="file-xv" class="add-photo-i fa fa-pencil-alt" id="label-xv-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom5}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom5_2 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> Bedroom Photo 2</label>
                                            <img id="preview-xv-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" 
                                                    >


                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-ii" class="add-photo" src="../${room.proom5_2}">

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" >

                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" title="Change Bedroom Photo"> </label>


                                        </div>
                                        `;
          }
          if (room.proom5_3 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> Bedroom Photo 3</label>
                                            <img id="preview-xv-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" 
                                                    >


                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-iii" class="add-photo" src="../${room.proom5_3}">

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" >

                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fifthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Single") {
            fifthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Share") {
            fifthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Executive") {
            fifthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Twin") {
            fifthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Double") {
            fifthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Bunker") {
            fifthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                
                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Avalible") {
            fifthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Occupied") {
            fifthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Disabled") {
            fifthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food5 == "NULL") {
            fifthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "Yes") {
            fifthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "No") {
            fifthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fifthr += `</select>
                                    
                                    </select>

                                </div>

                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15">
                                                    `;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15" value="${room.aprox5}">
                                                    `;
          }
          fifthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15">`;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15" value="${room.aprox_a5}">`;
          }

          fifthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            <br>                                                
                        </div>
    `;

          total = room_title + divrooms + firstr + secondr + thirdr + fourthr + fifthr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "6") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";
        let fourthr = "";
        let fifthr = "";
        let sixthr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>

                        `;

          fourthr += `
                        
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom4 == "NULL") {
            fourthr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiv" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv" class="add-photo" src="../${room.proom4}">
                        
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_2 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiv-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-ii" class="add-photo" src="../${room.proom4_2}">
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_3 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>
                    
                                            <img id="preview-xiv-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom4_3}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fourthr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                                            
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Single") {
            fourthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Share") {
            fourthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Executive") {
            fourthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Twin") {
            fourthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Double") {
            fourthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Bunker") {
            fourthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                            
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Avalible") {
            fourthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Occupied") {
            fourthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Disabled") {
            fourthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food4 == "NULL") {
            fourthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "Yes") {
            fourthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "No") {
            fourthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">
                                                    `;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15" value="${room.aprox4}">
                                                    `;
          }
          fourthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">`;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15" value="${room.aprox_a4}">`;
          }

          fourthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>
                        
                        `;

          fifthr += `
                        
                        <div class="tarjet tarjet-rooms ">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
                                </ol>

                                    <div class="carousel-inner">`;

          if (room.proom5 == "NULL") {
            fifthr += `
                                        
                                        <div class="carousel-item active">

                                        <label for="file-xv" class="photo-add" id="label-xv"> Bedroom Photo 1</label>
                                            <img id="preview-xv" class="add-photo d-none" >

                                            <input type="file" name="bed-v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none" 
                                                    >


                                            <label for="file-xv" class="add-photo-i fa fa-pencil-alt" id="label-xv-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom5}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom5_2 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> Bedroom Photo 2</label>
                                            <img id="preview-xv-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" 
                                                    >


                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-ii" class="add-photo" src="../${room.proom5_2}">

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" >

                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" title="Change Bedroom Photo"> </label>


                                        </div>
                                        `;
          }
          if (room.proom5_3 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> Bedroom Photo 3</label>
                                            <img id="preview-xv-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" 
                                                    >


                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-iii" class="add-photo" src="../${room.proom5_3}">

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" >

                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fifthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Single") {
            fifthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Share") {
            fifthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Executive") {
            fifthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Twin") {
            fifthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Double") {
            fifthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Bunker") {
            fifthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                
                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Avalible") {
            fifthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Occupied") {
            fifthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Disabled") {
            fifthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food5 == "NULL") {
            fifthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "Yes") {
            fifthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "No") {
            fifthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fifthr += `</select>
                                    
                                    </select>

                                </div>

                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15">
                                                    `;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15" value="${room.aprox5}">
                                                    `;
          }
          fifthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15">`;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15" value="${room.aprox_a5}">`;
          }

          fifthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            <br>                                                
                        </div>
                        `;

          sixthr += `
                        <div class="tarjet tarjet-rooms">
        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 6 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>
                                    </ol>

                                    <div class="carousel-inner">`;

          if (room.proom6 == "NULL") {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo d-none" >

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="photo-add" id="label-xvi"> Add Bedroom Photo 1 </label>

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo" src="../${room.proom6}">

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_2 == "NULL") {
            sixthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xvi-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="photo-add" id="label-xvi-ii"> Add Bedroom Photo 2 </label>

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-ii" class="add-photo" src="../${room.proom6_2}">

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_3 == "NULL") {
            sixthr += `<div class="carousel-item">

                                            <img id="preview-xvi-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="photo-add" id="label-xvi-iii"> Add Bedroom Photo 3 </label>

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-iii" class="add-photo" src="../${room.proom6_3}">

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          sixthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                                            
                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Single") {
            sixthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Share") {
            sixthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Executive") {
            sixthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Twin") {
            sixthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Double") {
            sixthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Bunker") {
            sixthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                        
                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Avalible") {
            sixthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Occupied") {
            sixthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Disabled") {
            sixthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          sixthr += `</select>
                                            
                                    </select>

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food6 == "NULL") {
            sixthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "Yes") {
            sixthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "No") {
            sixthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          sixthr += `</select>
                                            
                                    </select>

                                </div>

                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15">
                                                    `;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15" value="${room.aprox6}">
                                                    `;
          }
          sixthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15">`;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15" value="${room.aprox_a6}">`;
          }

          sixthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          total = room_title + divrooms + firstr + secondr + thirdr + fourthr + fifthr + sixthr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "7") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";
        let fourthr = "";
        let fifthr = "";
        let sixthr = "";
        let seventhr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>

                        `;

          fourthr += `
                        
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom4 == "NULL") {
            fourthr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiv" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv" class="add-photo" src="../${room.proom4}">
                        
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_2 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiv-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-ii" class="add-photo" src="../${room.proom4_2}">
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_3 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>
                    
                                            <img id="preview-xiv-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom4_3}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fourthr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                                            
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Single") {
            fourthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Share") {
            fourthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Executive") {
            fourthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Twin") {
            fourthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Double") {
            fourthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Bunker") {
            fourthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                            
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Avalible") {
            fourthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Occupied") {
            fourthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Disabled") {
            fourthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food4 == "NULL") {
            fourthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "Yes") {
            fourthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "No") {
            fourthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">
                                                    `;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15" value="${room.aprox4}">
                                                    `;
          }
          fourthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">`;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15" value="${room.aprox_a4}">`;
          }

          fourthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            
                            <br>
                                            
                    
                        </div>
                        
                        `;

          fifthr += `
                        
                        <div class="tarjet tarjet-rooms ">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
                                </ol>

                                    <div class="carousel-inner">`;

          if (room.proom5 == "NULL") {
            fifthr += `
                                        
                                        <div class="carousel-item active">

                                        <label for="file-xv" class="photo-add" id="label-xv"> Bedroom Photo 1</label>
                                            <img id="preview-xv" class="add-photo d-none" >

                                            <input type="file" name="bed-v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none" 
                                                    >


                                            <label for="file-xv" class="add-photo-i fa fa-pencil-alt" id="label-xv-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom5}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom5_2 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> Bedroom Photo 2</label>
                                            <img id="preview-xv-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" 
                                                    >


                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-ii" class="add-photo" src="../${room.proom5_2}">

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" >

                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" title="Change Bedroom Photo"> </label>


                                        </div>
                                        `;
          }
          if (room.proom5_3 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> Bedroom Photo 3</label>
                                            <img id="preview-xv-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" 
                                                    >


                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-iii" class="add-photo" src="../${room.proom5_3}">

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" >

                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fifthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Single") {
            fifthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Share") {
            fifthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Executive") {
            fifthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Twin") {
            fifthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Double") {
            fifthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Bunker") {
            fifthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                
                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Avalible") {
            fifthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Occupied") {
            fifthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Disabled") {
            fifthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food5 == "NULL") {
            fifthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "Yes") {
            fifthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "No") {
            fifthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fifthr += `</select>
                                    
                                    </select>

                                </div>

                                
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15">
                                                    `;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15" value="${room.aprox5}">
                                                    `;
          }
          fifthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15">`;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15" value="${room.aprox_a5}">`;
          }

          fifthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          sixthr += `
                        <div class="tarjet tarjet-rooms">
        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 6 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>
                                    </ol>

                                    <div class="carousel-inner">`;

          if (room.proom6 == "NULL") {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo d-none" >

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="photo-add" id="label-xvi"> Add Bedroom Photo 1 </label>

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo" src="../${room.proom6}">

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_2 == "NULL") {
            sixthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xvi-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="photo-add" id="label-xvi-ii"> Add Bedroom Photo 2 </label>

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-ii" class="add-photo" src="../${room.proom6_2}">

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_3 == "NULL") {
            sixthr += `<div class="carousel-item">

                                            <img id="preview-xvi-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="photo-add" id="label-xvi-iii"> Add Bedroom Photo 3 </label>

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-iii" class="add-photo" src="../${room.proom6_3}">

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          sixthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                                            
                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Single") {
            sixthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Share") {
            sixthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Executive") {
            sixthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Twin") {
            sixthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Double") {
            sixthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Bunker") {
            sixthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                        
                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Avalible") {
            sixthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Occupied") {
            sixthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Disabled") {
            sixthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          sixthr += `</select>
                                            

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food6 == "NULL") {
            sixthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "Yes") {
            sixthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "No") {
            sixthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          sixthr += `</select>
                                            

                                </div>

                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15">
                                                    `;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15" value="${room.aprox6}">
                                                    `;
          }
          sixthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15">`;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15" value="${room.aprox_a6}">`;
          }

          sixthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          seventhr += `
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 7 </h4>

                                <div class="div-img-r" align="center">

                                    <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li>
                                        </ol>

                                        <div class="carousel-inner">`;

          if (room.proom7 == "NULL") {
            seventhr += `
                                            <div class="carousel-item active">

                                                <img id="preview-xvii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >

                                                <label for="file-xvii" class="photo-add" id="label-xvii"> Add Bedroom Photo 1 </label>

                                                <label for="file-xvii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-i" style="display: none" title="Change Bedroom Photo">  </label>


                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xvii" class="add-photo" src="../${room.proom7}">

                                                <input type="file" name="bed-vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >

                                                <label for="file-xvii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }
          if (room.proom7_2 == "NULL") {
            seventhr += `
                                            <div class="carousel-item">

                                                <img id="preview-xvii-ii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii-ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >

                                                <label for="file-xvii-ii" class="photo-add" id="label-xvii-ii"> Add Bedroom Photo 2 </label>

                                                <label for="file-xvii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xvii-ii" class="add-photo" src="../${room.proom7_2}">

                                                <input type="file" name="bed-vii-ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >

                                                <label for="file-xvii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-ii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }
          if (room.proom7_3 == "NULL") {
            seventhr += `
                                            <div class="carousel-item">

                                                <img id="preview-xvii-iii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii-iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >

                                                <label for="file-xvii-iii" class="photo-add" id="label-xvii-iii"> Add Bedroom Photo 3 </label>

                                                <label for="file-xvii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xvii-iii" class="add-photo" src="../${room.proom7_3}">

                                                <input type="file" name="bed-vii-iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >

                                                <label for="file-xvii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-iii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }

          seventhr += `</div>

                                        <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>

                                        <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>

                                    </div>

                                            
                                </div><br>


                                <div class="row init">

                                    <div class="details in-d">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>

                                        <select name="type7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Single") {
            seventhr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Share") {
            seventhr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Executive") {
            seventhr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                    <div class="details">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                        </div>

                                        <select name="bed7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Twin">Twin</option>
                                                <option value="Double">Double</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Twin") {
            seventhr += `
                                                <option value="Twin" selected>Twin</option>
                                                <option value="Double">Double</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Double") {
            seventhr += `
                                                <option value="Double" selected>Double</option>
                                                <option value="Twin" >Twin</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Bunker") {
            seventhr += `
                                                <option value="Bunker" selected>Bunker </option>
                                                <option value="Double" >Double</option>
                                                <option value="Twin" >Twin</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                </div><br>


                                <div class="row init">
                                            
                                    <div class="details in-d">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                        </div>

                                        <select name="date7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Avalible" >Avalible</option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Avalible") {
            seventhr += `
                                                <option value="Avalible" selected>Avalible</option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Occupied") {
            seventhr += `
                                                <option value="Occupied" selected>Occupied</option>
                                                <option value="Avalible" >Avalible</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Disabled") {
            seventhr += `
                                                <option value="Disabled" selected> Disabled </option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Avalible" >Avalible</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                    <div class="details">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                        </div>

                                        <select name="food7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food7 == "NULL") {
            seventhr += `
                                                <option disabled selected> Select </option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            `;
          } else if (room.food7 == "Yes") {
            seventhr += `
                                                <option value="Yes" selected>Yes</option>
                                                <option value="No">No</option>
                                            `;
          } else if (room.food7 == "No") {
            seventhr += `
                                                <option value="No" selected>No</option>
                                                <option value="Yes">Yes</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>


                                </div>

                                <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox7 == "NULL") {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();" name="approx5" maxlength="15">
                                                    `;
          } else {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();" name="approx5" maxlength="15" value="${room.aprox7}">
                                                    `;
          }
          seventhr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a7 == "NULL") {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();" name="approx_a5" maxlength="15">`;
          } else {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();" name="approx_a5" maxlength="15" value="${room.aprox_a7}">`;
          }

          seventhr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                                
                                <br>                                                
                            </div>
                        `;

          total = room_title + divrooms + firstr + secondr + thirdr + fourthr + fifthr + sixthr + seventhr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  } else if ($("#room").val() == "8") {
    $.ajax({
      url: "rooms__edit.php",
      type: "POST",
      data: $("#idpass").serialize(),
      success: function (response) {
        let rooms = JSON.parse(response);
        let firstr = "";
        let secondr = "";
        let thirdr = "";
        let fourthr = "";
        let fifthr = "";
        let sixthr = "";
        let seventhr = "";
        let eighthr = "";

        total = room_title + divrooms;

        rooms.forEach((room) => {
          firstr += `
        
                        <!-- Room 1 -->
                        <div class="tarjet tarjet-rooms " id="room1">
                        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 1 </h4>
        
                            <div class="div-img-r" align="center">
        
                                <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
        
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li>
                                    </ol>
        
                                    <div class="carousel-inner">`;

          if (room.proom1 == "NULL") {
            firstr += `<div class="carousel-item active">
                                        <label for="file-xi" class="photo-add" id="label-xi"> Bedroom Photo 1</label>
        
                                            <img id="preview-xi" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item active">
        
                                            <img id="preview-xi" class="add-photo" src="../${room.proom1}">
        
                                            <input type="file" name="bed-i" id="file-xi" accept="image/*" onchange="previewImage_xi();" style="display: none">
        
                                            <label for="file-xi" class="add-photo-i fa fa-pencil-alt" id="label-xi-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_2 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-ii" class="photo-add" id="label-xi-ii"> Bedroom Photo 2</label>
        
                                            <img id="preview-xi-ii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i" style="display: none"> </label>
        
                                        </div> `;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-ii" class="add-photo" src="../${room.proom1_2}">
            
                                            <input type="file" name="bed-i-ii" id="file-xi-ii" accept="image/*" onchange="previewImage_xi_ii();" style="display: none">
        
                                            <label for="file-xi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xi-ii-i"> </label>
        
                                        </div>`;
          }
          if (room.proom1_3 == "NULL") {
            firstr += `<div class="carousel-item">
                                        <label for="file-xi-iii" class="photo-add" id="label-xi-iii"> Bedroom Photo 3</label>
        
                                            <img id="preview-xi-iii" class="add-photo d-none" >
        
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none">
        
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" style="display: none"> </label>
        
                                        </div>`;
          } else {
            firstr += `<div class="carousel-item">

                                            <img id="preview-xi-iii" class="add-photo" src="../${room.proom1_3}">
            
                                            <input type="file" name="bed-i-iii" id="file-xi-iii" accept="image/*" onchange="previewImage_xi_iii();" style="display: none" >
        
                                            <label for="file-xi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xi-iii-i" > </label>
                                        </div>`;
          }

          firstr += `</div>
        
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
        
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
        
                                    <select name="type1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type1 == "NULL") {
            firstr += `<option disabled selected hidden="option"> Select</option>
                                        <option value="Single">Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Single") {
            firstr += `
                                        <option value="Single" selected>Single</option>
                                        <option value="Share">Share</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Share") {
            firstr += `
                                        <option value="Share" selected>Share</option>
                                        <option value="Single">Single</option>
                                        <option value="Executive">Executive</option>`;
          } else if (room.type1 == "Executive") {
            firstr += `
                                        <option value="Executive" selected>Executive</option>
                                        <option value="Share" >Share</option>
                                        <option value="Single">Single</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img"  src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
        
                                    <select name="bed1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Twin") {
            firstr += `
                                        <option value="Twin" selected>Twin</option>
                                        <option value="Double">Double</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Double") {
            firstr += `
                                        <option value="Double" selected>Double</option>
                                        <option value="Twin">Twin</option>
                                        <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed1 == "Bunker") {
            firstr += `
                                        <option value="Bunker" selected>Bunker </option>
                                        <option value="Double">Double</option>
                                        <option value="Twin">Twin</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                            </div><br>
        
        
                            <div class="row init">
        
                                <div class="details in-d">
        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
        
                                    <select name="date1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date1 == "NULL") {
            firstr += `
                                            <option disabled selected hidden="option"> Select</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Avalible") {
            firstr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Occupied") {
            firstr += `
                                            <option value="Occupied" selected>Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" >Disabled</option>
                                        `;
          } else if (room.date1 == "Disabled") {
            firstr += `
                                            <option value="Disabled" selected>Disabled</option>
                                            <option value="Occupied" >Occupied</option>                 
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
                                <div class="details">
                                                        
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
        
                                    <select name="food1" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food1 == "NULL") {
            firstr += `
                                        <option disabled selected hidden="option"> Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                        `;
          } else if (room.food1 == "Yes") {
            firstr += `
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                        `;
          } else if (room.food1 == "No") {
            firstr += `
                                        <option value="No" selected>No</option>
                                        <option value="Yes">Yes</option>
                                        `;
          }

          firstr += `</select>
        
                                </div>
        
        
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15">
                                                    `;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction();" name="approx1" maxlength="15" value="${room.aprox1}">
                                                    `;
          }
          firstr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a1 == "NULL") {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15">`;
          } else {
            firstr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction1_2();" name="approx_a1" maxlength="15" value="${room.aprox_a1}">`;
          }

          firstr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
        
        
                        </div>`;

          secondr += `
                        
                        <!-- Room2 -->

                        <div class="tarjet tarjet-rooms">
                         
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 2 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-2" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-2" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom2 == "NULL") {
            secondr += `
                                        <div class="carousel-item active">
                                        <label for="file-xii" class="photo-add" id="label-xii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xii" class="add-photo" src="../${room.proom2}">
                        
                                            <input type="file" name="bed-ii" id="file-xii" accept="image/*" onchange="previewImage_xii();" style="display: none">
                    
                                            <label for="file-xii" class="add-photo-i fa fa-pencil-alt" id="label-xii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_2 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-ii" class="photo-add" id="label-xii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-ii" class="add-photo" src="../${room.proom2_2}">
                        
                                            <input type="file" name="bed-ii-ii" id="file-xii-ii" accept="image/*" onchange="previewImage_xii_ii();" style="display: none">
                    
                                            <label for="file-xii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom2_3 == "NULL") {
            secondr += `<div class="carousel-item">
                                        <label for="file-xii-iii" class="photo-add" id="label-xii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>
                                        `;
          } else {
            secondr += `
                                        <div class="carousel-item">

                                            <img id="preview-xii-iii" class="add-photo" src="../${room.proom2_3}">
                        
                                            <input type="file" name="bed-ii-iii" id="file-xii-iii" accept="image/*" onchange="previewImage_xii_iii();" style="display: none">
                    
                                            <label for="file-xii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xii-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          secondr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Single") {
            secondr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Share") {
            secondr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type2 == "Executive") {
            secondr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed2" class="fo custom-select" id="color" style="padding: 5px;padding-top: 7px">`;

          if (room.bed2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Twin") {
            secondr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Double") {
            secondr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed2 == "Bunker") {
            secondr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date2 == "NULL") {
            secondr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Avalible") {
            secondr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Occupied") {
            secondr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date2 == "Disabled") {
            secondr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food2" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food2 == "NULL") {
            secondr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "Yes") {
            secondr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food2 == "No") {
            secondr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          secondr += `</select>
                    
                                </div>
            
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15">
                                                    `;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction2();" name="approx2" maxlength="15" value="${room.aprox2}">
                                                    `;
          }
          secondr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a2 == "NULL") {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15">`;
          } else {
            secondr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction2_2();" name="approx_a2" maxlength="15" value="${room.aprox_a2}">`;
          }

          secondr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                   
                    
                        </div>
                        
                        `;

          thirdr += `
                        
                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 3 </h4>
                    
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-3" class="carousel slide" data-ride="carousel">
                    
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-3" data-slide-to="2"></li>
                                </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiii" class="photo-add" id="label-xiii"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiii" class="add-photo" src="../${room.proom3}">
                    
                                            <input type="file" name="bed-iii" id="file-xiii" accept="image/*" onchange="previewImage_xiii();" style="display: none">
                    
                                            <label for="file-xiii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom3_2 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-ii" class="photo-add" id="label-xiii-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiii-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-ii" class="add-photo" src="../${room.proom3_2}">
                        
                                            <input type="file" name="bed-iii-ii" id="file-xiii-ii" accept="image/*" onchange="previewImage_xiii_ii();" style="display: none" >
                    
                                            <label for="file-xiii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-ii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom3_3 == "NULL") {
            thirdr += `
                                        <div class="carousel-item">
                                        <label for="file-xiii-iii" class="photo-add" id="label-xiii-iii"> Bedroom Photo 3 </label>
                    
                                            <img id="preview-xiii-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            thirdr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiii-iii" class="add-photo" src="../${room.proom3_3}">
                    
                                            <input type="file" name="bed-iii-iii" id="file-xiii-iii" accept="image/*" onchange="previewImage_xiii_iii();" style="display: none" >
                    
                                            <label for="file-xiii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiii-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          thirdr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-3" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-3" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                    </div>
                    
                                                
                                </div><br>
                    
                    
                                <div class="row init">
                    
                                    <div class="details in-d">
                                                    
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>
                    
                                        <select name="type3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type3 == "NULL") {
            thirdr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Single") {
            thirdr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Share") {
            thirdr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type3 == "Executive") {
            thirdr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Twin") {
            thirdr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Double") {
            thirdr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed3 == "Bunker") {
            thirdr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date3 == "NULL") {
            thirdr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Avalible") {
            thirdr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Occupied") {
            thirdr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date3 == "Disabled") {
            thirdr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food3" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food3 == "NULL") {
            thirdr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "Yes") {
            thirdr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food3 == "No") {
            thirdr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          thirdr += `</select>
                    
                                </div>
                    
                                
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15">
                                                    `;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction3();" name="approx3" maxlength="15" value="${room.aprox3}">
                                                    `;
          }
          thirdr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a3 == "NULL") {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15">`;
          } else {
            thirdr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction3_2();" name="approx_a3" maxlength="15" value="${room.aprox_a3}">`;
          }

          thirdr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>

                        `;

          fourthr += `
                        
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 4 </h4>
                    
                            <div class="div-img-r" align="center">
                    
                                <div id="carouselExampleIndicators-4" class="carousel slide" data-ride="carousel">
                    
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-4" data-slide-to="2"></li>
                                    </ol>
                    
                                    <div class="carousel-inner">`;

          if (room.proom4 == "NULL") {
            fourthr += `
                                        <div class="carousel-item active">
                                        <label for="file-xiv" class="photo-add" id="label-xiv"> Bedroom Photo 1 </label>
                    
                                            <img id="preview-xiv" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv" class="add-photo" src="../${room.proom4}">
                        
                                            <input type="file" name="bed-iv" id="file-xiv" accept="image/*" onchange="previewImage_xiv();" style="display: none" >
                    
                                            <label for="file-xiv" class="add-photo-i fa fa-pencil-alt" id="label-xiv-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_2 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-ii" class="photo-add" id="label-xiv-ii"> Bedroom Photo 2 </label>
                    
                                            <img id="preview-xiv-ii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" style="display: none" title="Change Bedroom Photo">  </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-ii" class="add-photo" src="../${room.proom4_2}">
                    
                                            <input type="file" name="bed-iv-ii" id="file-xiv-ii" accept="image/*" onchange="previewImage_xiv_ii();" style="display: none">
                    
                                            <label for="file-xiv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom4_3 == "NULL") {
            fourthr += `
                                        <div class="carousel-item">
                                        <label for="file-xiv-iii" class="photo-add" id="label-xiv-iii"> Bedroom Photo 3</label>
                    
                                            <img id="preview-xiv-iii" class="add-photo d-none" >
                    
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>
                    
                                        </div>`;
          } else {
            fourthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom4_3}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fourthr += `</div>
                    
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                    
                                    <a class="carousel-control-next" href="#carouselExampleIndicators-4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                    
                                </div>
                    
                                            
                            </div><br>
                    
                    
                            <div class="row init">
                    
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>
                    
                                    <select name="type4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Single") {
            fourthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Share") {
            fourthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type4 == "Executive") {
            fourthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>
                    
                                    <select name="bed4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Twin") {
            fourthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Double") {
            fourthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed4 == "Bunker") {
            fourthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                            </div><br>
                    
                    
                            <div class="row init">
                                            
                                <div class="details in-d">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>
                    
                                    <select name="date4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date4 == "NULL") {
            fourthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Avalible") {
            fourthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Occupied") {
            fourthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date4 == "Disabled") {
            fourthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                                <div class="details">
                                                
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>
                    
                                    <select name="food4" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food4 == "NULL") {
            fourthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "Yes") {
            fourthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food4 == "No") {
            fourthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fourthr += `</select>
                    
                                </div>
                    
                    
                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15">
                                                    `;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction4();" name="approx4" maxlength="15" value="${room.aprox4}">
                                                    `;
          }
          fourthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a4 == "NULL") {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15">`;
          } else {
            fourthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction4_2();" name="approx_a4" maxlength="15" value="${room.aprox_a4}">`;
          }

          fourthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>
                                            
                    
                        </div>
                        
                        `;

          fifthr += `
                        
                        <div class="tarjet tarjet-rooms ">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 5 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-5" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="0" class="active"> </li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators-5" data-slide-to="2"></li>
                                </ol>

                                    <div class="carousel-inner">`;

          if (room.proom5 == "NULL") {
            fifthr += `
                                        
                                        <div class="carousel-item active">

                                        <label for="file-xv" class="photo-add" id="label-xv"> Bedroom Photo 1</label>
                                            <img id="preview-xv" class="add-photo d-none" >

                                            <input type="file" name="bed-v" id="file-xv" accept="image/*" onchange="previewImage_xv();" style="display: none" 
                                                    >


                                            <label for="file-xv" class="add-photo-i fa fa-pencil-alt" id="label-xv-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xiv-iii" class="add-photo" src="../${room.proom5}">
                        
                                            <input type="file" name="bed-iv-iii" id="file-xiv-iii" accept="image/*" onchange="previewImage_xiv_iii();" style="display: none" >
                    
                                            <label for="file-xiv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xiv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }
          if (room.proom5_2 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-ii" class="photo-add" id="label-xv-ii"> Bedroom Photo 2</label>
                                            <img id="preview-xv-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" 
                                                    >


                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-ii" class="add-photo" src="../${room.proom5_2}">

                                            <input type="file" name="bed-v-ii" id="file-xv-ii" accept="image/*" onchange="previewImage_xv_ii();" style="display: none" >

                                            <label for="file-xv-ii" class="add-photo-i fa fa-pencil-alt" id="label-xv-ii-i" title="Change Bedroom Photo"> </label>


                                        </div>
                                        `;
          }
          if (room.proom5_3 == "NULL") {
            fifthr += `
                                        <div class="carousel-item">

                                        <label for="file-xv-iii" class="photo-add" id="label-xv-iii"> Bedroom Photo 3</label>
                                            <img id="preview-xv-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" 
                                                    >


                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" style="display: none" title="Change Bedroom Photo"> </label>

                                        </div>`;
          } else {
            fifthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xv-iii" class="add-photo" src="../${room.proom5_3}">

                                            <input type="file" name="bed-v-iii" id="file-xv-iii" accept="image/*" onchange="previewImage_xv_iii();" style="display: none" >

                                            <label for="file-xv-iii" class="add-photo-i fa fa-pencil-alt" id="label-xv-iii-i" title="Change Bedroom Photo"> </label>

                                        </div>
                                        `;
          }

          fifthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-5" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-5" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Single") {
            fifthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Share") {
            fifthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type5 == "Executive") {
            fifthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Twin") {
            fifthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Double") {
            fifthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed5 == "Bunker") {
            fifthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                
                                <div class="details in-d">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date5 == "NULL") {
            fifthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Avalible") {
            fifthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Occupied") {
            fifthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date5 == "Disabled") {
            fifthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          fifthr += `</select>

                                </div>

                                <div class="details">
                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food5" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food5 == "NULL") {
            fifthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "Yes") {
            fifthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food5 == "No") {
            fifthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          fifthr += `</select>
                                    
                                    </select>

                                </div>


                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15">
                                                    `;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction5();" name="approx5" maxlength="15" value="${room.aprox5}">
                                                    `;
          }
          fifthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a5 == "NULL") {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15">`;
          } else {
            fifthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction5_2();" name="approx_a5" maxlength="15" value="${room.aprox_a5}">`;
          }

          fifthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          sixthr += `
                        <div class="tarjet tarjet-rooms">
        
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 6 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-6" class="carousel slide" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-6" data-slide-to="2"></li>
                                    </ol>

                                    <div class="carousel-inner">`;

          if (room.proom6 == "NULL") {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo d-none" >

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="photo-add" id="label-xvi"> Add Bedroom Photo 1 </label>

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" style="display: none" title="Change Bedroom Photo">  </label>


                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xvi" class="add-photo" src="../${room.proom6}">

                                            <input type="file" name="bed-vi" id="file-xvi" accept="image/*" onchange="previewImage_xvi();" style="display: none" >

                                            <label for="file-xvi" class="add-photo-i fa fa-pencil-alt" id="label-xvi-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_2 == "NULL") {
            sixthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xvi-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="photo-add" id="label-xvi-ii"> Add Bedroom Photo 2 </label>

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-ii" class="add-photo" src="../${room.proom6_2}">

                                            <input type="file" name="bed-vi-ii" id="file-xvi-ii" accept="image/*" onchange="previewImage_xvi_ii();" style="display: none" >

                                            <label for="file-xvi-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-ii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }
          if (room.proom6_3 == "NULL") {
            sixthr += `<div class="carousel-item">

                                            <img id="preview-xvi-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="photo-add" id="label-xvi-iii"> Add Bedroom Photo 3 </label>

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            sixthr += `
                                        <div class="carousel-item ">

                                            <img id="preview-xvi-iii" class="add-photo" src="../${room.proom6_3}">

                                            <input type="file" name="bed-vi-iii" id="file-xvi-iii" accept="image/*" onchange="previewImage_xvi_iii();" style="display: none" >

                                            <label for="file-xvi-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvi-iii-i" title="Change Bedroom Photo">  </label>

                                        </div>
                                        `;
          }

          sixthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-6" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-6" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                                            
                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Single") {
            sixthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Share") {
            sixthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type6 == "Executive") {
            sixthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Twin") {
            sixthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Double") {
            sixthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed6 == "Bunker") {
            sixthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          sixthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                        
                                <div class="details in-d">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date6 == "NULL") {
            sixthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Avalible") {
            sixthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Occupied") {
            sixthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date6 == "Disabled") {
            sixthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          sixthr += `</select>
                                            

                                </div>

                                <div class="details">
                                            
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food6" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food6 == "NULL") {
            sixthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "Yes") {
            sixthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food6 == "No") {
            sixthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          sixthr += `</select>
                                            

                                </div>


                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15">
                                                    `;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction6();" name="approx5" maxlength="15" value="${room.aprox6}">
                                                    `;
          }
          sixthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a6 == "NULL") {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15">`;
          } else {
            sixthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction6_2();" name="approx_a5" maxlength="15" value="${room.aprox_a6}">`;
          }

          sixthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          seventhr += `
                        <div class="tarjet tarjet-rooms">
            
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 7 </h4>

                                <div class="div-img-r" align="center">

                                    <div id="carouselExampleIndicators-7" class="carousel slide" data-ride="carousel">

                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="0" class="active"> </li>
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators-7" data-slide-to="2"></li>
                                        </ol>

                                        <div class="carousel-inner">`;

          if (room.proom7 == "NULL") {
            seventhr += `
                                            <div class="carousel-item active">

                                                <img id="preview-xvii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >

                                                <label for="file-xvii" class="photo-add" id="label-xvii"> Add Bedroom Photo 1 </label>

                                                <label for="file-xvii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-i" style="display: none" title="Change Bedroom Photo">  </label>


                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item active">

                                                <img id="preview-xvii" class="add-photo" src="../${room.proom7}">

                                                <input type="file" name="bed-vii" id="file-xvii" accept="image/*" onchange="previewImage_xvii();" style="display: none" >

                                                <label for="file-xvii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }
          if (room.proom7_2 == "NULL") {
            seventhr += `
                                            <div class="carousel-item">

                                                <img id="preview-xvii-ii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii-ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >

                                                <label for="file-xvii-ii" class="photo-add" id="label-xvii-ii"> Add Bedroom Photo 2 </label>

                                                <label for="file-xvii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xvii-ii" class="add-photo" src="../${room.proom7_2}">

                                                <input type="file" name="bed-vii-ii" id="file-xvii-ii" accept="image/*" onchange="previewImage_xvii_ii();" style="display: none" >

                                                <label for="file-xvii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-ii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }
          if (room.proom7_3 == "NULL") {
            seventhr += `
                                            <div class="carousel-item">

                                                <img id="preview-xvii-iii" class="add-photo d-none" >

                                                <input type="file" name="bed-vii-iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >

                                                <label for="file-xvii-iii" class="photo-add" id="label-xvii-iii"> Add Bedroom Photo 3 </label>

                                                <label for="file-xvii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                            </div>`;
          } else {
            seventhr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xvii-iii" class="add-photo" src="../${room.proom7_3}">

                                                <input type="file" name="bed-vii-iii" id="file-xvii-iii" accept="image/*" onchange="previewImage_xvii_iii();" style="display: none" >

                                                <label for="file-xvii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xvii-iii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                            `;
          }

          seventhr += `</div>

                                        <a class="carousel-control-prev" href="#carouselExampleIndicators-7" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>

                                        <a class="carousel-control-next" href="#carouselExampleIndicators-7" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>

                                    </div>

                                            
                                </div><br>


                                <div class="row init">

                                    <div class="details in-d">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                        </div>

                                        <select name="type7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Single">Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Single") {
            seventhr += `
                                                <option value="Single" selected>Single</option>
                                                <option value="Share">Share</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Share") {
            seventhr += `
                                                <option value="Share" selected>Share</option>
                                                <option value="Single" >Single</option>
                                                <option value="Executive">Executive</option>
                                            `;
          } else if (room.type7 == "Executive") {
            seventhr += `
                                                <option value="Executive" selected>Executive</option>
                                                <option value="Share" >Share</option>
                                                <option value="Single" >Single</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                    <div class="details">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                        </div>

                                        <select name="bed7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Twin">Twin</option>
                                                <option value="Double">Double</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Twin") {
            seventhr += `
                                                <option value="Twin" selected>Twin</option>
                                                <option value="Double">Double</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Double") {
            seventhr += `
                                                <option value="Double" selected>Double</option>
                                                <option value="Twin" >Twin</option>
                                                <option value="Bunker">Bunker </option>
                                            `;
          } else if (room.bed7 == "Bunker") {
            seventhr += `
                                                <option value="Bunker" selected>Bunker </option>
                                                <option value="Double" >Double</option>
                                                <option value="Twin" >Twin</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                </div><br>


                                <div class="row init">
                                            
                                    <div class="details in-d">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                        </div>

                                        <select name="date7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date7 == "NULL") {
            seventhr += `
                                                <option disabled selected hidden="option"> Select </option>
                                                <option value="Avalible" >Avalible</option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Avalible") {
            seventhr += `
                                                <option value="Avalible" selected>Avalible</option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Occupied") {
            seventhr += `
                                                <option value="Occupied" selected>Occupied</option>
                                                <option value="Avalible" >Avalible</option>
                                                <option value="Disabled" > Disabled </option>
                                            `;
          } else if (room.date7 == "Disabled") {
            seventhr += `
                                                <option value="Disabled" selected> Disabled </option>
                                                <option value="Occupied" >Occupied</option>
                                                <option value="Avalible" >Avalible</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                    <div class="details">
                                                
                                        <div class="icon-type">
                                            <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                        </div>

                                        <select name="food7" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food7 == "NULL") {
            seventhr += `
                                                <option disabled selected> Select </option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            `;
          } else if (room.food7 == "Yes") {
            seventhr += `
                                                <option value="Yes" selected>Yes</option>
                                                <option value="No">No</option>
                                            `;
          } else if (room.food7 == "No") {
            seventhr += `
                                                <option value="No" selected>No</option>
                                                <option value="Yes">Yes</option>
                                            `;
          }

          seventhr += `</select>

                                    </div>

                                </div>

                                <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox7 == "NULL") {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();" name="approx5" maxlength="15">
                                                    `;
          } else {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction7();" name="approx5" maxlength="15" value="${room.aprox7}">
                                                    `;
          }
          seventhr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a7 == "NULL") {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();" name="approx_a5" maxlength="15">`;
          } else {
            seventhr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction7_2();" name="approx_a5" maxlength="15" value="${room.aprox_a7}">`;
          }

          seventhr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                                <br>                                                
                            </div>
                        `;

          eighthr += `

                        <div class="tarjet tarjet-rooms">
                
                            <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Room 8 </h4>

                            <div class="div-img-r" align="center">

                                <div id="carouselExampleIndicators-8" class="carousel slide" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators-8" data-slide-to="0" class="active"> </li>
                                        <li data-target="#carouselExampleIndicators-8" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators-8" data-slide-to="2"></li>
                                    </ol>

                                    <div class="carousel-inner">`;

          if (room.proom8 == "NULL") {
            eighthr += `
                                        <div class="carousel-item active">

                                            <img id="preview-xviii" class="add-photo d-none" >

                                            <input type="file" name="bed-viii" id="file-xviii" accept="image/*" onchange="previewImage_xviii();" style="display: none" >

                                            <label for="file-xviii" class="photo-add" id="label-xviii"> Add Bedroom Photo 1</label>

                                            <label for="file-xviii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            eighthr += `
                                            <div class="carousel-item active">

                                                <img id="preview-xviii" class="add-photo" src="../${room.proom8}">

                                                <input type="file" name="bed-viii" id="file-xviii" accept="image/*" onchange="previewImage_xviii();" style="display: none" >

                                                <label for="file-xviii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                        `;
          }
          if (room.proom8_2 == "NULL") {
            eighthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xviii-ii" class="add-photo d-none" >

                                            <input type="file" name="bed-viii-ii" id="file-xviii-ii" accept="image/*" onchange="previewImage_xviii_ii();" style="display: none" >

                                            <label for="file-xviii-ii" class="photo-add" id="label-xviii-ii"> Add Bedroom Photo 2 </label>

                                            <label for="file-xviii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-ii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            eighthr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xviii-ii" class="add-photo" src="../${room.proom8_2}">

                                                <input type="file" name="bed-viii-ii" id="file-xviii-ii" accept="image/*" onchange="previewImage_xviii_ii();" style="display: none" >

                                                <label for="file-xviii-ii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-ii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                        `;
          }
          if (room.proom8_3 == "NULL") {
            eighthr += `
                                        <div class="carousel-item">

                                            <img id="preview-xviii-iii" class="add-photo d-none" >

                                            <input type="file" name="bed-viii-iii" id="file-xviii-iii" accept="image/*" onchange="previewImage_xviii_iii();" style="display: none" >

                                            <label for="file-xviii-iii" class="photo-add" id="label-xviii-iii"> Add Bedroom Photo 3 </label>

                                            <label for="file-xviii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-iii-i" style="display: none" title="Change Bedroom Photo">  </label>

                                        </div>`;
          } else {
            eighthr += `
                                            <div class="carousel-item ">

                                                <img id="preview-xviii-iii" class="add-photo" src="../${room.proom8_3}">

                                                <input type="file" name="bed-viii-iii" id="file-xviii-iii" accept="image/*" onchange="previewImage_xviii_iii();" style="display: none" >

                                                <label for="file-xviii-iii" class="add-photo-i fa fa-pencil-alt" id="label-xviii-iii-i" title="Change Bedroom Photo">  </label>

                                            </div>
                                        `;
          }

          eighthr += `</div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators-8" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>

                                    <a class="carousel-control-next" href="#carouselExampleIndicators-8" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                                            
                            </div><br>


                            <div class="row init">

                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/acomodacion 64.png" alt="" title="Type Accomodation">
                                    </div>

                                    <select name="type8" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.type8 == "NULL") {
            eighthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Single">Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type8 == "Single") {
            eighthr += `
                                            <option value="Single" selected>Single</option>
                                            <option value="Share">Share</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type8 == "Share") {
            eighthr += `
                                            <option value="Share" selected>Share</option>
                                            <option value="Single" >Single</option>
                                            <option value="Executive">Executive</option>
                                        `;
          } else if (room.type8 == "Executive") {
            eighthr += `
                                            <option value="Executive" selected>Executive</option>
                                            <option value="Share" >Share</option>
                                            <option value="Single" >Single</option>
                                        `;
          }

          eighthr += `</select>

                                </div>

                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/cama 64.png" alt="" title="Type of Bed">
                                    </div>

                                    <select name="bed8" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.bed8 == "NULL") {
            eighthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Twin">Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed8 == "Twin") {
            eighthr += `
                                            <option value="Twin" selected>Twin</option>
                                            <option value="Double">Double</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed8 == "Double") {
            eighthr += `
                                            <option value="Double" selected>Double</option>
                                            <option value="Twin" >Twin</option>
                                            <option value="Bunker">Bunker </option>
                                        `;
          } else if (room.bed8 == "Bunker") {
            eighthr += `
                                            <option value="Bunker" selected>Bunker </option>
                                            <option value="Double" >Double</option>
                                            <option value="Twin" >Twin</option>
                                        `;
          }

          eighthr += `</select>

                                </div>

                            </div><br>


                            <div class="row init">
                                                
                                <div class="details in-d">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/availability.png" alt="" title="Availability">
                                    </div>

                                    <select name="date8" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.date8 == "NULL") {
            eighthr += `
                                            <option disabled selected hidden="option"> Select </option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date8 == "Avalible") {
            eighthr += `
                                            <option value="Avalible" selected>Avalible</option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date8 == "Occupied") {
            eighthr += `
                                            <option value="Occupied" selected>Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                            <option value="Disabled" > Disabled </option>
                                        `;
          } else if (room.date8 == "Disabled") {
            eighthr += `
                                            <option value="Disabled" selected> Disabled </option>
                                            <option value="Occupied" >Occupied</option>
                                            <option value="Avalible" >Avalible</option>
                                        `;
          }

          eighthr += `</select>

                                </div>

                                <div class="details">
                                                    
                                    <div class="icon-type">
                                        <img class="type-img" src="../homestay/assets/icon/food 64.png" alt="" title="Food Service">
                                    </div>

                                    <select name="food8" class="fo custom-select" id="color" style="padding: 5px; padding-top: 7px">`;

          if (room.food8 == "NULL") {
            eighthr += `
                                            <option disabled selected> Select </option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food8 == "Yes") {
            eighthr += `
                                            <option value="Yes" selected>Yes</option>
                                            <option value="No">No</option>
                                        `;
          } else if (room.food8 == "No") {
            eighthr += `
                                            <option value="No" selected>No</option>
                                            <option value="Yes">Yes</option>
                                        `;
          }

          eighthr += `</select>
                                                    
                                    </select>

                                </div>

                            </div>

                            <div class="row p-0" style=" margin-top: 10%; justify-content: center">
        
                                <div class="container m-0" style="width: 95%;">
                                                            
                                    <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Weekly Price CAD$</p>

                                    <div class="content__aprox">
                                        
                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Homestay</p>`;

          if (room.aprox8 == "NULL") {
            eighthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction8();" name="approx5" maxlength="15">
                                                    `;
          } else {
            eighthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number" onkeyup="myFunction8();" name="approx5" maxlength="15" value="${room.aprox8}">
                                                    `;
          }
          eighthr += `

                                        </div>

                                        <div class="col-lg-6">
                                            
                                            <p class="m-0 mb-2" style="font-weight: normal; color: #4f177d; text-align: center"> Provider </p>`;

          if (room.aprox_a8 == "NULL") {
            eighthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction8_2();" name="approx_a5" maxlength="15">`;
          } else {
            eighthr += `
                                                    <input type="text" class="form-control input__tex sum" id="number1_2" onkeyup="myFunction8_2();" name="approx_a5" maxlength="15" value="${room.aprox_a8}">`;
          }

          eighthr += `
                                        </div>

                                    </div>
                                        
                                        
                                </div>
        
                            </div>
                            <br>                                                
                        </div>
                        `;

          total = room_title + divrooms + firstr + secondr + thirdr + fourthr + fifthr + sixthr + seventhr + eighthr;

          $(".bedrooms").html(total);
        });
      },
    });

    total += `
        </div>`;
  }

  $(".bedrooms").html(total);
}

viewBed();
