// TODO VARIABLES
const D = document;
const W = window;
const $bellBtn = D.getElementById("bell-btn");
const $newNotifications = D.getElementById("new-notifications");
// ? NOTIFICATION TEMPLATE
const $notificationTemplate = D.getElementById("notification-template").content.cloneNode(true);
// ? NOTIFICATION LIST CONTAINER
const $notificationList = D.getElementById("notification-list");

// TODO FUNCIONES Y/O CLASES
export default class Notifications {
  constructor() {
    //  * NOTIFICATION BELL BUTTON
    this.bellBtn = $bellBtn;
    // * NUM OF NEW NOTIFICATIONS
    this.newNotifications = $newNotifications;

    // * ULTIMA NOTIFICACIÓN
    this.lastJson = 0;
    // * ULTIMA NOTIFICACIÓN DEL SISTEMA
    this.lastSystemNotification = 0;
    // * LOCAL STORAGE
    this.myLocalStorage = W.localStorage;
    this.mySessionStorage = W.sessionStorage;

    // * NOTIFICATIONS WITH BUTTONS
    this.notificationsToAccept = [];

    // ? SHOW OR HIDE ELEMENT
    this.showElement = (show, elem) => {
      show ? elem.classList.replace("d-none", "d-flex") : elem.classList.replace("d-flex", "d-none");
    };

    /** // ? GET DAYS left:
     * @param {Date} initialDate Initial date to count
     * @param {Date} finalDate Final date to count
     * @returns Remaining days
     */
    this.getTimeAgo = (initialDate, finalDate) => {
      let firstDateResult = Date.UTC(initialDate.getFullYear(), initialDate.getMonth() - 1, initialDate.getDate());
      let lastDateResult = Date.UTC(finalDate.getFullYear(), finalDate.getMonth() - 1, finalDate.getDate());
      const CALCULATION = lastDateResult - firstDateResult;
      const DAYS = Math.floor(CALCULATION / (1000 * 60 * 60 * 24));

      if (DAYS === 0) {
        const CALCULATE_HOURS = finalDate.getHours() - initialDate.getHours();

        if (CALCULATE_HOURS === 0) {
          const CALCULATE_MINUTES = finalDate.getMinutes() - initialDate.getMinutes();
          return CALCULATE_MINUTES === 0 ? `Just a Moment` : `${CALCULATE_MINUTES} Minutes Ago`;
        } else return `${CALCULATE_HOURS} Hours Ago`;
      } else if (DAYS % 30 === 0) return `${DAYS} Months Ago`;
      else if (DAYS % 7 === 0) return `${DAYS} Weeks Ago`;
      else return `${DAYS} Days Ago`;
    };
  }

  // ? RECIBIR Y FILTRAR NOTIFICACIONES
  async getNotifications() {
    try {
      const REQUEST = await axios("../helpers/get_notifications.php");
      const RESPONSE = await REQUEST.data;

      return RESPONSE;
    } catch (error) {
      console.error("Notification Error:", error);
    }
  }

  // ? INSERTAR NOTIFICACIÓN
  insertNotification(notification, actions) {
    if (!notification && typeof notification[0] !== "number") return console.error("Not have any notification");

    const $notification = D.importNode($notificationTemplate, true);
    if (notification.state == "0") $notification.querySelector(".notification-item").classList.add("unread");
    $notification.querySelector(".notification-item").href = notification.url;
    $notification.querySelector(".notification-item").dataset.idNote = notification.id_not;
    // $notification.querySelector("[data-title]").textContent = notification.title;
    $notification.querySelector("[data-time-ago]").dataset.timeAgo = this.getTimeAgo(
      new Date(notification.date_),
      new Date()
    );
    $notification.querySelector("[data-issuer-image]").src = `../../${notification.issuerImage}`;
    if (notification.userImage1) {
      $notification.querySelector("[data-image-1]").classList.add("show");
      $notification.querySelector("[data-image-1]").src = `../../${notification.userImage1}`;
    }
    $notification.querySelector("[data-message]").innerHTML = notification.description;

    // * DATOS OPCIONALES
    if (actions) {
      this.notificationsToAccept.push(notification.id_not);
      // * IF THE NOTIFICATION IS RESERVATION REQUEST (HOMESTAY)
      if (notification.title === "Reservation Request") {
        $notification.querySelector("[data-image-2]").classList.add("d-none");
        if (notification.confirm == true) $notification.querySelector(".notification-item").classList.add("confirmed");
        else if (notification.confirm == false)
          $notification.querySelector(".notification-item").classList.add("rejected");
      }
      $notification.querySelector("[data-image-2]").src = `../../${notification.userImage2}`;
      $notification.querySelector("[data-student-date]").textContent = "2022-04-27 / 2022-05-12";
      $notification.querySelector("[data-confirm-student]").dataset.idStudent = "Student ID";
      $notification.querySelector("[data-reject-student]").dataset.idStudent = "Student ID";
    } else $notification.querySelector(".message-footer").remove();

    $notificationList.appendChild($notification);
  }

  // ? EVALUAR SI HAY NUEVAS NOTIFICACIONES
  evaluateNewNotifications(newNotifications) {
    if (newNotifications > 0) {
      $bellBtn.classList.add("anim");
      D.querySelectorAll(".new-notifications").forEach((badge) => {
        badge.textContent = newNotifications;
        badge.classList.add("show");
      });
      setTimeout(() => $bellBtn.classList.remove("anim"), 500);
    } else {
      D.querySelectorAll(".new-notifications").forEach((badge) => badge.classList.remove("show"));
    }
  }

  // ? MARCAR NOTIFICACION COMO LEIDA
  markAsRead(notification) {
    try {
      const updateNotifications = async () => {
        const formData = new FormData();
        formData.set("id_not", notification.dataset.idNote);
        const OPTIONS = {
          method: "POST",
          headers: { "content-type": "application/json; charset=utf-8" },
          data: formData,
        };

        const req = await axios("../helpers/mark_read_notification.php", OPTIONS);
        const data = await req.data;
        if (data === true) {
          D.querySelectorAll(".new-notifications").forEach((elem) => (elem.textContent = Number(elem.textContent) - 1));
          notification.classList.remove("unread");
        }
      };

      if (notification.className.includes("btn") && this.notificationsToAccept.includes(notification.dataset.idNote)) {
        updateNotifications();
      } else if (!this.notificationsToAccept.includes(notification.dataset.idNote)) updateNotifications();
    } catch (error) {
      console.error("Ha ocurrido un error al leer la notificación:", error);
    } finally {
      D.querySelectorAll(".new-notifications").forEach((elem) => {
        if (elem.textContent == 0) elem.classList.remove("show");
      });
    }
  }

  // ? MOSTRAR PANEL DE NOTIFICACIONES
  showPanel() {
    if (D.querySelector(".new-notifications").textContent == 0) {
      D.querySelectorAll(".new-notifications").forEach((elem) => elem.classList.remove("show"));
    }
    const bellBounding = this.bellBtn.getBoundingClientRect();

    if (this.bellBtn.dataset.show === "false") {
      this.bellBtn.dataset.show = true;

      let totalNotifications = $notificationList.querySelectorAll(".notification-item").length;
      if (totalNotifications >= 0 && totalNotifications <= 1) {
        $notificationList.dataset.totalNotifications = `You have ${totalNotifications} notification`;
        if (totalNotifications === 0) {
          if (!$notificationList.querySelector(".no-notifications")) {
            $notificationList.innerHTML += `
            <p class="no-notifications">You don't have any notification</p>
            <div class="m-0 p-0 mx-auto notifications-empty"><img src="../assets/emptys/notifications-empty.png"></div>
            `;
          }
        }
      } else $notificationList.dataset.totalNotifications = `You have ${totalNotifications} notifications`;

      if (W.innerWidth < 768) {
        D.body.style.overflowY = "hidden";
        $notificationList.style.left = "0";
      } else $notificationList.style.left = bellBounding.left - 400 + "px";

      this.showElement(true, $notificationList);
    } else {
      D.body.style.overflowY = "scroll";
      this.bellBtn.dataset.show = false;
      this.showElement(false, $notificationList);
      D.querySelectorAll(".new-notifications").forEach((elem) => elem.classList.remove("show"));
    }
  }
}
