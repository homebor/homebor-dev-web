<?php
error_reporting(0);?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/about_us.css">

    <link  rel="icon"   href="images/icon.png" type="image/png" />
    <title>Homebor - About Us</title>

</head>

<body>

<div class="ts-page-wrapper ts-homepage" id="page-top">
 <!--HEADER ===============================================================-->
<?php
    include 'header.php';
?>

<!-- Main =================================================================-->
            <p id="homebor"></p>
            <div id="section1">

                <!-- Section -->
                <br>
                <br>
                    <section class="container" id="container1">
                            <div class="row">
                                <div class="col-sm-6">
                                     <h2 id="title"><b>Homebor</b></h2>
                                    <p id="white">Homebor is a new accommodation platform. We help schools that runs a homestay department to stay connected with their homestay families in order to bring efficiency and maximize a good experience for everyone involved.</p>
                                    <p id="white">The management and organization of students and homestays in a single space has been systematized. We offer a cross-cutting workflow between the different stakeholders. Consequently, generating a detailed and constant follow-up between the homestay, student and educational organization.</p>
                                    <p id="white">We offer a new and easy experience to find family homes in the locations closest to your academy with updated and intuitive tools for users. Register and become part of our community.</p>
                                    <a href="work_with_us.php" class="btn" id="button">Work With Us</a>
                                    <p id="homestay"></p>
                                    <br>
                                    <br>
                                </div>
                                <div class="col col1 first">
                                       <img src="images/homebor_info.png" class="img-fluid" id="img1" />
                                       <br>
                                       <br>
                                </div>
                            </div>
                    </section>

                    <br>
                    <br>

            </div>
            <div id="div2">

                <!-- Section -->
                <br>
                <br>
                    <section class="container" id="container2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="images/homestay_p.jpg" class="img-fluid" id="img3"/>
                                </div>
                                <div class="col col1 first">
                                     <h2 id="title"><b>Homestay</b></h2>
                                     <p id="black"> A Homestay is a host family that provides accommodation services to foreign students. They offer a comfortable room and meals. Most importantly, they provide an opportunity to show an experience that students will not learn in a classroom. The idea is that the students obtain surprising cultural immersion from these families.</p>

                                    <br>
                                    <br>
                                    <a href="register.php" class="btn" id="button">Become a Homestay</a>

                                       <br>
                                       <br>
                                </div>
                            </div>
                            


                    </section>
                    <br>
                    <br>

            </div>
            
    <!--end #ts-main-->
    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->






<!--FOOTER ===============================================================-->
<?php
    include 'footer.php';
?>

</div>
<!--end page-->


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="assets/js/html/about_us.js"></script>

</body>
</html>
