<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homebor</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/header/fonts/icomoon/style.css">
    
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/index.css">
    <link rel="stylesheet" href="assets/css/header_style.css">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <script src="assets/js/scrollreveal.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    

</head>
<body>
    
     <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        
        <div class="site-mobile-menu-body"></div>
    </div>

    

    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

        <?php include 'header.php' ?>

    </header>

<main class="main_f">
    <!-- BANNER -->          
    <div class="banner_style">
            <img src="assets/img/banner_img.png" class="banner_img" alt="">

            <div class="content_banner">
                <h1>Simplify Your Homestay process</h1>
                <br/>
                <p class="information__banner" style="color: #fff;">Homebor is a student accommodation platform that efficiently handles your entire business process, maximizing the experience for your team, your homestay families and your students</p>
                <br/>
                <a class="btn__information_banner" href="https://calendly.com/homebor/demo?month=2021-08" target="_blank"> Book a Free Demo </a>
            </div>
        </div>

        <!-- SECTION DESCRIPTION -->

        <section class="bg-color2" id="about">

            <section class="section few__clicks">
                <div class="few__clicks_div">
                    <img src="assets/img/student_click.png" class="few__clicks_img" alt="">
                </div>
                
                <div class="few__clicks_information">
                    <h3 class="title__information_clicks">Perfect Matchmaking with just a few clicks</h3>
                    <p class="text__information_clicks">Our platform manages the selection process in a simple and intuitive way, integrating everyone.</p>
                </div>
            </section>

        </section>

        <section class="bg-color1">

            <section class="section">
                
                <div class="better__experience_information">
                    <h3 class="title__information_clicks">Happy Families, Happy Students</h3>
                    <p class="text__information_clicks">Homestay families easily set up their availability and confirm their bookings in our intuitive app.</p>
                </div>

                <div class="better__experience_div">
                    <img src="assets/img/homestay.png" class="better__experience_img" alt="">
                </div>
            </section>

        </section>

        <section class="bg-color2">

            <section class="section save__time">

                <div class="save__time_div">
                    <img src="assets/img/save.png" class="save__time_img" alt="">
                </div>
                
                <div class="save__time_information">
                    <h3 class="title__information_clicks">Save time and money</h3>
                    <p class="text__information_clicks">Get a real time availability of your homestay families without making a single phone call. Once the booking is confirmed everyone gets notified automatically.</p>
                </div>

            </section>

        </section>
        
        <!-- USER TYPES -->

        <div id="service">
            <section class="bg-color1" >

                <section class="section2">

                    <div class="target_info acommodation__provider">
                        <img src="assets/img/accommodation_home.png" class="target_info_img" alt="">
                        <h3>Accommodation Provider</h3>

                        <p class="text">Find the perfect match, availability and book a room in just a few clicks.</p>

                    </div>
                    <div class="target_info homestay">
                        <img src="assets/img/homestay_home.png" class="target_info_img" alt="">
                        <h3>Homestay</h3>

                        <p class="text">One simple and intuitive app to update availability, confirm bookings and communicate with the school.</p>
                    </div>
                    <div class="target_info student">
                        <img src="assets/img/student_home.png" class="target_info_img" alt="">
                        <h3>Student</h3>

                        <p class="text">Students take active part of the matching process, significantly reducing changes and cancellations.</p>
                    </div>

                </section>
            
            </section>
        </div>
        

    <!-- DOWNLOAD APP -->

        <section class="bg-color2">

            <section class="section">
                <div class="download__div">
                    <h3 class="title__information_clicks">Coming Soon!</h3>
                    <p class="text__information_clicks">We are working to give you a better experience from your mobile device</p>
                </div>
                <div class="download">
                    <img src="assets/img/download.png" class="download_img" alt="">
                </div>
            </section>
        </section>

    <!-- Information -->

        <section class="bg-color1 bottom">
            <section class=" section information__text">

                <div class="img__information">
                    <img src="assets/img/accommodation_home1.png" alt="" class="information__img">
                </div>

                <div class="text__information text-center">
                    <h3 class="">Work With Us</h3>
                    <p class="subtitle">We are reaching out to a small number of schools to offer our platform, at no cost, in exchange of only their feedback and ideas. It is extremely important for us to continue to build a product that add value to schools and family and creates the best matchmaking for students.</p>

                    <a class="btn regular-button text-center btn__book_free" href="https://calendly.com/homebor/demo?month=2021-08"> Book a Free Demo </a>
                </div>

            </section>
        </section>

    </main>

        
    <?php
        include 'footer.php';
    ?>
     

</main>
</body>
</html>