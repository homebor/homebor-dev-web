<?php
    require '../xeon.php';
    session_start();
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    $query2="SELECT * FROM `academy` ORDER BY id_ac DESC LIMIT 1";
    $resultado2=$link->query($query2);

    $row2=$resultado2->fetch_assoc();

    $id_ac = "$row2[id_ac]";
    $sum = '1';


    $n_a = $id_ac + $sum;

    if ($row['usert'] != 'Admin') {
        if ($row['usert'] != 'Cordinator') {
         header("location: ../logout.php");   
        }
    } 

    if ($row['usert'] == 'Admin') {
        $way = 'index.php';
    } elseif ($row['usert'] == 'Cordinator') {
       $way = 'cordinator.php';
       echo '<style type="text/css">a#admin{display:none;}</style>';
    }

?>
<!DOCTYPE>
<html>
  <head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/academy.css">
     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Administartor Panel</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
  </head>
 <body id="body">

<!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <main id="ts-main">

        <!--PAGE TITLE
            =========================================================================================================-->


        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                              <br>
                              <br>
                              <br>
                                <br>
                             <h2>Academy Administrator Panel </h2>
    <!-- Echo username -->
    <?php
        $usuario = $_SESSION['username'];
        echo "<p>You are logged in as <b>$usuario</b>.</p>"; 
    ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <hr>
            <div class="container">
              <br />
              <div class="card">
                <div class="card-body">
                  <div class="form-group">
                    <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" />
                  </div>
                  <div class="table-responsive" id="dynamic_content">
                  </div>

                    <div id="table-voun" class="table-responsive text-center ">
                        <table class="table table-hover" id="vou">
                            <thead style="background-color: #232159; color: white">
                                <tr>
                                    <th class="text-center align-middle">Photo Academy</th>
                                    <th class="text-center align-middle">Academy Name</th>
                                    <th class="text-center align-middle">Direction</th>
                                    <th class="text-center align-middle">State</th>
                                    <th class="text-center align-middle">City</th>
                                    <th class="text-center align-middle">Postal Code</th>
                                    <th class="text-center align-middle"></th>
                                </tr>
                            </thead>

                            <tbody class="table-bordered" id="vouchers"></tbody>

                        </table>
                    </div>
                </div>
              </div>
            </div>
</section>

<!--Register New Academy
        =============================================================================================================-->
        
<div class="container">
  <br>
  <br>
  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Register New Academy</button>
  <div id="demo" class="collapse">
    <br>
    <br>
    <br>
    <section id="register" >

        <!--LOGIN / REGISTER SECTION
            =========================================================================================================-->
            <section id="login-register">
            <div class="container">
                <div class="row" >

                    <div class="offset-md-2 col-md-8 offset-lg-3 col-lg-6" >

                        <!--LOGIN / REGISTER TABS
                            =========================================================================================-->
                            <!--REGISTER TAB
                                =====================================================================================-->
                            <div class="tab-pane active" id="register" role="tabpanel" aria-labelledby="register-tab">

                                <!--Register tab-->
                                    
                                    <form action="" method="post" autocomplete="off" enctype="multipart/form-data">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" placeholder="Academy Name" name="name" autocomplete="off" required>
                                    </div>
                                    
                                    <!-- Direction -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="dir" placeholder="Direction" name="dir" autocomplete="off" required>
                                    </div>

                                    <!--State-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="state" placeholder="State" name="state" autocomplete="off" required>
                                    </div>

                                    <!--City-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="city" placeholder="City" name="city" autocomplete="off" required>
                                    </div>

                                    <!--Postal Code-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="p_code" placeholder="Postal Code" name="p_code" autocomplete="off" required>
                                    </div>

                                    <!--Academy Number-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="n_a" placeholder="Academy" name="n_a" autocomplete="off" required value="<?php echo "$n_a" ?>" hidden>
                                    </div>

                                    <!--Image-->
                                        <div class="form-group">
                                             <label for="mail_s">Academy Photo</label>
                                            <div class="file-upload-previews"></div>
                                                    <div class="input-group-text">
                                                    <input type="file" class="file-upload-input with-preview " name="image[]" id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf">
                                                    
                                    </div>
                                        </div>
                                <!--end row-->
                            <br>
                                    <button id="register" type="submit" class="btn btn-primary" name="register" value="register">Register</button>
                                    <?php

                                    if (isset($_POST['register'])) {
                                         $search = "SELECT * FROM academy WHERE name = '$_POST[name]' and p_code = '$_POST[p_code]'";
                    $n_a = $_POST['n_a'];
                    $result = $link->query($search);
                    $count = mysqli_num_rows($result);
                    if ($count == 1) {
                         echo  "<script type='text/javascript'>window.top.location='academy.php?fallo=yes';</script>"; exit;
                      }
                    else{ 
                    $image = $_FILES['image']['name'];
                    $image_tmp = $_FILES['image']['tmp_name'];
                    $paths = '../public_a/'.$n_a.'/';
                    $path = 'public_a/'.$n_a.'/';
                    if (file_exists($paths)) {
                    for($i = 0; $i < count($image_tmp); $i++){
                    move_uploaded_file($image_tmp[$i], $paths.'/'.$image[$i]);}
                    }
                        else {
                            mkdir('../public_a/'.$n_a.'', 0777);
                            for($i = 0; $i < count($image_tmp); $i++){
                            move_uploaded_file($image_tmp[$i], $paths.'/'.$image[$i]);}
                             }
        if (empty($image[0])) {
            $photo = 'NULL';
        }
        else {
            $photo =  $path."".$image[0]; 
        }
        if (empty($_POST['name'])) {
            $name = 'NULL';
        }
        else {
            $name =  $_POST['name'];     
        }
        if (empty($_POST['dir'])) {
            $dir = 'NULL';
        }
        else {
            $dir =  $_POST['dir'];     
        }
        if (empty($_POST['state'])) {
            $state = 'NULL';
        }
        else {
            $state =  $_POST['state'];     
        }
        if (empty($_POST['city'])) {
            $city = 'NULL';
        }
        else {
            $city =  $_POST['city'];     
        }
        if (empty($_POST['p_code'])) {
            $p_code = 'NULL';
        }
        else {
            $p_code =  $_POST['p_code'];     
        }
        $date = date('Y-m-d H:i:s');
        $register = "INSERT INTO academy (name_a, dir_a, state_a, city_a, p_code_a, id_photo, photo_a) VALUES ('$name', '$dir', '$state', '$city', '$p_code', '$n_a', '$photo')";
        $register2 = "INSERT INTO webmaster (user, activity, dates, edit_user) VALUES ('$usuario', 'Register a New Academy', '$date', '$name, $p_code')";
        $resultado=$link->query($register);
        $resultado2=$link->query($register2);
        if($resultado==1)
        {       

        echo "Inserted successfully";
        echo  "<script type='text/javascript'>window.top.location='academy.php';</script>"; exit;  
        
        }
        else {       

        echo "Insertion Failed";

             }


                                            }
                                         



                                          }
                 
                                    ?>
                                </form>
                            </div>
                            <!--end #register.tab-pane-->
                        </div>
                        <!--end tab-content-->

                    </div>
                    <!--end offset-4.col-md-4-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
      </section>
    </section>
  </div>
</div>
        
<?php

                                    if (isset($_GET["fallo"]) && $_GET["fallo"] == 'yes') {
                                        echo "<h5 id='bad'><br><br>Academy is Already Reister</h5>";}
                                    ?>
        

</body>
</main>
</body>


<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>

<!--end page-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="assets/js/academies.js"></script>

</body>
</html>
