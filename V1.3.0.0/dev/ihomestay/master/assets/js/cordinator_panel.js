//CORDINATOR PANEL LIST
$(document).ready(function() {

    load_data(1);

    function load_data(page, query = '') {
        $.ajax({
            url: "cordinators_fecth.php",
            method: "POST",
            data: { page: page, query: query },
            success: function(data) {
                $('#dynamic_content').html(data);
            }
        });
    }

    $('#vou').hide();

    $('#search_box').keyup(function() {

        if ($('#search_box').val()) {

            $('#dynamic_content').hide();
            $('#vou').show();

            let search = $('#search_box').val();

            var searchLength = search.length;

            $.ajax({
                url: 'co-search.php',
                type: 'POST',
                data: { search },
                success: function(response) {
                    let coor = JSON.parse(response);
                    let template = '';

                    coor.forEach(co => {
                        if (co.id_photo != 'NULL') {
                            template += `
            <tr>

              <th class="text-center align-middle" style="font-weight:normal"><img src="../${co.photo}" width="90px;" height="90";></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.agency} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.a_mail} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.status} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.num} </th>
              <th class="text-center align-middle" >
                    <a id="edit" href="#" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                            <i class="fa fa-cog"></i>      
                                Advance Options
                                </a>
              </th>

            </tr>`
                        } else {
                            template += `
            <tr>

              <th class="text-center align-middle" style="font-weight:normal"></th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.names} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.agency} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.a_mail} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.status} </th>
              <th class="text-center align-middle" style="font-weight:normal"> ${co.num} </th>
              <th class="text-center align-middle">
                    <a id="edit" href="#" class="btn btn-primary btn-sm btn-block float-right" name="edit">
                            <i class="fa fa-cog"></i>      
                                Advance Options
                                </a>
              </th>

            </tr>`

                        }
                    });


                    $('#vouchers').html(template);
                }
            });
        } else {
            $('#dynamic_content').show();
            $('#vou').hide();
        }

    });

    $(document).on('click', '.page-link', function() {
        var page = $(this).data('page_number');
        var query = $('#search_box').val();
        load_data(page, query);
    });

});