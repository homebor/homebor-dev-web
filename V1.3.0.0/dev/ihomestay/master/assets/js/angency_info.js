//PROPERTIES LIST
$(document).ready(function(){

    load_data(1);

    function load_data(page, query = '')
    {
      $.ajax({
        url:"properties3.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content').html(data);  
        }
      });
    }

    $(document).on('click', '.page-link', function(){
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
    });

    $('#search_box').keyup(function(){
      var query = $('#search_box').val();
      load_data(1, query);
    });

  });

//STUDENTS LIST

$(document).ready(function(){

    load_data(1);

    function load_data(page, query = '')
    {
      $.ajax({
        url:"students3.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content2').html(data);
        }
      });
    }

    $(document).on('click', '.page-link', function(){
      var page = $(this).data('page_number2');
      var query = $('#search_box2').val();
      load_data(page, query);
    });

    $('#search_box2').keyup(function(){
      var query = $('#search_box2').val();
      load_data(1, query);
    });

  });

  //AGENTS LIST

  $(document).ready(function(){

    load_data(1);

    function load_data(page, query = '')
    {
      $.ajax({
        url:"agents.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content3').html(data);
        }
      });
    }

    $(document).on('click', '.page-link', function(){
      var page = $(this).data('page_number3');
      var query = $('#search_box3').val();
      load_data(page, query);
    });

    $('#search_box3').keyup(function(){
      var query = $('#search_box3').val();
      load_data(1, query);
    });

  });

//HISTORY LIST

$(document).ready(function(){

    load_data(1);

    function load_data(page, query = '')
    {
      $.ajax({
        url:"agency_history.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content4').html(data);
        }
      });
    }

    $(document).on('click', '.page-link', function(){
      var page = $(this).data('page_number4');
      var query = $('#search_box4').val();
      load_data(page, query);
    });

    $('#search_box4').keyup(function(){
      var query = $('#search_box4').val();
      load_data(1, query);
    });

  });