<?php
    require '../xeon.php';
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query11="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado11=$link->query($query11);

    $row11=$resultado11->fetch_assoc();

    if ($row11['usert'] == 'Admin') {
        $way = '../master/index.php';
    } elseif ($row11['usert'] == 'Cordinator') {
       $way = '../master/cordinator.php';
       echo '<style type="text/css"> a#admin{display:none;} </style>';
    }

?>

<link rel="stylesheet" type="text/css" href="assets/css/notification.css">
<!-- WRAPPER - FONDO
    =================================================================================================================-->

    <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
    <header id="ts-header" class="fixed-top">
        <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
        <link rel="stylesheet" type="text/css" href="assets/css/header.css">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container">

                <!--Brand Logo/ Logo HOMEBOR-->
                <a class="navbar-brand" href="<?php echo $way;?>">
                    <img src="../assets/logos/page.png" alt="">
                </a>

                <?php include 'notification-after.php' ?>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse header_m" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav">
                        <!--About Us (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">
                            <a class="nav-link" href="../master/homestay" id="active">Homestay</a>
                                <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/homestay" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Homestay Panel</p>
                                            </a>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/home_register" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Register Homestay</p>
                                            </a>

                                        </li>

                                        <li class="nav-item">

                                            <a href="../master/homestay_payments" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Homestay Payments</p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>
                        </li>

                        <li class="nav-item">
                            <a id="admin" class="nav-link" href="../master/cordinator_panel" style="color: #982A72;">Coordinators</a>
                        </li>

                        <li class="nav-item ts-has-child">
                            <a class="nav-link" href="../master/student" id="nav">Students</a>
                            <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/student" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Students Panel</p>
                                            </a>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/waiting_confirmation" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Awaiting Confirmation</p>
                                            </a>

                                        </li>

                                        <li class="nav-item">

                                            <a href="../master/student_register" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Register Student</p>
                                            </a>

                                        </li>

                                        <li class="nav-item">

                                            <a href="../master/student_register" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Student Payments</p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>
                        </li>
                       
                        <!--end About Us nav-item-->

                        
                        <!--end Student nav-item-->
                        <li class="nav-item ts-has-child">
                            <a class="nav-link" href="../master/agencies" id="nav">Providers</a>
                            <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item ts-has-child">

                                            <a href="#" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Managers </p>
                                            </a>

                                            <ul class="ts-child">

                                <!-- CATEGORY ICONS (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="../master/agencies" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Providers Panel </p>
                                    </a>

                                </li>
                                <!--end CATEGORY ICONS (1st level)-->

                                <!-- GRID (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="../master/agencies_payments" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Providers Payments </p>
                                    </a>

                                </li>

                                <li class="nav-item">

                                    <a href="../master/agencies_voucher" class="nav-link pl-2 pr-2">
                                        <p class="bar-ii zoom"> Providers Vouchers </p>
                                    </a>

                                </li>
                                <!--end GRID (1st level)-->

                            </ul>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/agents_panel" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Coordinators </p>
                                            </a>

                                        </li>
                                        <!--end MapBox-->

                                    </ul>
                        </li>
                        

                        <!--Host (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="../master/academy" id="nav">Academy</a>
                        </li>
                        <!--end Host nav-item-->

                        <li class="nav-item ts-has-child">
                            <a class="nav-link" href="../master/activity" id="active">Status</a>
                                <ul class="ts-child">

                                        <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/activity" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Activities</p>
                                            </a>

                                        </li>
                                        <!--end OpenStreetMap-->

                                        <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                                        <li class="nav-item">

                                            <a href="../master/reports" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom">Reports</p>
                                            </a>

                                        </li>

                                    </ul>
                        </li>

                        

                        <!--Work with US (Main level)
                        =============================================================================================-->

                        
                        <!--end Work with US nav-item-->

                        <?php include 'notification-before.php' ?>

                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav ml-auto">

                        <!--LOGIN (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="../logout" id="nav">Logout</a>
                        </li>



                    </ul>
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->

    </header>
    <!--end Header-->