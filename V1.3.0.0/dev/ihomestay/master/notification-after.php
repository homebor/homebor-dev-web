
<link rel="stylesheet" href="../master/assets/css/notification.css">

<style>
    .img-stu{ width: 2.5em; height: auto; }

</style>

<?php  

    $noti = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND confirmed = '0' ORDER BY id_not DESC ");
    $n = mysqli_num_rows($noti);

    $noti2 = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND state = '0' AND confirmed = '0' ");
    $n2 = mysqli_num_rows($noti2);

    $noti3 = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND state = '1' AND confirmed = '0' ");
    $n3 = mysqli_num_rows($noti3);

    $plan = $link->query("SELECT * FROM plan WHERE mail = '$usuario' ");
    $rplan=$plan->fetch_assoc();

    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');

?>
                     
    <li class="dropdown d-after" style="margin-top: 1%; list-style: none;">
        <a class="after" data-toggle="dropdown" href="#" onclick="load_ma()" style="color: #982A72; font-size: 14px;">
        <img src="../assets/icon/notification 64.png" alt="" width="32" height="32">
        <span class="badge" id="afm"> <?php echo $n2; ?></span></a>

        <?php 
            if ($n2 != 0 ) {
        ?>

            <style> .badge{display: inline;} </style>

        <?php
            }else{
        ?>

            <style> .badge{display: none;} </style>

        <?php
            }
        ?>

        <!-- List (1st level) -->
        <ul class="dropdown-menu d-m-after" style="padding: 0;">
                                
            <div class="container__notification">

                <form action="delete_noti.php" method="POST" class="m-0 p-0">

                    <input type="hidden" id="mail" name="mail" value="<?php echo $man['mail'] ?>">

                    <p class="title__noti_div">You have <?php echo $n; ?> Notification <button type="submit" class="button_delete_all" name="delete_all" title="Delete All notification">Delete all</button></p>

                </form>

                    <?php 
                    if ($n == 0) {
                    ?>

                    <style> .title__noti_div{display: none;} ul.dropdown-menu{height:3em; padding: 0;} .menu{overflow: hidden; display: none;} .title_not-i{color: #232159;} </style>

                                
                    <?php
                        echo "<li class='title_not-i'><center><b> You don't have notification request </b></center></li>";
                    }


                    elseif($n3 == 1){}

                    ?>

                    <div class="content__noti">

                        <div class="scrollbar" id="style-1">

                        <?php

                        while ($n = mysqli_fetch_array($noti)) {

                                $di = $n['id_not'];

                                $ma = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
                                $rma=$ma->fetch_assoc();

                                $stu = $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[report_s]' ");
                                $rs=$stu->fetch_assoc();

                                $peh = $link->query("SELECT * FROM pe_home WHERE mail_h = '$n[user_i_mail]' ");
                                $rp=$peh->fetch_assoc();

                                $start_date = new DateTime($n['date_']);
                                $since_start = $start_date->diff(new DateTime($date));

                                    

                                if($since_start->m > '1'){

                                    $datec = date_create($n['date_']);

                                    $time_send = date_format($datec, 'jS M \of\ Y');

                                } elseif($since_start->m == '1'){

                                    $time_send = $since_start->m.' month ago<br>';
                                            
                                }else{

                                    if($since_start->d > '1'){

                                        if($since_start->d > '7'){

                                            $time_send = floor(($since_start->format("%a") / 7)) . " weeks ago";

                                        }else{

                                            $time_send = $since_start->d.' days ago<br>';
                                                
                                        }

                                    }elseif($since_start->d == '1'){

                                        $time_send = $since_start->d.' day ago<br>';

                                    }else{

                                        if($since_start->h > '1'){

                                            $time_send = $since_start->h.' hours ago<br>';
                                                
                                        } elseif($since_start->h == '1'){

                                            $time_send = $since_start->h.' hour ago<br>';

                                        }else{

                                            if($since_start->i > '1'){

                                                $time_send = $since_start->i.' minutes ago<br>';

                                            } elseif($since_start->i == '1'){

                                                $time_send = $since_start->i.' minute ago<br>';

                                            }else{
                                                
                                                if($since_start->s != '0'){
                                                            
                                                    $time_send = 'Just a moment';
                                                        
                                                }
                                                    
                                            }
                                                
                                        }
                                            
                                    }
                                        
                                }

                                if($n['title'] == 'Cancel Reservation' && $n['status'] != 'Reply'){

                                    ?>
        
                                    <div class="div_panel_noti">
                                        <form action="delete_noti.php" method="POST">
                    
                                            <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">
                    
                                            <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one" title="Delete Notification"></button>
                    
                                        </form>
                                                    
                                        <?php if($n['state'] == '0'){ ?>
                    
                                        <p class="ban m-0 text_time"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel new" id="cancel_report">
                                                    
                                        <?php }else{ ?>
                    
                                        <p class="ban m-0"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel view" id="cancel_report">
                    
                                        <?php } ?>
                    
                                            <div class="col-2 p-0">
                                                <?php echo'<img class="photo" src="../'.$rp['phome'].'"/>'; ?>
                                            </div>
        
                                            <div class="col-10 p-0 pr-3">
                                                <?php if($n['state'] == '0'){ ?>
                                                                
                                                    <p class="text_wview"><b><?php echo $rp['h_name'] . "</b> wants to remove the <b>".$rs['name_s']. " " .$rs['l_name_s']."</b> reservation. Click for details"; ?></p>
                    
                                                <?php }else{ ?>
                    
                                                    <p class="font_view"><b><?php echo $rp['h_name'] . "</b> wants to remove the <b>".$rs['name_s']. " " .$rs['l_name_s']."</b> reservation. Click for details"; ?></p>
                    
                                                <?php } ?>
                                            </div>
                    
                                        </a>
                                    </div>
        
                                    <?php } elseif($n['title'] == 'Report Situation' && $n['status'] != 'Reply'){ ?>
        
                                    <div class="div_panel_noti">
                                        <form action="delete_noti.php" method="POST">
                    
                                            <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">
                    
                                            <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one" title="Delete Notification"></button>
                    
                                        </form>
                                                    
                                        <?php if($n['state'] == '0'){ ?>
                    
                                        <p class="ban m-0 text_time"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel new" id="cancel_report">
                                                    
                                        <?php }else{ ?>
                    
                                        <p class="ban m-0"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel view" id="cancel_report">
                    
                                        <?php } ?>
                    
                                            <div class="col-2 p-0">
                                                <?php echo'<img class="photo" src="../'.$rp['phome'].'"/>'; ?>
                                            </div>
        
                                            <div class="col-10 p-0 pr-3">
                                                <?php if($n['state'] == '0'){ ?>
                                                                
                                                    <p class="text_wview"><b><?php echo $rp['h_name'] . "</b> has reported to <b>".$rs['name_s']. " " .$rs['l_name_s']."</b>. Click for details"; ?></p>
                    
                                                <?php }else{ ?>
                    
                                                    <p class="font_view"><b><?php echo $rp['h_name'] . "</b> has reported to <b>".$rs['name_s']. " " .$rs['l_name_s']."</b>. Click for details"; ?></p>
                    
                                                <?php } ?>
                                            </div>
                    
                                        </a>
                                    </div>
        
                                    <?php } elseif($n['status'] == 'Reply'){ ?>
                                        
                                    <div class="div_panel_noti">
                                        <form action="delete_noti.php" method="POST">
                    
                                            <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">
                    
                                            <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one" title="Delete Notification"></button>
                    
                                        </form>
                                                    
                                        <?php if($n['state'] == '0'){ ?>
                    
                                        <p class="ban m-0 text_time"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel new" id="cancel_report">
                                                    
                                        <?php }else{ ?>
                    
                                        <p class="ban m-0"><?php echo $time_send ?></p>
                    
                                        <a href="../master/reports.php" class="notification_panel view" id="cancel_report">
                    
                                        <?php } ?>
                    
                                            <div class="col-2 p-0">
                                                <?php echo'<img class="photo" src="../'.$rp['phome'].'"/>'; ?>
                                            </div>
        
                                            <div class="col-10 p-0 pr-3">
                                                <?php if($n['state'] == '0'){ ?>
                                                                
                                                    <p class="text_wview"><b><?php echo $rp['h_name'] . "</b> has responded to your report. Click for details"; ?></p>
                    
                                                <?php }else{ ?>
                    
                                                    <p class="font_view"><b><?php echo $rp['h_name'] . "</b> has responded to your report. Click for details"; ?></p>
                    
                                                <?php } ?>
                                            </div>
                    
                                        </a>
                                    </div>
        
                                    <?php } elseif($n['title'] == 'Flight Data Confirmation'){ 
                                    
                                    $stu2 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[user_i_mail]' ");
                                    $rs2=$stu2->fetch_assoc();
        
                                    $hom = $link->query("SELECT * FROM pe_home WHERE mail_h = '$n[reserve_h]' ");
                                    $rhom=$hom->fetch_assoc();
                                    
                                    ?>
        
                                    <div class="div_panel_noti">
                                        <form action="delete_noti.php" method="POST">
                    
                                            <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">
                    
                                            <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one" title="Delete Notification"></button>
                    
                                        </form>
                                                    
                                        <?php if($n['state'] == '0'){ ?>
                    
                                        <p class="ban m-0 text_time"><?php echo $time_send ?></p>
                    
                                        <a href="<?php echo '../student/student_edit.php?art_id='.$rs2['id_student']?>" class="notification_panel new" id="cancel_report">
                                                    
                                        <?php }else{ ?>
                    
                                        <p class="ban m-0"><?php echo $time_send ?></p>
                    
                                        <a href="<?php echo '../student/student_edit.php?art_id='.$rs2['id_student']?>" class="notification_panel view" id="cancel_report">
                    
                                        <?php } ?>
                    
                                            <div class="col-2 p-0">
                                                <?php echo'<img class="photo" src="../'.$rs2['photo_s'].'"/>'; ?>
                                            </div>
        
                                            <div class="col-10 p-0 pr-3">
                                                <?php if($n['state'] == '0'){ ?>
                                                                
                                                    <p class="text_wview"><b><?php echo $rs2['name_s'] .' '. $rs2['l_name_s'] . "</b> has added a new Flight Information. Click to view"; ?></p>
                    
                                                <?php }else{ ?>
                    
                                                    <p class="font_view"><b><?php echo $rs2['name_s'] .' '. $rs2['l_name_s'] . "</b> has added a new Flight Information. Click to view"; ?></p>
                    
                                                <?php } ?>
                                            </div>
                    
                                        </a>
                                    </div>

                                    <?php } elseif($n['title'] == 'Payments Arrival'){ 

                                    $hom = $link->query("SELECT * FROM pe_home WHERE mail_h = '$n[user_i_mail]' ");
                                    $rhom=$hom->fetch_assoc();

                                    $stu_not = $link->query("SELECT * FROM pe_student WHERE mail_s = '$n[des]' ");
                                    $r_nots=$stu_not->fetch_assoc();

                                    ?>

                                        <div class="div_panel_noti">
                                            <form action="delete_noti.php" method="POST">

                                                <input type="hidden" id="id_not" name="id_not" value="<?php echo $n['id_not'] ?>">

                                                <button type="submit" class="fa fa-trash button_trash" id="btn_can" name="delete_one" title="Delete Notification"></button>

                                            </form>
                                                
                                            <?php if($n['state'] == '0'){ ?>

                                            <p class="ban m-0 text_time"><?php echo $time_send ?></p>

                                            <a href="../homestay/homedit?art_id=<?php echo $rhom['id_home'] ?>" class="notification_panel new" id="cancel_report">
                                                
                                            <?php }else{ ?>

                                            <p class="ban m-0"><?php echo $time_send ?></p>

                                            <a href="../homestay/homedit?art_id=<?php echo $rhom['id_home'] ?>" class="notification_panel view" id="cancel_report">

                                            <?php } ?>

                                                <div class="col-2 p-0">
                                                    <?php echo'<img class="photo" src="../'.$rhom['phome'].'"/>'; ?>
                                                </div>

                                                <div class="col-10 p-0 pr-3">
                                                    <?php if($n['state'] == '0'){ ?>
                                                            
                                                        <p class="text_wview"><?php echo "<b>". $r_nots['name_s'] . ' ' . $r_nots['l_name_s'] ."</b> has arrived at ". $rhom['h_name'] . ". Verify Payment Data." ; ?></p>

                                                    <?php }else{ ?>

                                                        <p class="font_view"><?php echo "<b>".  $r_nots['name_s'] . ' ' . $r_nots['l_name_s'] ."</b> has arrived at ". $rhom['h_name'] . ". Verify Payment Data." ; ?></p>

                                                    <?php } ?>
                                                </div>

                                            </a>
                                        </div>
        
                                    <?php }else{} ?>

                        <?php } ?>

                            <div class="force-overflow"></div>

                        </div>

                    </div>

                </div>

        </ul>
    </li>


        <!--<script>

                        function load_ma(){

                            let mailm = $('#mailm').val();

                            $.ajax({
                                url: 'notification_m.php',
                                type: 'POST',
                                data: {mailm},
                                success: function(response){

                                    $('#afm').hide();

                                }
                            });
                        }


                </script>-->