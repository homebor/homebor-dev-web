<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM academy WHERE id_ac = '$id' ");
$row=$query->fetch_assoc();


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
     header("location: ../logout.php");   
    }
}

if ($row5['usert'] == 'Admin') {
    $way = '../master/index.php';
} elseif ($row5['usert'] == 'Cordinator') {
   $way = '../master/cordinator.php';
    echo '<style type="text/css"> a#admin{display:none;} </style>';
}
}
?>
<!DOCTYPE>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/academy_preview.css">
    
    <!--Mapbox Links-->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Academy Profile</title>

</head>
<body>

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include '../master/header.php';
?>
    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <h1>Academy Information</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
        <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

        <form id="form-submit" class="ts-form" autocomplete="off" action="action.php" method="post" enctype="multipart/form-data">

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Basic Information</h3>

                                <div class="text-center">
                                     <?php
                                    
                                        if($row['photo_a'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo_a'].'" id="image-profile"/>';}
                                 
                                    ?>
                                </div>

                                 <div class="row">

                                    <!--Name-->
                                            <?php
                                            if($row['name_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Academy Name</label>
                                        <p>".$row['name_a']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                            </section>
                            <!--end #location-->

                             <!--LOCATION
                            =====================================================================================-->
                        <section id="location" class="mb-5">

                            <!--Title-->
                            <h3 class="text-muted border-bottom">Location</h3>

                            <div class="row">

                                <div class="col-sm-6">

                                    <!--Address-->
                                    <div class="input-group"><?php
                                            if($row['dir_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>Address</label>
                                        <p>".$row['dir_a']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                    <!--City-->
                                    <div class="form-group">
                                        <?php
                                            if($row['city_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>City</label>
                                        <p>".$row['city_a']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                    <!--State-->
                                    <div class="form-group">
                                        <?php
                                            if($row['state_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>State</label>
                                        <p>".$row['state_a']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                    <!--ZIP-->
                                    <div class="form-group mb-0">
                                        <?php
                                            if($row['p_code_a'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>Postal Code</label>
                                        <p>".$row['p_code_a']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                </div>
                                <!--end col-md-6-->

                                <!--Map-->
                                <div id='map'></div>
                                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address ="<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 16
                                    });
                                    var ei = document.createElement('div');
                                    ei.id = 'marker';

                                    new mapboxgl.Marker(ei)
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
  </script>
                                <!--end col-md-6-->

                            </div>
                            <!--end row-->
                        </section>
                        <!--end #location-->

                           </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include '../master/footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>

