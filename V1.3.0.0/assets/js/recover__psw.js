$(".card__section-help").find("a").click(function(){
    $(".card__section-help a").removeClass('active1');
    $(this).addClass('active1');
    });
    
    $("#submit_question").click(function(){
    $(".card__section-help a").removeClass('active1');
    $('#second').addClass('active1');
    });

    
        function first (){
            

            document.getElementById('questions').classList.remove('display1');
            document.getElementById('contact').classList.add('display2');
            document.getElementById('recover').classList.add('display3');

        }

        function second (){
            
            document.getElementById('contact').classList.remove('display2');
            document.getElementById('questions').classList.add('display1');
            document.getElementById('recover').classList.add('display3');

        }
        
        function third (){

            document.getElementById('recover').classList.remove('display3');
            document.getElementById('contact').classList.add('display2');
            document.getElementById('questions').classList.add('display1');

            
            
        }

        first ()

    const form__recover = document.getElementById('form__recover');

    form__recover.addEventListener('submit', (e) =>{
        e.preventDefault();

        document.getElementById('btn__send_recover').classList.add('btn_none');
        document.getElementById('div__send__recovery').classList.add('spin');

        $.ajax({
            url: 'email.php',
            type: 'POST',
            data: $('#form__recover').serialize(),
            success: function(response) {

                let recover = JSON.parse(response);

                recover.forEach(rec => {

                    if(rec.response == 'Sent'){

                        form__recover.reset();

                        document.getElementById('btn__send_recover').classList.remove('btn_none');
                        document.getElementById('div__send__recovery').classList.remove('spin');

                        setTimeout(() => {
                            $('#message__sent').html('message sent, please check your email').fadeIn("slow");

                            setTimeout(() => {
                                $('#message__sent').html('message sent, please check your email').fadeOut("slow");

                            }, 5000)
                        }, 100);

                        

                    }else if(rec.response == 'Fallo'){

                        document.getElementById('btn__send_recover').classList.remove('btn_none');
                        document.getElementById('div__send__recovery').classList.remove('spin');

                        $('#message__error').html('Connection Error')

                    }else if(rec.response == 'Not match'){

                        document.getElementById('btn__send_recover').classList.remove('btn_none');
                        document.getElementById('div__send__recovery').classList.remove('spin');

                        $('#message__error').html('This email does not belong to any user')

                    }

                });



            }
        });

    });