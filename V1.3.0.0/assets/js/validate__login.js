// Eye Function

let password = document.getElementById("password");
let viewPassword = document.getElementById("btn__eye");
let click = false;

// TODO VARIABLES DE FLOJOS
const D = document;
const W = window;

viewPassword.addEventListener("click", (e) => {
  if (!click) {
    password.type = "text";
    $("#btn__eye").removeClass("fa-eye").addClass("fa-eye-slash");
    click = true;
  } else if (click) {
    password.type = "password";
    $("#btn__eye").removeClass("fa-eye-slash").addClass("fa-eye");
    click = false;
  }
});

const formulary = document.getElementById("form__login");

const inputs = document.querySelectorAll("#form__login input");

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^\d{2,20}$/, // 2 a 14 numeros.
  rooms: /^\d{1,2}$/, // 1 a 14 numeros.
  dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^.{4,12}$/, // 4 a 12 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
  message: /^[a-zA-ZÀ-ÿ0-9\s]{1,500}$/, // Description message.
};

const campos = {
  email: true,
  password: true,
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    case "email":
      validateField(expresiones.correo, e.target, "email");
      break;

    case "password":
      validateField(expresiones.password, e.target, "password");
      break;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`group__${campo}`).classList.remove("form__group-incorrect");
    document.getElementById(`group__${campo}`).classList.add("form__group-correct");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  } else {
    document.getElementById(`group__${campo}`).classList.add("form__group-incorrect");
    document.getElementById(`group__${campo}`).classList.remove("form__group-correct");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.add("form__group__input-error-active");
    campos[campo] = false;
  }
};

async function comprobeUser(action) {
  if (action === true) {
    // TODO USER'S VARIABLES
    const $email = D.querySelector("#email").value.toLowerCase();
    const formLogin = new FormData(formulary);
    formLogin.set("email", $email);
    const DATA = { method: "POST", body: formLogin };

    // TODO FETCH LOGIN
    const jsonFormLogin = await fetch("./loguear.php", DATA);
    const resultFormLogin = await jsonFormLogin.json();

    resultFormLogin.forEach((user) => {
      if (user.originUser === "iHomestay") W.top.location = "ihomestay/comprobe?art_id=" + user.idUser;
      if (user.originUser === "Homebor") W.top.location = "comprobe?art_id=" + user.idUser;
      if (user.response == "no validado captcha") {
        document.querySelector(".btn__log-in").classList.remove("btn_none");
        document.querySelector(".div__btn").classList.remove("spin");
        document.getElementById("message__div").classList.add("block");

        setTimeout(() => {
          let message = "reCaptcha not validated";
          $("#message").html(message);

          setTimeout(() => {
            document.getElementById("message__div").classList.remove("block");
          }, 5000);
        }, 100);

        document.getElementById("group__email").classList.add("form__group-incorrect");
        document.getElementById("group__email").classList.remove("form__group-correct");
        document.getElementById("group__password").classList.add("form__group-incorrect");
        document.getElementById("group__password").classList.remove("form__group-correct");
        document.querySelector(".form__group__input-error-message").classList.add("form__group__input-error-active");
      }
      if (user.originUser === "Incorrect") {
        grecaptcha.reset();
        document.querySelector(".btn__log-in").classList.remove("btn_none");
        document.querySelector(".div__btn").classList.remove("spin");

        document.getElementById("message__div").classList.add("block");
        let message = "Incorrect Email or Password";
        $("#message").html(message);

        document.getElementById("group__email").classList.add("form__group-incorrect");
        document.getElementById("group__email").classList.remove("form__group-correct");
        document.getElementById("group__password").classList.add("form__group-incorrect");
        document.getElementById("group__password").classList.remove("form__group-correct");
        document.querySelector(".form__group__input-error-message").classList.add("form__group__input-error-active");
      }
    });
  }
}

formulary.addEventListener("submit", (e) => {
  e.preventDefault();

  document.querySelector(".btn__log-in").classList.add("btn_none");
  document.querySelector(".div__btn").classList.add("spin");

  comprobeUser(true);
});
