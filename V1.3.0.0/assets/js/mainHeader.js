$(function() {

    var siteSticky = function() {
        $(".js-sticky-header").sticky({ topSpacing: 0 });
    };
    siteSticky();

    var siteMenuClone = function() {

        $('.js-clone-nav').each(function() {
            var $this = $(this);
            $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
        });


        setTimeout(function() {

            var counter = 0;
            $('.site-mobile-menu .has-children').each(function() {
                var $this = $(this);

                $this.prepend('<span class="arrow-collapse collapsed">');

                $this.find('.arrow-collapse').attr({
                    'data-toggle': 'collapse',
                    'data-target': '#collapseItem' + counter,
                });

                $this.find('> ul').attr({
                    'class': 'collapse',
                    'id': 'collapseItem' + counter,
                });

                counter++;

            });

        }, 1000);

        $('body').on('click', '.arrow-collapse', function(e) {
            var $this = $(this);
            if ($this.closest('li').find('.collapse').hasClass('show')) {
                $this.removeClass('active');
            } else {
                $this.addClass('active');
            }
            e.preventDefault();

        });

        $(window).resize(function() {
            var $this = $(this),
                w = $this.width();

            if (w > 768) {
                if ($('body').hasClass('offcanvas-menu')) {
                    $('body').removeClass('offcanvas-menu');
                }
            }
        })

        $('body').on('click', '.js-menu-toggle', function(e) {
            var $this = $(this);
            e.preventDefault();

            if ($('body').hasClass('offcanvas-menu')) {
                $('body').removeClass('offcanvas-menu');
                $this.removeClass('active');
            } else {
                $('body').addClass('offcanvas-menu');
                $this.addClass('active');
            }
        })

        // click outisde offcanvas
        $(document).mouseup(function(e) {
            var container = $(".site-mobile-menu");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('offcanvas-menu')) {
                    $('body').removeClass('offcanvas-menu');
                }
            }
        });
    };
    siteMenuClone();


    window.sr = ScrollReveal( /*{ reset: true }*/ );

    sr.reveal('.content_banner', {
        duration: 2000,
        origin: 'bottom',
        distance: '50px'
    });

    sr.reveal('.few__clicks_div', {
        duration: 2000,
        origin: 'left',
        distance: '50px'
    });

    sr.reveal('.better__experience_div', {
        duration: 2000,
        origin: 'bottom',
        distance: '50px'
    });

    sr.reveal('.save__time_div', {
        duration: 2000,
        origin: 'left',
        distance: '50px'
    });

    sr.reveal('.target_info', {
        duration: 2000,
        origin: 'left',
        distance: '50px'
    });

    sr.reveal('.download', {
        duration: 2000,
        origin: 'bottom',
        distance: '50px'
    });

    sr.reveal('.contact__title', {
        duration: 2000,
        origin: 'top',
        distance: '50px',
    });

    sr.reveal('.img__information', {
        duration: 2000,
        origin: 'left',
        distance: '50px',
        delay: 500
    });

    sr.reveal('.father_form', {
        duration: 2000
    });

    sr.reveal('.homestay', {
        duration: 2500,
        delay: 100
    });
    sr.reveal('.student', {
        duration: 2000
    });

});