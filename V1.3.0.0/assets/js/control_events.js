((D, W) => {
  //! VARIABLES

  //! FUNCTIONS

  class ControlEvents {
    constructor() {
      this.$typeUser = D.querySelector("#type_user").value;
    }

    async getEvents() {
      const eventsForm = new FormData();
      eventsForm.set("type-user", this.$typeUser);
      const DATA = { method: "POST", body: eventsForm };

      const jsonEventsForm = await fetch("../helpers/get-events.php", DATA);
      const resultEventsForm = await jsonEventsForm.json();
    }
  }

  //! EVENTS

  const INSTANCE = new ControlEvents();

  D.addEventListener("DOMContentLoaded", (e) => {
    INSTANCE.getEvents();
  });
})(document, window);
