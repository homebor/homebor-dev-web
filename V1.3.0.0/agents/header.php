<!-- WRAPPER - FONDO
    =================================================================================================================-->

<!--*********************************************************************************************************-->
<!--HEADER **************************************************************************************************-->
<!--*********************************************************************************************************-->
<header id="ts-header" class="fixed-top">
  <link rel="stylesheet" type="text/css" href="../assets/css/header.css">
  <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
  <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
    <div class="container">

      <!--Brand Logo/ Logo HOMEBOR-->
      <a class="navbar-brand" href="index.php">
        <img src="../assets/logos/page.png" alt="">
      </a>

      <?php include 'notification-after.php'; ?>

      <!--Responsive Collapse Button-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary"
        aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!--Collapsing Navigation-->
      <div class="collapse navbar-collapse" id="navbarPrimary">

        <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
        <ul class="navbar-nav">

          <!--HOME (Main level)
                        =============================================================================================-->
          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link active" href="index" id="active">
              Main
              <span class="sr-only">(current)</span>
            </a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- MAP (1st level)
                                =====================================================================================-->
              <li class="nav-item ts-has-child">

                <a href="#edit_agent" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Coordinator </p>
                </a>

                <!--List (2nd level) -->
                <ul class="ts-child">

                  <!-- OPENSTREETMAP (2nd level level)
                                        =============================================================================-->
                  <li class="nav-item">

                    <a href="edit_agent" class="nav-link pl-2 pr-2">
                      <p class="bar-iii zoom"> Edit Coordinator Info </p>
                    </a>

                  </li>
                  <!--end OpenStreetMap-->

                  <!-- MAPBOX (2nd level level)
                                        =============================================================================-->
                  <li class="nav-item">

                    <a href="agent_profile" class="nav-link pl-2 pr-2">
                      <p class="bar-iii zoom"> Coordinator Profile </p>
                    </a>

                  </li>

                  <!--<li class="nav-item">

                                            <a href="agent_delete" class="nav-link pl-2 pr-2">
                                                <p class="bar-iii zoom"> Disable Account </p>
                                            </a>

                                        </li> -->
                  <!--end MapBox-->

                </ul>

                <!--end SLIDER (1st level)-->

            </ul>
            <!--end List (1st level) -->

          </li>
          <!--end HOME nav-item-->

          <!--LISTING (Main level)
                        =============================================================================================-->
          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link" href="directory_homestay" id="nav">Homestays</a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- CATEGORY ICONS (1st level)
                                =====================================================================================-->
              <li class="nav-item">

                <a href="directory_homestay" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Homestays Directory </p>
                </a>
              </li>
              <!--end CATEGORY ICONS (1st level)-->

              <!-- GRID (1st level)
                                =====================================================================================-->
              <li class="nav-item">

                <a href="non_certified_hom" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Non-certified Homestays </p>
                </a>

              </li>
              <!--end GRID (1st level)-->

              <!-- GRID (1st level)
                                =====================================================================================-->
              <li class="nav-item">

                <a href="homestay" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Register New Homestay </p>
                </a>

              </li>
              <!--end GRID (1st level)-->

            </ul>
            <!--end List (1st level) -->

          </li>
          <!--end LISTING nav-item-->


          <!--PAGES (Main level)
                        =============================================================================================-->
          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link" href="directory_students" id="nav">Students</a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- AGENCY (1st level)
                                =====================================================================================-->
              <li class="nav-item">

                <a href="directory_students" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom">Students Directory</p>
                </a>

              </li>
              
              <li class="nav-item">

                <a href="directory_history" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom">Students History</p>
                </a>

              </li>


              <li class="nav-item">
                <a href="student_profile" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom"> Register a New Student </p>
                </a>
              </li>

            </ul>
            <!--end List (1st level) -->

          </li>




          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link" href="#" id="nav">Schools</a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- AGENCY (1st level)
              =====================================================================================-->
              <li class="nav-item">

                <a href="academy_register" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom">Register New School</p>
                </a>

              </li>
              <!--end AGENCY (1st level)-->

            </ul>
            <!--end List (1st level) -->

          </li>
          <!--end PAGES nav-item-->

          <!-- //TODO UTILITIES -->

          <li class="nav-item ts-has-child">

            <!--Main level link-->
            <a class="nav-link" href="#" id="nav">Utilities</a>

            <!-- List (1st level) -->
            <ul class="ts-child">

              <!-- AGENCY (1st level)
              =====================================================================================-->
              <li class="nav-item">

                <a href="../agents/reports" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom">Reports</p>
                </a>

              </li>
              <!--end AGENCY (1st level)-->
              <!-- AGENCY (1st level)
              =====================================================================================-->
              <li class="nav-item">

                <a href="../agents/list_delete_user" class="nav-link pl-2 pr-2">
                  <p class="bar-ii zoom">Users to Delete </p>
                </a>

              </li>
              <!--end AGENCY (1st level)-->

            </ul>
            <!--end List (1st level) -->

          </li>
          <!--end PAGES nav-item-->

          <!--end ABOUT US nav-item-->

          <?php include 'notification-before.php' ?>

        </ul>
        <!--end Left navigation main level-->

        <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
        <ul class="navbar-nav ml-auto">

          <!--LOGIN (Main level)
                        =============================================================================================-->
          <li class="nav-item">
            <a class="nav-link" href="../logout" id="nav">Logout</a>
          </li>
        </ul>
        <!--end Right navigation-->

      </div>
      <!--end navbar-collapse-->
    </div>
    <!--end container-->
  </nav>
  <!--end #ts-primary-navigation.navbar-->

  <input type="hidden" id="type_user" value="agent">

</header>

<!-- // ? AXIOS -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script src="../assets/js/control_events.js?ver=1.0"></script>
