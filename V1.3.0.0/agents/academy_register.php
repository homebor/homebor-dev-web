<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

// If exist a id from Student Register

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

    $query6 = $link->query("SELECT * FROM academy WHERE id_ac = '$id' ");
    $row6=$query6->fetch_assoc();

    $message = "The assigned School is not registered. You must register it to continue with the Student registration";

}


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario'");
$row7=$query7->fetch_assoc();


$sql = "SELECT * FROM `pe_student` WHERE id_m = '$row7[id_m]'"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus);

if ($row5['usert'] != 'Agent') {
    header("location: ../logout.php");   
}

if ($row7['plan'] == 'No Plan') {
         header("location: ../manager/work_with_us.php");   
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/academy_register.css">

  <title>School Register</title>
</head>

<body>

  <header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
    <?php include 'header.php' ?>

  </header>
  <!--end Header-->

  <div class="container_register">

    <h1 class="title__register">Register New School</h1>

    <main class="card__register">

      <?php if(isset($message) && !empty($message)){  

            echo "<h4 class='warning__text'> ".$message." </h4>";

        }else{} ?>

      <div class="card column1">

        <h3 class="form__group-title">Basic Information</h3>

        <form id="form__academy" action="action_academies.php" method="POST" enctype="multipart/form-data">

          <div class="row d-flex justify-content-center align-items-center mb-4 w-100 ml-0">

            <div class="col-md-5 pl-0 pr-4 pb-4 mt-auto mb-auto">

              <div class="form__group__div__photo mb-5">
                <img class="form__group__photo d-none" id="preview" src="" alt="">

                <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="photo_aca"
                  id="file" maxlength="1" accept="jpg|png|jpeg|pdf">


                <label for="file" class="photo-add" id="label-i">
                  <p class="form__group-l_title"> Add Profile Photo </p>
                </label>

                <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                  title="Change Frontage Photo"></label>
              </div>

              <div class="col-md-12 input__div form__group__name_a" id="group__name_a">
                <label for="name_a" class="laber__form">School Name</label>
                <label for="name_a" class="icon icon__n_a"></label>

                <?php if(empty($row6['name_a']) || $row6['name_a'] == 'NULL'){ ?>

                <input type="text" class="form_group-input" name="name_a" id="name_a" placeholder="School Name">

                <?php }else{ ?>

                <input type="text" class="form_group-input" name="name_a" id="name_a"
                  value="<?php echo $row6['name_a'] ?>">

                <?php } ?>

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>

              <div class="col-md-12 input__div form__group__name_a" id="group__name_a">
                <label for="acronym_s" class="laber__form">School Acronym</label>
                <label for="acronym_s" class="icon icon__n_a"></label>

                <input type="text" class="form_group-input" name="acronym_s" id="acronym_s"
                  placeholder="e.g. IACT or ELCI">

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>

            </div>

            <div class="col-md-5 pl-0 pb-4">


              <div class="col-md-12 input__div form__group__dir" id="group__dir">
                <label for="dir" class="laber__form">Address</label>
                <label for="dir" class="icon icon__dir"></label>
                <input type="text" class="form_group-input" name="dir" id="dir" placeholder="Av, Street, etc.">

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>


              <div class="col-md-12 input__div" id="group__state">
                <label for="" class="laber__form">Province / State</label>
                <label for="" class="icon icon__dir"></label>
                <input type="text" class="form_group-input" name="state" id="state" placeholder="e.g. Ontario">

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>


              <div class="col-md-12 input__div" id="group__city">
                <label for="" class="laber__form">City</label>
                <label for="" class="icon icon__dir"></label>
                <input type="text" class="form_group-input" name="city" id="city" placeholder="e.g. Toronto">

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>


              <div class="col-md-12 input__div" id="group__p_code">
                <label for="" class="laber__form">Postal Code</label>
                <label for="" class="icon icon__dir"></label>
                <input type="text" class="form_group-input" name="p_code" id="p_code" placeholder="e.g. M4R 2G8">

                <p class="form__group__input-error">Enter a valid Date</p>
              </div>

              <input type="hidden" value="<?php echo $id ?>" name="id_ac">


            </div>


          </div>

          <div class="div__btn__register">

            <button type="submit" class="btn btn__register">Register School</button>

          </div>
        </form>

      </div>


    </main>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>


  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
        document.getElementById("preview").classList.remove("d-none");
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    } else {
      document.getElementById("preview").classList.add("d-none");
    }

  }
  </script>

  <?php include 'footer.php' ?>
  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/validate_register_academies.js"></script>


  <!--Date Input Safari-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
  <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
  <script src="assets/js/date-safari.js"></script>

</body>

</html>