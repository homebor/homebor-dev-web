<?php
    require '../xeon.php';
    session_start();
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Agent') {
    header("location: ../logout.php"); 
}

?>

<!DOCTYPE html>
<html>
  <head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/edit_students2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/wconfirmation.css">

     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Administartor Panel</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
  </head>
 <body id='body'>

<!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <main id="ts-main">

        <!--PAGE TITLE
            =========================================================================================================-->


        <section id="page-title">
            <div class="container">
                <div class="row py-4">
                    <div class=" col-lg-10">
                        <div class="">
                                <br>
                                <h2 style="color: #232159">Students Awaiting Confirmation </h2>
    <!-- Echo username -->
    <?php
        $usuario = $_SESSION['username'];
    ?>
    <br>
    <?php  

                                        if (isset($_GET["fallo"]) && $_GET["fallo"] == 'false') {
                                        echo "<h5 id='good'><br><br>User Created Successfully</h5>";
                                        }

                                        if (isset($_GET["fallo"]) && $_GET["fallo"] == 'yes'){
                                            echo "<h5 id='bad'> The User Email Submitted is Already Registred";
                                        }

                                        if (isset($_GET["fallo"]) && $_GET["fallo"] == 'plan'){
                                            echo "<h5 id='bad'>The Number of Registered Coordinators was Exceeded, Check our Plans";
                                        }

                                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <hr>
        <div class="container col-lg-11">
            <br />
            <div class="card">
              <div class="card-body">
                <div class="form-group">
                  <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" />
                </div>
                <div class="table-responsive" id="dynamic_content">
                </div>

                <div id="table-voun" class="table-responsive text-center">
                    <div class="text-center">
                        <table class="table table-hover table-striped" id="vou">
                            <thead style="background-color: #232159; color: white">
                                <tr>
                                    <th class="text-center align-middle photo_s">Photo Student</th>
                                    <th class="text-center align-middle student_n">Student Name</th>
                                    <th class="text-center align-middle gend_s">Gender</th>
                                    <th class="text-center align-middle mail_s">Mail</th>
                                    <th class="text-center align-middle back_s">Background</th>
                                    <th class="text-center align-middle situ_s">Situation</th>
                                    <th class="text-center align-middle date_s">Date Arrive / Last Date</th>
                                    <th class="text-center align-middle action_s"></th>

                                </tr>
                            </thead>

                            <tbody class="table-bordered" id="vouchers"></tbody>

                        </table>
                    </div>
          </div>
              </div>
            </div>
          </div>

  </section>
</main>



<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="assets/js/wconfirm_student.js"></script>

</body>
</html>
