(function (D, W) {
  // VARIABLES
  const SEARCH_ACTIONS = {
    homestays: D.getElementById("search-homestays"),
    students: D.getElementById("search-students"),
    list: D.getElementById("order-list"),
    cataloge: D.getElementById("order-cataloge"),
    detailLeft: D.getElementById("detail-left"),
    detailRight: D.getElementById("detail-right"),
  };
  const LIST = {
    homestays: D.getElementById("all-properties"),
    students: D.getElementById("all-students"),
  };

  // FUNCIONES
  function detailBtn(btn) {
    // WATCH VARIABLES
    const homestayWidth = SEARCH_ACTIONS.homestays.getBoundingClientRect().width;
    const studentsWidth = SEARCH_ACTIONS.students.getBoundingClientRect().width;
    const detailLeft = SEARCH_ACTIONS.detailLeft;
    // ORDER AS VARIABLES
    const listWidth = SEARCH_ACTIONS.list.getBoundingClientRect().width;
    const catalogeWidth = SEARCH_ACTIONS.cataloge.getBoundingClientRect().width;
    const detailRight = SEARCH_ACTIONS.detailRight;

    // WATCH
    if (btn.id === SEARCH_ACTIONS.homestays.id) {
      detailLeft.style.width = homestayWidth + "px";
      detailLeft.style.left = "0";

      LIST.students.classList.remove("show", "show__cataloge");
      if (SEARCH_ACTIONS.cataloge.classList.contains("active")) return LIST.homestays.classList.add("show__cataloge");
      else return LIST.homestays.classList.add("show");
    }
    if (btn.id === SEARCH_ACTIONS.students.id) {
      detailLeft.style.width = studentsWidth + "px";
      detailLeft.style.left = homestayWidth + "px";

      LIST.homestays.classList.remove("show", "show__cataloge");
      if (SEARCH_ACTIONS.cataloge.classList.contains("active")) return LIST.students.classList.add("show__cataloge");
      else return LIST.students.classList.add("show");
    }

    // ORDER AS
    if (btn.id === SEARCH_ACTIONS.list.id) {
      detailRight.style.width = listWidth + "px";
      detailRight.style.right = "0";

      if (SEARCH_ACTIONS.homestays.classList.contains("active"))
        return LIST.homestays.classList.replace("show__cataloge", "show");
      if (SEARCH_ACTIONS.students.classList.contains("active"))
        return LIST.students.classList.replace("show__cataloge", "show");
    }
    if (btn.id === SEARCH_ACTIONS.cataloge.id) {
      detailRight.style.width = catalogeWidth + "px";
      detailRight.style.right = listWidth + "px";

      if (SEARCH_ACTIONS.homestays.classList.contains("active"))
        return LIST.homestays.classList.replace("show", "show__cataloge");
      if (SEARCH_ACTIONS.students.classList.contains("active"))
        return LIST.students.classList.replace("show", "show__cataloge");
    }
  }

  function selectBtn(btn = "", oldBtn = "") {
    if (!btn || !oldBtn) return console.error("No hay datos!");

    if (btn.classList.contains("active")) return false;
    else {
      oldBtn.classList.remove("active");
      btn.classList.add("active");
      return detailBtn(btn);
    }
  }

  // EVENTOS
  D.addEventListener("DOMContentLoaded", (e) => {
    SEARCH_ACTIONS.homestays.classList.add("active");
    SEARCH_ACTIONS.list.classList.add("active");
    detailBtn(SEARCH_ACTIONS.homestays);
    detailBtn(SEARCH_ACTIONS.list);
  });

  D.addEventListener("click", (e) => {
    // WATCH EVENTS
    if (e.target.id === SEARCH_ACTIONS.homestays.id) selectBtn(e.target, SEARCH_ACTIONS.students);
    if (e.target.id === SEARCH_ACTIONS.students.id) selectBtn(e.target, SEARCH_ACTIONS.homestays);

    // ORDER AS EVENTS
    if (e.target.id === SEARCH_ACTIONS.list.id) selectBtn(e.target, SEARCH_ACTIONS.cataloge);
    if (e.target.id === SEARCH_ACTIONS.cataloge.id) selectBtn(e.target, SEARCH_ACTIONS.list);
  });
})(document, window);
