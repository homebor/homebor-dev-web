((D, W) => {
  // TODO VARIABLES

  // ? PENDING RESERVATION

  const templateListPendingReservation = D.getElementById("pending-reservations").content.cloneNode(true);

  const formularyPendindReservation = D.getElementById("pendig-reservations-parent");

  const $listPendingReservation = templateListPendingReservation.querySelector("#reservation-pending"),
    $titleRoom = templateListPendingReservation.querySelector("#title__reservation-pending"),
    $aLinkProfile = templateListPendingReservation.querySelector("#link__profile-pending"),
    $nameStudent = templateListPendingReservation.querySelector("#name__student-pending"),
    $divPendingReservationDetails = templateListPendingReservation.querySelector("#div__active__res-pending"),
    $divPendingReservationDetailsVerticalAlign =
      templateListPendingReservation.querySelector("#date__reservation-pending"),
    $divImageStudent = templateListPendingReservation.querySelector("#div__img-pending"),
    $imageStudent = templateListPendingReservation.querySelector("#student-img-pending"),
    $divArriveDate = templateListPendingReservation.querySelector("#arrive_date-pending"),
    $arriveDateTitle = templateListPendingReservation.querySelector("#arrive-title-pending"),
    $arriveDateContent = templateListPendingReservation.querySelector("#arrive-content-pending"),
    $divLeaveDate = templateListPendingReservation.querySelector("#leave_date-pending"),
    $leaveDateTitle = templateListPendingReservation.querySelector("#leave-title-pending"),
    $leaveDateContent = templateListPendingReservation.querySelector("#leave-content-pending"),
    $divButtonsReservation = templateListPendingReservation.querySelector(".buttons__confirm"),
    $inputIdStudent = templateListPendingReservation.querySelector("#id_student"),
    $inputIdNot = templateListPendingReservation.querySelector("#id_not"),
    $cancelReservationButton = templateListPendingReservation.querySelector(".btn__cancel-reservation"),
    $confirmReservationButton = templateListPendingReservation.querySelector(".btn__confirm-reservation");

  const $articleReservation = D.getElementById("article-pending-reservation");

  // ? ACTIVE RESERVATION

  const templateActiveReservation = D.getElementById("active-reservations").content.cloneNode(true);

  const $listActiveReservation = templateActiveReservation.querySelector("#reservation-active"),
    $titleRoomActive = templateActiveReservation.querySelector("#title__reservation-active"),
    $aLinkProfileActive = templateActiveReservation.querySelector("#link__profile-active"),
    $nameStudentActive = templateActiveReservation.querySelector("#name__student-active"),
    $divActiveReservationDetails = templateActiveReservation.querySelector("#div__active__res-active"),
    $divActiveReservationDetailsVerticalAlign = templateActiveReservation.querySelector("#date__reservation-active"),
    $divImageStudentActive = templateActiveReservation.querySelector("#div__img-active"),
    $imageStudentActive = templateActiveReservation.querySelector("#student-img-active"),
    $divArriveDateActive = templateActiveReservation.querySelector("#arrive_date-active"),
    $arriveDateTitleActive = templateActiveReservation.querySelector("#arrive-title-active"),
    $arriveDateContentActive = templateActiveReservation.querySelector("#arrive-content-active"),
    $divLeaveDateActive = templateActiveReservation.querySelector("#leave_date-active"),
    $leaveDateTitleActive = templateActiveReservation.querySelector("#leave-title-active"),
    $leaveDateContentActive = templateActiveReservation.querySelector("#leave-content-active"),
    $divButtonDownloadPdf = templateActiveReservation.querySelector("#div-button-download-pdf"),
    $linkDownloadPdf = templateActiveReservation.querySelector("#link-button-pdf"),
    $spanArrowPdf = templateActiveReservation.querySelector("#span-arrow-download"),
    $divButtonReport = templateActiveReservation.querySelector("#div-button-open-report"),
    $linkOpenReport = templateActiveReservation.querySelector("#button-open-report"),
    $spanArrowReport = templateActiveReservation.querySelector("#span-arrow-download");

  // TODO VARIABLES TO SHOW THE MODAL RESERVATIONS
  /* const divButtonReservation = D.querySelector("#reserves-button");
  const btnAddReservation = D.querySelector("#btn__add-reserve"); */
  const btnCloseModal = D.querySelectorAll("#btn__close-modal");
  const modalReservation = D.getElementById("modal__reservation");

  // TODO SEARCH INPUT

  const inputSearch = D.getElementById("search__input");

  // TODO RESERVATION FORMULARY

  const formReservation = D.getElementById("form__reservation");
  const btnSendReservation = D.getElementById("btn__submit-reservation");
  btnSendReservation.disabled = true;

  // TODO VARIABLES TO SHOW THE STUDENT listStyle:

  const templateListStudent = D.getElementById("student__list-ul").content.cloneNode(true);

  // TODO VARIABLES FAMILY MEMBER

  const templateFamilyMember = D.querySelector("#template-member-family").content.cloneNode(true);

  const $templateBedrooms = D.querySelector("#template-bedrooms").content.cloneNode(true);
  const $bedTemplate = D.getElementById("bed-template").content.cloneNode(true);
  const $divParentBedrooms = D.querySelector("#parent-room-choose");

  // TODO OPEN REPORT

  const modalReport = D.querySelector("#modal-add-report");

  // ? OPEN CLOSE REPORT

  const $buttonCloseModalReport = D.querySelector("#btn__close-modal-report");

  // ? FORMULARY: SENT REPORT

  const formSendReport = D.querySelector("#form-send-report");

  const HOMESTAY_DETAILS = {
    // ? DESCRIPTION HOUSE

    $descriptionHome: D.getElementById("description-house-content"),

    // ? HOMESTAY
    _ID: D.getElementById("homestay-id").value,
    $name: D.getElementById("homestay-name"),
    $address: D.getElementById("homestay-address"),
    detailsHome: {
      $email: D.getElementById("email-home"),
      $fullName: D.getElementById("fullname-home"),
      $dateOfBirth: D.getElementById("db-property-home"),
      $gender: D.getElementById("gender-home"),
      $pnumber: D.getElementById("pnumber-home"),
      $occupation: D.getElementById("occupation-home"),
      $dbCheck: D.getElementById("db-check-home"),
      /* $status: D.getElementById("status-home"), */
    },
    $addressHome: D.getElementById("address-home"),
    $city: D.getElementById("city-home"),
    $state: D.getElementById("state-home"),
    $postalCode: D.getElementById("pcode-home"),

    // ? QUICK INFO

    $rooms: D.getElementById("room-home"),
    $pets: D.getElementById("pet-home"),
    $yearsExperience: D.getElementById("y-experience-home"),
    $backgroundLanguage: D.getElementById("backl-home"),
    $agePreference: D.getElementById("a-preference-home"),
    $genderPreference: D.getElementById("genpre-home"),
    $foodService: D.getElementById("foodservice-home"),
    $specialDiet: D.getElementById("diet-home"),

    // ? COORDINATOR
    $logo: D.getElementById("agency-link"),
    $agencyName: D.getElementById("agency-name"),
    $agentName: D.getElementById("agent-name"),
    $agencyMail: D.getElementById("agency-mail"),
    $agencyNumber: D.getElementById("agency-number"),
    $agencyCoordinator: D.getElementById("agency-type"),
    $studentName: D.getElementById("name"),
    $studentMail: D.getElementById("email"),
    $studentMessage: D.getElementById("form-contact-message"),
    // ? HOMESTAY ROOMS
    $allReserves: D.getElementById("all-reserves"),
    $reserveBtn: D.getElementById("reserve-btn"),

    // ? ADDITIONAL INFORMATION
    $background: D.getElementById("background-home"),
    $religion: D.getElementById("religion-home"),
    $misdemeanor: D.getElementById("misdemeanor-home"),
    $academyPreference: D.getElementById("academy-home"),
    $backgroundCheck: D.getElementById("backgroundCheck-home"),

    // ? HEALTH INFORMATION
    $smokePolitics: D.getElementById("smoker-politics-home"),
    $allergies: D.getElementById("allergies-home"),
    $medication: D.getElementById("medication-home"),
    $pmcondition: D.getElementById("pmcondition-home"),
    $healthProblems: D.getElementById("health-problems-home"),

    // ? FAMILY MEMBER
    $allMembers: D.querySelector("#family-members-parent"),
  };

  //? ACTIVITY LOGS
  /* const $templateActivityLogs = D.getElementById("tbody-log-template").content.cloneNode(true); */

  // ! WINYERSON
  const $modal = D.getElementById("modal-zoom-image");

  // TODO FUNCIONES

  class DetailHomestay {
    constructor(HOMESTAY = HOMESTAY_DETAILS) {
      // ? DESCRIPTION

      this.$descriptionHome = HOMESTAY.$descriptionHome;

      // ? HOMESTAY
      this.ID = HOMESTAY._ID;
      this.$name = HOMESTAY.$name;
      this.$address = HOMESTAY.$address;
      this.details = HOMESTAY.detailsHome;
      this.$description = HOMESTAY.$description;
      /* this.$academyName = HOMESTAY.$academyName; */
      this.$genderPreference = HOMESTAY.$genderPreference;
      this.$agePreference = HOMESTAY.$agePreference;
      this.$addressHome = HOMESTAY.$addressHome;
      this.$city = HOMESTAY.$city;
      this.$state = HOMESTAY.$state;
      this.$postalCode = HOMESTAY.$postalCode;

      // ? QUICK INFO
      this.$rooms = HOMESTAY.$rooms;
      this.$pets = HOMESTAY.$pets;
      this.$yearsExperience = HOMESTAY.$yearsExperience;
      this.$backgroundLanguage = HOMESTAY.$backgroundLanguage;
      this.$foodService = HOMESTAY.$foodService;
      this.$specialDiet = HOMESTAY.$specialDiet;

      // ? PENDING RESERVATION
      this.$listPendingReservation = $listPendingReservation;
      this.$titleRoom = $titleRoom;
      this.$aLinkProfile = $aLinkProfile;
      this.$nameStudent = $nameStudent;
      this.$divPendingReservationDetails = $divPendingReservationDetails;
      this.$divPendingReservationDetailsVerticalAlign = $divPendingReservationDetailsVerticalAlign;
      this.$divImageStudent = $divImageStudent;
      this.$imageStudent = $imageStudent;
      this.$divArriveDate = $divArriveDate;
      this.$arriveDateTitle = $arriveDateTitle;
      this.$arriveDateContent = $arriveDateContent;
      this.$divLeaveDate = $divLeaveDate;
      this.$leaveDateTitle = $leaveDateTitle;
      this.$leaveDateContent = $leaveDateContent;
      this.$divButtonsReservation = $divButtonsReservation;
      this.$inputIdStudent = $inputIdStudent;
      this.$inputIdNot = $inputIdNot;
      this.$cancelReservationButton = $cancelReservationButton;
      this.$confirmReservationButton = $confirmReservationButton;
      this.$articleReservation = $articleReservation;

      // ? ACTIVE RESERVATION
      this.$listActiveReservation = $listActiveReservation;
      this.$titleRoomActive = $titleRoomActive;
      this.$aLinkProfileActive = $aLinkProfileActive;
      this.$nameStudentActive = $nameStudentActive;
      this.$divActiveReservationDetails = $divActiveReservationDetails;
      this.$divActiveReservationDetailsVerticalAlign = $divActiveReservationDetailsVerticalAlign;
      this.$divImageStudentActive = $divImageStudentActive;
      this.$imageStudentActive = $imageStudentActive;
      this.$divArriveDateActive = $divArriveDateActive;
      this.$arriveDateTitleActive = $arriveDateTitleActive;
      this.$arriveDateContentActive = $arriveDateContentActive;
      this.$divLeaveDateActive = $divLeaveDateActive;
      this.$leaveDateTitleActive = $leaveDateTitleActive;
      this.$leaveDateContentActive = $leaveDateContentActive;
      this.$divButtonDownloadPdf = $divButtonDownloadPdf;
      this.$linkDownloadPdf = $linkDownloadPdf;
      this.$spanArrowPdf = $spanArrowPdf;
      this.$divButtonReport = $divButtonReport;
      this.$linkOpenReport = $linkOpenReport;
      this.$spanArrowReport = $spanArrowReport;

      // ? COORDINATOR
      this.$logo = HOMESTAY.$logo;
      this.$agencyName = HOMESTAY.$agencyName;
      this.$agentName = HOMESTAY.$agentName;
      this.$agencyMail = HOMESTAY.$agencyMail;
      this.$agencyNumber = HOMESTAY.$agencyNumber;
      this.$agencyCoordinator = HOMESTAY.$agencyCoordinator;
      this.$studentName = HOMESTAY.$studentName;
      this.$studentMail = HOMESTAY.$studentMail;
      this.$studentMessage = HOMESTAY.$studentMessage;
      // ? HOMESTAY ROOMS
      this.$reserveTemplate = HOMESTAY.$reserveTemplate;
      this.$allReserves = HOMESTAY.$allReserves;
      this.reserves = [];
      this.roomIsAvailable;
      this.roomsAvailables = {};

      // ? ADD RESERVATION

      /* this.$divButtonReservation = divButtonReservation;
      this.$modalReservationBtn = btnAddReservation;
      this.$modalReservation = modalReservation;
      this.$modalCloseBtn = btnCloseModal; */
      this.$templateListStudent = templateListStudent;

      this.$allList = undefined;
      this.$parentListStudent = D.querySelector("#list__student");
      this.nameStudentArray = [];

      // ? ADDITIONAL INFORMATION

      this.$background = HOMESTAY.$background;
      this.$religion = HOMESTAY.$religion;
      this.$misdemeanor = HOMESTAY.$misdemeanor;
      this.$academyPreference = HOMESTAY.$academyPreference;
      this.$backgroundCheck = HOMESTAY.$backgroundCheck;

      // ? HEALTH INFORMATION

      this.$smokePolitics = HOMESTAY.$smokePolitics;
      this.$allergies = HOMESTAY.$allergies;
      this.$medication = HOMESTAY.$medication;
      this.$pmcondition = HOMESTAY.$pmcondition;
      this.$healthProblems = HOMESTAY.$healthProblems;

      // ? UTILITIES
      this.DATA = new FormData();

      // ? FAMILY MEMBER
      this.$templateFamilyMember = templateFamilyMember;
      this.$allMembers = HOMESTAY.$allMembers;

      this.$templateBedrooms = $templateBedrooms;
      this.$bedTemplate = $bedTemplate;
      this.$divParentBedrooms = $divParentBedrooms;
    }

    // ! REQUERIR RELLENAR DETALLES DE LA CASA
    async homestayDetails() {
      // * OBTENIENDO DATOS
      this.DATA.set("homestay_id", this.ID);
      this.DATA.set("request", "homestayDetails");

      const OPTIONS = { method: "POST", body: this.DATA };
      const REQUEST = await fetch("detail_data.php", OPTIONS);
      const RESPONSE = await REQUEST.json();

      // * RELLENANDO TITLE
      const $addressIcon = '<i class="fa fa-map-marker text-primary"></i>';
      this.$name.textContent = RESPONSE.name;
      this.$address.innerHTML = $addressIcon + " " + RESPONSE.address;

      // * DESCRIPTION HOMESTAY

      this.$descriptionHome.textContent = RESPONSE.description_home;

      // * RELLENANDO DETAILS
      this.details.$email.textContent = RESPONSE.mail_h;
      this.details.$fullName.textContent = RESPONSE.fullname;
      this.details.$dateOfBirth.textContent = RESPONSE.db_main;
      this.details.$gender.textContent = RESPONSE.gender;
      this.details.$pnumber.textContent = RESPONSE.pnumber;
      this.details.$occupation.textContent = RESPONSE.occupation;
      this.details.$dbCheck.textContent = RESPONSE.db_law;
      /* this.details.$status.textContent = RESPONSE.status; */
      /* this.details.$smokePolitics.textContent = RESPONSE.smokePolitics; */

      // * RELLENANDO QUICK INFO

      this.$rooms.textContent = RESPONSE.rooms;
      this.$pets.textContent = RESPONSE.pets;
      this.$yearsExperience.textContent = RESPONSE.years_experience;
      this.$backgroundLanguage.textContent = RESPONSE.background_language;
      this.$agePreference.textContent = RESPONSE.agePreference;
      this.$genderPreference.textContent = RESPONSE.genderPreference;
      this.$foodService.textContent = RESPONSE.meal_service;
      this.$specialDiet.textContent = RESPONSE.special_diet;

      // * RELLENANDO PETS INFORMATION

      // * RELLENANDO LOCATION
      this.$addressHome.textContent = RESPONSE.address;
      this.$city.textContent = RESPONSE.city;
      this.$state.textContent = RESPONSE.state;
      this.$postalCode.textContent = RESPONSE.postal_code;

      // * ADDITIONAL INFORMATION

      this.$background.textContent = RESPONSE.background;
      this.$religion.textContent = RESPONSE.religion;
      this.$misdemeanor.textContent = RESPONSE.misdemeanor;
      this.$academyPreference.textContent = RESPONSE.academyName;
      this.$backgroundCheck.textContent = RESPONSE.c_background;

      // * HEALTH INFORMATION

      this.$smokePolitics.textContent = RESPONSE.smokePolitics;
      this.$allergies.textContent = RESPONSE.allergies;
      this.$medication.textContent = RESPONSE.medication;
      this.$pmcondition.textContent = RESPONSE.pmcondition;
      this.$healthProblems.textContent = RESPONSE.healthProblems;
    }

    async homestayPendingReservation() {
      this.DATA.set("homestay_id", this.ID);
      this.DATA.set("request", "homestayPendingReservation");

      const OPTIONS = { method: "POST", body: this.DATA };
      const REQUEST = await fetch("detail_data.php", OPTIONS);
      const RESPONSE = await REQUEST.json();

      // * FRAGMENT HTML
      const $fragment = D.createDocumentFragment();
      const $articlePendingReservation = D.getElementById("pendig-reservations-parent");

      RESPONSE.forEach((student) => {
        const h3PendingReservation = D.getElementById("h-pending-reservation");

        if (student.id_student === "Empty") {
          h3PendingReservation.classList.add("d-none");
          this.$articleReservation.classList.add("d-none");
        } else {
          h3PendingReservation.classList.remove("d-none");
          this.$articleReservation.classList.remove("d-none");

          // TODO CLONE VARIABLES
          const $articleReservation = this.$articleReservation.cloneNode();
          const $listPendingReservation = this.$listPendingReservation.cloneNode();
          const $titleRoom = this.$titleRoom.cloneNode();
          const $aLinkProfile = this.$aLinkProfile.cloneNode();
          const $nameStudent = this.$nameStudent.cloneNode();
          const $divPendingReservationDetails = this.$divPendingReservationDetails.cloneNode();
          const $divPendingReservationDetailsVerticalAlign =
            this.$divPendingReservationDetailsVerticalAlign.cloneNode();
          const $divImageStudent = this.$divImageStudent.cloneNode();
          const $imageStudent = this.$imageStudent.cloneNode();
          const $divArriveDate = this.$divArriveDate.cloneNode();
          const $arriveDateTitle = this.$arriveDateTitle.cloneNode();
          const $arriveDateContent = this.$arriveDateContent.cloneNode();
          const $divLeaveDate = this.$divLeaveDate.cloneNode();
          const $leaveDateTitle = this.$leaveDateTitle.cloneNode();
          const $leaveDateContent = this.$leaveDateContent.cloneNode();
          const $divButtonsReservation = this.$divButtonsReservation.cloneNode();
          const $inputIdStudent = this.$inputIdStudent.cloneNode();
          const $inputIdNot = this.$inputIdNot.cloneNode();
          const $cancelReservationButton = this.$cancelReservationButton.cloneNode();
          const $confirmReservationButton = this.$confirmReservationButton.cloneNode();

          // TODO ASIGN VALUES TO THE VARIABLES

          $articleReservation.dataset.idNoti = student.id_not;
          $titleRoom.textContent = "Room " + student.room;
          $aLinkProfile.href = "../agents/student_info?art_id=" + student.id_student;
          $nameStudent.textContent = student.fullname;
          $imageStudent.src = "../" + student.photo_s;
          $arriveDateTitle.textContent = "Arrive";
          $arriveDateContent.textContent = student.start;
          $leaveDateTitle.textContent = "Leave";
          $leaveDateContent.textContent = student.end;
          $inputIdStudent.value = student.id_student;
          $inputIdNot.value = student.id_not;
          $cancelReservationButton.textContent = "Cancel";
          $confirmReservationButton.textContent = "Confirm";
          $divButtonsReservation.dataset.idNoti = student.id_not;
          $cancelReservationButton.dataset.idNoti = student.id_not;
          $confirmReservationButton.dataset.idNoti = student.id_not;

          // TODO ASSIGN CHILDREN

          $listPendingReservation.appendChild($titleRoom);
          $listPendingReservation.appendChild($aLinkProfile);
          $listPendingReservation.appendChild($divPendingReservationDetails);
          $listPendingReservation.appendChild($inputIdStudent);
          $listPendingReservation.appendChild($inputIdNot);
          $aLinkProfile.appendChild($nameStudent);
          $divPendingReservationDetails.appendChild($divPendingReservationDetailsVerticalAlign);
          $divPendingReservationDetails.appendChild($divButtonsReservation);
          $divPendingReservationDetailsVerticalAlign.appendChild($divImageStudent);
          $divPendingReservationDetailsVerticalAlign.appendChild($divArriveDate);
          $divPendingReservationDetailsVerticalAlign.appendChild($divLeaveDate);
          $divImageStudent.appendChild($imageStudent);
          $divArriveDate.appendChild($arriveDateTitle);
          $divArriveDate.appendChild($arriveDateContent);
          $divLeaveDate.appendChild($leaveDateTitle);
          $divLeaveDate.appendChild($leaveDateContent);
          $divButtonsReservation.appendChild($cancelReservationButton);
          $divButtonsReservation.appendChild($confirmReservationButton);

          $fragment.appendChild($listPendingReservation);
        }
      });

      $articlePendingReservation.appendChild($fragment);
    }

    async confirmReservation(buttonContent, button, dataSet) {
      const formPendingReservation = new FormData(formularyPendindReservation);
      formPendingReservation.set("homestay_id", this.ID);
      formPendingReservation.set("request", "confirmReservation");
      formPendingReservation.set("button", buttonContent);
      formPendingReservation.set("id_noti", dataSet);

      const DATA = { method: "POST", body: formPendingReservation };
      const jsonPendingReservation = await fetch("detail_data.php", DATA);
      const pendingReservationQuery = await jsonPendingReservation.json();

      pendingReservationQuery.forEach((notification) => {
        if (notification.num_notification > "0") {
          if (notification.id_noti === button.dataset.idNoti) {
            setTimeout(() => {
              $(button.parentNode.parentNode.parentNode).fadeOut("slow");
            }, 1000);
          }
        } else {
          setTimeout(() => {
            $("#h-pending-reservation").fadeOut("slow");
            $("#article-pending-reservation").fadeOut("slow");
          }, 1000);
        }
      });
    }

    async homestayActiveReservation() {
      this.DATA.set("homestay_id", this.ID);
      this.DATA.set("request", "homestayActiveReservation");

      const OPTIONS = { method: "POST", body: this.DATA };
      const REQUEST = await fetch("detail_data.php", OPTIONS);
      const RESPONSE = await REQUEST.json();

      // * FRAGMENT HTML
      const $fragment = D.createDocumentFragment();
      const $articleActiveReservation = D.getElementById("active-reservations-parent");
      const $h3ActiveReservation = D.getElementById("a-active-reservation");

      RESPONSE.forEach((student) => {
        if (student.id_student === "Empty") {
          $h3ActiveReservation.classList.add("d-none");
          $articleActiveReservation.classList.add("d-none");
        } else {
          $h3ActiveReservation.classList.remove("d-none");
          $articleActiveReservation.classList.remove("d-none");
          // TODO CLONE VARIABLES
          const $listActiveReservation = this.$listActiveReservation.cloneNode();
          const $titleRoomActive = this.$titleRoomActive.cloneNode();
          const $aLinkProfileActive = this.$aLinkProfileActive.cloneNode();
          const $nameStudentActive = this.$nameStudentActive.cloneNode();
          const $divActiveReservationDetails = this.$divActiveReservationDetails.cloneNode();
          const $divActiveReservationDetailsVerticalAlign = this.$divActiveReservationDetailsVerticalAlign.cloneNode();
          const $divImageStudentActive = this.$divImageStudentActive.cloneNode();
          const $imageStudentActive = this.$imageStudentActive.cloneNode();
          const $divArriveDateActive = this.$divArriveDateActive.cloneNode();
          const $arriveDateTitleActive = this.$arriveDateTitleActive.cloneNode();
          const $arriveDateContentActive = this.$arriveDateContentActive.cloneNode();
          const $divLeaveDateActive = this.$divLeaveDateActive.cloneNode();
          const $leaveDateTitleActive = this.$leaveDateTitleActive.cloneNode();
          const $leaveDateContentActive = this.$leaveDateContentActive.cloneNode();
          const $divButtonDownloadPdf = this.$divButtonDownloadPdf.cloneNode();
          const $linkDownloadPdf = this.$linkDownloadPdf.cloneNode();
          const $spanArrowPdf = this.$spanArrowPdf.cloneNode();
          const $divButtonReport = this.$divButtonReport.cloneNode();
          const $linkOpenReport = this.$linkOpenReport.cloneNode();
          const $spanArrowReport = this.$spanArrowReport.cloneNode();

          // TODO ASIGN VALUES TO THE VARIABLES

          $titleRoomActive.innerHTML = "Room " + student.room + " - Bed " + student.bed;
          $aLinkProfileActive.href = "../agents/student_info?art_id=" + student.id_student;
          $nameStudentActive.textContent = student.fullname;
          if (student.photo_s === "NULL") $imageStudentActive.src = "../assets/emptys/profile-student-empty.png";
          else $imageStudentActive.src = "../" + student.photo_s;
          $arriveDateTitleActive.textContent = "Arrive";
          $arriveDateContentActive.textContent = student.start;
          $leaveDateTitleActive.textContent = "Leave";
          $leaveDateContentActive.textContent = student.end;
          $linkDownloadPdf.textContent = "Download Student Information";
          $linkDownloadPdf.href = "../agents/vouches/studentinfo.php?art_id=" + student.id_student;
          $linkOpenReport.textContent = "Report Student";

          // TODO ASSIGN CHILDREN

          $listActiveReservation.appendChild($titleRoomActive);
          $listActiveReservation.appendChild($aLinkProfileActive);
          $listActiveReservation.appendChild($divActiveReservationDetails);
          $aLinkProfileActive.appendChild($nameStudentActive);
          $divActiveReservationDetails.appendChild($divActiveReservationDetailsVerticalAlign);
          $divActiveReservationDetails.appendChild($divButtonDownloadPdf);
          $divActiveReservationDetails.appendChild($divButtonReport);
          $divActiveReservationDetailsVerticalAlign.appendChild($divImageStudentActive);
          $divActiveReservationDetailsVerticalAlign.appendChild($divArriveDateActive);
          $divActiveReservationDetailsVerticalAlign.appendChild($divLeaveDateActive);
          $divButtonDownloadPdf.appendChild($linkDownloadPdf);
          $linkDownloadPdf.appendChild($spanArrowPdf);
          $divButtonReport.appendChild($linkOpenReport);
          $linkOpenReport.appendChild($spanArrowReport);
          $divImageStudentActive.appendChild($imageStudentActive);
          $divArriveDateActive.appendChild($arriveDateTitleActive);
          $divArriveDateActive.appendChild($arriveDateContentActive);
          $divLeaveDateActive.appendChild($leaveDateTitleActive);
          $divLeaveDateActive.appendChild($leaveDateContentActive);

          const functionArrecghisima = (btn, action) => {
            if (action === "over") {
              btn.querySelector(`.arrow__download`).classList.replace("arrow__download", "arrow__download-active");
            } else {
              btn
                .querySelector(`.arrow__download-active`)
                .classList.replace("arrow__download-active", "arrow__download");
            }
          };

          $listActiveReservation.addEventListener("mouseover", (e) => {
            if (e.target.matches(".buttons__link")) functionArrecghisima(e.target, "over");
          });

          $listActiveReservation.addEventListener("mouseout", (e) => {
            if (e.target.matches(".buttons__link")) functionArrecghisima(e.target);
          });

          $linkOpenReport.addEventListener("click", (e) => {
            e.preventDefault();
            this.openModalReport(student.id_student, student.fullname);
          });

          $fragment.appendChild($listActiveReservation);
        }
      });

      $articleActiveReservation.appendChild($fragment);
    }

    // TODO FUNCTION LIST ROOM HOUSE

    async roomsHouse(idHome) {
      const formDataHomestay = new FormData();
      /* formDataHomestay.set("id_student", idStudent);*/
      formDataHomestay.set("homestay_id", idHome);
      /* formDataHomestay.set("request", "roomsHouse"); */
      const DATA = { method: "POST", body: formDataHomestay };

      const jsonRoomsHouse = await fetch("../helpers/homestay/rooms_data.php", DATA);
      const roomsHouse = await jsonRoomsHouse.json();
      const $fragment = D.createDocumentFragment();

      for (const key in roomsHouse) {
        const ROOM = roomsHouse[key];

        // ! VALIDAR A QUE SI TODOS LOS DATE ESTAN EN DISABLED NO MOSTRAR
        let valid = true;

        for (const key in ROOM) {
          if (key !== "roomFeatures") continue;
          if (
            !ROOM[key].type ||
            ROOM[key].type === "Disabled" ||
            ROOM[key].type === "NULL" ||
            ROOM[key].type === "null"
          )
            valid = false;
        }

        if (!valid) continue;

        // ** TEMPLATES
        const $listRoom = this.$templateBedrooms.firstElementChild.cloneNode(true);
        const $newBed = this.$bedTemplate.cloneNode(true);

        // ** RELLENANDO HEADER
        this.roomHeader(ROOM.roomTitle, $listRoom);

        // ** RELLENANDO PHOTO
        const image1 = ROOM.roomImage.image1;
        const image2 = ROOM.roomImage.image2;
        const image3 = ROOM.roomImage.image3;

        if ((!image2 || image2 === "NULL") && (!image3 || image3 === "NULL"))
          this.roomPhoto(false, [image1], $listRoom);
        else this.roomPhoto(ROOM, [image1, image2, image3], $listRoom);

        // ** RELLENANDO FEATURES
        this.roomFeatures(ROOM.roomFeatures, $listRoom, ROOM.id_home);

        // ** RELLENAR DETALLES DE CAMAS
        this.roomBedsInfo(ROOM.roomReservesInfo, ROOM.roomTitle.color, $newBed, $listRoom);

        // ** RELLENANDO FOOTER
        this.roomFooter(ROOM.roomTitle.name, $listRoom, ROOM.status);

        // ** AGREGAR RESERVA
        $fragment.appendChild($listRoom);
      }

      // * AGREGAR TODAS LAS RESERVAS
      this.$divParentBedrooms.innerHTML = "";
      this.$divParentBedrooms.appendChild($fragment);
    }

    // ! RELLENAR HEADER DE LA HABITACIÓN
    roomHeader(header, reserveTemplate) {
      const $roomHeader = reserveTemplate.querySelector("#reserve-header");
      const $roomName = reserveTemplate.querySelector("#reserve-header h3");
      const $roomPrice = reserveTemplate.querySelector("#reserve-header p");

      $roomHeader.style.background = header.color;
      $roomName.textContent = header.name;
      $roomPrice.textContent = `CAD$ ${parseInt(header.priceAgent) + parseInt(header.priceHomestay)}`;
      // * WEEKLY PRICES
      reserveTemplate.querySelector("[data-price-agent]").textContent = header.priceAgent;
      reserveTemplate.querySelector("[data-price-homestay]").textContent = header.priceHomestay;
    }

    // ! RELLENAR IMÁGENES
    roomPhoto(roomData, images, reserveTemplate) {
      if (roomData) {
        const $fragment = D.createDocumentFragment();

        const $carouselContainer = D.createElement("div");
        $carouselContainer.id = roomData.roomTitle.name.replace(" ", "-");
        $carouselContainer.className = "w-100 carousel slide";
        $carouselContainer.dataset.ride = "carousel";

        const $indicators = D.createElement("ol");
        $indicators.className = "carousel-indicators";

        const $indicator = D.createElement("li");
        $indicator.dataset.target = `#${$carouselContainer.id}`;

        const $imagesContainer = D.createElement("div");
        $imagesContainer.className = "w-100 carousel-inner";

        const $btnPrevious = D.createElement("a");
        $btnPrevious.className = "carousel-control-prev";
        $btnPrevious.dataset.slide = "prev";
        $btnPrevious.setAttribute("role", "button");
        $btnPrevious.href = `#${$carouselContainer.id}`;

        const $btnNext = D.createElement("a");
        $btnNext.className = "carousel-control-next";
        $btnNext.dataset.slide = "next";
        $btnNext.setAttribute("role", "button");
        $btnNext.href = `#${$carouselContainer.id}`;

        images.forEach((image, i) => {
          if (image && image !== "NULL") {
            $indicator.dataset.slideTo = i;
            $indicators.appendChild($indicator.cloneNode(true));
            $indicators.firstElementChild.className = "active";

            const $imageItem = D.createElement("div");
            $imageItem.className = "w-100 carousel-item";
            const $image = D.createElement("img");
            $image.src = "../" + image;
            $image.className = "w-100 img-room";
            $imageItem.appendChild($image);
            $imagesContainer.appendChild($imageItem);
            $imagesContainer.firstElementChild.classList.add("active");

            $btnPrevious.innerHTML = `<span class="carousel-control-prev-icon mr-5" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>`;
            $btnNext.innerHTML = `<span class="carousel-control-next-icon ml-5" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>`;

            $carouselContainer.appendChild($indicators);
            $carouselContainer.appendChild($imagesContainer);
            $carouselContainer.appendChild($btnPrevious);
            $carouselContainer.appendChild($btnNext);

            $fragment.appendChild($carouselContainer);
          }
        });

        return reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
      } else {
        const $imageRoom = reserveTemplate.querySelector("#single-image-room");
        $imageRoom.classList.remove("d-none");
        if (images[0] && images[0] !== "NULL") $imageRoom.src = "../" + images[0];
        else $imageRoom.src = "../assets/img/homestay_home2.png";
      }
    }

    // ! RELLENAR CARACTERÍSTICAS
    roomFeatures(features, reserveTemplate, idHome) {
      const $typeRoom = reserveTemplate.querySelector("#type-room");
      const $roomFoodService = reserveTemplate.querySelector("#food-service");

      $typeRoom.textContent = features.type;
      $roomFoodService.textContent = features.food;
    }

    // ! ROOM BEDS INFO
    roomBedsInfo(roomBeds, roomTitle, bedTemplate, reserveTemplate) {
      const ALL_BEDS = Object.values(roomBeds.allBeds) || [];

      const RESERVES = [];
      for (const key in roomBeds.allBedsReserved) {
        RESERVES.push({
          roomName: roomTitle.name,
          roomColor: roomTitle.color,
          name: roomBeds.allBedsReserved[key][0],
          letter: roomBeds.allBedsReserved[key][1],
          firstDay: roomBeds.allBedsReserved[key][2],
          lastDay: roomBeds.allBedsReserved[key][3],
          image: roomBeds.allBedsReserved[key][4],
        });
      }
      RESERVES.sort((x, y) => new Date(x.firstDay) - new Date(y.firstDay));

      const BEDS_TRAVELED = [];
      const NEXT_RESERVATIONS = RESERVES.filter((reserve) => {
        if (!BEDS_TRAVELED.includes(reserve.letter)) {
          BEDS_TRAVELED.push(reserve.letter);
          return reserve;
        }
      });

      const $fragment = D.createDocumentFragment();
      const $bedsContainer = reserveTemplate.querySelector("#beds-container");

      if (ALL_BEDS && ALL_BEDS instanceof Array) {
        ALL_BEDS.forEach((bed) => {
          const ROOM = bed[0];
          const TYPE_ROOM = bed[4];
          let TYPE_BED;
          bed[1] === "Bunk-bed" || bed[1] === "Bunker" ? (TYPE_BED = "Bunk") : (TYPE_BED = bed[1]);

          let BED;
          if (bed[2] === "A") BED = "Bed 1";
          if (bed[2] === "B") BED = "Bed 2";
          if (bed[2] === "C") BED = "Bed 3";
          const STATUS = bed[3];

          bedTemplate.querySelector("#bed-type").textContent = TYPE_BED;
          bedTemplate.querySelector("#bed-letter").textContent = BED;
          bedTemplate.querySelector("#bed").setAttribute("for", bed[0] + bed[2]);
          bedTemplate.querySelector("#bed input").id = bed[0] + bed[2];

          const NEW_BED_DATA = {};
          for (let i = 0; i < NEXT_RESERVATIONS.length; i++) {
            const RESERVE = NEXT_RESERVATIONS[i];
            const ROOM = NEXT_RESERVATIONS[i].roomName;
            const BED = NEXT_RESERVATIONS[i].letter;

            if (BED === bed[2]) {
              NEW_BED_DATA.roomName = RESERVE.roomName;
              NEW_BED_DATA.studentBed = RESERVE.letter;
              NEW_BED_DATA.studentName = RESERVE.name;
              NEW_BED_DATA.studentDate = RESERVE.firstDay + " - " + RESERVE.lastDay;
              NEW_BED_DATA.studentImage = RESERVE.image;
            }
          }

          if (NEW_BED_DATA.studentBed === bed[2]) bedTemplate.querySelector("#bed").classList.remove("no-reservations");
          else bedTemplate.querySelector("#bed").classList.add("no-reservations");
          bedTemplate.querySelector("#student-name").textContent = NEW_BED_DATA.studentName || undefined;
          bedTemplate.querySelector("#student-date").textContent = NEW_BED_DATA.studentDate || undefined;
          bedTemplate.querySelector("#student-img").src = "../" + NEW_BED_DATA.studentImage || undefined;

          let $clone = D.importNode(bedTemplate, true);

          if (STATUS !== "Disabled" && STATUS !== "NULL" && TYPE_BED !== "NULL" && TYPE_BED !== "Disabled")
            $fragment.appendChild($clone);

          if (bed[3] === "Occupied" && bed[3] === "NULL") {
            console.log(D.getElementById(bed[0] + bed[2]));
            D.getElementById(bed[0] + bed[2]).remove();
          }

          if (bed[3] !== "NULL") {
            D.addEventListener("click", (e) => {
              if (!e.target) return;
              if (e.target.id === `${bed[0] + bed[2]}`) {
                const $inputSelected = e.target;
                D.querySelectorAll(`#bed input`).forEach((input, i) => {
                  if (!input) return;
                  const $bedsContainer = input.parentElement.parentElement;
                  const $inputLabel = $inputSelected.parentElement;
                  const $reserveContainer = $inputSelected.parentElement.parentElement.parentElement;
                  if (input !== $inputSelected) input.checked = false;

                  if ($inputSelected.checked) {
                    input.parentElement.classList.remove("checked");
                    $inputLabel.classList.add("checked");
                    $reserveContainer.querySelector(".room-card #reserve-btn").disabled = false;

                    if (!input.id.includes($inputSelected.id.slice(0, 6))) {
                      input.parentElement.parentElement.parentElement.querySelector("#reserve-btn").disabled = true;
                    }
                  } else {
                    $inputLabel.classList.remove("checked");
                    $reserveContainer.querySelector(".room-card #reserve-btn").disabled = true;
                  }
                });
              }
            });
          }
        });

        // * AGREGAR CAMA
        $bedsContainer.appendChild($fragment);

        for (let i = 0; i < $bedsContainer.querySelectorAll("#bed").length; i++) {
          const $statusDetail = $bedsContainer.parentElement.querySelector(".status-detail");
          const $bed = $bedsContainer.querySelectorAll("#bed")[i];

          if ($bed.classList.contains("no-reservations")) {
            $statusDetail.classList.add("available");
            $statusDetail.classList.remove("unavailable");
            break;
          } else {
            $statusDetail.classList.add("unavailable");
            $statusDetail.classList.remove("available");
          }
        }

        // * EMPTY BEDS
        if ($bedsContainer.children.length < 3) {
          const $studentsImg = $bedsContainer.querySelector("#students-img");
          $bedsContainer.appendChild($studentsImg);
          $studentsImg.classList.remove("d-none");
        }
      } else $bedsContainer.querySelector("#students-img").classList.remove("d-none");
    }

    roomFooter(roomName, reserveTemplate, statusRoom) {
      D.addEventListener("click", (e) => {
        if (e.target.matches("#reserve-btn")) {
          const INPUTS = e.target.parentElement.parentElement.parentElement.querySelectorAll("#beds-container input");

          INPUTS.forEach((input) => {
            if (input.checked) {
              this.showModal(e.target, input.id);
            }
          });
        }
      });
    }

    async listStudent(roomBed) {
      const formListStudent = new FormData();
      formListStudent.append("id_home", this.ID);
      formListStudent.append("room-bed", roomBed);
      const DATA = { method: "POST", body: formListStudent };
      const jsonListStudentReservation = await fetch("list_student_reservation.php", DATA);
      const listStudentReservation = await jsonListStudentReservation.json();

      const parentListStudent = D.getElementById("list__student");

      const ALLOWED = [];
      const HIDE_STUDENT = [];

      listStudentReservation.forEach((student) => {
        console.log(student);
        if (student.allEvents === "Empty") ALLOWED.push(student);
        else {
          for (let i = 0; i < student.allEvents.length; i++) {
            const element = student.allEvents[i];

            // * CONVERTIR FECHA DE USUARIO A UNIX
            const my_FD_Unix = Math.floor(new Date(student.firstd).getTime() / 1000);
            const my_LD_Unix = Math.floor(new Date(student.lastd).getTime() / 1000);
            const FD_Unix = Math.floor(new Date(element.startDateEvents).getTime() / 1000);
            const LD_Unix = Math.floor(new Date(element.endDateEvents).getTime() / 1000);

            if (element.colorEvents === student.colorRoom && element.bedEvents === student.bed) {
              if (FD_Unix >= my_FD_Unix && LD_Unix <= my_LD_Unix) HIDE_STUDENT.push(student);
              else if (FD_Unix <= my_FD_Unix && LD_Unix >= my_LD_Unix) HIDE_STUDENT.push(student);
              else if (FD_Unix >= my_FD_Unix && FD_Unix <= my_LD_Unix) HIDE_STUDENT.push(student);
              else if (LD_Unix >= my_FD_Unix && LD_Unix <= my_LD_Unix) HIDE_STUDENT.push(student);
              else ALLOWED.push(student);
            }
          }
        }
      });

      this.showStudent(ALLOWED, HIDE_STUDENT);

      this.$allList = D.querySelectorAll(".students__details");
    }

    showStudent(students, studentsHide) {
      D.getElementById("list__student").innerHTML = "";

      students.forEach((student) => {
        const $divNoStudents = D.querySelector("#div__no_student");
        const $divSearchStudent = D.querySelector(".div__search-input");
        const $footerReservation = D.querySelector("#modal__footer_resevation");
        const $titleReservationModal = D.querySelector(".title__modal-header");
        if (studentsHide.indexOf(student) !== -1) {
          D.getElementById("list__student").innerHTML = "";
          $divNoStudents.classList.replace("d-none", "d-flex");
          $divSearchStudent.classList.add("d-none");
          $footerReservation.classList.replace("d-flex", "d-none");
          $titleReservationModal.textContent = "No Students Available";
        } else {
          /* if (student.firstd >= student.date) { */
          $titleReservationModal.textContent = "Choose the Student";
          $divNoStudents.classList.replace("d-flex", "d-none");
          $divSearchStudent.classList.remove("d-none");
          $footerReservation.classList.replace("d-none", "d-flex");
          this.$templateListStudent.querySelector(".img__student").src = "../" + student.photo_s;
          this.$templateListStudent.querySelector(".names__students").textContent = student.fullnames;
          this.$templateListStudent.querySelector("#date-arrive").textContent = student.firstd;
          this.$templateListStudent.querySelector("#date-leave").textContent = student.lastd;
          this.$templateListStudent.querySelector(".div_checkbox input").id = student.id_student;
          const $clone = D.importNode(this.$templateListStudent, true);
          D.getElementById("list__student").appendChild($clone);
          /* } */
        }
      });
    }

    async showId() {
      document.getElementById("btn__submit-reservation").classList.add("btn_none");
      document.getElementById("btn__success").classList.add("spin");
      for (let i = 0; i < this.$allList.length; i++) {
        const $input = this.$allList[i].querySelector("input");

        if ($input.checked === true) {
          const formData = new FormData(formReservation);
          const DATA = { method: "POST", body: formData };

          const jsonsendReservation = await fetch("reservation__room.php", DATA);
          const sendReservation = await jsonsendReservation.json();

          document.getElementById("btn__submit-reservation").classList.remove("btn_none");
          document.getElementById("btn__success").classList.remove("spin");

          if (sendReservation.status == "Registered") {
            modalReservation.classList.replace("d-flex", "d-none");
            document.body.style.overflowY = "scroll";
            this.showInfoReservation();
            document.getElementById("contentp").innerHTML = "Reservation assigned successfully.";
          } else if (sendReservation.status == "Already") {
            this.showInfoReservation();
            document.getElementById("contentp").innerHTML = "The Student already has an active reservation.";
          } else {
            this.showInfoReservation();
            document.getElementById("contentp").innerHTML = "An error has occurred, please try again.";
          }
        }
      }
    }

    showInfoReservation() {
      setTimeout(() => {
        $("#house_much").fadeIn("slow");

        setTimeout(() => {
          $("#house_much").fadeOut("slow");
        }, 5000);
      }, 100);

      $("#close").on("click", function close() {
        event.preventDefault();
        $("#house_much").fadeOut("slow");
      });

      document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px green";
      document.getElementById("contentp").style.marginTop = "auto";
      document.getElementById("contentp").style.marginBottom = "auto";
      document.getElementById("contentp").style.color = "#000";

      W.location.reload();
    }

    async searchStudentsReservation(search) {
      const $students = D.querySelectorAll(".names__students");

      $students.forEach((name) => {
        name.parentElement.parentElement.parentElement.classList.add("d-none");
        if (name.textContent.toLowerCase().includes(search.toLowerCase())) {
          /* for (let i = 0; i < name.textContent.length; i++) {
            /* console.log(name.textContent[i]);
            if (search === name.textContent[i]) console.log(name.textContent[i]);
          } */
          name.parentElement.parentElement.parentElement.classList.remove("d-none");
        }
      });
    }

    clearCheckbox(e) {
      const $thisElement = e.target.querySelector("input");
      btnSendReservation.disabled = false;

      for (let i = 0; i < this.$allList.length; i++) {
        const $input = this.$allList[i].querySelector("input");

        if ($thisElement.id !== $input.id) {
          $input.checked = false;
        } else {
          $input.checked = true;
          btnSendReservation.disabled = false;
        }
        D.getElementById("id__student-reservation").value = $thisElement.id;
        if ($thisElement.checked === false) {
          $thisElement.parentNode.parentNode.style.backgroundColor = "#f3f8fc";
        } else {
          $thisElement.checked = true;
          $thisElement.parentNode.parentNode.style.backgroundColor = "#f3f8fc";
        }
        $input.parentNode.parentNode.style.backgroundColor = "transparent";
      }
    }

    showModal(btn, roomName) {
      modalReservation.classList.replace("d-none", "d-flex");
      document.body.style.overflowY = "hidden";
      D.getElementById("button-room").value = roomName;
    }

    closeModal() {
      modalReservation.classList.replace("d-flex", "d-none");
      document.body.style.overflowY = "scroll";
      btnSendReservation.disabled = true;
    }

    // ! FAMILY MEMBERS FUNCTIONS

    async listFamilyMembers() {
      this.DATA.set("id_home", this.ID);
      this.DATA.set("request", "familyMemberList");
      const DATA = { method: "POST", body: this.DATA };

      const fetchFamilyMember = await fetch("detail_data.php", DATA);
      const jsonFamilyMember = await fetchFamilyMember.json();

      // * FRAGMENT FAMILY MEMBER
      const $fragment = D.createDocumentFragment();
      const $fragmentBackground = D.createDocumentFragment();
      const $h3FamilyMember = D.querySelector("#family-members-title");
      const $h3BackgroundCheck = D.querySelector("#backgroundCheck-title");
      const $backgroundCheckContent = D.querySelector("#parent-background-check");

      if (jsonFamilyMember !== null) {
        $h3FamilyMember.classList.remove("d-none");
        // * ASSIGN FAMILY MEMBER
        for (const key in jsonFamilyMember) {
          const member = jsonFamilyMember[key];

          const $memberFamily = this.$templateFamilyMember.firstElementChild.cloneNode(true);

          this.detailsMember($memberFamily, member);

          $fragment.appendChild($memberFamily);
        }

        this.$allMembers.appendChild($fragment);
      } else {
        $h3FamilyMember.classList.add("d-none");
      }
    }

    // TODO ASSIGN CONTENT TO VARIABLES

    detailsMember(memberList, member) {
      // ? FAMILY MEMBER DETAILS
      const $h3FamilyMember = D.querySelector("#family-members-title");

      // TODO SEACH TAGS
      const $memberName = memberList.querySelector("#member-name");
      const $dateOfBirth = memberList.querySelector("#db-member");
      const $gender = memberList.querySelector("#gender");
      const $relation = memberList.querySelector("#relation");
      const $occupation = memberList.querySelector("#occupation");
      const $db_lawf = memberList.querySelector("#backgroundCheck");
      const $lawf = memberList.querySelector("#pdf-member-check");
      const $noBackground = memberList.querySelector("#no-background");
      const $labelBackground = memberList.querySelector(".label_background");

      $memberName.textContent = member.m_name;
      $dateOfBirth.textContent = member.db_member;
      $gender.textContent = member.gender;
      $relation.textContent = member.relation;
      $occupation.textContent = member.occupation;
      $db_lawf.textContent = member.db_lawf;

      if (member.lawf !== "Empty") {
        $lawf.src = "../" + member.lawf;
        $noBackground.classList.add("d-none");
        $lawf.classList.remove("d-none");
        $labelBackground.classList.remove("d-none");
      } else {
        $lawf.classList.add("d-none");
        $noBackground.classList.remove("d-none");
        $labelBackground.classList.add("d-none");
      }
    }

    // TODO MODAL REPORT

    async openModalReport(idStudent, namesStudent) {
      const formOpenModal = new FormData();
      formOpenModal.set("request", "searchReports");
      formOpenModal.set("id_student", idStudent);

      const DATA = { method: "POST", body: formOpenModal };
      const jsonFormModal = await fetch("report_save.php", DATA);
      const formModal = await jsonFormModal.json();

      if (formModal.response === "Already Report") W.top.location = "reports.php";
      else {
        modalReport.classList.replace("d-none", "d-flex");
        document.body.style.overflowY = "hidden";
        D.querySelector("#student-fullname").value = namesStudent;
        D.querySelector("#student-id").value = idStudent;
      }
    }

    closeModalReport() {
      modalReport.classList.replace("d-flex", "d-none");
      document.body.style.overflowY = "scroll";
    }

    // ? SEND REPORT

    async sendReport() {
      const sendFormReport = new FormData(formSendReport);
      sendFormReport.set("request", "sendReport");
      sendFormReport.set("homestay_id", this.ID);
      const DATA = { method: "POST", body: sendFormReport };

      const jsonFormReport = await fetch("report_save.php", DATA);
      const formReport = await jsonFormReport.json();

      if (formReport.response === "Report Sent") W.top.location = "reports.php";
    }

    //? ACTIVITY LOG

    /* async activityLog() {
      const sendFormLog = new FormData();
      sendFormLog.set("homestay_id", this.ID);
      sendFormLog.set("request", "activityLogs");
      const DATA = { method: "POST", body: sendFormLog };

      const jsonFormLogs = await fetch("detail_data.php", DATA);
      const resultFormLogs = await jsonFormLogs.json();
      const $fragment = D.createDocumentFragment();

      console.log(resultFormLogs);

      resultFormLogs.forEach((log) => {
        const $clone = D.importNode($templateActivityLogs, true);
        if (log.link === "no") $clone.querySelector("#creator-user").textContent = `${log.createUser}`;
        else $clone.querySelector("#creator-user").innerHTML = `<a href="${log.createUserId}">${log.createUser}</a>`;
        $clone.querySelector("#title-log").textContent = log.activity;
        $clone.querySelector("#date-log").textContent = log.date;
        $clone.querySelector("#receiving-user").textContent = log.receivingUser;
        $clone.querySelector("#student-report").textContent = log.studentReport;
        /* $clone.querySelector("#reason-log").textContent = log.reasonActivity; 
        $fragment.appendChild($clone);
      });

      D.querySelector("#tbody-logs").appendChild($fragment);
    } */
  }

  // TODO INSTANCIA DE LA CLASE
  const INSTANCE = new DetailHomestay();

  // TODO EVENTOS
  D.addEventListener("DOMContentLoaded", async (e) => {
    const $idHome = D.querySelector("#homestay-id").value;
    INSTANCE.homestayDetails();
    await INSTANCE.roomsHouse($idHome);
    await INSTANCE.homestayPendingReservation();
    await INSTANCE.homestayActiveReservation();
    INSTANCE.listFamilyMembers();
    D.querySelectorAll("[data-bg-image]").forEach((img) => {
      if (!img.style.cssText) img.style.cssText = `background-image: url("${img.dataset.bgImage}")`;
    });
    /* INSTANCE.activityLog(); */

    // ! WINYERSON
    $(".owl-carousel").owlCarousel({
      loop: true,
      center: true,
      autoWidth: true,
    });
  });

  D.addEventListener("click", (e) => {
    const divButtonReservation = e.target.parentNode;
    const activeButtonNoti = e.target;
    const datasetButtonNoti = e.target.dataset.idNoti;
    const buttonNoti = e.target;

    if (e.target.matches(".btn__confirm-reservation")) {
      e.preventDefault();
      const buttonReservation = "Confirm";
      divButtonReservation.classList.add("style__child");
      buttonNoti.parentNode.classList.add("spin");
      INSTANCE.confirmReservation(buttonReservation, activeButtonNoti, datasetButtonNoti);
    } else if (e.target.matches(".btn__cancel-reservation")) {
      e.preventDefault();
      const buttonReservation = "Cancel";
      divButtonReservation.classList.add("style__child");
      buttonNoti.parentNode.classList.add("spin");
      INSTANCE.confirmReservation(buttonReservation, activeButtonNoti, datasetButtonNoti);
    } else if (e.target.matches("#btn__close-modal-report")) {
      e.preventDefault();
      INSTANCE.closeModalReport();
    }

    // ! WINYERSON
    if (e.target.matches(".zoom-image")) {
      $modal.classList.toggle("show");

      if ($modal.classList.contains("show")) {
        $modal.querySelector("img").src = e.target.parentElement.querySelector("img").src;
      } else $modal.querySelector("img").src = "";
    }
    if (e.target.matches("#modal-zoom-image") || e.target.matches("#modal-zoom-image span")) {
      $modal.classList.remove("show");
      $modal.querySelector("img").src = "";
    }
  });

  // TODO EVENTS SHOW MODAL RESERVATION

  D.addEventListener("click", (e) => {
    if (e.target.matches("#reserve-btn")) {
      const roomBed = e.target.parentElement.parentElement.parentElement.querySelectorAll("#beds-container input");

      roomBed.forEach((input) => {
        if (input.checked) INSTANCE.listStudent(input.id);
      });
    }
  });

  // TODO EVENTS CLOSE MODAL RESERVATION

  btnCloseModal.forEach((btnClose) => {
    btnClose.addEventListener("click", INSTANCE.closeModal);
  });

  D.addEventListener("click", (e) => {
    if (e.target.matches("#label__select-student")) INSTANCE.clearCheckbox(e);
    else if (e.target.matches("#btn__submit-reservation")) {
      e.preventDefault();
      INSTANCE.showId();
    }

    // * STUDENT INFO ACTIONS
    if (e.target.matches(".student-info")) e.target.classList.toggle("show");
  });

  inputSearch.addEventListener("keyup", (e) => {
    INSTANCE.searchStudentsReservation(e.target.value);
  });

  // TODO EVENTS TO SEND REPORT

  formSendReport.addEventListener("submit", (e) => {
    e.preventDefault();
    INSTANCE.sendReport();
  });

  W.addEventListener(
    "error",
    (e) => {
      if (e.target.matches("[data-empty-img]")) e.target.src = e.target.dataset.emptyImg;
    },
    true
  );
})(document, window);
