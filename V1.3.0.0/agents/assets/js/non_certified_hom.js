// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES
$(document).ready(function () {
  load_data(1);

  function load_data(page, query = "") {
    $.ajax({
      url: "fetch_non-certified_hom.php",
      method: "POST",
      data: { page: page, query: query },
      success: function (data) {
        $("#dynamic_content").html(data);
      },
    });
  }

  $("#vou").hide();

  $("#search_box").keyup(function () {
    if ($("#search_box").val()) {
      $("#dynamic_content").hide();
      $("#vou").show();

      let search = $("#search_box").val();

      var searchLength = search.length;

      $.ajax({
        url: "nc_homestay_search.php",
        type: "POST",
        data: { search },
        success: function (response) {
          let homestays = JSON.parse(response);
          let template = "";

          homestays.forEach((homestay) => {
            if (homestay.photo != "NULL" && homestay.man4 == homestay.man3) {
              template += `
                          <tr>
                            <th class="text-center align-middle" style="font-weight:normal"><img src="../${homestay.photo}" width="90px;" height="90px";></th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.h_name} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.city} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.dir} </th>
                            <th class="text-center align-middle" align="left">
                              <ul>
                                <a href="detail.php?art_id=${homestay.id_home}" id="preview" class="btn btn-sm btn-block float-left" name="preview" style="background-color: #232159; border: 2px solid #232159; color: #fff">
                                  <i class="fa fa-eye mr-2"></i>
                                    Preview
                                </a>
                                <br>
                                <a id="edit" href="homedit.php?art_id=${homestay.id_home}" class="btn btn-primary btn-sm btn-block float-right" name="edit" style="background-color: #4f177d; border: 2px solid #4f177d">
                                  <i class="fa fa-pencil-alt mr-2"></i>      
                                    Edit Home
                                </a>
                                              
                              </ul> 
                            </th>
  
                          </tr>`;
            } else if (homestay.photo == "NULL" && homestay.man4 == homestay.man3) {
              template += `
                          <tr>
                            <th class="text-center align-middle" style="font-weight:normal"></th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.h_name} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.city} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.dir} </th>
                            <th class="text-center align-middle" align="left">
                              <ul>
                                <a href="detail.php?art_id=${homestay.id_home}" id="preview" class="btn btn-sm btn-block float-left" name="preview" style="background-color: #232159; border: 2px solid #232159; color: #fff">
                                  <i class="fa fa-eye mr-2"></i>
                                    Preview
                                </a>
                                <br>
                                <a id="edit" href="homedit.php?art_id=${homestay.id_home}" class="btn btn-primary btn-sm btn-block float-right" name="edit" style="background-color: #4f177d; border: 2px solid #4f177d">
                                  <i class="fa fa-pencil-alt mr-2"></i>      
                                    Edit Home
                                </a>
                                              
                              </ul> 
                            </th>
  
                          </tr>`;
            } else {
              template += `
                          <tr>
                            <th class="text-center align-middle" style="font-weight:normal"><img src="../${homestay.photo}" width="90px;" height="90px";></th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.h_name} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.city} </th>
                            <th class="text-center align-middle" style="font-weight:normal"> ${homestay.dir} </th>
                            <th class="text-center align-middle" align="left">
                              <ul>
                                <a href="detail.php?art_id=${homestay.id_home}" id="preview" class="btn btn-sm btn-block float-left" name="preview" style="background-color: #232159; border: 2px solid #232159; color: #fff">
                                  <i class="fa fa-eye mr-2"></i>
                                    Preview
                                </a>
                                <br>
                                <a id="edit" href="detail.php?art_id=${homestay.id_home}" class="btn btn-primary btn-sm btn-block float-right" name="edit" >
                                    <i class="fa fa-pencil-alt mr-2"></i>      
                                    Quick Info
                                </a>
                                              
                              </ul> 
                            </th>
  
                          </tr>`;
            }
          });

          $("#vouchers").html(template);
        },
      });
    } else {
      $("#dynamic_content").show();
      $("#vou").hide();
    }
  });

  $(document).on("click", ".page-link", function () {
    var page = $(this).data("page_number");
    var query = $("#search_box").val();
    load_data(page, query);
  });
});

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  _Messages.quitMessage();
});
