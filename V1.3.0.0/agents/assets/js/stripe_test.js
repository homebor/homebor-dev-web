// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES
async function getData(get) {
  const formData = new FormData();
  formData.set("get", get);

  const OPTIONS = {
    method: "POST",
    headers: { "Content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const request = await axios("stripe.php", OPTIONS);
  const response = await request.data;

  if (get === 1) renderCustomers(response.data);
  else renderCustomers(response.data);
}

function renderCustomers(data) {
  const $container = D.getElementById("container");
  const $template = D.getElementById("customer-template").content;
  const $fragment = D.createDocumentFragment();

  if (data) {
    $container.innerHTML = "";
    data.forEach((customer) => {
      const $customer = D.importNode($template, true);
      // $customer.querySelector('data-customer-image').src
      $customer.querySelector("[data-customer-name]").textContent = customer.name;
      if (customer.address == null) customer.address = { city: "Empty" }.city = "Empty";
      $customer.querySelector("[data-customer-address]").textContent = customer.address.city || "Empty";
      $customer.querySelector("[data-customer-phone]").textContent = customer.phone || "Empty";
      const getDate = (date) => {
        const DATE = new Date(Number(`${date}000`));
        const getMonth = (month) => (month < 10 ? `0${month}` : month);
        const dateText = `${DATE.getDate()}-${getMonth(DATE.getMonth() + 1)}-${DATE.getFullYear()}`;
        return dateText;
      };
      $customer.querySelector("[data-customer-created]").textContent = getDate(customer.created);
      $customer.querySelector("[data-customer-description]").textContent = customer.description;
      $customer.querySelector("[data-customer-rol]").dataset.customerRol = customer.metadata.rol || "Empty";
      $fragment.appendChild($customer);
    });

    $container.appendChild($fragment);
  }
}
// *
// *
// *
// *
// *
// *
// *
async function createCustomer(data) {
  const formData = new FormData();
  formData.set("create", 1);
  formData.set("name", data.name);
  formData.set("email", data.email);
  formData.set("address", data.address);
  formData.set("phone", data.phone);
  formData.set("description", data.description);
  formData.set("rol", data.rol);

  const OPTIONS = {
    method: "POST",
    headers: { "Content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const request = await axios("stripe.php", OPTIONS);
  const response = await request.data;

  console.log(response);
}
// This is your test publishable API key.
// const stripe = Stripe(
//   "pk_test_51LhOKKJkJ5U3Iny4BgkcivQ5P5oKOcPwsiUmdmVXMm0d4ROsZdmkOulIlxWUSTNo5du8DEwacaR4bSjtaGSSrObv00080r9spI"
// );

// The items the customer wants to buy
// const items = [{ id: "xl-tshirt" }];

// let elements;

// initialize();
// checkStatus();

// document.querySelector("#payment-form").addEventListener("submit", handleSubmit);

// Fetches a payment intent and captures the client secret
// async function initialize() {
//   const { clientSecret } = await fetch("/create.php", {
//     method: "POST",
//     headers: { "Content-Type": "application/json" },
//     body: JSON.stringify({ items }),
//   }).then((r) => r.json());

//   elements = stripe.elements({ clientSecret });

//   const paymentElement = elements.create("payment");
//   paymentElement.mount("#payment-element");
// }

// async function handleSubmit(e) {
//   e.preventDefault();
//   setLoading(true);

//   const { error } = await stripe.confirmPayment({
//     elements,
//     confirmParams: {
//       // Make sure to change this to your payment completion page
//       return_url: "http://localhost:4242/public/checkout.html",
//     },
//   });

// This point will only be reached if there is an immediate error when
// confirming the payment. Otherwise, your customer will be redirected to
// your `return_url`. For some payment methods like iDEAL, your customer will
// be redirected to an intermediate site first to authorize the payment, then
// redirected to the `return_url`.
//   if (error.type === "card_error" || error.type === "validation_error") {
//     showMessage(error.message);
//   } else {
//     showMessage("An unexpected error occurred.");
//   }

//   setLoading(false);
// }

// Fetches the payment intent status after payment submission
// async function checkStatus() {
//   const clientSecret = new URLSearchParams(window.location.search).get("payment_intent_client_secret");

//   if (!clientSecret) {
//     return;
//   }

//   const { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);

//   switch (paymentIntent.status) {
//     case "succeeded":
//       showMessage("Payment succeeded!");
//       break;
//     case "processing":
//       showMessage("Your payment is processing.");
//       break;
//     case "requires_payment_method":
//       showMessage("Your payment was not successful, please try again.");
//       break;
//     default:
//       showMessage("Something went wrong.");
//       break;
//   }
// }

// ------- UI helpers -------

// function showMessage(messageText) {
//   const messageContainer = document.querySelector("#payment-message");

//   messageContainer.classList.remove("hidden");
//   messageContainer.textContent = messageText;

//   setTimeout(function () {
//     messageContainer.classList.add("hidden");
//     messageText.textContent = "";
//   }, 4000);
// }

// Show a spinner on payment submission
// function setLoading(isLoading) {
//   if (isLoading) {
//     // Disable the button and show a spinner
//     document.querySelector("#submit").disabled = true;
//     document.querySelector("#spinner").classList.remove("hidden");
//     document.querySelector("#button-text").classList.add("hidden");
//   } else {
//     document.querySelector("#submit").disabled = false;
//     document.querySelector("#spinner").classList.add("hidden");
//     document.querySelector("#button-text").classList.remove("hidden");
//   }
// }
// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await getData(1);
  _Messages.quitMessage();
});
// ! CLICK
D.addEventListener("click", (e) => {
  if (e.target.id === "show-customers") {
    getData(1);
  }
  if (e.target.id === "show-products") {
    getData(2);
  }
});

// ! SUBMIT
D.addEventListener("submit", (e) => {
  e.preventDefault();

  if (e.target.id.includes("customer")) {
    const customerData = {
      name: e.target.name.value || "Empty",
      email: e.target.email.value || "Empty",
      address: e.target.address.value || "Empty",
      phone: e.target.phone.value || "Empty",
      description: e.target.description.value || "Empty",
      rol: e.target.rol.value || "Empty",
    };

    createCustomer(customerData);
  } else {
    console.log("Falta crear el producto");
  }
});

// ! CHANGE

// ! KEYDOWN
D.addEventListener("keydown", (e) => {
  // * CREATE CUSTOMER
  if (e.shiftKey && e.code === "Backquote") {
    D.querySelector("#create-customer-form").classList.add("d-none");
    D.querySelector("#create-product-form").classList.add("d-none");
  }

  if (e.shiftKey && e.code === "Digit1") {
    D.querySelector("#create-customer-form").classList.remove("d-none");
    D.querySelector("#create-product-form").classList.add("d-none");
  }
  // * CREATE PRODUCT
  if (e.shiftKey && e.code === "Digit2") {
    D.querySelector("#create-product-form").classList.remove("d-none");
    D.querySelector("#create-customer-form").classList.add("d-none");
  }
});
// ! ERROR
