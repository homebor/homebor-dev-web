<?php
// TODO WINYERSON
if ($_POST['homestay_id']) {
  session_start();
  include_once('../xeon.php');
  error_reporting(0);

  // TODO VARIABLES
  $id = $_POST['homestay_id'];
  $usuario = $_SESSION['username'];

  // TODO CONSULTAS

  // $queryMembersFamily_IJ = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
  // $row_members_family = $queryMembersFamily_IJ->fetch_assoc();



  // TODO FUNCIONES
  function homestayDetails($link, $id, $usuario)
  {
    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS
    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row_homestay = $homestayQuery->fetch_assoc();
    
    $academyQuery = $link->query("SELECT * FROM academy WHERE id_ac = '$row_homestay[a_pre]' ");
    $row_academy = $academyQuery->fetch_assoc();

    $db_main = date_create($row_homestay['db']);
    $date_db_main = date_format($db_main, 'jS M \of\ Y');

    // TODO ASSIGN VALUES TO THE VARIABLES

    if(empty($row_homestay['des']) || $row_homestay['des'] == 'NULL') $descriptionHome = 'Empty';
    else $descriptionHome = $row_homestay['des'];

    if (empty($row_homestay['h_name'] || $row_homestay['h_name'] == 'NULL')) $h_name = 'Empty';
    else $h_name = $row_homestay['h_name'];

    if ($row_homestay['name_h'] == 'NULL' && $row_homestay['l_name_h'] == 'NULL') $fullnames = 'Empty';
    else if($row_homestay['name_h'] == 'NULL' && $row_homestay['l_name_h'] != 'NULL') $fullnames = $row_homestay['l_name_h'];
    else if($row_homestay['name_h'] != 'NULL' && $row_homestay['l_name_h'] == 'NULL') $fullnames = $row_homestay['name_h'];
    else $fullnames = $row_homestay['name_h'] . " " . $row_homestay['l_name_h'];

    if($row_homestay['gender'] == 'NULL') $gender = 'Empty';
    else $gender = $row_homestay['gender'];

    if($row_homestay['status'] == 'NULL') $status = 'Empty';
    else $status = $row_homestay['status'];

    if($row_homestay['smoke'] == 'NULL') $smoke = 'Empty';
    else $smoke = $row_homestay['smoke'];

    if($row_homestay['des'] == 'NULL') $description = 'Empty';
    else $description = $row_homestay['des'];

    if($row_academy['name_a'] == 'NULL') $academy = 'Empty';
    else $academy = $row_academy['name_a'];

    if($row_homestay['g_pre'] == 'NULL') $genderPreference = 'Empty';
    else $genderPreference = $row_homestay['g_pre'];

    if($row_homestay['ag_pre'] == 'NULL') $agePreference = 'Empty';
    else $agePreference = $row_homestay['ag_pre'];

    if($row_homestay['dir'] == 'NULL') $address = 'Empty';
    else $address = $row_homestay['dir'];

    if($row_homestay['city'] == 'NULL') $city = 'Empty';
    else $city = $row_homestay['city'];

    if($row_homestay['state'] == 'NULL') $state = 'Empty';
    else $state = $row_homestay['state'];

    if($row_homestay['p_code'] == 'NULL') $postalCode = 'Empty';
    else $postalCode = $row_homestay['p_code'];

    if($row_homestay['mail_h'] == 'NULL') $mail_h = 'Empty';
    else $mail_h = $row_homestay['mail_h'];

    if($row_homestay['num'] == 'NULL') $pnumber = 'Empty';
    else $pnumber = $row_homestay['num'];

    if($row_homestay['occupation_m'] == 'NULL') $occupationM = 'Empty';
    else $occupationM = $row_homestay['occupation_m'];
    
    if($row_homestay['db_law'] == 'NULL') $dbLaw = 'Empty';
    else {
      $dbLaw = date_create($row_homestay['db_law']);
      $date_dbLaw = date_format($dbLaw, 'jS M \of\ Y');
    }

    if($row_homestay['backl'] == 'NULL') $backl = 'Empty';
    else $backl = $row_homestay['backl'];

    if ($row_homestay['y_service'] == 'NULL') $dateY = 'Empty';
    else {

      date_default_timezone_set("America/Toronto");
      $date = date('Y-m-d H:i:s');

      $firstDate = $row_homestay['y_service'];
      $secondDate = $date;

      $dateDifference = abs(strtotime($secondDate) - strtotime($firstDate));

      $years  = floor($dateDifference / (365 * 60 * 60 * 24));

      if ($years == '0') {

        $months = floor(($dateDifference - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));

        if ($months == '1') $dateY = $months . ' month';
        elseif ($months == '0') {

          $days   = floor(($dateDifference - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

          if ($days == '1') $dateY = $days . ' Day';
          elseif ($days == '0') $dateY = 'Today';
          else $dateY = $days . ' Days';
        
        } else $dateY = $months . ' Months';

      } else {

        if ($years == '1') $dateY = $years . ' Year';
        else $dateY = $years . ' Years';

      }
    }

    if($row_homestay['room'] == '0' || $row_homestay['room'] == 'NULL') $rooms = 'Empty';
    else $rooms = $row_homestay['room'];

    if($row_homestay['pet'] == 'Yes' && $row_homestay['dog'] == 'yes' && $row_homestay['cat'] == 'no' && $row_homestay['other'] == 'no') $pets = 'Dog';
    else if($row_homestay['pet'] == 'Yes' && $row_homestay['cat'] == 'yes' && $row_homestay['dog'] == 'no' && $row_homestay['other'] == 'no') $pets = 'Cat';
    else if($row_homestay['pet'] == 'Yes' && $row_homestay['other'] == 'yes' && $row_homestay['dog'] == 'no' && $row_homestay['cat'] == 'no') $pets = $row_homestay['type_pet'];
    else if($row_homestay['pet'] == 'No') $pets = $row_homestay['pet'];
    else $pets = $row_homestay['pet'];

    if($row_homestay['m_service'] == 'NULL') $mService = 'Empty';
    else $mService = $row_homestay['m_service'];

    if($row_homestay['vegetarians'] == 'yes' || $row_homestay['halal'] == 'yes' || $row_homestay['lactose'] == 'yes' || $row_homestay['gluten'] == 'yes' || $row_homestay['kosher'] == 'yes' || $row_homestay['pork'] == 'yes') $specialDiet = 'Yes';
    else $specialDiet = 'No';



    // ? ADDITIONAL INFORMATION

    if($row_homestay['backg'] == 'NULL') $background = 'Empty';
    else $background = $row_homestay['backg'];

    if($row_homestay['religion'] == 'NULL') $religion = 'Empty';
    else $religion = $row_homestay['religion'];

    if($row_homestay['misdemeanor'] == 'NULL') $misdemeanor = 'Empty';
    else $misdemeanor = $row_homestay['misdemeanor'];

    if($row_homestay['c_background'] == 'NULL') $c_background = 'Empty';
    else $c_background = $row_homestay['c_background'];
    
    if($row_homestay['allergies'] == 'NULL') $allergies = 'Empty';
    else $allergies = $row_homestay['allergies'];

    if($row_homestay['medic_f'] == 'NULL') $medication = 'Empty';
    else $medication = $row_homestay['medic_f'];
    
    if($row_homestay['condition_m'] == 'NULL') $condition_m = 'Empty';
    else $condition_m = $row_homestay['condition_m'];
    
    if($row_homestay['health_f'] == 'NULL') $healthProblems = 'Empty';
    else $healthProblems = $row_homestay['health_f'];

    $jsonToSend = array(
      'name' => $h_name,
      'id' => $row_homestay['id_home'],
      'fullname' => $fullnames,
      'gender' => $gender,
      'status' => $status,
      'smokePolitics' => $smoke,
      'description' => $description,
      'academyName' => $academy,
      'genderPreference' => $genderPreference,
      'agePreference' => $agePreference,
      'address' => $address,
      'city' => $city,
      'state' => $state,
      'postal_code' => $postalCode,
      'mail_h' => $mail_h,
      'pnumber' => $pnumber,
      'occupation' => $occupationM,
      'db_law' => $date_dbLaw,
      'db_main' => $date_db_main,
      'background_language' => $backl,
      'years_experience' => $dateY,
      'rooms' => $rooms,
      'pets' => $pets,
      'meal_service' => $mService,
      'special_diet' => $specialDiet,
      'description_home' => $descriptionHome,
      'background' => $background,
      'religion' => $religion,
      'misdemeanor' => $misdemeanor,
      'c_background' => $c_background,
      'allergies' => $allergies,
      'medication' => $medication,
      'pmcondition' => $condition_m,
      'healthProblems' => $healthProblems,
    );

    echo json_encode($jsonToSend);
  }

  function roomsDetails($link, $id, $usuario)
  {
    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS
    $userDataQuery = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
    $userData = $userDataQuery->fetch_assoc();


    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
    $row_homestay = $homestayQuery->fetch_assoc();


    $queryRoom_IJ = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
    $row_room = $queryRoom_IJ->fetch_assoc();


    $queryPhotoHome_IJ = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
    $row_photo_home = $queryPhotoHome_IJ->fetch_assoc();


    $roomNumber = 1;
    foreach ($row_room as $key => $value) {

      // * ROOM
      if (
        $row_room['type' . $roomNumber] != 'NULL' &&
        $row_room['date' . $roomNumber] != "Disabled" &&
        $row_room['aprox' . $roomNumber] != '0'
      ) {
        if ($roomNumber == 1) $colorRoom = "#232159";
        if ($roomNumber == 2) $colorRoom = "#982A72";
        if ($roomNumber == 3) $colorRoom = "#394893";
        if ($roomNumber == 4) $colorRoom = "#A54483";
        if ($roomNumber == 5) $colorRoom = "#5D418D";
        if ($roomNumber == 6) $colorRoom = "#392B84";
        if ($roomNumber == 7) $colorRoom = "#B15391";
        if ($roomNumber == 8) $colorRoom = "#4F177D";



        /* $queryNotification = "SELECT * FROM notification WHERE color = '$colorRoom' AND user_r = '$row_homestay[mail_h]' AND reserve_h = '$row_homestay[mail_h]' AND confirmed = '0' AND title = 'Reservation Request'";
        $notification = $link->query($queryNotification);
        $row_notification = $notification->fetch_assoc();
        $num_rows_notification = mysqli_num_rows($notification); */



         // ** DYNAMIC VARIABLES
        /* $allReservesDate = null; */
        $roomStatus = null;


        // ** ROOM STATUS
        if ($row_room['date' . $roomNumber] == 'Avalible') {
          $roomStatus = "Avalible";
          $strQueryEvents = "SELECT * FROM events WHERE color = '$colorRoom'  AND email = '$row_homestay[mail_h]' ";
          $queryEvents = $link->query($strQueryEvents);

          while ($value = mysqli_fetch_array($queryEvents)) {
            $allReservesDate[] = array($value['title'], $value['start'], $value['end']);
          }
        }else{
          $roomStatus = "Occupied";
        }


        // ** ROOM DATA
        $jsonToSend[] = array(
          'roomName' => 'Room ' . $roomNumber,
          'roomColor' => $colorRoom,
          'roomPrice' => 'CAD$ ' . $row_room['aprox' . $roomNumber] + $row_room['aprox_a' . $roomNumber],
          'roomImage' => array(
            'image1' => $row_photo_home['proom' . $roomNumber],
            'image2' => $row_photo_home['proom' . $roomNumber . "_2"],
            'image3' => $row_photo_home['proom' . $roomNumber . "_3"],
          ),
          'type' => $row_room['type' . $roomNumber],
          'bed' => $row_room['bed' . $roomNumber],
          'foodService' => $row_room['food' . $roomNumber],
          'status' => $roomStatus,
          'allReservesDate' => $allReservesDate,
          'aproxPrice' => $row_room['aprox' . $roomNumber],
          'aproxPrice2' => $row_room['aprox_a' . $roomNumber],
          'reservations' => $row_room['reservations' . $roomNumber],
        );
      }

      $roomNumber++;
      if ($roomNumber > 8) break;
    }

    echo json_encode($jsonToSend);
  }

  function homestayPendingReservation($link, $id, $usuario){

    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS

    $agentsQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
    $row_agents = $agentsQuery->fetch_assoc();

    $managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_agents[id_m]'");
    $row_manager = $managerQuery->fetch_assoc();

    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id'");
    $row_homestay = $homestayQuery->fetch_assoc();

    $notificationQuery = $link->query("SELECT * FROM notification WHERE user_i_mail != '$row_manager[mail]' AND user_r = '$row_homestay[mail_h]' AND title = 'Reservation Request' AND confirmed = '0'");
    $num_rows_notification = mysqli_num_rows($notificationQuery);

    if($num_rows_notification > 0){
      while ($row_notification = mysqli_fetch_array($notificationQuery)){

        $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_notification[user_i_mail]'");
        $row_student = $studentQuery->fetch_assoc();

        if($row_notification['bedrooms'] == 'NULL') $room = 'Empty';
        else $room = $row_notification['bedrooms'];

        if($row_notification['start'] == 'NULL') $start = 'Empty';
        else {
          $date_start = date_create($row_notification['start']);
          $start = date_format($date_start, 'Y/m/d');
        }

        if($row_notification['end_'] == 'NULL') $end = 'Empty';
        else{
          $date_end = date_create($row_notification['end_']);
          $end = date_format($date_end, 'Y/m/d');
        }

        $jsonToSend[] = array(
          'id_student' => $row_student['id_student'],
          'fullname' => $row_student['name_s']. ' ' . $row_student['l_name_s'],
          'photo_s' => $row_student['photo_s'],
          'id_not' => $row_notification['id_not'],
          'mail_s' => $row_notification['user_i_mail'],
          'start' => $start,
          'room' => $room,
          'end' => $end,
        );
      }
    }else{
      $jsonToSend[] = array(
        'id_student' => 'Empty'
      );
    }

    echo json_encode($jsonToSend);

  }

  function confirmReservation($link, $id, $usuario){

    // TODO JSON TO SEND
    $jsonToSend = null;

    // TODO QUERIES

    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');

    $agentsQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
    $row_agents = $agentsQuery->fetch_assoc();

    $managerQuery = $link->query("SELECT* FROM manager WHERE id_m = $row_agents[id_m]");
    $row_manager = $managerQuery->fetch_assoc();

    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id'");
    $row_homestay = $homestayQuery->fetch_assoc();

    $roomHomestayQuery = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$row_homestay[mail_h]' and room.id_home = pe_home.id_home");
    $rowRoomHomestay=$roomHomestayQuery->fetch_assoc();

    $notificationQuery = $link->query("SELECT * FROM notification WHERE id_not = '$_POST[id_noti]'");
    $row_notification = $notificationQuery->fetch_assoc();

    $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_notification[user_i_mail]'");
    $row_student = $studentQuery->fetch_assoc();


    // TODO ASSIGN VALUES TO VARIABLES

    // ? MANAGER
    $m_a_name = strstr($rowManager['a_name'], ' ', true);
    $m_l_a_name = strstr($rowManager['a_name'], ' ');
    $m_name = $row_manager['name'];
    $m_lname = $row_manager['l_name'];
    $m_mail = $row_manager['mail'];
    $m_id = $row_manager['id_m'];

    // ? STUDENT
    $s_firstd = $row_student['firstd'];
    $s_lastd = $row_student['lastd'];
    $s_mail = $row_student['mail_s'];
    $s_fullname = $row_student['name_s'] . ' ' . $row_student['l_name_s'];

    
    // ? HOMESTAY
    $h_mail_h = $row_homestay['mail_h'];
    $h_nameHouse = $row_homestay['h_name'];
    $h_name = $row_homestay['name_h'];
    $h_l_name = $row_homestay['l_name_h'];

    $type1 = $rowRoomHomestay['type1'];
    $type2 = $rowRoomHomestay['type2'];
    $type3 = $rowRoomHomestay['type3'];
    $type4 = $rowRoomHomestay['type4'];
    $type5 = $rowRoomHomestay['type5'];
    $type6 = $rowRoomHomestay['type6'];
    $type7 = $rowRoomHomestay['type7'];
    $type8 = $rowRoomHomestay['type8'];
    
    $reservations1 = $rowRoomHomestay['reservations1'];
    $reservations2 = $rowRoomHomestay['reservations2'];
    $reservations3 = $rowRoomHomestay['reservations3'];
    $reservations4 = $rowRoomHomestay['reservations4'];
    $reservations5 = $rowRoomHomestay['reservations5'];
    $reservations6 = $rowRoomHomestay['reservations6'];
    $reservations7 = $rowRoomHomestay['reservations7'];
    $reservations8 = $rowRoomHomestay['reservations8'];
    
    // ? TITILE
    $title = 'Reservation Provider';
    
    // ? NOTIFICATION
    $bedrooms = $row_notification['bedrooms'];
    $bed = $row_notification['des'];
    if($row_notification['bedrooms'] == '1') {
      $color = '#232159';
      $room_e = 'room1';
    }
    else if($row_notification['bedrooms'] == '2') {
      $color = '#982A72';
      $room_e = 'room2';
    }
    else if($row_notification['bedrooms'] == '3') {
      $color = '#394893';
      $room_e = 'room3';
    }
    else if($row_notification['bedrooms'] == '4') {
      $color = '#A54483';
      $room_e = 'room4';
    }
    else if($row_notification['bedrooms'] == '5') {
      $color = '#5D418D';
      $room_e = 'room5';
    }
    else if($row_notification['bedrooms'] == '6') {
      $color = '#392B84';
      $room_e = 'room6';
    }
    else if($row_notification['bedrooms'] == '7') {
      $color = '#B15391';
      $room_e = 'room7';
    }
    else if($row_notification['bedrooms'] == '8') {
      $color = '#4F177D';
      $room_e = 'room8';
    }

    if($_POST['button'] == 'Confirm'){
      
      // TODO INSERT IN DB
      if($s_lastd > $date) {
        $status = 'Active';

        $updateStudent = "UPDATE pe_student SET status = 'Homestay Found' WHERE mail_s = '$s_mail'";
        $result_updateStudent = $link->query($updateStudent);
      }else $status = 'Disabled';

      $updateNotification = "UPDATE notification SET user_i = '$m_name', user_i_l = '$m_lname', user_i_mail = '$m_mail', date_ = '$date', state = '0', confirmed = '0', title = '$title', des = '$s_mail' WHERE id_not = '$_POST[id_noti]'";
      $result_updateNotification = $link->query($updateNotification);
      
      $insertEvents = "INSERT INTO events (title, start, startingDay, end, endingDay, bed, color, room_e, email, mail_s, height, id_m, status) VALUES ('$s_fullname', '$s_firstd', 'true', '$s_lastd', 'true', '$bed', '$color', '$room_e', '$h_mail_h', '$s_mail', '80', '$m_id', '$status')";
      $result_insertEvents = $link->query($insertEvents);
      
      $insertNotiStudent = "INSERT INTO noti_student (h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, des) VALUES ('$h_nameHouse', '$h_name', '$h_l_name', '$h_mail_h', '$s_mail', '$date', '0', '0', 'Student Confirmed Provider')";
      $result_insertNotiStudent = $link->query($insertNotiStudent);
      
      if($status == 'Active' && $row_notification['bedrooms'] == '1'){
    
        $reservations = $reservations1 + 1;

        if($type1 == 'Single' && $reservations == '1' || $type1 == 'Share' && $reservations == '2' || $type1 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations1 = '$reservations', date1 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '2'){
        
        $reservations = $reservations2 + 1;

        if($type2 == 'Single' && $reservations == '1' || $type2 == 'Share' && $reservations == '2' || $type2 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations2 = '$reservations', date2 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '3'){
        
        $reservations = $reservations3 + 1;

        if($type3 == 'Single' && $reservations == '1' || $type3 == 'Share' && $reservations == '2' || $type3 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations3 = '$reservations', date3 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '4'){
        
        $reservations = $reservations4 + 1;

        if($type4 == 'Single' && $reservations == '1' || $type4 == 'Share' && $reservations == '2' || $type4 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations4 = '$reservations', date4 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '5'){
        
        $reservations = $reservations5 + 1;

        if($type5 == 'Single' && $reservations == '1' || $type5 == 'Share' && $reservations == '2' || $type5 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations5 = '$reservations', date5 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '6'){
        
        $reservations = $reservations6 + 1;

        if($type6 == 'Single' && $reservations == '1' || $type6 == 'Share' && $reservations == '2' || $type6 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations6 = '$reservations', date6 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '7'){
        
        $reservations = $reservations7 + 1;

        if($type7 == 'Single' && $reservations == '1' || $type7 == 'Share' && $reservations == '2' || $type7 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations7 = '$reservations', date7 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      if($status == 'Active' && $row_notification['bedrooms'] == '8'){
        
        $reservations = $reservations8 + 1;

        if($type8 == 'Single' && $reservations == '1' || $type8 == 'Share' && $reservations == '2' || $type8 == 'Executive' && $reservations == '1'){
          $availability = 'Occupied';
        }else{
          $availability = 'Avalible';
        }
        
        $updateRoom = "UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET reservations8 = '$reservations', date8 = '$availability'";
        $result_updateRoom = $link->query($updateRoom);
        
      }

      $notificationQuery = $link->query("SELECT * FROM notification WHERE user_i_mail != '$row_manager[mail]' AND user_r = '$row_homestay[mail_h]' AND title = 'Reservation Request' AND confirmed = '0'");
      $num_rows_notification = mysqli_num_rows($notificationQuery);

      // TODO JSON VALUES
      $jsonToSend[] = array(
          'button' => $_POST['button'],
          'num_notification' => $num_rows_notification,
          'id_noti' => $_POST['id_noti'],
        );

    }else if($_POST['button'] == 'Cancel'){

      // TODO INSERT / UPDATE ON DB

      $updateNotification = "UPDATE notification SET state = '2', confirmed = '1' WHERE id_not = '$_POST[id_noti]'";
      $result_updateNotification = $link->query($updateNotification);

      $insertNotification = "INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, report_s, reserve_h, status, extend) VALUES ('$m_a_name', '$m_l_a_name', '$s_mail', 'NULL', 'NULL', 'NULL', 'NULL', '$h_mail_h', '$date', '0', '0', 'Reservation Rejected Provider', 'NULL', 'NULL', 'NULL', 'No')";
      $result_insertNotification = $link->query($insertNotification);

      $insertNotiStudent = "INSERT INTO noti_student (h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, des) VALUES ('$h_nameHouse', '$h_name', '$h_l_name', '$h_mail_h', '$s_mail', '$date', '0', '0', 'Student Rejected Provider')";
      $result_insertNotiStudent = $link->query($insertNotiStudent);

      $updateStudent = "UPDATE pe_student SET status = 'Student for Select House' WHERE mail_s = '$s_mail'";
      $result_updateStudent = $link->query($updateStudent);

      $notificationQuery = $link->query("SELECT * FROM notification WHERE user_i_mail != '$row_manager[mail]' AND user_r = '$row_homestay[mail_h]' AND title = 'Reservation Request' AND confirmed = '0'");
      $num_rows_notification = mysqli_num_rows($notificationQuery);

      // TODO JSON VALUES
      $jsonToSend[] = array(
          'button' => $_POST['button'],
          'id_noti' => $_POST['id_noti'],
          'mail_s' => $s_mail
        );
    }

    echo json_encode($jsonToSend);

  }
  
  function homestayActiveReservation($link, $id, $usuario){

    // TODO JSON A ENVIAR
    $jsonToSend = null;

    // TODO VALIDACIONES
    if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

    // TODO CONSULTAS

    $agentsQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
    $row_agents = $agentsQuery->fetch_assoc();

    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id'");
    $row_homestay = $homestayQuery->fetch_assoc();

    $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$row_homestay[mail_h]' AND status = 'Active' ORDER BY id DESC");
    $num_rows_events = mysqli_num_rows($eventsQuery);

    if($num_rows_events > 0){
      while ($row_events = mysqli_fetch_array($eventsQuery)){

        $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_events[mail_s]'");
        $row_student = $studentQuery->fetch_assoc();

        if($row_events['color'] == '#232159') $room = '1';
        else if($row_events['color'] == '#982A72') $room = '2';
        else if($row_events['color'] == '#394893') $room = '3';
        else if($row_events['color'] == '#A54483') $room = '4';
        else if($row_events['color'] == '#5D418D') $room = '5';
        else if($row_events['color'] == '#392B84') $room = '6';
        else if($row_events['color'] == '#B15391') $room = '7';
        else if($row_events['color'] == '#4F177D') $room = '8';

        if($row_events['start'] == 'NULL') $start = 'Empty';
        else {
          $date_start = date_create($row_events['start']);
          $start = date_format($date_start, 'Y/m/d');
        }

        if($row_events['end'] == 'NULL') $end = 'Empty';
        else{
          $date_end = date_create($row_events['end']);
          $end = date_format($date_end, 'Y/m/d');
        }

        if($row_events['bed'] == 'A') $bed = '1'; 
        else if($row_events['bed'] == 'B') $bed = '2'; 
        else if($row_events['bed'] == 'C') $bed = '3'; 

        $jsonToSend[] = array(
          'id_student' => $row_student['id_student'],
          'fullname' => $row_student['name_s']. ' ' . $row_student['l_name_s'],
          'photo_s' => $row_student['photo_s'],
          'mail_s' => $row_events['mail_s'],
          'start' => $start,
          'room' => $room,
          'end' => $end,
          'bed' => $bed,
        );
      }
    }else{
      $jsonToSend[] = array(
        'id_student' => 'Empty',
      );
    }

    echo json_encode($jsonToSend);

  }

  function familyMemberList($link, $id, $usuario){
    
    // TODO JSON TO SEND
    $jsonToSend = null;

    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$_POST[id_home]'");
    $row_homestayQuery = $homestayQuery->fetch_assoc();

    $mem_fQuery = $link->query("SELECT * FROM mem_f WHERE id_home = '$_POST[id_home]'");
    $row_mem_fQuery = $mem_fQuery->fetch_assoc();

    // TODO ASIGN VARIABLES

    $familyMember = 1;

    foreach ($row_mem_fQuery as $key => $value) {


      
      if($row_mem_fQuery['f_name' . $familyMember] != 'NULL'){

        if($row_mem_fQuery  ['db' . $familyMember] != 'NULL'){

          $date_row_mem_fQuery = date_create($row_mem_fQuery  ['db' . $familyMember]);
          $dateOfBirth = date_format($date_row_mem_fQuery, 'jS M \of\ Y');

        }else{
          $dateOfBirth = 'Empty';
        }

        if ($row_mem_fQuery['f_name' . $familyMember] == "NULL" &&$row_mem_fQuery['f_lname' . $familyMember] == "NULL") $memberName = "Empty";
        else if ($row_mem_fQuery['f_name' . $familyMember] == "NULL" &&$row_mem_fQuery['f_lname' . $familyMember] != "NULL") $memberName =$row_mem_fQuery['f_lname' . $familyMember];
        else if ($row_mem_fQuery['f_name' . $familyMember] != "NULL" &&$row_mem_fQuery['f_lname' . $familyMember] == "NULL") $memberName = $row_mem_fQuery['f_name' . $familyMember];
        else $memberName = $row_mem_fQuery['f_name' . $familyMember] . ' ' . $row_mem_fQuery['f_lname' . $familyMember];

        if($row_mem_fQuery['gender' . $familyMember] == 'NULL') $gender = 'Empty';
        else $gender = $row_mem_fQuery['gender' . $familyMember];

        if($row_mem_fQuery['re' . $familyMember] == 'NULL') $relation = 'Empty';
        else $relation = $row_mem_fQuery['re' . $familyMember];

        if($row_mem_fQuery['occupation_f' . $familyMember] == 'NULL') $occupation = 'Empty';
        else $occupation = $row_mem_fQuery['occupation_f' . $familyMember];

        if($row_mem_fQuery['db_lawf' . $familyMember] == 'NULL') $db_law = 'Empty';
        else {
          $date_db_law = date_create($row_mem_fQuery  ['db_lawf' . $familyMember]);
          $db_law = date_format($date_db_law, 'jS M \of\ Y');
        }

        if($row_mem_fQuery['lawf' . $familyMember] == 'NULL') $lawf = 'Empty';
        else $lawf = $row_mem_fQuery['lawf' . $familyMember];

        // ** ROOM DATA
        $jsonToSend[] = array(
          'm_name' => $memberName,
          'db_member' => $dateOfBirth,
          'gender' => $gender,
          'relation' => $relation,
          'occupation' => $occupation,
          'db_lawf' => $db_law,
          'lawf' => $lawf,
        );
      }

      $familyMember++;
      if ($familyMember > 8) break;
    }

    echo json_encode($jsonToSend);
  }

  function activityLogs($link, $id, $usuario){

    // TODO JSON TO SEND
    $jsonLog = null;

    // TODO QUERIES LOGS
    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id'");
    $row_homestay = $homestayQuery->fetch_assoc();

    $webmasterQuery = $link->query("SELECT * FROM webmaster WHERE user = '$row_homestay[mail_h]' OR edit_user = '$row_homestay[mail_h]' ORDER BY id_act DESC");

    while($row_webmaster = mysqli_fetch_array($webmasterQuery)){

      if($row_webmaster['report_s'] != 'NULL' || !empty($row_webmaster['report_s'])){
        $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_webmaster[report_s]'");
        $row_student = $studentQuery->fetch_assoc();

        if($row_student['l_name_s'] == 'NULL' && $row_student['name_s'] != 'NULL') $fullnames = $row_student['name_s'];
        else if($row_student['name_s'] == 'NULL' && $row_student['l_name_s'] != 'NULL') {
          $fullnames = $row_student['l_name_s'];
        }
        else if($row_student['l_name_s'] != 'NULL' && $row_student['name_s'] != 'NULL') {
          $fullnames = $row_student['name_s'] . ' ' . $row_student['l_name_s'];
        }
        else $fullnames = "Empty";
      }else $fullnames = "Empty";

      if($row_webmaster['user'] != 'NULL' || !empty($row_webmaster['user'])){
        $homestayQuery = $link->query("SELECT * FROM pe_home WHERE mail_h = '$row_webmaster[user]'");
        $row_homestaylogs = $homestayQuery->fetch_assoc();

        $creatorUser = $row_homestaylogs['h_name'];
        $link = 'yes';
        $idUser = "detail?art_id=".$row_homestaylogs['id_home'];

        if(empty($creatorUser)){
          $managerQuery = $link->query("SELECT * FROM manager WHERE mail = '$row_webmaster[user]'");
          $row_manager = $managerQuery->fetch_assoc();

          $creatorUser = $row_manager['a_name'];
          $link = 'no';
          $idUser = $row_manager['id_m'];

        }
        if(empty($creatorUser)){
          $studentQueryUser = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_webmaster[user]'");
          $row_studentUser = $studentQueryUser->fetch_assoc();

          $creatorUser = $row_studentUser['name_s'] . ' ' . $row_studentUser['l_name_s'];
          $link = 'yes';
          $idUser = "student_info?art_id=".$row_studentUser['id_student'];
        }
        if(empty($creatorUser)){
          $agentQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$row_webmaster[user]'");
          $row_agents = $agentQuery->fetch_assoc();

          $creatorUser = $row_agents['name'] . ' ' . $row_agents['l_name'];
          $link = 'yes';
          $idUser = "edit_agent";
        }
      }

      if(empty($row_webmaster['activity']) || $row_webmaster['activity'] == 'NULL') $activity = 'Empty';
      else $activity = $row_webmaster['activity'];

      if(empty($row_webmaster['dates']) || $row_webmaster['dates'] == 'NULL'){
      }else{
        $dateLog = date_create($row_webmaster['dates']);
        $dates = date_format($dateLog, 'Y/m/d h:i A');
      }

      if($row_webmaster['edit_user'] != 'NULL' || !empty($row_webmaster['edit_user'])){
        $homestayQuery = $link->query("SELECT * FROM pe_home WHERE mail_h = '$row_webmaster[edit_user]'");
        $row_homestaylogs = $homestayQuery->fetch_assoc();

        $receivingUser = $row_homestaylogs['h_name'];

        if(empty($receivingUser)){
          $managerQuery = $link->query("SELECT * FROM manager WHERE mail = '$row_webmaster[edit_user]'");
          $row_manager = $managerQuery->fetch_assoc();

          $receivingUser = $row_manager['a_name'];
        }
        if(empty($receivingUser)){
          $studentQueryUser = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_webmaster[edit_user]'");
          $row_studentUser = $studentQueryUser->fetch_assoc();

          $receivingUser = $row_studentUser['name_s'] . ' ' . $row_studentUser['l_name_s'];
        }
        if(empty($receivingUser)){
          $agentQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$row_webmaster[edit_user]'");
          $row_agents = $agentQuery->fetch_assoc();

          $receivingUser = $row_agents['name'] . ' ' . $row_agents['l_name'];
        }
      }


      $jsonLog[] = array(
        'createUser' => $creatorUser,
        'createUserId' => $idUser,
        'link' => $link,
        'activity' => $activity,
        'date' => $dates,
        'receivingUser' => $receivingUser,
        'studentReport' => $fullnames,
        'reasonActivity' => $row_webmaster['reason'],
      );
    }

    echo json_encode($jsonLog);
  }

  // TODO RESPUESTAS
  if ($_POST['request'] == 'homestayDetails') homestayDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayCoordinator') coordinatorDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayRooms') roomsDetails($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayPendingReservation') homestayPendingReservation($link, $id, $usuario);
  else if ($_POST['request'] == 'confirmReservation') confirmReservation($link, $id, $usuario);
  else if ($_POST['request'] == 'homestayActiveReservation') homestayActiveReservation($link, $id, $usuario);
  else if ($_POST['request'] == 'familyMemberList') familyMemberList($link, $id, $usuario);
  else if ($_POST['request'] == 'activityLogs') activityLogs($link, $id, $usuario);
  else echo json_encode("No hay ninguna petición");
}