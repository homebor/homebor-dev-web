<?php 

include '../xeon.php';
session_start();

// TODO USER VARIABLE
$usuario = $_SESSION['username'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d');
$dateM = date('Y-m-d H:i:s');

// TODO QUERY AGENT
$agentQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
$row_agent = $agentQuery->fetch_assoc();

if($_POST['request'] == 'getHouses'){

  $propertieQuery = $link->query("SELECT * FROM propertie_control WHERE id_m = '$row_agent[id_m]' AND status = 'Available' ORDER BY id_p DESC");

  while ($row_propertie = mysqli_fetch_array($propertieQuery)){
    $houseQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$row_propertie[id_home]'");
    $row_house = $houseQuery->fetch_assoc();

    $roomQuery = $link->query("SELECT * FROM room WHERE id_home = '$row_propertie[id_home]'");
    $row_room = $roomQuery->fetch_assoc();

    if(empty($row_house['phome']) || $row_house['phome'] == 'NULL') $h_photo = "assets/emptys/frontage-empty.png";
    else $h_photo = $row_house['phome'];
    
    if(empty($row_house['status']) || $row_house['status'] == 'NULL') $h_status = "Empty";
    else $h_status = $row_house['status'];

    if(empty($row_house['h_name']) || $row_house['h_name'] == 'NULL') $h_name = "Empty";
    else $h_name = $row_house['h_name'];

    if(empty($row_house['m_city']) || $row_house['m_city'] == 'NULL') $h_mcity = 'Empty';
    else $h_mcity = $row_house['m_city'];
    
    $allhouses[] = array(
      'h_id' => $row_house['id_home'],
      'h_photo' => $h_photo,
      'h_status' => $h_status,
      'h_name' => $h_name,
      'h_mcity' => $h_mcity,
      'h_room' => $row_room,
    );
  }


  echo json_encode($allhouses);
}

if($_POST['request'] == 'changeReserve'){
  // TODO VARIABLE HOUSE BEFORE
  $h_idBefore = $_POST['id-house-before'];
  
  // TODO VARIABLE HOUSE
  $h_idAfter = $_POST['id-house'];
  
  // TODO VARIABLE STUDENT
  $s_id = $_POST['id-student'];

  // TODO DATE / REASON
  if(empty($_POST['date-change-reserve'])) $dateChange = 'NULL';
  else{
    $d_dateChange = date_create($_POST['date-change-reserve']);
    $dateChange = date_format($d_dateChange, 'Y-m-d');
  }

  if(empty($_POST['reason-change-reserve'])) $reasonChange = 'NULL';
  else $reasonChange = addslashes($_POST['reason-change-reserve']);

  // TODO QUERY HOUSE BEFORE
  $houseBeforeQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$h_idBefore'");
  $row_houseBefore = $houseBeforeQuery->fetch_assoc();
  
  $roomBeforeQuery = $link->query("SELECT * FROM room WHERE id_home = '$h_idBefore'");
  $row_roomBefore = $roomBeforeQuery->fetch_assoc();
  
  // TODO QUERY HOUSE AFTER
  $houseAfterQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$h_idAfter'");
  $row_houseAfter = $houseAfterQuery->fetch_assoc();
  
  $roomAfterQuery = $link->query("SELECT * FROM room WHERE id_home = '$h_idAfter'");
  $row_roomAfter = $roomAfterQuery->fetch_assoc();

  // TODO QUERY STUDENT
  $studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$s_id'");
  $row_student = $studentQuery->fetch_assoc();

  // TODO QUERY MANAGER
  $managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_student[id_m]'");
  $row_manager = $managerQuery->fetch_assoc();

  // TODO QUERY EVENTS
  $eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$row_student[mail_s]' AND status = 'Active' ORDER BY id DESC LIMIT 1");
  $row_events = $eventsQuery->fetch_assoc();

  if($row_student['name_s'] == 'NULL') $s_fullnames = $row_student['l_name_s'];
  else if($row_student['l_name_s'] == 'NULL') $s_fullnames = $row_student['name_s'];
  else $s_fullnames = $row_student['name_s'] .' '. $row_student['l_name_s'];

  if(strpos($row_manager['a_name'], " ")){
    $m_a_name = strstr($row_manager['a_name'], ' ', true);
    $m_l_a_name = strstr($row_manager['a_name'], ' ');
  }else{
    $m_a_name = $row_manager['a_name'];
    $m_l_a_name = "";
  }

  $bedHouseAfter = $_POST['id-room'];

  if($_POST['id-room'] == 'bed1' || $_POST['id-room'] == 'bed1_2' || $_POST['id-room'] == 'bed1_3'){
    $room = "1";
    $color = "#232159";
    $room_e = 'room1';
    $reservation = "reservations1";
    if($_POST['id-room'] == 'bed1') {
      $availability = 'date1';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed1_2') {
      $availability = 'date1_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed1_3') {
      $availability = 'date1_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations1'] + 1;
  }
  else if($_POST['id-room'] == 'bed2' || $_POST['id-room'] == 'bed2_2' || $_POST['id-room'] == 'bed2_3'){
    $room = "2";
    $color = "#982A72";
    $room_e = 'room2';
    $reservation = "reservations2";
    if($_POST['id-room'] == 'bed2') {
      $availability = 'date2';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed2_2') {
      $availability = 'date2_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed2_3') {
      $availability = 'date2_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations2'] + 1;
  }
  else if($_POST['id-room'] == 'bed3' || $_POST['id-room'] == 'bed3_2' || $_POST['id-room'] == 'bed3_3'){
    $room = "3";
    $color = "#394893";
    $room_e = 'room3';
    $reservation = "reservations3";
    if($_POST['id-room'] == 'bed3') {
      $availability = 'date3';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed3_2') {
      $availability = 'date3_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed3_3') {
      $availability = 'date3_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations3'] + 1;
  }
  else if($_POST['id-room'] == 'bed4' || $_POST['id-room'] == 'bed4_2' || $_POST['id-room'] == 'bed4_3'){
    $room = "4";
    $color = "#A54483";
    $room_e = 'room4';
    $reservation = "reservations4";
    if($_POST['id-room'] == 'bed4') {
      $availability = 'date4';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed4_2') {
      $availability = 'date4_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed4_3') {
      $availability = 'date4_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations4'] + 1;
  }
  else if($_POST['id-room'] == 'bed5' || $_POST['id-room'] == 'bed5_2' || $_POST['id-room'] == 'bed5_3'){
    $room = "5";
    $color = "#5D418D";
    $room_e = 'room5';
    $reservation = "reservations5";
    if($_POST['id-room'] == 'bed5') {
      $availability = 'date5';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed5_2') {
      $availability = 'date5_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed5_3') {
      $availability = 'date5_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations5'] + 1;
  }
  else if($_POST['id-room'] == 'bed6' || $_POST['id-room'] == 'bed6_2' || $_POST['id-room'] == 'bed6_3'){
    $room = "6";
    $color = "#392B84";
    $room_e = 'room6';
    $reservation = "reservations6";
    if($_POST['id-room'] == 'bed6') {
      $availability = 'date6';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed6_2') {
      $availability = 'date6_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed6_3') {
      $availability = 'date6_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations6'] + 1;
  }
  else if($_POST['id-room'] == 'bed7' || $_POST['id-room'] == 'bed7_2' || $_POST['id-room'] == 'bed7_3'){
    $room = "7";
    $color = "#B15391";
    $room_e = 'room7';
    $reservation = "reservations7";
    if($_POST['id-room'] == 'bed7') {
      $availability = 'date7';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed7_2') {
      $availability = 'date7_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed7_3') {
      $availability = 'date7_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations7'] + 1;
  }
  else if($_POST['id-room'] == 'bed8' || $_POST['id-room'] == 'bed8_2' || $_POST['id-room'] == 'bed8_3'){
    $room = "8";
    $color = "#4F177D";
    $room_e = 'room8';
    $reservation = "reservations8";
    if($_POST['id-room'] == 'bed8') {
      $availability = 'date8';
      $bed = "A";
    }
    if($_POST['id-room'] == 'bed8_2') {
      $availability = 'date8_2';
      $bed = "B";
    }
    if($_POST['id-room'] == 'bed8_3') {
      $availability = 'date8_3';
      $bed = "C";
    }

    $num_reserveAfter = $row_roomAfter['reservations8'] + 1;
  }

  if($row_events['room_e'] == 'room1') {
    $reservationBefore = "reservations1";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date1";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date1_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date1_3";
    if($row_roomBefore['reservations1'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations1'] - 1;
  }
  else if($row_events['room_e'] == 'room2') {
    $reservationBefore = "reservations2";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date2";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date2_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date2_3";
    if($row_roomBefore['reservations2'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations2'] - 1;
  }
  else if($row_events['room_e'] == 'room3') {
    $reservationBefore = "reservations3";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date3";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date3_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date3_3";
    if($row_roomBefore['reservations3'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations3'] - 1;
  }
  else if($row_events['room_e'] == 'room4') {
    $reservationBefore = "reservations4";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date4";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date4_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date4_3";
    if($row_roomBefore['reservations4'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations4'] - 1;
  }
  else if($row_events['room_e'] == 'room5') {
    $reservationBefore = "reservations5";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date5";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date5_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date5_3";
    if($row_roomBefore['reservations5'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations5'] - 1;
  }
  else if($row_events['room_e'] == 'room6') {
    $reservationBefore = "reservations6";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date6";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date6_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date6_3";
    if($row_roomBefore['reservations6'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations6'] - 1;
  }
  else if($row_events['room_e'] == 'room7') {
    $reservationBefore = "reservations7";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date7";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date7_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date7_3";
    if($row_roomBefore['reservations7'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations7'] - 1;
  }
  else if($row_events['room_e'] == 'room8') {
    $reservationBefore = "reservations8";
    if($row_events['bed'] == 'A') $bedHouseBefore = "date8";
    if($row_events['bed'] == 'B') $bedHouseBefore = "date8_2";
    if($row_events['bed'] == 'C') $bedHouseBefore = "date8_3";
    if($row_roomBefore['reservations8'] == "0") $num_reserveBefore = 0;
    else $num_reserveBefore = $row_roomBefore['reservations8'] - 1;
  }

  // TODO INSERT NOTIFICATION
    $insertNotificatioHousesBefore = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, extend) VALUES ('$m_a_name', '$m_l_a_name', '$row_student[mail_s]', '$row_student[firstd]', '$dateChange', 'NULL', '$row_events[color]', '$row_houseBefore[mail_h]', '$dateM', '0', '0', 'Reservation Change Before', '$row_events[bed]', '$row_student[l_name_s] $row_student[name_s]', '$row_manager[mail]', 'No')");

    $insertNotificatioHousesAfter = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, extend) VALUES ('$m_a_name', '$m_l_a_name', '$row_student[mail_s]', '$dateChange', '$row_student[lastd]', '$room', '$color', '$row_houseAfter[mail_h]', '$dateM', '0', '0', 'Reservation Change After', '$bed', '$row_student[l_name_s] $row_student[name_s]', '$row_student[mail_s]', 'No')");
    
    $insertNotificationProvider = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, extend) VALUES ('$row_agent[name]', '$row_agent[l_name]', '$row_agent[a_mail]', '$dateChange', '$row_student[lastd]', '$room', '$color', '$row_manager[mail]', '$dateM', '0', '0', 'Change Student', '$bed', '$row_student[mail_s]', '$row_houseAfter[mail_h]', 'No')");

    $insertNotiStudent = $link->query("INSERT INTO noti_student (h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, change_house, des) VALUES ('$row_houseAfter[h_name]', '$m_a_name', '$m_l_a_name', '$row_manager[mail]', '$row_student[mail_s]', '$dateM', '0', '0', '$row_houseAfter[mail_h]', 'Reservation Change')");

  // TODO UPDATE / INSERT EVENT
    $updateEventOld = $link->query("UPDATE events SET end = '$dateChange' WHERE id = '$row_events[id]'");
    if($date > $dateChange) 
      $updateEventOld2 = $link->query("UPDATE events SET status = 'Disabled' WHERE id = '$row_events[id]'");
    
    $eventsQueryNew = $link->query("SELECT * FROM events WHERE id ='$row_events[id]'");
    $row_newEvents = $eventsQueryNew->fetch_assoc();

    if($row_newEvents['start'] == $row_newEvents['end']){
      $deleteEvent = $link->query("DELETE FROM events WHERE id = '$row_newEvents[id]'");
    }
    $insertNewEvent = $link->query("INSERT INTO events (title, start, startingDay, end, endingDay, bed, color, room_e, email, mail_s, height, id_m, status) VALUES ('$s_fullnames', '$dateChange', 'true', '$row_student[lastd]', 'true', '$bed', '$color', '$room_e', '$row_houseAfter[mail_h]', '$row_student[mail_s]', '80', '$row_student[id_m]', 'Active')");

  // TODO UPDATE ROOM
    $updateRoomBefore = $link->query("UPDATE room SET $reservationBefore = '$num_reserveBefore', $bedHouseBefore = 'Available' WHERE id_home = '$row_houseBefore[id_home]'");

    $updateRoomAfter = $link->query("UPDATE room SET $reservation = '$num_reserveAfter', $availability = 'Occupied' WHERE id_home = '$row_houseAfter[id_home]'");
  
  // TODO INSET WEBMASTER
    $insertWebmasterBefore = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m, reason, report_s) VALUES ('$usuario', 'Reservation Change Before', '$dateM', '$row_houseBefore[mail_h]', '$row_agent[id_m]', '$reasonChange', '$row_student[mail_s]')");
    
    $insertWebmasterAfter = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m, reason, report_s) VALUES ('$usuario', 'Reservation Change After', '$dateM', '$row_houseAfter[mail_h]', '$row_agent[id_m]', '$reasonChange', '$row_student[mail_s]')");

  if(insertNewEvent)
    $response = "Success";
  else $response = "Error";

  echo json_encode($response);
}

?>