<?php
include_once('../xeon.php');
session_start();
error_reporting(0);
// Almacenando la variable de sesión
$usuario = $_SESSION['username'];

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <!-- //TODO DATERANGEPICKER -->
  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js">
  </script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <!--// TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.15">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.15">
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/edit_students2.css?ver=1.0.15"> -->
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.15">
  <link rel="stylesheet" href="assets/css/directory_students.css?ver=1.0.15">

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js?ver=1.0.15"></script> -->

  <!--// TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students Directory</title>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js?ver=1.0.15"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css?ver=1.0.15"> -->
</head>

<!-- // TODO BODY -->

<body>

  <!-- // TODO HEADER -->
  <?php include 'header.php'; ?>

  <!-- // TODO MAIN -->
  <br><br><br><br><br><br>
  <main class="container-fluid col-12 col-sm-10">
    <!-- // ? SEARCH STUDENTS -->
    <section id="search-container" class="search-container">
      <article class="col-12 mx-auto d-flex justify-content-center">
        <label for="search-student" class="d-flex m-0 p-0 align-items-center fa fa-search"></label>
        <input type="text" id="search-student" placeholder="Insert student name or email">
        <button class="d-lg-none ml-3 py-2 px-3 d-flex btn btn-filter"><i class="icon-filter"></i></button>
      </article>
    </section>

    <br><br><br>

    <!-- // ? ALL STUDENTS SECTION CONTAINER -->
    <section class="students-section">

      <!-- // * ALL FILTERS -->
      <section id="all-filters" class="d-flex flex-column">
        <article id="filter-tags" class="w-100 px-4 px-lg-0 mx-auto row filter-tags position-relative">
          <h3 class="w-100 py-3 py-lg-0 px-lg-3 mb-2 d-flex justify-content-between font-weight-bold">
            Filter Students by
            <span class="d-lg-none hide-filters">X</span>
          </h3>

          <div class="col-12 d-none justify-content-center align-items-center mb-0 mt-4">
            <button class="btn clear-all-filters">Clear All Filters</button>
          </div>
        </article>

        <hr class="w-100 ">

        <article class="w-100 px-2 px-lg-0 d-flex flex-column justify-content-around">
          <!-- // *** SHOW ALL -->
          <aside class="d-flex flex-lg-column search-status">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-all">
              <input hidden="" type="checkbox" id="search-all" data-filter="status" value="show all">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Show All</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** SITUATION -->
          <aside class="mb-2 d-flex flex-column search-situation">
            <select id="situation" data-filter="situation" class="col-10 col-lg-auto mx-auto custom-select">
              <option value="" hidden>Select Situation</option>
              <option value="confirm registration">Confirm Registration</option>
              <option value="search for homestay">Search for Homestay</option>
              <option value="student for select house">Student for Select House</option>
              <option value="house for accept student">House for Accept Student</option>
              <option value="homestay found">Homestay Found</option>
            </select>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** DESTINATION CITY -->
          <aside class="mb-2 d-flex flex-lg-column search-destination-city">
            <select id="destination-city" data-filter="destinationCity"
              class="col-10 col-lg-auto mx-auto custom-select text-capitalize">
              <option value="" hidden>Select Destination City</option>
            </select>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** DATE RANGE -->
          <aside class="mt-2 mt-lg-0 mb-2 d-flex flex-lg-column search-date-range">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-5-days">
              <input hidden="" type="checkbox" id="search-5-days" data-filter="dateRange" value="1 - 5 days">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">1 - 5 Days</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-7-days">
              <input hidden="" type="checkbox" id="search-7-days" data-filter="dateRange" value="6 - 14 days">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">6 - 14 Days</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-30-days">
              <input hidden="" type="checkbox" id="search-30-days" data-filter="dateRange" value="15 - 30 days">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">15 - 30 Days</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-30-days-onwards">
              <input hidden="" type="checkbox" id="search-30-days-onwards" data-filter="dateRange"
                value="30 days onwards">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">30 Days Onwards</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** DATE FROM TO -->
          <aside class="my-4 my-lg-0 mb-lg-2 d-flex flex-column search-date">
            <input type="text" id="search-date" class="col-6 col-lg-auto mx-auto mx-lg-0 py-4 py-lg-2 text-center"
              placeholder="Select a date" data-filter="date" readonly="">
            <button class="d-none quit-date">X</button>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** TYPE ACCOMMODATION -->
          <aside class="d-flex flex-lg-column search-type-accommodation">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-single">
              <input hidden="" type="checkbox" id="search-single" data-filter="typeAccommodation" value="single">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Single</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share">
              <input hidden="" type="checkbox" id="search-share" data-filter="typeAccommodation" value="share">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Share</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-executive">
              <input hidden="" type="checkbox" id="search-executive" data-filter="typeAccommodation" value="executive">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Executive</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">


          <!-- // *** GENDER -->
          <aside class="mt-2 mt-lg-0 d-flex flex-lg-column search-gender">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-female">
              <input hidden="" type="checkbox" id="search-female" data-filter="gender" value="female">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Female</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-male">
              <input hidden="" type="checkbox" id="search-male" data-filter="gender" value="male">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Male</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-non-binary">
              <input hidden="" type="checkbox" id="search-non-binary" data-filter="gender" value="non-binary">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Non-binary</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block m-0 hide-anim">

          <aside class="mt-2 mt-lg-0 d-flex flex-lg-column search-food-service">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-food-service-yes">
              <input hidden type="checkbox" id="search-food-service-yes" data-filter="food" value="food yes">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Yes</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-food-service-no">
              <input hidden type="checkbox" id="search-food-service-no" data-filter="food" value="food no">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">No</span>
            </label>
          </aside>


          <aside class="mt-2 mt-lg-0 d-flex flex-column search-special-diet">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-vegetarians">
              <input hidden type="checkbox" id="search-vegetarians" data-filter="vegetarians" value="vegetarians">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Vegetarians</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-halal">
              <input hidden type="checkbox" id="search-halal" data-filter="halal" value="halal">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Halal</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-kosher">
              <input hidden type="checkbox" id="search-kosher" data-filter="kosher" value="kosher">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Kosher</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-lactose">
              <input hidden type="checkbox" id="search-lactose" data-filter="lactose" value="lactose">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Lactose</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-gluten">
              <input hidden type="checkbox" id="search-gluten" data-filter="gluten" value="gluten">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Gluten</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pork">
              <input hidden type="checkbox" id="search-pork" data-filter="pork" value="pork">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Pork</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** AGE RANGE -->
          <aside class="mt-2 mt-lg-0 mb-2 d-flex flex-lg-column search-age-range">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-8-years">
              <input hidden="" type="checkbox" id="search-8-years" data-filter="ageRange" value="0 - 8 years">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">0 - 8 Years</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-14-years">
              <input hidden="" type="checkbox" id="search-14-years" data-filter="ageRange" value="8 - 14 years">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">8 - 14 Years</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-21-years">
              <input hidden="" type="checkbox" id="search-21-years" data-filter="ageRange" value="14 - 21 years">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">14 - 21 Years</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-22-years-onwards">
              <input hidden="" type="checkbox" id="search-22-years-onwards" data-filter="ageRange"
                value="22 years onwards">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">22 Years Onwards</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** PICK UP SERVICE -->
          <aside class="mt-2 mt-lg-0 mb-2 d-flex flex-lg-column search-pick-up-service">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pick-up-service-yes">
              <input hidden type="checkbox" id="search-pick-up-service-yes" data-filter="pickUpService"
                value="pick up service yes">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Yes</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-pick-up-service-no">
              <input hidden type="checkbox" id="search-pick-up-service-no" data-filter="pickUpService"
                value="pick up service no">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">No</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** DROP OFF SERVICE -->
          <aside class="mt-2 mt-lg-0 mb-2 d-flex flex-lg-column search-drop-off-service">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-drop-off-service-yes">
              <input hidden type="checkbox" id="search-drop-off-service-yes" data-filter="dropOffService"
                value="drop off service yes">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Yes</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-drop-off-service-no">
              <input hidden type="checkbox" id="search-drop-off-service-no" data-filter="dropOffService"
                value="drop off service no">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">No</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** SCHOOL -->
          <aside class="mb-2 d-flex flex-lg-column search-academy">
            <select id="academy" data-filter="academy" class="col-10 col-lg-auto mx-auto custom-select text-capitalize">
              <option value="" hidden>Select School</option>
            </select>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** LANGUAGE -->
          <aside class="mb-2 d-flex flex-lg-column search-language">
            <select id="language" data-filter="language"
              class="col-10 col-lg-auto mx-auto custom-select text-capitalize">
              <option value="" hidden>Select Language</option>
            </select>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** ANOTHER LANGUAGE -->
          <aside class="mb-2 d-flex flex-lg-column search-another-language">
            <select id="another-language" data-filter="anotherLanguage"
              class="col-10 col-lg-auto mx-auto custom-select text-capitalize">
              <option value="" hidden>Select Another Language</option>
            </select>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** TYPE STUDENT -->
          <aside class="mb-2 row d-lg-flex flex-lg-column mx-auto search-type-student">
            <label class="col-6 col-lg-auto m-0 mb-2 px-2 d-flex align-items-center" for="search-english-and-french">
              <input hidden="" type="checkbox" id="search-english-and-french" data-filter="typeStudent"
                value="english and french">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">English and French Course</span>
            </label>
            <label class="col-6 col-lg-auto m-0 mb-2 px-2 d-flex align-items-center" for="search-high-school">
              <input hidden="" type="checkbox" id="search-high-school" data-filter="typeStudent" value="high school">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">High School</span>
            </label>
            <br class="d-lg-none"><br class="d-lg-none">
            <label class="col-6 col-lg-auto m-0 mb-2 px-2 d-flex align-items-center" for="search-college-university">
              <input hidden="" type="checkbox" id="search-college-university" data-filter="typeStudent"
                value="college university">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">College / University</span>
            </label>
            <label class="col-6 col-lg-auto m-0 mb-2 px-2 d-flex align-items-center" for="search-summer-camps">
              <input hidden="" type="checkbox" id="search-summer-camps" data-filter="typeStudent" value="summer camps">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Summer Camps</span>
            </label>
          </aside>

          <hr class="w-100 d-none d-lg-block">

          <!-- // *** CAN SHARE WITH -->
          <aside class="d-flex flex-lg-column search-can-share-with">
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share-with-smokers">
              <input hidden="" type="checkbox" id="search-share-with-smokers" data-filter="shareWithSmokers"
                value="share with smokers">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Smokers</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share-with-children">
              <input hidden="" type="checkbox" id="search-share-with-children" data-filter="shareWithChildren"
                value="share with children">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Children</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share-with-teenager">
              <input hidden="" type="checkbox" id="search-share-with-teenager" data-filter="shareWithTeenager"
                value="share with teenager">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Teenager</span>
            </label>
            <label class="m-0 mb-2 px-2 d-flex align-items-center" for="search-share-with-pets">
              <input hidden="" type="checkbox" id="search-share-with-pets" data-filter="shareWithPets"
                value="share with pets">
              <svg viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false">
                <path fill="none" d="m4 16.5 8 8 16-16"></path>
              </svg>
              <span class="px-2">Pets</span>
            </label>
          </aside>
          <br class="d-lg-none">
          <br class="d-lg-none">
        </article>
      </section>



      <!-- // * ALL STUDENTS -->
      <section id="all-students" class="d-flex flex-column align-items-center"
        style="opacity: 1; transform: translateY(0px);">


        <!-- // ** SEE MORE STUDENTS -->
        <button class="d-none mt-4 px-3 btn see-more" data-page="0">See More</button>
        <button class="d-none mt-4 px-3 btn show-more">Show More</button>
      </section>


      <!-- // ** TEMPLATES -->
      <template id="student-card-template">
        <div class="d-flex flex-column flex-lg-row mb-5 mb-lg-4 pb-2 pb-lg-0 bg-white card-student" data-perfect="yes"
          data-id-student>
          <!-- // *** CARD HEADER-->
          <div class="p-0 d-flex flex-column align-items-center card-header" data-student-profile title="See profile">
            <img src="../assets/emptys/profile-student-empty.png" class="mx-auto" data-student-image
              alt="student, homestay">
          </div>

          <!-- // *** CARD BODY CONTAINER-->
          <div class="w-100 py-2 px-3 card-body">
            <!-- // **** CARD MAIN INFORMATION-->
            <div class="p-0 main-information">
              <!-- // ***** STUDENT NAME  -->
              <div class="d-flex justify-content-between align-items-center">
                <span class="h4 m-0 text-capitalize" data-student-name>Student Name</span>
                <span class="text-capitalize" data-student-nationality>Empty</span>
              </div>

              <hr class="my-2 pt-1">

              <!-- // ***** ROW 2  -->
              <dl class="m-0 mb-3 d-flex align-items-center">
                <dt>School</dt>
                <dd class="m-0 px-2" data-student-academy>Student School</dd>
              </dl>

              <!-- // ***** ROW 3  -->
              <div class="pb-0 mb-3 d-flex align-items-center justify-content-between">
                <dl class="m-0">
                  <dt>Gender</dt>
                  <dd class="m-0 text-capitalize" data-student-gender>Female</dd>
                </dl>

                <dl class="m-0">
                  <dt class="text-center">Age</dt>
                  <dd class="m-0 text-center" data-student-age>Adult</dd>
                </dl>

                <dl class="m-0">
                  <dt class="text-center">First Day</dt>
                  <dd class="m-0 text-center" data-student-first-day>2022-05-24</dd>
                </dl>

                <dl class="m-0">
                  <dt class="text-center">Last Day</dt>
                  <dd class="m-0 text-center" data-student-last-day>2022-07-11</dd>
                </dl>

                <dl class="m-0">
                  <dt data-student-stay class="text-center">Stay</dt>
                  <dd class="m-0 text-center" data-student-time-stay data-arrival-date>Stay</dd>
                </dl>
              </div>


              <!-- // ***** ROW 4  -->
              <div class="pb-0 mb-0 d-flex align-items-center justify-content-between">
                <dl class="m-0">
                  <dt>Situation</dt>
                  <dd class="m-0 text-capitalize" data-student-situation>Search Homestay</dd>
                </dl>

                <dl class="m-0">
                  <dt>Type Student</dt>
                  <dd class="m-0 text-capitalize" data-student-type-student>High School</dd>
                </dl>

                <dl class="m-0">
                  <dt>Language</dt>
                  <dd class="m-0 text-capitalize" data-student-language>English</dd>
                </dl>

                <dl class="m-0">
                  <dt>Destination City</dt>
                  <dd class="m-0 text-capitalize" data-student-destination-city>Empty</dd>
                </dl>
              </div>

              <!-- // **** SHOW MORE INFO BUTTON  -->
              <button class="ml-auto py-lg-1 shadow btn btn-more-info" data-more-info-button>More Info</button>
            </div>


            <!-- // *** MORE INFO-->
            <div class="pt-2 container more-info-container" data-student-more-info>
              <dt class="py-2">More Info</dt>

              <div class="d-flex mt-4 mt-lg-0 flex-lg-column justify-content-around">
                <div
                  class="mt-3 pb-0 mb-0 d-flex flex-column flex-lg-row align-items-lg-center justify-content-lg-between">
                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Accommodation</dt>
                    <dd class="m-0 text-capitalize" data-student-type-accommodation>Single</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Meal Plan</dt>
                    <dd class="m-0 text-capitalize" data-student-meal-plan>Yes</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Special Diet</dt>
                    <dd class="m-0 text-capitalize" data-student-special-diet data-special-diet>Yes</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Smokers</dt>
                    <dd class="m-0 text-capitalize" data-student-smokers>Yes</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Children</dt>
                    <dd class="m-0 text-capitalize" data-student-children>No</dd>
                  </dl>
                </div>

                <br class="d-none d-lg-block">

                <div
                  class="mt-2 pb-0 mb-0 d-flex flex-column flex-lg-row align-items-lg-center justify-content-lg-between">
                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Teenagers</dt>
                    <dd class="m-0 text-capitalize" data-student-teenagers>Yes</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Pets</dt>
                    <dd class="m-0 text-capitalize" data-student-pets>No</dd>
                  </dl>

                  <dl class="m-0 mb-3 mb-lg-0">
                    <dt>Pick Up Service</dt>
                    <dd class="m-0 text-capitalize" data-student-pick-up>Yes</dd>
                  </dl>

                  <dl class="m-0">
                    <dt>Drop Off Service</dt>
                    <dd class="m-0 text-capitalize" data-student-drop-off>Yes</dd>
                  </dl>
                </div>
              </div>

            </div>
          </div>
        </div>
      </template>
    </section>

    <!-- // TODO MODALS -->
    <modals>
      <!-- // TODO MODAL DYNAMIC -->
      <div id="modal-dynamic" class="modal-dynamic">
        <section class="modal-content">
          <span class="close">X</span>
          <h3>Title</h3>
        </section>
      </div>
    </modals>
  </main>


  <!-- // TODO FOOTER -->
  <?php include 'footer.php'; ?>

  <!-- // TODO SCRIPTS -->
  <script>
  $("#search-date").daterangepicker({
    "autoUpdateInput": false,
    format: "",
    opens: "right"
  }, (start, end, label) => {
    document.querySelector('#search-date').value =
      `${start.format('YYYY/MM/DD')} - ${end.format('YYYY/MM/DD')}`
  });
  </script>

  <!-- // ? AXIOS -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.15"></script>

  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.15"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.15"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.15"></script>
  <script src="assets/js/directory_students.js?ver=1.0.15" type="module"></script>
</body>

</html>