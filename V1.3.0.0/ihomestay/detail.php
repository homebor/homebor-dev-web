<?php
error_reporting(0);
include_once('xeon.php');
if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2=$query2->fetch_assoc();

$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM manager INNER JOIN pe_home ON pe_home.id_home = '$id' and manager.id_m = pe_home.id_m");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM agents INNER JOIN pe_home ON pe_home.id_home = '$id' and agents.id_ag = pe_home.id_ag");
$row6 = $query6->fetch_assoc();

}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/leaflet.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/detail-rooms.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/detail.css">

    <title>Homebor - Detail Preview</title>
    <link  rel="icon"   href="images/icon.png" type="image/png" />

</head>

<body>

<div class="ts-page-wrapper ts-homepage" id="page-top">
 <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->


    <main id="ts-main">


        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">

                <div class="d-block d-sm-flex justify-content-between">

                    <!--Title-->
                    <div class="ts-title mb-0">
                        <br>
                        <br>
                        <br>
                        <br>
                        <h1><?php echo $row['h_name'] ?></h1>
                    </div>

                </div>

            </div>
        </section>

        <!--GALLERY CAROUSEL
            =========================================================================================================-->
        <section id="gallery-carousel">

            <div class="owl-carousel ts-gallery-carousel ts-gallery-carousel__multi" data-owl-dots="1" data-owl-items="3" data-owl-center="1" data-owl-loop="1">

                
                    <?php
                                    
                                     if($row['phome'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="'.$row['phome'].'">
                        <a href="'.$row['phome'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                    
                

                
                    <?php
                                    
                                     if($row4['pliving'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="'.$row4['pliving'].'">
                        <a href="'.$row4['pliving'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

                
                    <?php
                                    
                                     if($row4['parea1'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="'.$row4['parea1'].'">
                        <a href="'.$row4['parea1'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

                
                   <?php
                                    
                                     if($row4['parea2'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="'.$row4['parea2'].'">
                        <a href="'.$row4['parea2'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

                
                    <?php
                                    
                                     if($row4['pbath1'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<!--Slide-->
                <div class="slide">
                <div class="ts-image" data-bg-image="'.$row4['pbath1'].'">
                        <a href="'.$row4['pbath1'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
                    </div>
                    </div>';}
                                    
                                    ?>
                

            </div>

        </section>

        <!--CONTENT
            =========================================================================================================-->
        <section id="content">
            <div class="container">
                <div class="row flex-wrap-reverse">

                    <!--LEFT SIDE
                        =============================================================================================-->
                    <div class="col-md-5 col-lg-4">

                        <!--DETAILS
                            =========================================================================================-->
                        <section>
                            <h3>Details</h3>
                            <div class="ts-box">

                                <dl class="ts-description-list__line mb-0">

                                    <dt>ID:</dt>
                                    <dd>#<?php echo $row['id_home'] ?></dd>

                                    <dt>Category:</dt>
                                    <dd>Homestay</dd>

                                    <dt>Status:</dt>
                                    <dd><?php echo $row['status'] ?></dd>

                                    <dt>Bedrooms:</dt>
                                    <dd><?php echo $row['room'] ?></dd><br>

                                    <dt>Certified:</dt>
                                    <dd><?php echo $row['certified'] ?></dd>

                                    <h4 class="more-in"> <a href="login.php"> Login </a> or <a href="register.php"> Resgister </a> for more info: </h4>

                                </dl>

                            </div>
                        </section>

                        <!--CONTACT THE AGENT
                            =========================================================================================-->

                             <?php
                            if ($row['id_m'] == '0') { echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="assets/logos/7.png"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">Homebor</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            sebi@homebor.com
                                        </p>
                                    </figure>
                                </div>
                                <div class="text-right">
                                <a id="contact" href="register.php" class="btn btn-outline-dark mb-4">Register Now</a>
                                </div>

                            </div>
                        </section>';
                                # code...
                            } else {

                                if ($row['id_ag'] == '0') {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="'.$row5['photo'].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row5['a_name'].'</h5>
                                    </figure>


                                </div>
                                <div class="text-right">
                                <a id="contact" href="register.php" class="btn btn-outline-dark mb-4">Register Now</a>
                                </div>

                            </div>

                        </section>';
                                } else {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="'.$row6['photo'].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row6['agency'].'</h5>
                                        <p>'.$row6['name'].' '.$row6['l_name'].'</p>
                                    </figure>


                                </div>
                                <div class="text-right">
                                <a id="contact" href="register.php" class="btn btn-outline-dark mb-4">Register Now</a>
                                </div>

                            </div>

                        </section>';
                                }
                                
                            }

                            ?>
                        


                    </div>
                    <!--end col-md-4-->

                    <!--RIGHT SIDE
                        =============================================================================================-->
                    <div class="col-md-7 col-lg-8">

                        <!--QUICK INFO
                            =========================================================================================-->
                        <section id="quick-info">
                            <h3>Quick Info</h3>

                            <!--Quick Info-->
                            <div class="ts-quick-info ts-box">

                                <!--Row-->
                                <div class="row no-gutters">

                                    <!--Bedrooms-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/habitacion.png">
                                            <h6>Bedrooms</h6>
                                            <figure><?php echo $row['room'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Gender-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/genero.png">
                                            <h6>Gender Preference</h6>
                                            <figure><?php echo $row['g_pre'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Pets-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/mascotas.png">
                                            <h6>Pets</h6>
                                            <figure><?php echo $row['pet'] ?></figure>
                                        </div>
                                    </div>

                                    <!--Age-->
                                    <div class="col-sm-3">
                                        <div class="ts-quick-info__item" data-bg-image="assets/img/icon-quick-info-garages.png">
                                            <h6>Age of Preference</h6>
                                            <figure><?php echo $row['ag_pre'] ?></figure>
                                        </div>
                                    </div>

                                </div>
                                <!--end row-->

                            </div>
                            <!--end ts-quick-info-->

                        </section>

                        <!--DESCRIPTION
                            =========================================================================================-->
                        <section id="description">

                            <h3>Description</h3>

                            <p>
                               <?php echo $row['des'] ?>
                            </p>

                        </section>

                        <!--FEATURES
                            =========================================================================================-->
                        <section id="features">

                            <h3>Features</h3>

                            <ul class="list-unstyled ts-list-icons ts-column-count-4 ts-column-count-sm-2 ts-column-count-md-2">
                                <li>
                                    <i class="fa fa-university"></i>
                                    <?php echo $row['a_pre'] ?>
                                </li>
                                <li>
                                    <i class="fa fa-female"></i><i class="fa fa-male"></i>
                                    <?php echo $row['g_pre'] ?>
                                </li>
                                <li>
                                    <i class="fa fa-child"></i><i class="fa fa-male"></i>
                                    <?php echo $row['ag_pre'] ?>
                                </li>
                            </ul>

                        </section>


                        <!--FLOOR PLANS
                            =========================================================================================-->
                        <section id="floor-plans">

                            <h3>Bedrooms Info</h3>


                            <?php 

                                $usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
                                $u=$usu->fetch_assoc();

                            ?>

                            <div class="c-log">

                            <?php
                                if($row4['proom1'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login100">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 1</h3>

                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom1'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="../assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom1']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>

                                        </a>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row4['proom2'] == "NULL"){
                                            
                                }
                                else {
                            ?>
                                    <div class="wrap-login200">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 2</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom2'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="../assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom2']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>
                                        </a>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type3'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login300">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 3</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom3'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom3']; ?>" alt="Room3" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>
                                        </a>
                                        
                                    </div>
                            <?php } ?>



                            <?php
                                if($row2['type4'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login400">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 4</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom4'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom4']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>
                                        </a>

                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type5'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login500">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 5</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom5'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom5']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>

                                        </a>

                                        
                                    </div>
                            <?php } ?>


                            <?php
                                if($row2['type6'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login600">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 6</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom6'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom6']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>

                                        </a>


                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type7'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login700">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 7</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom7'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom7']; ?>" alt="Room1" class="img-room" id="proom">

                                            <?php } ?>

                                            </div>
                                        </a>
                                    </div>

                            <?php } ?>


                            <?php
                                if($row2['type8'] == "NULL"){
                                            
                                }
                                else {
                            ?>

                                    <div class="wrap-login800">

                                        <a href="login.php" class="a_login">

                                            <h3 class="title-room"> Room 8</h3>



                                            <div class='div-img-r' align='center'>

                                            <?php 

                                                if ($row4['proom8'] == 'NULL') {
                                            ?>
                                    
                                                    <img src="assets/logos/homebor 1024px.png" alt="" class="img-alt">

                                            <?php
                                                }else{

                                            ?>

                                            <img src="<?php echo $row4['proom8']; ?>" alt="Room1" class="img-room">

                                            <?php } ?>

                                            </div>
                                        </a>

                                    </div>            

                            <?php } ?>
                            


                        </section>

                        <!--AMENITIES
                            =========================================================================================-->
                        <section id="amenities">

                            <h3>Special Diet</h3>

                            <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                                <?php
                                            if($row['vegetarians'] == "yes"){
                                            echo "<li>Vegetarians</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['halal'] == "yes"){
                                            echo "<li>Halal</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['kosher'] == "yes"){
                                            echo "<li>Kosher (Jews)</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['lactose'] == "yes"){
                                            echo "<li>Lacotose Intolerant</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['gluten'] == "yes"){
                                            echo "<li>Gluten Free Diet</li>";
                                            }
                                        else {
                                        }
                                    ?>
                                    <?php
                                            if($row['pork'] == "no"){
                                            
                                            }
                                        else {
                                            echo "<li>No Pork</li>";
                                        }
                                    ?>
                                     <?php
                                            if($row['none'] == "yes"){
                                            echo "<li>None</li>";
                                            }
                                        else {
                                            
                                        }
                                    ?>
                            </ul>

                        </section>

                    </div>
                    <!--end col-md-8-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

        <!--SIMILAR PROPERTIES
        =============================================================================================================-->
        <section id="similar-properties">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-4 col-sm-12 col-lg-8">

                        <hr class="mb-5">

                        <h3>Similar Homes</h3>

                        

                              <?php 
        error_reporting(0);
        $sql = "SELECT * FROM pe_home WHERE pet = '$row[pet]' OR g_pre = '$row[g_pre]' OR room = '$row[room]' OR ag_pre = '$row[ag_pre]' ORDER BY RAND() LIMIT 3"; //get article id too
        $result = $link->query($sql); //it makes the query
    echo "<tbody>";
      while($rows = $result->fetch_assoc()) {
          if ($row['certified'] == 'Yes'){
            echo '<!--Item-->
            <div class="card ts-item ts-item__list ts-card">
            <!--Ribbon-->
                <div class="ts-ribbon" id="ribbon">
                    <i class="fa fa-star"></i>
                </div>

                <!--Card Image-->
                <a href="detail.php?art_id='.$rows['id_home'].'" class="card-img ts-item__image" data-bg-image="'.$rows['phome'].'"></a>

                <!--Card Body-->
                 <a href="detail.php?art_id='.$rows['id_home'].'">
                <div class="card-body ts-item__body">

                    <figure class="ts-item__info">
                        <h4>'.$rows['h_name'].'</h4>
                        <aside>
                            <i class="fa fa-map-marker mr-2"></i>
                            '.$rows['city'].'
                        </aside>
                    </figure>

                    <div class="ts-item__info-badge">'.$rows['status'].'</div>

                    <div class="ts-description-lists">
                        <dl>
                            <dt>Bedrooms</dt>
                            <dd>'.$rows['room'].'</dd>
                        </dl>
                        <dl>
                            <dt>Gender</dt>
                            <dd>'.$rows['g_pre'].'</dd>
                        </dl>
                        <dl>
                            <dt>Pets</dt>
                            <dd>'.$rows['pet'].'</dd>
                        </dl>
                        <dl>
                            <dt>Age</dt>
                            <dd>'.$rows['ag_pre'].'</dd>
                        </dl>

                    </div>
                </div>
                </a>

                <!--Card Footer-->
                <a href="detail.php?art_id='.$rows['id_home'].'" class="card-footer ts-item__footer">
                    <span class="ts-btn-arrow">Detail</span>
                </a>

            </div>
';
          } else{}
          
     }  

    ?>


                    </div>

                </div>
            </div>
        </section>

    </main>
    <!--end #ts-main-->

    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->
    <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/leaflet.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/map-leaflet.js"></script>

</body>
</html>
