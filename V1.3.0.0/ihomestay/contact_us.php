<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homebor</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/header/fonts/icomoon/style.css">
    
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/header_style.css">
    <link rel="stylesheet" href="assets/css/about_us.css">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <script src="assets/js/scrollreveal.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    

</head>

<body>

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        
        <div class="site-mobile-menu-body"></div>
    </div>

    

    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

        <?php include 'header.php' ?>

    </header>

<!-- END HEADER -->

    <div class="container__contact">


            <div class="banner__contact_div">
                <h1 class="contact__title">About us</h1>
            </div>

            <section class="about_us color1 homestay">

                <h4>Homebor</h4>

                <div class="flex-content">

                    <div class="img__about">
                        <img src="images/homebor_info.png" alt="" class="about__img ">
                    </div>

                    <div class="text__about_homestay text-center">
                        <p class="subtitle__about">Homebor is a new accommodation platform. We help schools that runs a homestay department to stay connected with their homestay families in order to bring efficiency and maximize a good experience for everyone involved.</p>
                        <p class="subtitle__about">The management and organization of students and homestays in a single space has been systematized. We offer a cross-cutting workflow between the different stakeholders. Consequently, generating a detailed and constant follow-up between the homestay, student and educational organization.</p>
                        <p class="subtitle__about">We offer a new and easy experience to find family homes in the locations closest to your academy with updated and intuitive tools for users. Register and become part of our community.</p>

                        <!-- <a class="btn regular-button text-center btn__book_free" href="https://calendly.com/homebor/demo?month=2021-08"> Book a Free Demo </a>-->
                    </div>

                </div>

            </section>

            <section class="about_us color2 student">

                <h4>Homestay</h4>

                <div class="flex-content">

                    <div class="text__about_student text-center">
                        
                        <p class="subtitle__about">A Homestay is a host family that offers home accommodation services to foreign students, such as accommodation and comfortable meals. Most importantly, they facilitate a unique and meaningful cultural immersion, allowing students to better understand and experience the host country, which cannot be learned in a classroom. </p>

                        <a href="register.php" class="btn btn_become">Become a Homestay</a>

                    </div>

                    <div class="img__about">
                        <img src="images/homestay_p.jpg" alt="" class="about__img">
                    </div>

                </div>

            </section>


            <section class="contact_div">

                <div class="contact__form">
                    <form action="#" class="w-100" id="form_contact">

                        <section class="contact__email">
                            <h4 class="send_question">Send us your Question</h4>

                            <div class="fr__group">

                                <div class="form__group form__group__name" id="group__name">

                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">First Name</label>

                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                        
                                </div>
                                
                                <div class="form__group form__group__email" id="group__email">

                                    <div class="form__group__icon-input">
                                        <label for="email" class="form__label">Email</label>

                                        <label for="email" class="icon"><i class="icon icon__email"></i></label>
                                        <input type="text" id="email" name="email" class="form__group-input email" placeholder="e.g. John Smith">

                                    </div>
                                    <p class="form__group__input-error">It is not a valid Email</p>
                                        
                                </div>

                            </div>

                            <div class="fr__group">

                                <div class="form__group form__group__subject" id="group__subject">

                                    <div class="form__group__icon-input">
                                        <label for="subject" class="form__label">Subject</label>

                                        <label for="subject" class="icon"><i class="icon icon__subject"></i></label>
                                        <input type="text" id="subject" name="subject" class="form__group-input subject" placeholder="e.g. How can I Register?">

                                    </div>
                                    <p class="form__group__input-error">It is not a valid Email</p>
                                    
                                </div>

                            </div>
                            
                            <div class="fr__group">

                                <div class="form__group form__group__message" id="group__message">

                                    <div class="form__group__icon-input">
                                        <label for="message" class="form__label label__message">Message Content</label>

                                        <textarea rows="6" id="message" name="message" class="form__group-input message" placeholder="e.g. How can I Register?"></textarea>

                                    </div>
                                    <p class="form__group__input-error">Please write your question</p>
                                    
                                </div>

                            </div>

                            <div class="form__message" id="form__message">
                                <p>Please fill in the fields Correctly</p>
                            </div>

                            <div class="fr__group" id="btn__success">
                                <button type="submit" name="submit" class="btn__send_concact" id="btn_" > Send Message </button>
                                
                            </div>

                            <p class="form__message-sucess" id="form__message-sucess"> Message sent</p>

                        </section>

                    </form>

                    

                    <section class="contact__info">

                        <div class="email__div">
                            <img src="assets/icon/email.png" class="icon-email" alt="">
                        </div>

                        <div class="email__text">
                            <h5>info@homebor.com</h5>
                        </div>

                    </section>

                </div>

            </section>

    </div>
    
    <?php
        include 'footer.php';
    ?>




<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/header/js/jquery.sticky.js"></script>
<script src="assets/js/mainHeader.js"></script>
<script src="assets/js/validate_form.js"></script>
</body>
</html>