<?php
include '../../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail_h FROM pe_home WHERE mail_h = '$usuario'";

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
$row6=$query6->fetch_assoc();

$sql = "SELECT * FROM pe_home WHERE id_m = '$row6[id_m]'"; 

 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

$query8 = $link->query("SELECT * FROM academy");
$row8=$query8->fetch_assoc();

 if ($row5['usert'] != 'Manager') {
         header("location: ../logout");   
    }

$query2="SELECT * FROM manager WHERE mail = '$usuario'";
$resultado=$link->query($query2);

$row2=$resultado->fetch_assoc();

if ($row2['plan'] == 'No Plan') {
        header("location: work_with_us");
    }

?>
<html lang="en">
<head>
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Homestay Profile</title>
     <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../homestay/assets/css/carousel.css">
    <link rel="stylesheet" href="../homestay/assets/css/galery.css">
    <link rel="stylesheet" href="assets/css/homestay.css">

    <link rel="stylesheet" href="../assets/datepicker/css/rome.css">
    <link rel="stylesheet" href="../assets/datepicker/css/style.css">

    <!-- REFERENCE OF MAPBOX API -->
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js"></script>
    <link
    rel="stylesheet"
    href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
    type="text/css"
    />
    <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

    <!-- Daterangepicker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>

<body style="background-color: #F3F8FC;">
    <!-- WRAPPER - FONDO
    =================================================================================================================-->

    <!--*********************************************************************************************************-->
    <!--HEADER **************************************************************************************************-->
    <!--*********************************************************************************************************-->
     <header id="ts-header" class="fixed-top" style="background-color: white;">

        <?php include 'header.php' ?>

    </header>

    <div class="container__title">
        <h1 class="title__register">Register New Propertie <p class="required">Required Files *</p></h1>

    </div>

    <form id="form" class="ts-form" action="#" autocomplete="off" enctype="multipart/form-data">
        <input type="hidden" class="form-control" id="id_m" name="id_m" required value="<?php echo $row6['id_m'] ;?>">

        <input type="text" class="form-control" id="plan" name="plan" value="<?php echo $row6['plan']?>" hidden>

        <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $numberOfRows?>" hidden>
        <main class="card-main">
        <div class="form__group-house_in form__group">

            <div class="form__target-header">
                <h3 class="title_target"> Basic Information </h3>
            </div>

            <h2 class="title__group-section">House Information</h2>

            <div class="row justify-content-center form__group__row_box">

                <div class="form__group form__group-h_name col-lg-4" id="form__group-h_name">

                    <div class="form__group-hinf">
                        <label for="h_name" class="label__name house_info">House Name *</label>
                        <label for="h_name" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>
                        <input type="text" name="h_name" id="h_name group__h_name" class="form__group-input hname" placeholder="e.g. John Smith Residence">
                    </div>
                    <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>

                </div>
                
                <div class="form__group form__group-num col-lg-4" id="form__group-num">

                    <div class="form__group-hinf">
                        <label for="p_num" class="label__name house_info">Phone Number *</label>
                        <label for="p_num" class="l_icon" title="Phone Number"> <i class="icon icon_pnumber"></i> </label>
                        <input type="text" name="num" id="p_num group__p_num" class="form__group-input pnumber" placeholder="e.g. 55575846">
                    </div>

                    <p class="message__input_error"> Only numbers. Min 6 characters </p>

                </div>

                <div class="form__group form__group-room col-lg-4" id="form__group-room">

                    <div class="form__group-hinf">
                        <label for="room" class="label__name hi_room">Rooms in your House *</label>
                        <label for="room" class="l_icon" title="Rooms in your House"> <i class="icon icon_rooms"></i> </label>
                        <input type="text" name="room" id="room" class="form__group-input rooms" placeholder="e.g. 5" onKeyDown="viewBed()" onKeyUp="viewBed()" maxlength="1">
                    </div>

                    <p class="message__input_error"> Only numbers. Max 8 rooms </p>

                </div>

                <div class="form__group form__group-h_type col-lg-4" id="form__group-h_type">

                    <div class="form__group-hinf">
                        <label for="h_type" class="label__name house_info">Type of Residence</label>
                        <label for="h_type" class="l_icon" title="House name"> <i class="icon icon_hname"></i> </label>

                        <select name="h_type" id="h_type group__h_type" class="custom-select form__group-input hname">

                            <option selected hidden="option">-- Select Option --</option>
                            <option value="House"> House </option>
                            <option value="Apartment"> Apartment </option>
                            <option value="Condominium"> Condominium </option>

                        </select>

                    </div>
                    <p class="message__input_error"> Please, only letters, numbers and hyphens. Minimum 4 characters </p>

                </div>

                <div class="form__group form__group-mail col-lg-4" id="form__group-mail">

                    <div class="form__group-hinf">
                        <label for="mail" class="label__name hi_room">Homestay Mail *</label>
                        <label for="mail" class="l_icon" title="Mail"> <i class="icon icon_mail"></i> </label>
                        <input type="text" name="mail" id="pass group__mail" class="form__group-input mail" placeholder="emaple@example.com">
                    </div>

                    <p class="message__input_error"> Enter a Valid Email </p>

                </div>

                <div class="form__group form__group-pass col-lg-4" id="form__group-pass">

                    <div class="form__group-hinf">
                        <label for="pass" class="label__name hi_room">Password *</label>
                        <label for="pass" class="l_icon" title="Phone Number"> <i class="icon icon_pass"></i> </label>

                        <input type="password" name="pass" id="pass group__pass" class="form__group-input pass" placeholder="Enter Password">
                    </div>

                    <p class="message__input_error"> Min 4 characters </p>



                </div>

            </div>



            <h2 class="title__group-section">Location</h2>

            <div class="row form__location px-4 end_div">

                <div class="form__group-adress col-lg-6">
                    
                    <div class="form__group-location col-sm-12 form__group-m_city mb-4">
                        <label for="m_city" class="label__name location">Main City</label>
                        <label for="m_city" class="l_icon" title="Avenue, street, boulevard, etc."> <i class="icon icon_location"></i> </label>
                        <select name="m_city" id="m_city" class="custom-select form__group-input address">

                            <option hidden="option" selected disabled>Select Option</option>
                            <option value="Toronto">Toronto</option>
                            <option value="Montreal">Montreal</option>
                            <option value="Ottawa">Ottawa</option>
                            <option value="Quebec">Quebec</option>
                            <option value="Calgary">Calgary</option>
                            <option value="Vancouver">Vancouver</option>
                            <option value="Victoria">Victoria</option>

                        </select>
                    </div>
                        
                        <div class="form__group-location col-sm-12 form__group-addr">
                            <label for="address" class="label__name location">Address</label>
                            <label for="address" class="l_icon" title="Avenue, street, boulevard, etc."> <i class="icon icon_location"></i> </label>
                            <input type="text" name="dir" id="address" class="form__group-input address" placeholder="Av, Street, etc.">
                        </div>

                        <div class="form__group-location col-sm-12 form__group-city">
                            <label for="city" class="label__name location">City</label>
                            <label for="city" class="l_icon" title="City"> <i class="icon icon_location"></i> </label>
                            <input type="text" name="city" id="city" class="form__group-input city" placeholder="e.g. Davenport">
                        </div>


                        <div class="form__group-location col-sm-12 form__group-state">
                            <label for="state" class="label__name location">State / Province</label>
                            <label for="state" class="l_icon" title="State / Province"> <i class="icon icon_location"></i> </label>
                            <input type="text" name="state" id="state" class="form__group-input state" placeholder="e.g. Ontario">
                        </div>

                        <div class="form__group-location col-sm-12 form__group-pcode">
                            <label for="p_code" class="label__name location">Postal Code</label>
                            <label for="p_code" class="l_icon" title="Postal Code"> <i class="icon icon_location"></i> </label>
                            <input type="text" name="p_code" id="p_code" class="form__group-input pcode" placeholder="No Special Characters">
                        </div>

                </div>

                <div class="form__group-map col-lg-6">
                    <div id='map' style="transform: translateY(12.5%);"></div>
                </div>

                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
    
                <script>
                mapboxgl.accessToken = 'pk.eyJ1Ijoiam9zZXhlb24iLCJhIjoiY2szaXpqdXBpMDB1bTNpczR1cGt0ajgzYSJ9.Dd0-oK7-s-VU2RZxac1VRg';
                var map = new mapboxgl.Map({

                    container: 'map', // container id
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [-79.347015, 43.651070], // starting position
                    zoom: 11 // starting zoom
                });
                
                // Add geolocate control to the map.
                map.addControl(
                new mapboxgl.GeolocateControl({
                    positionOptions: {
                        enableHighAccuracy: true
                    },
                    trackUserLocation: true
                    })
                );

                </script>
            
            </div><br>

        </div>
        
    </main>

    <main class="card-main">
        <div class="form__group-house_in form__group">

            <div class="form__target-header">
                <h3 class="title_target"> House Details </h3>
            </div>
        
            <h2 class="title__group-section">Photo Gallery</h2>


            <div class="row" style="margin: 0; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">


                <div class="tarjet">
                    
                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Frontage Photo </h4>


                    <div class="div-img-r" align="center">


                        <div class="carousel-item active">


                            <label for="file" class="photo-add" id="label-i"> <p class="form__group-l_title"> + </p> </label>

                            <img id="preview" class="add-photo d-none" >

                            <input type="file" name="main" id="file" accept="image/*" onchange="previewImage();" style="display: none" >



                            <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;" title="Change Frontage Photo"></label>

                        </div>

                    </div>

                </div>




                <!-- Living Room Photo -->
                <div class="tarjet" >

                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Living Room Photo </h4>

                    <div class="div-img-r" align="center">

                        <div class="carousel-item active">

                            <label for="file-ii" class="photo-add" id="label-ii"> <p class="form__group-l_title"> + </p> </label>

                            <img id="preview-ii" class="add-photo d-none" >

                            <input type="file" name="lroom" id="file-ii" accept="image/*" onchange="previewImage_ii();" style="display: none" >


                            <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-ii-i" style="display: none" title="Change Living Room"> </label>

                        </div>
                        
                    </div>

                </div>



                <div class="tarjet">
  
                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; "> Family Picture </h4>

                    <div class="div-img-r" align="center">


                        <div class="carousel-item active">

                            <label for="family" class="photo-add" id="fp"> <p class="form__group-l_title"> + </p> </label>

                            <input type="file" name="fp" id="family" accept="image/*" onchange="previewImagefp();" style="display: none">

                            <img id="preview-fp" class="add-photo d-none" >



                            <label for="family" class="add-photo-i fa fa-pencil-alt" id="fp-i" style="display: none;" title="Change Frontage Photo"></label>

                        </div>
    
                    </div>
                                

                </div>

                        
            </div>




            <!-- Area Photos -->

            <div class="row div-area" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%;">
            
                <div class="tarjet" align="center">

                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Kitchen </h4>

                    <div class="carousel-item active">
                        <label for="file-iii" class="photo-add" id="label-iii"> <p class="form__group-l_title"> + </p> </label>


                        <img id="preview-iii" class="add-photo d-none" >

                        <input type="file" name="area-i" id="file-iii" accept="image/*" onchange="previewImage_iii();" style="display: none" 
                        >


                        <label for="file-iii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i" style="display: none" title="Change Kitchen Photo"></label>


                    </div>
               

                </div>


                <div class="tarjet" align="center">

                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em; text-align: left;"> Dining Room </h4>



                    <div class="carousel-item active">

                        <label for="file-iv" class="photo-add" id="label-iv"> <p class="form__group-l_title"> + </p> </label>

                        <img id="preview-iv" class="add-photo d-none" >

                        <input type="file" name="area-ii" id="file-iv" accept="image/*" onchange="previewImage_iv();" style="display: none" 
                                    >


                        <label for="file-iv" class="add-photo-i fa fa-pencil-alt" id="label-iv-i" style="display: none" title="Change Dining Room Photo"> </label>

                    </div>
               

                </div>




                <div class="tarjet3 area-3">
        
                    <center><label for="collapse-floor-1"><button class="add-room iii" id="iii" type="button" data-toggle="collapse" data-target="#collapse-floor-1" aria-expanded="false" aria-controls="collapse-floor-1" onclick="mostrarBoton_1()" style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;" title="Add Another Area of the House" >

                        +

                    </button></label></center>

                    <div class="tarjet collapse" id="collapse-floor-1" style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

                        <h3 class="title-room"> House Area 3 <button class="fa fa-window-close iii" id="iii-i" type="button" data-toggle="collapse" data-target="#collapse-floor-1" aria-expanded="false" aria-controls="collapse-floor-1" onclick="hidBoton_1()" style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;">
                        </button></h3>

                        <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                                <label for="file-v" class="photo-add" id="label-v"> <p class="form__group-l_title"> + </p> </label>

                                <img id="preview-v" class="add-photo d-none" >

                                <input type="file" name="area-iii" id="file-v" accept="image/*" onchange="previewImage_v();" style="display: none" 
                                >


                                <label for="file-v" class="add-photo-i fa fa-pencil-alt" id="label-v-i" style="display: none" title="Change House Area Photo"> </label>


                            </div>



                        </div>
            
                    </div>

                </div>




                <div class="wrap-div area-iii" id="area-iii" style="display: none">
                                        
                    <center><label for="collapse-floor-2"><button class="add-room iii" id="iv" type="button" data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false" aria-controls="collapse-floor-2" onclick="mostrarBoton_2()" style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px; margin-top: 2%;" title="Add Another Area of the House" >

                    +

                    </button></label></center>

                    <div class="tarjet collapse" id="collapse-floor-2" style="width: 100%; background: white; padding-top: 2%; margin-left: 3%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

                        <h3 class="title-room"> House Area 4 <button class="fa fa-window-close iii" id="iv-i" type="button" data-toggle="collapse" data-target="#collapse-floor-2" aria-expanded="false" aria-controls="collapse-floor-2" onclick="hidBoton_2()" style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button></h3>

                        <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                                <label for="file-vi" class="photo-add" id="label-vi"> <p class="form__group-l_title"> + </p> </label>

                                <img id="preview-vi" class="add-photo d-none" >

                                <input type="file" name="area-iv" id="file-vi" accept="image/*" onchange="previewImage_vi();" style="display: none" 
                                                        >


                                <label for="file-vi" class="add-photo-i fa fa-pencil-alt" id="label-vi-i" style="display: none" title="Change House Area Photo"> </label>

                            </div>
                                
                        </div>
                                            
                    </div>

                </div>


            </div>

            <!-- Bathroom Photos -->

            <div class="row gallery-end" style="margin-left: 0%; justify-content: center; align-items: center;flex-flow: row wrap; -webkit-clip-path: polygon(0,0, 100%, 83%, 0, 100%);clip-path: polygon(0,0, 100%, 83%, 0, 100%); width: 100%; margin-top: 4%;">

                                
                <div class="tarjet">
                                       
                    <h4 class="title-room" style="margin: .4em .9em; padding: .5em .3em; border-bottom: 2px solid #4F177D; margin-bottom: 1em;"> Bathroom Photo 1 </h4>

                    <div class="div-img-r" align="center">

                        <div class="carousel-item active">

                            <label for="file-vii" class="photo-add" id="label-vii">  <p class="form__group-l_title"> + </p> </label>

                            <img id="preview-vii" class="add-photo d-none" >

                            <input type="file" name="bath-i" id="file-vii" accept="image/*" onchange="previewImage_vii();" style="display: none" >


                            <label for="file-vii" class="add-photo-i fa fa-pencil-alt" id="label-vii-i" style="display: none" title="Change Bathroom Photo"> </label>

                        </div>
                                    
                    </div>                  

                </div>



                <div class="wrap-div bath-ii">
                                        
                    <center><label for="collapse-floor-4"><button class="add-room iii" id="vi" type="button" data-toggle="collapse" data-target="#collapse-floor-4" aria-expanded="false" aria-controls="collapse-floor-4" onclick="mostrarBoton_4()" style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;" title="Add Another Bathroom" >

                        +

                    </button></label></center>

                    <div class="tarjet collapse" id="collapse-floor-4" style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

                        <h3 class="title-room"> Bathroom 2 <button class="fa fa-window-close iii" id="vi-i" type="button" data-toggle="collapse" data-target="#collapse-floor-4" aria-expanded="false" aria-controls="collapse-floor-4" onclick="hidBoton_4()" style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button></h3>

                        <div class="div-img-r" align="center">


                            <div class="carousel-item active">

                                <label for="file-viii" class="photo-add" id="label-viii">  <p class="form__group-l_title"> + </p> </label>

                                <img id="preview-viii" class="add-photo d-none" >

                                <input type="file" name="bath-ii" id="file-viii" accept="image/*" onchange="previewImage_viii();" style="display: none" 
                                                >


                                <label for="file-viii" class="add-photo-i fa fa-pencil-alt" id="label-viii-i" style="display: none" title="Change Bathroom Photo">  </label>

                            </div>

                        </div>
                                            
                    </div>

                </div>





                <div class="wrap-div bath-3" id="bath-iii" style="display: none">
                                        
                    <center><label for="collapse-floor-5"><button class="add-room iii" id="vii" type="button" data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false" aria-controls="collapse-floor-5" onclick="mostrarBoton_5()" style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;" title="Add Another Bathroom" >

                        +

                    </button></label></center>

                    <div class="tarjet collapse" id="collapse-floor-5" style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

                        <h3 class="title-room"> Bathroom 3 <button class="fa fa-window-close iii" id="vii-i" type="button" data-toggle="collapse" data-target="#collapse-floor-5" aria-expanded="false" aria-controls="collapse-floor-5" onclick="hidBoton_5()" style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; display: none; color: #4F177D; float: right; margin-right: -.5em;"></button></h3>

                        <div class="div-img-r" align="center">

                            <div class="carousel-item active">

                                <label for="file-ix" class="photo-add" id="label-ix">  <p class="form__group-l_title"> + </p> </label>

                                <img id="preview-ix" class="add-photo d-none" >

                                <input type="file" name="bath-iii" id="file-ix" accept="image/*" onchange="previewImage_ix();" style="display: none"  >


                                <label for="file-ix" class="add-photo-i fa fa-pencil-alt" id="label-ix-i" style="display: none" title="Change Bathroom Photo">  </label>

                            </div>

                        </div>
                                            
                    </div>

                </div>





                <div class="wrap-div bath-iv" id="bath-iv" style="display: none">
                                        
                    <center><label for="collapse-floor-6"><button class="add-room iii" id="viii" type="button" data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false" aria-controls="collapse-floor-6" onclick="mostrarBoton_6()" style="width: 100%; padding: .5em 1em; font-size: 16px; background: white; border: 1px solid #232159; border-radius:18px;" title="Add Bathroom Photo 4" >

                        +

                    </button></label></center>

                    <div class="tarjet2 collapse" id="collapse-floor-6" style="width: 100%; background: white; padding-top: 2%; margin-right: 2%; padding-bottom: 1%; background: white; box-shadow: 1px 1px 5px 0px #4f177d; ">

                        <h3 class="title-room"> Bathroom 4 <button class="fa fa-window-close iii" id="viii-i" type="button" data-toggle="collapse" data-target="#collapse-floor-6" aria-expanded="false" aria-controls="collapse-floor-6" onclick="hidBoton_6()" style=" font-size: 20px; border: none; background: white; margin-bottom: .5em; color: #4F177D; float: right; margin-right: -.5em;"></button></h3>

                        <div class="div-img-r" align="center">

                            <div class="carousel-item active">

                                <label for="file-x" class="photo-add" id="label-x">  <p class="form__group-l_title"> + </p> </label>

                                <img id="preview-x" class="add-photo d-none" >

                                <input type="file" name="bath-iv" id="file-x" accept="image/*" onchange="previewImage_x();" style="display: none">


                                <label for="file-x" class="add-photo-i fa fa-pencil-alt" id="label-x-i" style="display: none" title="Change Bathroom Photo">  </label>

                            </div>
                                
                        </div>
                                            
                    </div>

                </div>

            </div>

            

            <div class="m-0 p-0 bedrooms">

                

                

            </div>

            


            

    </main>



    <main class="card-main">
        
        <div class="form__group-house_in form__group">

        <div class="form__target-header">
            <h3 class="title_target"> Additional Information </h3>
        </div>

        <h2 class="title__group-section">Description of your House</h2>

        <div class="form__group-description_div">

            <div class="form__group-description">

                <label for="address" class="l_icon" title="Description"> <i class="icon icon_description"></i> </label>
                <textarea rows="3" maxlength="255" class="form__group-input description" name="des" id="des" placeholder="Description: Describe your house using few words, no special characters."></textarea>
                
            </div>


        </div>

        <h2 class="title__group-section">Preferences</h2>

        <div class="row form__group-preferences form__group-preferences1 pt-4">
                    
            <div class="form__group form__group-academy col-xl-12 ">
                <label for="a_pre" class="label__name preference">Academy Preference</label>
                <label for="a_pre" class="l_icon" title="Academy Preference"> <i class="icon icon_p_academy"></i> </label>
                <select class="custom-select form__group-input academy" id="a_pre" name="a_pre">   
                    <?php    
                        while ( $row8 = $query8->fetch_array() )    
                        {
                    ?>
                        <option hidden="option" selected disabled> -- Select Preference -- </option>
                        <option value=" <?php echo $row8['id_ac'] ?> ">
                        <?php echo $row8['name_a']; ?><?php echo ', '?><?php echo $row8['dir_a']; ?>
                        </option>
       
                    <?php
                        }    
                    ?>       
                </select>
            </div>

            <div class="form__group form__group-gender col-xl-4 mb-4">
                <label for="g_pre" class="label__name preference">Gender Preference</label>
                <label for="g_pre" class="l_icon" title="Gender Preference"> <i class="icon icon_gender"></i> </label>
                <select class="custom-select form__group-input gender" id="g_pre" name="g_pre">
                    <option hidden="option" selected disabled>-- Select Gender --</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Any">Any</option>
                </select>
            </div>

            <div class="form__group form__group-age col-xl-4">
                <label for="ag_pre" class="label__name preference">Age Preference</label>
                <label for="ag_pre" class="l_icon" title="Age Preference"> <i class="icon icon_age"></i> </label>
                <select class="custom-select form__group-input age" id="ag_pre" name="ag_pre">
                    <option hidden="option" selected disabled>-- Select Age --</option>
                    <option value="Teenager">Teenager</option>
                    <option value="Adult">Adult</option>
                    <option value="Any">Any</option>
                </select>
            </div>


                        
            <div class="form__group col-xl-4">
                <label for="status" class="label__name preference">House Status</label>
                <label for="status" class="l_icon" title="House Status"> <i class="icon icon_status"></i> </label>
                <select class="custom-select form__group-input status" id="status" name="status">
                    <option hidden="option" selected disabled>-- Select Status --</option>
                    <option value="Avalible">Avalible</option>
                    <option value="Occupied">Occupied</option>
                </select>
            </div>

            <div class="form__group form__group-smokers col-xl-4">
                <label for="smoke" class="label__name preference">Smokers Politics</label>
                <label for="smoke" class="l_icon" title="Smokers Politics"> <i class="icon icon_smokers"></i> </label>
                <select class="custom-select form__group-input smoke" id="smoke" name="smoke">
                    <option hidden="option" selected disabled>-- Select Preference --</option>
                    <option value="Outside-OK">Outside-OK</option>
                    <option value="Inside-OK">Inside-OK</option>
                    <option value="Strictly Non-Smoking">Strictly Non-Smoking</option>
                </select>
            </div>

            <div class="form__group form__group-smokers col-xl-4 ">
                <label for="m_service" class="label__name preference">Meals Service</label>
                <label for="m_service" class="l_icon" title="Smokers Politics"> <i class="icon icon_meals"></i> </label>
                <select class="custom-select form__group-input smoke" id="m_service" name="m_service">
                    <option hidden="option" selected disabled>- Select -</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </div>
            
            <div class="form__group form__group-smokers col-xl-4">
                <label for="y_service" class="label__name preference">Since when have you been a Homestay?</label>
                <label for="y_service" class="l_icon" title="Years Service"> <i class="icon icon_years"></i> </label>
                <input type="date" class="form__group-input y_service" id="y_service" name="y_service">
            </div>

            
            <div class="form__group form__group-ypets col-xl-4">
                <label for="y_service" class="label__name preference">Do you have pets?</label>
                <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>
                <select class="custom-select form__group-input ypets" id="pet" name="pet" onchange="yesPets()">
                    <option hidden="option" selected disabled> -- Select Option -- </option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </div>

            <div class="form__group form__group-ypets col-xl-4 d-none" id="num__pets">
                <label for="y_service" class="label__name preference">How many pets?</label>
                <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>
                <input type="number" class="form__group-input npets" id="npets" name="" min="1" max="10" placeholder="Pets Number">
            </div>
            
            <div class="form__group form__group-ypets col-xl-4 d-none" id="kind__pets">
                <label for="y_service" class="label__name preference">Kind of Pet</label>

                <div class="type__pets">
                    <div class="pet_dog">
                        <input type="checkbox" id="dog" name="dog" value="yes">
                        <label for="">Dog</label>
                    </div>

                    <div class="pet_cat">
                        <input type="checkbox" id="cat" name="cat" value="yes">
                        <label for="">Cat</label>
                    </div>

                    <div class="pet_other">
                        <input type="checkbox" id="other" name="other" value="yes">
                        <label for="">Other</label>
                    </div>
                </div>
                
            </div>

            <div class="form__group form__group-ypets col-xl-4" id="specify_pets">
                
                <label for="y_service" class="label__name preference">Specify</label>
                <label for="pet" class="l_icon" title="Pets"> <i class="icon icon_pets"></i> </label>
                <input type="text" class="form__group-input specify_pets" id="type_pet" name="type_pet" placeholder="Specify">

            </div>
            


            <script>
                const $pets = document.querySelector("#pet");

                function yesPets() {

                    const petsI = $pets.selectedIndex;
                    if(petsI === -1) return; // Esto es cuando no hay elementos
                    const optionPets = $pets.options[petsI];

                    if(optionPets.value == 'Yes'){

                        document.getElementById('num__pets').classList.remove('d-none');
                        document.getElementById('kind__pets').classList.remove('d-none');

                    }else if(optionPets.value == 'No'){
                        document.getElementById('num__pets').classList.add('d-none');
                        document.getElementById('kind__pets').classList.add('d-none');
                    }else{
                        document.getElementById('num__pets').classList.add('d-none');
                        document.getElementById('kind__pets').classList.add('d-none');

                    }

                };

                // obtener campos ocultar div
                var checkbox = $("#other");
                var hidden = $("#specify_pets");
                //var populate = $("#populate");
                    
                hidden.hide();
                    checkbox.change(function() {
                        if (checkbox.is(':checked')) {
                        //hidden.show();
                            $("#specify_pets").fadeIn("200")
                        } else {
                            //hidden.hide();
                            $("#specify_pets").fadeOut("100")
                            $("#type_pet").val(""); // limpia los valores de lols input al ser ocultado
                            $('input[type=checkbox]').prop('checked',false);// limpia los valores de checkbox al ser ocultado

                        }
                    });


            </script>

        </div>


        <div class="row justify-content-center form__group-preferences">
            
            <div class="form__group-diet col-lg-5 p-0">
                <h2 class="title__group-preference"><i class="icon icon-diet"></i> Special Diet</h2>

                <div class="form__group-diet-in">
                
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="vegetarians" name="vegetarians" value="yes">
                        <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">
                        <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                    </div>

                </div>

                <div class="form__group-diet-in">

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">
                        <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes">
                        <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                    </div>

                </div>

                <div class="form__group-diet-in">

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">
                        <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="no">
                        <label class="custom-control-label" for="pork">No Pork</label>
                    </div>
                
                </div>

                <div class="form__group-diet-in">

                    <div class="custom-control custom-checkbox-none custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">
                        <label class="custom-control-label" for="none">None</label>
                    </div>

                </div>

            </div>


        </div>

    </main>


    <main class="card-main">

        <div class="form__group-house_in form__group">
            <div class="form__target-header">
                <h3 class="title_target"> Family Information </h3>
            </div>

            <h2 class="title__group-section">Any Member of your Family:</h2>

            <div class="row form__group-row margin-t">

                <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-allergies">
                    
                    <label for="allergies" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i> </label>
                    <label for="allergies" class="label__name label__family" >Have Allergies?</label>
                    <select class="custom-select form__group-input" id="allergies" name="allergies" onchange="otherAllergy()">
                        <option hidden="option" selected disabled>-- Select Option --</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>

                    <p class="message__input_error"> Write a valid Name </p>
                    
                </div>

                <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__allergies">
                                    
                </div>

                
                <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-medic_f">
                    
                    <label for="medic_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies"></i> </label>
                    <label for="medic_f" class="label__name label__family" >Take any Medication?</label>
                    <select class="custom-select form__group-input" id="medic_f" name="medic_f" onchange="otherMedication()">
                        <option hidden="option" selected disabled>-- Select Option --</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>

                    <p class="message__input_error"> Write a valid Name </p>
                </div>

                <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__medic_f">
                                    
                </div>
                
                <div class="form__group-mname form__group-hinf2 col-md-4" id="form__group-health_f">
                    
                    <label for="health_f" class="l_icon" title="Name Main Contact"> <i class="icon icon_allergies2"></i> </label>
                    <label for="health_f" class="label__name label__family" >Have Health Problems?</label>
                    <select class="custom-select form__group-input" id="health_f" name="health_f" onchange="otherHealth()">
                        <option hidden="option" selected disabled>-- Select Option --</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>

                    <p class="message__input_error"> Write a valid Name </p>
                </div>

                <div class="form__group-mname form__group-hinf2 col-md-4 d-none" id="group__health_f">
                                    
                </div>

            </div>

            
    
    
            <script>
                const $allergy = document.querySelector("#allergies");
    
                function otherAllergy() {
                                        
                    const allergyI = $allergy.selectedIndex;
                                        
                    if(allergyI === -1) return; // Esto es cuando no hay elementos
                                        
                        const optionAllergy = $allergy.options[allergyI];
        
                        if(optionAllergy.value == 'Yes'){
                                            
                            document.getElementById('group__allergies').classList.remove('d-none');
                                            
                            $('#group__allergies').html(`
    
                                <label class="label__other__city">Specify the Allergy</label>
                                <input class="input__other_city" type="text" name="allergies"> 
    
                            `);
          
                        }else{
                            document.getElementById('group__allergies').classList.add('d-none');
                        }
    
                }
                
                const $medication = document.querySelector("#medic_f");
    
                function otherMedication() {
                                        
                    const medicationI = $medication.selectedIndex;
                                        
                    if(medicationI === -1) return; // Esto es cuando no hay elementos
                                        
                        const optionMedication = $medication.options[medicationI];
        
                        if(optionMedication.value == 'Yes'){
                                            
                            document.getElementById('group__medic_f').classList.remove('d-none');
                                            
                            $('#group__medic_f').html(`
    
                                <label class="label__other__city">Specify the Medication</label>
                                <input class="input__other_city" type="text" name="medic_f"> 
    
                            `);
          
                        }else{
                            document.getElementById('group__medic_f').classList.add('d-none');
                        }
    
                }
                
                const $health = document.querySelector("#health_f");
    
                function otherHealth() {
                                        
                    const healthI = $health.selectedIndex;
                                        
                    if(healthI === -1) return; // Esto es cuando no hay elementos
                                        
                        const optionHealth = $health.options[healthI];
        
                        if(optionHealth.value == 'Yes'){
                                            
                            document.getElementById('group__health_f').classList.remove('d-none');
                                            
                            $('#group__health_f').html(`
    
                                <label class="label__other__city">Specify the Problems</label>
                                <input class="input__other_city" type="text" name="health_f"> 
    
                            `);
          
                        }else{
                            document.getElementById('group__health_f').classList.add('d-none');
                        }
    
                }
    
            </script>

            <h2 class="title__group-section">Main Contact Information</h2>

            <div class="row form__group-row margin-t">

                <div class="form__group-mname form__group-hinf2 col-md-4">
                    <label for="name_h" class="l_icon" title="Name Main Contact"> <i class="icon icon_mname"></i> </label>
                    <label for="name_h" class="label__name label__family" >Name Main Contact</label>
                    <input type="text" class="form__group-input mname" id="name_h" name="name_h" placeholder="e.g. Eva">
                </div>
                
                <div class="form__group-lmname form__group-hinf2 col-md-4">
                    <label for="l_name_h" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_mname"></i> </label>
                    <label for="l_name_h" class="label__name label__family" >Last Name</label>
                    <input type="text" class="form__group-input lmname" id="l_name_h" name="l_name_h" placeholder="e.g. Smith">
                </div>

                <div class="form__group-lmname form__group-hinf2 col-md-4">
                    <label for="db" class="l_icon" title="Last Name Main Contact"> <i class="icon icon_db"></i> </label>
                    <label for="db" class="label__name label__family" >Date of Birth</label>
                    <input type="date" class="form__group-input dateb" id="db" name="db" placeholder="MM-DD-YYYY">
                </div>

            

                
                <div class="form__group-gender form__group-hinf2 col-md-4">
                    <label for="gender" class="label__name label__family" >Gender</label>
                    <label for="gender" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                    <select class="custom-select form__group-input mgender" id="gender" name="gender">
                        <option hidden="option" selected disabled>-- Select Gender --</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Private">Private</option>
                    </select>
                </div>

                <div class="form__group-db-check form__group-hinf2 col-md-4">

                    <label for="cell" class="label__name label__family" >Phone Number</label>
                    <label for="cell" class="l_icon" title="Alternative Phone"> <i class="icon icon_pnumber2"></i> </label>
                    <input type="text" class="form__group-input alter_phone" id="cell" name="cell" placeholder="e.g. 55578994">

                </div>

                <div class="form__group-occupation_m form__group-hinf2 col-md-4">
                    <label for="occupation_m" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                    <label for="occupation_m" class="label__name label__family" >Occupation</label>
                    <input type="text" class="form__group-input db_law" id="occupation_m" name="occupation_m" placeholder="e.g. Lawyer">
                </div>

                <div class="form__group-db-check form__group-hinf2 col-md-4">
                    <label for="db_law" class="l_icon" title="Date of Background Check"> <i class="icon icon_db"></i> </label>
                    <label for="db_law" class="label__name label__family" >Date of Background Check</label>
                    <input type="date" class="form__group-input db_law" id="db_law" name="db_law" placeholder="MM-DD-YYYY">
                </div>
                
                
                <div class="form__group-b-check form__group-hinf2 col-md-7">
                    <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                    <input type="file" class="form__group-input db_law" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                </div>

            </div>


            <h2 class="title__group-section">Family Preferences</h2>

            <div class="row form__group-row margin-t">

                <div class="form__group-nmembers form__group-hinf2 col-md-4">
                    
                    <label for="num_mem" class="label__name label__family" >Number Members</label>
                    <label for="num_mem" class="l_icon" title="Number Members"> <i class="icon icon_family"></i> </label>
                    <input type="number" class="form__group-input nmembers" id="num_mem" name="num_mem" min="0" max="10" placeholder="Only Numbers">

                </div>

                <div class="form__group-background form__group-hinf2 col-md-4">
                    
                    <label for="backg" class="label__name label__family" >Background</label>
                    <label for="backg" class="l_icon" title="Background"> <i class="icon icon_bcheck"></i> </label>
                    <input type="text" class="form__group-input background" id="backg" name="backg" placeholder="e.g. Canadian">

                </div>

                <div class="form__group-background_l form__group-hinf2 col-md-4">

                    <label for="backl" class="label__name label__family" >Background Language</label>
                    <label for="backl" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>
                    <input type="text" class="form__group-input background_l" id="backl" name="backl" placeholder="e.g. English">

                </div>
                
                <div class="form__group-religion form__group-hinf2 col-md-4">

                    <label for="religion" class="label__name label__family" >Do you belong to a religion?</label>
                    <label for="religion" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>

                    <select name="religion" id="religion" class="custom-select form__group-input background_l" onchange="otherReligion()">

                        <option hidden="option" selected disabled> -- Select Option -- </option>
                        <option value="Yes"> Yes </option>
                        <option value="No"> No </option>

                    </select>

                </div>

                <div class="form__group-religion form__group-hinf2 col-md-4 d-none" id="group__religion">
                                    
                </div>

                <div class="form__group-condition_m form__group-hinf2 col-md-4">

                    <label for="condition_m" class="label__name label__family" >Any Physical or Mental condition?</label>
                    <label for="condition_m" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>

                    <select name="condition_m" id="condition_m" class="custom-select form__group-input background_l" onchange="otherCondition()">

                        <option hidden="option" selected disabled> -- Select Option -- </option>
                        <option value="Yes"> Yes </option>
                        <option value="No"> No </option>

                    </select>

                </div>

                <div class="form__group-condition form__group-hinf2 col-md-4 d-none" id="group__condition_m">
                                    
                </div>

                <div class="form__group-misdemeanor form__group-hinf2 col-md-4">

                    <label for="misdemeanor" class="label__name label__family" >Have they committed a misdemeanor?</label>
                    <label for="misdemeanor" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>

                    <select name="misdemeanor" id="misdemeanor" class="custom-select form__group-input background_l" onchange="otherMisdemeanor()">

                        <option hidden="option" selected disabled> -- Select Option -- </option>
                        <option value="Yes"> Yes </option>
                        <option value="No"> No </option>

                    </select>

                </div>

                <div class="form__group-misdemeanor form__group-hinf2 col-md-4 d-none" id="group__misdemeanor">
                                    
                </div>

                <script>
                                        
                    const $misdemeanor = document.querySelector("#misdemeanor");
    
                    function otherMisdemeanor() {
                                            
                        const misdemeanorI = $misdemeanor.selectedIndex;
                                            
                        if(misdemeanorI === -1) return; // Esto es cuando no hay elementos
                                            
                            const optionMisdemeanor = $misdemeanor.options[misdemeanorI];

                            if(optionMisdemeanor.value == 'Yes'){
                                                
                                document.getElementById('group__misdemeanor').classList.remove('d-none');
                                                
                                $('#group__misdemeanor').html(`

                                    <label class="label__other__city">Specify?</label>
                                    <input class="input__other_city" type="text" name="misdemeanor"> 

                                `);

                            }else{
                                document.getElementById('group__misdemeanor').classList.add('d-none');
                            }

                    }
                    
                    const $religion = document.querySelector("#religion");
    
                    function otherReligion() {
                                            
                        const religionI = $religion.selectedIndex;
                                            
                        if(religionI === -1) return; // Esto es cuando no hay elementos
                                            
                            const optionReligion = $religion.options[religionI];

                            if(optionReligion.value == 'Yes'){
                                                
                                document.getElementById('group__religion').classList.remove('d-none');
                                                
                                $('#group__religion').html(`

                                    <label class="label__other__city">Which Religion?</label>
                                    <input class="input__other_city" type="text" name="religion"> 

                                `);

                            }else{
                                document.getElementById('group__religion').classList.add('d-none');
                            }

                    }


                    const $condition = document.querySelector("#condition_m");
    
                    function otherCondition() {
                                            
                        const conditionI = $condition.selectedIndex;
                                            
                        if(conditionI === -1) return; // Esto es cuando no hay elementos
                                            
                            const optionCondition = $condition.options[conditionI];

                            if(optionCondition.value == 'Yes'){
                                                
                                document.getElementById('group__condition_m').classList.remove('d-none');
                                                
                                $('#group__condition_m').html(`

                                    <label class="label__other__city">Which Condition?</label>
                                    <input class="input__other_city" type="text" name="condition_m"> 

                                `);

                            }else{
                                document.getElementById('group__condition_m').classList.add('d-none');
                            }

                    }

                </script>


                <div class="form__group-c_background form__group-hinf2 col-xl-10">

                    <label for="c_background" class="label__name label__family" >Do you give us your consent to go to the authorities and check your criminal background check?</label>
                    <label for="c_background" class="l_icon" title="Background Language"> <i class="icon icon_bcheck"></i> </label>

                    <select name="c_background" id="c_background" class="custom-select form__group-input background_l">

                        <option hidden="option" selected disabled> -- Select Option -- </option>
                        <option value="Yes"> Yes </option>
                        <option value="No"> No </option>

                    </select>

                </div>

            </div>

            
            <div class="form__group-row2">

                <button type="button" data-toggle="collapse" class="add__family" data-target="#family1" aria-expanded="false"> Add Family Member </button>

                <div id="family1" class="panel-collapse collapse">

                    <div class="row member1">

                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name1" class="label__name label__family" >Name</label>
                            <label for="f_name1" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name1" id="f_name1" name="f_name1" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname1" class="label__name label__family" >Last Name</label>
                            <label for="f_lname1" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname1" id="f_lname1" name="f_lname1" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db1" class="label__name label__family" >Date of Birth</label>
                            <label for="db1" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input dbirth1" id="db1" name="db1" placeholder="MM-DD-YYYY">

                        </div>


                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">

                            <label for="gender1" class="label__name label__family" >Gender</label>
                            <label for="gender1" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="form__group-input gender1" id="gender1" name="gender1">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>

                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re1" class="label__name label__family" >Relation</label>
                            <label for="re1" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                            <select class="custom-select form__group-input relation1" id="re1" name="re1">
                                <option hidden="option" selected disabled>-- Select Relation --</option>
                                <option value="Dad">Dad</option>
                                <option value="Mom">Mom</option>
                                <option value="Son">Son</option>
                                <option value="Daughter">Daughter</option>
                                <option value="Grandparents">Grandparents</option>
                                <option value="Other">Others</option>
                            </select>
                        
                        </div>

                        <div class="form__group-occupation_f1 form__group-hinf2 col-md-4">
                            <label for="occupation_f1" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f1" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f1" name="occupation_f1" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf1" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf1" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input relation1" id="db_lawf1" name="db_lawf1" placeholder="MM-DD-YYYY">

                        </div>

                                                            
                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family2" aria-expanded="false"> Add Family Member </button>
                    
                </div>


                <div id="family2" class="panel-collapse collapse">
                    
                    <div class="row member1">

                        <div class="form__group-fname form__group-hinf3 col-lg-4">
                            <label for="f_name2" class="label__name label__family" >Name</label>
                            <label for="f_name2" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name2" id="f_name2" name="f_name2" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname2" class="label__name label__family" >Last Name</label>
                            <label for="f_lname2" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname2" id="f_lname2" name="f_lname2" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db2" class="label__name label__family" >Date of Birth</label>
                            <label for="db2" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db2" id="db2" name="db2" placeholder="MM-DD-YYYY">

                        </div>
                        

                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender2" class="label__name label__family" >Gender</label>
                            <label for="gender2" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender2" id="gender2" name="gender2">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re2" class="label__name label__family" >Relation</label>
                            <label for="re2" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                            <select class="custom-select form__group-input relation2" id="re2" name="re2">
                                <option hidden="option" selected disabled>-- Select Relation --</option>
                                <option value="Dad">Dad</option>
                                <option value="Mom">Mom</option>
                                <option value="Son">Son</option>
                                <option value="Daughter">Daughter</option>
                                <option value="Grandparents">Grandparents</option>
                                <option value="Other">Others</option>
                            </select>

                        </div>

                        <div class="form__group-occupation_f2 form__group-hinf2 col-md-4">
                            <label for="occupation_f2" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f2" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f2" name="occupation_f2" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf2" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf2" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf2" id="db_lawf2" name="db_lawf2" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input lawf2" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family3" aria-expanded="false"> Add Family Member </button>

                </div>


                <div id="family3" class="panel-collapse collapse">

                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">
                            
                            <label for="f_name3" class="label__name label__family" >Name</label>
                            <label for="f_name3" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name3" id="f_name3" name="f_name3" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname3" class="label__name label__family" >Last Name</label>
                            <label for="f_lname3" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname3" id="f_lname3" name="f_lname3" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db3" class="label__name label__family" >Date of Birth</label>
                            <label for="db3" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db3" id="db3" name="db3"placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender3" class="label__name label__family" >Gender</label>
                            <label for="gender3" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender3" id="gender3" name="gender3">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re3" class="label__name label__family" >Relation</label>
                            <label for="re3" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input relation3" id="re3" name="re3">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f3 form__group-hinf2 col-md-4">
                            <label for="occupation_f3" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f3" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f3" name="occupation_f3" placeholder="e.g. Lawyer">
                        </div>


                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf3" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf3" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf3" id="db_lawf3" name="db_lawf3" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input lawf3" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family4" aria-expanded="false"> Add Family Member </button>

                </div>

                <div id="family4" class="panel-collapse collapse">

                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name4" class="label__name label__family" >Name</label>
                            <label for="f_name4" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name4" id="f_name4" name="f_name4" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname4" class="label__name label__family" >Last Name</label>
                            <label for="f_lname4" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname4" id="f_lname4" name="f_lname4" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db4" class="label__name label__family" >Date of Birth</label>
                            <label for="db4" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db4" id="db4" name="db4"placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender4" class="label__name label__family" >Gender</label>
                            <label for="gender4" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender4" id="gender4" name="gender4">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re4" class="label__name label__family" >Relation</label>
                            <label for="re4" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input relation4" id="re4" name="re4">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f4 form__group-hinf2 col-md-4">
                            <label for="occupation_f4" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f4" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f4" name="occupation_f4" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf4" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf4" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf4" id="db_lawf4" name="db_lawf4" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input lawf4" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family5" aria-expanded="false"> Add Family Member </button>

                </div>


                <div id="family5" class="panel-collapse collapse">

                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name5" class="label__name label__family" >Name</label>
                            <label for="f_name5" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name5" id="f_name5" name="f_name5" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname5" class="label__name label__family" >Last Name</label>
                            <label for="f_lname5" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname5" id="f_lname5" name="f_lname5" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db5" class="label__name label__family" >Date of Birth</label>
                            <label for="db5" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db5" id="db5" name="db5"placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender5" class="label__name label__family" >Gender</label>
                            <label for="gender5" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender5" id="gender5" name="gender5">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re5" class="label__name label__family" >Relation</label>
                            <label for="re5" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input re5" id="re5" name="re5">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f5 form__group-hinf2 col-md-4">
                            <label for="occupation_f5" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f5" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f5" name="occupation_f5" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf5" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf5" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf5" id="db_lawf5" name="db_lawf5" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family6" aria-expanded="false"> Add Family Member </button>

                </div>



                <div id="family6" class="panel-collapse collapse">

                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name6" class="label__name label__family" >Name</label>
                            <label for="f_name6" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name6" id="f_name6" name="f_name6" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname6" class="label__name label__family" >Last Name</label>
                            <label for="f_lname6" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname6" id="f_lname6" name="f_lname6" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db6" class="label__name label__family" >Date of Birth</label>
                            <label for="db6" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db6" id="db6" name="db6"placeholder="MM-DD-YYYY">

                        </div>

                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender6" class="label__name label__family" >Gender</label>
                            <label for="gender6" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender6" id="gender6" name="gender6">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re6" class="label__name label__family" >Relation</label>
                            <label for="re6" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input relation6" id="re6" name="re6">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f6 form__group-hinf2 col-md-4">
                            <label for="occupation_f6" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f6" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f6" name="occupation_f6" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf6" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf6" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf6" id="db_lawf6" name="db_lawf6" placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family7" aria-expanded="false"> Add Family Member </button>

                </div>



                <div id="family7" class="panel-collapse collapse">
                        
                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name7" class="label__name label__family" >Name</label>
                            <label for="f_name7" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name7" id="f_name7" name="f_name7" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname7" class="label__name label__family" >Last Name</label>
                            <label for="f_lname7" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname7" id="f_lname7" name="f_lname7" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db7" class="label__name label__family" >Date of Birth</label>
                            <label for="db7" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db7" id="db7" name="db7"placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender7" class="label__name label__family" >Gender</label>
                            <label for="gender7" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender7" id="gender7" name="gender7">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re7" class="label__name label__family" >Relation</label>
                            <label for="re7" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input relation7" id="re7" name="re7">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f7 form__group-hinf2 col-md-4">
                            <label for="occupation_f7" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f7" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f7" name="occupation_f7" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf7" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf7" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf7" id="db_lawf7" name="db_lawf7" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>

                    <button type="button" data-toggle="collapse" class="add__family family2" data-target="#family8" aria-expanded="false"> Add Family Member </button>

                </div>

                <!-- Family Member 8 -->

                <div id="family8" class="panel-collapse collapse">
                        
                    <div class="row member1">
                        <div class="form__group-fname form__group-hinf3 col-lg-4">

                            <label for="f_name8" class="label__name label__family" >Name</label>
                            <label for="f_name8" class="l_icon" title="Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input name8" id="f_name8" name="f_name8" placeholder="e.g. Melissa">

                        </div>

                        <div class="form__group-flname form__group-hinf3 col-lg-4">

                            <label for="f_lname8" class="label__name label__family" >Last Name</label>
                            <label for="f_lname8" class="l_icon" title="Last Name"> <i class="icon icon_mname"></i> </label>
                            <input type="text" class="form__group-input lname8" id="f_lname8" name="f_lname8" placeholder="e.g. Smith">

                        </div>

                        <div class="form__group-db1 form__group-hinf3 col-lg-4">

                            <label for="db8" class="label__name label__family" >Date of Birth</label>
                            <label for="db8" class="l_icon" title="Date of Birth"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db8" id="db8" name="db8"placeholder="MM-DD-YYYY">

                        </div>
                    
                        <div class="form__group-gender1 form__group-hinf3 col-lg-4">
                            <label for="gender8" class="label__name label__family" >Gender</label>
                            <label for="gender8" class="l_icon" title="Gender"> <i class="icon icon_gender2"></i> </label>
                            <select class="custom-select form__group-input gender8" id="gender8" name="gender8">
                                <option hidden="option" selected disabled>-- Select Gender --</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Private">Private</option>
                            </select>
                        </div>

                        <div class="form__group-relation1 form__group-hinf3 col-lg-4">

                            <label for="re8" class="label__name label__family" >Relation</label>
                            <label for="re8" class="l_icon" title="Relation"> <i class="icon icon_family"></i> </label>
                                <select class="custom-select form__group-input re8" id="re8" name="re8">
                                    <option hidden="option" selected disabled>-- Select Relation --</option>
                                    <option value="Dad">Dad</option>
                                    <option value="Mom">Mom</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Grandparents">Grandparents</option>
                                    <option value="Other">Others</option>
                                </select>
                        </div>

                        <div class="form__group-occupation_f8 form__group-hinf2 col-md-4">
                            <label for="occupation_f8" class="l_icon" title="Date of Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <label for="occupation_f8" class="label__name label__family" >Occupation</label>
                            <input type="text" class="form__group-input db_law" id="occupation_f8" name="occupation_f8" placeholder="e.g. Lawyer">
                        </div>

                        <div class="form__group-db_law1 form__group-hinf3 col-lg-4">

                            <label for="db_lawf8" class="label__name label__family" >Date Background Check</label>
                            <label for="db_lawf8" class="l_icon" title="Date Background Check"> <i class="icon icon_db"></i> </label>
                            <input type="date" class="form__group-input db_lawf8" id="db_lawf8" name="db_lawf8" placeholder="MM-DD-YYYY">

                        </div>

                        <div class="form__group-b-check form__group-hinf3 col-lg-7">
                            <label for="profile-img" class="label__name label__family" >Background Check</label>
                            <label for="profile-img" class="l_icon" title="Background Check"> <i class="icon icon_bcheck"></i> </label>
                            <input type="file" class="form__group-input" name="image[]" id="profile-img" maxlength="1" accept="pdf" style="width: 100%;">
                        </div>
                    </div>
                </div>

            </div>




            <div class="form__group-save">
                <button type="submit" id="submit" class="btn btn-primary ts-btn-arrow btn-lg float-right" style="color: white; background-color: #232159; border: 2px solid #232159;">
                    <i class="fa fa-save mr-2"></i> Register
                </button>
            </div>

            

        </div>
    </main>
    </form>
    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content__p">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content__p">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


    <footer id="ts-footer">

        <?php include 'footer.php' ?>
    </footer>

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/rooms.js"></script>
<script src="assets/js/galery-homestay.js"></script>
<script src="assets/js/rooms_register.js"></script>
<script src="assets/js/validate__register__homestay.js"></script>

<!--Date Input Safari-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script src="assets/js/date-safari.js"></script>

</body>
</html>