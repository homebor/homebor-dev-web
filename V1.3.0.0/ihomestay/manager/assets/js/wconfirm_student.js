$(document).ready(function(){

    load_data(1);
  
    function load_data(page, query = '')
    {
      $.ajax({
        url:"fetch_confirm.php",
        method:"POST",
        data:{page:page, query:query},
        success:function(data)
        {
          $('#dynamic_content').html(data);
        }
      });
    }
  
    $('#vou').hide();
  
    $('#search_box').keyup(function() {
  
        if ($('#search_box').val()) {
  
            $('#dynamic_content').hide();
            $('#vou').show();
  
            let search = $('#search_box').val();
  
            var searchLength = search.length;
  
            $.ajax({
                url: 'wconfirm_stu.php',
                type: 'POST',
                data: { search, },
                success: function(response) {
                    let wstudents = JSON.parse(response);
                    let template = '';
  
                    wstudents.forEach(wstudent => {
                      if(wstudent.photo_s != 'NULL'){
                        template += `
            <tr>
  
              <th class="align-middle text-center" style="font-weight:normal"><img src="../${wstudent.photo_s}" width="90px;" height="90px"></th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.names} </th>
              <th class="align-middle text-center" style="font-weight:normal"> <a class="mail_reserve" href="student_info.php?art_id=${wstudent.id_student}"> ${wstudent.mail_s} </a></th>
              <th class="align-middle text-center" style="font-weight:normal"> <a class="mail_reserve" href="detail.php?art_id=${wstudent.id_home}"> ${wstudent.h_name} </a></th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.bedrooms} </th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.start} </th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.end} </th>
              <th class="align-middle text-center">
                    <form class="buttons" action="confirm_request.php" method="post">
                        <input type="hidden" name="mail_h" value="${wstudent.mail_h}">
                        <input type="hidden" name="mail_s" value="${wstudent.mail_s}">
                        <button type="" name="confirm" class="btn confirm__reserve" title="Confirm Request"> Confirm </button>
                        <button type="button" class="btn cancel__reserve" title="Reject Request" > Reject </button>
                    </form> 
              </th>
  
            </tr>`
                      } else {
                        template += `
            <tr>

              <th class="align-middle text-center" style="font-weight:normal"></th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.names} </th>
              <th class="align-middle text-center" style="font-weight:normal"> <a class="mail_reserve" href="student_info.php?art_id=${wstudent.id_student}"> ${wstudent.mail_s} </a></th>
              <th class="align-middle text-center" style="font-weight:normal"> <a class="mail_reserve" href="detail.php?art_id=${wstudent.id_home}"> ${wstudent.h_name} </a></th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.bedrooms} </th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.start} </th>
              <th class="align-middle text-center" style="font-weight:normal"> ${wstudent.end} </th>
              <th class="align-middle text-center">
                <form class="buttons" action="confirm_request.php" method="post">
                    <input type="hidden" name="mail_h" value="${wstudent.mail_h}">
                    <input type="hidden" name="mail_s" value="${wstudent.mail_s}">
                    <button type="" name="confirm" class="btn confirm__reserve" title="Confirm Request"> Confirm </button>
                    <button type="button" class="btn cancel__reserve" title="Reject Request" > Reject </button>
                </form> 
              </th>
  
            </tr>`
                      }
                        
                    });
  
  
                    $('#vouchers').html(template);
                }
            });
        } else {
            $('#dynamic_content').show();
            $('#vou').hide();
        }
  
    });
  
    $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
    });
  
  });