<?php
    require '../../xeon.php';
    include '../../cript.php';
    session_start(); 
    error_reporting(0);

    // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

    $payments = $link->query("SELECT * FROM payments WHERE r_mail = '$usuario'");
    $rp=mysqli_num_rows($payments);

    
$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Manager') {
        header("location: ../logout.php");   
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homestay Payments</title>

    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">

    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="assets/css/homestay_payments.css">
</head>

<body>

    <header id="ts-header" class="fixed-top" style="background-color: white;">

        <?php include 'header.php' ?>

    </header>

    <main class="main__payments" id="ts-main">

        <div class="payments__title_div">
            <h1>Homestay Payments</h1>
        </div>

        <hr>

        <div class="container col-lg-11">
            <br />
            <div class="card">
              <div class="card-body">
                <!--<div class="form-group">
                  <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here" />
                </div>-->
                <div class="table-responsive" id="dynamic_content">
                </div>

                <div id="table-voun" class="table-responsive text-center ">
                  <table class="table table-hover" id="vou">
                      <thead style="background-color: #232159; color: white">
                        <tr>
                          <th class="text-center align-middle">N° Invoice</th>
                          <th class="text-center align-middle">Date</th>
                          <th class="text-center align-middle">Price</th>
                          <th class="text-center align-middle">Student</th>
                          <th class="text-center align-middle">Status</th>
                          <th class="text-center align-middle"></th>
                        </tr>
                      </thead>

                    <tbody class="table-bordered">
                        <?php while($pay = mysqli_fetch_array($payments)){ 
                            $id_p = strlen($pay['id_p']);

                            $student = $link->query("SELECT * FROM pe_student WHERE mail_s = '$pay[reserve_s]'");
                            $r_stu=$student->fetch_assoc();
                            ?>
                            <tr>
                                <th class="text-center align-middle text_table1">

                                <?php if($id_p == '1'){ ?>

                                    #0000<?php echo $pay['id_p'] ?>

                                <?php }else if($id_p == '2'){ ?>

                                    #000<?php echo $pay['id_p'] ?>

                                <?php }else if($id_p == '3'){ ?>

                                    #00<?php echo $pay['id_p'] ?>

                                <?php }else if($id_p == '4'){ ?>

                                    #0<?php echo $pay['id_p'] ?>

                                <?php }else if($id_p == '5'){ ?>

                                    #<?php echo $pay['id_p'] ?>

                                <?php } ?>

                                </th>
                                <th class="text-center align-middle"><?php echo substr($pay['date_p'], 0, 10); ?></th>
                                <th class="text-center align-middle"><b> CAD$ </b><?php echo $pay['price_p'] ?></th>
                                <th class="text-center align-middle"><a class="link_stu" href="student_info?art_id=<?= $r_stu['id_student'] ?>"><?php echo $r_stu['name_s'] . ' ' . $r_stu['l_name_s'] ?></a></th>
                                <?php if($pay['status_p'] == 'Payable'){ ?>
                                    <th class="text-center align-middle payable">&#9724; <?php echo $pay['status_p'] ?></th>
                                <?php }else if($pay['status_p'] == 'Budgeted'){ ?>
                                    <th class="text-center align-middle budgeted">&#9724; <?php echo $pay['status_p'] ?></th>
                                <?php }else if($pay['status_p'] == 'Paid'){  ?>
                                    <th class="text-center align-middle paid">&#9724; <?php echo $pay['status_p'] ?></th>
                                <?php }else{} ?>
                                <th class="text-center align-middle"><button class="btn" style="background-color: #4f177d; color: white;"> See Invoice</button></th>
                            </tr>

                        <?php } ?>
                        
                    </tbody>

                  </table>
          </div>
              </div>
            </div>
          </div>

</main>

    <footer id="ts-footer">

        <?php include 'footer.php' ?>
    </footer>

</body>
</html>