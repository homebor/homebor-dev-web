<?php error_reporting(0);?>
<link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
    <!--*********************************************************************************************************-->
    <!--************ FOOTER *************************************************************************************-->
    <!--*********************************************************************************************************-->

    <footer id="ts-footer">

        <!--MAIN FOOTER CONTENT
            =========================================================================================================-->
<section id="ts-footer-main" >
            <div class="container">
                <div class="row">
                    <!--Brand and description-->
                    <div class="col-md-6">
                        <a href="index.php" class="brand">
                            <img src="../assets/logos/page.png">
                        </a>
                        <p class="mb-4" id="text">
                        We are an accommodation platform that help schools to connects quality homestay families with international students offering them the best experience for everyone involved.
                        </p>    
                    </div>
                    <!--Navigation-->
                    <div id="nav-footer" class="col-md-2">
                        <h4 id="footer">Navigation</h4>
                        <nav class="nav flex-row flex-md-column mb-4">
                            <a href="index.php" class="nav-link">Main</a>
                            <a href="agency_info.php" class="nav-link">Provider Info</a>
                            <a href="edit_agents.php" class="nav-link">Coordinators</a>
                            <a href="edit_propertie.php" class="nav-link">Properties</a>
                            <a href="edit_students.php" class="nav-link">Students</a>
                            <a href="../logout.php" class="nav-link">Logout</a>
                        </nav>
                    </div>
                    <!--Contact Info-->
                    <div id="con-footer" class="col-md-4">
                        <h4 id="footer">Contact</h4>
                        <address class="ts-text-color-light">
                            <strong>Email: </strong>
                            <a href="#" class="btn-link">info@homebor.com</a>
                            <br>
                            <strong>Phone:</strong>
                            +1 (407) 485-0374
                        </address>
                    </div>

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-main-->
        <!--SECONDARY FOOTER CONTENT
        =============================================================================================================-->
        <section id="ts-footer-secondary" class="mb-0" data-bg-pattern="../assets/img/bg-pattern-dot.png" style="background-image: url('../assets/img/bg-pattern-dot.png');">
            <div class="container">
                <!--Copyright-->
                <img id="white-logo" src="../assets/logos/white.png">
                <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
                <!--Social Icons-->
                <div class="ts-footer-nav">
                <nav class="nav">
                        <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
                            <i class="fab fa-facebook-f" id="footer"></i>
                        </a>
                        <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
                            <i class="fab fa-twitter" id="footer"></i>
                        </a>
                        <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
                            <i class="fab fa-instagram" id="footer"></i>
                        </a>
                    </nav>
                </div>
                <!--end ts-footer-nav-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-secondary-->
    </footer>
    <!--end #ts-footer-->