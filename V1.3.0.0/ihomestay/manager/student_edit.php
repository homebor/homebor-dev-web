<?php
session_start();
include_once('../../xeon.php');
include '../../cript.php';
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_student WHERE id_student = '$id' ");
$row=$query->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$id' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query8 = $link->query("SELECT * FROM users WHERE mail = '$row[mail_s]' ");
$row8=$query8->fetch_assoc();


$query6 = $link->query("SELECT * FROM academy WHERE id_ac != '0'");
$row6=$query6->fetch_assoc();


$query1 = $link->query("SELECT * FROM pe_student INNER JOIN academy ON pe_student.id_student = '$id' AND pe_student.n_a = academy.id_ac");
$row1=$query1->fetch_assoc();

$query7 = $link->query("SELECT * FROM manager WHERE mail = '$usuario' ");
    $row7=$query7->fetch_assoc();



if ($row['id_m'] != $row7['id_m'] ) {
    header("location: index.php");
 }

}

if ($row5['usert'] != 'Manager') {
        
    header("location: ../logout.php");   
   }
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/student_delete.css">
  <link rel="stylesheet" type="text/css" href="assets/css/student_info2.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students Advance Options</title>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>


</head>

<body onload="calWeek()">


  <header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
    <?php include 'header.php' ?>

  </header>
  <!--end Header-->


  <div class="container_register">

    <main class="card__register">
      <form class="ts-form" action="editstudent.php" method="post" autocomplete="off" enctype="multipart/form-data">

        <?php
            echo '<input type="text" class="form-control" id="mail" name="mail"  value='.$row7['mail'].' hidden>
            <input type="text" class="form-control" id="id_m" name="id_m"  value='.$row7['id_m'].' hidden>
            <input type="text" class="form-control" id="mail_s" name="mail_s"  value='.$row['mail_s'].' hidden>';
        ?>

        <div class="row m-0 p-0">

          <div class="col-md-4 column-1">

            <div class="card">

              <h3 class="form__group-title__col-4">Basic Information</h3>

              <div class="information__content">

                <div class="form__group__div__photo">

                  <?php if(empty($row['photo_s']) || $row['photo_s'] == 'NULL'){ ?>

                  <img class="form__group__photo" id="preview" src="" alt="">

                  <label for="file" class="photo-add" id="label-i">
                    <p class="form__group-l_title"> Add Profile Photo </p>
                  </label>

                  <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s"
                    id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                  <?php }else{ ?>

                  <img class="form__group__photo" id="preview" src="../../<?php echo $row['photo_s'] ?>" alt="">

                  <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s"
                    id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                  <?php } ?>


                  <?php if(empty($row['photo_s']) || $row['photo_s'] == 'NULL'){ ?>

                  <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;"
                    title="Change Frontage Photo"></label>

                  <?php }else{ ?>

                  <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i"
                    title="Change Frontage Photo"></label>

                  <?php } ?>


                </div>

                <div class="info"></div>



                <!-- Group Name -->

                <div class="form__group form__group__name" id="group__name">
                  <div class="form__group__icon-input">
                    <label for="name" class="form__label">First Name</label>

                    <label for="name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['name_s']) || $row['name_s'] == 'NULL'){ ?>

                    <input type="text" id="name" name="name" class="form__group-input names" placeholder="First Name"
                      placeholder="e.g. John">

                    <?php }else{ ?>

                    <input type="text" id="name" name="name" class="form__group-input names" placeholder="First Name"
                      value="<?php echo $row['name_s'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>

                </div>

                <!-- Group Last Name -->

                <div class="form__group form__group__l_name" id="group__l_name">
                  <div class="form__group__icon-input">

                    <label for="l_name" class="form__label">Last Name</label>
                    <label for="l_name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['l_name_s']) || $row['l_name_s'] == 'NULL'){ ?>

                    <input type="text" id="l_name" name="l_name" class="form__group-input names"
                      placeholder="Last Name">

                    <?php }else{ ?>

                    <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="Last Name"
                      value="<?php echo $row['l_name_s'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>
                </div>


                <!-- Group Mail -->

                <div class="form__group form__group__mail_s" id="group__mail_s">
                  <div class="form__group__icon-input">

                    <label for="mail_s" class="form__label">Student Mail</label>

                    <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>

                    <?php if(empty($row['mail_s']) || $row['mail_s'] == 'NULL'){ ?>

                    <input type="text" id="mail_s" name="mail_s" class="form__group-input mail"
                      placeholder="e.g. jsmith@gmail.com">

                    <?php }else{ ?>

                    <input type="text" id="mail_s" name="mail_s" class="form__group-input mail"
                      value="<?php echo $row['mail_s'] ?>" readonly>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">It is not a valid Email</p>
                </div>


                <!-- Group Password -->

                <div class="form__group form__group__password" id="group__password">
                  <div class="form__group__icon-input">

                    <label for="password" class="form__label">Password</label>

                    <label for="password" class="icon"><i class="icon icon__password"></i></label>

                    <?php if(empty($row5['psw']) || $row5['psw'] == 'NULL'){ ?>

                    <input type="password" id="password" name="password" class="form__group-input password"
                      placeholder="min 8 Characters" readonly>

                    <?php }else{
                                    $psw = SED::decryption($row5['psw']); 
                                ?>

                    <input type="password" id="password" name="password" class="form__group-input password"
                      value="<?php echo $psw ?>" readonly>

                    <?php } ?>


                  </div>
                  <p class="form__group__input-error">Numbers, Uppercase, Lowercase, and Special Characters</p>
                </div>

                <!-- Arrive Date -->
                <div class="form__group form__group-arrive_g">

                  <div class="form__group__icon-input">

                    <label for="arrive_g" class="form__label">Arrive Date</label>
                    <label for="arrive_g" class="icon"><i class="icon icon__date"></i></label>

                    <?php if(empty($row['arrive_g']) || $row['arrive_g'] == 'NULL'){ 

                                    $arrive_g_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado. 
                                    
                                ?>

                    <input type="text" id="arrive_g" name="arrive_g" class="form__group-input h_date" readonly>

                    <input type="text" id="date_arrive_g" class="form__group-input h_date"
                      value="<?php echo $arrive_g_date ?>" hidden readonly>

                    <?php }else{ 

                                    $date_arrive_g = date_create($row['arrive_g']); //Se crea una fecha con el formato que recibes de la vista.
                                    $arrive_g_date = date_format($date_arrive_g, 'm/d/Y'); //Obtienes la fecha en el formato deseado.  
                                    
                                ?>

                    <input type="text" id="arrive_g" name="arrive_g" class="form__group-input h_date"
                      value="<?php echo $row['arrive_g'] ?>" readonly>

                    <input type="text" id="date_arrive_g" class="form__group-input h_date"
                      value="<?php echo $arrive_g_date ?>" hidden readonly>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>


                <!-- Group Destination City  -->
                <div class="form__group form__group__basic form__group__destination_c" id="group__destination_c">

                  <div class="form__group__icon-input">

                    <label for="destination_c" class="form__label smoker"> Destination City </label>

                    <label for="destination_c" class="icon"><i class="icon icon__city"></i></label>

                    <select class="custom-select form__group-input smoke_s" id="destination_c" name="destination_c"
                      onchange="otherCity()">

                      <?php if(empty($row['destination_c']) || $row['destination_c'] == 'NULL'){ ?>

                      <option hidden="option" selected disabled>Select Option</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Toronto'){ ?>

                      <option value="Toronto" selected>Toronto</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Montreal'){ ?>

                      <option value="Montreal" selected>Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Ottawa'){ ?>

                      <option value="Ottawa" selected>Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Quebec'){ ?>

                      <option value="Quebec" selected>Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Calgary'){ ?>

                      <option value="Calgary" selected>Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Vancouver'){ ?>

                      <option value="Vancouver" selected>Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] == 'Victoria'){ ?>

                      <option value="Victoria" selected>Victoria</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Other">Other</option>

                      <?php }elseif($row['destination_c'] != 'Victoria' || $row['destination_c'] != 'Vancouver' || $row['destination_c'] != 'Calgary' || $row['destination_c'] != 'Quebec' || $row['destination_c'] != 'Ottawa' || $row['destination_c'] != 'Montreal' || $row['destination_c'] != 'Toronto'){ ?>

                      <option value="Other" selected>Other</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>

                      <?php } ?>


                    </select>
                  </div>
                  <p class="form__group__input-error">Enter a valid Date</p>

                </div>

                <div class="form__group form__group__basic form__group__other_c d-none" id="group__other_c">

                  <label class="label__other__city">Specify the Other City</label>

                  <?php if($row['destination_c'] != 'Victoria' || $row['destination_c'] != 'Vancouver' || $row['destination_c'] != 'Calgary' || $row['destination_c'] != 'Quebec' || $row['destination_c'] != 'Ottawa' || $row['destination_c'] != 'Montreal' || $row['destination_c'] != 'Toronto'){ ?>

                  <input class="input__other_city" id="text_destination" type="text" name="destination_c_o"
                    value="<?php echo $row['destination_c'] ?>">

                  <?php }else{ ?>

                  <input class="input__other_city" id="text_destination" type="text" name="destination_c_o">

                  <?php } ?>

                </div>


                <script>
                const $city = document.querySelector("#destination_c");

                function otherCity() {
                  const citiI = $city.selectedIndex;
                  if (citiI === -1) return; // Esto es cuando no hay elementos
                  const optionCity = $city.options[citiI];

                  if (optionCity.value == 'Other') {

                    document.getElementById('group__other_c').classList.remove('d-none');

                  } else {
                    document.getElementById('group__other_c').classList.add('d-none');
                    document.getElementById('text_destination').value = '';
                  }

                };

                otherCity();
                </script>



              </div>

            </div>


            <div class="card column2">


              <h3 class="form__group-title__col-4">Personal Student Address</h3>

              <div class="information__content">

                <!-- Group Origin City -->
                <div class="form__group form__group__basic form__group__country" id="group__country">
                  <div class="form__group__icon-input">

                    <label for="country" class="form__label">Country</label>

                    <label for="country" class="icon"><i class="icon icon__dir"></i></label>

                    <?php if(empty($row['country']) || $row['country'] == 'NULL'){ ?>

                    <input type="text" id="country" name="country" class="form__group-input country"
                      placeholder="e.g. Mexico">

                    <?php }else{ ?>

                    <input type="text" id="country" name="country" class="form__group-input country"
                      value="<?php echo $row['country'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Enter a valid Date</p>
                </div>


                <!-- Group Address -->
                <div class="form__group form__group__dir" id="group__dir">
                  <div class="form__group__icon-input">

                    <label for="dir" class="form__label">Address </label>

                    <label for="dir" class="icon"><i class="icon icon__dir"></i></label>

                    <?php if(empty($row['dir_s']) || $row['dir_s'] == 'NULL'){ ?>

                    <input type="text" id="dir" name="dir" class="form__group-input dir" placeholder="Av, Street, etc.">

                    <?php }else{ ?>

                    <input type="text" id="dir" name="dir" class="form__group-input dir"
                      value="<?php echo $row['dir_s'] ?>">

                    <?php } ?>

                  </div>

                  <p class="form__group__input-error">Please enter a valid Address</p>
                </div>

                <!-- Group City -->
                <div class="form__group form__group__city" id="group__city">
                  <div class="form__group__icon-input">

                    <label for="city" class="form__label">City </label>

                    <label for="city" class="icon"><i class="icon icon__dir"></i></label>


                    <?php if(empty($row['city_s']) || $row['city_s'] == 'NULL'){ ?>

                    <input type="text" id="city" name="city" class="form__group-input city2"
                      placeholder="e.g. Monterrey">

                    <?php }else{ ?>

                    <input type="text" id="city" name="city" class="form__group-input city2"
                      value="<?php echo $row['city_s'] ?>">

                    <?php } ?>



                  </div>

                  <p class="form__group__input-error">Please enter a valid City</p>
                </div>

                <!-- Group State -->
                <div class="form__group form__group__state" id="group__state">
                  <div class="form__group__icon-input">

                    <label for="state" class="form__label"> Province / State </label>

                    <label for="state" class="icon"><i class="icon icon__dir"></i></label>

                    <?php if(empty($row['state_s']) || $row['state_s'] == 'NULL'){ ?>

                    <input type="text" id="state" name="state" class="form__group-input state"
                      placeholder="e.g. Yucatán">

                    <?php }else{ ?>

                    <input type="text" id="state" name="state" class="form__group-input state"
                      value="<?php echo $row['state_s'] ?>">

                    <?php } ?>

                  </div>

                  <p class="form__group__input-error">Please enter a valid State</p>
                </div>

                <!-- Group Postal Code -->
                <div class="form__group form__group__p_code" id="group__p_code">
                  <div class="form__group__icon-input">

                    <label for="p_code" class="form__label">Postal Code </label>

                    <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>

                    <?php if(empty($row['p_code_s']) || $row['p_code_s'] == 'NULL'){ ?>

                    <input type="text" id="p_code" name="p_code" class="form__group-input p_code"
                      placeholder="e.g. 145187">

                    <?php }else{ ?>

                    <input type="text" id="p_code" name="p_code" class="form__group-input p_code"
                      value="<?php echo $row['p_code_s'] ?>">

                    <?php } ?>

                  </div>

                  <p class="form__group__input-error">Please enter a valid Postal Code</p>
                </div>

              </div>

            </div>


            <div class="card column3">


              <h3 class="form__group-title__col-4">Accommodation Request</h3>

              <div class="information__content">

                <!-- Group Start Date of Stay -->
                <div class="form__group form__group__firstd" id="group__firstd">
                  <div class="form__group__icon-input">

                    <label for="firstd" class="form__label">Start Date of Stay</label>
                    <label for="firstd" class="icon"><i class="icon icon__date"></i></label>

                    <?php if(empty($row['firstd']) || $row['firstd'] == 'NULL'){ 

                                    $firstd_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado. 
                                    
                                ?>

                    <input type="text" id="firstd" name="firstd" class="form__group-input firstd" readonly>

                    <input type="text" id="date_firstd" class="form__group-input h_date"
                      value="<?php echo $firstd_date ?>" hidden readonly>

                    <?php }else{ 

                                    $date_firstd = date_create($row['firstd']); //Se crea una fecha con el formato que recibes de la vista.
                                    $firstd_date = date_format($date_firstd, 'm/d/Y'); //Obtienes la fecha en el formato deseado. 
                                    
                                ?>

                    <input type="text" id="firstd" name="firstd" class="form__group-input firstd"
                      value="<?php echo $row['firstd'] ?>">

                    <input type="text" id="date_firstd" class="form__group-input h_date"
                      value="<?php echo $firstd_date ?>" hidden readonly>

                    <?php } ?>



                  </div>
                  <p class="form__group__input-error">Enter a valid Date</p>
                </div>

                <!-- Group End Date of stay -->
                <div class="form__group form__group__lastd" id="group__lastd">
                  <div class="form__group__icon-input">

                    <label for="lastd" class="form__label">End Date of Stay</label>
                    <label for="lastd" class="icon"><i class="icon icon__date"></i></label>

                    <?php if(empty($row['lastd']) || $row['lastd'] == 'NULL'){ 

                                    $lastd_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado. 
                                    
                                ?>

                    <input type="text" id="lastd" name="lastd" class="form__group-input lastd" readonly>

                    <input type="text" id="date_firstd" class="form__group-input h_date"
                      value="<?php echo $lastd_date ?>" hidden readonly>

                    <?php }else{ 

                                    $datetime1 = new DateTime($row["firstd"]);
                                    $datetime2 = new DateTime($row["lastd"]);
                                    $interval = $datetime1->diff($datetime2);

                                    if($interval->format("%a") % 7 == '0'){

                                        $w_date = floor(($interval->format("%a") / 7)) . " weeks";

                                    }else if(floor(($interval->format("%a") / 7)) / 7 == '0'){

                                        $w_date = floor(($interval->format("%a") % 7));
                                        
                                    }else{

                                        $w_date = floor(($interval->format("%a") / 7)) . " weeks and ". ($interval->format("%a") % 7) . " days";
                                    }

                                    $date_lastd = date_create($row['lastd']); //Se crea una fecha con el formato que recibes de la vista.
                                    $lastd_date = date_format($date_lastd, 'm/d/Y'); //Obtienes la fecha en el formato deseado. 
                                    
                                ?>

                    <input type="text" id="lastd" name="lastd" class="form__group-input lastd"
                      value="<?php echo $row['lastd'] ?>" readonly>

                    <input type="text" id="date_lastd" class="form__group-input h_date"
                      value="<?php echo $lastd_date ?>" hidden readonly>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Enter a valid Date</p>
                </div>



                <input type="date" id="firstd2" hidden>
                <input type="date" id="lastd2" hidden>

                <p id="calcWeeks" class="calc__weeks"></p>

              </div>

            </div>


            <div class="card">


              <h3 class="form__group-title__col-4">Flight Information</h3>

              <div class="information__content">

                <!-- Group Booking Confirmation -->
                <div class="form__group form__group__n_airline" id="group__n_airline">
                  <div class="form__group__icon-input">

                    <label for="n_airline" class="form__label">Booking Confirmation</label>

                    <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                    <?php if(empty($row['n_airline']) || $row['n_airline'] == 'NULL'){  ?>

                    <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline"
                      placeholder="Booking Confirmation">

                    <?php }else{ ?>

                    <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline"
                      placeholder="Booking Confirmation" value="<?php echo $row['n_airline']; ?>">

                    <?php } ?>
                    <p class="form__group__input-error">Minimum 4 Characters</p>

                  </div>
                </div>

                <!-- Group Booking Confirmation -->
                <div class="form__group form__group__n_flight" id="group__n_flight">
                  <div class="form__group__icon-input">

                    <label for="n_flight" class="form__label">Landing Flight Number</label>

                    <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                    <?php if(empty($row['n_flight']) || $row['n_flight'] == 'NULL'){  ?>

                    <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight"
                      placeholder="Landing Flight Number">

                    <?php } else{ ?>

                    <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight"
                      placeholder="Landing Flight Number" value="<?php echo $row['n_flight']; ?>">

                    <?php } ?>
                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>


                <div class="form__group form__group-f_date ">
                  <div class="form__group__icon-input">
                    <label for="f_date" class="form__label">Flight Date</label>
                    <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                    <?php if(empty($row['departure_f']) || $row['departure_f'] == 'NULL'){ 
                                        
                                        $f_date_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                    <input type="text" id="f_date" name="f_date" class="form__group-input f_date" readonly>

                    <input type="text" id="date_firstd" class="form__group-input h_date"
                      value="<?php echo $f_date_date ?>" hidden readonly>

                    <?php } else{ 
                                        
                                        $date_f_date = date_create($row['departure_f']); //Se crea una fecha con el formato que recibes de la vista.
                                        $f_date_date = date_format($date_f_date, 'm/d/Y h:i A'); //Obtienes la fecha en el formato deseado.  
                                        
                                    ?>

                    <input type="text" id="f_date" name="f_date" class="form__group-input f_date"
                      value="<?php echo $row['departure_f']; ?>" readonly>

                    <input type="text" id="date_f_date" class="form__group-input h_date"
                      value="<?php echo $f_date_date ?>" hidden readonly>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>

                <div class="form__group form__group-h_date">

                  <div class="form__group__icon-input">

                    <label for="h_date" class="form__label">Arrival at the Homestay</label>
                    <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                    <?php if(empty($row['arrive_f']) || $row['arrive_f'] == 'NULL'){  

                                        $h_date_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado. 
                                        
                                    ?>

                    <input type="text" id="h_date" name="h_date" class="form__group-input h_date" readonly>

                    <input type="text" id="date_h_date" class="form__group-input h_date"
                      value="<?php echo $h_date_date ?>" hidden readonly>

                    <?php }else{ 

                                        $date_h_date = date_create($row['arrive_f']); //Se crea una fecha con el formato que recibes de la vista.
                                        $h_date_date = date_format($date_h_date, 'm/d/Y'); //Obtienes la fecha en el formato deseado. 
                                        
                                    ?>

                    <input type="text" id="h_date" name="h_date" class="form__group-input h_date"
                      value="<?php echo $row['arrive_f']; ?>" readonly>

                    <input type="text" id="date_h_date" class="form__group-input h_date"
                      value="<?php echo $h_date_date ?>" hidden readonly>

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>

              </div>

            </div>



            <div class="card">

              <h3 class="form__group-title__col-4">Emergency Contact</h3>

              <div class="information__content">


                <!-- Group Emergency Name -->
                <div class="form__group form__group__cont_name" id="group__cont_name">
                  <div class="form__group__icon-input">

                    <label for="cont_name" class="form__label">Contact Name</label>

                    <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['cont_name']) || $row['cont_name'] == 'NULL'){  ?>

                    <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name"
                      placeholder="Contact Name">

                    <?php }else{ ?>

                    <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name"
                      placeholder="Contact Name" value="<?php echo $row['cont_name']; ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>

                <!-- Group Emergency Last Name -->
                <div class="form__group form__group__cont_lname" id="group__cont_lname">

                  <div class="form__group__icon-input">

                    <label for="cont_lname" class="form__label">Contact Last Name</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['cont_lname']) || $row['cont_lname'] == 'NULL'){  ?>

                    <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname"
                      placeholder="Contact Last Name">

                    <?php } else{ ?>

                    <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname"
                      placeholder="Contact Last Name" value="<?php echo $row['cont_lname'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>


                <!-- Group Emergency phone -->
                <div class="form__group form__group__num_conts" id="group__num_conts">

                  <div class="form__group__icon-input">

                    <label for="num_conts" class="form__label">Emergency Phone Number</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                    <?php if(empty($row['num_conts']) || $row['num_conts'] == 'NULL'){  ?>

                    <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts"
                      placeholder="Emergency Phone Number">

                    <?php } else{ ?>

                    <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts"
                      placeholder="Emergency Phone Number" value="<?php echo $row['num_conts']; ?>">

                    <?php } ?>
                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>

                <!-- Group Alternative Phone -->
                <div class="form__group form__group__cell_s" id="group__cell_s">
                  <div class="form__group__icon-input">

                    <label for="cell_s" class="form__label">Alternative Phone Number</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                    <?php if(empty($row['cell_s']) || $row['cell_s'] == 'NULL'){  ?>

                    <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s"
                      placeholder="Alternative Phone Number">

                    <?php }else{ ?>

                    <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s"
                      placeholder="Alternative Phone Number" value="<?php echo $row['cell_s'] ?>">

                    <?php } ?>
                  </div>
                  <p class="form__group__input-error">Minimum 4 Characters</p>
                </div>


              </div>

            </div>

          </div>







          <div class="col-md-8 column-2">

            <div class="card">

              <h3 class="form__group-title__col-4">Personal Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Current About Me -->
                  <div class="form__group col-sm-12 form__group__basic form__group__preference form__group__about_me"
                    id="group__about_me">

                    <div class="form__group__icon-input">

                      <label for="about_me" class="form__label allergies_l">Tell us about yourself (max. 150
                        Characters).</label>

                      <label for="about_me" class="icon"><i class="icon icon__about_me"></i></label>

                      <?php if(empty($row['about_me']) || $row['about_me'] == 'NULL'){  ?>

                      <textarea class="form__group-input allergies" id="about_me" rows="2" name="about_me"
                        placeholder="About you. Maximum 150 characters." maxlength="150"></textarea>

                      <?php } else{ ?>

                      <textarea class="form__group-input allergies" id="about_me" rows="2" name="about_me"
                        placeholder="About you. Maximum 150 characters."
                        maxlength="150"><?php echo $row['about_me'] ?></textarea>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Date of Birth -->
                  <div class="form__group col-lg-4 form__group__db" id="group__db">
                    <div class="form__group__icon-input">

                      <label for="db" class="form__label">Date of Birth</label>
                      <label for="db" class="icon"><i class="icon icon__date"></i></label>

                      <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL'){  

                                        $db_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                      <input type="text" id="db" name="db" class="form__group-input db" readonly>

                      <input type="text" id="date_firstd" class="form__group-input h_date"
                        value="<?php echo $db_date ?>" hidden readonly>

                      <?php }else{ 

                                        $date_db = date_create($row['db_s']); //Se crea una fecha con el formato que recibes de la vista.
                                        $db_date = date_format($date_db, 'm/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                      <input type="text" id="db" name="db" class="form__group-input db"
                        value="<?php echo $row['db_s']; ?>" readonly>

                      <input type="text" id="date_db" class="form__group-input h_date" value="<?php echo $db_date ?>"
                        hidden readonly>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Gender -->
                  <div class="form__group col-lg-4 form__group__basic form__group__gender" id="group__gender">

                    <div class="form__group__icon-input">

                      <label for="gender" class="form__label">Gender</label>

                      <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                      <select class="custom-select form__group-input gender" id="gender" name="gender">
                        <?php if(empty($row['gen_s']) || $row['gen_s'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled> Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Any">Any</option>

                        <?php }else if($row['gen_s'] == 'Male'){ ?>

                        <option value="Male" selected>Male</option>
                        <option value="Female">Female</option>
                        <option value="Any">Any</option>

                        <?php }else if($row['gen_s'] == 'Female'){ ?>

                        <option value="Female" selected>Female</option>
                        <option value="Male">Male</option>
                        <option value="Any">Any</option>

                        <?php }else if($row['gen_s'] == 'Any'){ ?>

                        <option value="Any" selected>Any</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>

                        <?php } ?>

                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a Gender</p>
                  </div>


                  <!-- Group Number -->

                  <div class="form__group col-lg-4 form__group__num_s" id="group__num_s">

                    <div class="form__group__icon-input">

                      <label for="num_s" class="form__label">Phone Number</label>

                      <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>

                      <?php if(empty($row['num_s']) || $row['num_s'] == 'NULL'){  ?>

                      <input type="num" id="num_s" name="num_s" class="form__group-input num_s"
                        placeholder="Phone Number">

                      <?php }else{ ?>

                      <input type="num" id="num_s" name="num_s" class="form__group-input num_s"
                        placeholder="Phone Number" value="<?php echo $row['num_s']; ?>">
                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Minimum 4 Characters</p>
                  </div>



                  <!-- Group Background -->
                  <div class="form__group col-lg-4 form__group__basic form__group__nacionality" id="group__nacionality">
                    <div class="form__group__icon-input">

                      <label for="nacionality" class="form__label">Background</label>

                      <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>
                      <?php if(empty($row['nationality']) || $row['nationality'] == 'NULL'){  ?>
                      <input type="text" id="nacionality" name="nationality" class="form__group-input nacionality"
                        placeholder="e.g. Mexican">

                      <?php }else{ ?>

                      <input type="text" id="nacionality" name="nationality" class="form__group-input nacionality"
                        placeholder="e.g. Mexican" value="<?php echo $row['nationality'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Origin Language -->
                  <div class="form__group col-lg-4 form__group__basic form__group__lang_s" id="group__lang_s">
                    <div class="form__group__icon-input">

                      <label for="lang_s" class="form__label">Origin Language</label>

                      <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>
                      <?php if(empty($row['lang_s']) || $row['lang_s'] == 'NULL'){  ?>
                      <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s"
                        placeholder="Origin Language">
                      <?php }else{ ?>
                      <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s"
                        placeholder="Origin Language" value="<?php echo $row['lang_s'] ?>">
                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Another Language -->
                  <div class="form__group col-lg-4 form__group__basic form__group__language_a" id="group__language_a">
                    <div class="form__group__icon-input">

                      <label for="language_a" class="form__label">Another Language</label>

                      <label for="language_a" class="icon"><i class="icon icon__nacionality"></i></label>

                      <?php if(empty($row['language_a']) || $row['language_a'] == 'NULL'){  ?>

                      <input type="text" id="language_a" name="language_a" class="form__group-input nacionality"
                        placeholder="e.g. Spanish">

                      <?php }else{ ?>

                      <input type="text" id="language_a" name="language_a" class="form__group-input nacionality"
                        value="<?php echo $row['language_a'] ?>">

                      <?php } ?>


                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Passport -->
                  <div class="form__group col-lg-4 form__group__basic form__group__pass" id="group__pass">
                    <div class="form__group__icon-input">

                      <label for="pass" class="form__label visa">Passport</label>

                      <label for="pass" class="icon"><i class="icon icon__pass"></i></label>

                      <?php if(empty($row['passport']) || $row['passport'] == 'NULL'){  ?>
                      <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport">
                      <?php } else{ ?>

                      <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport"
                        value="<?php echo $row['passport'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Passport -->
                  <div class="form__group col-lg-4 form__group__basic form__group__exp_pass" id="group__exp_pass">
                    <div class="form__group__icon-input">

                      <label for="exp_pass" class="form__label visa">Passport Expiration Date</label>

                      <label for="exp_pass" class="icon"><i class="icon icon__bl"></i></label>

                      <?php if(empty($row['exp_pass']) || $row['exp_pass'] == 'NULL'){ 

                                        $exp_pass_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                      <input type="text" id="exp_pass" name="exp_pass" class="form__group-input pass" readonly>

                      <input type="text" id="date_exp_pass" class="form__group-input h_date"
                        value="<?php echo $exp_pass_date ?>" hidden readonly>

                      <?php } else{ 

                                        $date_exp_pass = date_create($row['exp_pass']); //Se crea una fecha con el formato que recibes de la vista.
                                        $exp_pass_date = date_format($date_exp_pass, 'm/d/Y'); //Obtienes la fecha en el formato deseado.  
                                        
                                    ?>

                      <input type="text" id="exp_pass" name="exp_pass" class="form__group-input pass"
                        value="<?php echo $row['exp_pass'] ?>" readonly>

                      <input type="text" id="date_exp_pass" class="form__group-input h_date"
                        value="<?php echo $exp_pass_date ?>" hidden readonly>

                      <?php } ?>


                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Visa Expiration Date -->
                  <div class="form__group col-lg-4 form__group__basic form__group__bl" id="group__bl">
                    <div class="form__group__icon-input">

                      <label for="bl" class="form__label visa">Visa Expiration Date</label>
                      <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                      <?php if(empty($row['db_visa']) || $row['db_visa'] == 'NULL'){  

                                        $bl_date = date('m/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                      <input type="text" id="bl" name="bl" class="form__group-input bl"
                        placeholder="Visa Expiration Date">

                      <input type="text" id="date_bl" class="form__group-input h_date" value="<?php echo $bl_date ?>"
                        hidden readonly>

                      <?php }else{ 

                                        $date_bl = date_create($row['db_visa']); //Se crea una fecha con el formato que recibes de la vista.
                                        $bl_date = date_format($date_bl, 'm/d/Y'); //Obtienes la fecha en el formato deseado.
                                        
                                    ?>

                      <input type="text" id="bl" name="bl" class="form__group-input bl"
                        value="<?php echo $row['db_visa'] ?>" readonly>

                      <input type="text" id="date_bl" class="form__group-input h_date" value="<?php echo $bl_date ?>"
                        hidden readonly>

                      <?php } ?>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                </div>

              </div>

            </div>

            <div class="card column7">

              <h3 class="form__group-title__col-4">Health Information</h3>

              <!-- <h3 class="subtitle__section-card">Health Information</h3> -->

              <div class="information__content information__content-col-8 pb-2 pt-4">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Smoker -->
                  <div class="form__group col-lg-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Do you smoke?</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="smoke_s" name="smoke_s">

                        <?php if(empty($row['smoke_s']) || $row['smoke_s'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['smoke_s'] == 'Yes'){  ?>

                        <option value="Yes" selected>Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['smoke_s'] == 'No'){  ?>

                        <option value="No" selected>No</option>
                        <option value="Yes">Yes</option>

                        <?php } ?>


                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <!-- Group Alcoholic Beverages -->
                  <div class="form__group col-lg-4 form__group__basic form__group__drinks_alc" id="group__drinks_alc">

                    <div class="form__group__icon-input">

                      <label for="drinks_alc" class="form__label smoker">Do you Drink Alcohol?</label>

                      <label for="drinks_alc" class="icon"><i class="icon icon__drinks_alc"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="drinks_alc" name="drinks_alc">
                        <?php if(empty($row['drinks_alc']) || $row['drinks_alc'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['drinks_alc'] == 'yes'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['drinks_alc'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <!-- Group Drugs -->
                  <div class="form__group col-lg-4 form__group__basic form__group__drugs" id="group__drugs">

                    <div class="form__group__icon-input">

                      <label for="drugs" class="form__label smoker">Have you used any drugs?</label>

                      <label for="drugs" class="icon"><i class="icon icon__drugs"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="drugs" name="drugs">
                        <?php if(empty($row['drugs']) || $row['drugs'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['drugs'] == 'yes'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['drugs'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <!-- Group Allergy to Animals -->
                  <div class="form__group col-lg-5 form__group__basic form__group__allergy_a" id="group__allergy_a">

                    <div class="form__group__icon-input">

                      <label for="allergy_a" class="form__label pets_l">Any Allergy to Animals</label>

                      <label for="allergy_a" class="icon"><i class="icon icon__pets"></i></label>

                      <select class="custom-select form__group-input pets" id="allergy_a" name="allergy_a"
                        onchange="allergyAnimals()">
                        <?php if(empty($row['allergy_a']) || $row['allergy_a'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['allergy_a'] != 'no'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['allergy_a'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>


                    <div class="form__group__icon-input d-none" id="specify_allergy_a">

                      <label class="label__specify2">Specify</label>

                      <?php if($row['allergy_a'] != 'no'){  ?>

                      <input class="input__specify" id="text__allergy_a" type="text" name="allergy_a_o"
                        value="<?php echo $row['allergy_a'] ?>">

                      <?php }else { ?>

                      <input class="input__specify" id="text__allergy_a" type="text" name="allergy_a_o">

                      <?php } ?>

                    </div>


                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>



                  <!-- Group Dietary Restrictions -->
                  <div class="form__group col-lg-5 form__group__basic form__group__allergy_m" id="group__allergy_m">

                    <div class="form__group__icon-input">

                      <label for="allergy_m" class="form__label pets_l">Dietary Restrictions</label>

                      <label for="allergy_m" class="icon"><i class="icon icon__food"></i></label>

                      <select class="custom-select form__group-input pets" id="allergy_m" name="allergy_m"
                        onchange="allergyMeals()">
                        <?php if(empty($row['allergy_m']) || $row['allergy_m'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['allergy_m'] != 'no'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['allergy_m'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>


                    <div class="form__group__icon-input d-none" id="specify_allergy_m">

                      <label class="label__specify2">Specify</label>

                      <?php if($row['allergy_m'] != 'no'){  ?>

                      <input class="input__specify" id="text__allergy_m" type="text" name="allergy_m_o"
                        value="<?php echo $row['allergy_m'] ?>">

                      <?php }else { ?>

                      <input class="input__specify" id="text__allergy_m" type="text" name="allergy_m_o">

                      <?php } ?>


                    </div>


                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <script>
                  const $allergyA = document.querySelector("#allergy_a");

                  function allergyAnimals() {
                    const allergyA = $allergyA.selectedIndex;
                    if (allergyA === -1) return; // Esto es cuando no hay elementos
                    const optionAallergy = $allergyA.options[allergyA];

                    if (optionAallergy.value == 'yes') {

                      document.getElementById('specify_allergy_a').classList.remove('d-none');

                    } else {
                      document.getElementById('specify_allergy_a').classList.add('d-none');

                      document.getElementById('text__allergy_a').value = '';
                    }

                  };

                  allergyAnimals();


                  // Allergy Meals

                  const $allergyM = document.querySelector("#allergy_m");

                  function allergyMeals() {
                    const allergyM = $allergyM.selectedIndex;
                    if (allergyM === -1) return; // Esto es cuando no hay elementos
                    const optionMallergy = $allergyM.options[allergyM];

                    if (optionMallergy.value == 'yes') {

                      document.getElementById('specify_allergy_m').classList.remove('d-none');

                    } else {
                      document.getElementById('specify_allergy_m').classList.add('d-none');

                      document.getElementById('text__allergy_m').value = '';
                    }

                  };

                  allergyMeals();
                  </script>


                </div>

              </div>

              <div class="information__content information__content-col-8  mt-4">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Current Heal Information -->
                  <div class="form__group col-sm-12 form__group__basic form__group__preference form__group__healt_s"
                    id="group__healt_s">

                    <div class="form__group__icon-input">

                      <label for="healt_s" class="form__label allergies_l">What is your state of health at the
                        moment?</label>

                      <label for="healt_s" class="icon"><i class="icon icon__healt_s"></i></label>

                      <?php if(empty($row['healt_s']) || $row['healt_s'] == 'NULL'){  ?>

                      <textarea class="form__group-input allergies" id="healt_s" rows="3" name="healt_s"
                        placeholder="Write using few words."></textarea>

                      <?php } else{ ?>

                      <textarea class="form__group-input allergies" id="healt_s" rows="3" name="healt_s"
                        placeholder="Write using few words."><?php echo $row['healt_s'] ?></textarea>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Disease -->
                  <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__disease"
                    id="group__disease">

                    <label for="disease" class="allergies_l">Do you suffer from any disease?</label>

                    <div class="check__div">

                      <div class="check__yes">

                        <?php if(empty($row['disease']) || $row['disease'] != 'no'){  ?>

                        <input type="radio" id="yes_disease" class="input_check" checked>

                        <?php }else{ ?>

                        <input type="radio" id="yes_disease" class="input_check">

                        <?php } ?>

                        <label class="label__check" for="yes">Yes</label>
                      </div>

                      <div class="check__no">
                        <?php if(empty($row['disease']) || $row['disease'] != 'no'){  ?>

                        <input type="radio" name="disease" id="no_disease" class="input_check" value="no">

                        <?php }else{ ?>

                        <input type="radio" name="disease" id="no_disease" class="input_check" value="no" checked>

                        <?php } ?>

                        <label class="label__check" for="no">No</label>
                      </div>

                    </div>


                    <div class="textarea__div-desease" id="textarea__div-desease">
                      <label for="text__disease" class="label__specify">Specify</label>

                      <?php if(empty($row['disease']) || $row['disease'] != 'no'){  ?>

                      <textarea name="disease_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__disease" rows="3"
                        placeholder="Write using few words."><?php echo $row['disease'] ?></textarea>

                      <?php }else{ ?>

                      <textarea name="disease_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__disease" rows="3" placeholder="Write using few words."></textarea>

                      <?php } ?>
                    </div>

                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Treatment -->
                  <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__treatment"
                    id="group__treatment">

                    <label for="treatment" class="allergies_l">Are you under treatment or taking any medication?</label>

                    <div class="check__div">

                      <div class="check__yes">

                        <?php if(empty($row['treatment']) || $row['treatment'] != 'no'){  ?>

                        <input type="radio" id="yes_treatment" class="input_check" checked>

                        <?php }else{ ?>

                        <input type="radio" id="yes_treatment" class="input_check">

                        <?php } ?>

                        <label class="label__check" for="yes">Yes</label>
                      </div>

                      <div class="check__no">

                        <?php if(empty($row['treatment']) || $row['treatment'] != 'no'){  ?>

                        <input type="radio" name="treatment" id="no_treatment" class="input_check" value="no">

                        <?php }else{ ?>

                        <input type="radio" name="treatment" id="no_treatment" class="input_check" value="no" checked>

                        <?php } ?>

                        <label class="label__check" for="no">No</label>
                      </div>

                    </div>

                    <div class="textarea__div-desease" id="textarea__div-treatment">
                      <label for="text__treatment" class="label__specify">Specify</label>

                      <?php if(empty($row['treatment']) || $row['treatment'] != 'no'){  ?>

                      <textarea name="treatment_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__treatment" rows="3"
                        placeholder="Write using few words."><?php echo $row['treatment'] ?></textarea>

                      <?php }else{ ?>

                      <textarea name="treatment_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__treatment" rows="3" placeholder="Write using few words."></textarea>

                      <?php } ?>
                    </div>

                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Psychological Treatment -->
                  <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__treatment_p"
                    id="group__treatment_p">

                    <label for="treatment_p" class="allergies_l text-center">Are you or were you undergoing
                      psychological or psychiatric treatment?</label>

                    <div class="check__div">

                      <div class="check__yes">

                        <?php if(empty($row['treatment_p']) || $row['treatment_p'] != 'no'){  ?>

                        <input type="radio" id="yes_treatment_p" class="input_check" checked>

                        <?php }else{ ?>

                        <input type="radio" id="yes_treatment_p" class="input_check">

                        <?php } ?>
                        <label class="label__check" for="yes">Yes</label>
                      </div>

                      <div class="check__no">

                        <?php if(empty($row['treatment_p']) || $row['treatment_p'] != 'no'){  ?>

                        <input type="radio" name="treatment_p" id="no_treatment_p" class="input_check" value="no">

                        <?php }else{ ?>

                        <input type="radio" name="treatment_p" id="no_treatment_p" class="input_check" value="no"
                          checked>

                        <?php } ?>

                        <label class="label__check" for="no">No</label>
                      </div>

                    </div>

                    <div class="textarea__div-desease" id="textarea__div-treatment_p">
                      <label for="text__treatment_p" class="label__specify">Specify</label>

                      <?php if(empty($row['treatment_p']) || $row['treatment_p'] != 'no'){  ?>

                      <textarea name="treatment_p_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__treatment_p" rows="3"
                        placeholder="Write using few words."><?php echo $row['treatment_p'] ?></textarea>

                      <?php }else{ ?>

                      <textarea name="treatment_p_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__treatment_p" rows="3" placeholder="Write using few words."></textarea>

                      <?php } ?>
                    </div>

                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Allergies -->
                  <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__allergies"
                    id="group__allergies">

                    <label for="allergies" class="allergies_l text-center">Do you suffer from any allergies?</label>

                    <div class="check__div">

                      <div class="check__yes">

                        <?php if(empty($row['allergies']) || $row['allergies'] != 'no'){  ?>

                        <input type="radio" id="yes_allergies" class="input_check" checked>

                        <?php }else{ ?>

                        <input type="radio" id="yes_allergies" class="input_check">

                        <?php } ?>
                        <label class="label__check" for="yes">Yes</label>
                      </div>

                      <div class="check__no">

                        <?php if(empty($row['allergies']) || $row['allergies'] != 'no'){  ?>

                        <input type="radio" name="allergies" id="no_allergies" class="input_check" value="no">

                        <?php }else{ ?>

                        <input type="radio" name="allergies" id="no_allergies" class="input_check" value="no" checked>

                        <?php } ?>

                        <label class="label__check" for="no">No</label>
                      </div>

                    </div>

                    <div class="textarea__div-desease" id="textarea__div-allergies">
                      <label for="text__allergies" class="label__specify">Specify</label>

                      <?php if(empty($row['allergies']) || $row['allergies'] != 'no'){  ?>

                      <textarea name="allergies_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__allergies" rows="3"
                        placeholder="Write using few words."><?php echo $row['allergies'] ?></textarea>

                      <?php }else{ ?>

                      <textarea name="allergies_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__allergies" rows="3" placeholder="Write using few words."></textarea>

                      <?php } ?>
                    </div>

                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Surgery -->
                  <div class="form__group col-sm-6 form__group__basic form__group__preference form__group__surgery mb-0"
                    id="group__surgery">

                    <label for="surgery" class="allergies_l text-center">Have you had any surgery in the 12 months prior
                      to your trip?</label>

                    <div class="check__div">

                      <div class="check__yes">

                        <?php if(empty($row['surgery']) || $row['surgery'] != 'no'){  ?>

                        <input type="radio" id="yes_surgery" class="input_check" checked>

                        <?php }else{ ?>

                        <input type="radio" id="yes_surgery" class="input_check">

                        <?php } ?>
                        <label class="label__check" for="yes">Yes</label>
                      </div>

                      <div class="check__no">

                        <?php if(empty($row['surgery']) || $row['surgery'] != 'no'){  ?>

                        <input type="radio" name="surgery" id="no_surgery" class="input_check" value="no">

                        <?php }else{ ?>

                        <input type="radio" name="surgery" id="no_surgery" class="input_check" value="no" checked>

                        <?php } ?>
                        <label class="label__check" for="no">No</label>
                      </div>

                    </div>

                    <div class="textarea__div-desease" id="textarea__div-surgery">
                      <label for="text__surgery" class="label__specify">Specify</label>

                      <?php if(empty($row['surgery']) || $row['surgery'] != 'no'){  ?>

                      <textarea name="surgery_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__surgery" rows="3"
                        placeholder="Write using few words."><?php echo $row['surgery'] ?></textarea>

                      <?php }else{ ?>

                      <textarea name="surgery_o" class="col-sm-11 ml-auto mr-auto form__group-input healt"
                        id="text__surgery" rows="3" placeholder="Write using few words."></textarea>

                      <?php } ?>
                    </div>

                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <script>
                  // Disease

                  var yes_d = document.querySelector('#yes_disease');
                  var no_d = document.querySelector('#no_disease');

                  yes_d.addEventListener("click", () => {

                    no_d.checked = false;
                    yes_d.checked = true;

                    document.getElementById('textarea__div-desease').classList.add('textarea__div-desease-active');

                  });


                  no_d.addEventListener("click", () => {

                    no_d.checked = true;
                    yes_d.checked = false;

                    document.getElementById('textarea__div-desease').classList.remove(
                      'textarea__div-desease-active');

                    document.getElementById('text__disease').value = '';

                  });

                  if (yes_d.checked) {
                    document.getElementById('textarea__div-desease').classList.add('textarea__div-desease-active');
                  }



                  // Treatment

                  var yes_t = document.querySelector('#yes_treatment');
                  var no_t = document.querySelector('#no_treatment');

                  yes_t.addEventListener("click", () => {

                    no_t.checked = false;
                    yes_t.checked = true;

                    document.getElementById('textarea__div-treatment').classList.add(
                    'textarea__div-desease-active');

                  });

                  no_t.addEventListener("click", () => {

                    no_t.checked = true;
                    yes_t.checked = false;

                    document.getElementById('textarea__div-treatment').classList.remove(
                      'textarea__div-desease-active');

                    document.getElementById('text__treatment').value = '';

                  });

                  if (yes_t.checked) {
                    document.getElementById('textarea__div-treatment').classList.add('textarea__div-desease-active');
                  }


                  // Psychological Treatment

                  var yes_tp = document.querySelector('#yes_treatment_p');
                  var no_tp = document.querySelector('#no_treatment_p');

                  yes_tp.addEventListener("click", () => {

                    no_tp.checked = false;
                    yes_tp.checked = true;

                    document.getElementById('textarea__div-treatment_p').classList.add(
                      'textarea__div-desease-active');

                  });

                  no_tp.addEventListener("click", () => {

                    no_tp.checked = true;
                    yes_tp.checked = false;

                    document.getElementById('textarea__div-treatment_p').classList.remove(
                      'textarea__div-desease-active');

                    document.getElementById('text__treatment_p').value = '';

                  });

                  if (yes_tp.checked) {
                    document.getElementById('textarea__div-treatment_p').classList.add('textarea__div-desease-active');
                  }

                  // Allergies

                  var yes_a = document.querySelector('#yes_allergies');
                  var no_a = document.querySelector('#no_allergies');

                  yes_a.addEventListener("click", () => {

                    no_a.checked = false;
                    yes_a.checked = true;

                    document.getElementById('textarea__div-allergies').classList.add(
                    'textarea__div-desease-active');

                  });

                  no_a.addEventListener("click", () => {

                    no_a.checked = true;
                    yes_a.checked = false;

                    document.getElementById('textarea__div-allergies').classList.remove(
                      'textarea__div-desease-active');

                    document.getElementById('text__allergies').value = '';

                  });

                  if (yes_a.checked) {
                    document.getElementById('textarea__div-allergies').classList.add('textarea__div-desease-active');
                  }

                  // Surgery

                  var yes_s = document.querySelector('#yes_surgery');
                  var no_s = document.querySelector('#no_surgery');

                  yes_s.addEventListener("click", () => {

                    no_s.checked = false;
                    yes_s.checked = true;

                    document.getElementById('textarea__div-surgery').classList.add('textarea__div-desease-active');

                  });

                  no_s.addEventListener("click", () => {

                    no_s.checked = true;
                    yes_s.checked = false;

                    document.getElementById('textarea__div-surgery').classList.remove(
                      'textarea__div-desease-active');

                    document.getElementById('text__surgery').value = '';

                  });

                  if (yes_s.checked) {
                    document.getElementById('textarea__div-surgery').classList.add('textarea__div-desease-active');
                  }
                  </script>
                </div>

              </div>



            </div>


            <div class="card">

              <h3 class="form__group-title__col-4">Professional Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">


                  <!-- Group Academy Preference -->
                  <div class="form__group col-lg-12 form__group__basic form__group__n_a" id="group__n_a">

                    <div class="form__group__icon-input">

                      <label for="n_a" class="form__label">Academy Preference</label>

                      <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                      <select class="custom-select form__group-input n_a" id="n_a" name="n_a">
                        <?php    
                                                            
                                        if(empty($row['n_a']) || $row['n_a'] == 'NULL'){
                                                                
                                            while ( $row6 = $query6->fetch_array() ){
                                    
                                    ?>

                        <?php 
                                                if($row6['name_a'] == 'NULL'){}else{
                                            ?>

                        <option hidden="option" selected disabled>Academy Preference</option>
                        <option value=" <?php echo $row6['id_ac'] ?> ">
                          <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                        </option>

                        <?php }}
                                                                
                                        }else{ 
                                                                    
                                            $query7 = $link->query("SELECT * FROM academy WHERE id_ac = '$row[n_a]'");
                                            $row7=$query7->fetch_assoc(); ?>

                        <option value=" <?php echo $row7['id_ac'] ?> " selected>
                          <?php echo $row7['name_a']; ?><?php echo ', '?><?php echo $row7['dir_a']; ?></option>

                        <?php
                                                while ( $row6 = $query6->fetch_array() ){

                                                    if($row6['name_a'] == 'NULL'){}else{
                                                                
                                            ?>

                        <option value=" <?php echo $row6['id_ac'] ?> ">
                          <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                        </option>

                        <?php }} ?>

                        <?php } ?>

                        <?php
                                                                
                                                        ?>






                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group English Level -->
                  <div class="form__group col-lg-4 form__group__basic form__group__english_l" id="group__english_l">

                    <div class="form__group__icon-input">

                      <label for="english_l" class="form__label smoker">Current English level</label>

                      <label for="english_l" class="icon"><i class="icon icon__city"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="english_l" name="english_l">

                        <?php if(empty($row['english_l']) || $row['english_l'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="Beginner">Beginner</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Advanced">Advanced</option>

                        <?php }elseif($row['english_l'] == 'Beginner'){  ?>

                        <option value="Beginner" selected>Beginner</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Advanced">Advanced</option>

                        <?php }elseif($row['english_l'] == 'Intermediate'){  ?>

                        <option value="Intermediate" selected>Intermediate</option>
                        <option value="Beginner">Beginner</option>
                        <option value="Advanced">Advanced</option>

                        <?php }elseif($row['english_l'] == 'Advanced'){  ?>

                        <option value="Advanced" selected>Advanced</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Beginner">Beginner</option>

                        <?php }  ?>
                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <!-- Group Type Student -->
                  <div class="form__group col-lg-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Type Student</label>

                      <label for="type_s" class="icon"><i class="icon icon__names"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="type_s" name="type_s"
                        onchange="otherProgram()">
                        <?php if(empty($row['type_s']) || $row['type_s'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="High School">High School</option>
                        <option value="English and French">English and French Course</option>
                        <option value="College / University">College / University </option>
                        <option value="Summer Camps">Summer Camps</option>

                        <?php }elseif(empty($row['type_s']) || $row['type_s'] == 'High School'){  ?>

                        <option value="High School" selected>High School</option>
                        <option value="English and French">English and French Course</option>
                        <option value="College / University">College / University </option>
                        <option value="Summer Camps">Summer Camps</option>

                        <?php }elseif(empty($row['type_s']) || $row['type_s'] == 'English and French'){  ?>

                        <option value="English and French" selected>English and French Course</option>
                        <option value="High School">High School</option>
                        <option value="College / University">College / University </option>
                        <option value="Summer Camps">Summer Camps</option>

                        <?php }elseif(empty($row['type_s']) || $row['type_s'] == 'College / University'){  ?>

                        <option value="College / University" selected>College / University </option>
                        <option value="English and French">English and French Course</option>
                        <option value="High School">High School</option>
                        <option value="Summer Camps">Summer Camps</option>

                        <?php }elseif(empty($row['type_s']) || $row['type_s'] == 'Summer Camps'){  ?>

                        <option value="Summer Camps" selected>Summer Camps</option>
                        <option value="College / University">College / University </option>
                        <option value="English and French">English and French Course</option>
                        <option value="High School">High School</option>

                        <?php }  ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <?php 
                            if($row['type_s'] == 'NULL' || $row['type_s'] == 'High School' || $row['type_s'] == 'English and French' || $row['prog_selec'] == "NULL"){ ?>

                  <!-- Group Program to Study in Canada -->
                  <div class="form__group col-lg-4 form__group__basic form__group__prog_selec d-none"
                    id="group__prog_selec">

                    <div class="form__group__icon-input">

                      <label for="prog_selec" class="form__label">Program to Study </label>

                      <label for="prog_selec" class="icon"><i class="icon icon__prog_selec"></i></label>

                      <?php if(empty($row['prog_selec']) || $row['prog_selec'] == 'NULL'){  ?>

                      <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s"
                        placeholder="e.g. Business">

                      <?php }else{ ?>

                      <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s"
                        placeholder="e.g. Business" value="<?php echo $row['prog_selec'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>


                  </div>


                  <?php }else{ ?>

                  <!-- Group Program to Study in Canada -->
                  <div class="form__group col-lg-4 form__group__basic form__group__prog_selec" id="group__prog_selec">

                    <div class="form__group__icon-input">

                      <label for="prog_selec" class="form__label">Program to Study </label>

                      <label for="prog_selec" class="icon"><i class="icon icon__prog_selec"></i></label>

                      <?php if(empty($row['prog_selec']) || $row['prog_selec'] == 'NULL'){  ?>

                      <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s"
                        placeholder="e.g. Business">

                      <?php }else{ ?>

                      <input type="text" id="prog_selec" name="prog_selec" class="form__group-input type_s"
                        placeholder="e.g. Business" value="<?php echo $row['prog_selec'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                  <?php } ?>


                  <script>
                  const $program = document.querySelector("#type_s");

                  function otherProgram() {
                    const programI = $program.selectedIndex;
                    if (programI === -1) return; // Esto es cuando no hay elementos
                    const optionProgram = $program.options[programI];

                    if (optionProgram.value == 'College / University' || optionProgram.value == 'Summer Camps') {

                      document.getElementById('group__prog_selec').classList.remove('d-none');

                    } else {
                      document.getElementById('group__prog_selec').classList.add('d-none');
                    }

                  };

                  otherProgram();
                  </script>




                  <!-- Group Schedule  -->
                  <div class="form__group col-lg-4 form__group__basic form__group__schedule" id="group__schedule">

                    <div class="form__group__icon-input">

                      <label for="schedule" class="form__label smoker"> Schedule </label>

                      <label for="schedule" class="icon"><i class="icon icon__city"></i></label>

                      <select class="custom-select form__group-input smoke_s" id="schedule" name="schedule">

                        <?php if(empty($row['schedule']) || $row['schedule'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="Day">Day</option>
                        <option value="Evening">Evening</option>

                        <?php }elseif(empty($row['schedule']) || $row['schedule'] == 'Day'){  ?>

                        <option value="Day" selected>Day</option>
                        <option value="Evening">Evening</option>

                        <?php }elseif(empty($row['schedule']) || $row['schedule'] == 'Evening'){  ?>

                        <option value="Evening" selected>Evening</option>
                        <option value="Day">Day</option>

                        <?php }  ?>
                      </select>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                </div>

              </div>

            </div>


            <div class="card">

              <h3 class="form__group-title__col-4">House Preferences</h3>

              <h3 class="subtitle__section-card mb-4">Can you Share With:</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Living with Smoker -->
                  <div class="form__group col-lg-4 form__group__basic form__group__smoker_l" id="group__smoker_l">

                    <div class="form__group__icon-input">

                      <label for="smoker_l" class="form__label pets_l">Smokers?</label>

                      <label for="smoker_l" class="icon"><i class="icon icon__smoke_s"></i></label>

                      <select class="custom-select form__group-input pets" id="smoker_l" name="smoker_l">
                        <?php if(empty($row['smoker_l']) || $row['smoker_l'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['smoker_l'] != 'no'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['smoker_l'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Can you live with Children? -->
                  <div class="form__group col-lg-4 form__group__basic form__group__children" id="group__children">

                    <div class="form__group__icon-input">

                      <label for="children" class="form__label pets_l">Children?</label>

                      <label for="children" class="icon"><i class="icon icon__children"></i></label>

                      <select class="custom-select form__group-input pets" id="children" name="children">
                        <?php if(empty($row['children']) || $row['children'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['children'] != 'no'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['children'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Can you live with Teenagers? -->
                  <div class="form__group col-lg-4 form__group__basic form__group__teenagers" id="group__teenagers">

                    <div class="form__group__icon-input">

                      <label for="teenagers" class="form__label pets_l">Teenagers?</label>

                      <label for="teenagers" class="icon"><i class="icon icon__children"></i></label>

                      <select class="custom-select form__group-input pets" id="teenagers" name="teenagers">
                        <?php if(empty($row['teenagers']) || $row['teenagers'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['teenagers'] != 'no'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['teenagers'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Pets -->
                  <div class="form__group col-lg-5 form__group__basic form__group__pets" id="group__pets">

                    <div class="form__group__icon-input">

                      <label for="pets" class="form__label pets_l">Pets?</label>

                      <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                      <select class="custom-select form__group-input pets" id="pets" name="pets">
                        <?php if(empty($row['pets']) || $row['pets'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['pets'] != 'No'){  ?>

                        <option value="Yes" selected>Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['pets'] == 'No'){  ?>

                        <option value="No" selected>No</option>
                        <option value="Yes">Yes</option>

                        <?php } ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>





                </div>



              </div>


              <h3 class="subtitle__section-card mb-4">Accommodation</p>
              </h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group End Date of stay -->
                  <div class="form__group col-lg-4 form__group__lodging_type" id="group__lodging_type">
                    <div class="form__group__icon-input">

                      <label for="lodging_type" class="form__label">Type of Accommodation</label>
                      <label for="lodging_type" class="icon"><i class="icon icon__lodging"></i></label>

                      <select class="custom-select form__group-input gender" id="lodging_type" name="lodging_type"
                        onchange="mostrar()">

                        <?php if(empty($row['lodging_type']) || $row['lodging_type'] == 'NULL'){  ?>

                        <option hidden="option" selected disabled> Select Option</option>
                        <option value="Single">Single</option>
                        <option value="Share">Share</option>
                        <option value="Executive">Executive</option>

                        <?php }elseif($row['lodging_type'] == 'Single'){  ?>

                        <option value="Single" selected>Single</option>
                        <option value="Share">Share</option>
                        <option value="Executive">Executive</option>

                        <?php }elseif($row['lodging_type'] == 'Share'){  ?>

                        <option value="Share" selected>Share</option>
                        <option value="Single">Single</option>
                        <option value="Executive">Executive</option>

                        <?php }elseif($row['lodging_type'] == 'Executive'){  ?>

                        <option value="Executive" selected>Executive</option>
                        <option value="Single">Single</option>
                        <option value="Share">Share</option>

                        <?php }  ?>
                      </select>



                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Food -->
                  <div class="form__group col-lg-4 form__group__basic form__group__food " id="group__food">

                    <div class="form__group__icon-input">

                      <label for="pets" class="form__label pets_l">Meal Plan</label>

                      <label for="food" class="icon"><i class="icon icon__food"></i></label>

                      <select class="custom-select form__group-input food" id="food" name="food" onchange="mostrar()">
                        <?php if(empty($row['food']) || $row['food'] == 'NULL'){  ?>
                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['food'] == 'Yes'){  ?>

                        <option value="Yes">Yes</option>
                        <option value="No">No</option>

                        <?php }elseif($row['food'] == 'No'){  ?>

                        <option value="Yes">Yes</option>
                        <option value="No">No</option>

                        <?php } ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>


                  <!-- Group Food -->
                  <div class="form__group col-lg-4 form__group__basic form__group__diet d-none" id="group__diet">

                    <div class="form__group__icon-input">

                      <label for="diet" class="form__label pets_l">Special Diet</label>

                      <label for="diet" class="icon"><i class="icon icon__food"></i></label>

                      <select class="custom-select form__group-input food" id="diet" name="diet" onchange="mostrar()">

                        <?php if($row['vegetarians'] == 'yes'){  ?>

                        <option value="Vegetarians" selected>Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="Pork">No Pork</option>
                        <option value="None">None</option>

                        <?php }elseif($row['halal'] == 'yes'){  ?>

                        <option value="Halal" selected>Halal (Muslims)</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="Pork">No Pork</option>
                        <option value="None">None</option>

                        <?php }elseif($row['kosher'] == 'yes'){  ?>

                        <option value="Kosher" selected>Kosher (Jews)</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="Pork">No Pork</option>
                        <option value="None">None</option>

                        <?php }elseif($row['lactose'] == 'yes'){  ?>

                        <option value="Lactose" selected>Lactose Intolerant</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="Pork">No Pork</option>
                        <option value="None">None</option>

                        <?php }elseif($row['gluten'] == 'yes'){  ?>

                        <option value="Gluten" selected>Gluten Free Diet</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Pork">No Pork</option>
                        <option value="None">None</option>

                        <?php }elseif($row['pork'] == 'yes'){  ?>

                        <option value="Pork" selected>No Pork</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="None">None</option>

                        <?php }elseif($row['none'] == 'yes'){  ?>

                        <option value="None" selected>None</option>
                        <option value="Vegetarians">Vegetarians</option>
                        <option value="Halal">Halal (Muslims)</option>
                        <option value="Kosher">Kosher (Jews)</option>
                        <option value="Lactose">Lactose Intolerant</option>
                        <option value="Gluten">Gluten Free Diet</option>
                        <option value="Pork">No Pork</option>

                        <?php }  ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                </div>

                <p class="text_diet d-none" id="text_meal">* The meal plan will cost an additional $35 CAD each week *
                </p>
                <p class="text_diet d-none" id="text_diet">* $15 CAD will be charged each day when special diet is
                  required *</p>

              </div>

              <!-- Actual Price -->

              <div class="div__price">

                <div class="price__div col-md-3">

                  <label for="total" class="label__price">Actual Weekly Price</label>

                  <input type="text" id="total_weckly" value="0" readonly class="input__pricet" name="a_price">

                </div>

              </div>


              <h3 class="subtitle__section-card mb-4">Transport <p class="additional__text"> (Each service has a cost of
                  $120 CAD)</p>
              </h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Pick-Up Service -->
                  <div class="form__group col-sm-5 form__group__basic form__group__pick_up" id="group__pick_up">

                    <div class="form__group__icon-input">

                      <label for="pick_up" class="form__label pets_l">Pick Up Service</label>

                      <label for="pick_up" class="icon"><i class="icon icon__city"></i></label>

                      <select class="custom-select form__group-input pets" id="pick_up" name="pick_up"
                        onchange="mostrar()">
                        <?php if(empty($row['pick_up']) || $row['pick_up'] == 'no'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['pick_up'] == 'yes'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif($row['pick_up'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php }  ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                  <!-- Group Drop Off Service -->
                  <div class="form__group col-sm-5 form__group__basic form__group__drop_off" id="group__drop_off">

                    <div class="form__group__icon-input">

                      <label for="drop_off" class="form__label pets_l">Drop off service</label>

                      <label for="drop_off" class="icon"><i class="icon icon__city"></i></label>

                      <select class="custom-select form__group-input pets" id="drop_off" name="drop_off"
                        onchange="mostrar()">
                        <?php if(empty($row['drop_off']) || $row['drop_off'] == 'no'){  ?>

                        <option hidden="option" selected disabled>Select Option</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>

                        <?php }elseif(empty($row['drop_off']) || $row['drop_off'] == 'yes'){  ?>

                        <option value="yes" selected>Yes</option>
                        <option value="no">No</option>

                        <?php }elseif(empty($row['drop_off']) || $row['drop_off'] == 'no'){  ?>

                        <option value="no" selected>No</option>
                        <option value="yes">Yes</option>

                        <?php }  ?>
                      </select>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                </div>

              </div>

              <!-- Actual Price -->

              <div class="div__price">

                <div class="price__div col-md-3">

                  <label for="total" class="label__price">Total Transport Services</label>

                  <input type="text" id="total" value="0" readonly class="input__pricet" name="a_price">

                </div>

              </div>




              <script>
              const $select = document.querySelector("#lodging_type");

              const $specialD = document.querySelector("#food");

              const $diet = document.querySelector("#diet");

              const $pick_up = document.querySelector("#pick_up");

              const $drop_off = document.querySelector("#drop_off");

              var total = document.getElementById('total').value;

              function mostrar() {


                var accommType = '0';

                var mealAdd = '0';

                var dietS = '0';

                var pickUp = '0';

                var dropOff = '0';

                // Accommodation

                const indice = $select.selectedIndex;
                if (indice === -1) return; // Esto es cuando no hay elementos
                const opcionSeleccionada = $select.options[indice];


                if (opcionSeleccionada.value == 'Share') {

                  accommType = '200';

                } else if (opcionSeleccionada.value == 'Single') {

                  accommType = '260';


                } else if (opcionSeleccionada.value == 'Executive') {

                  accommType = '335';
                } else {
                  accommType = '0';
                }


                // Special Diets

                const diet = $diet.selectedIndex;
                if (diet === -1) return; // Esto es cuando no hay elementos
                const opcionDiet = $diet.options[diet];




                if (opcionDiet.value == 'Vegetarians') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'Halal') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'Kosher') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'Lactose') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'Gluten') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'Pork') {

                  dietS = '15' * 7;

                } else if (opcionDiet.value == 'None') {

                  dietS = '0';


                }


                // Meal Plan

                const specialD = $specialD.selectedIndex;
                if (specialD === -1) return; // Esto es cuando no hay elementos
                const optionSdiet = $specialD.options[specialD];



                if (optionSdiet.value == 'Yes') {

                  mealAdd = '35';

                  document.getElementById('group__diet').classList.remove('d-none');
                  document.getElementById('text_diet').classList.remove('d-none');
                  document.getElementById('text_meal').classList.remove('d-none');


                } else {

                  mealAdd = '0';

                  document.getElementById('total').value = mealAdd;

                  document.getElementById('group__diet').classList.add('d-none');
                  document.getElementById('text_diet').classList.add('d-none');
                  document.getElementById('text_meal').classList.add('d-none');

                }


                // Pick Up

                const pick_up = $pick_up.selectedIndex;
                if (pick_up === -1) return; // Esto es cuando no hay elementos
                const optionPickUp = $pick_up.options[pick_up];



                if (optionPickUp.value == 'yes') {

                  pickUp = '120';


                } else {

                  pickUp = '0';

                  document.getElementById('total').value = pickUp;

                }


                // Drop Off

                const drop_off = $drop_off.selectedIndex;
                if (drop_off === -1) return; // Esto es cuando no hay elementos
                const optionDropOff = $drop_off.options[drop_off];



                if (optionDropOff.value == 'yes') {

                  dropOff = '120';


                } else {

                  dropOff = '0';

                  document.getElementById('total').value = dropOff;

                }



                // Actual Price Weckly

                if (accommType == '0') {

                  var wTotal = parseFloat(mealAdd) + parseFloat(dietS);

                } else if (mealAdd == '0') {

                  var wTotal = parseFloat(accommType) + parseFloat(dietS);

                } else if (dietS == '0') {

                  var wTotal = parseFloat(accommType) + parseFloat(mealAdd);

                } else if (mealAdd == '0' && dietS == '0') {

                  var wTotal = accommType;

                } else if (accommType == '0' && dietS == '0') {

                  var wTotal = parseFloat(mealAdd);

                } else if (accommType == '0' && mealAdd == '0') {

                  var wTotal = parseFloat(dietS);

                } else {

                  var wTotal = parseFloat(accommType) + parseFloat(mealAdd) + parseFloat(dietS);

                }

                document.getElementById('total_weckly').value = "$" + wTotal + " CAD";










                // Total Transport Services

                if (dropOff == '0') {

                  var tTotal = parseFloat(pickUp);

                } else if (pickUp == '0') {

                  var tTotal = parseFloat(dropOff);

                } else {

                  var tTotal = parseFloat(pickUp) + parseFloat(dropOff);

                }

                document.getElementById('total').value = "$" + tTotal + " CAD";




              };

              mostrar();
              </script>


            </div>

          </div>

          <div class="card col-md-9 ml-auto mr-auto p-0">

            <h3 class="form__group-title__col-4"> Attached Files </h3>

            <div class="information__content information__content-col-8">

              <div class="row justify-content-center suboption">

                <div class="col-md-5">

                  <h3 class="subtitle__section-card mb-4">Passport Photo</h3>

                  <div class="form__group form__group__basic form__group__visa" id="group__visa">

                    <div class="form__group__div__photo__visa">

                      <?php if(empty($row['pass_photo']) || $row['pass_photo'] == 'NULL'){ ?>
                      <iframe class="form__group__photo__visa d-none" id="preview-ii" src="" id="visa"></iframe>

                      <input type="file" accept="image/*" onchange="previewImageII();" style="display:none"
                        name="passport" id="file-ii" maxlength="1" accept="jpg|png|jpeg|pdf">


                      <label for="file-ii" class="visa-add" id="label-iii"> <label for="file-ii"
                          class="icon icon__file"></label>
                        <p class="form__group-l_title_files"> Upload Files </p>
                      </label>

                      <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i" style="display: none;"
                        title="Change File"></label>


                      <?php }else{ ?>

                      <iframe class="form__group__photo__visa" id="ifra__pass"
                        src="../../<?php echo $row['pass_photo'] ?>" id="Passport"></iframe>

                      <img src="" alt="" class="form__group__photo__visa d-none" id="preview-ii">

                      <input type="file" accept="image/*" onchange="previewImageII();" style="display:none"
                        name="passport" id="file-ii" maxlength="1" accept="jpg|png|jpeg|pdf">

                      <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-iii-i"
                        title="Change Frontage Photo"></label>


                      <?php } ?>
                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>

                  </div>

                </div>

                <div class="col-md-1"></div>

                <div class="col-md-5">

                  <h3 class="subtitle__section-card mb-4">Visa Photo</h3>

                  <div class="form__group form__group__basic form__group__visa" id="group__visa">

                    <div class="form__group__div__photo__visa">

                      <?php if(empty($row['visa']) || $row['visa'] == 'NULL'){ ?>

                      <iframe class="form__group__photo__visa d-none" id="preview-i" src="" id="visa"></iframe>

                      <input type="file" accept="image/*" onchange="previewImageI();" style="display:none" name="visa"
                        id="file-i" maxlength="1" accept="jpg|png|jpeg|pdf">


                      <label for="file-i" class="visa-add" id="label-ii"> <label for="file-ii"
                          class="icon icon__file"></label>
                        <p class="form__group-l_title_files"> Upload Files </p>
                      </label>

                      <label for="file-i" class="add-photo-i fa fa-pencil-alt" id="label-ii-i" style="display: none;"
                        title="Change Frontage Photo"></label>

                      <?php }else{ ?>

                      <iframe class="form__group__photo__visa" id="ifra__pass-i" src="../../<?php echo $row['visa'] ?>"
                        id="visa"></iframe>

                      <img src="" alt="" class="form__group__photo__visa d-none" id="preview-i">

                      <input type="file" accept="image/*" onchange="previewImageI();" style="display:none" name="visa"
                        id="file-i" maxlength="1" accept="jpg|png|jpeg|pdf">

                      <label for="file-i" class="add-photo-i fa fa-pencil-alt" id="label-ii-i"
                        title="Change Frontage Photo"></label>


                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter a valid Date</p>
                  </div>

                </div>

              </div>


              <div class="row justify-content-center suboption">

                <div>

                  <h3 class="subtitle__section-card mb-4">Sign here</h3>
                  <canvas id="canvas" class="div__sign"></canvas>

                  <div class="btn__div">
                    <button type="button" id="btnLimpiar" class="btn btn_clean">Clear</button>
                    <button class="d-none" type="button" id="btnDescargar">Descargar</button>
                    <button class="d-none" id="btnGenerarDocumento">Pasar a documento</button>
                  </div>
                  <br>

                </div>

              </div>

              <script>
              const $canvas = document.querySelector("#canvas"),
                $btnDescargar = document.querySelector("#btnDescargar"),
                $btnLimpiar = document.querySelector("#btnLimpiar"),
                $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

              const contexto = $canvas.getContext("2d");
              const COLOR_PINCEL = 'black';
              const COLOR_FONDO = 'white';
              const GROSOR = 2;
              let xAnterior = 0,
                yAnterior = 0,
                xActual = 0,
                yActual = 0;
              const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
              const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
              let haComenzadoDibujo = false;

              // Clean


              const limpiarCanvas = () => {
                // Colocar color blanco en fondo de canvas
                contexto.fillStyle = COLOR_FONDO;
                contexto.fillRect(0, 0, $canvas.width, $canvas.height);
              };
              limpiarCanvas();
              $btnLimpiar.onclick = limpiarCanvas;



              // Download

              // Escuchar clic del botón para descargar el canvas
              $btnDescargar.onclick = () => {
                const enlace = document.createElement('a');
                // El título
                enlace.download = "Firma.png";
                // Convertir la imagen a Base64 y ponerlo en el enlace
                enlace.href = $canvas.toDataURL();
                // Hacer click en él
                enlace.click();
              };

              // Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
              $canvas.addEventListener("mousedown", evento => {
                // En este evento solo se ha iniciado el clic, así que dibujamos un punto
                xAnterior = xActual;
                yAnterior = yActual;
                xActual = obtenerXReal(evento.clientX);
                yActual = obtenerYReal(evento.clientY);
                contexto.beginPath();
                contexto.fillStyle = COLOR_PINCEL;
                contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
                contexto.closePath();
                // Y establecemos la bandera
                haComenzadoDibujo = true;
              });

              $canvas.addEventListener("mousemove", (evento) => {
                if (!haComenzadoDibujo) {
                  return;
                }
                // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

                xAnterior = xActual;
                yAnterior = yActual;
                xActual = obtenerXReal(evento.clientX);
                yActual = obtenerYReal(evento.clientY);
                contexto.beginPath();
                contexto.moveTo(xAnterior, yAnterior);
                contexto.lineTo(xActual, yActual);
                contexto.strokeStyle = COLOR_PINCEL;
                contexto.lineWidth = GROSOR;
                contexto.stroke();
                contexto.closePath();
              });
              ["mouseup", "mouseout"].forEach(nombreDeEvento => {
                $canvas.addEventListener(nombreDeEvento, () => {
                  haComenzadoDibujo = false;
                });
              });
              </script>

            </div>

          </div>

        </div><br>

        <div class="form__group__btn-send mb-5">
          <button type="submit" name="confirm" class="form__btn confirm_changes"><i
              class="fa fa-save ts-opacity__50 mr-2"></i>Confirm Information</button>
          <button type="submit" name="update" class="form__btn"><i class="fa fa-save ts-opacity__50 mr-2"></i>Save
            Changes</button>
          <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>
        </div>




      </form>

      <hr>

      <!--DELETE SECTION-->
      <div class="panel-group">

        <div class="panel panel-default">
          <div class="panel-heading">

            <h4 class="panel-title">
              <a data-toggle="collapse" href="#collapse1"><i class="fa fa-cog" id="options"></i> Advance Options</a>
            </h4>
          </div>

          <div id="collapse1" class="panel-collapse collapse">
            <div class=" row text-center">

              <div class="col-sm-4">

                <a data-toggle="collapse" href="#disable1" class="btn btn-info"
                  style="background-color: #982a72; color: #fff; border: 1px solid #982a72" id="active"><i
                    class="fa fa-user-times"></i> Disable Student</a>

                <div id="disable1" class="collapse">
                  <br>

                  <h3>Disable Student</h3>

                  <p>The student will be disable immediately.<br />

                    This action will <b>disable the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its
                    files, information and events.<br />

                    <b>If you wish disable this user account press the button?</b>
                  </p>
                  <a data-toggle="modal" class="btn btn-info" href="#myModal2" id="active"
                    style="background-color:#982a72; color: #fff; border: 1px solid #982a72"><i class="fa fa-user-times"
                      id="options"></i> Disable Student</a>


                </div>
              </div>

              <div class="col-sm-4">

                <a data-toggle="collapse" href="#activate" class="btn btn-info"
                  style="background-color:#232159; color: #fff; border: 1px solid #232159" id="active"><i
                    class="fa fa-user-plus"></i> Activate Student</a>

                <div id="activate" class="collapse">
                  <br>

                  <h3>Activate Student</h3>
                  <p>The student will be reactivate immediately.<br />

                    This action will <b>reactivate the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including
                    its files, information and events.<br />

                    <b>If you wish reactivate this user account press the button?</b>
                  </p>

                  <a data-toggle="modal" class="btn btn-info" href="#myModal22" id="active"
                    style="background-color:#232159; color: #fff; border: 1px solid #232159"><i class="fa fa-user-plus"
                      id="options"></i> Reactivate Student</a>


                  <br>
                </div>
              </div>

              <div class="col-sm-4">

                <a data-toggle="collapse" href="#delete1" class="btn btn-info"
                  style="background-color:#4f177d; color: #fff; border: 1px solid #4f177d" id="active"><i
                    class="fa fa-eraser"></i> Delete Student</a>



                <div id="delete1" class="collapse">
                  <!-- Trigger the modal with a button -->
                  <br>
                  <!-- Trigger the modal with a button -->

                  <h3 id="delete">Delete Student</h3>
                  <p>The Student will be permanently deleted immediately.<br />

                    This action will <b>permanently delete</b> <?php echo "".$row['mail_s']."";?> <b>immediately</b>,
                    including its files, information and events.<br />

                    <b>Are you ABSOLUTELY SURE you wish to delete this student?</b>
                  </p>

                  <a data-toggle="modal" class="btn btn-info" href="#myModal" id="active"
                    style="background-color:#4f177d; color: #fff; border: 1px solid #4f177d"><i class="fa fa-eraser"
                      id="options"></i> Delete Student</a>

                  <br>
                </div>
              </div>
            </div>
            <div>
            </div><br>
          </div>
          <!--END DELETE SECTION-->







          </section>

          </form>
          <!--end #form-submit-->

        </div>
        <!--end offset-lg-1 col-lg-10-->
      </div>
      <!--end row-->
  </div>
  <!--end container-->
  </section>

  </main>
  <!--end #ts-main-->

  <!-- Modal Disable Student -->
  <div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Disable Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body" id="background">
          <div class="modal-body" id="body">
            <p>
              <i class="fa fa-asterisk" id="options"></i> You are about to disable this student<br />
              Once a student is disable the user will not login in homebor until his user has been reactivated again.
              its files, all information and events will be not removing of the data.
            </p>
          </div>
        </div>

        <div class="modal-body" id="background">
          <p><b>All Fields Required</b></p>
          <p>Please type the following to confirm:<br>

            <?php echo "<b>".$row['id_student']."</b>";?></p>
          <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post"
            enctype="multipart/form-data">

            <div class="row">

              <!--House Name-->
              <div class="col-sm-8">
                <div class="form-group">

                  <input type="text" class="form-control" id="del_id" name="del_id" required>

                  <input type="text" class="form-control" id="user" name="user" required
                    value="<?php echo "$usuario";?>" hidden>

                  <input type="text" class="form-control" id="id_s" name="id_s" required
                    value="<?php echo "$row[id_student]";?>" hidden>

                  <input type="text" class="form-control" id="name" name="name" required
                    value="<?php echo "$row[mail_s]";?>" hidden>

                  <input type="text" class="form-control" id="id_m" name="id_m" required
                    value="<?php echo "$row7[id_m]";?>" hidden>

                  <label for="des">Reason</label>

                  <textarea class="form-control" id="des" rows="2" name="des" required></textarea>

                </div>
              </div>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
              <button type="submit" id="submit" name="disable" class="btn btn-default">
                <i class="fa fa-user-times"></i> Disable</button>
            </div>
          </form>
        </div>

      </div>

    </div>
  </div>

  <!-- Modal Reactivate Student -->
  <div id="myModal22" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Reactivate Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body" id="background">
          <div class="modal-body" id="body">
            <p> <i class="fa fa-asterisk" id="options"></i> You are about to reactivate this student<br />
              Once a student is reactivate the user will login in homebor.</p>
          </div>
        </div>

        <div class="modal-body" id="background">
          <p><b>All Fields Required</b></p>
          <p>Please type the following to confirm:<br>

            <?php echo "<b>".$row['id_student']."</b>";?></p>

          <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post"
            enctype="multipart/form-data">

            <div class="row">

              <!--House Name-->
              <div class="col-sm-8">
                <div class="form-group">
                  <input type="text" class="form-control" id="del_id" name="del_id" required>

                  <input type="text" class="form-control" id="user" name="user" required
                    value="<?php echo "$usuario";?>" hidden>

                  <input type="text" class="form-control" id="id_s" name="id_s" required
                    value="<?php echo "$row[id_student]";?>" hidden>

                  <input type="text" class="form-control" id="name" name="name" required
                    value="<?php echo "$row[mail_s]";?>" hidden>

                  <input type="text" class="form-control" id="id_m" name="id_m" required
                    value="<?php echo "$row7[id_m]";?>" hidden>

                  <label for="des">Reason</label>

                  <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                </div>
              </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="submit">Close</button>
              <button type="submit" id="active" name="reactivate" class="btn btn-default" id="submit">
                <i class="fa fa-user-plus"></i> Reactivate</button>

            </div>
          </form>
        </div>
      </div>

    </div>
  </div>


  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="delete">Delete Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body" id="background">
          <div class="modal-body" id="body">
            <p><i class="fa fa-asterisk" id="options"></i> You are about to permanently delete this student<br />
              Once a student is permanently deleted it cannot be recovered. Permanently deleting this student will
              immediately delete its files, all information and events.
            </p>
          </div>
        </div>

        <div class="modal-body" id="background">
          <p><b>All Fields Required</b></p>
          <p>This action cannot be undone. You will lose the student's information, events and all files.<br><br>

            Please type the following to confirm:<br>

            <?php echo "<b>".$row['id_student']."</b>";?></p>
          <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post"
            enctype="multipart/form-data">

            <div class="row">

              <!--House Name-->
              <div class="col-sm-8">
                <div class="form-group">

                  <input type="text" class="form-control" id="del_id" name="del_id" required>

                  <input type="text" class="form-control" id="user" name="user" required
                    value="<?php echo "$usuario";?>" hidden>

                  <input type="text" class="form-control" id="id_s" name="id_s" required
                    value="<?php echo "$row[id_student]";?>" hidden>

                  <input type="text" class="form-control" id="name" name="name" required
                    value="<?php echo "$row[mail_s]";?>" hidden>

                  <input type="text" class="form-control" id="id_m" name="id_m" required
                    value="<?php echo "$row7[id_m]";?>" hidden>

                  <label for="des">Reason</label>

                  <textarea class="form-control" id="des" rows="2" name="des" required></textarea>

                </div>
              </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
              <button type="submit" id="submit" name="register" class="btn btn-default">
                <i class="fa fa-eraser"></i> Delete</button>
            </div>

          </form>
        </div>

      </div>

    </div>
  </div>


  </main>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>
  <div id="house_ass" style="display: none;">
    <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-9" id="assh">

        </div>
      </div>
    </div>

  </div>


  <?php include 'footer.php' ?>

  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    }

  }
  </script>

  <script>
  document.getElementById("arrive_g").onchange = function() {
    fields()
  };

  function fields() {

    var arrive = document.getElementById("arrive_g").value;

    document.getElementById("firstd").value = arrive;
    document.getElementById("firstd2").value = arrive;
    document.getElementById("f_date").value = arrive;
    document.getElementById("h_date").value = arrive;

  }

  document.getElementById("firstd").onchange = function() {
    fields2()
  };

  function fields2() {

    var firstd = document.getElementById("firstd").value;

    document.getElementById("f_date").value = firstd;
    document.getElementById("h_date").value = firstd;

  }

  document.getElementById("lastd").onchange = function() {
    calWeek()
  };

  function calWeek() {

    var firstd = document.getElementById("firstd").value;
    var lastd2 = document.getElementById("lastd").value;

    document.getElementById("firstd2").value = firstd;
    document.getElementById("lastd2").value = lastd2;

    //$('#calcWeeks').html(firstd);

    const date1 = new Date(firstd);
    const date2 = new Date(lastd2);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    const diffWeeksD = diffDays / 7;

    if (diffWeeksD % 1 == 0) {

      const diffWeeks = diffDays / 7;

      $('#calcWeeks').html(diffWeeks + " weeks accommodation");

    } else {

      const diffWeeks = Math.floor(diffDays / 7);
      const diffDays2 = Math.floor(diffDays - diffWeeks * 7);

      $('#calcWeeks').html(diffWeeks + " weeks and " + diffDays2 + " days accommodation");

    }

  }

  calWeek();


  /* function myFunction(x) {
  if (x.matches) { // If media query matches
      document.getElementById('div__flex').classList.remove('col-md-4');
      document.getElementById('div__flex').classList.remove('col-md-8');
  } else {}
  }

  var x = window.matchMedia("(max-width: 767px)")
  myFunction(x) // Call listener function at run time
  x.addListener(myFunction) // Attach listener function on state changes 
  */
  </script>

  <!-- Daterangepicker -->

  <script>
  $(function() {

    var arrive_g = document.getElementById('date_arrive_g').value;

    var firstd = document.getElementById('date_firstd').value;

    var lastd = document.getElementById('date_lastd').value;

    var f_date = document.getElementById('date_f_date').value;

    var h_date = document.getElementById('date_h_date').value;

    var db = document.getElementById('date_db').value;

    var exp_pass = document.getElementById('date_exp_pass').value;

    var bl = document.getElementById('date_bl').value;

    $('#arrive_g').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      startDate: arrive_g,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });

    $('#firstd').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      startDate: firstd,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });

    $('#lastd').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      startDate: lastd,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });


    $('#f_date').daterangepicker({
      singleDatePicker: true,
      timePicker: true,
      startDate: f_date,
      endDate: moment().startOf('hour').add(32, 'hour'),
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    });

    $('#h_date').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      startDate: h_date,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });

    $('#db').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      maxDate: moment().startOf('hour'),
      startDate: db,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });

    $('#exp_pass').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: exp_pass,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });

    $('#bl').daterangepicker({
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: bl,
      locale: {
        format: 'MM/DD/YYYY'
      }
    });
  });
  </script>



  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/galery.js"></script>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


</body>

</html>