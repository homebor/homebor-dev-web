<?php
include '../../xeon.php';
error_reporting(0);
session_start();

$usuario = $_SESSION['username'];

//TOTAL OF HOMESTAY
$sql = "SELECT * FROM `pe_home`"; 
 
$connStatus = $link->query($sql); 
 
$numberOfRows = mysqli_num_rows($connStatus); 

//TOTAL OF STUDENTS
$sql2 = "SELECT * FROM `pe_student`"; 
 
$connStatus2 = $link->query($sql2); 
 
$numberOfRows2 = mysqli_num_rows($connStatus2);

$a = 1;

$numberOfRows21 = $numberOfRows2 - $a;

//TOTAL OF Agents
$sql3 = "SELECT * FROM `users` WHERE usert='Admin' OR usert='Cordinator' OR usert='Agent'"; 
 
$connStatus3 = $link->query($sql3); 
 
$numberOfRows3 = mysqli_num_rows($connStatus3); 

//TOTAL OF Academies
$sql4 = "SELECT * FROM `academy`"; 
 
$connStatus4 = $link->query($sql4); 
 
$numberOfRows4 = mysqli_num_rows($connStatus4); 

$numberOfRows41 = $numberOfRows4 - $a;
 

$query="SELECT * FROM manager WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row2=$resultado->fetch_assoc();


//TOTAL OF Academies
$sql5 = "SELECT * FROM `propertie_control` WHERE id_m = '0' AND agency = '$row2[id_m]'"; 
 
$connStatus5 = $link->query($sql5); 
 
$numberOfRows5 = mysqli_num_rows($connStatus5);

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');


$date2 = date('Y-m-d', strtotime($date. ' + 1 month'));
$date3 = date('Y-m-d', strtotime($date. ' + 3 month'));
$date4 = date('Y-m-d', strtotime($date. ' + 6 month'));
$date5 = date('Y-m-d', strtotime($date. ' + 12 month'));


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Manager') {
    header("location: ../logout.php");   
}




?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/work_with_us.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Work With Us</title>

</head>

<body>

  <div class="ts-page-wrapper" id="page-top">

    <!--HEADER ===============================================================-->
    <?php 
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!--************ MAIN ***************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main">

      <!--PAGE TITLE ******************************************************************************************-->
      <section id="page-title">
        <div class="container">
          <div class="ts-title">
            <br>
            <h1>Plans</h1>
          </div>
          <!--end ts-title-->
        </div>
        <!--end container-->
      </section>

      <!--PRICING
            =========================================================================================================-->
      <section id="pricing">
        <div class="container">

          <div class="row no-gutters ts-cards-same-height">

            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
              <div class="card text-center ts-price-box">

                <!--Header-->
                <div class="card-header" data-bg-color="#dadada">
                  <h5 class="text-white" data-bg-color="#232159">Basic</h5>
                  <div class="ts-title">
                    <h3 class="font-weight-normal">TBA</h3>
                  </div>
                </div>

                <!--Body-->
                <div class="card-body p-0 border-bottom-0">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">Register Up To 200 Properties</li>
                    <li class="list-group-item">5 Coordinators Profile</li>
                    <li class="list-group-item">100 Active Students Profiles</li>
                    <li class="list-group-item">20 Properties Certified by Homebor</li>
                  </ul>
                </div>

                <!--Footer-->
                <div class="card-footer bg-transparent pt-0 border-0">
                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2">Select
                    Now</button>
                </div>

              </div>
            </div>
            <!--end price-box-->

            <!--Price Box Promoted-->
            <div class="col-sm-4 col-lg-4">
              <div class="card text-center ts-price-box ts-price-box__promoted">

                <!--Header-->
                <div class="card-header" data-bg-color="#ED84C9">
                  <h5 class="text-white" data-bg-color="#982a72">Premium</h5>
                  <div class="ts-title text-white">
                    <h3 class="font-weight-normal">
                      TBA
                    </h3>
                  </div>
                </div>

                <!--Body-->
                <div class="card-body p-0">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">Register Up To 500 Properties</li>
                    <li class="list-group-item">10 Coordinators Profiles</li>
                    <li class="list-group-item">500 Active Students Profiles</li>
                    <li class="list-group-item">50 Properties Certified by Homebor</li>
                  </ul>
                </div>

                <!--Footer-->
                <div class="card-footer bg-transparent pt-0 border-0">
                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal3">Select
                    Now</button>
                </div>

              </div>
            </div>
            <!--end price-box-->

            <!--Price Box-->
            <div class="col-sm-4 col-lg-4">
              <div class="card text-center ts-price-box">

                <!--Header-->
                <div class="card-header" data-bg-color="#dadada">
                  <h5 class="text-white" data-bg-color="#232159">Professional</h5>
                  <div class="ts-title">
                    <h3 class="font-weight-normal">
                      TBA
                    </h3>
                  </div>
                </div>

                <!--Body-->
                <div class="card-body p-0">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">Register Up To 1000 Properties</li>
                    <li class="list-group-item">100 Coordinators Profile</li>
                    <li class="list-group-item">1000 Active Students Profiles</li>
                    <li class="list-group-item">100 / + Properties Certified by Homebor</li>
                  </ul>
                </div>

                <!--Footer-->
                <div class="card-footer bg-transparent pt-0 border-0">
                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal4">Select
                    Now</button>
                </div>

              </div>
            </div>
            <!--end price-box-->

          </div>
          <!--end row-->
        </div>
        <!--end container-->
      </section>

      <!-- Free Modal -->
      <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Basic Plan</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-7">
                    <H1>Billing Screen</H1>
                    <p><b>All Fields Required</b></p>

                    <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                      enctype="multipart/form-data">
                      <div class="row">

                        <!--House Name-->
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label for="des">Name</label>
                            <input type="text" class="form-control" id="client" name="client" required
                              value="<?php echo "$row2[name] $row2[l_name]";?>">
                            <label for="des">Phone</label>
                            <input type="text" class="form-control" id="num" name="num" required
                              value="<?php echo "$row2[num]";?>">
                            <label for="des">User</label>
                            <input type="text" class="form-control" id="mail" name="mail" required
                              value="<?php echo "$row2[mail]";?>" readonly>
                            <label for="des">Id Accommodation Provider</label>
                            <input type="text" class="form-control" id="id_m" name="id_m" required
                              value="<?php echo "$row2[id_m]";?>" readonly>
                            <label for="des">Date</label>
                            <input type="text" class="form-control" id="db" name="db" required
                              value="<?php echo "$date";?>" readonly>
                            <input type="text" class="form-control" id="plan" name="plan" required value="Free" hidden>
                            <input type="text" class="form-control" id="limit" name="limit" required
                              value="<?php echo "$numberOfRows5";?>" hidden>

                          </div>
                        </div>
                      </div>

                  </div>

                  <div class="col-5">
                    <br>
                    <br><br>
                    <h3><b>Summary</b></h3>
                    <section>
                      <div class="ts-box">

                        <dl class="ts-description-list__line mb-0">

                          <dt>Plan:</dt>
                          <dd>Basic</dd>

                          <dt>Properties:</dt>
                          <dd>200</dd>

                          <dt>Coordinators:</dt>
                          <dd>5</dd>

                          <dt>Students:</dt>
                          <dd>100</dd>

                          <dt>Homebor:</dt>
                          <dd>20</dd>

                          <dt>Price:</dt>
                          <dd class="border-bottom">USD0.00</dd>

                          <dt>Total:</dt>
                          <dd>USD0.00</dd>

                          <dt>Pay:</dt>
                          <dd class="border-bottom">USD0.00</dd>

                        </dl>

                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="submit" name="free" class="btn btn-default">
                <i class="fa fa-credit-card"></i> Buy</button>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>



  <!-- Premium Modal -->
  <!-- 1 MONTH -->
  <div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><b>Premium Plan</b></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse1" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 1 month</th>
                    <th id="price">CAD$ 9,99</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date2";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required value="Premium"
                                  hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Premium</dd>

                              <dt>Properties:</dt>
                              <dd>500</dd>

                              <dt>Coordinators:</dt>
                              <dd>10</dd>

                              <dt>Students:</dt>
                              <dd>500</dd>

                              <dt>Homebor:</dt>
                              <dd>50</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="premium" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- 3 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse2" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 3 month</th>
                    <th id="price">CAD$ 29,97</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date3";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required value="Premium"
                                  hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Premium</dd>

                              <dt>Properties:</dt>
                              <dd>500</dd>

                              <dt>Coordinators:</dt>
                              <dd>10</dd>

                              <dt>Students:</dt>
                              <dd>500</dd>

                              <dt>Homebor:</dt>
                              <dd>50</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="premium" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- 6 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse3" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 6 month</th>
                    <th id="price">CAD$ 59,94</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date4";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required value="Premium"
                                  hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Premium</dd>

                              <dt>Properties:</dt>
                              <dd>500</dd>

                              <dt>Coordinators:</dt>
                              <dd>10</dd>

                              <dt>Students:</dt>
                              <dd>500</dd>

                              <dt>Homebor:</dt>
                              <dd>50</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="premium" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- 12 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse4" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p12"><i class="fa fa-credit-card"></i> 12 month</th>
                    <th id="price12">CAD$ 119,88</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date5";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required value="Premium"
                                  hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Premium</dd>

                              <dt>Properties:</dt>
                              <dd>500</dd>

                              <dt>Coordinators:</dt>
                              <dd>10</dd>

                              <dt>Students:</dt>
                              <dd>500</dd>

                              <dt>Homebor:</dt>
                              <dd>50</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="premium" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!--END-->
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>


  <!-- Professional Modal -->
  <!-- 1 MONTH -->
  <div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Professional Plan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse5" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 1 month</th>
                    <th id="price">CAD$ 19,99</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date2";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required
                                  value="Professional" hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Professional</dd>

                              <dt>Properties:</dt>
                              <dd>1000</dd>

                              <dt>Coordinators:</dt>
                              <dd>100</dd>

                              <dt>Students:</dt>
                              <dd>1000</dd>

                              <dt>Homebor:</dt>
                              <dd>100</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="professional" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- 3 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse6" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 3 month</th>
                    <th id="price">CAD$ 59,97</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date3";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required
                                  value="Professional" hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Professional</dd>

                              <dt>Properties:</dt>
                              <dd>1000</dd>

                              <dt>Coordinators:</dt>
                              <dd>100</dd>

                              <dt>Students:</dt>
                              <dd>1000</dd>

                              <dt>Homebor:</dt>
                              <dd>100</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>
                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="professional" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- 6 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse7" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p"><i class="fa fa-credit-card"></i> 6 month</th>
                    <th id="price">CAD$ 119,88</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date4";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required
                                  value="Professional" hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Professional</dd>

                              <dt>Properties:</dt>
                              <dd>1000</dd>

                              <dt>Coordinators:</dt>
                              <dd>100</dd>

                              <dt>Students:</dt>
                              <dd>1000</dd>

                              <dt>Homebor:</dt>
                              <dd>100</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>
                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="professional" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- 12 month -->
        <div class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <br>
                <a data-toggle="collapse" href="#collapse8" id="price">
                  <table>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="p12"><i class="fa fa-credit-card"></i> 12 month</th>
                    <th id="price12">CAD$ 239,88</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </table>
                </a>
              </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="modal-body">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-7">
                        <H1>Billing Screen</H1>
                        <p><b>All Fields Required</b></p>
                        <form id="form-submit" class="ts-form" autocomplete="off" action="buy_plan.php" method="post"
                          enctype="multipart/form-data">
                          <div class="row">


                            <!--House Name-->
                            <div class="col-sm-8">
                              <div class="form-group">
                                <label for="des">Name</label>
                                <input type="text" class="form-control" id="client" name="client" required
                                  value="<?php echo "$row2[name] $row2[l_name]";?>">
                                <label for="des">Phone</label>
                                <input type="text" class="form-control" id="num" name="num" required
                                  value="<?php echo "$row2[num]";?>">
                                <label for="des">User</label>
                                <input type="text" class="form-control" id="mail" name="mail" required
                                  value="<?php echo "$row2[mail]";?>" readonly>
                                <label for="des">Id Accommodation Provider</label>
                                <input type="text" class="form-control" id="id_m" name="id_m" required
                                  value="<?php echo "$row2[id_m]";?>" readonly>
                                <label for="des">Date</label>
                                <input type="text" class="form-control" id="db" name="db" required
                                  value="<?php echo "$date";?>" readonly>
                                <input type="text" class="form-control" id="exp" name="exp" required
                                  value="<?php echo "$date5";?>" hidden>
                                <label for="des">Bank Name</label>
                                <input type="text" class="form-control" id="bank" name="bank" required required>
                                <label for="des">Proof of Payment</label>
                                <div class="input-group-text">
                                  <input type="file" class="file-upload-input with-preview " name="image[]"
                                    id="profile-img" maxlength="1" accept="jpg|png|jpeg|pdf" required>

                                </div>
                                <input type="text" class="form-control" id="plan" name="plan" required
                                  value="Professional" hidden>
                                <input type="text" class="form-control" id="limit" name="limit" required
                                  value="<?php echo "$numberOfRows5";?>" hidden>

                              </div>
                            </div>
                          </div>

                      </div>

                      <div class="col-5">
                        <br>
                        <br><br>
                        <h3><b>Summary</b></h3>
                        <section>
                          <div class="ts-box">

                            <dl class="ts-description-list__line mb-0">

                              <dt>Plan:</dt>
                              <dd>Professional</dd>

                              <dt>Properties:</dt>
                              <dd>1000</dd>

                              <dt>Coordinators:</dt>
                              <dd>100</dd>

                              <dt>Students:</dt>
                              <dd>1000</dd>

                              <dt>Homebor:</dt>
                              <dd>100</dd>

                              <dt>Price:</dt>
                              <dd class="border-bottom">USD0.00</dd>

                              <dt>Total:</dt>
                              <dd>USD0.00</dd>

                              <dt>Pay:</dt>
                              <dd class="border-bottom">USD0.00</dd>
                            </dl>

                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit" name="professional" class="btn btn-default">
                  <i class="fa fa-credit-card"></i> Buy</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!--END-->
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>


  <!--NUMBERS *********************************************************************************************-->
  <section id="about-us-numbers">
    <div id="numbers" class="py-5 text-white text-center ts-separate-bg-element img-fluid" data-bg-color="#000037"
      data-bg-image="../assets/logos/homebor.png" data-bg-image-opacity=".3">
      <div class="container py-5">
        <div class="ts-promo-numbers">
          <div class="row">

            <div class="col-sm-3">
              <div class="ts-promo-number">
                <h2><?php echo "$numberOfRows"?></h2>
                <h4 class="mb-0 ts-opacity__50">Homestay</h4>
              </div>
              <!--end ts-promo-number-->
            </div>
            <!--end col-sm-3-->

            <div class="col-sm-3">
              <div class="ts-promo-number">
                <h2><?php echo "$numberOfRows3"?></h2>
                <h4 class="mb-0 ts-opacity__50"> Coordinators</h4>
              </div>
              <!--end ts-promo-number-->
            </div>
            <!--end col-sm-3-->

            <div class="col-sm-3">
              <div class="ts-promo-number">
                <h2><?php echo "$numberOfRows21"?></h2>
                <h4 class="mb-0 ts-opacity__50">Students</h4>
              </div>
              <!--end ts-promo-number-->
            </div>
            <!--end col-sm-3-->

            <div class="col-sm-3">
              <div class="ts-promo-number">
                <h2><?php echo "$numberOfRows41"?></h2>
                <h4 class="mb-0 ts-opacity__50">Academies</h4>
              </div>
              <!--end ts-promo-number-->
            </div>
            <!--end col-sm-3-->

          </div>
          <!--end row-->
        </div>
        <!--end ts-promo-numbers-->
      </div>
      <!--end container-->
    </div>
    <!--end #numbers-->

  </section>


  <!--PARTNERS ********************************************************************************************-->
  <section id="partners">
    <div class="ts-block py-4">
      <div class="container">
        <!--block of logos-->
        <div class="d-block d-md-flex justify-content-between align-items-center text-center ts-partners py-3">
          <a href="#">
            <img src="assets/img/logo-01.png" alt="">
          </a>
          <a href="#">
            <img src="assets/img/logo-02.png" alt="">
          </a>
          <a href="#">
            <img src="../assets/logos/page.png" alt="">
          </a>
          <a href="#">
            <img src="assets/img/logo-04.png" alt="">
          </a>
          <a href="#">
            <img src="assets/img/logo-05.png" alt="">
          </a>
        </div>
        <!--end logos-->
      </div>
      <!--end container-->
    </div>
  </section>

  </main>
  <!--end #ts-main-->

  <!--FOOTER ===============================================================-->
  <?php 
    include 'footer.php';
?>

  </div>
  <!--end page-->

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/owl.carousel.min.js"></script>
  <script src="../assets/js/custom.js"></script>

</body>

</html>