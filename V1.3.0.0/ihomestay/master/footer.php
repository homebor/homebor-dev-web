<?php
    require '../xeon.php';
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query12="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado12=$link->query($query12);

    $row12=$resultado12->fetch_assoc();

    if ($row12['usert'] == 'Admin') {
        $way = '../master/index.php';
    } elseif ($row12['usert'] == 'Cordinator') {
       $way = '../master/cordinator.php';
       echo '<style type="text/css"> a#admin{display:none;} </style>';
    }

?>
<footer id="ts-footer">
    <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">

        <!--MAIN FOOTER CONTENT
            =========================================================================================================-->
<section id="ts-footer-main" >
            <div class="container">
                <div class="row">
                    <!--Brand and description-->
                    <div class="col-md-6">
                        <a href="<?php echo $way;?>" class="brand">
                            <img src="../assets/logos/page.png">
                        </a>
                        <p class="mb-4" id="text">
                        We are an accommodation platform that help schools to connects quality homestay families with international students offering them the best experience for everyone involved.
                        </p>     
                    </div>
                    <!--Navigation-->
                    <div id="nav-footer" class="col-md-2">
                        <h4 id="footer">Navigation</h4>
                        <nav class="nav flex-row flex-md-column mb-4">
                            <a href="<?php echo $way;?>" class="nav-link">Main Panel</a>
                            <a href="../master/homestay.php" class="nav-link">Homestay</a>
                            <a href="../master/student.php" class="nav-link">Students</a>
                            <a href="../master/academy.php" class="nav-link">Academy</a>
                            <a id="admin" href="../master/activity.php" class="nav-link">Activity Log</a>
                            <a href="#" class="nav-link">Logout</a>
                        </nav>
                    </div>
                    <!--Contact Info-->
                    <div id="con-footer" class="col-md-4">
                        <h4 id="footer">Contact</h4>
                        <address class="ts-text-color-light">
                            <strong>Email: </strong>
                            <a href="#" class="btn-link">info@homebor.com</a>
                            <br>
                            <strong>Phone:</strong>
                            +1 (407) 485-0374
                        </address>
                    </div>

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-main-->
        <!--SECONDARY FOOTER CONTENT
            =========================================================================================================-->
        <section id="ts-footer-secondary" class="mb-0" data-bg-pattern="../assets/img/bg-pattern-dot.png" style="background-image: url('../assets/img/bg-pattern-dot.png');">
            <div class="container">
                <!--Copyright-->
                <img id="white-logo" src="../assets/logos/white.png">
                <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
                <!--Social Icons-->
                <div class="ts-footer-nav">
                <nav class="nav">
                        <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
                            <i class="fab fa-facebook-f" id="footer"></i>
                        </a>
                        <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
                            <i class="fab fa-twitter" id="footer"></i>
                        </a>
                        <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
                            <i class="fab fa-instagram" id="footer"></i>
                        </a>
                    </nav>
                </div>
                <!--end ts-footer-nav-->

            </div>
            <!--end container-->
        </section>
        <!--end ts-footer-secondary-->

    </footer>
    <!--end #ts-footer-->