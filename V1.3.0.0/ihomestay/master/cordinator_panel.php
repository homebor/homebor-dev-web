<?php
    require '../xeon.php';
    session_start();
    error_reporting(0);

     // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


    //Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

      if ($row['usert'] != 'Admin') {
        
         header("location: ../logout.php");   
        
    } 

    if ($row['usert'] == 'Admin') {
      $way = 'index.php';
  } elseif ($row['usert'] == 'Cordinator') {
     $way = 'cordinator.php';
     echo '<style type="text/css"> a#admin{display:none;} </style>';
  }

?>
<!DOCTYPE>
<html>
  <head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/cordinator_panel.css">

     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Administartor Panel</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
  </head>
 <body id="body">

<!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <main id="ts-main">
      <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                              <br>
                              <br>
                              <br>
                                <br>
                             <h2>Coordinators Administrator Panel </h2>
    <!-- Echo username -->
    <?php
        $usuario = $_SESSION['username'];
        echo "<p>You are logged in as <b>$usuario</b>.</p>"; 
    ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>

        <!--PAGE TITLE
            =========================================================================================================-->


    <div class="container">
      <br />
      <div class="card">
        <div class="card-body">
          <div class="form-group">
            <input type="text" name="search_box" id="search_box" class="form-control" placeholder="Type your search query here">
          </div>
          <div class="table-responsive" id="dynamic_content">
          </div>

            <div id="table-voun" class="table-responsive text-center ">
              <table class="table table-hover" id="vou">
                <thead style="background-color: #232159; color: white">
                  <tr>
                    <th class="text-center align-middle">Photo Agent</th>
                    <th class="text-center align-middle">Agent Name</th>
                    <th class="text-center align-middle">Agency</th>
                    <th class="text-center align-middle">Mail</th>
                    <th class="text-center align-middle">Status</th>
                    <th class="text-center align-middle">Contact</th>
                    <th class="text-center align-middle"></th>
                  </tr>
                </thead>

                <tbody class="table-bordered" id="vouchers"></tbody>

              </table>
            </div>
        </div>
      </div>
    </div>
</section>

</body>
</main>
</body>


<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>

</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="assets/js/cordinator_panel.js"></script>

</body>
</html>
