<?php
    require '../xeon.php';
    include '../cript.php';
    session_start(); 
    error_reporting(0);

    // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    if ($row['usert'] != 'Admin') {
        header("location: ../logout.php");
    }

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/index.css"> 
     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Administartor Panel</title>

</head>
<body>


 <div class="ts-page-wrapper ts-homepage" id="page-top">
   <!--HEADER ===============================================================-->
<?php 
	include 'header.php';
?>

    <main id="ts-main" style="background-color: #fff;">

        <div class="col-xl-12 container justify-content-center" >

            
            <?php echo "<h3 class='text-center'>"."Welcome to Administrator Panel " . $row['name']. " " . $row['l_name']. "</h3>";

            $usuario = $_SESSION['username'];
            echo "<p class='text-center mb-5'>You are logged in as <b>$usuario</b>.</p>"; 
            ?>
            

            <div class="row justify-content-center margin-t">

                <div class="row col-xl-6 justify-content-center margin-b">
                    <div class="row span-circle justify-content-center">
                        <span class="circle" id="circle">
                            <i class="fa fa-check"></i>
                        </span>
                        <h3 style="text-align:right;">Homestays</h3>
                    </div>
                    <div class="flex row">
                        <div class="col-sm-5 mt-auto mb-auto homestay-image mt-2"><a href="homestay.php">
                            <img src="../assets/img/admin.acomo.2.png" class=" img-size" alt="" >
                        </a></div>
                        <div class="col-sm-6 text-center f-left mt-auto mb-auto homestay-options">
                            <ul class="list-group list-group-flush">
                                <a href="homestay.php" class="ref">
                                    <li class="list-group-item">Homestay Administrator Panel</li>
                                </a>
                                <a href="../master/home_register.php" class="ref">
                                    <li class="list-group-item">Register a new Homestay</li>
                                </a>
                                <a href="#" class="ref">
                                    <li class="list-group-item">Homestay Payments</li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                <div class="row col-xl-6 flex justify-content-center margin-b">
                    <div class="row span-circle justify-content-center">
                        <span class="circle" id="circle">
                            <i class="fa fa-check"></i>
                        </span>
                        <h3 style="text-align:right;">Students</h3>
                    </div>
                    <div class="flex row">
                        <div class="col-sm-5 mt-auto mb-auto"><a href="student.php">
                            <img src="../assets/img/admin.student.png" class="img-mirror img-size" alt="">
                        </a></div>
                        <div class="col-sm-6 text-center f-left mt-auto mb-auto">
                            <ul class="list-group list-group-flush">
                                <a href="student.php" class="ref">
                                    <li class="list-group-item">Students Administrator Panel</li>
                                </a>
                                <a href="../master/waiting_confirmation.php" class="ref">
                                    <li class="list-group-item">Awaiting Confirmation</li>
                                </a>
                                <a href="../master/student_register.php" class="ref">
                                    <li class="list-group-item">Register a New Student</li>
                                </a>
                                <a href="#" class="ref">
                                    <li class="list-group-item">Student Payments</li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>



            <div class="mt-5 row justify-content-center">
                <div class="row col-xl-6 flex justify-content-center margin-b">
                    <div class="row span-circle justify-content-center">
                        <span class="circle" id="circle">
                            <i class="fa fa-check"></i>
                        </span>
                        <h3 style="text-align:right;">Accomodation Providers</h3>
                    </div>
                    <div class="flex row">
                        <div class="col-sm-5 mt-auto mb-auto">
                            <img src="../assets/img/admin.acomo.png" class="img-size" alt="">

                            
                        </div>
                        <div class="col-sm-6 text-center f-left mt-auto mb-auto">
                            <ul class="list-group list-group-flush">
                                <a href="../master/agencies.php" class="ref">
                                    <li class="list-group-item">Providers Administrator Panel</li>
                                </a>
                                <a href="../master/agencies_payments.php" class="ref">
                                    <li class="list-group-item">Providers Payments</li>
                                </a>
                                <a href="../master/agencies_voucher.php" class="ref">
                                    <li class="list-group-item">Providers Vouchers</li>
                                </a>
                                <a href="../master/agents_panel.php" class="ref">
                                    <li class="list-group-item">Coordinators</li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <!--Create
        =============================================================================================================-->
        <section id="pricing" class="ts-block pt-0" data-bg-pattern="assets/img/bg-pattern-dot.png">
              <br>
              <br>

        <!--LOGIN / REGISTER SECTION
            =========================================================================================================-->
        <section id="login-register">
            <div class="container">
                <div class="row" >

                    <div class="offset-md-2 col-md-8 offset-lg-3 col-lg-6" >

                        <!--LOGIN / REGISTER TABS
                            =========================================================================================-->
                        <ul class="nav nav-tabs" id="login-register-tabs" role="tablist">

                            <!--Register tab-->
                            <li class="nav-item">
                                <a class="nav-link active" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">
                                    <h3>Create Administrators</h3>
                                </a>
                            </li>

                        </ul>
                            <!--REGISTER TAB
                                =====================================================================================-->
                            <div class="tab-pane active" id="register" role="tabpanel" aria-labelledby="register-tab">

                                <!--Register tab-->
                                    
                                    <form action="" method="post" autocomplete="off">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="name" class="form-control" id="name" placeholder="Name" name="name" autocomplete="off" required>
                                    </div>
                                    
                                    <!-- Las name -->
                                    <div class="form-group">
                                        <input type="l_name" class="form-control" id="l_name" placeholder="Last Name" name="l_name" autocomplete="off" required>
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" autocomplete="off" required>
                                    </div>

                                     <div class="form-group">
                                        <input type="text" class="form-control" id="num" placeholder="Phone Number" name="num" autocomplete="off" required>
                                    </div>

                                    <!--Password-->
                                    <div class="input-group ts-has-password-toggle">
                                        <input type="password" class="form-control border-right-0" placeholder="Password" name="pass" maxlength="50" required>
                                        <div class="input-group-append">
                                            <a href="#" class="input-group-text bg-white border-left-0 ts-password-toggle">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <!--Repeat Password-->
                                    <div class="input-group ts-has-password-toggle">
                                        <input type="password" class="form-control border-right-0" name="pass2" placeholder="Repeat Password" maxlength="50" required>
                                        <div class="input-group-append">
                                            <a href="#" class="input-group-text bg-white border-left-0 ts-password-toggle">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <!--Checkbox-->
                                    <select class="custom-select my-2" name="opt">
                                        <option hidden="option" selected disabled>Register As</option>
                                        <option value="1">Register as Administrator</option>
                                        <option value="2">Register as Coordinator</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                                     
                                    <div class="mt-4" align="center">
                                        <button id="register" type="submit" class="btn btn-primary justify-content-center" name="register" value="register">Register</button>
                                    </div>
                            <br>
                                    
                                    <?php

                                    if (isset($_POST['register'])) {
                                         switch ($_POST['opt']) {
                                        case '1':
                                            $search = "SELECT * FROM users WHERE mail = '$_POST[email]' ";
                                            $result = $link->query($search);
                                            $count = mysqli_num_rows($result);

                                            if ($count == 0) {
                                            $passD = SED::encryption($_POST['pass']);
                                            $query = "INSERT INTO users (mail, psw, usert, name, l_name) VALUES ('$_POST[email]', '$passD','Admin', '$_POST[name]', '$_POST[l_name]')";
                                            date_default_timezone_set("America/Toronto");
            								                $date = date('Y-m-d H:i:s');
                                            $query2 = "INSERT INTO webmaster (user, activity, dates, edit_user) VALUES ('$usuario', 'Created Administrator', '$date', '$_POST[email]')";
                                            $query3 = "INSERT INTO agents (name, l_name, country, city, state, a_mail, num, agency, id_m, photo, plan) VALUES ('$_POST[name]', '$_POST[l_name]', 'Canada', 'Toronto', 'ON', '$_POST[email]', '$_POST[num]', 'Homebor', '0', '/assets/logos/6.png', 'Ilimitated')";
                                            $resultado=$link->query($query);
                                            $resultado2=$link->query($query2);
                                            $resultado3=$link->query($query3);
                                            echo  "<script type='text/javascript'>window.top.location='index.php';</script>"; exit;
                                            }

                                            else{
                                                echo "<p class='message' align='center' id='bad'><b> The Email Already Exists, Please Enter Another Email. <b></p>";
                                            }

                                            break;
                                        case '2':
                                            $search = "SELECT * FROM users WHERE mail = '$_POST[email]' ";
                                            $result = $link->query($search);
                                            $count = mysqli_num_rows($result);

                                            if ($count == 0) {
                                            $passD = SED::encryption($_POST['pass']);
                                            $query = "INSERT INTO users (mail, psw, usert, name, l_name) VALUES ('$_POST[email]', '$passD','Cordinator', '$_POST[name]', '$_POST[l_name]')";
                                           date_default_timezone_set("America/Toronto");
            								                $date = date('Y-m-d H:i:s');
                                            $query2 = "INSERT INTO webmaster (user, activity, dates, edit_user) VALUES ('$usuario', 'Created a Coordinator', '$date', '$_POST[email]')";
                                            $query3 = "INSERT INTO agents (name, l_name, country, city, state, a_mail, num, agency, id_m, photo, plan) VALUES ('$_POST[name]', '$_POST[l_name]', 'Canada', 'Toronto', 'ON', '$_POST[email]', '$_POST[num]', 'Homebor', '0', '/assets/logos/6.png', 'Ilimitated')";
                                             $resultado=$link->query($query);
                                             $resultado2=$link->query($query2);
                                             $resultado3=$link->query($query3);
                                            echo  "<script type='text/javascript'>window.top.location='index.php';</script>"; exit;
                                            }

                                            else{
                                                echo "<p class='message' align='center' id='bad'><b> The Email Already Exists, Please Enter Another Email. <b></p>";
                                            }
                                            break;
                                        }
                                    }
                                    ?>
                                </form>
                            </div>
                            <!--end #register.tab-pane-->
                        </div>
                        <!--end tab-content-->

                    </div>
                    <!--end offset-4.col-md-4-->

                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        </section>



       <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->
<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.magnific-popup.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/custom.js"></script>
</body>
</html>