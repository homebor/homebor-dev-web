<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM manager WHERE id_m = '$id' ");
$row=$query->fetch_assoc();

$query6 = $link->query("SELECT * FROM academy");
$row6=$query6->fetch_assoc();

}

$query3="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado3=$link->query($query3);

    $row3=$resultado3->fetch_assoc();

    if ($row3['usert'] != 'Admin') {
        if ($row3['usert'] != 'Cordinator') {
         header("location: ../logout.php");   
        }
    }

    if ($row3['usert'] == 'Admin') {
        $way = 'index.php';
    } elseif ($row3['usert'] == 'Cordinator') {
       $way = 'cordinator.php';
       echo '<style type="text/css">a#admin{display:none;}</style>';
    }

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/edit_manager.css">

    <!--Mapbox Links-->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Edit Provider</title>

</head>
<body>

<?php
    include 'header.php';
?>

<div class="container_register">

    <main class="card__register">
        <form id="form" class="ts-form" action="edit_manager_admin.php" method="POST" autocomplete="off" enctype="multipart/form-data">
            
            <h1 class="title__register">Edit Provider Information</h1>
            
            <div class="row m-0 p-0">

                <div class="col-md-4 column-1">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Basic Provider Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">
                                <img class="form__group__photo" id="preview" src="../<?php echo $row['photo'] ?>" alt="">

                                <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_a" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                                <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" title="Change Frontage Photo"></label>
                            </div>

                            <div class="info"></div>


                                <!-- Group Name -->

                                <div class="form__group form__group__name" id="group__name">
                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">Name</label>

                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>

                                        <?php if(empty($row['name'] || $row['name'] == 'NULL')){ ?>

                                            <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">
                                            
                                        <?php }else{ ?>
                                                
                                            <input type="text" id="name" name="name" class="form__group-input names" value="<?php echo $row['name'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                    
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__l_name" id="group__l_name">
                                    <div class="form__group__icon-input">

                                        <label for="l_name" class="form__label">Last Name</label>

                                        <label for="l_name" class="icon"><i class="icon icon__names"></i></label>

                                        <?php if(empty($row['l_name'] || $row['l_name'] == 'NULL')){ ?>

                                            <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="e.g. Smith">

                                        <?php }else{ ?>

                                            <input type="text" id="l_name" name="l_name" class="form__group-input names" value="<?php echo $row['l_name'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__mail" id="group__mail">
                                    <div class="form__group__icon-input">

                                        <label for="mail" class="form__label">Mail *</label>

                                        <label for="mail" class="icon"><i class="icon icon__mail2"></i></label>

                                        <?php if(empty($row['mail'] || $row['mail'] == 'NULL')){ ?>

                                            <input type="text" id="mail" name="mail" class="form__group-input names" placeholder="e.g. Smith">

                                        <?php }else{ ?>

                                            <input type="text" id="mail" name="mail" class="form__group-input names" value="<?php echo $row['mail'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                        
                        
                        </div>

                    </div>


                    <div class="card">


                        <h3 class="form__group-title__col-4 location">Location</h3>

                        <div id='map'></div>
                        
                        <div class="information__content location__div">

                        
                            <!-- Group Address -->
                            <div class="form__group form__group__dir" id="group__dir">
                                <div class="form__group__icon-input">

                                    <label for="dir" class="form__label">Address</label>

                                    <label for="dir" class="icon"><i class="icon icon__dir"></i></label>

                                    <?php if(empty($row['dir'] || $row['dir'] == 'NULL')){ ?>
                                    
                                        <input type="text" id="dir" name="dir" class="form__group-input dir" placeholder="Av, Street, etc.">

                                    <?php }else{ ?>

                                        <input type="text" id="dir" name="dir" class="form__group-input dir" value="<?php echo $row['dir'] ?>">

                                    <?php } ?>
                                    
                                </div>
                                <p class="form__group__input-error">Please enter a valid Address</p>
                            </div>
                            
                            <!-- Group City -->
                            <div class="form__group form__group__city" id="group__city">
                                <div class="form__group__icon-input">

                                    <label for="city" class="form__label">City</label>

                                    <label for="city" class="icon"><i class="icon icon__dir"></i></label>

                                    <?php if(empty($row['city'] || $row['city'] == 'NULL')){ ?>
                                    
                                        <input type="text" id="city" name="city" class="form__group-input city2" placeholder="e.g. Toronto">

                                    <?php }else{ ?>

                                        <input type="text" id="city" name="city" class="form__group-input city2" value="<?php echo $row['city'] ?>">

                                    <?php } ?>

                                </div>
                                <p class="form__group__input-error">Please enter a valid City</p>
                            </div>
                            
                            <!-- Group State -->
                            <div class="form__group form__group__state" id="group__state">
                                <div class="form__group__icon-input">

                                    <label for="state" class="form__label">State</label>

                                    <label for="state" class="icon"><i class="icon icon__dir"></i></label>

                                    <?php if(empty($row['state'] || $row['state'] == 'NULL')){ ?>

                                        <input type="text" id="state" name="state" class="form__group-input state" placeholder="e.g. Ontario">
                                    
                                    <?php }else{ ?>    
                                        
                                        <input type="text" id="state" name="state" class="form__group-input state" value="<?php echo $row['state'] ?>">
                                    
                                    <?php } ?>
                                    
                                </div>
                                <p class="form__group__input-error">Please enter a valid State</p>
                            </div>
                            
                            <!-- Group Postal Code -->
                            <div class="form__group form__group__p_code" id="group__p_code">
                                <div class="form__group__icon-input">

                                    <label for="p_code" class="form__label">Postal Code</label>

                                    <label for="p_code" class="icon"><i class="icon icon__dir"></i></label>

                                    <?php if(empty($row['p_code'] || $row['p_code'] == 'NULL')){ ?>

                                        <input type="text" id="p_code" name="p_code" class="form__group-input p_code" placeholder="e.g. 145187">

                                    <?php }else{ ?>

                                        <input type="text" id="p_code" name="p_code" class="form__group-input p_code" value="<?php echo $row['p_code'] ?>">

                                    <?php } ?>

                                </div>
                                <p class="form__group__input-error">Please enter a valid Postal Code</p>
                            </div>


                                

                        </div>

                    </div>


                </div>



                <div class="col-md-8 column-2">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Acomodation Provider Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">
                                
                                <!-- Group Accommodation P. Name -->
                                <div class="form__group col-sm-4 form__group__basic form__group__a_name" id="group__a_name">
                                    <div class="form__group__icon-input">

                                        <label for="a_name" class="form__label">Accommodation P. Name</label>

                                        <label for="a_name" class="icon"><i class="icon icon__a_name"></i></label>

                                        <?php if(empty($row['a_name'] || $row['a_name'] == 'NULL')){ ?>

                                            <input type="text" id="a_name" name="a_name" class="form__group-input a_name" placeholder="e.g. Enterprise">

                                        <?php }else{ ?>

                                            <input type="text" id="a_name" name="a_name" class="form__group-input a_name" value="<?php echo $row['a_name'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Write the name correctly</p>
                                </div>
                                
                                <!-- Group Alternative Mail -->
                                <div class="form__group col-sm-4 form__group__basic form__group__mail2" id="group__mail2">
                                    <div class="form__group__icon-input">

                                        <label for="mail2" class="form__label">Alternative Mail</label>

                                        <label for="mail2" class="icon"><i class="icon icon__mail2"></i></label>

                                        <?php if(empty($row['mail2'] || $row['mail2'] == 'NULL')){ ?>

                                            <input type="text" id="mail2" name="mail2" class="form__group-input mail2" placeholder="e.g. enterprise@gmail.com">

                                        <?php }else{ ?>

                                            <input type="text" id="mail2" name="mail2" class="form__group-input mail2" value="<?php echo $row['mail2'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid email</p>
                                </div>
                                
                                <!-- Group Agency Number -->
                                <div class="form__group col-sm-4 form__group__basic form__group__num" id="group__num">
                                    <div class="form__group__icon-input">

                                        <label for="num" class="form__label">Agency Number</label>

                                        <label for="num" class="icon"><i class="icon icon__num"></i></label>

                                        <?php if(empty($row['num'] || $row['num'] == 'NULL')){ ?>

                                            <input type="text" id="num" name="num" class="form__group-input num" placeholder="e.g. 5557891">

                                        <?php }else{ ?>

                                            <input type="text" id="num" name="num" class="form__group-input num" value="<?php echo $row['num'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter the number correctly</p>
                                </div>
                                
                                
                                <!-- Group Description -->
                                <div class="form__group col-sm-12 form__group__basic form__group__des" id="group__des">
                                    <div class="form__group__icon-input">

                                        <label for="des" class="form__label label__description">Description</label>

                                        <label for="des" class="icon"><i class="icon icon__des"></i></label>

                                        <?php if(empty($row['des'] || $row['des'] == 'NULL')){ ?>

                                            <textarea type="text" id="des" rows="4" name="des" class="form__group-input des" placeholder="e.g. Describe your Accommodation Provider using a few words"></textarea>
                                            
                                        <?php }else{ ?>
                                                
                                            <textarea type="text" id="des" rows="4" name="des" class="form__group-input des"><?php echo $row['des'] ?></textarea>

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>






                    <div class="card">

                        <h3 class="form__group-title__col-4">Favourites Academies</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <h4 class="title__academies_f">You can only select 5 Academies</h4>

                                <div class="content__academies">
                                <?php
                                    while($row6=mysqli_fetch_array($query6)){
                                        if ($row6['id_ac'] == $row['n_a'] OR $row6['id_ac'] == $row['n_a2'] OR $row6['id_ac'] == $row['n_a3'] OR $row6['id_ac'] == $row['n_a4'] OR $row6['id_ac'] == $row['n_a5']) {
                                            echo "<tr>

                                        <td><input type='checkbox' value=".$row6['id_ac']." name='n_a[]' checked></td>
                                        <td>".$row6['name_a'].",</td>
                                        <td>".$row6['dir']."</td>
                                        <br>
                                    </tr>";
                                        } else {
                                
                                echo "<tr>

                                        <td><input type='checkbox' value=".$row6['id_ac']." name='n_a[]''></td>
                                        <td>".$row6['name_a'].",</td>
                                        <td>".$row6['dir_a']."</td>
                                        <br>
                                    </tr>";
                                }
                                    }
                                ?>
                                </div>
                            
                            </div>

                        </div>

                    </div>


                </div>
            </div><br>

            <div class="form__message" id="form__message">
                <p>llene los campos correctamente</p>
            </div>

            <div class="form__group__btn-send" align="center" id="btn__success">
                <button type="submit" name="discard" class="form__btn discard" id="btn_">Save Changes</button>
                <button type="submit" name="options" class="form__btn options" id="btn_">Save Changes</button>
                <button type="submit" name="update" class="form__btn save" id="btn_">Save Changes</button>
            </div>
            
            <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>

            

            
        </form>
    </main>

</div>


    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


<?php include 'footer.php' ?>

<script>
                                function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }
</script>

<script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address = "<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 14
                                    });
                                    new mapboxgl.Marker( )
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
                                </script>
                                <!--end col-md-6-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>