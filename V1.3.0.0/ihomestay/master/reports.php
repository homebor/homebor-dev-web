<?php 
include '../xeon.php';
session_start();


$usuario = $_SESSION['username'];

$query2 = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND report_s != 'NULL' ORDER BY date_ DESC");


$e = ' ';


$query="SELECT * FROM users WHERE mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

if ($row['usert'] != 'Admin') {
    
header("location: ../logout.php");   

}



/* Notifications */

$req =  $link->query("SELECT * FROM events WHERE email = '$usuario'");
$r = mysqli_num_rows($req);

$noti =  $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND state = '0' ");
$n = mysqli_num_rows($noti);

$usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$u=$usu->fetch_assoc();

$peh = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
$pe=$peh->fetch_assoc();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="assets/css/reports1.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Manager</title>

    <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>
<body style="background-image: url('../assets/img/wallpaper.jpg'); ">

    <!--HEADER ============================================================================-->
    <?php 
        include 'header.php';
    ?>

<div class="line__top"></div>

<main class="main__div">

    <div class="form__group__reports__div">
        <div class="col-md-4 form__group__reports__details">
            <div class="form__group__reports__titles reports_right">
                <h2 class="form__group__reports__title">Reports</h2>
                <!--<a href="#" onclick="newReport()" class="form__group_reports_add">+</a>-->
            </div>

            <div class="form__goup__div__search__report">
                <label for="search_box" class="fa fa-search label__search"></label>
                <input type="text" name="search_box" id="search_box" class="input__search" placeholder="Search a Report">
            </div>


            <div class="form__group__reports__div2">

                <div class="form__group__reports__content content1"></div>
                <div class="form__group__reports__content2 content2"></div>

            </div>
        </div>

        <div class="col-md-8 form__group__reports__description">
            
        </div>
    </div>

</main>

<input type="hidden" id="mail_u" value="<?php echo $usuario ?>">


<script>

function newReport(){
        $('#view').modal("show");
    }

    $('.content2').hide();

    $('#search_box').keyup(function() {

        if ($('#search_box').val()) { 

            $('.content1').hide();
            $('.content2').show();

            let search = $('#search_box').val();

            var searchLength = search.length;

            $.ajax({ 
                url: 'report_search.php',
                type: 'POST',
                data: { search, },
                success: function(response) {
                    let reports = JSON.parse(response);
                    let template = '';

                    reports.forEach(report => {

                        if(report.view_a == '0'){
                                template += `
                                    <button class="reports div_new_report" id="reports" onclick="viewRep(${report.id_not})">

                                    <div class="new__report"></div>

                                        <p class="date_report new_report">${report.date}</p>
                                `;
                            }else{
                                template += `
                                    <button class="reports div_read_report" id="reports" onclick="viewRep(${report.id_not})">

                                        <p class="date_report ">${report.date}</p>
                                `;
                            }
                            
                                    template += `
                                        <input type="hidden" name="id_not3" value="${report.id_not}" />
                                        <div class="col-2 p-0 img_profile_photo">
                                    `;
                                    template += `
                                            <img src="../${report.phome}" class="photo" alt="" title="${report.h_name}">
                                            <img src="../${report.photo_s}" class="photo_stu_rep" alt="" title="${report.names}">

                                        </div>
                                    `;

                                    if(report.view_a == '0'){
                                        template += `
                                        <div class="col-10 mt-auto mb-auto description_rep ">
                                            <p class="reports__content__title new_report"> ${report.h_name} </p>
                                            <p class="reports__content read"><b>${report.title}</b></p>
                                        `;
                                    }else{
                                        template += `
                                        <div class="col-10 mt-auto mb-auto description_rep">
                                            <p class="reports__content__title read_report"> ${report.h_name} </p>
                                            <p class="reports__content read">${report.title}</p>
                                        `;
                                    }
                                            template += `
                                            <div class="status">
                                            `;

                                                if(report.status == 'Active'){
                                                
                                                    template += `
                                                        <p class="p-0 m-0 status_active"> &#9724;${report.status}</p>
                                                    `;
                                                }else{
                                                    
                                                    template += `
                                                        <p class="p-0 m-0 status_close"> &#9724;${report.status}</p>
                                                    `;
                                                }

                                template += `
                                            </div>
                                        </div>
                                    
                                    </button>

                                    
                                `;

                    });

                    $('.content2').html(template);

                }

            });

        }else {
          $('.content1').show();
          $('.content2').hide();
        }

    });

    $(document).on('click', '.page-link', function() {
        var page = $(this).data('page_number');
        var query = $('#search_box').val();
        load_data(page, query);
    });

    function report_list(){

        var mail_a = $('#mail_u').val();

        $.ajax({
            url:"report_list.php",
            method:"POST",
            data:{mail_a},
            success:function(listrep)
            {
                let students = JSON.parse(listrep);
                let template = '';

                    students.forEach(student => {

                        if(student.view_a == '0'){
                            template += `
                                <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                                <div class="new__report"></div>

                                    <p class="date_report new_report">${student.date}</p>
                            `;
                        }else{
                            template += `
                                <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                    <p class="date_report ">${student.date}</p>
                            `;
                        }
                        
                                template += `
                                    <div class="col-2 p-0 img_profile_photo">
                                `;
                                template += `
                                        <img src="../${student.phome}" class="photo" alt="" title="${student.h_name}">
                                        <img src="../${student.photo_s}" class="photo_stu_rep" alt="" title="${student.names}">

                                    </div>
                                `;

                                if(student.view_a == '0'){
                                    template += `
                                    <div class="col-10 mt-auto mb-auto description_rep ">
                                        <p class="reports__content__title new_report"> ${student.h_name}</p>
                                        <p class="reports__content read"><b>${student.title}</b></p>
                                    `;
                                }else{
                                    template += `
                                    <div class="col-10 mt-auto mb-auto description_rep">
                                        <p class="reports__content__title read_report"> ${student.h_name}</p>
                                        <p class="reports__content read">${student.title}</p>
                                    `;
                                }
                                        template += `
                                        <div class="status">
                                        `;

                                            if(student.status == 'Active'){
                                            
                                                template += `
                                                    <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                                `;
                                            }else{
                                                
                                                template += `
                                                    <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                                `;
                                            }

                            template += `
                                        </div>
                                    </div>
                                
                                </button>
                            `;

                    });

                $('.form__group__reports__content').html(template);


            }
        });
    }

    setInterval( function(){
                    
        report_list();
                            
    },1000);//Actualizo cada 3 segundos


    var id_s = $('#id_s').val();

        


    function viewRep($s){
        var id = $s;

        

        $.ajax({
            url:"feed_reports.php",
            method:"POST",
            data:{id, id_s},
            success:function(data)
            {
                $('.form__group__reports__description').html(data);

            }
        });


        var mail_a = $('#mail_u').val();

        $.ajax({
            url:"report_list.php",
            method:"POST",
            data:{mail_a},
            success:function(listrep)
            {
                let students = JSON.parse(listrep);
            let template = '';

                students.forEach(student => {

                    if(student.view_a == '0'){
                        template += `
                            <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                            <div class="new__report"></div>

                                <p class="date_report new_report">${student.date}</p>
                        `;
                    }else{
                        template += `
                            <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                <p class="date_report ">${student.date}</p>
                        `;
                    }
                    
                            template += `
                                <div class="col-2 p-0 img_profile_photo">
                            `;
                            template += `
                                    <img src="../${student.phome}" class="photo" alt="" title="${student.h_name}">
                                    <img src="../${student.photo_s}" class="photo_stu_rep" alt="" title="${student.names}">

                                </div>
                            `;

                            if(student.view_a == '0'){
                                template += `
                                <div class="col-10 mt-auto mb-auto description_rep ">
                                    <p class="reports__content__title new_report"> ${student.h_name}</p>
                                    <p class="reports__content read"><b>${student.title}</b></p>
                                `;
                            }else{
                                template += `
                                <div class="col-10 mt-auto mb-auto description_rep">
                                    <p class="reports__content__title read_report"> ${student.h_name}</p>
                                    <p class="reports__content read">${student.title}</p>
                                `;
                            }
                                    template += `
                                    <div class="status">
                                    `;

                                        if(student.status == 'Active'){
                                        
                                            template += `
                                                <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                            `;
                                        }else{
                                            
                                            template += `
                                                <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                            `;
                                        }

                        template += `
                                    </div>
                                </div>
                            
                            </button>
                        `;
                        template += `
                        
                        `;

                });

            $('.form__group__reports__content').html(template);

            }
        });
        


        
    }
</script>


<script>
                                function previewImageii() {
                            var file = document.getElementById("file-ii").files;

                            if (file.length > 0) {
                            var fileReader = new FileReader();

                            fileReader.onload = function (event) {
                            document.getElementById("preview-ii").setAttribute("src", event.target.result);
                            };

                            fileReader.readAsDataURL(file[0]);

                            var label_1 = document.getElementById("label-ii");
                            var label_1_1 = document.getElementById("label-ii-i");

                            label_1.style.display = "none";
                            label_1_1.style.display = "inline";

                            }

                            }
                            </script>

    

    




<?php include 'footer.php' ?>

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>




</body>
</html>