<?php
    require '../xeon.php';
    include '../cript.php';
    session_start(); 
    error_reporting(0);

    // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion

    $query="SELECT * FROM users WHERE mail = '$usuario'";
    $resultado=$link->query($query);

    $row=$resultado->fetch_assoc();

    if ($row['usert'] == 'Admin') {
        $way = 'index.php';
    } elseif ($row['usert'] == 'Cordinator') {
       $way = 'cordinator.php';
       echo '<style type="text/css">a#admin{display:none;}</style>';
    }
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/simplebar.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/index.css"> 
     <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Administartor Panel</title>

</head>
<body class=" bg-white">


 <div class="ts-page-wrapper ts-homepage " id="page-top">
   <!--HEADER ===============================================================-->
<?php 
	include 'header.php';
?>

    <main id="ts-main-cordinator">

      










        <!-- Services
        =============================================================================================================-->
        <section class="ts-block bg-white" data-bg-pattern="">

            <section id="page-title">
                       <div class="text-center">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                              <br>
                              <br>
                              <br>
                                <br>
                             <?php echo "<h2>"."Welcome to Administrator Panel " . $row['name']. " " . $row['l_name']. "</h2>"?>
    <!-- Echo username -->
    <?php
        $usuario = $_SESSION['username'];
        echo "<p>You are logged in as <b>$usuario</b>.</p>"; 
    ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            <div id="margin" class="container py-4">
                <div class="row">
                    <table id="table" class="table table table-borderless table-responsive">
  <thead>
    <tr>
      <th scope="col">
        <a href="homestay.php">
            <div class="ts-feature">
                <a href="homestay.php">
                    <figure class="ts-feature__icon p-2">
                        <a href="homestay.php"> <img src="../images/homestay.png" id="homestay"></a>

                                    <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <a id="homestay2" href="homestay.php"><h3 id="homestay">Homestay</h3></a>
                               
                            </figure></a>
                           
                        </div>
      </th>
      <th></th>
      <th scope="col">
          <ul class="list-group list-group-flush">
              <a id="homestay2" href="homestay.php"><li class="list-group-item">Homestay Administrator Panel</li></a>
              <li class="list-group-item">Check the Registred Homestay</li>
              <li class="list-group-item">Certified the Information Submitted</li>
              <li class="list-group-item">Edit the Submitted Property</li>
              <li class="list-group-item">Delete Homestay</li>
          </ul>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <b>
        <ul class="list-group list-group-flush">
              <a id="homestay2" href="student.php"><li class="list-group-item">Students Administrator Panel</li></a>
              <li class="list-group-item">Check the Registred Students</li>
              <li class="list-group-item">Verify the Students Informations</li>
              <li class="list-group-item">Delete Students</li>
          </ul>
          </b>
      </td>
      <td></td>
      <td>
          <div id="student2" class="col-sm-6 col-md-4">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                <a href="student.php"><img src="../images/student.png" id="student"></a>
                                <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                     <a href="student.php"><h3 id="stu2">Students</h3></a>
                            </figure>
                           
                        </div>
                    </div>
      </td>
    </tr>
    <tr>
      <td>
          <div id="academy2" class="col-sm-6 col-md-3">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                <img src="../images/agency.png" id="agency">
                                <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <h3 id="aca2">Academy</h3>
                            </figure>
                        </div>
                    </div>
      </td>
      <td></td>
      <td>
          <b>
        <ul class="list-group list-group-flush">
              <a id="homestay2" href="#"><li class="list-group-item">Academy Administrator Panel</li></a>
              <li class="list-group-item">Check the Registred Academies</li>
              <li class="list-group-item">Verify the Academies Informations</li>
              <li class="list-group-item">Check and Verify the Registred Coordinators</li>
              <li class="list-group-item">Delete Academy or Coordinators</li>
          </ul>
          </b>
      </td>
    </tr>
  </tbody>
</table>





<table id="table-mobile" class="table table table-borderless table-responsive">
  <thead>
    <tr>
      <th scope="col">
        <a href="homestay.php">
            <div class="ts-feature">
                <a href="homestay.php">
                    <figure class="ts-feature__icon p-2">
                        <a href="homestay.php"> <img src="../images/homestay.png" id="homestay"></a>

                                    <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <a id="homestay2" href="homestay.php"><h3 id="homestay">Homestay</h3></a>
                               
                            </figure></a>
                           
                        </div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
          <div id="student2" class="col-sm-6 col-md-4">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                <img src="../images/student.png" id="student">
                                <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                     <h3 id="stu2">Students</h3>
                            </figure>
                           
                        </div>
                    </div>
      </td>
    </tr>
    <tr>
      <td>
          <div id="academy2" class="col-sm-6 col-md-3">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                <img src="../images/agency.png" id="agency">
                                <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <h3 id="aca2">Academy</h3>
                            </figure>
                        </div>
                    </div>
      </td>
    </tr>
  </tbody>
</table>
                <!--end row-->
            </div>
            <!--end container-->
            </div>
        </section>
        <!--end ts-block-->

        













        <!--Create
        =============================================================================================================-->
        <section hidden>
              </div>
              </div>
        </section>
        </main>



       <!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->
<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.magnific-popup.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/custom.js"></script>
</body>
</html>