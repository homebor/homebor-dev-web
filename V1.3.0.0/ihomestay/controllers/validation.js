// TODO VARIABLES
const D = document;
const W = window;

// ? EXPRESIONES REGULARES
const expReg = {
  name: /^[a-zA-ZÀ-ÿ\_\-\s]{1,100}$/, // Letras, guion, guion_bajo y espacios
  nameSpecials: /^[a-zA-ZÀ-ÿ!"#$%&=?¡°|*¨_:;-<>\s]{1,40}$/, // Letras, especiales, espacios y acentos.
  phone: /^[\d()+\s\d]{2,20}$/, // 2 a 20 numeros.
  address: /^[a-zA-Z0-9\_\-\s\°]{2,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code_eeuu: /(^\d{5}$)|(^\d{5}-\d{4}$)/, // Validar codigo postal
  p_code_canada: /[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]\d/, // Validar codigo postal CANADIENSE
  p_code_canada2: /^(?!.*[DFIOQU])[A-VXY][0-9][A-Z]\s?[0-9][A-Z][0-9]$/, // Validar codigo postal CANADIENSE 2
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/, // 8 a 15 digitos.
  email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/, // Validar Email
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/, // Validar fecha
  description: /^[a-zA-ZÀ-ÿ0-9\s\W_.+-]{1,300}$/, // Descripcion
  removeSpaces: /^[ s]+|[ s]+$/, // No espacios en blanco
};

// TODO FUNCIONES
export default function validate(input, typeValue, word = "invalid") {
  const validateField = (exp, exp2, exp3) => {
    if (exp3) {
      exp.test(input.value) || exp2.test(input.value) || exp3.test(input.value)
        ? input.classList.remove(word)
        : input.classList.add(word);
    } else if (exp2) {
      exp.test(input.value) || exp2.test(input.value) ? input.classList.remove(word) : input.classList.add(word);
    } else {
      exp.test(input.value) ? input.classList.remove(word) : input.classList.add(word);
    }
  };

  // * COMMON VALIDATIONS
  if (typeValue === "name") validateField(expReg.name);
  if (typeValue === "name-special") validateField(expReg.nameSpecials);
  if (typeValue === "phone") validateField(expReg.phone);
  if (typeValue === "email") validateField(expReg.email);
  if (typeValue === "password") validateField(expReg.password);
  if (typeValue === "address") validateField(expReg.address);
  if (typeValue === "city") validateField(expReg.city);
  if (typeValue === "state") validateField(expReg.state);
  if (typeValue === "postal-code") validateField(expReg.p_code_eeuu, expReg.p_code_canada);
  if (typeValue === "description") validateField(expReg.description);
  if (typeValue === "date") validateField(expReg.date);

  // * SPECIFIC VALIDATIONS
  if (typeValue === "rooms") {
    if (/[^0-8$]/g.test(input.value) || input.value < 0) input.value = "";
    else if (input.value > 8) input.value = 8;
  }

  if (typeValue === "number") {
    if (/^\d+$/gi.test(input.value)) input.classList.remove(word);
    else input.classList.add(word);
  }

  if (typeValue === "greaterThanZero") {
    if (/^\d+$/gi.test(input.value) && input.value > 0) input.classList.remove(word);
    else input.classList.add(word);
  }

  if (typeValue === "image") {
    if (!input.files[0].type.includes("image/")) {
      input.value = "";
      return "The upload of this file is not of type image";
    } else {
      const $currentImage = input.parentElement.querySelector("img");
      const Reader = new FileReader();
      Reader.readAsDataURL(input.files[0]);
      Reader.onload = (e) => ($currentImage.src = e.target.result);
    }
  }

  if (typeValue === "pdf") {
    if (!input.files[0].type.includes("application/pdf")) {
      input.value = "";
      return "The upload of this file is not of type pdf";
    } else {
      let $iframe = "";
      if (!input.id.includes("member") && !input.id.includes("BackgroundCheck")) {
        $iframe = D.querySelector(`[data-input="${input.id}"]`);
      } else $iframe = input.parentElement.querySelector("iframe");

      const Reader = new FileReader();
      Reader.readAsDataURL(input.files[0]);
      Reader.onload = (e) => ($iframe.src = e.target.result);
    }
  }

  if (typeValue instanceof Array) {
    if (typeValue.includes(input.value)) input.classList.remove(word);
    else input.classList.add(word);
  }
}

// TODO EVENTOS
D.addEventListener("keyup", (e) => {
  if (e.target.tagName === "INPUT") e.target.classList.remove("invalid");
  if (e.target.tagName === "TEXTAREA") e.target.classList.remove("invalid");
});
D.addEventListener("change", (e) => {
  if (e.target.tagName === "SELECT") e.target.classList.remove("invalid");
});
