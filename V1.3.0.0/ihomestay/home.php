<?php
    require 'xeon.php';
    error_reporting(0);
    ?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/html/index.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor</title>

</head>

<body>

<div class="ts-page-wrapper ts-homepage" id="page-top">
<!--HEADER ===============================================================-->
<?php 
	include 'header.php';
?>



    <!-- HERO SLIDER GALLERY
    =================================================================================================================-->
    <section id="ts-hero" class="ts-hero-slider ts-bg-black mb-0 ">

        <div class="ts-min-h__70vh w-100">

            <!--Owl Carousel-->
            <div class="owl-carousel" data-owl-loop="1" data-owl-nav="1">

                <!-- SLIDE Infografy
                =====================================================================================================-->
                <div class="ts-slide" data-bg-image="">
                <img src='assets/img/new2.jpg' width="500" height="600"/>
                    <div class="ts-slide-description pb-0 h-100 ts-center__vertical" style="background-color: rgba(0, 0, 0, 0);">
                        <div class="container">

                            <!--Title-->
                            <h1 class="mb-3"></h1>

                            <!--Location-->
                            <figure class="ts-opacity__50">
                                
                            </figure>
                                <dt></dt>

                            <!--Features-->
                            <div class="ts-description-lists d-none d-md-block ts-responsive-block">
                                <dl>
                                    <dt></dt>
                                    <dd><sup></sup></dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd></dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd></dd>
                                </dl>
                            </div>

                            
                        </div>
                    </div>
                </div>

                <!-- SLIDE Infografy
                =====================================================================================================-->
                <div class="ts-slide" data-bg-image="">
                <img src='assets/img/infostudent.jpg' width="500" height="600"/>
                    <div class="ts-slide-description pb-0 h-100 ts-center__vertical" style="background-color: rgba(0, 0, 0, 0);">
                        <div class="container">

                            <!--Title-->
                            <h1 class="mb-3"></h1>

                            <!--Location-->
                            <figure class="ts-opacity__50">
                                
                            </figure>
                                <dt></dt>

                            <!--Features-->
                            <div class="ts-description-lists d-none d-md-block ts-responsive-block">
                                <dl>
                                    <dt></dt>
                                    <dd><sup></sup></dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd></dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd></dd>
                                </dl>
                            </div>

                            
                        </div>
                    </div>
                </div>



            </div>
            <!--end owl-carousel-->

            <!--Hero slider control-->
            <div class="ts-hero-slider-control">
                <div class="container" id="owl-control"></div>
            </div>

        </div>

    </section>
    <!--end ts-hero-->
    <main id="ts-main">








        <!-- Services ICONS
        =============================================================================================================-->
        <section class="ts-block bg-white" id='service'>
            <div class="container py-4">
                <div class="row">
                    <!--Home Information-->
                    <div class="col-sm- col-md-4" id="home-info">
                        <div class="ts-feature">
                            <a href="work_with_us.php"><figure class="ts-feature__icon p-2">
                                    <span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span>
                               <a href="work_with_us.php"> <img src="images/agency.png" id="service-image"></a>
                            </figure></a>
                            <a id="homestay2" href="work_with_us.php"><h4>Accommodation Providers</h4></a>
                            <p id="homestay2">Find the perfect match, availability and book a room in just a few clicks.</p>
                        </div>
                    </div>
                    <!--Students-->
                    <div id="student2" class="col-sm-6 col-md-4">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                    <a href="about_us.php#homestay"><span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span></a>
                                <a href="about_us.php#homestay"><img src="images/homestay.png" id="service-image"></a>
                            </figure>
                            <a href="about_us.php#homestay"><h4 id="stu2">Homestay</h4></a>
                            <p id="stu2">One simple and intuitive app to update availability, confirm bookings and communicate with the school.</p>
                        </div>
                    </div>
                    <!--Academies-->
                    <div id="academy2" class="col-sm-6 col-md-3">
                        <div class="ts-feature">
                            <figure class="ts-feature__icon p-2">
                                    <a href="#"><span class="ts-circle" id="circle">
                                        <i class="fa fa-check"></i>
                                    </span></a>
                                <a href="#"><img src="images/student.png" id="service-image"></a>
                            </figure>
                            <a href="#"><h4 id="aca2">Students</h4></a>
                            <p id="aca2"> Students take active part of the matching process, significantly reducing changes and cancellations.</p>
                        </div>
                    </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end ts-block-->

        <!--IMAGE PROMO
        =============================================================================================================-->
        <section id="pricing" class="ts-block pt-0">
            <div class="container">
                <!--Title-->
                <div class="ts-title text-center">
                    <br>
                    <br>
                    <h2 class="mb-5">Our App</h2>
                </div>
                <img src="images/4.png" id="app">
            </div>
            <!--end container-->
        </section>
        </div>



<!--FOOTER ===============================================================-->
<?php
	include 'footer.php';
?>

</div>
</section>
</main>
</main>
</div>
<!--end page-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/custom.js"></script>
</body>
</html>