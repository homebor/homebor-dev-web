<?php
// TODO WINYERSON
if ($_POST['homestay_id']) {
  require '../../../xeon.php';

  session_start();
  error_reporting(0);

  // TODO VARIABLES
  $id = $_POST['homestay_id'];
  $usuario = $_SESSION['username'];

  // TODO VALIDACIONES
  if (!$link || !$id || !$usuario) echo "Insuficientes parámetros", exit;

  // TODO CONSULTAS
  $homestayQuery = $link->query("SELECT id_home, mail_h, name_h, l_name_h FROM pe_home WHERE id_home = '$id' ");
  $row_homestay = $homestayQuery->fetch_assoc();


  $queryRoom_IJ = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home ");
  $row_room = $queryRoom_IJ->fetch_assoc();


  $queryPhotoHome_IJ = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
  $row_photo_home = $queryPhotoHome_IJ->fetch_assoc();


  $queryStudent = $link->query("SELECT name_s, l_name_s, firstd, lastd, status FROM pe_student WHERE mail_s = '$usuario'");
  $row_student = $queryStudent->fetch_assoc();


  $queryEventStudent = $link->query("SELECT * FROM events WHERE mail_s = '$usuario'");
  $row_event_student = $queryEventStudent->fetch_assoc();


  $queryNotificationLimit = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND title = 'Reservation Request'";
  $notifications_limit = $link->query($queryNotificationLimit);
  $num_rows_notification_limit = mysqli_num_rows($notifications_limit);


  $roomNumber = 1;
  foreach ($row_room as $key => $value) {
    // * ROOM
    if ($roomNumber == 1) $roomColor = "#232159";
    if ($roomNumber == 2) $roomColor = "#982A72";
    if ($roomNumber == 3) $roomColor = "#394893";
    if ($roomNumber == 4) $roomColor = "#A54483";
    if ($roomNumber == 5) $roomColor = "#5D418D";
    if ($roomNumber == 6) $roomColor = "#392B84";
    if ($roomNumber == 7) $roomColor = "#B15391";
    if ($roomNumber == 8) $roomColor = "#4F177D";

    // ** ROOM HEADER
    $roomName = "Room " . $roomNumber;
    $roomPriceHomestay = $row_room['aprox' . $roomNumber];
    $roomPriceAgent = $row_room['aprox_a' . $roomNumber];

    // ** ROOM FEATURES
    $roomType = $row_room['type' . $roomNumber];
    $roomFood = $row_room['food' . $roomNumber];


    // ** ROOM RESERVES AND BEDS
    $queryNotification = "SELECT * FROM notification WHERE user_i_mail = '$usuario' AND color = '$roomColor' AND user_r = '$row_homestay[mail_h]' AND reserve_h = '$row_homestay[mail_h]' AND confirmed = '0' AND title = 'Reservation Request'";
    $notification = $link->query($queryNotification);
    $row_notification = $notification->fetch_assoc();
    $num_rows_notification = mysqli_num_rows($notification);

    if ($num_rows_notification == 1) $myRequest = $roomName;
    if ($num_rows_notification_limit == 3) {
      $myRequestsLimit = [true];
      $total_notification = $link->query($queryNotificationLimit);
      while ($value = mysqli_fetch_array($total_notification)) {
        $myRequestsLimit[] = [$value['user_r'], $value['des'], $value['color']];
      }
    }
    if ($row_student['status'] == "Homestay Found") $homestayFound = $row_event_student;


    // * CURRENT ROOM DATA MAIN
    $strQueryRooms = "SELECT * FROM room WHERE id_home = $id";
    $queryRooms = $link->query($strQueryRooms);

    $strQueryEvents = "SELECT * FROM events WHERE color = '$roomColor'  AND email = '$row_homestay[mail_h]' AND status = 'Active' ";
    $queryEvents = $link->query($strQueryEvents);

    $myReserveDate = array($row_student['firstd'], $row_student['lastd']);

    // ?? CAMBIAR POSICIÓN DE DATOS AFECTA A LA HORA DE RENDERIZARLOS
    $allBeds = array(
      'bed1' => [
        $roomName,                          // 0
        $row_room['bed' . $roomNumber],     // 1
        "A",                                // 2
        $row_room['date' . $roomNumber],    // 3
        $row_room['type' . $roomNumber]     // 4
      ],
      'bed2' => [
        $roomName,                                // 0
        $row_room['bed' . $roomNumber . '_2'],    // 1
        "B",                                      // 2
        $row_room['date' . $roomNumber . "_2"],   // 3
        $row_room['type' . $roomNumber]           // 4
      ],
      'bed3' => [
        $roomName,                                // 0
        $row_room['bed' . $roomNumber . '_3'],    // 1
        "C",                                      // 2
        $row_room['date' . $roomNumber . "_3"],   // 3
        $row_room['type' . $roomNumber]           // 4
      ],
    );

    $allBedsReserved = null;
    while ($value = mysqli_fetch_array($queryEvents)) {
      $queryStudent2 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$value[mail_s]' ");
      $row_student2 = $queryStudent2->fetch_assoc();


      $allBedsReserved[] = array(
        $value['title'],
        $value['bed'],
        $value['start'],
        $value['end'],
        $row_student2['photo_s'],
        $row_student2['id_student']
      );
    }


    // ** ROOM DATA
    $jsonToSend[] = array(
      // ** ID 
      'id_home' => $row_homestay['id_home'],
      // ** HEADER
      'roomTitle' => array(
        'name' => $roomName,
        'priceHomestay' => $roomPriceHomestay,
        'priceAgent' => $roomPriceAgent,
        'color' => $roomColor,
      ),
      // ** PHOTO
      'roomImage' => array(
        'image1' => $row_photo_home['proom' . $roomNumber],
        'image2' => $row_photo_home['proom' . $roomNumber . "_2"],
        'image3' => $row_photo_home['proom' . $roomNumber . "_3"],
      ),
      // ** FEATURES
      'roomFeatures' => array(
        'type' => $roomType,
        'food' => $roomFood,
      ),
      // ** ALL BEDS AND RESERVES
      'myReservesInfo' => array(
        'myReserveDate' => $myReserveDate,
        'myRequest' => $myRequest,
        'myRequestsLimit' => $myRequestsLimit,
        'homestayFound' => $homestayFound,
      ),
      'roomReservesInfo' => array(
        'allBeds' => $allBeds,
        'allBedsReserved' => $allBedsReserved,
      ),
      // ** RESERVATION FORM DATA (ONLY STUDENTS)
      'homestayFullName' => $row_homestay['name_h'] . " " . $row_homestay['l_name_h'],
      'homestayMail' => $row_homestay['mail_h'],
      'studentFullName' => $row_student['name_s'] . " " . $row_student['l_name_s'],
      'studentFirstDay' => $row_student['firstd'],
      'studentLastDay' => $row_student['lastd'],
      // ** STATUS
      'reservations' => $row_room['reservations' . $roomNumber]
    );

    $roomNumber++;
    if ($roomNumber > 8) break;
  }

  echo json_encode($jsonToSend);
} else echo json_encode("Se necesita el ID de una casa");
