<?php
require '../../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$ID_STUDENT = $_POST['student_id'];
$section = $_POST['section'];

$queryAgents = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario' ");
$row_agent = $queryAgents->fetch_assoc();

$queryHomestay = $link->query("SELECT * FROM pe_home WHERE id_m = '$row_agent[id_m]' ");
$row_homestay = $queryHomestay->fetch_assoc();

$queryStudent = $link->query("SELECT id_m, photo_s, name_s, l_name_s, nationality, firstd, lastd, gen_s, db_s, lodging_type, smoke_s, pets, food, vegetarians, halal, kosher, lactose, gluten, pork, status, ac_confirm FROM pe_student WHERE id_student = '$ID_STUDENT' ");
$row_student = $queryStudent->fetch_assoc();

$queryAcademy_IJ = $link->query("SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$ID_STUDENT' and academy.id_ac = pe_student.n_a");
$row_academy_IJ = $queryAcademy_IJ->fetch_assoc();

if (
  $row_agent['id_m'] != $row_student['id_m'] ||
  $row_student['status'] == 'Homestay Found' ||
  $row_student['status'] == 'Confirm Registration' ||
  $row_student['ac_confirm'] == 'No'
) {
  echo json_encode(['redirect', 'index.php']), exit;
}

while ($homestay = mysqli_fetch_array($queryHomestay)) {
  // * EVENTOS (RESERVACIONES)
  $queryEvents = $link->query("SELECT * FROM events WHERE email = '$homestay[mail_h]' ");
  while ($event = mysqli_fetch_array($queryEvents)) $allEvents[] = $event;


  $queryRooms = $link->query("SELECT * FROM room WHERE id_home = '$homestay[id_home]' ");
  $row_room = $queryRooms->fetch_assoc();


  // * VALIDAR CAMPOS DE LA CASA
  if (!file_exists("../../../" . $homestay['phome'])) $homestay['phome'] = "assets/emptys/frontage-empty.png";
  if (empty($homestay['dir']) || $homestay['dir'] === "NULL") $homestay['dir'] = "Empty";
  if (
    empty($homestay['city']) || $homestay['city'] === "NULL"
  ) $homestay['city'] = "Empty";
  if (empty($homestay['state']) || $homestay['state'] === "NULL") $homestay['state'] = "Empty";
  if (empty($homestay['g_pre']) || $homestay['g_pre'] === "NULL") $homestay['g_pre'] = "Empty";
  if (empty($homestay['ag_pre']) || $homestay['ag_pre'] === "NULL") $homestay['ag_pre'] = "Empty";
  if (
    empty($homestay['room']) || $homestay['room'] === "NULL"
  ) $homestay['room'] = "Empty";
  if (empty($homestay['smoke']) || $homestay['smoke'] === "NULL") $homestay['smoke'] = "Empty";
  if (empty($homestay['pet']) || $homestay['pet'] === "NULL") $homestay['pet'] = "Empty";
  if (empty($homestay['backl']) || $homestay['backl'] === "NULL") $homestay['backl'] = "Empty";
  if (empty($homestay['m_service']) || $homestay['m_service'] === "NULL") $homestay['m_service'] = "Empty";

  if (isset($homestay) && isset($row_room)) $allHouses[] = array($homestay, $allEvents, $row_room);
}


if ($section == 'student') {
  // * VALIDAR CAMPOS DE ESTUDIANTE
  if (!file_exists("../../../" . $row_student['photo_s'])) {
    $row_student['photo_s'] = "assets/emptys/profile-student-empty.png";
  }
  if (empty($row_student['nationality']) || $row_student['nationality'] === "NULL") {
    $row_student['nationality'] = "Empty";
  }
  if (empty($row_student['firstd']) || $row_student['firstd'] === "NULL") $row_student['firstd'] = "Empty";
  if (empty($row_student['lastd']) || $row_student['lastd'] === "NULL") $row_student['lastd'] = "Empty";
  if (empty($row_student['gen_s']) || $row_student['gen_s'] === "NULL") $row_student['gen_s'] = "Empty";
  if (empty($row_student['db_s']) || $row_student['db_s'] === "NULL") $row_student['db_s'] = "Empty";

  // * VALIDAR CAMPOS DE ACADEMIA
  if (empty($row_academy_IJ['dir_a']) || $row_academy_IJ['dir_a'] === "NULL") $row_academy_IJ['dir_a'] = "Empty";
  if (empty($row_academy_IJ['city_a']) || $row_academy_IJ['city_a'] === "NULL") $row_academy_IJ['city_a'] = "Empty";
  if (empty($row_academy_IJ['state_a']) || $row_academy_IJ['state_a'] === "NULL") $row_academy_IJ['state_a'] = "Empty";

  $jsonToSend = array(
    'student' => array(
      'image' => $row_student['photo_s'],
      'name' => $row_student['name_s'],
      'lastname' => $row_student['l_name_s'],
      'nationality' => $row_student['nationality'],
      'academy' => $row_academy_IJ['name_a'],
      'addressAcademy' => $row_academy_IJ['dir_a'],
      'cityAcademy' => $row_academy_IJ['city_a'],
      'stateAcademy' => $row_academy_IJ['state_a'],
      'firstDay' => $row_student['firstd'],
      'lastDay' => $row_student['lastd'],
      'gender' => $row_student['gen_s'],
      'dateBirth' => $row_student['db_s'],
      'more' => array(
        ['Accommodation', $row_student['lodging_type']],
        ['Smoke', $row_student['smoke_s']],
        ['Pets', $row_student['pets']],
        ['Food', $row_student['food']],
        ['Vegetarian', $row_student['vegetarians']],
        ['Halal', $row_student['halal']],
        ['Kosher', $row_student['kosher']],
        ['Lactose', $row_student['lactose']],
        ['Gluten', $row_student['gluten']],
        ['Pork', $row_student['pork']],
      )
    ),
  );
} else $jsonToSend = [$allHouses, $row_student];



echo json_encode($jsonToSend);
