<?php
require '../../../xeon.php';

session_start();
error_reporting(0);

$usuario = $_SESSION['username'];
$ID_STUDENT = $_POST['student_id'];


if ($usuario && $ID_STUDENT) {
  // ? SELECT USERS
  $queryUsers = $link->query("SELECT usert FROM users WHERE mail = '$usuario' ");
  $row_user = $queryUsers->fetch_assoc();


  function sendAssignment($agentData, $agencymail, $ID_STUDENT, $usuario, $link)
  {
    $HOUSE1 = $_POST['house1'];
    $HOUSE2 = $_POST['house2'];
    $HOUSE3 = $_POST['house3'];
    $HOUSE4 = $_POST['house4'];
    $HOUSE5 = $_POST['house5'];


    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');


    // ? SELECT PE_STUDENT
    $queryStudents = $link->query("SELECT mail_s FROM pe_student WHERE id_student = '$ID_STUDENT' ");
    $row_student = $queryStudents->fetch_assoc();


    // ? INSERT NOTI_STUDENT
    $queryNotiStudent = $link->query("INSERT INTO `noti_student`(`h_name`, `user_i`, `user_i_l`, `user_i_mail`, `user_r`, `date_`, `state`, `confirmed`, `des`) VALUES ('NULL', '$agentData[name]', '$agentData[l_name]', '$usuario', '$row_student[mail_s]', '$date', '0', '0', 'Reassigned')");


    // ? INSERT NOTIFICATION
    $queryNotification = $link->query("INSERT INTO notification (`user_i`, `user_i_l`, `user_i_mail`, `start`, `end_`, `bedrooms`, `color`, `user_r`, `date_`, `state`, `confirmed`, `title`, `des`, `report_s`, `reserve_h`, `status`, `extend`) VALUES ('$agentData[name]', '$agentData[l_name]', '$usuario', 'NULL', 'NULL', 'NULL', 'NULL', '$agencymail', '$date', '0', '0', 'New Houses Assigned', 'NULL', '$row_student[mail_s]', 'NULL', 'NULL', 'NULL') ");


    // ? INSERT WEB MASTER
    $queryWebmaster = $link->query("INSERT INTO webmaster (`user`, `activity`, `dates`, `edit_user`, `id_m`, `report_s`, `reason`) VALUES ('$usuario', 'New Houses Assigned', '$date', '$row_student[mail_s]', '$agentData[id_m]', 'NULL', 'NULL')");


    // ? UPDATE PE_STUDENT
    $queryStudent = $link->query("UPDATE pe_student SET status='Student for Select House', house1='$HOUSE1', house2='$HOUSE2', house3='$HOUSE3', house4='$HOUSE4', house5='$HOUSE5' WHERE id_student = '$ID_STUDENT' ");

    echo json_encode($queryStudent);
  }



  if (strtolower($row_user['usert']) == "manager") {
    // ? SELECT MANAGER
    $queryManager = $link->query("SELECT name, l_name, mail FROM manager WHERE mail = '$usuario' ");
    $row_manager = $queryManager->fetch_assoc();

    sendAssignment($row_manager, $row_manager['mail'], $ID_STUDENT, $usuario, $link);
  } else if (strtolower($row_user['usert']) == "agent") {
    // ? SELECT AGENTS
    $queryAgents = $link->query("SELECT name, l_name FROM agents WHERE a_mail = '$usuario' ");
    $row_agent = $queryAgents->fetch_assoc();


    // ? SELECT MANAGER
    $queryManager = $link->query("SELECT mail FROM manager WHERE id_m = '$row_agent[id_m]' ");
    $row_manager = $queryManager->fetch_assoc();

    sendAssignment($row_agent, $row_manager['mail'], $ID_STUDENT, $usuario, $link);
  }
}
