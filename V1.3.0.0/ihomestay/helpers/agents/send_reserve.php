<?php
if ($_POST['homestay_id'] && $_POST['student_id'] && $_POST['room_color'] && $_POST['room_bed']) {
  require '../../../xeon.php';

  session_start();
  error_reporting(0);

  $usuario = $_SESSION['username'];


  // ? INSERT NOTI_STUDENT
  // $queryNotiStudent = $link->query("INSERT INTO noti_student (`h_name`, `user_i`, `user_i_l`, `user_i_mail`, `user_r`, `date_`, `state`, `confirmed`, `change_house`, `des`, `status`) VALUES ('NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', '0', '0', 'NULL', 'NULL', 'NULL')");


  function sendReserve($agencyData, $agentData, $usuario, $link)
  {
    $ID_HOME = $_POST['homestay_id'];
    $ID_STUDENT = $_POST['student_id'];

    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');


    // ? SELECT PE_HOME
    $queryHomeData = $link->query("SELECT mail_h FROM pe_home WHERE id_home = '$ID_HOME' ");
    $row_home = $queryHomeData->fetch_assoc();


    // ? SELECT PE_STUDENT
    $queryStudentData = $link->query("SELECT name_s, l_name_s, mail_s, firstd, lastd FROM pe_student WHERE id_student = '$ID_STUDENT' ");
    $row_student = $queryStudentData->fetch_assoc();


    $roomBed = null;
    $roomColor = $_POST['room_color'];


    if (
      $_POST['room_bed'] == 1
    ) $roomBed = "A";
    if ($_POST['room_bed'] == 2) $roomBed = "B";
    if ($_POST['room_bed'] == 3) $roomBed = "C";

    if ($roomColor == "#232159") $bedRoom = 1;
    if ($roomColor == "#982A72") $bedRoom = 2;
    if ($roomColor == "#394893") $bedRoom = 3;
    if (
      $roomColor == "#A54483"
    ) $bedRoom = 4;
    if ($roomColor == "#5D418D") $bedRoom = 5;
    if ($roomColor == "#392B84") $bedRoom = 6;
    if ($roomColor == "#B15391") $bedRoom = 7;
    if ($roomColor == "#4F177D") $bedRoom = 8;


    // ? INSERT NOTIFICATION (TO HOMESTAY)
    $queryNotificationHOMESTAY = $link->query("INSERT INTO notification (`user_i`, `user_i_l`, `user_i_mail`, `start`, `end_`, `bedrooms`, `color`, `user_r`, `date_`, `state`, `confirmed`, `title`, `des`, `report_s`, `reserve_h`, `status`, `extend`) VALUES ('$agencyData[name]', '$agencyData[l_name]', '$row_student[mail_s]', '$row_student[firstd]', '$row_student[lastd]', '$bedRoom', '$roomColor', '$row_home[mail_h]', '$date', '0', '0', 'Reservation Provider', '$roomBed', '$agentData[mail]', 'NULL', 'NULL', 'NULL') ");


    // ? INSERT NOTIFICATION (TO AGENCY)
    $queryNotificationAGENCY = $link->query("INSERT INTO notification (`user_i`, `user_i_l`, `user_i_mail`, `start`, `end_`, `bedrooms`, `color`, `user_r`, `date_`, `state`, `confirmed`, `title`, `des`, `report_s`, `reserve_h`, `status`, `extend`) VALUES ('$agentData[name]', '$agentData[l_name]', '$usuario', '$row_student[firstd]', '$row_student[lastd]', '$bedRoom', '$roomColor', '$agencyData[mail]', '$date', '0', '0', 'Reservation Provider', '$roomBed', '$row_student[mail_s]', '$row_home[mail_h]', 'NULL', 'NULL') ");


    // ? INSERT WEB MASTER
    $queryWebmaster = $link->query("INSERT INTO webmaster (`user`, `activity`, `dates`, `edit_user`, `id_m`, `report_s`, `reason`) VALUES ('$usuario', 'Reservation Provider', '$date', '$row_home[mail_h]', '$agencyData[id_m]', '$row_student[mail_s]', 'NULL')");


    // ? UPDATE STUDENT STATUS
    $queryStudent = $link->query("UPDATE pe_student SET status='Waiting Answer' WHERE id_student = '$ID_STUDENT' ");


    if (
      $queryNotificationHOMESTAY == true &&
      $queryNotificationAGENCY == true &&
      $queryStudent == true &&
      $queryWebmaster == true
    ) {
      echo json_encode("ok");
    }
  }


  // ? SELECT USERS
  $queryUsers = $link->query("SELECT usert FROM users WHERE mail = '$usuario' ");
  $row_user = $queryUsers->fetch_assoc();


  if (strtolower($row_user['usert']) == "manager") {
    // ? SELECT MANAGER
    $queryManager = $link->query("SELECT id_m, name, l_name, mail FROM manager WHERE mail = '$usuario' ");
    $row_manager = $queryManager->fetch_assoc();

    sendReserve($row_manager, $row_manager, $usuario, $link);
  } else if (strtolower($row_user['usert']) == "agent") {
    // ? SELECT AGENTS
    $queryAgent = $link->query("SELECT id_m, name, l_name, a_mail FROM agents WHERE a_mail = '$usuario' ");
    $row_agent = $queryAgent->fetch_assoc();
    $row_agent['mail'] = $row_agent['a_mail'];

    // ? SELECT MANAGER
    $queryManager = $link->query("SELECT id_m, name, l_name, mail FROM manager WHERE id_m = '$row_agent[id_m]' ");
    $row_manager = $queryManager->fetch_assoc();

    sendReserve($row_manager, $row_agent, $usuario, $link);
  }
}
