<?php error_reporting(0);?>
<link rel="stylesheet" type="text/css" href="assets/css/footer.css">
<!--*********************************************************************************************************-->
<!-- //TODO ************ FOOTER *************************************************************************************-->
<!--*********************************************************************************************************-->

<footer id="ts-footer">

  <!--//TODO MAIN FOOTER CONTENT
            =========================================================================================================-->
  <section id="ts-footer-main" class="row w-100 ml-0 d-flex justify-content-around">
    <!-- //* FIRST COLUMN FROM FOOTER -->
    <div class="col-md-4">
      <!-- // TODO LOGO IHOMESTAY  -->
      <div class="text-center p-0 m-0">
        <img src="../assets/iHomestay/logos/iHomestay_logo.png" alt="" class="logo__ihomestay">
        <h5>We are a Canadian company with headquarters in Toronto Canada</h5>
      </div>

      <h4>Follow us!</h4>
      <a target="_blank" href="https://www.facebook.com/ihomestaycanada">
        <i class="fab fa-facebook-f"></i>
        Facebok
      </a><br>
      <a target="_blank" href="https://www.instagram.com/ihome.stay/">
        <i class="fab fa-instagram"></i>
        Instagram
      </a>
    </div>

    <!-- //* SECOND COLUMN FROM FOOTER -->
    <div class="col-md-4">
      <p class="title__column column2 mb-3">Contact</p>

      <p class="address">2800 Keele St, Toronto, ON M3M 0B8</p>
      <a href="#" class="link__mail">info@ihomestaycanada.com</a>
      <p></p>
      <a href="#" class="link__number">+1 4165799541</a><br><br>
      <p class="r_ihomestay">I Homestay 2019 ® Canada <img draggable="false" role="img" class="emoji_canada" alt="🇨🇦"
          src="https://s.w.org/images/core/emoji/14.0.0/svg/1f1e8-1f1e6.svg"></p>
      <a href="https://ihomestaycanada.com/privacy-policy/" target="_blank" class="link__politics">Privacy
        Policy</a><br><br>
    </div>

    <!-- //* THIRD COLUMN FROM FOOTER -->
    <div class="col-md-4">
      <p class="title__column column3 mb-3">Links</p>

      <a href="https://ihomestaycanada.com/become-an-agent/" class="link__header">Become an
        Agent</a>
      <p></p>
      <a href="https://ihomestaycanada.com/host-a-student/" class="link__header">Become a
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        Host
      </a>
      <p></p>
      <a href="https://homebor.com/ihomestay-students-register" class="link__header">
        Apply for accommodation
      </a>
      <p></p>
      <a href="https://ihomestaycanada.com/contact-us/" class="link__header">About Us</a>
      <p></p>
      <a href="https://ihomestaycanada.com/contact-us/" class="link__header">Payments</a>

    </div>
    <!--end container-->
  </section>
  <!--end ts-footer-main-->
  <!--SECONDARY FOOTER CONTENT
        =============================================================================================================-->
  <section id="ts-footer-secondary" data-bg-pattern="../assets/img/bg-pattern-dot.png"
    style="background-image: url('../assets/img/bg-pattern-dot.png');">
    <div class="container">
      <!--Copyright-->
      <img id="white-logo" src="../assets/logos/white.png">
      <div class="ts-copyright">(C) Copyright 2021, All rights reserved</div>
      <!--Social Icons-->
      <div class="ts-footer-nav">
        <nav class="nav">
          <a href="https://www.facebook.com/homebor.platform" target="_blank" class="nav-link">
            <i class="fab fa-facebook-f" id="footer"></i>
          </a>
          <a href="https://www.twitter.com/@Homebor_Platfrm" target="_blank" class="nav-link">
            <i class="fab fa-twitter" id="footer"></i>
          </a>
          <a href="https://instagram.com/homebor.platform?utm_medium=copy_link" target="_blank" class="nav-link">
            <i class="fab fa-instagram" id="footer"></i>
          </a>
        </nav>
      </div>
      <!--end ts-footer-nav-->
    </div>
    <!--end container-->
  </section>
  <!--end ts-footer-secondary-->
</footer>
<!--end #ts-footer-->