<?php
require('../../fpdf/fpdf.php');
include '../../../xeon.php';

define('Tag',chr(149));
define('Tag2',chr(155));


session_start();
$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM pe_home WHERE id_home = '$id'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

if ($usuario = $row['a_mail']) {
   
} else {
     header("location: ../index.php");
}

}

$username = $row2['name_h'].' '.$row2['l_name_h'];
$address = $row2['dir'].', '.$row2['city'];

$orgDate = $row2['db'];
$newDate = date("d-m-Y", strtotime($orgDate));

$agent = $row['name'].' '.$row['l_name'];

 
$pdf = new FPDF('p', 'mm', 'Letter');
$pdf->AddPage(); 
$pdf->SetTitle('Homestay Provider Agreement');

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',120,10,-300);


$pdf->SetFont('Arial','B',16);
$pdf->SetXY(0,50);
$pdf->Cell(0,0,"Homestay Provider Agreement 2022",0,0,'C');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,60);
$pdf->Cell(0,0,"iHomestay Inc (IH) is committed to providing students and homestay providers (HP/hosts) with positive and safe homestay experiences.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,63);
$pdf->Cell(0,0,"IH recognizes that our HPs come from a variety of cultural backgrounds. Cultural and religious backgrounds will not be the basis of any decision by IH",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,66);
$pdf->Cell(0,0,"relating to the selection of HPs. Students will be matched with families as per their personal requirements and no continuous supply of students to HPs",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,69);
$pdf->Cell(0,0,"can be guaranteed.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,75);
$pdf->Cell(0,0,"IH reserves the right to move a student from a homestay at any time. IH also reserves the right to carry out regular inspections of the premises to ensure",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,78);
$pdf->Cell(0,0,"that standards remain high and to industry expectations. HPs agree to IH providing their contact details to agents and students when placing students.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,81);
$pdf->Cell(0,0,"The HP also agrees to keep Student's/students personal information confidential and private.",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,89);
$pdf->Cell(0,0,"Homestay Provider Responsibilities",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,93);
$pdf->Cell(0,0,"Hosts are expected to:",0,0,'L');



$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,97);
$pdf->Cell(0,0,Tag." Provide police clearance for everyone 18+ and currently residing in the house",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,101);
$pdf->Cell(0,0,Tag." Not take students for monetary purposes nor discuss direct payment/money issues to the students",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,105);
$pdf->Cell(0,0,Tag2." Ensure a responsible adult member of the family is at home to meet the students when they arrive",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,108);
$pdf->Cell(0,0,Tag2." Contact IH immediately if a responsible adult will be unable to meet the student on arrival so that alternative arrangements can be made",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,111);
$pdf->Cell(0,0,"to care for the student until your return",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,114);
$pdf->Cell(0,0,Tag2." Offer a refreshment on arrival",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,117);
$pdf->Cell(0,0,Tag2." Conduct orientation about your house rules, showing a map and direction to get to school",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,120);
$pdf->Cell(0,0,Tag2." Bring the student to school on or before the 1st day of school by public transportation",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,123);
$pdf->Cell(0,0,Tag2." Respect the student's right to privacy, understanding that privacy does not mean isolation",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,126);
$pdf->Cell(0,0,Tag2." Respect and accommodate appropriately the culture, customs, language and beliefs of their student, and acknowledge their presence",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,129);
$pdf->Cell(0,0,"within the household",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,132);
$pdf->Cell(0,0,Tag2." Provide all meals as detailed in the Homestay Handbook",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,135);
$pdf->Cell(0,0,Tag2." Provide internet access for homework tasks and to allow the student to contact their family",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,138);
$pdf->Cell(0,0,Tag2." Provide a Duty of Care towards the student",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,141);
$pdf->Cell(0,0,Tag2." Read through, have an understanding of, and agree to the Safeguarding Guidance provided in the Homestay Handbook",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,144);
$pdf->Cell(0,0,Tag2." Provide the student with a secure, private, clean and (warm in winter / cool in summer) bedroom, a healthy, balanced/varied diet,",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,147);
$pdf->Cell(0,0,"laundry facilities and a supportive environment",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,150);
$pdf->Cell(0,0,Tag2." Ensure that the room offered to students is a designated room within the family home",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,153);
$pdf->Cell(0,0,Tag2." Ensure that the room offered to students is a designated room within the family home",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,156);
$pdf->Cell(0,0,Tag2." Ensure the student does not share a room with another family member or student unless specifically requested",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,159);
$pdf->Cell(0,0,Tag2." Assist the students on their departure, in line with arrangements communicated to you by IH",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,162);
$pdf->Cell(0,0,Tag." Communicate with IH immediately",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,165);
$pdf->Cell(0,0,Tag2." If there are any concerns regarding the welfare and well-being of students",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,168);
$pdf->Cell(0,0,Tag2." The student does not return to the homestay",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,171);
$pdf->Cell(0,0,Tag2." If any dispute occurs between the student and any member of the host's family",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,174);
$pdf->Cell(0,0,Tag2." If any aspect regarding living in your home has changed e.g. family/visitors/other students/change in living conditions",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,177);
$pdf->Cell(0,0,Tag2." If you are no longer able to host students either before they arrive, or during their stay",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,180);
$pdf->Cell(0,0,Tag2." If the student is absent 3 consecutive days",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,186);
$pdf->Cell(0,0,"iHomestay Inc Responsabilities",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,189);
$pdf->Cell(0,0,"iH will:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,192);
$pdf->Cell(0,0,Tag." Communicate with you regarding arrival and departures",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,195);
$pdf->Cell(0,0,Tag2." Send you confirmation of your homestay bookings in writing either by email or phone",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,198);
$pdf->Cell(0,0,Tag2." Advise you of expected arrival times",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(36,201);
$pdf->Cell(0,0,Tag2." Advise you of expected departure times",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,204);
$pdf->Cell(0,0,Tag." Pay hosts in a timely manner",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,207);
$pdf->Cell(0,0,Tag." Carry out an initial inspection visit, followed by biennial visits. In addition, the school may ask to conduct a full or shorter visit in response to a change in",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,210);
$pdf->Cell(0,0,"circumstances, such as a change in the layout of the house, or in response to feedback from students",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,216);
$pdf->Cell(0,0,"Students Under 18 Years of Age",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,219);
$pdf->Cell(0,0,Tag." IH and the HP have a duty of care to their students.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,222);
$pdf->Cell(0,0,Tag." Only students aged 16 and over can be given a door key. Students under the age of 16 years should be supervised at all times when in the homestay",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,225);
$pdf->Cell(0,0,Tag." Unless otherwise advised by IH, any student aged 17 or under must be back home by dinner time (7PM) or no later than 10PM if there are school",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,228);
$pdf->Cell(0,0,Tag." activities after class. You must call the emergency telephone if they do not return",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,231);
$pdf->Cell(0,0,Tag." Take the student to school and back home on the 1st day",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,234);
$pdf->Cell(0,0,Tag." HP must make all reasonable efforts to contact the school if they feel a student requires medical attention. The school or IH will make every effort to",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,237);
$pdf->Cell(0,0,Tag." contact the parents to inform them of the situation.",0,0,'L');

$pdf->AddPage();

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',120,10,-300);

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,49);
$pdf->Cell(0,0,"Damage/Theft/Insurance",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,53);
$pdf->Cell(0,0,Tag." IH cannot accept liability or responsibility for damage to your property caused by students",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,56);
$pdf->Cell(0,0,Tag." You should ensure you have household insurance that covers accidental damage by your students. It is imperative that you inform your household",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,59);
$pdf->Cell(0,0,"contents policy insurers that you have paying guests/students in your home",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,62);
$pdf->Cell(0,0,Tag." Fair wear and tear should not be charged to students, but they may be expected to pay for any damage they may have caused through carelessness. In",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,65);
$pdf->Cell(0,0,"all cases the HP should liaise with the school first",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,68);
$pdf->Cell(0,0,Tag." In cases of dispute, IH will be willing to arbitrate and should be contacted at an early stage, before the student leaves",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,74);
$pdf->Cell(0,0,"Payments & Cancellations",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,77);
$pdf->Cell(0,0,Tag." Payment is made bi-weekly following the iHomestay Inc Payroll Calendar (please see the separate document)",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,80);
$pdf->Cell(0,0,Tag." Payment is made bi-weekly following the iHomestay Inc Payroll Calendar (please see the separate document)",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,83);
$pdf->Cell(0,0,Tag." Payments are calculated on a Saturday-Saturday or Sun-Sun basis. Payments will be pro-rated on a nightly rate (27 nights or 28 days) should your",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,86);
$pdf->Cell(0,0,Tag." student depart on a day other than initially agreed",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,89);
$pdf->Cell(0,0,Tag." Hosts must not collect any accommodation fees direct from students without prior agreement from Ihomestay Inc",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,92);
$pdf->Cell(0,0,Tag." We will require 2 weeks of notice if you want to cancel hosting a student",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,98);
$pdf->Cell(0,0,"Legal Requirements",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,102);
$pdf->Cell(0,0,Tag." SH process host's personal data in accordance with the General Data Protection Regulation (GDPR) and the CEG Privacy Notice. For example,",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,105);
$pdf->Cell(0,0,"host's personal data will be used to produce pre-arrival documents for students.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,108);
$pdf->Cell(0,0,Tag." Full details of the personal data collected and processed by iHomestay Inc, the purposes for which it is collected and the legal basis for doing so",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,111);
$pdf->Cell(0,0,"are contained in the iHomestay Inc.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,114);
$pdf->Cell(0,0,Tag." IH store computer records of hosts in accordance with the Data Protection Act",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,117);
$pdf->Cell(0,0,Tag." Home Insurance. We advise that all homestay providers ensure that their insurance policy covers accidental damage by paying guests.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,120);
$pdf->Cell(0,0,"IHOMESTAY INC will not be held responsible for any such damages should they occur whilst hosting one of our students",0,0,'L');

$pdf->SetFont('Arial','B',7);
$pdf->SetXY(23,127);
$pdf->Cell(0,0,"I/We understand that IHOMESTAY INC is bringing the highest level of homestay support services to facilitate successful",0,0,'L');

$pdf->SetFont('Arial','B',7);
$pdf->SetXY(23,130);
$pdf->Cell(0,0,"cultural exchanges and agree to the above conditions relating to the provision of Homestay arrangements as a host",0,0,'L');

$pdf->SetFont('Arial','B',7);
$pdf->SetXY(23,133);
$pdf->Cell(0,0,"for ihomestay Inc.",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,152);
$pdf->Cell(0,0,"Homestay Host(s) Name(s) (Printed):",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,153);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,152);
$pdf->Cell(0,0,$username,0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,170);
$pdf->Cell(0,0,"Homestay Host(s) Signature(s):",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,171);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,179);
$pdf->Cell(0,0,"Date:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,180);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');


$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,185);
$pdf->Cell(0,0,"Address:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,186);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,185);
$pdf->Cell(0,0,$row2['dir'],0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,190);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,189);
$pdf->Cell(0,0,$row2['city'],0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,190);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,194);
$pdf->Cell(0,0,$row2['state'],0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,194);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,197);
$pdf->Cell(0,0,$row2['p_code'],0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,198);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');


$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,212);
$pdf->Cell(0,0,"Ihomestay Canada Staff Member:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,213);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(67,212);
$pdf->Cell(0,0,$agent,0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,225);
$pdf->Cell(0,0,"Signature:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,226);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(23,235);
$pdf->Cell(0,0,"Date:",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->SetXY(66,236);
$pdf->Cell(0,0,"____________________________________________",0,0,'L');

$pdf->SetFont('Arial','B',9);
$pdf->SetXY(23,255);
$pdf->Cell(0,0,"Please retain this copy for your records.",0,0,'L');




$pdf->Output();
?>