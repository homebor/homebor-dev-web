<?php 
include '../../xeon.php';
session_start();


$usuario = $_SESSION['username'];




$e = ' ';




/* Notifications */

$age =  $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
$a = $age->fetch_assoc();

$man = $link->query("SELECT * FROM manager WHERE id_m = '$a[id_m]' ");
$m=$man->fetch_assoc();

$query2 = $link->query("SELECT * FROM notification WHERE user_r = '$m[mail]' AND report_s != 'NULL' ORDER BY date_ DESC");
$q = mysqli_num_rows($query2);


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

if ($row5['usert'] != 'Agent') {
    header("location: ../logout.php"); 
}

?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/reports1.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Reports</title>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>

<body style="background-image: url('../assets/img/wallpaper.jpg'); ">

  <!--HEADER ============================================================================-->
  <?php 
        include 'header.php';
    ?>

  <div class="line__top"></div>

  <main class="main__div">

    <div class="form__group__reports__div">
      <div class="col-md-4 form__group__reports__details">
        <div class="form__group__reports__titles reports_right">
          <h2 class="form__group__reports__title">Reports</h2>
          <!--<a href="#" onclick="newReport()" class="form__group_reports_add">+</a>-->
        </div>

        <div class="form__goup__div__search__report">
          <label for="search_box" class="fa fa-search label__search"></label>
          <input type="text" name="search_box" id="search_box" class="input__search" placeholder="Search a Report">
        </div>

        <div class="form__group__reports__div2">

          <div class="form__group__reports__content content1"></div>
          <div class="form__group__reports__content2 content2"></div>

        </div>
      </div>

      <div class="col-md-8 form__group__reports__description">

      </div>
    </div>

  </main>

  <input type="hidden" id="mail_u" value="<?php echo $usuario ?>">


  <script>
  function newReport() {
    $('#view').modal("show");
  }

  $('.content2').hide();

  $('#search_box').keyup(function() {

    if ($('#search_box').val()) {

      $('.content1').hide();
      $('.content2').show();

      let search = $('#search_box').val();

      var searchLength = search.length;

      $.ajax({
        url: 'report_search.php',
        type: 'POST',
        data: {
          search,
        },
        success: function(response) {
          let reports = JSON.parse(response);
          let template = '';

          reports.forEach(report => {

            if (report.view_a == '0') {
              template += `
                                    <button class="reports div_new_report" id="reports" onclick="viewRep(${report.id_not})">

                                    <div class="new__report"></div>

                                        <p class="date_report new_report">${report.date}</p>
                                `;
            } else {
              template += `
                                    <button class="reports div_read_report" id="reports" onclick="viewRep(${report.id_not})">

                                        <p class="date_report">${report.date}</p>
                                `;
            }

            template += `
                                        <input type="hidden" name="id_not3" value="${report.id_not}" />
                                        <div class="col-2 p-0 img_profile_photo">
                                    `;
            template += `
                                            <img src="../${report.phome}" class="photo" alt="" title="${report.h_name}">
                                            <img src="../${report.photo_s}" class="photo_stu_rep" alt="" title="${report.names}">

                                        </div>
                                    `;

            if (report.view_a == '0') {
              template += `
                                        <div class="col-10 mt-auto mb-auto description_rep ">
                                            <p class="reports__content__title new_report"> ${report.h_name} </p>
                                            <p class="reports__content read"><b>${report.title}</b></p>
                                        `;
            } else {
              template += `
                                        <div class="col-10 mt-auto mb-auto description_rep">
                                            <p class="reports__content__title read_report"> ${report.h_name} </p>
                                            <p class="reports__content read">${report.title}</p>
                                        `;
            }
            template += `
                                            <div class="status">
                                            `;

            if (report.status == 'Active') {

              template += `
                                                        <p class="p-0 m-0 status_active"> &#9724;${report.status}</p>
                                                    `;
            } else {

              template += `
                                                        <p class="p-0 m-0 status_close"> &#9724;${report.status}</p>
                                                    `;
            }

            template += `
                                            </div>
                                        </div>
                                    
                                    </button>

                                    
                                `;

          });

          $('.content2').html(template);

        }

      });

    } else {
      $('.content1').show();
      $('.content2').hide();
    }

  });

  $(document).on('click', '.page-link', function() {
    var page = $(this).data('page_number');
    var query = $('#search_box').val();
    load_data(page, query);
  });

  function report_list() {

    var mail_a = $('#mail_u').val();

    $.ajax({
      url: "report_list.php",
      method: "POST",
      data: {
        mail_a
      },
      success: function(listrep) {
        let students = JSON.parse(listrep);
        let template = '';

        students.forEach(student => {


          if (student.view_a == '0') {
            template += `
                                    <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                                    <div class="new__report"></div>

                                        <p class="date_report new_report">${student.date}</p>
                                `;
          } else {
            template += `
                                    <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                        <p class="date_report ">${student.date}</p>
                                `;
          }

          template += `
                                        <div class="col-2 p-0 img_profile_photo">
                                    `;
          template += `
                                            <img src="../${student.phome}" class="photo" alt="">
                                            <img src="../${student.photo_s}" class="photo_stu_rep" alt="">

                                        </div>
                                    `;

          if (student.view_a == '0') {
            template += `
                                        <div class="col-10 mt-auto mb-auto description_rep ">
                                            <p class="reports__content__title new_report"> ${student.h_name}</p>
                                            <p class="reports__content read"><b>${student.title}</b></p>
                                        `;
          } else {
            template += `
                                        <div class="col-10 mt-auto mb-auto description_rep">
                                            <p class="reports__content__title read_report"> ${student.h_name}</p>
                                            <p class="reports__content read">${student.title}</p>
                                        `;
          }
          template += `
                                            <div class="status">
                                            `;

          if (student.status == 'Active') {

            template += `
                                                        <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                                    `;
          } else {

            template += `
                                                        <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                                    `;
          }

          template += `
                                            </div>
                                        </div>
                                    
                                    </button>
                                `;
        });

        $('.form__group__reports__content').html(template);

      }
    });

  }


  setInterval(function() {

    report_list();

  }, 1000); //Actualizo cada 3 segundos


  var id_s = $('#id_s').val();


  function viewRep($s) {
    var id = $s;



    $.ajax({
      url: "feed_reports.php",
      method: "POST",
      data: {
        id,
        id_s
      },
      success: function(data) {
        $('.form__group__reports__description').html(data);

        $('.reports__content__title').style.fontWeight = "300";

      }
    });

    var mail_a = $('#mail_u').val();

    $.ajax({
      url: "report_list.php",
      method: "POST",
      data: {
        mail_a
      },
      success: function(listrep) {
        let students = JSON.parse(listrep);
        let template = '';

        students.forEach(student => {

          if (student.view_a == '0') {
            template += `
                            <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                            <div class="new__report"></div>

                                <p class="date_report new_report">${student.date}</p>
                        `;
          } else {
            template += `
                            <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                <p class="date_report ">${student.date}</p>
                        `;
          }

          template += `
                                <div class="col-2 p-0 img_profile_photo">
                            `;
          template += `
                                    <img src="../${student.phome}" class="photo" alt="">
                                    <img src="../${student.photo_s}" class="photo_stu_rep" alt="">

                                </div>
                            `;

          if (student.view_a == '0') {
            template += `
                                <div class="col-10 mt-auto mb-auto description_rep ">
                                    <p class="reports__content__title new_report"> ${student.h_name}</p>
                                    <p class="reports__content read"> <b>${student.title}</b></p>
                                `;
          } else {
            template += `
                                <div class="col-10 mt-auto mb-auto description_rep">
                                    <p class="reports__content__title read_report"> ${student.h_name}</p>
                                    <p class="reports__content read"> ${student.title}</p>
                                `;
          }
          template += `
                                    <div class="status">
                                    `;

          if (student.status == 'Active') {

            template += `
                                                <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                            `;
          } else {

            template += `
                                                <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                            `;
          }

          template += `
                                    </div>
                                </div>

                            
                            </button>
                        `;
          template += `
                        <div class="modal fade" id="view" tabindex="-1" role="dialog">
   
                            <div class="modal-dialog" role="document">
                                <form class="mt-3" action="close_report.php" method="POST" autocomplete="off" enctype="multipart/form-data">
                                    <div class="col-xl-7 modal-content report_div d-block ml-auto mr-auto p-0">
                                        <div class="modal-header header-report">
                                            <h4 class="m-0 p-0 ml-3 title_new_report">Close Report</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white"><span style="color: white" aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="content_report">
                                            <div class="container cont_report pt-3 pb-0">
                                                
                                                <div class="col-sm-12 student_reported mt-4 mb-4">
                                                    <label class="title_student_report mr-2" for="title_rep">Report title:</label>
                                                    <select name="close_title" class="custom-select title-rep" id="title-rep">
                                                        <option disabled selected hidden="option"> - Select Title - </option>
                                                        <option value="Keep Reservation" class="option"> Keep Reservation </option>
                                                        <option value="Delete Reservation" class="option"> Delete Reservation </option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-12 p-0 " style="max-height: 300px">
                                                    <div class="row m-0">
                                                        <div class="col-sm-3 image_report p-0">

                                                            <div class="carousel-item active p-0" >
                                                            
                                                            <img id="preview-ii" class="add-photo" style="margin-top: 5%; margin-bottom: 2%;">

                                                            <input type="file" name="report_image" id="file-ii" accept="image/*" onchange="previewImageii();" style="display: none" >


                                                            <label for="file-ii" class="photo-add" id="label-ii"> <p class="form__group-l_title text-center"> Add Report Image </p> </label>

                                                            <label for="file-ii" class="add-photo-i fa fa-pencil-alt" id="label-ii-i" style="display:none;" title="Change Frontage Photo"></label>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-9 comment_report ">
                                                            <textarea rows="6" class="input-borderless" name="des_close" id="des" placeholder="Describe the problem. No special characters."></textarea>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="id_stu" value="${student.id_student}" >
                                                    <input type="hidden" name="id_not" value="${student.id_not}">
                                                    <input type="hidden" name="title" value="${student.title}">
                                                </div>

                                            </div>
                                        </div>

                                        
                                        <div class="col-sm-12 div_button" align="right">
                                            <button type="submit" class="button_send_report" id="notify" onclick="report()">Close Report</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            </div>
                        `;

        });

        $('.form__group__reports__content').html(template);

      }
    });




  }
  </script>

  <script>
  function previewImageii() {
    var file = document.getElementById("file-ii").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview-ii").setAttribute("src", event.target.result);
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-ii");
      var label_1_1 = document.getElementById("label-ii-i");

      label_1.style.display = "none";
      label_1_1.style.display = "inline";

    }

  }
  </script>








  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>



  <footer id="ts-footer" style="margin-top: 15em;">

    <!--MAIN FOOTER CONTENT
            =========================================================================================================-->
    <section id="ts-footer-main">
      <div class="container">
        <div class="row">
          <!--Brand and description-->
          <div class="col-md-6" style="display: block;">
            <a href="index.php" class="brand">
              <img src="../assets/logos/page.png">
            </a>
            <p class="mb-4" style="color: black;">
              We are a technological platform that connects quality homestay families with international students
              offering them the best accommodation options in their destination city.
            </p>
            <a id="contact" href="#" class="btn btn-outline-dark mb-4">Contact Us</a>
          </div>
          <!--Navigation-->
          <div id="nav-footer" class="col-md-2">
            <h4 style="color: #982A72;">Navigation</h4>
            <nav class="nav flex-row flex-md-column mb-4">
              <a href="index.php" class="nav-link" style="color: #982A72;">Calendar</a>
              <a href="#" class="nav-link" style="color: #982A72;">Rooms</a>
              <a href="edit-property.php" class="nav-link" style="color: #982A72;">Edit Property</a>
              <a href="#" class="nav-link" style="color: #982A72;">Notifications</a>
              <a href="homestay.php" class="nav-link" style="color: #982A72;">Profile</a>
              <a href="../logout.php" class="nav-link" style="color: #982A72;">Logout</a>
            </nav>
          </div>
          <!--Contact Info-->
          <div id="con-footer" class="col-md-4">
            <h4 style="color: #982A72;">Contact</h4>
            <address class="ts-text-color-light">
              Sebi Battaglia
              <br>
              Venezuela, Caracas
              <br>
              <strong>Email: </strong>
              <a href="#" class="btn-link">office@xeon.com</a>
              <br>
              <strong>Phone:</strong>
              +1 215-606-0391
              <br>
              <strong>Skype: </strong>
              real.estate1
            </address>
          </div>

        </div>
        <!--end row-->
      </div>
      <!--end container-->
    </section>
    <!--end ts-footer-main-->
    <!--SECONDARY FOOTER CONTENT
        =============================================================================================================-->
    <section id="ts-footer-secondary" data-bg-pattern="assets/img/bg-pattern-dot.png" style="background-color: rgb(93,65,141);
background-color: linear-gradient(180deg, rgba(93,65,141,1) 3%, rgba(65,43,132,1) 29%, rgba(57,43,132,1) 55%);">
      <div class="container">
        <!--Copyright-->
        <img id="white-logo" src="../assets/logos/white.png" style="width: 150px;">
        <div class="ts-copyright" style="color: white;">(C) 2019 Xeon Developers, All rights reserved</div>
        <!--Social Icons-->
        <div class="ts-footer-nav">
          <nav class="nav">
            <a href="#" class="nav-link">
              <i class="fab fa-facebook-f" style="color: white;"></i>
            </a>
            <a href="#" class="nav-link">
              <i class="fab fa-twitter" style="color: white;"></i>
            </a>
            <a href="#" class="nav-link">
              <i class="fab fa-pinterest-p" style="color: white;"></i>
            </a>
            <a href="#" class="nav-link">
              <i class="fab fa-youtube" style="color: white;"></i>
            </a>
          </nav>
        </div>
        <!--end ts-footer-nav-->
      </div>
      <!--end container-->
    </section>
    <!--end ts-footer-secondary-->
  </footer>
  <!--end #ts-footer-->

</body>

</html>