<?php 

session_start();
include '../../xeon.php';
$usuario = $_SESSION['username'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');

/* TODO: VARIABLES POST */
$id_student = $_POST['id_student'];
$id_room = substr($_POST['id_room'], 0, 6);
$id_h = $_POST['id_h'];

$room_bed = substr($_POST['id_room'], 6);

if($id_room == 'Room 1') {
  $color = '#232159'; 
  $room_e = 'room1';
  $room = '1';
} else if($id_room == 'Room 2') {
  $color = '#982A72'; 
  $room_e = 'room2';
  $room = '2';
}else if($id_room == 'Room 3') {
  $color = '#394893'; 
  $room_e = 'room3';
  $room = '3';
}else if($id_room == 'Room 4') {
  $color = '#A54483'; 
  $room_e = 'room4';
  $room = '4';
}else if($id_room == 'Room 5') {
  $color = '#5D418D'; 
  $room_e = 'room5';
  $room = '5';
}else if($id_room == 'Room 6') {
  $color = '#392B84'; 
  $room_e = 'room6';
  $room = '6';
}else if($id_room == 'Room 7') {
  $color = '#B15391'; 
  $room_e = 'room7';
  $room = '7';
}else if($id_room == 'Room 8') {
  $color = '#4f177D'; 
  $room_e = 'room8';
  $room = '8';
}

/* TODO: SEARCH IN EVENTS */


/* TODO: QUERIES OF COORDINATOR, AGENTS AND STUDENTS */

$coordinatorQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
$rowCoordinator = $coordinatorQuery->fetch_assoc();

$managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$rowCoordinator[id_m]'");
$rowManager = $managerQuery->fetch_assoc(); 

$studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$id_student'");
$rowStudent = $studentQuery->fetch_assoc();

$homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$id_h'");
$rowHomestay = $homestayQuery->fetch_assoc();

$roomHomestayQuery = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$rowHomestay[mail_h]' and room.id_home = pe_home.id_home");
$rowRoomHomestay=$roomHomestayQuery->fetch_assoc();

$events = $link->query("SELECT * FROM events WHERE email = '$rowHomestay[mail_h]' AND mail_s = '$rowStudent[mail_s]' AND status = 'Active'");
$nEvents = mysqli_num_rows($events);

if($nEvents < '1'){

  /* TODO: VARIABLES TO USE */

  /* MANAGER */

  $m_name = $rowManager['name'];
  $m_l_name = $rowManager['l_name'];
  $m_mail = $rowManager['mail'];
  $m_id_m = $rowManager['id_m'];

  if(strpos($rowManager['a_name'], " ")){
    $m_a_name = strstr($rowManager['a_name'], ' ', true);
    $m_l_a_name = strstr($rowManager['a_name'], ' ');
  }else{
    $m_a_name = $rowManager['a_name'];
    $m_l_a_name = "";
  }



  /* STUDENT */

  $s_firstd = $rowStudent['firstd'];
  $s_lastd = $rowStudent['lastd'];
  $s_mail = $rowStudent['mail_s'];
  $s_name = $rowStudent['name_s'];
  $s_l_name = $rowStudent['l_name_s'];
  $s_fulllnames = $rowStudent['name_s'] . ' ' . $rowStudent['l_name_s'];

  if($s_lastd > $date) {
    $status = 'Active';

    $updateStudent = "UPDATE pe_student SET status = 'Homestay Found' WHERE id_student = '$id_student'";
    $result_updateStudent = $link->query($updateStudent);
  }
  else $status = 'Disabled';

  /* HOMESTAY */

  $h_id_home = $rowHomestay['id_home'];
  $h_mail_h = $rowHomestay['mail_h'];
  $h_name_h = $rowHomestay['h_name'];
  $h_name = $rowHomestay['name_h'];
  $h_l_name = $rowHomestay['l_name_h'];

  $type1 = $rowRoomHomestay['type1'];
  $type2 = $rowRoomHomestay['type2'];
  $type3 = $rowRoomHomestay['type3'];
  $type4 = $rowRoomHomestay['type4'];
  $type5 = $rowRoomHomestay['type5'];
  $type6 = $rowRoomHomestay['type6'];
  $type7 = $rowRoomHomestay['type7'];
  $type8 = $rowRoomHomestay['type8'];

  $reservations1 = $rowRoomHomestay['reservations1'];
  $reservations2 = $rowRoomHomestay['reservations2'];
  $reservations3 = $rowRoomHomestay['reservations3'];
  $reservations4 = $rowRoomHomestay['reservations4'];
  $reservations5 = $rowRoomHomestay['reservations5'];
  $reservations6 = $rowRoomHomestay['reservations6'];
  $reservations7 = $rowRoomHomestay['reservations7'];
  $reservations8 = $rowRoomHomestay['reservations8'];



  // TODO TITLE

  $title = 'Reservation Provider';

  // TODO SEARCH ACTIVE REQUEST IN NOTIFICATION

  $searchNotification = $link->query("SELECT * FROM notification WHERE user_i_mail = '$s_mail' AND title = 'Reservation Request' AND confirmed = '0'");
  $resultSearch = mysqli_num_rows($searchNotification);

  if($resultSearch > 0){
    $updateNotification = $link->query("UPDATE notification SET state = '1', confirmed = '1' WHERE user_i_mail = '$s_mail' AND title = 'Reservation Request' AND confirmed = '0'");
  }
  
  // TODO INSERT

  $save_notification = "INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, report_s, reserve_h, status, extend) VALUES('$m_a_name', '$m_l_a_name', '$s_mail', '$s_firstd', '$s_lastd', '$room', '$color', '$h_mail_h', '$date', '0', '0', '$title', 'NULL', '$h_mail_h', 'NULL', 'No')";
  $result_save_notification = $link->query($save_notification);

  $save_noti_student = "INSERT INTO noti_student (h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, des) VALUES('$h_name_h', '$h_name', '$h_l_name', '$h_mail_h', '$s_mail', '$date', '0', '0', 'Student Confirmed Provider')";
  $result_save_noti_student = $link->query($save_noti_student);

  $save_events = "INSERT INTO events (title, start, startingDay, end, endingDay, color, room_e, bed, email, mail_s, height, id_m, status) VALUES ('$s_fulllnames', '$s_firstd', 'true', '$s_lastd', 'true', '$color', '$room_e', '$room_bed', '$h_mail_h', '$s_mail', '80', '$m_id_m', '$status')";
  $result_save_events = $link->query($save_events);

  if($status == 'Active'){

    if($room == '1'){
      if($room_bed == 'A') $availability = 'date1';
      if($room_bed == 'B') $availability = 'date1_2';
      if($room_bed == 'C') $availability = 'date1_3';

      $reservations = 'reservations1';
      $num_reserve = $reservations1 + 1;
    }
    if($room == '2'){
      if($room_bed == 'A') $availability = 'date2';
      if($room_bed == 'B') $availability = 'date2_2';
      if($room_bed == 'C') $availability = 'date2_3';

      $reservations = 'reservations2';
      $num_reserve = $reservations2 + 1;
    }
    if($room == '3'){
      if($room_bed == 'A') $availability = 'date3';
      if($room_bed == 'B') $availability = 'date3_2';
      if($room_bed == 'C') $availability = 'date3_3';

      $reservations = 'reservations3';
      $num_reserve = $reservations3 + 1;
    }
    if($room == '4'){
      if($room_bed == 'A') $availability = 'date4';
      if($room_bed == 'B') $availability = 'date4_2';
      if($room_bed == 'C') $availability = 'date4_3';

      $reservations = 'reservations4';
      $num_reserve = $reservations4 + 1;
    }
    if($room == '5'){
      if($room_bed == 'A') $availability = 'date5';
      if($room_bed == 'B') $availability = 'date5_2';
      if($room_bed == 'C') $availability = 'date5_3';

      $reservations = 'reservations5';
      $num_reserve = $reservations5 + 1;
    }
    if($room == '6'){
      if($room_bed == 'A') $availability = 'date6';
      if($room_bed == 'B') $availability = 'date6_2';
      if($room_bed == 'C') $availability = 'date6_3';

      $reservations = 'reservations6';
      $num_reserve = $reservations6 + 1;
    }
    if($room == '7'){
      if($room_bed == 'A') $availability = 'date7';
      if($room_bed == 'B') $availability = 'date7_2';
      if($room_bed == 'C') $availability = 'date7_3';

      $reservations = 'reservations7';
      $num_reserve = $reservations7 + 1;
    }
    if($room == '8'){
      if($room_bed == 'A') $availability = 'date8';
      if($room_bed == 'B') $availability = 'date8_2';
      if($room_bed == 'C') $availability = 'date8_3';

      $reservations = 'reservations8';
      $num_reserve = $reservations8 + 1;
    }

    $updateRoom = $link->query("UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$h_mail_h' SET $reservations = '$num_reserve', $availability = 'Occupied'");
  }


  $jsonMessage = array(
    'status' => 'Registered',
    'id_home' => $id_h,
    'num' => $resultSearch
  );

}else{

  $jsonMessage = array(
    'status' => 'Already',
    'id_home' => 'Empty',
  );

}

  echo json_encode($jsonMessage);



?>