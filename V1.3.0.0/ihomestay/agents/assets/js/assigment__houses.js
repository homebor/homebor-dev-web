var btn = document.getElementById('submit');



// Evento que se ejecuta al soltar una tecla en el input

$("#cantidad").keydown(function(){

$("input[type=checkbox]").prop('checked', false);

$("#seleccionados").html("0");

});



// Evento que se ejecuta al pulsar en un checkbox

$("input[type=checkbox]").change(function(){



// Cogemos el elemento actual

var elemento=this;

var contador=0;



// Recorremos todos los checkbox para contar los que estan seleccionados

$("input[type=checkbox]").each(function(){

   if($(this).is(":checked"))

       contador++;

       

});



var cantidadMaxima=parseInt($("#cantidad").val()) || 0;


var submit = document.getElementById('submit');
var submit3 = document.getElementById('submit3');



// Comprovamos si supera la cantidad máxima indicada

if(contador>cantidadMaxima)

{
   btn.disabled = false;


   
   submit.style.display="inline";
   submit3.style.display="none";

   setTimeout(() => {
       $("#house_much").fadeIn("slow");

       setTimeout(() => {
           $("#house_much").fadeOut("slow");

       }, 5000)
   }, 100);

   $('#close').on("click", function close(){
       event.preventDefault();
       $("#house_much").fadeOut("slow");
   });
   document.getElementById("contentp").innerHTML = "There are many selected houses (max. 5 houses).";
   document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
   document.getElementById("contentp").style.marginTop  = "auto";
   document.getElementById("contentp").style.marginBottom  = "auto";
   document.getElementById("contentp").style.color = "#000";



   // Desmarcamos el ultimo elemento

   $(elemento).prop('checked', false);

   
   

   contador--;

} else if(contador == '0'){

   btn.disabled = true;
   document.getElementById('submit').classList.add('disabled');

   submit.style.display="none";
   submit3.style.display="inline";
   

   setTimeout(() => {
       $("#house_much").fadeIn("slow");

       setTimeout(() => {
           $("#house_much").fadeOut("slow");

       }, 5000)
   }, 100);

   $('#close').on("click", function close(){
       event.preventDefault();
       $("#house_ass").fadeOut("slow");
   });
   document.getElementById("contentp").innerHTML = "Select at least one House";
   document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
   document.getElementById("contentp").style.marginTop  = "auto";
   document.getElementById("contentp").style.marginBottom  = "auto";
   document.getElementById("contentp").style.color = "#000";


}else if(contador > '0' && contador < cantidadMaxima){
   btn.disabled = false;
   document.getElementById('submit').classList.remove('disabled');

   submit.style.display="inline";
   submit3.style.display="none";

   
}


});

function saveAssign(){

    const formulary = document.getElementById('houses');


  


   formulary.addEventListener('submit', (e) =>{
       e.preventDefault();

       $.ajax({
           url: 'assignament_edit.php',
           type: 'POST',
           data: $('#houses').serialize(),
           success: function(response){

                let students = JSON.parse(response);

                students.forEach(student => {
                    
                    if(student.link == 'my student'){

                        window.top.location = 'edit_students';

                    }else if (student.link == 'all students'){

                        window.top.location = 'agency_students';

                    }else if (student.link == 'none'){

                        alert('Conection Error');

                    }

                });

                
                setTimeout(() => {
                   $("#house_much").fadeIn("slow");
   
                   setTimeout(() => {
                       $("#house_much").fadeOut("slow");
   
                   }, 5000)
                }, 100);
               
                $('#close').on("click", function close(){
                    event.preventDefault();
                    $("#house_much").fadeOut("slow");
                });
                document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(5, 112, 28)";
                document.getElementById("contentp").style.marginTop  = "auto";
                document.getElementById("contentp").style.marginBottom  = "auto";
                document.getElementById("contentp").style.color = "#000";
                document.getElementById("contentp").innerHTML = "Student Assigned Houses";

                submit.style.display="none";
                submit3.style.display="inline";

               
           }
       });

   })

}

btn.disabled = false;