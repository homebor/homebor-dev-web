"use strict";
// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";
import validateInput from "../../../controllers/validation.js";
import { MAP } from "./map.js";

// TODO VARIABLES
const D = document;
const W = window;
const searchParams = new URLSearchParams(W.location.search);
// ? TEMPLATES
// ? INSTANCIAS
const _Messages = new Messages();

const $_HOMESTAY = {
  // ? HOMESTAY
  $address: D.getElementById("homestay-address"),
  detailsHome: {
    $id: D.getElementById("id-home"),
    $fullName: D.getElementById("fullname-home"),
    $gender: D.getElementById("gender-home"),
    $status: D.getElementById("status-home"),
    $smokePolitics: D.getElementById("smoke-politics-home"),
  },
  $description: D.getElementById("homestay-description"),
  $academyName: D.getElementById("academy-name"),
  $genderPreference: D.getElementById("gender-preference"),
  $agePreference: D.getElementById("age-preference"),
  $addressHome: D.getElementById("address-home"),
  $city: D.getElementById("city-home"),
  $state: D.getElementById("state-home"),

  // ? HOMESTAY ROOMS
  $reserveTemplate: D.getElementById("reserve-template").content.cloneNode(true),
  $bedTemplate: D.getElementById("bed-template").content.cloneNode(true),
  $allReserves: D.getElementById("all-rooms"),
};

// ? DATA SAVED
const dataSaved = [];

// TODO FUNCIONES
export default class Homestay {
  constructor(HOMESTAY = $_HOMESTAY) {
    // ? HOMESTAY
    this.$name = HOMESTAY.$name;
    this.$address = HOMESTAY.$address;
    this.details = HOMESTAY.detailsHome;
    this.$description = HOMESTAY.$description;
    this.$academyName = HOMESTAY.$academyName;
    this.$genderPreference = HOMESTAY.$genderPreference;
    this.$agePreference = HOMESTAY.$agePreference;
    this.$addressHome = HOMESTAY.$addressHome;
    this.$city = HOMESTAY.$city;
    this.$state = HOMESTAY.$state;
    // ? HOMESTAY ROOMS
    this.$reserveTemplate = HOMESTAY.$reserveTemplate;
    this.$bedTemplate = HOMESTAY.$bedTemplate;
    this.$allReserves = HOMESTAY.$allReserves;
  }

  // ! RELLENAR HEADER DE LA HABITACIÓN
  roomHeader(roomTitle, reserveTemplate) {
    const $roomHeader = reserveTemplate.querySelector("#reserve-header");
    const $roomName = reserveTemplate.querySelector("#reserve-header h3");
    const $roomPrice = reserveTemplate.querySelector("#reserve-header p");
    const $homestayPrice = reserveTemplate.querySelector("#homestay-price");
    const $providerPrice = reserveTemplate.querySelector("#provider-price");

    $roomHeader.style.background = roomTitle.color;
    $roomName.textContent = roomTitle.name;
    $roomPrice.textContent = `CAD$ ${parseInt(roomTitle.priceHomestay) + parseInt(roomTitle.priceAgent)}`;

    $homestayPrice.value = roomTitle.priceHomestay;
    $providerPrice.value = roomTitle.priceAgent;
  }

  // ! RELLENAR IMÁGENES
  roomPhoto(roomData, images, reserveTemplate) {
    const $fragment = D.createDocumentFragment();

    const $carouselContainer = D.createElement("div");
    $carouselContainer.id = roomData.roomTitle.name.replace(" ", "-");
    $carouselContainer.className = "w-100 carousel slide";
    $carouselContainer.dataset.ride = "carousel";

    const $indicators = D.createElement("ol");
    $indicators.className = "carousel-indicators";

    const $indicator = D.createElement("li");
    $indicator.dataset.target = `#${$carouselContainer.id}`;

    const $imagesContainer = D.createElement("div");
    $imagesContainer.className = "w-100 carousel-inner";

    const $btnPrevious = D.createElement("a");
    $btnPrevious.className = "carousel-control-prev";
    $btnPrevious.dataset.slide = "prev";
    $btnPrevious.setAttribute("role", "button");
    $btnPrevious.href = `#${$carouselContainer.id}`;

    const $btnNext = D.createElement("a");
    $btnNext.className = "carousel-control-next";
    $btnNext.dataset.slide = "next";
    $btnNext.setAttribute("role", "button");
    $btnNext.href = `#${$carouselContainer.id}`;

    images.forEach((image, i) => {
      $indicator.dataset.slideTo = i;
      $indicators.appendChild($indicator.cloneNode(true));
      $indicators.firstElementChild.className = "active";

      const $imageItem = D.createElement("div");
      $imageItem.className = "w-100 carousel-item";
      const $image = D.createElement("img");
      const $photoAdd = D.createElement("label");
      const $photoAddInput = D.createElement("input");

      $photoAdd.setAttribute("for", `${roomData.roomTitle.name} Image ${i + 1}`);
      $photoAdd.className = "photo-add";

      $photoAddInput.id = `${roomData.roomTitle.name} Image ${i + 1}`;
      !image || image === "NULL" || image === "null"
        ? ($photoAdd.innerHTML = `<img src="../assets/emptys/room-empty.png" alt="">`)
        : ($photoAdd.innerHTML = `<img src="https://homebor.com/${image}" alt="">`);
      $photoAddInput.value = "";
      $photoAddInput.hidden = true;
      $photoAddInput.type = "file";

      $imageItem.appendChild($photoAdd);
      $imageItem.appendChild($photoAddInput);

      $imagesContainer.appendChild($imageItem);
      $imagesContainer.firstElementChild.classList.add("active");

      $btnPrevious.innerHTML = `<span class="carousel-control-prev-icon mr-5" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>`;
      $btnNext.innerHTML = `<span class="carousel-control-next-icon ml-5" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>`;

      $carouselContainer.appendChild($indicators);
      $carouselContainer.appendChild($imagesContainer);
      $carouselContainer.appendChild($btnPrevious);
      $carouselContainer.appendChild($btnNext);

      $fragment.appendChild($carouselContainer);
    });

    if (reserveTemplate.querySelector("#reserve-photo").children[2]) {
      reserveTemplate.querySelector("#reserve-photo").children[2].remove();
      reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
    } else reserveTemplate.querySelector("#reserve-photo").appendChild($fragment);
  }

  // ! RELLENAR CARACTERÍSTICAS
  roomFeatures(features, reserveTemplate) {
    const $typeRoom = reserveTemplate.querySelector("#type-room");
    const $roomFoodService = reserveTemplate.querySelector("#food-service");

    $typeRoom.value = features.type;
    $roomFoodService.value = features.food;
  }

  // ! ROOM BEDS INFO
  roomBedsInfo(roomBeds, roomTitle, bedTemplate, reserveTemplate) {
    const ALL_BEDS = Object.values(roomBeds.allBeds) || [];
    dataSaved.push([roomTitle.name, roomBeds.allBedsReserved]);

    const $fragment = D.createDocumentFragment();
    const $bedsContainer = reserveTemplate.querySelector("#beds-container");

    if (ALL_BEDS && ALL_BEDS instanceof Array) {
      ALL_BEDS.forEach((bed) => {
        const ROOM = bed[0];
        const TYPE_ROOM = bed[4];
        let TYPE_BED;
        bed[1] === "Bunk-bed" || bed[1] === "Bunker" ? (TYPE_BED = "Bunk") : (TYPE_BED = bed[1]);

        let BED;
        if (bed[2] === "A") BED = "Bed 1";
        if (bed[2] === "B") BED = "Bed 2";
        if (bed[2] === "C") BED = "Bed 3";
        const STATUS = bed[3];

        bedTemplate.querySelector("#bed-type").name = BED;
        bedTemplate.querySelector("#bed-letter").textContent = BED;

        const statusBed = (classBed, selectStatus, bedAdditional = false) => {
          bedTemplate.querySelector("#bed").classList.add(classBed);
          bedTemplate.querySelector("#bed select").disabled = selectStatus;

          bedTemplate.querySelectorAll(`#bed select option`).forEach((option) => {
            if (option.value === TYPE_BED) option.setAttribute("selected", "");
            else option.removeAttribute("selected");

            if (option.value === "") option.setAttribute("selected", "");
          });

          if (bedAdditional && BED === "Bed 3") {
            bedTemplate.querySelector("#bed").classList.add("more");
            bedTemplate.querySelector("#bed button").classList.add("d-none");
          }
        };

        const disableBed = () => {
          if (TYPE_ROOM === "Single" || TYPE_ROOM === "Executive") {
            bedTemplate.querySelector("#bed").classList.remove("added");
            bedTemplate.querySelector("#bed select").disabled = true;
            const $options = bedTemplate.querySelectorAll(`#bed select option`);
            $options.forEach((option) => (option.value === "" ? option.setAttribute("selected", "") : {}));
          } else if (TYPE_ROOM === "Share") {
            bedTemplate.querySelectorAll(`#bed select option`).forEach((option) => {
              option.removeAttribute("selected");
              if (option.value === "") option.setAttribute("selected", "");
            });
          }
        };

        if (TYPE_ROOM === "Single" || TYPE_ROOM === "Executive") {
          STATUS && STATUS !== "Disabled" && STATUS !== "NULL" ? statusBed("added", false) : disableBed();
        }

        if (TYPE_ROOM === "Share") {
          STATUS && STATUS !== "Disabled" && STATUS !== "NULL" ? statusBed("added", false, true) : disableBed();
        }

        let $clone = D.importNode(bedTemplate, true);

        $fragment.appendChild($clone);
      });

      // * AGREGAR CAMA
      $bedsContainer.appendChild($fragment);
    }
  }

  // ! EDIT BEDS
  editBeds(btn) {
    const $bedsContainer = btn.parentElement.parentElement.parentElement.parentElement.parentElement;
    const selectValue = btn.value;

    if (selectValue === "Single" || selectValue === "Executive") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        if (i === 1 || i === 2) {
          bed.classList.remove("added");
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = true));
          bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = ""));
        }
      });
    } else if (selectValue === "Share") {
      $bedsContainer.querySelectorAll("#bed").forEach((bed, i) => {
        bed.classList.add("added");
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = false));
        bed.querySelectorAll("#bed select").forEach((bed) => (bed.value = ""));
        if (i === 2) bed.classList.remove("more");
      });
    } else {
      $bedsContainer.querySelectorAll("#bed").forEach((bed) => bed.classList.remove("added"));
      $bedsContainer.querySelectorAll("#bed select").forEach((bed) => (bed.disabled = "false"));
    }
  }

  // ! SAVE BEDROOMS
  async saveBedrooms(room, id) {
    room.forEach(async (info) => {
      try {
        const roomName = info.querySelector("h3").textContent;
        const $images = info.querySelectorAll(".carousel-item img");
        const $imageInput = info.querySelectorAll(".carousel-item input");
        const $selects = info.querySelectorAll("select");
        const $priceHomestay = info.querySelector("#homestay-price");
        const $priceAgent = info.querySelector("#provider-price");

        const images = [];
        const readImages = () => $imageInput.forEach((input) => images.push(input.files[0]));
        await readImages();

        const roomData = new FormData();
        roomData.set("homestay_id", id);
        if (roomName && roomName[roomName.length - 1]) roomData.set("roomNumber", roomName[roomName.length - 1]);

        if ($images[0]) roomData.set("image1", images[0]);
        if ($images[1]) roomData.set("image2", images[1]);
        if ($images[2]) roomData.set("image3", images[2]);

        if ($selects && $selects[0].value) roomData.set("typeRoom", $selects[0].value);
        if ($selects && $selects[1].value) roomData.set("food", $selects[1].value);

        if ($selects[2] && $selects[2].value) roomData.set("bed1", $selects[2].value);
        if ($selects[3] && $selects[3].value) roomData.set("bed2", $selects[3].value);
        if ($selects[4] && $selects[4].value) roomData.set("bed3", $selects[4].value);

        if ($priceHomestay && $priceHomestay.value) roomData.set("priceHomestay", $priceHomestay.value);
        if ($priceAgent && $priceAgent.value) roomData.set("priceAgent", $priceAgent.value);

        const options = {
          method: "POST",
          header: { "Content-type": "application/json; charset=utf-8" },
          data: roomData,
        };

        const res = await axios("../helpers/homestay/edit_rooms.php", options);
        const data = await res.data;

        // if (D.querySelector(`[data-room="${data[0]}"]`)) {
        //   D.querySelector("#modal-dynamic").classList.add("show");
        //   D.querySelector("#modal-dynamic").style.opacity = "1";
        //   D.querySelector("#modal-dynamic").innerHTML = `
        //   <div class="d-flex flex-column text-center justify-content-center save-changes">
        //     <h4>You haven't finished the configuration of your rooms, <br>
        //     do you want to continue the process without that information?</h4>
        //     <div class="d-flex align-items-center justify-content-center">
        //       <button id="save-changes-yes" disabled class="btn w-25 mx-2 enable-room">Yes</button>
        //       <button id="save-changes-no" disabled class="btn w-25 mx-2 disable-room">No</button>
        //     </div>
        //   </div>
        //   `;

        //   D.querySelector(`[data-room="${data[0]}"]`).classList.add("required-fields");

        //   D.querySelector("#save-changes-no").addEventListener("click", (e) => {
        //     D.querySelector("#modal-dynamic").style.opacity = "0";
        //     D.querySelector("#modal-dynamic").classList.remove("show");
        //     D.querySelector("#modal-dynamic").style.cssText = "";
        //     D.querySelector(`.required-fields`).scrollIntoView({ block: "start", behavior: "smooth" });
        //   });

        //   D.querySelector("#save-changes-yes").addEventListener("click", (e) => {
        //     _Messages.showMessage(`${D.querySelector("#house-name").value} Has Been Successfully Registered`, 2);
        //     D.querySelector("#modal-dynamic").style.opacity = "0";
        //     D.querySelector("#modal-dynamic").classList.remove("show");
        // setTimeout(() => (window.location.href = "directory_homestay"), 2000);
        //   });
        // }

        // * ERROR EN LA PROGRAMACION, CLIENTE, RED O SERVIDOR
        if (data === "error") {
          _Messages.showMessage("An error occurred while saving the rooms.", 4, 6, ".rooms-container");
        }

        // * ERRORES MANEJADOS
        // if (data[1] === "no-type-room") D.querySelector(`[data-room="${data[0]}"] #type-room`).classList.add("invalid");
        // else if (data[1] === "no-shared-bed") {
        //   D.querySelectorAll(`[data-room="${data[0]}"] #bed`).forEach((bed, i) => {
        //     if (i == 2) return;
        //     bed.classList.add("no-bed");
        //     bed.dataset.noBed = "Select type bed";
        //   });
        // } else if (data[1] === "no-bed") D.querySelector(`[data-room="${data[0]}"] #bed`).classList.add("no-bed");
        // else if (data[1] === "price-homestay-invalid") {
        //   D.querySelector(`[data-room="${data[0]}"] #homestay-price`).classList.add("invalid");
        // } else if (data[1] === "price-agent-invalid") {
        //   D.querySelector(`[data-room="${data[0]}"] #provider-price`).classList.add("invalid");
        // } // * MENSAJE DE GUARDADO SATISFACTORIO
        // else if (data.status === true) {
        //   D.querySelector(`[data-room="${data.room}"]`).classList.add("saved");
        //   setTimeout(() => D.querySelector(`[data-room="${data.room}"]`).classList.remove("saved"), 4000);
        // }

        // setTimeout(() => {
        //   D.querySelector("#modal-dynamic #save-changes-yes").disabled = false;
        //   D.querySelector("#modal-dynamic #save-changes-no").disabled = false;
        // }, 800);
      } catch (error) {
        console.error("Ha ocurrido un error al guardar los cambios!:", error);
      }
    });
  }

  // ! ADD NEW ROOM
  addNewRoom(reference) {
    const $allRooms = D.getElementById("all-rooms");
    const $template = D.getElementById("reserve-template").content;

    let $lastRoomName;
    if ($allRooms.querySelector("#reserve-header")) {
      $lastRoomName = parseInt($allRooms.lastElementChild.querySelector("#reserve-header h3").textContent.slice(-1));
    } else $lastRoomName = 0;

    const $lastRoom = D.querySelector(`[data-room="Room ${$lastRoomName}"]`);

    const ADD_ROOM = () => {
      const ROOM_PREV = $lastRoomName + 1;
      let roomColor;

      if (ROOM_PREV > 0 && ROOM_PREV <= 8) {
        try {
          if (ROOM_PREV == 1) roomColor = "#232159";
          if (ROOM_PREV == 2) roomColor = "#982A72";
          if (ROOM_PREV == 3) roomColor = "#394893";
          if (ROOM_PREV == 4) roomColor = "#A54483";
          if (ROOM_PREV == 5) roomColor = "#5D418D";
          if (ROOM_PREV == 6) roomColor = "#392B84";
          if (ROOM_PREV == 7) roomColor = "#B15391";
          if (ROOM_PREV == 8) roomColor = "#4F177D";

          $template.querySelector("#reserve").dataset.room = "Room " + ROOM_PREV;
          $template.querySelector("#reserve-header").style.background = roomColor;
          $template.querySelector("#reserve-header h3").textContent = "Room " + ROOM_PREV;
          this.roomPhoto({ roomTitle: { name: "Room " + ROOM_PREV } }, [false, false, false], $template);

          const $bedHTML = `
            <div id="bed" class="mb-4 my-lg-auto rounded d-flex justify-content-center bed-info-homestay"
              data-no-bed="Select type bed">

              <div class="my-4 d-flex">
                <div class="w-100 h-50 d-flex align-items-center justify-content-center">
                  <img src="../assets/icon/food 64.png" width="25px" height="25px" alt="">
                  <select name="bed-type" id="bed-type" class="px-2" disabled required>
                    <option value="" hidden selected>Type bed</option>
                    <option value="Twin">Twin</option>
                    <option value="Bunk-bed">Bunk</option>
                    <option value="Double">Double</option>
                  </select>
                </div>

                <div class="d-flex align-items-center justify-content-center">
                  <span id="bed-letter" class="text-white">Bed</span>
                </div>
              </div>

              <button id="add-bed" class="my-3 d-none" title="Add bed">+</button>
            </div>
          `;

          $template.querySelector("#beds-container").innerHTML = `${$bedHTML}${$bedHTML}${$bedHTML}`;

          $allRooms.appendChild(D.importNode($template, true));

          return;
        } catch (error) {
          console.error("Ha ocurrido un error: " + error);
        } finally {
          if (reference !== "toInput") {
            D.querySelector("#total-rooms").value = $allRooms.querySelectorAll("#reserve").length;
          }
        }
      }
    };

    if (!$lastRoom || reference === "toInput") ADD_ROOM();
    else if (
      $lastRoom.querySelector("#type-room").value === "NULL" ||
      $lastRoom.querySelector("#bed-type").value === "NULL" ||
      (!$lastRoom.querySelector("#homestay-price").value && !$lastRoom.querySelector("#provider-price").value) ||
      ($lastRoom.querySelector("#homestay-price").value <= 0 && $lastRoom.querySelector("#provider-price").value <= 0)
    ) {
      _Messages.showMessage(
        "Please complete the information of the previous room to add more.",
        4,
        false,
        `[data-room="Room ${$lastRoomName}"]`
      );
    } else ADD_ROOM();
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? SAVE NEW HOMESTAY INFORMATION
  async saveDataHomestay(inputs, selects, textarea) {
    try {
      // ? BASIC INFORMATION
      // * VALIDAR BASIC INFORMATION
      if (
        !inputs[1].value ||
        !inputs[2].value ||
        selects[0].value === "NULL" ||
        !inputs[3].value ||
        !inputs[4].value ||
        selects[1].value === "NULL" ||
        !inputs[5].value ||
        !inputs[6].value ||
        !inputs[7].value ||
        !inputs[8].value
      ) {
        if (selects[0].value === "NULL") selects[0].classList.add("invalid");
        if (selects[1].value === "NULL") selects[1].classList.add("invalid");
        [inputs[1], inputs[2], inputs[3], inputs[4], inputs[5], inputs[6], inputs[7], inputs[8]].forEach((input) => {
          if (!input.value) input.classList.add("invalid");
        });
        _Messages.showMessage("You must fill in at least all the basic information fields.", 4, false, "h1");
        return;
      }

      // * VALIDAR NAME MAIN CONTACT Y LAST NAME
      if (!D.querySelector("#name-contact").value || !D.querySelector("#last-name-contact").value) {
        if (!D.querySelector("#name-contact").value) D.querySelector("#name-contact").classList.add("invalid");
        if (!D.querySelector("#last-name-contact").value) {
          D.querySelector("#last-name-contact").classList.add("invalid");
        }
        _Messages.showMessage(
          "You must fill in at least all the basic information fields.",
          4,
          false,
          "#responses-any-members"
        );
        return;
      }

      const $nameContact = D.querySelector("#name-contact").value;
      const $lastnameContact = D.querySelector("#last-name-contact").value;
      D.querySelector("#house-name").value = `${$lastnameContact.toUpperCase()}, ${$nameContact}`;

      const basicData = new FormData();
      basicData.set("basicInformation", true);
      basicData.set("houseName", D.querySelector("#house-name").value);
      inputs[1].value ? basicData.set("phoneNumber", inputs[1].value) : inputs[1].classList.add("invalid");
      inputs[2].value ? basicData.set("totalRooms", inputs[2].value) : inputs[2].classList.add("invalid");
      selects[0].value !== "NULL" ? basicData.set("houseType", selects[0].value) : selects[0].classList.add("invalid");
      inputs[3].value ? basicData.set("houseMail", inputs[3].value.toLowerCase()) : inputs[3].classList.add("invalid");
      inputs[4].value ? basicData.set("password", inputs[4].value) : inputs[4].classList.add("invalid");

      selects[1].value !== "NULL" ? basicData.set("mainCity", selects[1].value) : selects[1].classList.add("invalid");
      inputs[5].value ? basicData.set("address", inputs[5].value) : inputs[5].classList.add("invalid");
      inputs[6].value ? basicData.set("city", inputs[6].value) : inputs[6].classList.add("invalid");
      inputs[7].value ? basicData.set("state", inputs[7].value) : inputs[7].classList.add("invalid");
      inputs[8].value ? basicData.set("postalCode", inputs[8].value) : inputs[8].classList.add("invalid");

      const basicOptions = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: basicData,
      };

      const sendBasicData = await axios("./register_homestay.php", basicOptions);
      const resultBasicData = await sendBasicData.data;

      if (resultBasicData === "Existing user") {
        _Messages.showMessage("This email has already been registered.", 4, 6, "h1");
        D.querySelector("#house-mail").classList.add("invalid");
        return;
      } else if (resultBasicData.status !== "ok") {
        _Messages.showMessage("An error has occurred in the basic information, please check the fields.", 4, 6, "h1");
        return;
      }

      // console.log("Basic Information Enviado!", resultBasicData);

      // * BEDROOMS
      const result = await this.saveBedrooms(D.querySelectorAll("[data-room]"), resultBasicData.result.id_home);

      // * HOUSE GALLERY
      const houseGallery = new FormData();
      houseGallery.set("updateHouseGallery", true);
      houseGallery.set("homestay_id", resultBasicData.result.id_home);
      houseGallery.set("mail", resultBasicData.result.mail_h);
      if (inputs[9].files[0]) houseGallery.set("frontage", inputs[9].files[0]);
      if (inputs[10].files[0]) houseGallery.set("livingRoom", inputs[10].files[0]);
      if (inputs[11].files[0]) houseGallery.set("familyPicture", inputs[11].files[0]);
      if (inputs[12].files[0]) houseGallery.set("kitchen", inputs[12].files[0]);
      if (inputs[13].files[0]) houseGallery.set("diningRoom", inputs[13].files[0]);
      if (inputs[14].files[0]) houseGallery.set("houseArea3", inputs[14].files[0]);
      if (inputs[15].files[0]) houseGallery.set("houseArea4", inputs[15].files[0]);
      if (inputs[16].files[0]) houseGallery.set("bathroom1", inputs[16].files[0]);
      if (inputs[17].files[0]) houseGallery.set("bathroom2", inputs[17].files[0]);
      if (inputs[18].files[0]) houseGallery.set("bathroom3", inputs[18].files[0]);
      if (inputs[19].files[0]) houseGallery.set("bathroom4", inputs[19].files[0]);

      D.querySelectorAll(".carousel-item input").forEach((input) => {
        if (!input.files[0]) return;

        let room;
        const roomNumber = input.id.slice(5, 6);
        const roomImage = input.id.slice(-1);

        roomImage > 1 ? (room = `proom${roomNumber}_${roomImage}`) : (room = `proom${roomNumber}`);
        houseGallery.set(room, input.files[0]);
      });

      const houseGalleryOptions = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: houseGallery,
      };

      const sendHouseGallery = await axios("./register_homestay.php", houseGalleryOptions);
      const resultHouseGallery = await sendHouseGallery.data;
      // console.log("House Gallery Enviado!", resultHouseGallery);

      // * ADDITIONAL INFORMATION
      const additionalData = new FormData();
      additionalData.set("updateAdditionalInformation", true);
      additionalData.set("homestay_id", resultBasicData.result.id_home);
      additionalData.set("mail", resultBasicData.result.mail_h);
      additionalData.set("description", textarea.value || "NULL");
      additionalData.set("academy", selects[2].value || 0);
      additionalData.set("gender", selects[3].value || "NULL");
      additionalData.set("age", selects[4].value || "NULL");
      additionalData.set("smokers", selects[5].value || "NULL");
      additionalData.set("meals", selects[6].value || "NULL");
      additionalData.set("totalYears", inputs[20].value || "NULL");

      additionalData.set("pet", selects[7].value || "NULL");
      additionalData.set("totalPets", selects[8].value || "NULL");
      // additionalData.set("totalPets", inputs[21].value || "NULL");
      inputs.forEach((input) => {
        if (input.id === "dog") input.checked ? additionalData.set("dog", "yes") : additionalData.set("dog", "no");
        if (input.id === "cat") input.checked ? additionalData.set("cat", "yes") : additionalData.set("cat", "no");
        if (input.id === "other") {
          if (input.checked) {
            additionalData.set("other", "yes");
            additionalData.set("specifyPets", inputs[24].value);
          } else {
            additionalData.set("other", "no");
            additionalData.set("specifyPets", "NULL");
          }
        }
      });

      inputs[25].checked ? additionalData.set("vegetarians", "yes") : additionalData.set("vegetarians", "no");
      inputs[26].checked ? additionalData.set("halal", "yes") : additionalData.set("halal", "no");
      inputs[27].checked ? additionalData.set("kosher", "yes") : additionalData.set("kosher", "no");
      inputs[28].checked ? additionalData.set("lactose", "yes") : additionalData.set("lactose", "no");
      inputs[29].checked ? additionalData.set("gluten", "yes") : additionalData.set("gluten", "no");
      inputs[30].checked ? additionalData.set("pork", "yes") : additionalData.set("pork", "no");

      const additionalOptions = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: additionalData,
      };

      const sendAdditionalData = await axios("./register_homestay.php", additionalOptions);
      const resultAdditionalData = await sendAdditionalData.data;

      if (resultAdditionalData.status == "fail") {
        _Messages.showMessage(
          "An error has occurred in the additional information, please check the fields.",
          4,
          6,
          "#add-new-room"
        );
        return;
      }
      // console.log("Additional Information Enviado!", resultAdditionalData);

      // * FAMILY INFORMATION
      const familyData = new FormData();
      familyData.set("updateFamilyInformation", true);
      familyData.set("homestay_id", resultBasicData.result.id_home);
      familyData.set("mail", resultBasicData.result.mail_h);
      // ** ANY MEMBER OF YOUR FAMILY
      const setResponse = (value1, value2) => (value1 === "Yes" ? value2 : "No");
      familyData.set("allergies", setResponse(selects[9].value, inputs[31].value));
      familyData.set("medications", setResponse(selects[10].value, inputs[32].value));
      familyData.set("healthProblems", setResponse(selects[11].value, inputs[33].value));
      // ** MAIN CONTACT
      familyData.set("nameContact", inputs[34].value);
      familyData.set("lastNameContact", inputs[35].value);
      familyData.set("dateBirthContact", inputs[36].value || "NULL");
      familyData.set("genderContact", selects[12].value || "NULL");
      familyData.set("phoneContact", inputs[37].value || "NULL");
      familyData.set("occupationContact", inputs[38].value || "NULL");
      familyData.set("dateBackgroundCheckContact", inputs[39].value || "NULL");
      inputs[40].files[0]
        ? familyData.set("backgroundCheck", inputs[40].files[0])
        : familyData.set("backgroundCheck", "NULL");
      // ** FAMILY PREFERENCES
      familyData.set("totalMembers", selects[13].value || "NULL");
      familyData.set("background", inputs[41].value || "NULL");
      familyData.set("backgroundLanguage", inputs[42].value || "NULL");
      familyData.set("religion", setResponse(selects[14].value, inputs[43].value));
      familyData.set("condition", setResponse(selects[15].value, inputs[44].value));
      familyData.set("misdemeanor", setResponse(selects[16].value, inputs[45].value));
      familyData.set("finalQuestion", selects[17].value);

      const familyOptions = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: familyData,
      };

      const sendFamilyData = await axios("./register_homestay.php", familyOptions);
      const resultFamilyData = await sendFamilyData.data;
      // console.log("Family Information Enviado!", resultFamilyData);

      // * SAVE FAMILY MEMBERS
      D.querySelectorAll(".family-member").forEach(async (section, i) => {
        const familyMembers = new FormData();
        familyMembers.set("updateFamilyMembers", true);
        familyMembers.set("homestay_id", resultBasicData.result.id_home);
        familyMembers.set("mail", resultBasicData.result.mail_h);
        familyMembers.set(`member${i + 1}_name`, section.querySelector("[data-name]").value || "NULL");
        familyMembers.set(`member${i + 1}_lastName`, section.querySelector("[data-last-name]").value || "NULL");
        familyMembers.set(`member${i + 1}_dateBirth`, section.querySelector("[data-date-birth]").value || "NULL");
        familyMembers.set(`member${i + 1}_gender`, section.querySelector("[data-gender]").value || "NULL");
        familyMembers.set(`member${i + 1}_relation`, section.querySelector("[data-relation]").value || "NULL");
        familyMembers.set(`member${i + 1}_occupation`, section.querySelector("[data-occupation]").value || "NULL");
        familyMembers.set(
          `member${i + 1}_dateBackground`,
          section.querySelector("[data-date-background-check]").value || "NULL"
        );
        familyMembers.set(`member${i + 1}_backgroundCheck`, section.querySelector("[data-background-check]").files[0]);

        const familyMembersOptions = {
          method: "POST",
          header: { "Content-type": "application/json; charset=utf-8" },
          data: familyMembers,
        };

        const sendFamilyMembers = await axios("./register_homestay.php", familyMembersOptions);
        const resultFamilyMembers = await sendFamilyMembers.data;
        // console.log("Family Members Enviado!", resultFamilyMembers);
      });

      // * COMPLETE DATA (USERS AND PROPERTIE CONTROL)
      const completeData = new FormData();
      completeData.set("completeData", true);
      completeData.set("homestay_id", resultBasicData.result.id_home);
      completeData.set("mail", resultBasicData.result.mail_h);
      completeData.set("nameNotification", $nameContact); // NAME HOUSE
      completeData.set("lastnameNotification", $lastnameContact); // LASTNAME HOUSE

      // ** DATA USERS
      completeData.set("nameContact", resultFamilyData.result.nameContact);
      completeData.set("lastNameContact", resultFamilyData.result.lastNameContact);

      // ** DATA PROPERTIE CONTROL
      completeData.set("houseName", resultBasicData.result.h_name);
      completeData.set("totalRooms", resultBasicData.result.room);
      completeData.set("address", resultBasicData.result.dir);
      completeData.set("city", resultBasicData.result.city);
      completeData.set("gender", resultAdditionalData.result.gender);
      completeData.set("pet", resultAdditionalData.result.pet);
      completeData.set("age", resultAdditionalData.result.age);
      completeData.set("smokers", resultAdditionalData.result.smokers);
      completeData.set("vegetarians", resultAdditionalData.result.vegetarians);
      completeData.set("halal", resultAdditionalData.result.halal);
      completeData.set("kosher", resultAdditionalData.result.kosher);
      completeData.set("lactose", resultAdditionalData.result.lactose);
      completeData.set("gluten", resultAdditionalData.result.gluten);
      completeData.set("pork", resultAdditionalData.result.pork);

      const completeDataOptions = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: completeData,
      };

      const sendCompleteData = await axios("./register_homestay.php", completeDataOptions);
      const resultCompleteData = await sendCompleteData.data;
      // console.log("COMPLETE DATA Enviado!!", resultCompleteData);

      if (
        resultBasicData.status === "ok" &&
        resultAdditionalData.status === "ok" &&
        resultFamilyData.status === "ok" &&
        resultCompleteData.statusUser === "ok" &&
        resultCompleteData.statusControl === "ok"
      ) {
        _Messages.showMessage(`${D.querySelector("#house-name").value} has been successfully registered`, 2);
        setTimeout(() => (window.location.href = "directory_homestay"), 2000);
      } else {
        const errors = [
          resultBasicData.status,
          resultAdditionalData.status,
          resultFamilyData.status,
          resultCompleteData.statusUser,
          resultCompleteData.statusControl,
        ].filter((elem) => (elem === "ok" ? elem : []));
        throw errors;
      }
    } catch (error) {
      console.error("Error to save homestay", error);
    }
  }

  dateRangePicker() {
    const parentFamilyMembers = D.querySelector("#family-members");

    const action = {
      autoApply: true,
      singleDatePicker: true,
      showDropdowns: true,
      startDate: moment().startOf("hour"),
      maxDate: moment().startOf("hour"),
      locale: {
        format: "MM/DD/YYYY",
      },
    };

    parentFamilyMembers.querySelectorAll("input").forEach((input) => {
      if (
        input.id.includes("member") &&
        (input.id.includes("DateofBirth") || input.id.includes("DateofBackgroundCheck"))
      ) {
        $("#" + input.id).daterangepicker(action);
      }
    });
  }
}
// ?
// ?
// ?
// ?
// ?
// ?
// ? OBTENER ACADEMIAS
function getAcademys() {
  return new Promise(async (resolve, reject) => {
    try {
      const formData = new FormData();

      const OPTIONS = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("../helpers/get_all_schools.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";

      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

      data.forEach((academy, i) => {
        const $clone = D.createElement("option");
        for (const key in academy[0]) {
          $clone.textContent = `${academy[0]["name_a"]}, ${academy[0]["dir_a"]}`;
          $clone.value = academy[0]["id_ac"];
        }
        D.querySelector("#academy").appendChild($clone);
      });
    } catch (error) {
      console.error(error);
    }
    resolve(true);
  });
}
// ?
// ?
// ?
// ?
// ?
// ?
// ? MOSTRAR RESPUESTA DE MASCOTAS
function petShowResponses(response = undefined) {
  if (D.querySelector("#pets").value === "Yes") D.querySelector(".pet-response").classList.add("show");

  if (response === 1) {
    !D.querySelector(".pet-response").classList.add("response-1");
    !D.querySelector(".pet-response").classList.add("response-2");
    if (!D.querySelector("#other").checked) !D.querySelector(".pet-response").classList.remove("response-3");
  } else if (response === 2) !D.querySelector(".pet-response").classList.add("response-3");
  else if (response === undefined) {
    D.querySelector(".pet-response").classList.remove("show", "response-1", "response-2", "response-3");
  }
}
// ?
// ?
// ?
// ?
// ?
// ?
// ? MOSTRAR OTRAS RESPUESTAS
function showResponses(container, response = undefined, action = "remove") {
  const $elem = D.querySelector(container);
  const className = `response-${response}`;
  $elem.classList.add("show");
  if (response === 1) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);
  else if (response === 2) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);
  else if (response === 3) action === "add" ? $elem.classList.add(className) : $elem.classList.remove(className);

  if (
    !$elem.classList.contains("response-1") &&
    !$elem.classList.contains("response-2") &&
    !$elem.classList.contains("response-3")
  )
    $elem.classList.remove("show");
}
// ?
// ?
// ?
// ?
// ?
// ?
// ? LIMPIAR INPUTS
const cleanInputs = () => {
  D.querySelector("#house-mail").value = "";
  D.querySelector("#password").value = "";

  D.querySelector("#pets").value === "Yes" ? petShowResponses(1) : {};
  D.querySelector("#other").checked ? petShowResponses(2) : {};

  D.querySelector("#religion").value === "Yes" ? showResponses("#family-responses", 1, "add") : {};
  D.querySelector("#condition").value === "Yes" ? showResponses("#family-responses", 2, "add") : {};
  D.querySelector("#misdemeanor").value === "Yes" ? showResponses("#family-responses", 3, "add") : {};

  D.querySelector("#allergies").value === "Yes" ? showResponses("#responses-any-members", 1, "add") : {};
  D.querySelector("#take-medication").value === "Yes" ? showResponses("#responses-any-members", 2, "add") : {};
  D.querySelector("#health-problems").value === "Yes" ? showResponses("#responses-any-members", 3, "add") : {};
};
// ?
// ?
// ?
// ?
// ?
// ?
// ? AÑADIR MIEMBROS DE FAMILIA
const addMembersFamily = (e) => {
  e.preventDefault();

  const addMember = () => {
    const $template = D.getElementById("family-member-template").content;

    if (D.querySelectorAll("#family-members section").length >= 8) return;

    $template.querySelectorAll("input").forEach((input) => (input.value = ""));
    $template.querySelectorAll("select option").forEach((option) => {
      option.value === "NULL" ? option.setAttribute("selected", "") : option.removeAttribute("selected");
    });
    $template.querySelectorAll("label").forEach((label) => {
      const FORID = `member-${D.querySelectorAll(".family-member").length + 1}-${label.innerText
        .split(" ")
        .join("")
        .trim()}`;
      label.setAttribute("for", FORID);
      if (label.parentElement.parentElement.querySelector("input")) {
        label.parentElement.parentElement.querySelector("input").id = FORID;
      }
      if (label.parentElement.parentElement.querySelector("iframe")) {
        label.parentElement.parentElement.querySelector("iframe").dataset.input = FORID;
      }
    });
    const $clone = D.importNode($template, true);
    D.getElementById("family-members").appendChild($clone);

    // * DATE PICKER
    INSTANCE.dateRangePicker();
  };
  if (e.target.id === "add-member") {
    D.querySelector("#number-members").value = Number(D.querySelector("#number-members").value) + 1;
    addMember();
  } else {
    D.querySelector("#family-members").innerHTML = "";
    for (let i = 0; i < D.querySelector("#number-members").value - 1; i++) addMember();
  }
};

// ? SHOW OR HIDE PASSWORD
const showPassword = (btn) => {
  if (btn.classList.contains("fa-eye")) btn.classList.replace("fa-eye", "fa-eye-slash");
  else btn.classList.replace("fa-eye-slash", "fa-eye");

  btn.classList.contains("fa-eye-slash")
    ? D.querySelector("#password").setAttribute("type", "text")
    : D.querySelector("#password").setAttribute("type", "password");
};

// TODO INSTANCIA DE LA CLASE
const INSTANCE = new Homestay();

// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await getAcademys();
  setTimeout(() => cleanInputs(), 1000);
  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // * SHOW PASSWORD
  if (e.target.id === "see-password") showPassword(e.target);
  // * PREGUNTAS Y RESPUESTAS DE MASCOTAS CONTROL
  if (e.target.id === "other") e.target.checked ? petShowResponses(2) : petShowResponses(1);
  // * AÑADIR CAMA
  if (e.target.matches("#add-bed")) {
    const $bed = e.target.parentElement;
    const $select = e.target.parentElement.querySelector("select");

    $bed.classList.add("more");
    $select.disabled = false;
  }
  // * AÑADIR IMAGEN DE GALLERY
  if (e.target.matches(".add-photo-btn")) {
    e.preventDefault();
    if (e.target.parentElement.querySelector("h4").textContent.includes("House Area 3")) {
      D.getElementById("add-area-4").classList.remove("d-none");
      D.getElementById("house-area-4").parentElement.style.marginTop = "5rem";
    }
    if (e.target.parentElement.querySelector("h4").textContent.includes("Bathroom Photo 3")) {
      D.getElementById("add-bathroom-4").classList.remove("d-none");
    }

    e.target.parentElement.querySelector(".gallery-photo-container").classList.replace("hide", "position-relative");
    e.target.classList.add("d-none");
  }

  // * QUITAR IMAGEN DE GALERY
  if (e.target.matches("h4 .close")) {
    if (e.target.parentElement.textContent.includes("House Area 3")) {
      D.getElementById("add-area-4").classList.add("d-none");
      D.getElementById("house-area-4").classList.replace("position-relative", "hide");
      D.getElementById("house-area-4").parentElement.style.marginTop = "0rem";
    }
    if (e.target.parentElement.textContent.includes("Bathroom Photo 3")) {
      D.getElementById("add-bathroom-4").classList.add("d-none");
      D.getElementById("bathroom-4").classList.replace("position-relative", "hide");
    }
    e.target.parentElement.parentElement.classList.replace("position-relative", "hide");
    e.target.parentElement.parentElement.parentElement.querySelector("button").classList.remove("d-none");
  }

  // * AÑADIR HABITACION
  if (e.target.id === "add-new-room") INSTANCE.addNewRoom();

  // * REGISTER BUTTON
  if (e.target.id === "submit") {
    e.preventDefault();

    _Messages.showMessage("Please Wait...", 1, false);

    // * CREAR FUNCION QUE MANDA A GUARDAR LA INFORMACION PARA USARLA EN CUALQUIER CASO NECESARIO
    const submitAction = () => {
      INSTANCE.saveDataHomestay(
        D.querySelectorAll("[data-form-register] input"),
        D.querySelectorAll("[data-form-register] select"),
        D.querySelector("[data-form-register] textarea")
      );
    };

    // * CORREGIR MEMBERS FAMILY
    if (D.querySelector("#number-members").value == 0) D.querySelector("#number-members").value = 1;
    if (D.querySelector("#number-members").value < 1 || D.querySelector("#number-members").value > 9) {
      _Messages.showMessage("The value of this field must be in a range of 1 to 9 characters.", 4, false);
      D.querySelector("#number-members").value = D.querySelectorAll("#family-members section").length + 1;
    }

    // * VERIFICAR CAMPOS INVALIDOS
    let invalids = false;
    D.querySelectorAll("input").forEach((elem) => (elem.classList.contains("invalid") ? (invalids = true) : {}));
    D.querySelectorAll("select").forEach((elem) => (elem.classList.contains("invalid") ? (invalids = true) : {}));
    D.querySelectorAll("textarea").forEach((elem) => (elem.classList.contains("invalid") ? (invalids = true) : {}));

    // * (invalids) CAMPOS INCORRECTOS O INVALIDOS EN LOS FORMULARIOS
    if (invalids) {
      _Messages.showMessage(
        "It seems there are some fields with invalid data, please correct them to continue.",
        4,
        false,
        ".invalid"
      );
    } else {
      let save = true;
      D.querySelectorAll("#reserve").forEach((reserve) => {
        if (
          reserve.querySelector("#type-room").value === "NULL" ||
          reserve.querySelector("#bed-type").value === "NULL" ||
          Number(reserve.querySelector("#homestay-price").value) <= 0 ||
          Number(reserve.querySelector("#provider-price").value) <= 0
        ) {
          reserve.classList.add("required-fields");
          save = false;
        }
      });

      // * (save) VALIDACION DEL (tipoDeHabitacion-tiposDeCama-precioHomestay-precioAgente)
      if (save === false) {
        _Messages.quitMessage();

        D.querySelector("#modal-dynamic").classList.add("show");
        D.querySelector("#modal-dynamic").style.opacity = "1";

        D.querySelector("#modal-dynamic").innerHTML = `
          <div class="d-flex flex-column text-center justify-content-center save-changes">
            <h4>You have not finished configuring the rooms, do you want <br> 
            to save the changes without the rooms?</h4>
            <div class="d-flex align-items-center justify-content-center">
              <button id="save-changes-yes" class="btn w-25 mx-2 enable-room">Yes</button>
              <button id="save-changes-no" class="btn w-25 mx-2 disable-room">No</button>
            </div>
          </div>
        `;

        // * OPCION NO
        D.querySelector("#save-changes-no").addEventListener("click", (e) => {
          D.querySelector("#modal-dynamic").style.cssText = "";
          D.querySelector("#modal-dynamic").style.opacity = "0";
          D.querySelector("#modal-dynamic").classList.remove("show");
          D.querySelector(`.required-fields`).scrollIntoView({ block: "center", behavior: "smooth" });
          setTimeout(() => {
            D.querySelectorAll(".required-fields").forEach((reserve) => reserve.classList.remove("required-fields"));
          }, 2500);
        });

        // * OPCION YES
        D.querySelector("#save-changes-yes").addEventListener("click", (e) => {
          if (typeof D.querySelector("#provider-price").value !== "number") {
            D.querySelector("#provider-price").value = 0;
          }
          D.querySelectorAll(".required-fields").forEach((reserve) => reserve.remove());
          D.querySelector("#total-rooms").value = D.querySelectorAll("#reserve").length;

          _Messages.showMessage("Please Wait...", 1, false);

          D.querySelector("#modal-dynamic").style.cssText = "";
          D.querySelector("#modal-dynamic").style.opacity = "0";
          D.querySelector("#modal-dynamic").classList.remove("show");

          submitAction();
        });
      } else submitAction();
    }
  }

  // * AÑADIR MIEMBRO DE FAMILIA
  if (e.target.matches("#add-member")) {
    addMembersFamily(e);

    INSTANCE.dateRangePicker();
  }

  // * DELETE FAMILY MEMBER BUTTON
  if (e.target.matches(".delete-member")) {
    e.target.parentElement.remove();
    D.querySelector("#number-members").value = D.querySelectorAll("#family-members section").length + 1;
  }

  // * MODAL ACTIONS
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").style.cssText = "";
    D.querySelector("#modal-dynamic").classList.remove("show");
  }
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  // ? VALIDATION
  if (e.target.id === "house-name") validateInput(e.target, "name-special");
  if (e.target.id === "phone-number") validateInput(e.target, "phone");
  if (e.target.id === "total-rooms") validateInput(e.target, "rooms");
  if (e.target.id === "house-mail") validateInput(e.target, "email");
  if (e.target.id === "password") validateInput(e.target, "password");
  if (e.target.id === "address") validateInput(e.target, "address");
  if (e.target.id === "city") validateInput(e.target, "address");
  if (e.target.id === "state") validateInput(e.target, "address");
  if (e.target.id === "postal-code") validateInput(e.target, "address");
  // if (e.target.id === "postal-code") validateInput(e.target, "postal-code");
  if (e.target.id === "homestay-price") validateInput(e.target, "number");
  if (e.target.id === "provider-price") validateInput(e.target, "number");
  if (e.target.id === "house-description") validateInput(e.target, "description");
  // if (e.target.id === "total-years") validateInput(e.target, "date");
  if (e.target.id === "specify-pet") validateInput(e.target, "name");
  if (e.target.id === "specify-allergy") validateInput(e.target, "name");
  if (e.target.id === "specify-medication") validateInput(e.target, "name");
  if (e.target.id === "specify-problems") validateInput(e.target, "name");
  if (e.target.id === "name-contact") validateInput(e.target, "name-special");
  if (e.target.id === "last-name-contact") validateInput(e.target, "name-special");
  // if (e.target.id === "date-birth-contact") validateInput(e.target, "date");
  if (e.target.id === "phone-contact") validateInput(e.target, "phone");
  // if (e.target.id === "date-background-check") validateInput(e.target, "date");
  if (e.target.id === "background") validateInput(e.target, "name");
  if (e.target.id === "background-language") validateInput(e.target, "name");
  if (e.target.id === "specify-religion") validateInput(e.target, "name");
  if (e.target.id === "specify-condition") validateInput(e.target, "name");
  if (e.target.id === "specify-misdemeanor") validateInput(e.target, "name");
  if (e.target.matches("[data-name]")) validateInput(e.target, "name");
  if (e.target.matches("[data-last-name]")) validateInput(e.target, "name");
  // if (e.target.matches("[data-date-birth]")) validateInput(e.target, "date");
  if (e.target.matches("[data-occupation]")) validateInput(e.target, "name");
  // if (e.target.matches("[data-date-background-check]")) validateInput(e.target, "date");

  if (e.target.matches("#total-rooms")) {
    if (e.target.value > 0 && e.target.value <= 8) {
      D.querySelectorAll("#all-rooms #reserve").forEach((elem) => elem.remove());
      for (let i = 0; i < e.target.value; i++) INSTANCE.addNewRoom("toInput");

      D.querySelector("#total-rooms").value = D.querySelectorAll("#all-rooms #reserve").length;
    }
  }

  // * PHONE NUMBERS
  if (e.target.matches("#phone-number")) D.querySelector("#phone-contact").value = e.target.value;
  if (e.target.matches("#phone-contact")) {
    e.target.disabled = true;
    e.target.classList.remove("invalid");
    e.target.value = D.querySelector("#phone-number").value;
    _Messages.showMessage("It is not possible to alter this field at the moment.", 4, false);
  }
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? VALIDATION
  if (e.target.id === "house-type") validateInput(e.target, ["House", "Apartment", "Condominium"]);
  if (e.target.id === "main-city") validateInput(e.target, "name");
  if (e.target.matches("[data-gallery-photo]") || e.target.matches(".carousel-item input")) {
    const result = validateInput(e.target, "image");
    if (result) _Messages.showMessage(result, 4, false);
  }
  if (e.target.id === "type-room") validateInput(e.target, ["Single", "Executive", "Share"]);
  if (e.target.id === "food-service") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "bed-type") validateInput(e.target, ["Twin", "Bunk-bed", "Double"]);
  if (e.target.id === "academy") validateInput(e.target, "number");
  if (e.target.id === "preferences-gender") validateInput(e.target, ["Male", "Female", "Any"]);
  if (e.target.id === "preferences-age") validateInput(e.target, ["Teenager", "Adult", "Any"]);
  if (e.target.id === "smokers-politics") validateInput(e.target, ["Outside-OK", "Inside-OK", "Strictly Non-Smoking"]);
  if (e.target.id === "meals-service") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "pets") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "total-pets") validateInput(e.target, "number");
  if (e.target.id === "allergies") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "take-medication") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "health-problems") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "gender-contact") validateInput(e.target, ["Male", "Female", "Private"]);
  if (e.target.id === "pdf-file") {
    const result = validateInput(e.target, "pdf");
    if (result) _Messages.showMessage(result, 4, false);
  }
  if (e.target.id === "number-members") validateInput(e.target, "number");
  if (e.target.id === "religion") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "condition") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "misdemeanor") validateInput(e.target, ["Yes", "No"]);
  if (e.target.id === "family-final-question") validateInput(e.target, ["Yes", "No"]);
  if (e.target.matches("[data-gender]")) validateInput(e.target, ["Male", "Female", "Private"]);
  if (e.target.matches("[data-relation]"))
    validateInput(e.target, ["Dad", "Mom", "Son", "Daughter", "Grandparents", "Others"]);
  if (e.target.matches("[data-background-check]")) {
    const result = validateInput(e.target, "pdf");
    if (result) _Messages.showMessage(result, 4, 6);
  }

  if (e.target.id === "meals-service") {
    if (e.target.value === "Yes") {
      D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? (h3.style.cssText = "") : {}));
      D.querySelector(".special-diet").classList.replace("d-none", "d-flex");
    } else {
      D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? (h3.style.display = "none") : {}));
      D.querySelector(".special-diet").classList.replace("d-flex", "d-none");
    }
  }

  if (e.target.id === "pets") e.target.value === "Yes" ? petShowResponses(1) : petShowResponses();

  if (e.target.id === "religion")
    e.target.value === "Yes" ? showResponses("#family-responses", 1, "add") : showResponses("#family-responses", 1);
  if (e.target.id === "condition")
    e.target.value === "Yes" ? showResponses("#family-responses", 2, "add") : showResponses("#family-responses", 2);
  if (e.target.id === "misdemeanor")
    e.target.value === "Yes" ? showResponses("#family-responses", 3, "add") : showResponses("#family-responses", 3);

  if (e.target.id === "allergies")
    e.target.value === "Yes"
      ? showResponses("#responses-any-members", 1, "add")
      : showResponses("#responses-any-members", 1);
  if (e.target.id === "take-medication")
    e.target.value === "Yes"
      ? showResponses("#responses-any-members", 2, "add")
      : showResponses("#responses-any-members", 2);
  if (e.target.id === "health-problems")
    e.target.value === "Yes"
      ? showResponses("#responses-any-members", 3, "add")
      : showResponses("#responses-any-members", 3);

  if (e.target.matches("#type-room")) INSTANCE.editBeds(e.target);

  // * ADD MULTIPLE MEMBERS FAMILY
  if (e.target.matches("#number-members")) addMembersFamily(e);
});
