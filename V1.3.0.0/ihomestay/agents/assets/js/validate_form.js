// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

const formulary = document.getElementById("form");

const inputs = document.querySelectorAll("#form input");

const _Messages = new Messages();

const D = document;
const W = window;

// Eye Function

let password = document.getElementById("password");
let viewPassword = document.getElementById("btn__eye");
let click = false;

viewPassword.addEventListener("click", (e) => {
  if (!click) {
    password.type = "text";
    $("#btn__eye").removeClass("fa-eye").addClass("fa-eye-slash");
    click = true;
  } else if (click) {
    password.type = "password";
    $("#btn__eye").removeClass("fa-eye-slash").addClass("fa-eye");
    click = false;
  }
});

// TODO CANVAS

const $canvas = document.querySelector("#canvas"),
  $btnDescargar = document.querySelector("#btnDescargar"),
  $btnLimpiar = document.querySelector("#btnLimpiar"),
  $btnGenerarDocumento = document.querySelector("#btnGenerarDocumento");

const contexto = $canvas.getContext("2d");
const COLOR_PINCEL = "black";
const COLOR_FONDO = "white";
const GROSOR = 2;
let xAnterior = 0,
  yAnterior = 0,
  xActual = 0,
  yActual = 0;
const obtenerXReal = (clientX) => clientX - $canvas.getBoundingClientRect().left;
const obtenerYReal = (clientY) => clientY - $canvas.getBoundingClientRect().top;
let haComenzadoDibujo = false;
let finishedSignature = false;

// Clean

const limpiarCanvas = () => {
  // Colocar color blanco en fondo de canvas
  contexto.fillStyle = COLOR_FONDO;
  contexto.fillRect(0, 0, $canvas.width, $canvas.height);
  finishedSignature = false;
};
limpiarCanvas();
$btnLimpiar.onclick = limpiarCanvas;

// Download

// Escuchar clic del botón para descargar el canvas
/* $btnDescargar.onclick = () => {}; */

// Lo demás tiene que ver con pintar sobre el canvas en los eventos del mouse
$canvas.addEventListener("mousedown", (evento) => {
  // En este evento solo se ha iniciado el clic, así que dibujamos un punto
  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.fillStyle = COLOR_PINCEL;
  contexto.fillRect(xActual, yActual, GROSOR, GROSOR);
  contexto.closePath();
  // Y establecemos la bandera
  haComenzadoDibujo = true;
});

$canvas.addEventListener("mousemove", (evento) => {
  if (!haComenzadoDibujo) {
    return;
  }
  // El mouse se está moviendo y el usuario está presionando el botón, así que dibujamos todo

  xAnterior = xActual;
  yAnterior = yActual;
  xActual = obtenerXReal(evento.clientX);
  yActual = obtenerYReal(evento.clientY);
  contexto.beginPath();
  contexto.moveTo(xAnterior, yAnterior);
  contexto.lineTo(xActual, yActual);
  contexto.strokeStyle = COLOR_PINCEL;
  contexto.lineWidth = GROSOR;
  contexto.stroke();
  contexto.closePath();
});
["mouseup", "mouseout"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    haComenzadoDibujo = false;
  });
});
["mouseup", "mousedown"].forEach((nombreDeEvento) => {
  $canvas.addEventListener(nombreDeEvento, () => {
    finishedSignature = true;
  });
});

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^\d{2,20}$/, // 2 a 14 numeros.
  rooms: /^\d{1,2}$/, // 1 a 14 numeros.
  dir: /^[a-zA-Z0-9\_\-\s\°\S]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s\S]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s\S]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/, // 8 a 15 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
};

const campos = {
  name: false,
  l_name: false,
  mail_s: false,
  password: false,
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    // Required fields
    case "name":
      validateField(expresiones.nombre, e.target, "name");
      break;

    case "l_name":
      validateField(expresiones.nombre, e.target, "l_name");
      break;

    case "mail_s":
      validateField(expresiones.correo, e.target, "mail_s");
      break;

    case "password":
      validateField(expresiones.password, e.target, "password");
      break;

    /*case "firstd":
            validateField(expresiones.date, e.target, 'firstd');
        break;*/

    case "dir":
      validateField(expresiones.dir, e.target, "dir");
      break;

    case "city":
      validateField(expresiones.city, e.target, "city");
      break;

    case "state":
      validateField(expresiones.state, e.target, "state");
      break;

    case "p_code":
      validateField(expresiones.p_code, e.target, "p_code");
      break;

    // Optional Fields

    case "num_s":
      validateField(expresiones.phone, e.target, "num_s");
      break;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`group__${campo}`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  } else {
    document.getElementById(`group__${campo}`).classList.add("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.add("form__group__input-error-active");
    campos[campo] = false;
  }
};

inputs.forEach((input) => {
  input.addEventListener("keyup", validateFormulary);
  input.addEventListener("blur", validateFormulary);
});

// ! FUNCTIONS

async function saveStudent(form) {
  const formData = new FormData(form);
  const DATA = { method: "POST", body: formData };

  const jsonSaveStudent = await fetch("./action_student.php", DATA);
  const resultSaveStudent = await jsonSaveStudent.json();

  console.log(resultSaveStudent);

  if (resultSaveStudent) {
    _Messages.quitMessage();

    if (resultSaveStudent.title === "mail exist") {
      _Messages.showMessage("Email exists, please write another Email.", 4, 6, "#div__flex");
      document.getElementById(`group__mail_s`).classList.add("form__group-incorrect");
    } else if (resultSaveStudent.title === "true") {
      _Messages.showMessage(`${form.name.value} ${form.l_name.value}, has been successfully registered.`, 2);
      setTimeout(() => (window.location.href = `directory_students`), 2000);
    } else if (resultSaveStudent.title === "new school") {
      _Messages.showMessage(`${form.name.value} ${form.l_name.value}, has been successfully registered.`, 2);
      setTimeout(() => (window.location.href = `academy_register?art_id=${resultSaveStudent.id_school}`), 2000);
    } else if (resultSaveStudent.title === "false") {
      _Messages.showMessage("An error has occurred in the additional information, please try again.", 2, 4);
      W.top.reload();
    }
  }

  /* resultSaveStudent.forEach((response) => {
  }); */
}

formulary.addEventListener("submit", (e) => {
  e.preventDefault();
  _Messages.showMessage("Please Wait...", 1, false);
  const enlace = document.createElement("a");
  // Convertir la imagen a Base64 y ponerlo en el enlace
  enlace.href = $canvas.toDataURL();

  document.querySelector("#signature-canvas").value = enlace.href;

  if (campos.name && campos.l_name && campos.mail_s && campos.password) {
    saveStudent(formulary);
  } else {
    if (!campos.mail_s) document.getElementById(`group__mail_s`).classList.add("form__group-incorrect");
    if (!campos.name) document.getElementById(`group__name`).classList.add("form__group-incorrect");
    if (!campos.l_name) document.getElementById(`group__l_name`).classList.add("form__group-incorrect");
    if (!campos.password) document.getElementById(`group__password`).classList.add("form__group-incorrect");
    _Messages.quitMessage();
    _Messages.showMessage(
      "An error has occurred in the additional information, please check the fields.",
      4,
      6,
      "#div__flex"
    );
  }
});
