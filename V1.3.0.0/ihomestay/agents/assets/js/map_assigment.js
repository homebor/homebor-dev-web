export default async function MAP(allDataHouses) {
  const D = document;
  const W = window;

  // ? OBTENER LOS PARAMETROS DEL URL
  const searchParams = new URLSearchParams(W.location.search);
  // ? OBTENER EL PARAMETRO art_id DE LA URL
  const ID_STUDENT = searchParams.get("art_id");

  const formData = new FormData();
  formData.set("student_id", ID_STUDENT);

  const OPTIONS = {
    method: "POST",
    header: { "content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const req = await axios("../helpers/agents/map_assigment.php", OPTIONS);
  const academy = await req.data;
  // console.log(academy);

  mapboxgl.accessToken = "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg";
  var client = new MapboxClient(mapboxgl.accessToken);

  // console.log(allDataHouses);

  var directionA;
  academy.address === "NULL" ? (directionA = "Toronto") : (directionA = academy.address);
  var test = await client.geocodeForward(directionA, async (err, data, res) => {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties

    var coordinatesA = data.features[0].center;
    //Esta variable define el centro de tu mapa todas las direcciones deben tenerla de la misma manera y zoom o no encontrara el centro
    const mapOption = {
      container: "map",
      style: "mapbox://styles/mapbox/streets-v10",
      center: coordinatesA,
      zoom: 10,
    };
    var map = new mapboxgl.Map(mapOption);

    var direction1 = allDataHouses.house1.address;
    var test = client.geocodeForward(direction1, (err, data, res) => {
      var coordinates1 = data.features[0].center;
      var map = new mapboxgl.Map(mapOption);

      // * Directions box
      var directions = new MapboxDirections({
        accessToken: "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
        unit: "metric",
      });
      map.addControl(directions, "top-right");

      console.log(directionA);

      if (academy.address !== "NULL") map.on("load", () => directions.setDestination(directionA));

      if (allDataHouses.house2) {
        var direction2 = allDataHouses.house2.address;
        var test = client.geocodeForward(direction2, (err, data, res) => {
          var coordinates2 = data.features[0].center;
          var map = new mapboxgl.Map(mapOption);

          // * Directions box
          var directions = new MapboxDirections({
            accessToken: "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
            unit: "metric",
          });
          map.addControl(directions, "top-right");

          map.on("load", () => directions.setDestination(directionA));

          if (allDataHouses.house3) {
            var direction3 = allDataHouses.house3.address;
            var test = client.geocodeForward(direction3, (err, data, res) => {
              var coordinates3 = data.features[0].center;
              var map = new mapboxgl.Map(mapOption);

              // * Directions box
              var directions = new MapboxDirections({
                accessToken:
                  "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
                unit: "metric",
              });
              map.addControl(directions, "top-right");

              map.on("load", () => directions.setDestination(directionA));

              if (allDataHouses.house4) {
                var direction4 = allDataHouses.house4.address;
                var test = client.geocodeForward(direction4, (err, data, res) => {
                  var coordinates4 = data.features[0].center;
                  var map = new mapboxgl.Map(mapOption);

                  // * Directions box
                  var directions = new MapboxDirections({
                    accessToken:
                      "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
                    unit: "metric",
                  });
                  map.addControl(directions, "top-right");

                  map.on("load", () => directions.setDestination(directionA));

                  if (allDataHouses.house5) {
                    var direction5 = allDataHouses.house5.address;
                    var test = client.geocodeForward(direction5, (err, data, res) => {
                      var coordinates5 = data.features[0].center;
                      var map = new mapboxgl.Map(mapOption);

                      // * Directions box
                      var directions = new MapboxDirections({
                        accessToken:
                          "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
                        unit: "metric",
                      });
                      map.addControl(directions, "top-right");

                      map.on("load", () => directions.setDestination(directionA));

                      // * POPUP 1
                      var popup1 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>`
                      );
                      // create DOM element for the marker
                      var ea = document.createElement("div");
                      ea.id = "house";
                      new mapboxgl.Marker(ea).setLngLat(coordinates1).setPopup(popup1).addTo(map);

                      // * POPUP 2
                      var popup2 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house2.id}"><img src="${allDataHouses.house2.photo}" class="w-100" id="map-img"></a>`
                      );
                      // create DOM element for the marker
                      var eb = document.createElement("div");
                      eb.id = "house";
                      new mapboxgl.Marker(eb).setLngLat(coordinates2).setPopup(popup2).addTo(map);

                      // * POPUP 3
                      var popup3 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house3.id}"><img src="${allDataHouses.house3.photo}" class="w-100" id="map-img"></a>`
                      );
                      // create DOM element for the marker
                      var ec = document.createElement("div");
                      ec.id = "house";
                      new mapboxgl.Marker(ec).setLngLat(coordinates3).setPopup(popup3).addTo(map);

                      // * POPUP 4
                      var popup4 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house4.id}">${allDataHouses.house4.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house4.id}">${allDataHouses.house4.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house4.id}"><img src="${allDataHouses.house4.photo}" class="w-100" id="map-img"></a>`
                      );
                      // create DOM element for the marker
                      var ed = document.createElement("div");
                      ed.id = "house";
                      new mapboxgl.Marker(ed).setLngLat(coordinates4).setPopup(popup4).addTo(map);

                      // * POPUP 5
                      var popup5 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house5.id}">${allDataHouses.house5.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house5.id}">${allDataHouses.house5.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house5.id}"><img src="${allDataHouses.house5.photo}" class="w-100" id="map-img"></a>`
                      );
                      // create DOM element for the marker
                      var ef = document.createElement("div");
                      ef.id = "house";
                      new mapboxgl.Marker(ef).setLngLat(coordinates5).setPopup(popup5).addTo(map);

                      // * POPUP ACADEMY
                      //Para marcar varios puntos debes copiar el bloque de busqueda como tenemos abajo y es muy importante pegarlo antes de cerrar el primer parentesis debajo del addto(map), ademas el bloque completo de new mapboxgl.marker debe estar en la ultima consulta es decir el ultimo bloque interno sino la consulta fallara, las variables se deben subir al numero inmediato superior
                      // PRIMERA CONSULTA CON EL POPUP AGREGADO
                      var popupA = new mapboxgl.Popup({ offset: 25 }).setHTML(
                        `<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">`
                      );
                      // create DOM element for the marker
                      var el = document.createElement("div");
                      el.id = "marker";
                      // lo superior es el popupA
                      // marker en el mapa // marker // pertenece a la funcion del popupA // marker
                      new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);
                    });
                  } else {
                    // * POPUP 1
                    var popup1 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                      `<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>`
                    );
                    // create DOM element for the marker
                    var ea = document.createElement("div");
                    ea.id = "house";

                    new mapboxgl.Marker(ea).setLngLat(coordinates1).setPopup(popup1).addTo(map);

                    // * POPUP 2
                    var popup2 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                      `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house2.id}"><img src="${allDataHouses.house2.photo}" class="w-100" id="map-img"></a>`
                    );
                    var eb = document.createElement("div");
                    eb.id = "house";

                    new mapboxgl.Marker(eb).setLngLat(coordinates2).setPopup(popup2).addTo(map);

                    // * POPUP 3
                    var popup3 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                      `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house3.id}"><img src="${allDataHouses.house3.photo}" class="w-100" id="map-img"></a>`
                    );
                    var ec = document.createElement("div");
                    ec.id = "house";
                    new mapboxgl.Marker(ec).setLngLat(coordinates3).setPopup(popup3).addTo(map);

                    // * POPUP 4
                    var popup4 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                      `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house4.id}">${allDataHouses.house4.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house4.id}">${allDataHouses.house4.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house4.id}"><img src="${allDataHouses.house4.photo}" class="w-100" id="map-img"></a>`
                    );
                    var ed = document.createElement("div");
                    ed.id = "house";
                    new mapboxgl.Marker(ed).setLngLat(coordinates4).setPopup(popup4).addTo(map);

                    // * POPUP ACADEMY
                    var popupA = new mapboxgl.Popup({ offset: 25 }).setHTML(
                      `<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">`
                    );
                    var el = document.createElement("div");
                    el.id = "marker";
                    new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);
                  }
                });
              } else {
                // * POPUP 1
                var popup1 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                  `<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>`
                );
                var ea = document.createElement("div");
                ea.id = "house";

                new mapboxgl.Marker(ea).setLngLat(coordinates1).setPopup(popup1).addTo(map);

                // * POPUP 2
                var popup2 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                  `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house2.id}"><img src="${allDataHouses.house2.photo}" class="w-100" id="map-img"></a>`
                );
                var eb = document.createElement("div");
                eb.id = "house";

                new mapboxgl.Marker(eb).setLngLat(coordinates2).setPopup(popup2).addTo(map);

                // * POPUP 3
                var popup3 = new mapboxgl.Popup({ offset: 25 }).setHTML(
                  `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house3.id}">${allDataHouses.house3.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house3.id}"><img src="${allDataHouses.house3.photo}" class="w-100" id="map-img"></a>`
                );
                var ec = document.createElement("div");
                ec.id = "house";
                new mapboxgl.Marker(ec).setLngLat(coordinates3).setPopup(popup3).addTo(map);

                // * POPUT ACADEMY
                var popupA = new mapboxgl.Popup({ offset: 25 }).setHTML(
                  `<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">`
                );
                var el = document.createElement("div");
                el.id = "marker";
                new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);
              }
            });
          } else {
            // * POPUP 1
            var popup1 = new mapboxgl.Popup({ offset: 25 }).setHTML(
              `<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>`
            );
            var ea = document.createElement("div");
            ea.id = "house";

            new mapboxgl.Marker(ea).setLngLat(coordinates1).setPopup(popup1).addTo(map);

            // * POPUP 2
            var popup2 = new mapboxgl.Popup({ offset: 25 }).setHTML(
              `<strong>Name: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house2.id}">${allDataHouses.house2.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house2.id}"><img src="${allDataHouses.house2.photo}" class="w-100" id="map-img"></a>`
            );
            var eb = document.createElement("div");
            eb.id = "house";

            new mapboxgl.Marker(eb).setLngLat(coordinates2).setPopup(popup2).addTo(map);

            // * POPUT ACADEMY
            var popupA = new mapboxgl.Popup({ offset: 25 }).setHTML(
              `<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">`
            );
            var el = document.createElement("div");
            el.id = "marker";
            new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);
          }
        });
      } else {
        // * POPUP 1
        var popup1 = new mapboxgl.Popup({ offset: 25 }).setHTML(
          `<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>`
        );
        var ea = document.createElement("div");
        ea.id = "house";

        new mapboxgl.Marker(ea).setLngLat(coordinates1).setPopup(popup1).addTo(map);

        // * POPUP ACADEMY
        if (academy.address !== "NULL") {
          var popupA = new mapboxgl.Popup({ offset: 25 })
            .setHTML(`<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">
          `);
          var el = document.createElement("div");
          el.id = "marker";
          new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);
        }
      }
    });
  });
}
