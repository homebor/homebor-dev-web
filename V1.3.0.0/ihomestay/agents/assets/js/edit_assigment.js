// TODO IMPORTACIONES
import MAP from "./map_assigment.js";
import studentData from "./student_data.js";
import homestayData from "./houses_data.js";
import { defaultFilters } from "./houses_data.js";
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_STUDENT = searchParams.get("art_id");
// ? INSTANCIAS
const _Messages = new Messages();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await studentData();
  const result = await homestayData();
  if (result) await defaultFilters();

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }

  // ? BACK TO PANEL
  if (e.target.matches(".back_panel")) window.location.href = "directory_students";

  // ? SEND THE ADDRESS OF HOUSES SELECTED TO THE MAP
  if (e.target.id === "next-step") {
    const allDataHouses = {};
    document.querySelectorAll("#all-houses-selected > div").forEach((house, i) => {
      allDataHouses[`house${i + 1}`] = {
        id: house.querySelector("[data-house-profile]").dataset.houseProfile,
        name: house.querySelector("label").textContent.replace("Select Bed", "").trim(),
        address: house.querySelector("[data-house-direction]").textContent,
        photo: house.querySelector("img").src,
      };
    });

    MAP(allDataHouses);
  }
});

// ! KEYUP
D.addEventListener("keyup", (e) => {});

// ! CHANGE
D.addEventListener("change", (e) => {});
