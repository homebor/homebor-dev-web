// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES
async function getData(get) {
  const formData = new FormData();
  formData.set("get", get);

  const OPTIONS = {
    method: "POST",
    headers: { "Content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const request = await axios("stripe.php", OPTIONS);
  const response = await request.data;

  if (get === 1) renderCustomers(response.data);
  else renderCustomers(response.data);
}

function renderCustomers(data) {
  const $container = D.getElementById("container");
  const $template = D.getElementById("customer-template").content;
  const $fragment = D.createDocumentFragment();

  if (data) {
    $container.innerHTML = "";
    data.forEach((customer) => {
      const $customer = D.importNode($template, true);
      // $customer.querySelector('data-customer-image').src
      $customer.querySelector("[data-customer-name]").textContent = customer.name;
      if (customer.city == null) customer.city = { city: "Empty" }.city = "Empty";
      $customer.querySelector("[data-customer-city]").textContent = customer.city.city || "Empty";
      $customer.querySelector("[data-customer-phone]").textContent = customer.phone || "Empty";
      const getDate = (date) => {
        const DATE = new Date(Number(`${date}000`));
        const getMonth = (month) => (month < 10 ? `0${month}` : month);
        const dateText = `${DATE.getDate()}-${getMonth(DATE.getMonth() + 1)}-${DATE.getFullYear()}`;
        return dateText;
      };
      $customer.querySelector("[data-customer-created]").textContent = getDate(customer.created);
      $customer.querySelector("[data-customer-description]").textContent = customer.description;
      $customer.querySelector("[data-customer-rol]").dataset.customerRol = customer.metadata.rol || "Empty";
      $fragment.appendChild($customer);
    });

    $container.appendChild($fragment);
  }
}
// *
// *
// *
// *
// *
// *
// *
async function createCustomer(data) {
  if (!data.email || data.email === "Empty") {
    D.querySelector("[email-input]").classList.add("invalid");
    return _Messages.showMessage("Email required", 4, false);
  }

  return;
  const formData = new FormData();
  formData.set("create", 1);
  formData.set("name", data.name);
  formData.set("email", data.email);
  formData.set("city", data.city);
  formData.set("phone", data.phone);
  formData.set("description", data.description);
  formData.set("rol", data.rol);

  const OPTIONS = {
    method: "POST",
    headers: { "Content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const request = await axios("stripe.php", OPTIONS);
  const response = await request.data;

  console.log(response);
}

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await getData(1);
  _Messages.quitMessage();
});
// ! CLICK
D.addEventListener("click", (e) => {
  if (e.target.id === "show-customers") {
    getData(1);
  }
  if (e.target.id === "show-products") {
    getData(2);
  }
  if (e.target.id === "existent-yes") {
    D.getElementById("more-inputs").classList.add("d-none");
    if (D.getElementById("existent-yes").checked) D.getElementById("existent-no").checked = false;
  }
  if (e.target.id === "existent-no") {
    if (D.getElementById("existent-no").checked) {
      D.getElementById("more-inputs").classList.remove("d-none");
      D.getElementById("existent-yes").checked = false;
    } else D.getElementById("more-inputs").classList.add("d-none");
  }
});

// ! SUBMIT
D.addEventListener("submit", (e) => {
  e.preventDefault();

  if (e.target.id.includes("customer")) {
    const customerData = {
      email: e.target.email.value || "Empty",
      name: e.target.name.value || "Empty",
      rol: e.target.rol.value || "Empty",
      existentYes: e.target["existent-yes"].checked,
      existentNo: e.target["existent-no"].checked,
      city: e.target.city.value || "Empty",
      phone: e.target.phone.value || "Empty",
      description: e.target.description.value || "Empty",
    };

    createCustomer(customerData);
  } else {
    console.log("Falta crear el producto");
  }
});

// ! CHANGE

// ! KEYDOWN
D.addEventListener("keydown", (e) => {
  // * CREATE CUSTOMER
  if (e.shiftKey && e.code === "Backquote") {
    D.querySelector("#create-customer-form").classList.add("d-none");
    D.querySelector("#create-product-form").classList.add("d-none");
  }

  if (e.shiftKey && e.code === "Digit1") {
    D.querySelector("#create-customer-form").classList.remove("d-none");
    D.querySelector("#create-product-form").classList.add("d-none");
  }
  // * CREATE PRODUCT
  if (e.shiftKey && e.code === "Digit2") {
    D.querySelector("#create-product-form").classList.remove("d-none");
    D.querySelector("#create-customer-form").classList.add("d-none");
  }
});
// ! ERROR
