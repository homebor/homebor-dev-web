// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_STUDENT = searchParams.get("art_id");
// ? INSTANCIAS
const _Messages = new Messages();

// ? STUDENT DATA AND PREFERENCES
const studentData = {
  age: "",
  gender: "",
  lang: "",
  pets: "",
  food: "",
  specialDiet: {
    vegetarians: "",
    halal: "",
    kosher: "",
    lactose: "",
    gluten: "",
    pork: "",
  },
  smoke: "",
};
// ? TODAS LAS CASAS
const allHouses = [];
// ? TODAS LAS CASAS POR PAGINACION
const pages = [];
// ? LIMITE DE CASAS POR PAGINA
const LIMIT = 100;
// ? DATA TO SEARCH
const filterTags = {
  typeRoom: "",
  typeBed: "",
  gender: "",
  age: "",
  pets: "",
  smoke: "",
  food: "",
  // * SPECIAL DIET
  vegetarian: "",
  halal: "",
  kosher: "",
  lactose: "",
  gluten: "",
  pork: "",
  // * STATUS
  status: "",
};
const beds = ["A", "B", "C"];
const roomsColors = ["#232159", "#982A72", "#394893", "#A54483", "#5D418D", "#392B84", "#B15391", "#4F177D"];

// ? TEMPLATE DE HOMESTAY
const $template = D.getElementById("house-card-template").content;
// ? TEMPLATE DE HOMESTAY
const $templateBedrooms = D.getElementById("bedrooms-template").content;

// TODO FUNCIONES
// ? FUNCION QUE OBTIENE DE LA BASE DE DATOS LA INFORMACION ADICIONAL
export default async function getHomestayData() {
  try {
    // * GET HOMESTAY DATA
    const formData = new FormData();
    formData.set("student_id", ID_STUDENT);
    const OPTIONS = {
      method: "POST",
      header: { "Content-type": "application/json; charset=utf-8" },
      data: formData,
    };
    const getHomestayData = await axios("../helpers/agents/edit_assigment.php", OPTIONS);
    const homestayData = await getHomestayData.data;
    console.log(homestayData);
    if (!homestayData) throw "empty-data";
    if (homestayData && homestayData instanceof Array && homestayData[0] === "redirect") {
      window.location.href = homestayData[1];
    }

    const HOUSES = homestayData[0];
    const STUDENT = homestayData[1];

    const result = await filterHomestay(HOUSES, STUDENT);
    return result;
  } catch (error) {
    if (error === "empty-data") {
      D.querySelector("#houses-container").classList.add(
        ["d-flex"],
        ["align-items-center"],
        ["justify-content-center"]
      );
      D.querySelector("#houses-container").innerHTML = `
        <h3 class="m-0 pt-5 pb-4">There are no houses available at the moment</h3>
        <img src="../assets/emptys/frontage-empty.png" class="m-0 mb-5 p-0" width="300px" height="auto" alt="">
      `;
    } else console.error(error);
  }
}

/** // ? FUNCION QUE FILTRA LAS CASAS Y ORDENA POR RANKING (12-0)
 * @param {Array} houses Arreglo de casas a filtrar
 * @param {Array} student Datos del estudiante a asignar
 */
async function filterHomestay(houses, student) {
  return new Promise((resolve, reject) => {
    const getAge = (date) => {
      const currentDate = new Date();
      const dateBirth = new Date(date);
      let age = currentDate.getFullYear() - dateBirth.getFullYear();
      const month = currentDate.getMonth() - dateBirth.getMonth();

      if (month < 0 || (month === 0 && currentDate.getDate() < dateBirth.getDate())) age--;

      if (age >= 22) return "Adult";
      if (age <= 21) return "Teenager";
    };
    // * STEP 1 ORDER HOUSES BY PREFERENCES
    const ranking = { 12: [], 11: [], 10: [], 9: [], 8: [], 7: [], 6: [], 5: [], 4: [], 3: [], 2: [], 1: [], 0: [] };

    if (houses) {
      houses.forEach((data, i) => {
        const house = data[0];
        const reserves = data[1];
        const room = data[2];

        if (house.certified.toLowerCase() === "yes") {
          // ** HOUSES BY PREFERENCES
          studentData.age = getAge(student.db_s);
          studentData.gender = student.gen_s;
          studentData.lang = student.lang_s;
          studentData.pets = student.pets;
          studentData.food = student.food;
          studentData.specialDiet.vegetarians = student.vegetarians;
          studentData.specialDiet.vegetarians = student.halal;
          studentData.specialDiet.vegetarians = student.kosher;
          studentData.specialDiet.vegetarians = student.lactose;
          studentData.specialDiet.vegetarians = student.gluten;
          studentData.specialDiet.vegetarians = student.pork;
          studentData.smoke = student.smoke_s;

          let houseRanking = 0;
          if (studentData.age === house.ag_pre || house.ag_pre === "Any") houseRanking++;
          if (studentData.gender === house.g_pre) houseRanking++;
          if (studentData.lang === house.backl) houseRanking++;
          if (studentData.food === house.m_service) houseRanking++;
          if (studentData.pets === house.pet) houseRanking++;
          if (studentData.smoke === "No" && house.smoke.includes("Non-Smoking")) houseRanking++;
          if (studentData.smoke === "Yes" && (house.smoke.includes("Outside") || house.smoke.includes("Inside"))) {
            houseRanking++;
          }
          if (studentData.specialDiet.vegetarians === house.vegetarians) houseRanking++;
          if (studentData.specialDiet.halal === house.halal) houseRanking++;
          if (studentData.specialDiet.kosher === house.kosher) houseRanking++;
          if (studentData.specialDiet.lactose === house.lactose) houseRanking++;
          if (studentData.specialDiet.gluten === house.gluten) houseRanking++;
          if (studentData.specialDiet.pork === house.pork) houseRanking++;

          ranking[houseRanking].push([house, reserves, room]);
        }
      });
    }

    // * STEP 2 SORT BY BETTER HOMES
    let priority = 12;

    while (priority > 0) {
      ranking[priority].forEach((data) => {
        const house = data[0];
        const reserves = data[1];
        const room = data[2];

        const allRoomsData = new Map();

        let currentBed = 0;
        let currentRoom = 0;
        const roomKeys = Object.keys(room);
        const roomValues = Object.values(room);

        for (let i = 0; i < roomValues.length; i++) {
          if (currentRoom < 8 && !allRoomsData.has(roomsColors[currentRoom])) {
            allRoomsData.set(roomsColors[currentRoom], {
              A: { reserves: [], typeRoom: "", type: "", status: "Avalible" },
              B: { reserves: [], typeRoom: "", type: "", status: "Avalible" },
              C: { reserves: [], typeRoom: "", type: "", status: "Avalible" },
            });
          }

          // *** DEFINIR TIPO DE CAMA Y RESERVAS
          if (roomKeys[i].includes("bed")) {
            if (reserves) {
              reserves.forEach((reserve) => {
                if (reserve[2] === roomsColors[currentRoom] && reserve[4] === beds[currentBed]) {
                  const DATA = [reserve[4], reserve[1], reserve[5], reserve[7]];
                  allRoomsData.get(roomsColors[currentRoom])[beds[currentBed]].reserves.push(DATA);
                }
              });
            }
            if (i == 2 || i == 13 || i == 24 || i == 35 || i == 46 || i == 57 || i == 68 || i == 79) {
              allRoomsData.get(roomsColors[currentRoom]).A.typeRoom = roomValues[i - 1];
              allRoomsData.get(roomsColors[currentRoom]).B.typeRoom = roomValues[i - 1];
              allRoomsData.get(roomsColors[currentRoom]).C.typeRoom = roomValues[i - 1];
            }
            allRoomsData.get(roomsColors[currentRoom])[beds[currentBed]].type = roomValues[i];
            currentBed++;
          }

          if (currentBed === 3) currentBed = 0;

          // *** DEFINIR STATUS DE LA CAMA
          if (roomKeys[i].includes("date")) {
            allRoomsData.get(roomsColors[currentRoom])[beds[currentBed]].reserves.forEach((reserve) => {
              if (!verifyDates(reserve, student)) {
                allRoomsData.get(roomsColors[currentRoom])[reserve[0]].status = "Occupied";
              }
            });
            currentBed++;
          }

          // *** SI YA SE RECORRIERON LAS 3 CAMAS PASAR A LA SIGUIENTE HABITACION
          if (currentBed === 3) {
            currentBed = 0;
            currentRoom++;
          }
        }

        if (currentRoom === 8) {
          house.roomsData = allRoomsData;
          allHouses.push(house);
        }
      });

      priority--;
    }

    // * CREATE PAGINATION
    for (let i = 0; i < allHouses.length; i += LIMIT) pages.push(allHouses.slice(i, i + LIMIT));

    if (allHouses[0]) {
      renderHouses(allHouses, LIMIT);
      resolve(true);
    } else {
      D.querySelector(".search-house").parentElement.remove();
      D.querySelector(".loading-houses").style.opacity = "0";
      const $div = `<div class="w-100 m-0 py-4 d-flex flex-column align-items-center justify-content-center"><img src="../assets/emptys/frontage-empty.png" class="w-25 m-0 p-0 mx-auto"><br><h3 class="m-0 p-0">No Houses Found</h3></div>`;
      D.querySelector("#all-houses").style.opacity = "1";
      D.querySelector("#all-houses").innerHTML = $div;
      resolve(false);
    }
  });
}
// ? COLOCAR FILTROS POR DEFECTO DE ACUERDO AL ESTUDIANTE
export async function defaultFilters() {
  const clickFilter = (selector) => {
    D.querySelector(selector) ? D.querySelector(selector).click() : false;
  };

  // * AGE
  clickFilter(`#search-${studentData.age.toLowerCase()}`);

  // * GENDER
  if (!D.querySelector("#gender-student").textContent.includes("Any")) {
    clickFilter(`#search-${D.querySelector("#gender-student").textContent.toLowerCase().trim()}`);
  }

  // * SMOKE / PETS / FOOD / SPECIAL DIET
  D.querySelectorAll(".ts-description-lists > div dd").forEach((preference) => {
    const filter = preference.dataset.filter;
    const value = preference.textContent.toLowerCase();
    if (filter === "accommodation" && value === "single") clickFilter("#search-single");
    else if (filter === "accommodation" && value === "executive") clickFilter("#search-executive");
    else if (filter === "accommodation" && value === "share") clickFilter("#search-share");
    else if (filter === "smoke" && value === "yes") clickFilter("#search-outside-ok");
    else if (filter === "smoke" && value === "no") clickFilter("#search-non-smoking");
    else if (filter === "pets" && value === "yes") clickFilter("#search-pets-yes");
    else if (filter === "pets" && value === "no") clickFilter("#search-pets-no");
    else if (filter === "food" && value === "yes") clickFilter("#search-food-service-yes");
    else if (filter === "food" && value === "no") clickFilter("#search-food-service-no");
    else clickFilter(`#search-${filter}`);
  });

  // * STATUS
  clickFilter("#search-available");
  if (D.querySelector("#search-date").value) {
    searchDateHouse(
      D.querySelector("#search-date").value.split("-")[0].replaceAll("/", "-").trim(),
      D.querySelector("#search-date").value.split("-")[1].replaceAll("/", "-").trim()
    );
  }
}
// ? CONTROL DE SELECCION DE CASAS
function selectHouse(btn) {
  const adjustHouses = (house) => {
    if (house.nextElementSibling && house.nextElementSibling.classList.contains("show")) {
      house.parentElement.appendChild(house);
    }
    house.classList.remove("show");
    house.removeAttribute("data-current-id-home");
    house.querySelector("img").src = "../assets/emptys/frontage-empty.png";
    house.querySelector("input").checked = false;
    house.querySelector("label").removeAttribute("for");
    house.querySelector("input").removeAttribute("id");
    house.querySelector("span").textContent = `House Name`;
  };

  let totalHousesSelected = 0;
  const $house = btn.parentElement.parentElement.parentElement.parentElement;

  D.querySelectorAll("#all-houses input").forEach((input) => {
    input.checked ? totalHousesSelected++ : {};
    if (totalHousesSelected > 5) {
      totalHousesSelected--;
      btn.checked = false;
      _Messages.showMessage("It is not possible to select more than 5 houses", 4, false);
    }
  });

  if (totalHousesSelected > 0 && totalHousesSelected <= 5) {
    D.querySelector("#next-step").disabled = false;
    D.querySelector(".final-buttons").classList.add("show");
    D.querySelector(".current-houses-selected").classList.remove("d-none");
    D.querySelectorAll(".current-houses-selected article").forEach((house, i) => {
      if (btn.checked && i === totalHousesSelected - 1) {
        house.classList.add("show");
        house.dataset.currentIdHome = $house.dataset.idHome;
        house.querySelector("img").src = $house.querySelector(".card-header img").src;
        house.querySelector("input").checked = true;
        house.querySelector("label").setAttribute("for", `house-${$house.dataset.idHome}`);
        house.querySelector("input").id = `house-${$house.dataset.idHome}`;
        house.querySelector("span").textContent = $house.querySelector("[data-house-name]").textContent;
      } else if (btn.checked && i > totalHousesSelected - 1) house.classList.remove("show");
      else if (!btn.checked && $house.dataset.idHome === house.dataset.currentIdHome) adjustHouses(house);
    });
  } else {
    D.querySelector("#next-step").disabled = true;
    D.querySelector(".final-buttons").classList.remove("show");
    D.querySelector(".current-houses-selected").classList.add("d-none");
    D.querySelectorAll(".current-houses-selected article").forEach((house, i) => adjustHouses(house));
  }
}
// ? SEARCH HOUSES
async function searchHouse(text) {
  D.querySelector("#all-houses").innerHTML = `
  <button class="d-none mt-4 btn show-more">Show More</button>
  <button class="mt-4 btn see-more d-none" data-page="${D.querySelector(".see-more").dataset.page}">See More</button>
  `;
  renderHouses(allHouses, allHouses.length);

  D.querySelector(".see-more").classList.add("d-none");

  D.querySelectorAll("#all-houses > div").forEach((house) => {
    if (filterTags.status === "only available" && house.dataset.status !== "Available") return;

    const name = house.querySelector("[data-house-name]").textContent.toLowerCase();
    const direction = house.querySelector("[data-house-direction]").textContent.toLowerCase();
    const language = house.querySelector("[data-house-background-language]").textContent.toLowerCase();

    if (name.includes(text) || direction.includes(text) || language.includes(text)) showOrHide(house, true);
    else showOrHide(house);
  });

  let housesNotFound = true;
  D.querySelectorAll("#all-houses > div").forEach((house) => {
    if (!housesNotFound) return;
    if (house.classList.contains("d-flex")) housesNotFound = false;
  });

  if (housesNotFound) D.querySelector("#all-houses").classList.add("not-found");
  else D.querySelector("#all-houses").classList.remove("not-found");

  if (!text) {
    D.querySelector("#all-houses").innerHTML = `
      <button class="d-none mt-4 btn show-more">Show More</button>
      <button class="mt-4 btn see-more d-none" data-page="${
        D.querySelector(".see-more").dataset.page
      }">See More</button>
    `;

    for (let i = 0; i <= D.querySelector(".see-more").dataset.page; i++) renderHouses(pages[i], LIMIT);

    if (Number(D.querySelector(".see-more").dataset.page) + 1 === pages.length) {
      showOrHide(D.querySelector(".show-more"), false);
      showOrHide(D.querySelector(".see-more"), false);
    } else {
      showOrHide(D.querySelector(".show-more"), false);
      showOrHide(D.querySelector(".see-more"), true);
    }
  }

  searchDateHouse(
    D.querySelector("#search-date").value.split("-")[0].replaceAll("/", "-").trim(),
    D.querySelector("#search-date").value.split("-")[1].replaceAll("/", "-").trim()
  );
}
// ? SEARCH DATE HOUSE
function searchDateHouse(firstDay, lastDay) {
  const studentData = {
    name_s: D.querySelector("#fullname-student").textContent,
    firstd: firstDay,
    lastd: lastDay,
  };

  // * EVALUAR FECHAS Y DISPONIBILIDAD
  allHouses.forEach((house, i) => {
    if (!D.querySelector(`[data-id-home="${house.id_home}"]`)) return;

    house.roomsData.forEach((beds, room) => {
      if (beds.A.reserves.length || beds.B.reserves.length || beds.C.reserves.length) {
        const $room = D.querySelector(`[data-id-home="${house.id_home}"] [data-room-container="${room}"] > div`);
        if (!$room) return;
        const $bed1 = $room.children[0];
        const $bed2 = $room.children[1];
        const $bed3 = $room.children[2];

        if ($bed1) {
          beds.A.reserves.some((reserve) => {
            if (!verifyDates(reserve, studentData)) {
              $bed1.classList.replace("bg-success", "bg-secondary");
              return true;
            } else {
              $bed1.classList.replace("bg-secondary", "bg-success");
            }
          });
        }

        if ($bed2) {
          beds.B.reserves.some((reserve) => {
            if (!verifyDates(reserve, studentData)) {
              $bed2.classList.replace("bg-success", "bg-secondary");
              return true;
            } else {
              $bed2.classList.replace("bg-secondary", "bg-success");
            }
          });
        }

        if ($bed3) {
          beds.C.reserves.some((reserve) => {
            if (!verifyDates(reserve, studentData)) {
              $bed3.classList.replace("bg-success", "bg-secondary");
              return true;
            } else $bed3.classList.replace("bg-secondary", "bg-success");
          });
        }
      }
    });

    // * EVALUAR DISPONIBLIDAD DE CASA
    let houseAvailable = false;
    D.querySelectorAll(`[data-id-home="${house.id_home}"] [data-room-container]`).forEach((room) => {
      if (houseAvailable) return;
      room.querySelectorAll("[data-room-container] > div > span").forEach((bed) => {
        if (houseAvailable) return;
        if (bed.classList.contains("bg-success")) houseAvailable = true;
      });
    });

    if (D.querySelector(`[data-id-home="${house.id_home}"]`)) {
      if (houseAvailable) {
        D.querySelector(`[data-id-home="${house.id_home}"]`).dataset.status = "Available";
      } else {
        D.querySelector(`[data-id-home="${house.id_home}"]`).dataset.status = "Unavailable";
      }
    }
  });

  filterHouse();
}
// ? FILTER HOUSES
function filterHouse() {
  if (D.querySelector(".show-more").classList.contains("d-flex")) D.querySelector(".show-more").click();

  const housesFiltered = {
    0: new Set(),
    1: new Set(),
    2: new Set(),
    3: new Set(),
    4: new Set(),
    5: new Set(),
    6: new Set(),
    7: new Set(),
    8: new Set(),
    9: new Set(),
    10: new Set(),
    11: new Set(),
    12: new Set(),
  };

  D.querySelectorAll("#all-houses > div").forEach((house) => {
    const houseStatus = house.dataset.status;
    const typeRoom = house.querySelector(`[data-type="${filterTags.typeRoom}"]`);
    const typeBed = house.querySelector(`[data-bed="${filterTags.typeBed}"]`);
    const gender = house.querySelector("[data-house-gender-preference]").textContent.toLowerCase();
    const age = house.querySelector("[data-house-age-preference]").textContent.toLowerCase();
    const pets = house.querySelector("[data-house-pets-preference]").textContent.toLowerCase();
    const smoke = house.querySelector("[data-house-smokers-preference]").textContent.toLowerCase();
    const food = house.querySelector("[data-house-food-service]").textContent.toLowerCase();
    const diet = house.querySelector("[data-house-special-diet]").dataset.diet;

    const filtersFound = {};
    Object.values(filterTags).forEach((filter, i) => {
      if (filter) {
        if (typeRoom && filter === typeRoom.dataset.type) filtersFound[Object.keys(filterTags)[i]] = filter;
        if (typeBed && filter === typeBed.dataset.bed) filtersFound[Object.keys(filterTags)[i]] = filter;
        if (filter === gender) filtersFound[Object.keys(filterTags)[i]] = filter;
        if (filter === age) filtersFound[Object.keys(filterTags)[i]] = filter;
        if (pets === "yes" && filter === "pets yes") filtersFound[Object.keys(filterTags)[i]] = filter;
        if (pets === "no" && filter === "pets no") filtersFound[Object.keys(filterTags)[i]] = filter;
        if (filter === smoke) filtersFound[Object.keys(filterTags)[i]] = filter;
        if (food === "yes" && filter === "food yes") filtersFound[Object.keys(filterTags)[i]] = filter;
        if (food === "no" && filter === "food no") filtersFound[Object.keys(filterTags)[i]] = filter;
        if (diet && diet.includes(filter)) filtersFound[Object.keys(filterTags)[i]] = filter;
      }
    });

    housesFiltered[Object.keys(filtersFound).length].add(house);

    // * FILTRAR STATUS
    if (filterTags.status === "show all") showOrHide(house, true);
    else if (filterTags.status === "only available") {
      houseStatus === "Available" ? showOrHide(house, true) : showOrHide(house);
    }
  });

  let rank = 12;
  while (rank >= 0) {
    housesFiltered[rank].forEach((house) => {
      if (house.classList.contains("d-none")) {
        house.classList.replace("d-flex", "d-none");
        D.querySelector("#all-houses").appendChild(house);
      } else D.querySelector("#all-houses").appendChild(house);
    });

    rank--;
  }

  D.querySelector("#all-houses").appendChild(D.querySelector(".see-more"));
  D.querySelector("#all-houses").appendChild(D.querySelector(".show-more"));

  if (!D.querySelector("#search-available").checked) D.querySelector(".search-date").classList.remove("show");
  else D.querySelector(".search-date").classList.add("show");

  // * ADD TAG (TOP 10)
  top10();
}
// ? HOMESTAYS SELECTED
async function changeStep(step) {
  if (step === 1) {
    D.querySelector("#all-houses-selected").innerHTML = "";
    D.querySelector("#houses-container").classList.remove("d-none");
    D.querySelector("#all-houses-selected").classList.replace("d-flex", "d-none");
    D.querySelector("#houses-container").classList.remove("d-none");
    D.querySelector("#map").parentElement.classList.remove("show");

    D.querySelector(".back-to-panel").classList.remove("d-none");
    D.querySelector("#next-step").classList.remove("d-none");
    D.querySelector("#previous-step").classList.add("d-none");
    D.querySelector("#save-assignment").classList.add("d-none");
    D.querySelector("#save-assignment").disabled = true;
  } else if (step === 2) {
    D.querySelector("#all-houses-selected").innerHTML = `<h2 class="alert border-bottom">Homestays selected</h2><br>`;
    const $fragment = D.createDocumentFragment();
    D.querySelectorAll("#all-houses > div").forEach((house) => {
      if (house.querySelector("label input").checked) {
        const $houseClone = house.cloneNode(true);
        const $button = D.createElement("button");
        $button.id = "select-bed";
        $button.className = "btn btn-select-bed";
        $button.textContent = "Select Bed";
        $houseClone.querySelector("[data-house-name]").appendChild($button);
        $fragment.appendChild($houseClone);
      }
    });
    D.querySelector("#all-houses-selected").appendChild($fragment);
    D.querySelector("#houses-container").classList.add("d-none");
    D.querySelector("#all-houses-selected").classList.replace("d-none", "d-flex");
    D.querySelector("#map").parentElement.classList.add("show");

    const $saveButtons = D.querySelector(".save-buttons").cloneNode(true);
    $saveButtons.classList.remove("d-none");
    D.querySelector("#all-houses-selected").appendChild($saveButtons);

    D.querySelector(".back-to-panel").classList.add("d-none");
    D.querySelector("#next-step").classList.add("d-none");
    D.querySelector("#previous-step").classList.remove("d-none");
    D.querySelector("#save-assignment").classList.remove("d-none");
    D.querySelector("#save-assignment").disabled = false;

    scrollTo(0, 0);
  } else {
    try {
      _Messages.showMessage("Please Wait...", 1, false);

      const idHouses = (house) => {
        if (D.querySelectorAll("#all-houses-selected [data-id-home]")[house - 1]) {
          return D.querySelectorAll("#all-houses-selected [data-id-home]")[house - 1].dataset.idHome;
        } else return "NULL";
      };
      const formData = new FormData();
      formData.set("student_id", ID_STUDENT);
      formData.set("house1", await idHouses(1));
      formData.set("house2", await idHouses(2));
      formData.set("house3", await idHouses(3));
      formData.set("house4", await idHouses(4));
      formData.set("house5", await idHouses(5));
      const OPTIONS = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };
      const saveAssignment = await axios("../helpers/agents/send_assigment.php", OPTIONS);
      const assignmentResult = await saveAssignment.data;
      console.log(assignmentResult);
      const req = await axios("../helpers/agents/map_assigment.php", OPTIONS);
      const academy = await req.data;

      if (assignmentResult) {
        _Messages.showMessage("The homestay selection has been sent", 2, false);
        setTimeout(() => (window.location.href = "directory_students"), 2000);
      } else throw "An unexpected error has occurred, please reload";
    } catch (error) {
      _Messages.showMessage(error, 4, false);
    }
  }
}
// TODO FUNCTION UTILITIES
/** // ? FUNCION QUE RENDERIZA LAS CASAS LA INFORMACION OBTENIDA
 * @param {Array} houses Arreglo de casas a renderizar
 * @param {Number} limit Limite de casas a renderizar
 */
async function renderHouses(houses, limit = houses.length) {
  D.querySelector("#all-houses").style.opacity = "0";
  D.querySelector("#all-houses").style.transform = "translateY(-10rem)";
  D.querySelector(".loading-houses").style.opacity = "1";

  const $fragment = D.createDocumentFragment();

  // * FILL DATA HOUSE
  houses.some((house, i) => {
    if (i === limit) return true;
    const $clone = D.importNode($template, true);

    let roomNumber = 1;
    house.roomsData.forEach((room, i) => {
      const $room = $templateBedrooms.firstElementChild.cloneNode(true);
      $room.querySelector("[data-room-name]").textContent = `Room ${roomNumber} - ${room.A.typeRoom}`;
      $room.dataset.type = room.A.typeRoom.toLowerCase();

      let isAvalible = false;

      const fillRoomData = (data, bed) => {
        if (data.type === "Bunk-bed") data.type = "Bunk";
        $room.querySelector(bed).textContent = data.type;
        $room.querySelector(bed).dataset.bed = data.type.toLowerCase();

        if (data.status === "Avalible") $room.querySelector(bed).classList.replace("bg-secondary", "bg-success");
        else $room.querySelector(bed).classList.replace("bg-success", "bg-secondary");

        if (data.type === "NULL" || data.type === "Disabled") $room.querySelector(bed).remove();
        isAvalible = true;
      };

      // * DELETE BEDS DISABLED OR NULL
      if (room.A.type !== "NULL" || room.B.type !== "NULL" || room.C.type !== "NULL") {
        fillRoomData(room.A, "[data-room-bed-1]");
        fillRoomData(room.B, "[data-room-bed-2]");
        fillRoomData(room.C, "[data-room-bed-3]");
      }

      if (isAvalible) $clone.querySelector("[data-house-bedrooms] #bedrooms").appendChild($room);
      roomNumber++;
    });

    $clone.querySelector("[data-id-home]").dataset.idHome = house.id_home;
    $clone.querySelector("[data-house-profile]").dataset.houseProfile = house.id_home;
    $clone.querySelector("img").src = "https://homebor.com/" + house.phome;
    $clone.querySelector("[data-house-name]").textContent = `${house.h_name}`;
    $clone.querySelector("[data-house-name]").innerHTML += `<input id="${house.id_home}" type="checkbox" hidden>`;
    $clone.querySelector(
      "[data-house-name]"
    ).innerHTML += `<span><svg viewBox="0 0 32 32"aria-hidden="true" role="presentation" focusable="false"><path fill="none" d="m4 16.5 8 8 16-16"></path></svg>
    </span>`;
    $clone.querySelector("[data-house-direction]").textContent = `${house.dir} ${house.city}, ${house.state}`;
    $clone.querySelector("[data-house-gender-preference]").textContent = house.g_pre;
    $clone.querySelector("[data-house-age-preference]").textContent = house.ag_pre;
    $clone.querySelector("[data-house-rooms]").textContent = house.room;
    $clone.querySelector("[data-house-smokers-preference]").textContent = house.smoke;
    $clone.querySelector("[data-house-pets-preference]").textContent = house.pet;
    $clone.querySelector("[data-house-background-language]").textContent = house.backl;
    const mealService = (value) => (!value || value === "NULL" ? "No" : value);
    $clone.querySelector("[data-house-food-service]").textContent = mealService(house.m_service);
    if (
      house.vegetarians.toLowerCase() === "yes" ||
      house.halal.toLowerCase() === "yes" ||
      house.kosher.toLowerCase() === "yes" ||
      house.lactose.toLowerCase() === "yes" ||
      house.gluten.toLowerCase() === "yes" ||
      house.pork.toLowerCase() === "yes"
    ) {
      $clone.querySelector("[data-house-special-diet]").textContent = "Yes";
      const $specialDietData = `
      ${house.vegetarians.toLowerCase() === "yes" ? "vegetarian" : "none"}, 
      ${house.halal.toLowerCase() === "yes" ? "halal" : "none"}, 
      ${house.kosher.toLowerCase() === "yes" ? "kosher" : "none"}, 
      ${house.lactose.toLowerCase() === "yes" ? "lactose" : "none"}, 
      ${house.gluten.toLowerCase() === "yes" ? "gluten" : "none"}, 
      ${house.pork.toLowerCase() === "yes" ? "pork" : "none"}`;
      $clone.querySelector("[data-house-special-diet]").dataset.diet = $specialDietData;
    } else $clone.querySelector("[data-house-special-diet]").textContent = "No";

    $clone.querySelectorAll("#bedrooms > div").forEach((room, i) => (room.dataset.roomContainer = roomsColors[i]));
    $fragment.appendChild($clone);
  });

  setTimeout(() => (D.querySelector("#all-houses").style.transform = "translateY(10rem)"), 300);
  setTimeout(() => {
    D.querySelector(".loading-houses").style.opacity = "0";
    D.querySelector("#all-houses").style.opacity = "1";
    D.querySelector("#all-houses").style.transform = "translateY(0)";
  }, 600);

  // * ADD ALL HOUSES TO DOM
  D.querySelector("#all-houses").insertBefore($fragment, D.querySelector("#all-houses").firstChild);

  // * ADD TAG (TOP 10)
  top10();
}
// ? VERIFY HOUSES RESERVES
function verifyDates(reserve, student) {
  let available = true;
  const FD_STUDENT = new Date(student.firstd).getTime();
  const LD_STUDENT = new Date(student.lastd).getTime();

  const FD_RESERVE = new Date(reserve[2]).getTime();
  const LD_RESERVE = new Date(reserve[3]).getTime();

  if (FD_RESERVE >= FD_STUDENT && LD_RESERVE <= LD_STUDENT) available = false;
  else if (FD_RESERVE <= FD_STUDENT && LD_RESERVE >= LD_STUDENT) available = false;
  else if (FD_RESERVE > FD_STUDENT && FD_RESERVE < LD_STUDENT) available = false;
  else if (LD_RESERVE > FD_STUDENT && LD_RESERVE < LD_STUDENT) available = false;
  return available;
}
// ? CHECKBOX AND TAGS CONTROL
function checkboxControl(value, filter, parent) {
  let quitFilter = true;
  parent.querySelectorAll("input").forEach((input) => {
    if (!["vegetarian", "halal", "kosher", "lactose", "gluten", "pork"].includes(filter)) {
      input.value !== value ? (input.checked = false) : {};
      input.checked ? (quitFilter = false) : {};
    } else if (input.value === filter && input.checked) quitFilter = false;
  });
  quitFilter ? (filterTags[filter] = "") : (filterTags[filter] = value);

  Object.values(filterTags).forEach((filter, i) => {
    if (filter) {
      const $span = `<span class="mb-1 mr-2" data-tag="${i}">${value}<span id="quit-filter">X</span></span>`;
      if (value === filter) {
        if (D.querySelector(`#filter-tags [data-tag="${i}"]`))
          D.querySelector(`#filter-tags [data-tag="${i}"]`).remove();
        D.querySelector("#filter-tags").innerHTML += $span;

        let quitFilter = true;
        parent.querySelectorAll("input").forEach((input) => (input.checked ? (quitFilter = false) : {}));
        if (quitFilter) D.querySelector(`#filter-tags [data-tag="${i}"]`).remove();
      }
    } else if (!filterTags[filter] && D.querySelector(`#filter-tags [data-tag="${i}"]`)) {
      D.querySelector(`#filter-tags [data-tag="${i}"]`).remove();
    }
  });

  filterHouse();
}
/** // ? SHOW OR HIDE ELEMENT
 * @param {Element} elem Element to show or hide
 * @param {Boolean} add True = show element / False = hide element
 */
function showOrHide(elem, add) {
  add ? elem.classList.replace("d-none", "d-flex") : elem.classList.replace("d-flex", "d-none");
}
// ? DEFINE TOP 10
function top10() {
  D.querySelectorAll("#all-houses > div").forEach((house, i) => {
    if (i <= 9) house.querySelector("[data-house-status]").textContent = "Top 10";
    else house.querySelector("[data-house-status]").textContent = "Available";
  });
  // * SEE MORE
  const currentPage = Number(D.querySelector(".see-more").dataset.page);
  if (pages[currentPage] && pages[currentPage].length < LIMIT) {
    D.querySelector(".see-more").classList.add("d-none");
  } else if (pages[currentPage] && pages[currentPage].length === LIMIT) {
    if (!pages[currentPage + 1] || !pages[currentPage + 1].length) {
      D.querySelector(".see-more").classList.add("d-none");
    }
  }
}
// ? MOBILE SHOW OR HIDE ALL FILTERS
const allFiltersMobile = (add) => {
  if (add) {
    D.body.style.overflowY = "hidden";
    D.querySelector("#all-filters").classList.add("show");
  } else {
    D.body.style.overflowY = "scroll";
    D.querySelector("#all-filters").classList.remove("show");
  }
};
// ? SEND RESERVE
const sendReserve = async (action, btn) => {
  if (action === "Select bed") {
    D.querySelector("#modal-dynamic").classList.add("show");
    D.querySelector(
      "#modal-dynamic"
    ).innerHTML = `<article data-id-home="${btn.parentElement.parentElement.parentElement.parentElement.dataset.idHome}" class="col-6 col-lg-5 py-2 px-5 shadow rounded select-bed-modal">
    <h3 class="text-center">Select Bed</h3>
  </article>`;
    let $bedRooms = "";
    const bedClone = (room, selector, number) => {
      let bedClass = "";
      if (room.querySelector(selector).textContent === "Bunk-bed") room.querySelector(selector).textContent = "Bunk";
      if (room.querySelector(selector).classList.contains("bg-secondary")) {
        bedClass = "bg-secondary";
        return `<label for="${room.dataset.roomContainer}-${number}" class="${bedClass} btn" title="Occupied">
              ${room.querySelector(selector).textContent}
              </label>`;
      } else {
        return `<input hidden type="checkbox" id="${room.dataset.roomContainer}-${number}" />
              <label for="${room.dataset.roomContainer}-${number}" class="${bedClass} btn" title="Available">
                ${room.querySelector(selector).textContent}
              </label>`;
      }
    };
    btn.parentElement.parentElement.parentElement.querySelectorAll("#bedrooms > div").forEach((room) => {
      $bedRooms += `<aside class="beds-to-select" data-room-color="${room.dataset.roomContainer}">
      <h4>${room.querySelector("[data-room-name]").textContent}</h4>
      <div>`;
      if (room.querySelector("[data-room-bed-1]")) $bedRooms += bedClone(room, "[data-room-bed-1]", 1);
      if (room.querySelector("[data-room-bed-2]")) $bedRooms += bedClone(room, "[data-room-bed-2]", 2);
      if (room.querySelector("[data-room-bed-3]")) $bedRooms += bedClone(room, "[data-room-bed-3]", 3);
      $bedRooms += "</div></aside>";
    });
    $bedRooms += `<button id="send-reserve" class="btn">Send Reserve</button>`;
    $bedRooms.replace("Bunk-bed", "Bunk");
    D.querySelector("#modal-dynamic article").innerHTML += $bedRooms;
  } else {
    let available = false;
    btn.parentElement.querySelectorAll("aside input").forEach((input) => (input.checked ? (available = input) : {}));

    if (available) {
      _Messages.showMessage("Please Wait...", 1, false);
      try {
        const idHome = btn.parentElement.dataset.idHome;

        const formData = new FormData();
        formData.set("homestay_id", idHome);
        formData.set("student_id", ID_STUDENT);
        formData.set("room_color", available.id.split("-")[0]);
        formData.set("room_bed", available.id.split("-")[1]);

        const OPTIONS = {
          method: "POST",
          header: { "Content-type": "application/json; charset=utf-8" },
          data: formData,
        };

        const sendReserve = await axios("../helpers/agents/send_reserve.php", OPTIONS);
        const result = await sendReserve.data;

        if (result === "ok") {
          _Messages.showMessage("You have sent the request successfully", 2, false);
          setTimeout(() => (W.location.href = "directory_students"), 2000);
        } else throw "An error has occurred, make sure to send all the required data";
      } catch (error) {
        _Messages.showMessage(error, 4, false);
      }
    }
  }
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", (e) => {});

// ! CLICK
D.addEventListener("click", (e) => {
  // * SELECT HOUSE
  if (e.target.matches("#all-houses input")) selectHouse(e.target);

  // * REMOVE HOUSE SELECTED
  if (e.target.matches(".current-house input") && !e.target.checked) {
    D.querySelector(`input[id="${e.target.id.split("-")[1]}"]`).click();
  }
  // * REMOVE HOUSE SELECTED
  if (e.target.matches("#all-houses-selected input") && !e.target.checked) {
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
    D.querySelector(`#house-${e.target.id}`).click();
  }

  // * MOBILE SHOW ALL FILTERS
  if (e.target.matches(".icon-filter-button")) allFiltersMobile(true);

  // * MOBILE HIDE ALL FILTERS
  if (e.target.matches(".hide-filters")) allFiltersMobile(false);

  // * SEARCH HOUSE BY DATE
  if (e.target.matches(".applyBtn")) {
    searchDateHouse(
      D.querySelector("#search-date").value.split("-")[0].replaceAll("/", "-").trim(),
      D.querySelector("#search-date").value.split("-")[1].replaceAll("/", "-").trim()
    );
  }
  // * SHOW HOUSE MORE INFO
  if (e.target.matches("[data-more-info-button]")) {
    // * SHOW INFORMATION ABOUT THE HOUSE
    e.target.parentElement.parentElement.parentElement.classList.toggle("show-more-info");
  }

  // * LINK TO HOUSE PROFILE
  if (e.target.matches("[data-house-profile] img") || e.target.matches("[data-house-profile] div")) {
    window.open(`detail?art_id=${e.target.parentElement.dataset.houseProfile}`, "_blank");
  }

  // * SELECT FILTER
  if (e.target.matches("input[type=checkbox]") && e.target.id.includes("search")) {
    checkboxControl(e.target.value, e.target.dataset.filter, e.target.parentElement.parentElement);

    const specialDietControl = (show) => {
      const $specialDiet = D.querySelector(".search-special-diet");
      if (show) $specialDiet.classList.add("show");
      else {
        $specialDiet.classList.remove("show");
        $specialDiet.querySelectorAll("input").forEach((input) => (input.checked = false));
      }
    };
    if (e.target.value === "food yes") e.target.checked ? specialDietControl(true) : specialDietControl(false);
    if (e.target.value === "food no") specialDietControl(false);

    if (D.querySelector("#search-house").value) searchHouse(D.querySelector("#search-house").value.toLowerCase());

    if (e.target.id === "search-available" && !e.target.checked) D.querySelector("#search-all").click();
  }

  // * QUIT FILTER
  if (e.target.id === "quit-filter") {
    D.querySelectorAll('input[type="checkbox"]').forEach((input) => {
      if (input.value.toLowerCase() === e.target.parentElement.textContent.replace("X", "")) {
        input.checked = false;
        filterTags[input.dataset.filter] = "";
      }
    });
    e.target.parentElement.remove();

    filterHouse();
  }

  // * SEE MORE BUTTON
  if (e.target.matches(".see-more")) {
    // ** VALIDAR SIGUIENTE PAGINA EXISTENTE
    if (!pages[Number(e.target.dataset.page) + 1]) return;

    // ** AUMENTAR PAGINA ACTUAL
    e.target.dataset.page = new Number(e.target.dataset.page) + 1;

    // ** RENDERIZAR Y FILTRAR PAGINA
    renderHouses(pages[e.target.dataset.page]);
    filterHouse();

    // ** REEMPLAZAR "-" POR "/" EN EL DATE FROM - TO Y APLICARLO
    searchDateHouse(
      D.querySelector("#search-date").value.split("-")[0].replaceAll("/", "-").trim(),
      D.querySelector("#search-date").value.split("-")[1].replaceAll("/", "-").trim()
    );

    // ** OCULTAR BOTON SEE MORE AL LIMITE DE PAGINACIONES
    if (Number(e.target.dataset.page) + 1 === pages.length) e.target.classList.add("d-none");
  }

  // * SHOW MORE BUTTON
  if (e.target.matches(".show-more")) {
    showOrHide(e.target, false);

    if (pages.length === Number(D.querySelector(".see-more").dataset.page) + 1) {
      showOrHide(D.querySelector(".see-more"), false);
    } else showOrHide(D.querySelector(".see-more"), true);

    D.querySelectorAll("#all-houses > div").forEach((house) => showOrHide(house, true));
  }

  // * SELECT BED RESTRICTION
  if (e.target.matches(".select-bed-modal input")) {
    e.target.parentElement.parentElement.parentElement.querySelectorAll("input").forEach((input) => {
      if (e.target.checked && input !== e.target) input.checked = false;
    });
  }

  // * SELECT BED TO RESERVE
  if (e.target.id === "select-bed") sendReserve("Select bed", e.target);

  // * SEND RESERVE TO HOUSE
  if (e.target.id === "send-reserve") sendReserve("Send reserve", e.target);

  // * BACK TO SELECTION
  if (e.target.id === "previous-step") changeStep(1);

  // * SEE YOUR SELECTION AND MAP
  if (e.target.id === "next-step") changeStep(2);

  // * SEND SELECTION TO THE STUDENT
  if (e.target.id === "save-assignment") changeStep(3);
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  if (e.key !== "Tab" && e.key !== "Alt" && e.target.id === "search-house") searchHouse(e.target.value.toLowerCase());
});

// ! CHANGE
D.addEventListener("change", (e) => {});
