// TODO VARIABLES
const D = document;
const W = window;
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID_HOME = searchParams.get("art_id");

// TODO FUNCIONES
export default class AdditionalInformation {
  constructor() {}
  // ? FUNCION QUE OBTIENE DE LA BASE DE DATOS LA INFORMACION ADICIONAL
  async getAdditionalInformation() {
    try {
      const formData = new FormData();
      formData.set("homestay_id", ID_HOME);

      const OPTIONS = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("homedit_data.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";
      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

      await this.renderAdditionalInformation("description", data.additionalInformation.description);
      await this.renderAdditionalInformation("preferences", data.additionalInformation.preferences);
      await this.renderAdditionalInformation("specialDiet", data.additionalInformation.specialDiet);
    } catch (error) {
      console.error(error);
    }
  }
  /** // ? FUNCION QUE RENDERIZA Y RELLENA LOS CAMPOS CON LA INFORMACION OBTENIDA
   * @param {String} title Detalles de los campos a rellenar
   * @param {Object|Array} data Datos a insertar
   */
  async renderAdditionalInformation(title, data) {
    if (title === "description") D.querySelector("#house-description").value = data;
    if (title === "preferences") {
      data.academy[0].forEach((academy, i) => {
        const $clone = D.createElement("option");
        for (const key in academy[0]) {
          $clone.textContent = `${academy[0]["name_a"]}, ${academy[0]["dir_a"]}`;
          $clone.value = academy[0]["id_ac"];
        }
        D.querySelector("#academy").appendChild($clone);
      });
      D.querySelector("#academy").value = data.academy[1].replaceAll(" ", "");
      D.querySelector("#preferences-gender").value = data.gender;
      D.querySelector("#preferences-age").value = data.age;
      D.querySelector("#smokers-politics").value = data.smokers;
      D.querySelector("#meals-service").value = data.mealsService;
      D.querySelector("#total-years").value = data.totalYears;

      D.querySelector("#pets").value = data.pets.response;
      if (data.pets.response.toLowerCase() === "yes") D.querySelector("#total-pets").value = data.pets.totalPets;
      if (data.pets.dog.toLowerCase() === "yes") D.querySelector(`#dog`).checked = true;
      if (data.pets.cat.toLowerCase() === "yes") D.querySelector(`#cat`).checked = true;
      if (data.pets.other.toLowerCase() === "yes") {
        D.querySelector(`#other`).checked = true;
        D.querySelector(`#specify-pet`).value = data.pets.kindPet;
      }
    }
    if (title === "specialDiet") {
      Object.keys(data).forEach((key) =>
        data[key].toLowerCase() === "yes" ? (D.querySelector(`#${key}`).checked = true) : {}
      );
    }
  }

  // ! SAVE ADDITIONAL INFORMATION
  async saveAdditionalInformation(inputs, selects, textarea) {
    try {
      const roomData = new FormData();
      roomData.set("additionalInformation", true);
      roomData.set("homestay_id", ID_HOME);
      roomData.set("description", textarea.value);
      roomData.set("academy", selects[0].value);
      roomData.set("gender", selects[1].value);
      roomData.set("age", selects[2].value);
      roomData.set("smokers", selects[3].value);
      roomData.set("meals", selects[4].value);
      roomData.set("totalYears", inputs[0].value);

      roomData.set("pet", selects[5].value);
      roomData.set("totalPets", inputs[1].value);
      inputs.forEach((input) => {
        if (input.id === "dog") input.checked ? roomData.set("dog", "yes") : roomData.set("dog", "no");
        if (input.id === "cat") input.checked ? roomData.set("cat", "yes") : roomData.set("cat", "no");
        if (input.id === "other") {
          if (input.checked) {
            roomData.set("other", "yes");
            roomData.set("specifyPets", inputs[5].value);
          } else {
            roomData.set("other", "no");
            roomData.set("specifyPets", "NULL");
          }
        }
      });

      if (D.querySelector("#meals-service").value === "Yes") {
        inputs[6].checked ? roomData.set("vegetarians", "yes") : roomData.set("vegetarians", "no");
        inputs[7].checked ? roomData.set("halal", "yes") : roomData.set("halal", "no");
        inputs[8].checked ? roomData.set("kosher", "yes") : roomData.set("kosher", "no");
        inputs[9].checked ? roomData.set("lactose", "yes") : roomData.set("lactose", "no");
        inputs[10].checked ? roomData.set("gluten", "yes") : roomData.set("gluten", "no");
        inputs[11].checked ? roomData.set("pork", "yes") : roomData.set("pork", "no");
      }

      const options = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("./edit-admin-propertie.php", options);
      const data = await res.data;
    } catch (error) {
      console.error("Error en Additional Information", error);
    }
  }

  /** // ? MUESTRA U OCULTA LA SPECIAL DIET
   * @param {Element} input Elemento select de servicio de comida (meals service)
   */
  static specialDiet(input) {
    let $h3;
    const $specialDiet = D.querySelector(".special-diet");

    D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? ($h3 = h3) : {}));

    if (input.value === "Yes") {
      $h3.classList.remove("d-none");
      $specialDiet.classList.replace("d-none", "d-flex");
    } else {
      $h3.classList.add("d-none");
      $specialDiet.classList.replace("d-flex", "d-none");
    }
  }

  // ? FUNCION QUE MUESTRA LAS RESPUESTAS DE LAS PREGUNTAS DE MASCOTAS
  static petShowResponses() {
    if (D.querySelector("#pets").value.toLowerCase() === "yes") {
      D.querySelector(".pet-response").classList.add("show");
      D.querySelector(".pet-response").classList.add("response-1");
      D.querySelector(".pet-response").classList.add("response-2");
      if (D.querySelector("#other").checked) D.querySelector(".pet-response").classList.add("response-3");
      else D.querySelector(".pet-response").classList.remove("response-3");
    } else {
      D.querySelector(".pet-response").classList.remove("show", "response-1", "response-2", "response-3");
    }
  }
}

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  setTimeout(() => {
    D.querySelector("#pets").value.toLowerCase() === "yes" ? AdditionalInformation.petShowResponses() : {};
    D.querySelector("#other").checked ? AdditionalInformation.petShowResponses() : {};
    if (D.querySelector("#meals-service").value === "No") {
      D.querySelector(".special-diet").classList.replace("d-flex", "d-none");
      D.querySelectorAll("h3").forEach((h3) => (h3.textContent === "Special Diet" ? h3.classList.add("d-none") : {}));
    }
  }, 1000);
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? SHOW OR HIDE SPECIFY PET
  if (e.target.id === "other") AdditionalInformation.petShowResponses();
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? SHOW OR HIDE SPECIAL DIET
  if (e.target.matches("#meals-service")) AdditionalInformation.specialDiet(e.target);
  // ? SHOW OR HIDE PET RESPONSE
  if (e.target.id === "pets") AdditionalInformation.petShowResponses();
});
