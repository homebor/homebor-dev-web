//TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

//TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? TEMPLATES
const $templateHouseList = D.querySelector("#template_house_list").content.cloneNode(true);

const pages = [];
const allHouses = [];

const filters = {
  status: "",
  date: "",
  mainCity: "",
  typeRoom: "",
  gender: "",
  food_s: "",
  diet: "",
  pets: "",
  typeBed: "",
  age: "",
  language: "",
  smoke: "",
};

//TODO FUNCIONES
class HomestayDirectory {
  constructor() {
    this.$templateHouseList = $templateHouseList;

    this.showOrHide = (house, show) => {
      if (show) house.classList.replace("d-none", "d-flex");
      else house.classList.replace("d-flex", "d-none");
    };
  }

  async housesList() {
    const jsonHouseDirectory = await fetch("./get_directory_houses.php");
    const resultHouseDiretory = await jsonHouseDirectory.json();
    const ROOMS_INFORMATION = [];

    for (let i = 0; i < resultHouseDiretory.length; i += 30) pages.push(resultHouseDiretory.slice(i, i + 30));

    allHouses.push(...resultHouseDiretory);

    D.querySelector(".pag").textContent = "0";

    if (D.querySelector(".pag").textContent === "0" && pages.length == 1) {
      D.querySelector("#btn__more").classList.replace("d-flex", "d-none");
    }
    this.renderHouses(pages[0], pages[0].length);
  }

  renderHouses(houses, count) {
    const $fragment = D.createDocumentFragment();

    houses.forEach((house) => {
      const $clone = D.importNode(this.$templateHouseList, true);
      const TYPE_ROOM = [
        house.h_rooms.type1,
        house.h_rooms.type2,
        house.h_rooms.type3,
        house.h_rooms.type4,
        house.h_rooms.type5,
        house.h_rooms.type6,
        house.h_rooms.type7,
        house.h_rooms.type8,
      ];

      const TYPE_BED = [
        house.h_rooms.bed1,
        house.h_rooms.bed1_2,
        house.h_rooms.bed1_3,
        house.h_rooms.bed2,
        house.h_rooms.bed2_2,
        house.h_rooms.bed2_3,
        house.h_rooms.bed3,
        house.h_rooms.bed3_2,
        house.h_rooms.bed3_3,
        house.h_rooms.bed4,
        house.h_rooms.bed4_2,
        house.h_rooms.bed4_3,
        house.h_rooms.bed5,
        house.h_rooms.bed5_2,
        house.h_rooms.bed5_3,
        house.h_rooms.bed6,
        house.h_rooms.bed6_2,
        house.h_rooms.bed6_3,
        house.h_rooms.bed7,
        house.h_rooms.bed7_2,
        house.h_rooms.bed7_3,
        house.h_rooms.bed8,
        house.h_rooms.bed8_2,
        house.h_rooms.bed8_3,
      ];

      const TYPE_DIET = [
        house.h_vegetarians,
        house.h_halal,
        house.h_kosher,
        house.h_lactose,
        house.h_gluten,
        house.h_pork,
      ];

      const EVENTS = [];

      if (house.h_events) {
        for (let i = 0; i < house.h_events.length; i++) {
          const element = house.h_events[i];
          EVENTS.push(element);
        }
      }
      $clone.querySelector(".name_house").textContent = house.h_name;
      if (house.h_photo === "NULL" || house.h_photo === "")
        $clone.querySelector(".img__house").src = "../assets/emptys/frontage-empty.png";
      else $clone.querySelector(".img__house").src = "https://homebor.com/" + house.h_photo;

      if (house.h_status === "Available") $clone.querySelector(".card-house").setAttribute("data-available", true);
      $clone.querySelector("#status").textContent = house.h_status;
      $clone.querySelector(".text__direction").textContent = house.h_address;
      $clone.querySelector(".gender").textContent = house.h_gender;
      $clone.querySelector(".age").textContent = house.h_age;
      $clone.querySelector(".rooms").textContent = house.h_bedrooms;
      $clone.querySelector(".language").textContent = house.h_language;
      $clone.querySelector(".smokers_politics").textContent = house.h_smoke;
      $clone.querySelector(".food_s").textContent = house.h_foods;
      $clone.querySelector(".diet").textContent = house.h_diet;
      $clone.querySelector(".pets").textContent = house.h_pets;
      $clone.querySelector("#profile").href = `detail?art_id=${house.h_id}`;
      $clone.querySelector("#edit").href = `homedit?art_id=${house.h_id}`;
      $clone.querySelector(".type_room").textContent = TYPE_ROOM;
      $clone.querySelector(".type_bed").textContent = TYPE_BED;
      $clone.querySelector(".type_diet").textContent = TYPE_DIET;
      $clone.querySelector(".main_city").textContent = house.h_mCity;

      EVENTS.forEach((event) => {
        $clone.querySelector(".event_start").textContent += event.e_start + " - " + event.e_end + ",";
      });

      $fragment.appendChild($clone);
    });

    D.querySelector(".section__houses_list").appendChild($fragment);
  }

  pagination(btn, pagination) {
    if (Number(pagination.textContent) > 0 && btn.dataset.remember === "true") {
      D.querySelector(".section__houses_list").innerHTML = "";

      // for (let i = 0; i <= Number(pagination.textContent); i++) this.renderHouses(pages[i]);
      for (let i = 0; i <= Number(pagination.textContent); i++) this.renderHouses(pages[i]);
    } else if (pagination.textContent < pages.length) {
      const page = parseFloat(pagination.textContent) + 1;
      pagination.textContent = page;
      this.renderHouses(pages[page]);
    }

    if (pagination.textContent == parseFloat(pages.length) - 1) {
      btn.classList.replace("d-flex", "d-none");
    }
    /*const $parentLinks = D.querySelector("#div__buttons_links");
      const $buttonsLinks = $parentLinks.querySelectorAll("button");

      if (btn.id === "btn__next") {
        if ($pagination.textContent < pages.length) {
          const page = parseFloat($pagination.textContent) + 1;
          $pagination.textContent = page;
          $buttonsLinks.forEach((button) => {
            if (button.dataset.page == page) button.classList.add("active");
            else button.classList.remove("active");
          });
          this.renderHouses(pages[page - 1]);
        }
      }
      if (btn.id === "btn__prev") {
        if ($pagination.textContent > 1) {
          const page = parseFloat($pagination.textContent) - 1;
          $pagination.textContent = page;

          $buttonsLinks.forEach((button) => {
            if (button.dataset.page == page) button.classList.add("active");
            else button.classList.remove("active");
          });

          this.renderHouses(pages[page - 1]);
        }
      }
      if (btn.id === "btn__links") {
        const page = btn.dataset.page;
        this.renderHouses(pages[page - 1]);
      } */
  }

  overArrow(btn, action) {
    if (action === "over") {
      if (btn.id === "btn__next") {
        btn.querySelector(".arrow__next").classList.replace("arrow__next", "arrow__next-active");
      } else if (btn.id === "btn__prev") {
        btn.querySelector(".arrow__prev").classList.replace("arrow__prev", "arrow__prev-active");
      }
    } else {
      if (btn.id === "btn__next") {
        btn.querySelector(".arrow__next-active").classList.replace("arrow__next-active", "arrow__next");
      } else if (btn.id === "btn__prev") {
        btn.querySelector(".arrow__prev-active").classList.replace("arrow__prev-active", "arrow__prev");
      }
    }
  }

  searchname(text) {
    console.log(D.querySelector("#btn__more"));
    if (text) {
      if (D.querySelector(".section__houses_list").dataset.all === "no") {
        D.querySelector(".section__houses_list").innerHTML = "";
        this.renderHouses(allHouses);
        D.querySelector(".section__houses_list").dataset.all = "yes";
      }
      D.querySelector("#btn__more").classList.replace("d-flex", "d-none");
      D.querySelectorAll(".section__houses_list > div").forEach((house) => {
        const name = house.querySelector(".name_house").textContent.toLowerCase();
        const direction = house.querySelector(".text__direction").textContent.toLowerCase();
        if (name.includes(text) || direction.includes(text)) this.showOrHide(house, true);
        else this.showOrHide(house, false);
      });
    } else {
      D.querySelector(".section__houses_list").innerHTML = "";
      this.renderHouses(pages[0]);
      D.querySelector("#btn__more").dataset.remember = true;
      if (pages.length > 1) this.showOrHide(D.querySelector("#btn__more"), true);
      D.querySelector(".section__houses_list").dataset.all = "no";
    }
  }

  // ? SEARCH HOUSES
  searchHouse(text) {
    /* const filterOrder = []; */
    const filterOrder = {
      11: new Set(),
      10: new Set(),
      9: new Set(),
      8: new Set(),
      7: new Set(),
      6: new Set(),
      5: new Set(),
      4: new Set(),
      3: new Set(),
      2: new Set(),
      1: new Set(),
    };
    D.querySelector("#btn__more").classList.add("d-none");

    if (D.querySelector(".section__houses_list").childElementCount !== allHouses.length && text)
      this.renderHouses(allHouses);

    D.querySelectorAll(".section__houses_list > div").forEach((house) => {
      /* const statusHouse = house.querySelector(".card-house").dataset.status.toLocaleLowerCase(); */
      const status = house.querySelector("#status").textContent.toLocaleLowerCase();
      const gender = house.querySelector(".gender").textContent.toLowerCase();
      const age = house.querySelector(".age").textContent.toLowerCase();
      const name = house.querySelector(".name_house").textContent.toLowerCase();
      const direction = house.querySelector(".text__direction").textContent.toLowerCase();
      const pets = house.querySelector(".pets").textContent.toLowerCase();
      const smoke = house.querySelector(".smokers_politics").textContent.toLowerCase();
      const foodS = house.querySelector(".food_s").textContent.toLowerCase();
      const room = house.querySelector(".type_room").textContent.toLowerCase();
      const bed = house.querySelector(".type_bed").textContent.toLowerCase();
      const diet = house.querySelector(".type_diet").textContent.toLowerCase();
      const language = house.querySelector(".language").textContent.toLowerCase();
      const events = house.querySelector(".event_start").textContent.toLowerCase();
      const mainCity = house.querySelector(".main_city").textContent.toLowerCase();
      const typeRoom = room.split(",");
      const typeBed = bed.split(",");
      const typeDiet = diet.split(",");
      const dateFilter = filters.date.split(" - ");

      const eventsAll = events.split(",");

      let resultStatus = "";

      if (status === "available") {
        resultStatus = "true";
      } else {
        resultStatus = "";
      }

      const filtersTraveled = [];
      const filtersFound = [];
      Object.values(filters).forEach((filter) => {
        if (filter) {
          if (eventsAll[0]) {
            if (!filtersTraveled.includes("reserves")) filtersFound.push("no-reserved");
            /* console.log("La casa:", name, "tiene:", eventsAll.length - 1, "reservas"); */
            for (let i = 0; i < eventsAll.length; i++) {
              const element = eventsAll[i];

              const datesEvents = element.split(" - ");
              const startFilter = new Date(datesEvents[0]).getTime();
              const endFilter = new Date(datesEvents[1]).getTime();
              if (
                (dateFilter[0] >= startFilter && dateFilter[1] <= endFilter) ||
                (dateFilter[0] <= startFilter && dateFilter[1] >= endFilter) ||
                (dateFilter[0] >= startFilter && dateFilter[0] <= endFilter) ||
                (dateFilter[1] >= startFilter && dateFilter[1] <= endFilter)
              ) {
                filtersFound[filtersFound.indexOf("no-reserved")] = "reserved";
                break;
              }
            }
          } else if (!filtersTraveled.includes("reserves")) filtersFound.push("no-reserved");

          if (gender === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (foodS === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (age === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (name === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (direction === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (pets === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (smoke === filter && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (mainCity.includes(filter) && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (typeRoom.includes(filter) && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (typeBed.includes(filter) && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (typeDiet.includes(filter) && !filtersFound.includes(filter)) filtersFound.push(filter);
          if (language.includes(filter) && !filtersFound.includes(filter)) filtersFound.push(filter);

          if (
            typeRoom.includes(filter) ||
            gender === filter ||
            foodS === filter ||
            age === filter ||
            name === filter ||
            direction === filter ||
            pets === filter ||
            smoke === filter ||
            mainCity.includes(filter) ||
            typeRoom.includes(filter) ||
            typeBed.includes(filter) ||
            typeDiet.includes(filter) ||
            language.includes(filter) ||
            filters.status
          )
            this.showOrHide(house, true);
          else this.showOrHide(house, false);

          if (filters.date !== filter) filtersTraveled.push(filter);
          if (!filtersTraveled.includes("reserves")) filtersTraveled.push("reserves");
        }
      });

      if (filters.status && !house.hasAttribute("data-available")) house.classList.replace("d-flex", "d-none");

      house.dataset.reserved = "no";

      if (filtersTraveled.length > 0) this.showOrHide(D.querySelector("#btn__more"), false);
      else this.showOrHide(D.querySelector("#btn__more"), true);

      if (filtersFound.length === filtersTraveled.length) house.dataset.perfect = true;
      else house.dataset.perfect = false;

      if (filtersFound.length > 0) {
        if (!filtersFound.includes("reserved")) {
          filterOrder[filtersFound.length].add(house);
          house.dataset.reserved = "no";
        } else {
          house.dataset.reserved = "yes";
          console.log(name, filtersFound, filtersTraveled);
        }
      }
    });

    D.querySelectorAll(".section__houses_list > div").forEach((house) => {
      if (
        D.querySelector("#checkbox__show_all").checked == true &&
        (house.dataset.reserved === "no" || house.dataset.reserved === "yes")
      ) {
        this.showOrHide(house, true);
      } else if (
        D.querySelector("#checkbox__show_all").checked == false &&
        D.querySelector("#checkbox__filter_only_available").checked == false
      ) {
        if (house.dataset.perfect === "true") this.showOrHide(house, true);
        else this.showOrHide(house, false);
      } else if (house.dataset.reserved === "yes") {
        this.showOrHide(house, false);
      } else {
        this.showOrHide(house, true);
      }
    });
    let i = 11;

    while (i >= 1) {
      filterOrder[i].forEach((house) => {
        D.querySelector(".section__houses_list").appendChild(house);
      });
      i--;
    }
  }

  checkboxAvailabilityFilters(inputCheck, parentCheck) {
    if (inputCheck.id === "input__background_language" || inputCheck.id === "input__date_filter") {
      if (inputCheck.id === "input__background_language") {
        filters[inputCheck.dataset.filter] = inputCheck.value.toLowerCase();
      }
      if (inputCheck.id === "input__date_filter") {
        const date = inputCheck.value;
        const dateA = date.split(" - ");
        const fechaStart = `${new Date(dateA[0]).getFullYear()}-${("0" + (new Date(dateA[0]).getMonth() + 1)).slice(
          -2
        )}-${new Date(dateA[0]).getDate()}`;
        const fechaEnd = `${new Date(dateA[1]).getFullYear()}-${("0" + (new Date(dateA[1]).getMonth() + 1)).slice(
          -2
        )}-${new Date(dateA[1]).getDate()}`;

        const startEvent = new Date(fechaStart).getTime();
        const endEvent = new Date(fechaEnd).getTime();

        filters[inputCheck.dataset.filter] = startEvent + " - " + endEvent;
      }
    } else {
      parentCheck.parentElement.querySelectorAll("input").forEach((input) => {
        if (inputCheck !== input && input.checked) {
          input.checked = false;
        } else if (inputCheck === input && input.checked === true) {
          filters[inputCheck.dataset.filter] = inputCheck.value.toLowerCase();
        } else if (inputCheck === input && input.checked === false) {
          filters[inputCheck.dataset.filter] = "";
        }
      });
    }

    this.searchHouse(D.querySelector("#input__search").value);
  }

  fillTags(inputCheck, parentCheck) {
    if (filters.status) {
      const status = filters.status;
      const tstatus = status[0].toUpperCase() + status.substring(1);
      D.querySelector("#filter-status").classList.remove("d-none");
      D.querySelector(
        "#filter-status"
      ).innerHTML = `Only Available <button class="btn__close-tag" id="status">X</button>`;
    } else {
      D.querySelector("#filter-status").classList.add("d-none");
      D.querySelector("#filter-status").innerHTML = "";
    }

    if (filters.gender) {
      const gender = filters.gender;
      const tgender = gender[0].toUpperCase() + gender.substring(1);
      D.querySelector("#filter-gender").classList.remove("d-none");
      D.querySelector("#filter-gender").innerHTML = `${tgender} <button class="btn__close-tag" id="gender">X</button>`;
    } else {
      D.querySelector("#filter-gender").innerHTML = "";
      D.querySelector("#filter-gender").classList.add("d-none");
    }

    if (filters.typeRoom) {
      const typeRoom = filters.typeRoom;
      const ttypeRoom = typeRoom[0].toUpperCase() + typeRoom.substring(1);
      D.querySelector("#filter-type-room").classList.remove("d-none");
      D.querySelector(
        "#filter-type-room"
      ).innerHTML = `${ttypeRoom} <button class="btn__close-tag" id="typeRoom">X</button>`;
    } else {
      D.querySelector("#filter-type-room").innerHTML = "";
      D.querySelector("#filter-type-room").classList.add("d-none");
    }

    if (filters.typeBed) {
      const typeBed = filters.typeBed;
      const ttypeBed = typeBed[0].toUpperCase() + typeBed.substring(1);
      D.querySelector("#filter-type-bed").classList.remove("d-none");
      D.querySelector(
        "#filter-type-bed"
      ).innerHTML = `${ttypeBed} <button class="btn__close-tag" id="typeBed">X</button>`;
    } else {
      D.querySelector("#filter-type-bed").innerHTML = "";
      D.querySelector("#filter-type-bed").classList.add("d-none");
    }

    if (filters.age) {
      const age = filters.age;
      const tage = age[0].toUpperCase() + age.substring(1);
      D.querySelector("#filter-age").classList.remove("d-none");
      D.querySelector("#filter-age").innerHTML = `${tage} <button class="btn__close-tag" id="age">X</button>`;
    } else {
      D.querySelector("#filter-age").classList.add("d-none");
      D.querySelector("#filter-age").innerHTML = "";
    }

    if (filters.food_s) {
      const food_s = filters.food_s;
      const tfood_s = food_s[0].toUpperCase() + food_s.substring(1);
      D.querySelector("#filter-food").classList.remove("d-none");
      D.querySelector("#filter-food").innerHTML = `${tfood_s} <button class="btn__close-tag" id="food_s">X</button>`;
    } else {
      D.querySelector("#filter-food").classList.add("d-none");
      D.querySelector("#filter-food").innerHTML = "";
    }

    if (filters.diet) {
      const diet = filters.diet;
      const tdiet = diet[0].toUpperCase() + diet.substring(1);
      D.querySelector("#filter-diet").classList.remove("d-none");
      D.querySelector("#filter-diet").innerHTML = `${tdiet} <button class="btn__close-tag" id="food_s">X</button>`;
    } else {
      D.querySelector("#filter-diet").classList.add("d-none");
      D.querySelector("#filter-diet").innerHTML = "";
    }

    if (filters.pets) {
      const pets = filters.pets;
      const tpets = pets[0].toUpperCase() + pets.substring(1);
      D.querySelector("#filter-pets").classList.remove("d-none");
      D.querySelector("#filter-pets").innerHTML = `${tpets} <button class="btn__close-tag" id="food_s">X</button>`;
    } else {
      D.querySelector("#filter-pets").classList.add("d-none");
      D.querySelector("#filter-pets").innerHTML = "";
    }

    if (filters.smoke) {
      const smoke = filters.smoke;
      const tsmoke = smoke[0].toUpperCase() + smoke.substring(1);
      D.querySelector("#filter-smoke").classList.remove("d-none");
      D.querySelector("#filter-smoke").innerHTML = `${tsmoke} <button class="btn__close-tag" id="food_s">X</button>`;
    } else {
      D.querySelector("#filter-smoke").classList.add("d-none");
      D.querySelector("#filter-smoke").innerHTML = "";
    }

    D.addEventListener("click", (e) => {
      if (e.target.matches(".btn__close-tag")) {
        D.querySelectorAll("[data-filter]").forEach((input) => {
          if (e.target.id === input.dataset.filter) {
            input.checked = false;
            filters[input.dataset.filter] = "";
            this.searchHouse();

            if (input.dataset.filter === "status") {
              D.querySelector("#filter__date").classList.replace("d-flex", "d-none");
            }
          }
        });
        e.target.parentElement.classList.add("d-none");
      }
    });
  }
}

//TODO INSTANCE CLASS
const INSTANCE = new HomestayDirectory();

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  INSTANCE.housesList();

  // TODO RESPONSIVE
  const $buttonFilter = D.querySelector("#button__filter");

  if (screen.width <= "575") $buttonFilter.innerHTML = `<i class="icon__filter"></i>`;
  else $buttonFilter.innerHTML = `<i class="icon__filter"></i> Filters`;

  _Messages.quitMessage();
});
D.addEventListener("click", (e) => {
  if (e.target.matches("#button__filter")) D.querySelector("#section__filters").classList.toggle("d-block");
  if (e.target.matches("#close_filter")) D.querySelector("#section__filters").classList.replace("d-block", "d-none");
  if (e.target.matches("#close__filter") || e.target.matches("#btn__search_house")) {
    e.preventDefault();
    D.querySelector("#modal-dynamic").classList.remove("show");
  }
  if (e.target.matches(".parent__check input") && e.target.id !== "input__date_filter") {
    INSTANCE.checkboxAvailabilityFilters(e.target, e.target.parentElement);
    INSTANCE.fillTags(e.target, e.target.parentElement);
  }
  if (e.target.matches(".applyBtn"))
    INSTANCE.checkboxAvailabilityFilters(
      D.querySelector("#input__date_filter"),
      D.querySelector("#input__date_filter").parentElement
    );
  if (e.target.matches("#btn__more")) {
    const $pagination = D.querySelector(".pag");
    INSTANCE.pagination(e.target, $pagination);
  }
  if (e.target.dataset.filter === "food_s") {
    if (e.target.value === "yes") D.querySelector("#filter__food_fiet").classList.toggle("d-none");
    else if (e.target.value === "no") D.querySelector("#filter__food_fiet").classList.add("d-none");
  }
  if (e.target.dataset.filter === "status") {
    if (e.target.value === "true" && e.target.checked)
      D.querySelector("#filter__date").classList.replace("d-none", "d-flex");
    else D.querySelector("#filter__date").classList.replace("d-flex", "d-none");
  }
});

D.addEventListener("change", (e) => {
  if (e.target.matches(".parent__check select")) {
    INSTANCE.checkboxAvailabilityFilters(e.target, e.target.parentElement);
    INSTANCE.fillTags(e.target, e.target.parentElement);
  }
});

D.addEventListener("mouseover", (e) => {
  if (e.target.matches(".btn__pagination")) INSTANCE.overArrow(e.target, "over");
});

D.addEventListener("mouseout", (e) => {
  if (e.target.matches(".btn__pagination")) INSTANCE.overArrow(e.target);
});

D.addEventListener("keyup", (e) => {
  if (e.target.matches("#input__search")) INSTANCE.searchname(e.target.value.toLowerCase());
  if (e.target.matches("#input__background_language"))
    INSTANCE.checkboxAvailabilityFilters(e.target, e.target.parentElement);
});

// ! ERROR
W.addEventListener(
  "error",
  (e) => {
    if (e.target.tagName === "IMG") e.target.src = "../assets/emptys/frontage-empty.png";
  },
  true
);
