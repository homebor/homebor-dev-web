// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();

// TODO EVENTOS

// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", (e) => {
  if (D.querySelector("#transport_total-db").value === "") D.querySelector("#transport_total-db").value = 0;
  if (D.querySelector("#total_weckly").value === "") D.querySelector("#total_weckly").value = 0;
  if (D.querySelector("#total__price").value === "") D.querySelector("#total__price").value = 0;
  if (D.querySelector("#booking_fee").value === "") D.querySelector("#booking_fee").value = 250;

  _Messages.quitMessage();
});

// ! CHANGE
D.addEventListener("change", (e) => {
  if (
    e.target.matches("#meal_p") ||
    e.target.matches("#lodging_type") ||
    e.target.matches("#pick_up") ||
    e.target.matches("#drop_off") ||
    e.target.matches("#accomm-near") ||
    e.target.matches("#accomm-urgent") ||
    e.target.matches("#accomm-guardianship") ||
    e.target.matches("#summer-fee") ||
    e.target.matches("#minor-fee")
  ) {
    let accommodation = 0;
    let transportTotal = 0;
    const mealValue = D.querySelector("#meal_p").value;
    const accommodationValue = D.querySelector("#lodging_type").value;

    if (accommodationValue === "Share") {
      if (mealValue === "Only Room") accommodation = 900 / 4;
      else if (mealValue === "2 Meals") accommodation = 1100 / 4;
      else if (mealValue === "3 Meals") accommodation = 1200 / 4;
    } else if (accommodationValue === "Executive") {
      if (mealValue === "Only Room") accommodation = 1450 / 4;
      else if (mealValue === "2 Meals") accommodation = 1800 / 4;
      else if (mealValue === "3 Meals") accommodation = 1900 / 4;
    } else {
      if (mealValue === "Only Room") accommodation = 1050 / 4;
      else if (mealValue === "2 Meals") accommodation = 1400 / 4;
      else if (mealValue === "3 Meals") accommodation = 1500 / 4;
    }

    if (mealValue === "Only Room") {
      D.querySelector("#food").value = "No";
      D.querySelector("#group__diet").classList.replace("d-block", "d-none");
    } else if (mealValue === "2 Meals" || mealValue === "3 Meals") {
      D.querySelector("#group__diet").classList.replace("d-none", "d-block");
      D.querySelector("#food").value = "Yes";
    }
    /* D.querySelector("#total_weckly").value = "$" + accommodation + " CAD"; */
    D.querySelector("#total_weckly").value = accommodation;

    let pickUp = D.querySelector("#pick_up").value;
    let dropOff = D.querySelector("#drop_off").value;

    if (pickUp === "yes") pickUp = 200;
    else pickUp = 0;

    if (dropOff === "yes") dropOff = 100;
    else dropOff = 0;

    transportTotal = parseFloat(pickUp) + parseFloat(dropOff);
    /* D.querySelector("#transport_total").value = transportTotal; */
    D.querySelector("#transport_total-db").value = transportTotal;
    let bookingFee = D.querySelector("#booking_fee").value;
    const $nearToSchool = D.querySelector("#accomm-near").value;
    const $accomUrgent = D.querySelector("#accomm-urgent").value;
    const $summerFee = D.querySelector("#summer-fee").value;
    const $minorFee = D.querySelector("#minor-fee").value;
    const $accomGuardianship = D.querySelector("#accomm-guardianship").value;
    let totalAccommodation = accommodation * 4;
    let totalPrice = 0;
    let priceNearSchool = 0;
    let priceAccomUrgent = 0;
    let priceSummerFee = 0;
    let priceMinorFee = 0;
    let priceAccomGuardianship = 0;

    // TODO NEAR TO SCHOOL
    if ($nearToSchool === "yes") priceNearSchool = 35 * 7 * 4;
    else priceNearSchool = 0;

    // TODO ACCOMMODATION URGENT
    if ($accomUrgent === "yes") priceAccomUrgent = 150;
    else priceAccomUrgent = 0;

    // TODO SUMMER FEE
    if ($summerFee === "yes") priceSummerFee = 45 * 4;
    else priceSummerFee = 0;

    // TODO MINOR FEE
    if ($minorFee === "yes") priceMinorFee = 75 * 4;
    else priceMinorFee = 0;

    // TODO GUARDIANSHIP
    if ($accomGuardianship === "yes") priceAccomGuardianship = 550;
    else priceAccomGuardianship = 0;

    if (totalAccommodation == 0)
      totalPrice =
        parseFloat(transportTotal) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);
    else if (transportTotal == 0)
      totalPrice =
        parseFloat(totalAccommodation) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);
    else
      totalPrice =
        parseFloat(totalAccommodation) +
        parseFloat(transportTotal) +
        parseFloat(bookingFee) +
        parseFloat(priceNearSchool) +
        parseFloat(priceAccomUrgent) +
        parseFloat(priceSummerFee) +
        parseFloat(priceMinorFee) +
        parseFloat(priceAccomGuardianship);

    D.querySelector("#total_weckly-db").value = totalAccommodation;
    D.querySelector("#total__price").value = totalPrice;
  }

  if (e.target.dataset.preview === "true") {
    const Reader = new FileReader();

    Reader.readAsDataURL(e.target.files[0]);
    Reader.onload = (result) => {
      e.target.parentElement.querySelector(".gallery-photo").src = result.target.result;
    };
  }
});
