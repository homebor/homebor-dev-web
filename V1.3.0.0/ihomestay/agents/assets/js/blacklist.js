// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;

const _Messages = new Messages();

// TODO FUNCIONES
class Blacklist {
  constructor() {
    this.blacklistEmpty = () => {
      const text = `<h3 class="m-0 py-4 text-center text-white">You do not have any house disabled</h2>`;
      D.querySelector(".blacklist").innerHTML = text;
    };
  }

  async getBlacklist() {
    try {
      const getDataBlacklist = await axios("blacklist_data.php");
      const dataBlacklist = await getDataBlacklist.data;
      if (dataBlacklist) await this.renderBlacklist(dataBlacklist);
      else throw "Not found results";
    } catch (error) {
      this.blacklistEmpty();
      _Messages.showMessage(error, 4, 6);
    }
  }

  async renderBlacklist(blacklist) {
    const $fragment = D.createDocumentFragment();
    const housesTraveled = [];

    blacklist.forEach((house) => {
      if (housesTraveled.includes(house["id_home"])) return;
      const $clone = D.importNode(D.getElementById("blacklist-template").content, true);
      $clone.querySelector("#house-image").src = `../${house["phome"]}`;
      $clone.querySelector("#house-name").textContent = house["h_name"];
      $clone.querySelector("#house-email").textContent = house["mail_h"];
      $clone.querySelector("#disabled-date").textContent = house["dates"];
      $clone.querySelector("#disabled-reason").textContent = house["reason"];
      $clone.querySelector("#edit-homestay").dataset.idHome = house["id_home"];
      $clone.querySelector("#disabled-homestay");
      $fragment.appendChild($clone);
      housesTraveled.push(house["id_home"]);
    });
    D.querySelector(".blacklist tbody").appendChild($fragment);

    if (D.querySelectorAll(".blacklist tbody tr").length === 0) this.blacklistEmpty();
  }
}

// TODO INSTANCIA
const INSTANCE = new Blacklist();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await INSTANCE.getBlacklist();

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  if (e.target.id === "edit-homestay") W.location.href = `homedit?art_id=${e.target.dataset.idHome}`;
});

// ! KEYUP
D.addEventListener("keyup", (e) => {});

// ! CHANGE
D.addEventListener("change", (e) => {});
