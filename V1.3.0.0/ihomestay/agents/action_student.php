<?php 
require '../../xeon.php';
include '../../cript.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

// * IF MAIL EXIST
if(!empty($_POST['mail_s']) || $_POST['mail_s'] == 'NULL'){

  $mail_s = $_POST['mail_s'];

  //TODO DATE TIME
  date_default_timezone_set("America/Toronto");
  $date = date('Y-m-d H:i:s');
  $dateImage = date('YmdHisv');
  $dateweb = date('Y-m-d H:i:s');
  $userstudent = $name." ".$l_name;

  // TODO FILE'S DIRECTORY STUDENT
  $paths = '../../public_s/' . $mail_s;

  
  // * QUERY TO VERIFY IF THE MAIL EXIST
  $usersQuery = $link->query("SELECT mail FROM users WHERE mail = '$mail_s'");
  $num_user = mysqli_num_rows($usersQuery);

  if($num_user > 0) {
    $jsonResponse = array(
      "title" => "mail exist",
    );
  }else{

    // TODO BADIC INFORMATION

      if(!empty($_FILES['profile_s']['name'])){

        // $deletePhoto = unlink("../../".$row["photo_s"]); 

        $profile_tmp = $_FILES['profile_s']['tmp_name'];
        $profileType = stristr($_FILES['profile_s']['type'], "/");
        $profileExtension = str_replace( "/", ".", $profileType);
        $s_profileUrl = "public_s/".$mail_s."/profilePhoto" . $dateImage . $profileExtension;
        $s_profile = "profilePhoto" . $dateImage . $profileExtension;

        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($profile_tmp, $paths . '/' . $s_profile);
              
      } else $s_profileUrl = 'NULL';

      if (empty($_POST['name'])) $name = 'NULL';
      else $name =  addslashes($_POST['name']);

      if (empty($_POST['l_name'])) $l_name = 'NULL';
      else $l_name =  addslashes($_POST['l_name']); 

      if (empty($_POST['password'])) $password = 'NULL';
      else {
        $password =  $_POST['password'];
        $passwordD = SED::encryption($password);
      }

      if (empty($_POST['arrive_g'])) $arrive_g =  'NULL';
      else {   
        $g_arrive = date_create($_POST['arrive_g']); //Se crea una fecha con el formato que recibes de la vista.
        $arrive_g = date_format($g_arrive, 'Y-m-d'); //Obtienes la fecha en el formato deseado.
      }

      if (empty($_POST['destination_c'])) $destination_c =  'NULL';
      else $destination_c =  $_POST['destination_c'];

    // TODO PERSONAL STUDENT ADDRESS

      if (empty($_POST['country'])) $country = 'NULL';
      else $country =  addslashes($_POST['country']);

      if (empty($_POST['dir'])) $dir =  'NULL';
      else $dir =  addslashes($_POST['dir']);    
      
      if (empty($_POST['city'])) $city =  'NULL';
      else $city =  addslashes($_POST['city']);    
      
      if (empty($_POST['state'])) $state =  'NULL';
      else $state =  addslashes($_POST['state']);    
      
      if (empty($_POST['p_code'])) $p_code =  'NULL';
      else $p_code =  addslashes($_POST['p_code']);    
      
    // TODO ACCOMMODATION REQUEST

      if (empty($_POST['firstd'])) $firstd = 'NULL';
      else {
        $f_firstd = date_create($_POST['firstd']); //Se crea una fecha con el formato que recibes de la vista.
        $firstd = date_format($f_firstd, 'Y-m-d'); //Obtienes la fecha en el formato deseado.   
      }

      if (empty($_POST['lastd'])) $lastd = 'NULL';
      else {
        $l_lastd = date_create($_POST['lastd']); //Se crea una fecha con el formato que recibes de la vista.
        $lastd = date_format($l_lastd, 'Y-m-d'); //Obtienes la fecha en el formato deseado.   
      }

    // TODO FLIGHT INFORMATION

      if (empty($_POST['n_airline'])) $n_airline =  'NULL';
      else $n_airline =  addslashes($_POST['n_airline']);    
      
      if (empty($_POST['n_flight'])) $n_flight =  'NULL';
      else $n_flight =  addslashes($_POST['n_flight']);    
      
      if (empty($_POST['f_date'])) $f_date =  'NULL';
      else {
        $date_f = date_create($_POST['f_date']); //Se crea una fecha con el formato que recibes de la vista.
        $f_date = date_format($date_f, 'Y-m-d h:i A'); //Obtienes la fecha en el formato deseado. 
      }
      
      if (empty($_POST['h_date'])) $h_date =  'NULL';
      else {
        $date_h = date_create($_POST['h_date']); //Se crea una fecha con el formato que recibes de la vista.
        $h_date = date_format($date_h, 'Y-m-d'); //Obtienes la fecha en el formato deseado.  
      }
      
    // TODO EMERGENCY CONTACT

      if (empty($_POST['cont_name'])) $cont_name = 'NULL';
      else $cont_name =  $_POST['cont_name'];

      if (empty($_POST['cont_lname'])) $cont_lname = 'NULL';
      else $cont_lname =  $_POST['cont_lname'];

      if (empty($_POST['num_conts'])) $num_conts = 'NULL';
      else $num_conts =  $_POST['num_conts'];

      if(empty($_POST['relationship'])) $s_relationship = 'NULL';
      else $s_relationship = addslashes($_POST['relationship']);

    // TODO OTHER VAIRABLES

      if (empty($_POST['id_m'])) $id_m =  '0';
      else $id_m =  $_POST['id_m'];  
      
      if (empty($_POST['agency'])) $agency = 'NULL';
      else $agency = $_POST['agency'];

    // TODO PERSONAL INFORMATION

      if (empty($_POST['about_me'])) $about_me =  'NULL';
      else $about_me =  addslashes($_POST['about_me']);

      if (empty($_POST['db'])) $db = 'NULL';
      else {
        $d_db = date_create($_POST['db']); //Se crea una fecha con el formato que recibes de la vista.
        $db = date_format($d_db, 'Y-m-d'); //Obtienes la fecha en el formato deseado.     
      }

      if (empty($_POST['gender'])) $gender = 'NULL';
      else $gender =  $_POST['gender'];

      if (empty($_POST['num_s'])) $num_s = 'NULL';
      else $num_s =  $_POST['num_s'];

      if (empty($_POST['nationality'])) $nationality =  'NULL';
      else $nationality =  addslashes($_POST['nationality']);

      if (empty($_POST['lang_s'])) $lang_s = 'NULL';
      else $lang_s =  addslashes($_POST['lang_s']);

      if (empty($_POST['language_a'])) $language_a =  'NULL';
      else $language_a =  addslashes($_POST['language_a']);

      if (empty($_POST['pass'])) $pass = 'NULL';
      else $pass =  $_POST['pass'];

      if (empty($_POST['exp_pass'])) $exp_pass =  'NULL';
      else {
        $pass_exp = date_create($_POST['exp_pass']); //Se crea una fecha con el formato que recibes de la vista.
        $exp_pass = date_format($pass_exp, 'Y-m-d'); //Obtienes la fecha en el formato deseado. 
      }

      if (empty($_POST['bl'])) $bl = 'NULL';
      else {
        $lb = date_create($_POST['bl']); //Se crea una fecha con el formato que recibes de la vista.
        $bl = date_format($lb, 'Y-m-d'); //Obtienes la fecha en el formato deseado.   
      }

    // TODO HEALTH INFORMATION

      if (empty($_POST['smoke_s'])) $smoke_s = 'NULL';
      else $smoke_s =  $_POST['smoke_s'];

      if (empty($_POST['drinks_alc'])) $drinks_alc =  'no';
      else $drinks_alc =  $_POST['drinks_alc'];

      if (empty($_POST['drugs'])) $drugs =  'no';
      else $drugs =  $_POST['drugs'];

      if (empty($_POST['allergy_a'])) $allergy_a =  'no';
      else $allergy_a =  addslashes($_POST['allergy_a']);

      if (empty($_POST['allergy_m'])) $allergy_m =  'no';
      else $allergy_m =  addslashes($_POST['allergy_m']);

      if (empty($_POST['healt_s'])) $healt_s =  'NULL';
      else $healt_s =  addslashes($_POST['healt_s']);

      if (empty($_POST['disease'])) $disease =  'no';
      else $disease =  addslashes($_POST['disease']);    
      
      if (empty($_POST['treatment'])) $treatment =  'no';
      else $treatment =  addslashes($_POST['treatment']);    
        
      if (empty($_POST['treatment_p'])) $treatment_p =  'no';
      else $treatment_p =  addslashes($_POST['treatment_p']);

      if (empty($_POST['allergies'])) $allergies =  'no';
      else $allergies =  $_POST['allergies']; 
      
      if (empty($_POST['surgery'])) $surgery =  'no';
      else $surgery =  addslashes($_POST['surgery']);

    // TODO PROFESSIONAL INFORMATION
      
      // ? ACADEMY INFORMATION 

        if (empty($_POST['n_a'])) $n_a = 'NULL';
        else $n_a =  $_POST['n_a'];

        if (empty($_POST['name_a'])) $name_a =  'NULL';
        else $name_a =  $_POST['name_a'];

      if (empty($_POST['english_l'])) $english_l =  'NULL';
      else $english_l =  $_POST['english_l'];

      if (empty($_POST['type_s'])) $type_s = 'NULL';
      else $type_s =  $_POST['type_s'];

      if (empty($_POST['prog_selec'])) $prog_selec =  'NULL';
      else $prog_selec =  addslashes($_POST['prog_selec']);

      if (empty($_POST['schedule'])) $schedule =  'NULL';
      else $schedule =  addslashes($_POST['schedule']);    
      
    // TODO HOUSE PREFERENCE

      // ? CAN YOU SHARE WITH
      
        if (empty($_POST['smoker_l'])) $smoker_l =  'no';
        else $smoker_l =  $_POST['smoker_l'];

        if (empty($_POST['children'])) $children =  'yes';
        else $children =  addslashes($_POST['children']);

        if (empty($_POST['teenagers'])) $teenagers =  'yes';
        else $teenagers =  $_POST['teenagers'];

        if (empty($_POST['pets'])) $pets = 'NULL';
        else $pets =  $_POST['pets'];

      // ? ACCOMMODATION

        if (empty($_POST['lodging_type'])) $lodging_type =  'NULL';
        else $lodging_type =  $_POST['lodging_type'];

        if(empty($_POST['meal_p'])) $s_mealPlan = 'NULL';
        else $s_mealPlan = $_POST['meal_p'];

        if (empty($_POST['food'])) $food = 'NULL';
        else $food =  $_POST['food'];

        if (empty($_POST['diet'])) {
            $diet =  'NULL';
            $vegetarians = 'no';
            $halal =  'no';
            $kosher =  'no';
            $lactose =  'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'yes';

        }elseif($_POST['diet'] == 'Vegetarians'){

            $vegetarians = 'yes';
            $halal =  'no';
            $kosher =  'no';
            $lactose =  'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'no';

        }elseif($_POST['diet'] == 'Halal'){

            $halal =  'yes';
            $vegetarians = 'no';
            $kosher =  'no';
            $lactose =  'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'no';

        }elseif($_POST['diet'] == 'Kosher'){

            $kosher =  'yes';
            $halal =  'no';
            $vegetarians = 'no';
            $lactose =  'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'no';
        
        }elseif($_POST['diet'] == 'Lactose'){

            $lactose =  'yes';
            $kosher =  'no';
            $halal =  'no';
            $vegetarians = 'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'no';
        
        }elseif($_POST['diet'] == 'Gluten'){

            $gluten =  'yes';
            $lactose =  'no';
            $kosher =  'no';
            $halal =  'no';
            $vegetarians = 'no';
            $pork =  'no';
            $none =  'no';
        
        }elseif($_POST['diet'] == 'Pork'){

            $pork =  'yes';
            $gluten =  'no';
            $lactose =  'no';
            $kosher =  'no';
            $halal =  'no';
            $vegetarians = 'no';
            $none =  'no';
        
        }elseif($_POST['diet'] == 'None'){

            $none =  'yes';
            $pork =  'no';
            $gluten =  'no';
            $lactose =  'no';
            $kosher =  'no';
            $halal =  'no';
            $vegetarians = 'no';

        }else{
            $vegetarians = 'no';
            $halal =  'no';
            $kosher =  'no';
            $lactose =  'no';
            $gluten =  'no';
            $pork =  'no';
            $none =  'yes';
        }

        if (empty($_POST['booking_fee'])) $s_booking_fee =  '0';
        else $s_booking_fee =  $_POST['booking_fee'];

        if (empty($_POST['a_price'])) $a_price =  '0';
        else $a_price =  $_POST['a_price'];

      // ? SUPPLEMENTS
        
        if(empty($_POST['accomm-near'])) $s_accommNear = 'NULL';
        else $s_accommNear = $_POST['accomm-near'];
        
        if(empty($_POST['accomm-urgent'])) $s_accommUrgent = 'NULL';
        else $s_accommUrgent = $_POST['accomm-urgent'];

        if(empty($_POST['summer-fee'])) $s_summerFee = 'NULL';
        else $s_summerFee = $_POST['summer-fee'];

        if(empty($_POST['minor-fee'])) $s_minorFee = 'NULL';
        else $s_minorFee = $_POST['minor-fee'];

        if(empty($_POST['accomm-guardianship'])) $s_accommGuard = 'NULL';
        else $s_accommGuard = $_POST['accomm-guardianship'];

      // ? TRANSPORT

        if (empty($_POST['pick_up'])) $pick_up =  'no';
        else $pick_up =  $_POST['pick_up'];    
        
        if (empty($_POST['drop_off'])) $drop_off =  'no';
        else $drop_off =  $_POST['drop_off'];

        if(empty($_POST['t_price'])) $s_transportPrice = '0';
        else $s_transportPrice = $_POST['t_price'];
        
    // TODO ATTACHED FILES 
      
      if(!empty($_FILES['passport']['name'])){

        /* $deletePhoto = unlink("../".$row["pass_photo"]); */ 

        $passportImage_tmp = $_FILES['passport']['tmp_name'];
        $passportImageType = stristr($_FILES['passport']['type'], "/");
        $passportImageExtension = str_replace( "/", ".", $passportImageType);
        $s_passportImageUrl = "public_s/".$mail_s."/passportPhoto" . $dateImage . $passportImageExtension;
        $s_passportImage = "passportPhoto" . $dateImage . $passportImageExtension;

        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($passportImage_tmp, $paths . '/' . $s_passportImage);
              
      } else $s_passportImageUrl = 'NULL';
          
      if(!empty($_FILES['visa']['name'])){

        /* $deletePhoto = unlink("../".$row["visa"]); */ 

        $visaImage_tmp = $_FILES['visa']['tmp_name'];
        $visaImageType = stristr($_FILES['visa']['type'], "/");
        $visaImageExtension = str_replace( "/", ".", $visaImageType);
        $s_visaImageUrl = "public_s/".$mail_s."/visaPhoto" . $dateImage . $visaImageExtension;
        $s_visaImage = "visaPhoto" . $dateImage . $visaImageExtension;

        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($visaImage_tmp, $paths . '/' . $s_visaImage);
              
      } else $s_visaImageUrl = 'NULL';
        
      if(!empty($_FILES['flight-image']['name'])){

        /* $deletePhoto = unlink("../".$row["flight-image"]); */ 

        $flightImage_tmp = $_FILES['flight-image']['tmp_name'];
        $flightImageType = stristr($_FILES['flight-image']['type'], "/");
        $flightImageExtension = str_replace( "/", ".", $flightImageType);
        $s_flightImageUrl = "public_s/".$mail_s."/flightImage" . $dateImage . $flightImageExtension;
        $s_flightImage = "flightImage" . $dateImage . $flightImageExtension;

        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($flightImage_tmp, $paths . '/' . $s_flightImage);

      } else $s_flightImageUrl = 'NULL';

      if(!empty($_POST["signature-image"])){
        
        if (!file_exists($paths)) mkdir($paths, 0777);
        $signature = $_POST["signature-image"];

        $folderPath = "../../public_s/".$mail_s."/";
        $image_parts = explode(";base64", $signature);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . "signature". $dateImage . ".png";
        file_put_contents($file, $image_base64);

        $signatureURL = "public_s/".$mail_s."/signature". $dateImage . ".png";

      }else $signatureURL = 'NULL';

    // TODO INSERTIONS

      $insertUser = $link->query("INSERT INTO users (mail, psw, usert, name, l_name, origin) VALUES ('$mail_s', '$passwordD', 'student', '$name', '$l_name', 'iHomestay')");
        
      $insertStudent = $link->query("INSERT INTO pe_student (name_s, l_name_s, mail_s, db_s, nationality, dir_s, city_s, state_s, p_code_s, country, passport, exp_pass, gen_s, db_visa, n_a, lang_s, type_s, arrive_g, firstd, lastd, smoke_s, pets, food, num_s, num_conts, cont_name, cont_lname, healt_s, disease, treatment, treatment_p, surgery, drinks_alc, drugs, english_l, language_a, prog_selec, destination_c, schedule, lodging_type, allergies, smoker_l, allergy_a, allergy_m, children, teenagers, vegetarians, halal, kosher, lactose, gluten, pork, none, n_airline, n_flight, departure_f, arrive_f, pick_up, drop_off, about_me, id_m, photo_s, pass_photo, visa, flight_image, ac_confirm, status, a_price, t_price, booking_fee, relationship, meal_p, id_ag, a_near, a_urgent, summer_fee, minor_fee, a_guardianship, signature_s) VALUES ('$name', '$l_name', '$mail_s', '$db', '$nationality', '$dir', '$city', '$state', '$p_code', '$country', '$pass', '$exp_pass', '$gender', '$bl', '$n_a', '$lang_s', '$type_s', '$arrive_g', '$firstd', '$lastd', '$smoke_s', '$pets', '$food', '$num_s', '$num_conts', '$cont_name', '$cont_lname', '$healt_s', '$disease', '$treatment', '$treatment_p', '$surgery', '$drinks_alc', '$drugs', '$english_l', '$language_a', '$prog_selec', '$destination_c', '$schedule', '$lodging_type', '$allergies', '$smoker_l', '$allergy_a', '$allergy_m', '$children', '$teenagers', '$vegetarians', '$halal', '$kosher', '$lactose', '$gluten', '$pork', '$none', '$n_airline', '$n_flight', '$f_date', '$h_date', '$pick_up', '$drop_off', '$about_me', '$id_m', '$s_profileUrl', '$s_passportImageUrl', '$s_visaImageUrl', '$s_flightImageUrl', 'No', 'Confirm Registration', '$a_price', '$s_transportPrice', '$s_booking_fee', '$s_relationship', '$s_mealPlan', '0', '$s_accommNear', '$s_accommUrgent', '$s_summerFee', '$s_minorFee', '$s_accommGuard', '$signatureURL')");
        
      $insertWebmaster = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m) VALUES ('$usuario', 'Register a Student', '$dateweb', '$mail_s', '$id_m')");
        
      $insertStudentTv = $link->query("INSERT INTO studentv ( title, email, user, sender, id_m, dates, link) VALUES ('Welcome to Homebor', '$mail_s', '$userstudent', '$agency', '$id_m', '$dateweb', 'welcomea.php')");

      $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$_POST[mail_s]'");
      $row_student=$studentQuery->fetch_assoc();

      if($name_a === 'NULL') {
        $jsonResponse = array(
          'title' => "true",
          'id_student' => $row_student['id_student'],
        );
      }else{
        $save_aca = $link->query("INSERT INTO academy (name_a, acronyms, dir_a, state_a, city_a, p_code_a, id_photo, photo_a, status_a) VALUES ('$name_a', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'Disabled')");
        
        $academy_r = $link->query("SELECT * FROM academy ORDER BY id_ac DESC LIMIT 1");
        $r_acar=$academy_r->fetch_assoc();

        $upd_aca = "UPDATE academy SET id_photo='$r_acar[id_ac]' WHERE id_ac='$r_acar[id_ac]'";
        $re_up=$link->query($upd_aca);

        $updateStudent = $link->query("UPDATE pe_student SET n_a = '$r_acar[id_ac]' WHERE mail_s = '$mail_s'");

        $search__aca = $link->query("SELECT * FROM academy WHERE name_a = '$name_a' AND status_a = 'Disabled' ORDER BY id_ac DESC LIMIT 1");
        $re_search=$search__aca->fetch_assoc();

        $jsonResponse = array(
          'title' => "new school",
          'id_school' => $re_search['id_ac'],
        );
      }

  }
}else {
  $jsonResponse = array(
    "title" => "false"
  );
}

echo json_encode($jsonResponse);




// ? CONDICION PARA PLANES Y LIMITES DE LAS AGENCIAS
/* if (empty($_POST['mail_s'])) {
            $mail_s = 'NULL';
        }
        else {
            $mail_s =  $_POST['mail_s'];     
        }

        if (empty($_POST['plan'])) {
            $plan = 'NULL';
        }
        else {
            $plan =  $_POST['plan'];     
        }
        if (empty($_POST['limit'])) {
            $limit = 'NULL';
        }
        else {
            $limit =  $_POST['limit'];     
        }
        if ($limit = 0) {
            $limit = 1;
        }
//------------------------------------------- 
        
    }elseif ($plan == 'Free' AND $limit >= '100') {

      $json[] = array(
        'register' => 'Plan'
      );
      $jsonstring = json_encode($json);
            
      echo $jsonstring; 

    }elseif ($plan == 'Premium' AND $limit >= '500') {
                        
      $json[] = array(
        'register' => 'Plan'
      );
      $jsonstring = json_encode($json);
            
      echo $jsonstring; 
                        
    } elseif ($plan == 'Professional' AND $limit >= '1000') {
                        
      $json[] = array(
        'register' => 'Plan'
      );
      $jsonstring = json_encode($json);
            
      echo $jsonstring; 
          
*/


?>