<?php 
session_start();

include '../../xeon.php';

// TODO ASSIGN VALUES TO VARIABLES $_
$usuario = $_SESSION['username'];
$id_student = $_POST['id_student'];

function searchReports($link, $id_student){
  
  $jsonSearchReport = null;
  
  // TODO QUERIES
  
  $studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$id_student'");
  $row_studentQuery = $studentQuery->fetch_assoc();

  $notificationQuery = $link->query("SELECT * FROM notification WHERE report_s = '$row_studentQuery[mail_s]' AND status = 'Active'");
  $nums_notificationQuery = mysqli_num_rows($notificationQuery);

  if($nums_notificationQuery > 0){
    $jsonSearchReport = array(
      'response' => 'Already Report',
    );
  }else{
    $jsonSearchReport = array(
      'response' => 'No Report'
    );
  }
  
  // TODO ASSIGN ROWS VALUES
  
  $s_mail = $row_studentQuery['mail_s'];
  $s_id = $row_studentQuery['id_student'];
  
  
  echo json_encode($jsonSearchReport);
}

function sendReport($link, $id_student, $usuario){

  // TODO ASSIGN VALUES TO VARIABLES $_POST
  date_default_timezone_set("America/Toronto");
  $date = date('Y-m-d H:i:s');
  
  $title_report = $_POST['title_report'];
  $description_report = $_POST['description_report'];
  $homestay_id = $_POST['homestay_id']; 

  $jsonReport = null;
  
  // TODO QUERIES

  $agentsQuery = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario'");
  $row_agentsQuery = $agentsQuery->fetch_assoc();

  $managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_agentsQuery[id_m]'");
  $row_managerQuery = $managerQuery->fetch_assoc();
  
  $homestayQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$homestay_id'");
  $row_homestayQuery = $homestayQuery->fetch_assoc();

  $studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$id_student'");
  $row_studentQuery = $studentQuery->fetch_assoc();

  // TODO ASSIGN VALUES ROWS TO VARIABLES

  // ? AGENTS

  $a_fullname = $row_agentsQuery['name'] . ' ' . $row_agentsQuery['l_name'];

  // ? MANAGER

  $m_name = $row_managerQuery['name'];
  $m_lname = $row_managerQuery['l_name'];
  $m_mail = $row_managerQuery['mail'];
  $m_id = $row_managerQuery['id_m'];

  // ? HOMESTAY

  $h_mail = $row_homestayQuery['mail_h'];
  $h_nameHouse = $row_homestayQuery['h_name']; 

  // ? STUDENT

  $s_mail = $row_studentQuery['mail_s'];
  $s_fullname = $row_studentQuery['name_s'] . ' ' . $row_studentQuery['l_name_s'];

  // TODO INSERT TO DATABASE

  $insertNotiStudent = $link->query("INSERT INTO noti_student (h_name, user_i, user_i_l, user_i_mail, user_r, date_, des, status) VALUES ('$h_mail', '$m_name', '$m_lname', '$m_mail', '$s_mail', '$date', '$title_report', 'Active')");

  $insertNotification = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, user_r, date_, title, report_s, status) VALUES ('$m_name', '$m_lname', '$m_mail', '$s_mail', '$date', '$title_report', '$s_mail', 'Active')");

  // TODO SEARCH NOTIFICATION INSERTED

  $querySearchNotification = $link->query("SELECT * FROM notification WHERE user_i_mail = '$m_mail' AND user_r = '$s_mail' AND title = '$title_report' AND status = 'Active' ");
  $row_querySearchNotification = $querySearchNotification->fetch_assoc(); 

  // TODO ASSIGN VALUES ROWS TO VARIABLES

  // ? NOTIFICATION

  $ns_id = $row_querySearchNotification['id_not']; 

  // ? DATE

  $dateSaveDB = date('YmdHisv');

  // ? IMAGE REPORT

  if(!empty($_FILES)){
  $image = $_FILES['report_image']['name'];
  $image_tmp = $_FILES['report_image']['tmp_name'];
  $imageType = stristr($_FILES['report_image']['type'], "/");
  $paths = "../public_s/".$s_mail."/Reports" . "/".$ns_id."/";

  $extensionReport = str_replace( "/", ".", $imageType);
  $image = "Report-" . $dateSaveDB . $extensionReport;

  if (!file_exists($paths)) {
    mkdir($paths, 0777);
  }
  move_uploaded_file($image_tmp, $paths.$image);

  }else $image = 'NULL';

  // ? ----------------------------------------------------

  // TODO INSERT TO DATABASE

  $insertReport = $link->query("INSERT INTO reports (names_i, mail_i, names_r, mail_r, stu_rep, title, des, date, status, id_not, report_img, view_h, view_a, view_s) VALUES ('$a_fullname', '$usuario', '$s_fullname', '$s_mail', '$s_mail', '$title_report', '$description_report', '$date', 'Active', '$ns_id', '$image', '0', '1', '0')");

  $insertWebmaster = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m, report_s, reason) VALUES ('$usuario', 'Report Student', '$date', '$h_mail', '$m_id', '$s_mail', '$description_report')");

  $jsonReport = array(
    'response' => 'Report Sent',
  );
  
  echo json_encode($jsonReport);
}


if ($_POST['request'] == 'searchReports') searchReports($link, $id_student);
else if ($_POST['request'] == 'sendReport') sendReport($link, $id_student, $usuario);

?>