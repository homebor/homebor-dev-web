<?php
require '../../xeon.php';
require '../../cript.php';

session_start();
error_reporting(0);

date_default_timezone_set("America/Toronto");
$date = date('YmdHisv');

$usuario = $_SESSION['username'];

$queryAgent = $link->query("SELECT id_m, id_ag, a_mail FROM agents WHERE a_mail = '$usuario' ");
$row_agent = $queryAgent->fetch_assoc();

$queryManager = $link->query("SELECT mail, a_name FROM manager WHERE id_m = '$row_agent[id_m]' ");
$row_manager = $queryManager->fetch_assoc();

if(strpos($row_manager['a_name'], " ")){
  $m_a_name = strstr($row_manager['a_name'], ' ', true);
  $m_l_a_name = strstr($row_manager['a_name'], ' ');
}else{
  $m_a_name = $row_manager['a_name'];
  $m_l_a_name = "";
}

$idAgent = $row_agent['id_ag'];
$idAgency = $row_agent['id_m'];

if ($_POST['basicInformation']) {
  // TODO SAVE BASIC INFORMATION
  $houseName = $_POST['houseName'];
  $phoneNumber = $_POST['phoneNumber'];
  $totalRooms = $_POST['totalRooms'];
  $houseType = $_POST['houseType'];
  $houseMail = $_POST['houseMail'];
  $password = SED::encryption($_POST['password']);

  $mainCity = $_POST['mainCity'];
  $address = $_POST['address'];
  $city = $_POST['city'];
  $state = $_POST['state'];
  $postalCode = $_POST['postalCode'];

  // ? SELECT USERS VERIFY EXISTING
  $searchRegister = $link->query("SELECT id_user FROM users WHERE mail = '$houseMail' ");
  $searchResult = $searchRegister->fetch_assoc();
  if ($searchResult) echo json_encode("Existing user"), exit;


  // ? INSERT USER IN USERS
  $newUserQuery = "INSERT INTO users (mail, psw, usert, name, l_name, origin) VALUES ('$houseMail', '$password', 'homestay', 'NULL', 'NULL', 'iHomestay')";
  $newUser = $link->query($newUserQuery);


  // ? INSERT HOMESTAY DATA IN PE_HOME
  $basicInformationQuery = "INSERT INTO 
  pe_home (name_h, l_name_h, mail_h, h_name, num, room, m_city, dir, city, state, p_code, des, num_mem, backg, backl, a_pre, g_pre, ag_pre, status, cell, smoke, vegetarians, halal, kosher, lactose, gluten, pork, none, pet, pet_num, type_pet, dog, cat, other, db, gender, m_service, y_service, db_law, allergies, medic_f, health_f, h_type, occupation_m, religion, condition_m, misdemeanor, c_background, id_m, id_ag, law, certified, phome, food_s, appnoti) 
  VALUES('NULL', 'NULL', '$houseMail', '$houseName', '$phoneNumber', '$totalRooms', '$mainCity', '$address', '$city', '$state', '$postalCode', 'NULL', 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'NULL', 0, 'NULL', 'no', 'no', 'no', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', '$houseType', 'NULL', 'No', 'No', 'No', 'No', $idAgency, $idAgent, 'NULL', 'No', 'NULL', 'NULL', 'NULL')";
  $basicInformationUpdate = $link->query($basicInformationQuery);


  // ? INSERT HOMESTAY DATA ROOMS IN ROOM
  $roomsQuery = "INSERT INTO room (id_home, type1, bed1, bed1_2, bed1_3, food1, date1, date1_2, date1_3, aprox1, aprox_a1, reservations1, type2, bed2, bed2_2, bed2_3, food2, date2, date2_2, date2_3, aprox2, aprox_a2, reservations2, type3, bed3, bed3_2, bed3_3, food3, date3, date3_2, date3_3, aprox3, aprox_a3, reservations3, type4, bed4, bed4_2, bed4_3, food4, date4, date4_2, date4_3, aprox4, aprox_a4, reservations4, type5, bed5, bed5_2, bed5_3, food5, date5, date5_2, date5_3, aprox5, aprox_a5, reservations5, type6, bed6, bed6_2, bed6_3, food6, date6, date6_2, date6_3, aprox6, aprox_a6, reservations6, type7, bed7, bed7_2, bed7_3, food7, date7, date7_2, date7_3, aprox7, aprox_a7, reservations7, type8, bed8, bed8_2, bed8_3, food8, date8, date8_2, date8_3, aprox8, aprox_a8, reservations8) VALUES(Last_insert_id(), 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 0)";
  $rooms = $link->query($roomsQuery);


  // ? INSERT HOMESTAY DATA ROOMS IN ROOM
  $membersFamilyQuery = "INSERT INTO mem_f (id_home, f_name1, f_lname1, db1, gender1, occupation_f1, db_lawf1, lawf1, re1, f_name2, f_lname2, db2, gender2, occupation_f2, db_lawf2, lawf2, re2, f_name3, f_lname3, db3, gender3, occupation_f3, db_lawf3, lawf3, re3, f_name4, f_lname4, db4, gender4, occupation_f4, db_lawf4, lawf4, re4, f_name5, f_lname5, db5, gender5, occupation_f5, db_lawf5, lawf5, re5, f_name6, f_lname6, db6, gender6, occupation_f6, db_lawf6, lawf6, re6, f_name7, f_lname7, db7, gender7, occupation_f7, db_lawf7, lawf7, re7, f_name8, f_lname8, db8, gender8, occupation_f8, db_lawf8, lawf8, re8) VALUES(Last_insert_id(), 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL')";
  $membersFamily = $link->query($membersFamilyQuery);


  // ? INSERT HOMESTAY DATA ALL IMAGES IN PHOTO_HOME
  $photoHomeQuery = "INSERT INTO photo_home (id_home, pliving, parea1, parea2, parea3, parea4, pbath1, pbath2, pbath3, pbath4, fp, proom1, proom1_2, proom1_3, proom2, proom2_2, proom2_3, proom3, proom3_2, proom3_3, proom4, proom4_2, proom4_3, proom5, proom5_2, proom5_3, proom6, proom6_2, proom6_3, proom7, proom7_2, proom7_3, proom8, proom8_2, proom8_3) VALUES(Last_insert_id(), 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL')";
  $photoHome = $link->query($photoHomeQuery);


  // ? INSERT HOMESTAY DATA IN PROPERTIE_CONTROL
  $propertieControlQuery = "INSERT INTO propertie_control (id_home, id_m, agency, id_ag, db, h_name, room, dir, city, g_pre, ag_pre, pet, status, certified, photo, smoke, vegetarians, halal, kosher, lactose, gluten, pork, none, control) VALUES(Last_insert_id(), 0, 0, 0, 'NULL', 'NULL', 0, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'No', 'NULL', 'NULL', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'Active')";
  $propertieControl = $link->query($propertieControlQuery);

  if (
    $newUser == true &&
    $basicInformationUpdate == true &&
    $rooms == true &&
    $membersFamily == true &&
    $photoHome == true &&
    $propertieControl == true
  ) {
    $status = "ok";
  } else {
    $homeDeleteQuery = $link->query("DELETE FROM pe_home WHERE id_home = Last_insert_id() ");
    $roomDeleteQuery = $link->query("DELETE FROM room WHERE id_home = Last_insert_id() ");
    $membersDeleteQuery = $link->query("DELETE FROM mem_f WHERE id_home = Last_insert_id() ");
    $photoHomeDeleteQuery = $link->query("DELETE FROM photo_home WHERE id_home = Last_insert_id() ");
    $propertieControlDeleteQuery = $link->query("DELETE FROM propertie_control WHERE id_home = Last_insert_id() ");
    $status = 'fail';
  };


  $queryHomestaySaved = $link->query("SELECT id_home, mail_h, h_name, room, dir, city FROM pe_home WHERE mail_h = '$houseMail' ");
  $row_homestay = $queryHomestaySaved->fetch_assoc();
  $resultBasicInformation = array('status' => $status, 'result' => $row_homestay);

  echo json_encode($resultBasicInformation);
} else if ($_POST['updateHouseGallery']) {
  // TODO SAVE HOUSE DETAILS
  function insertImage($image, $dbRow, $link, $folderName, $idHome)
  {
    $imageName = $image['name'];
    $imageTMP = $image['tmp_name'];
    $destination = "public/$folderName/$imageName";

    if ($dbRow == "phome") {
      $updateImageQuery = "UPDATE pe_home SET phome = '$destination' WHERE id_home = '$idHome'  ";
      $updateImageQuery2 = "UPDATE propertie_control SET photo = '$destination' WHERE id_home = '$idHome'  ";
    } else {
      $updateImageQuery = "UPDATE photo_home SET $dbRow = '$destination' WHERE id_home = '$idHome' ";
    }


    if (!file_exists("../../public/$folderName")) mkdir("../../public/$folderName");
    $upload = move_uploaded_file($imageTMP, "../../$destination");


    if ($upload) {
      $updateImage = $link->query($updateImageQuery);
      if (isset($updateImageQuery2)) $updateImage2 = $link->query($updateImageQuery2);

      echo json_encode([$imageName, "OK"]);
    } else echo json_encode([$imageName, "FAIL"]);
  }

  $houseMail = $_POST['mail'];
  $idHome = $_POST['homestay_id'];

  $frontage = $_FILES['frontage'];
  $livingRoom = $_FILES['livingRoom'];
  $familyPicture = $_FILES['familyPicture'];
  $kitchen = $_FILES['kitchen'];
  $diningRoom = $_FILES['diningRoom'];
  $houseArea3 = $_FILES['houseArea3'];
  $houseArea4 = $_FILES['houseArea4'];
  $bathroom1 = $_FILES['bathroom1'];
  $bathroom2 = $_FILES['bathroom2'];
  $bathroom3 = $_FILES['bathroom3'];
  $bathroom4 = $_FILES['bathroom4'];

  $frontage['name'] = "frontage-photo-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $livingRoom['name'] = "living-room-photo-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $familyPicture['name'] = "family-picture-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $kitchen['name'] = "kitchen-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $diningRoom['name'] = "dining-room-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $houseArea3['name'] = "house-area-3-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $houseArea4['name'] = "house-area-4-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom1['name'] = "bathroom-1-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom2['name'] = "bathroom-2-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom3['name'] = "bathroom-3-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $bathroom4['name'] = "bathroom-4-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));


  // ? HOUSE GALLERY
  if (isset($frontage)) insertImage($frontage, "phome", $link,  $houseMail, $idHome);
  if (isset($livingRoom)) insertImage($livingRoom, "pliving", $link,  $houseMail, $idHome);
  if (isset($familyPicture)) insertImage($familyPicture, "fp", $link,  $houseMail, $idHome);

  if (isset($kitchen)) insertImage($kitchen, "parea1", $link,  $houseMail, $idHome);
  if (isset($diningRoom)) insertImage($diningRoom, "parea2", $link,  $houseMail, $idHome);
  if (isset($houseArea3)) insertImage($houseArea3, "parea3", $link,  $houseMail, $idHome);
  if (isset($houseArea4)) insertImage($houseArea4, "parea4", $link,  $houseMail, $idHome);

  if (isset($bathroom1)) insertImage($bathroom1, "pbath1", $link,  $houseMail, $idHome);
  if (isset($bathroom2)) insertImage($bathroom2, "pbath2", $link,  $houseMail, $idHome);
  if (isset($bathroom3)) insertImage($bathroom3, "pbath3", $link,  $houseMail, $idHome);
  if (isset($bathroom4)) insertImage($bathroom4, "pbath4", $link,  $houseMail, $idHome);


  // ?--------------------------------------------------------------------------->
  $proom1 = $_FILES['proom1'];
  $proom1_2 = $_FILES['proom1_2'];
  $proom1_3 = $_FILES['proom1_3'];

  $proom1['name'] = "room-1-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom1_2['name'] = "room-1-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom1_3['name'] = "room-1-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom2 = $_FILES['proom2'];
  $proom2_2 = $_FILES['proom2_2'];
  $proom2_3 = $_FILES['proom2_3'];

  $proom2['name'] = "room-2-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom2_2['name'] = "room-2-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom2_3['name'] = "room-2-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom3 = $_FILES['proom3'];
  $proom3_2 = $_FILES['proom3_2'];
  $proom3_3 = $_FILES['proom3_3'];

  $proom3['name'] = "room-3-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom3_2['name'] = "room-3-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom3_3['name'] = "room-3-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom4 = $_FILES['proom4'];
  $proom4_2 = $_FILES['proom4_2'];
  $proom4_3 = $_FILES['proom4_3'];

  $proom4['name'] = "room-4-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom4_2['name'] = "room-4-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom4_3['name'] = "room-4-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom5 = $_FILES['proom5'];
  $proom5_2 = $_FILES['proom5_2'];
  $proom5_3 = $_FILES['proom5_3'];

  $proom5['name'] = "room-5-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom5_2['name'] = "room-5-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom5_3['name'] = "room-5-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom6 = $_FILES['proom6'];
  $proom6_2 = $_FILES['proom6_2'];
  $proom6_3 = $_FILES['proom6_3'];

  $proom6['name'] = "room-6-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom6_2['name'] = "room-6-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom6_3['name'] = "room-6-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom7 = $_FILES['proom7'];
  $proom7_2 = $_FILES['proom7_2'];
  $proom7_3 = $_FILES['proom7_3'];

  $proom7['name'] = "room-7-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom7_2['name'] = "room-7-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom7_3['name'] = "room-7-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));

  // ?--------------------------------------------------------------------------->

  $proom8 = $_FILES['proom8'];
  $proom8_2 = $_FILES['proom8_2'];
  $proom8_3 = $_FILES['proom8_3'];

  $proom8['name'] = "room-8-a-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom8_2['name'] = "room-8-b-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));
  $proom8_3['name'] = "room-8-c-$date" . str_replace("/", ".", stristr($frontage['type'], "/"));


  // ? ROOMS IMAGES
  if (isset($proom1)) insertImage($proom1, "proom1", $link,  $houseMail, $idHome);
  if (isset($proom1_2)) insertImage($proom1_2, "proom1_2", $link,  $houseMail, $idHome);
  if (isset($proom1_3)) insertImage($proom1_3, "proom1_3", $link,  $houseMail, $idHome);

  if (isset($proom2)) insertImage($proom2, "proom2", $link,  $houseMail, $idHome);
  if (isset($proom2_2)) insertImage($proom2_2, "proom2_2", $link,  $houseMail, $idHome);
  if (isset($proom2_3)) insertImage($proom2_3, "proom2_3", $link,  $houseMail, $idHome);

  if (isset($proom3)) insertImage($proom3, "proom3", $link,  $houseMail, $idHome);
  if (isset($proom3_2)) insertImage($proom3_2, "proom3_2", $link,  $houseMail, $idHome);
  if (isset($proom3_3)) insertImage($proom3_3, "proom3_3", $link,  $houseMail, $idHome);

  if (isset($proom4)) insertImage($proom4, "proom4", $link,  $houseMail, $idHome);
  if (isset($proom4_2)) insertImage($proom4_2, "proom4_2", $link,  $houseMail, $idHome);
  if (isset($proom4_3)) insertImage($proom4_3, "proom4_3", $link,  $houseMail, $idHome);

  if (isset($proom5)) insertImage($proom5, "proom5", $link,  $houseMail, $idHome);
  if (isset($proom5_2)) insertImage($proom5_2, "proom5_2", $link,  $houseMail, $idHome);
  if (isset($proom5_3)) insertImage($proom5_3, "proom5_3", $link,  $houseMail, $idHome);

  if (isset($proom6)) insertImage($proom6, "proom6", $link,  $houseMail, $idHome);
  if (isset($proom6_2)) insertImage($proom6_2, "proom6_2", $link,  $houseMail, $idHome);
  if (isset($proom6_3)) insertImage($proom6_3, "proom6_3", $link,  $houseMail, $idHome);

  if (isset($proom7)) insertImage($proom7, "proom7", $link,  $houseMail, $idHome);
  if (isset($proom7_2)) insertImage($proom7_2, "proom7_2", $link,  $houseMail, $idHome);
  if (isset($proom7_3)) insertImage($proom7_3, "proom7_3", $link,  $houseMail, $idHome);

  if (isset($proom8)) insertImage($proom8, "proom8", $link,  $houseMail, $idHome);
  if (isset($proom8_2)) insertImage($proom8_2, "proom8_2", $link,  $houseMail, $idHome);
  if (isset($proom8_3)) insertImage($proom8_3, "proom8_3", $link,  $houseMail, $idHome);
} else if ($_POST['updateAdditionalInformation']) {
  // TODO SAVE ADDITIONAL INFORMATION
  if (empty($_POST['totalYears']) || $_POST['totalYears'] === 'NULL') $totalyears = 'NULL';
  else {
    $d_totalyears = date_create($_POST['totalYears']);
    $totalyears = date_format($d_totalyears, 'Y-m-d');
  }
  $description = addslashes($_POST['description']);
  if ($_POST['pet'] == "Yes" && $_POST['totalPets'] < 1 || $_POST['totalPets'] > 9) {
    $_POST['totalPets'] = "NULL";
    $_POST['specifyPets'] = "NULL";
  } else if ($_POST['pet'] != "Yes") {
    $_POST['totalPets'] = "NULL";
    $_POST['specifyPets'] = "NULL";
  }
  $queryAdditionalInformation =
    "UPDATE pe_home SET 
      des = '$description', 
      a_pre= '$_POST[academy]', 
      g_pre= '$_POST[gender]', 
      ag_pre= '$_POST[age]', 
      smoke= '$_POST[smokers]', 
      vegetarians= '$_POST[vegetarians]', 
      halal= '$_POST[halal]', 
      kosher= '$_POST[kosher]', 
      lactose= '$_POST[lactose]', 
      gluten= '$_POST[gluten]', 
      pork= '$_POST[pork]',  
      none= 'NULL',
      m_service = '$_POST[meals]', 
      y_service= '$totalyears', 
      pet= '$_POST[pet]', 
      pet_num= '$_POST[totalPets]', 
      type_pet= '$_POST[specifyPets]', 
      dog= '$_POST[dog]', 
      cat= '$_POST[cat]', 
      other= '$_POST[other]'
      WHERE id_home = '$_POST[homestay_id]'  ";
  $updateAdditionalInformation = $link->query($queryAdditionalInformation);

  if ($updateAdditionalInformation == true) $status = "ok";
  else {
    $homeDeleteQuery = $link->query("DELETE FROM pe_home WHERE id_home = '$_POST[homestay_id]' ");
    $roomDeleteQuery = $link->query("DELETE FROM room WHERE id_home = '$_POST[homestay_id]' ");
    $membersDeleteQuery = $link->query("DELETE FROM mem_f WHERE id_home = '$_POST[homestay_id]' ");
    $photoHomeDeleteQuery = $link->query("DELETE FROM photo_home WHERE id_home = '$_POST[homestay_id]' ");
    $propertieControlDeleteQuery = $link->query("DELETE FROM propertie_control WHERE id_home = '$_POST[homestay_id]' ");
    $status = 'fail';
  }

  $resultAdditionalInformation = array(
    'status' => $status,
    'result' => array(
      'description' => $_POST['description'],
      'academy' => $_POST['academy'],
      'gender' => $_POST['gender'],
      'age' => $_POST['age'],
      'smokers' => $_POST['smokers'],
      'meals' => $_POST['meals'],
      'totalYears' => $_POST['totalYears'],

      'pet' => $_POST['pet'],
      'totalPets' => $_POST['totalPets'],
      'dog' => $_POST['dog'],
      'cat' => $_POST['cat'],
      'other' => $_POST['other'],
      'specifyPets' => $_POST['specifyPets'],

      'vegetarians' => $_POST['vegetarians'],
      'halal' => $_POST['halal'],
      'kosher' => $_POST['kosher'],
      'lactose' => $_POST['lactose'],
      'gluten' => $_POST['gluten'],
      'pork' => $_POST['pork'],
    ),
  );

  echo json_encode($resultAdditionalInformation);
} else if ($_POST['updateFamilyInformation']) {
  // TODO SAVE FAMILY INFORMATION
  if (empty($_POST['dateBackgroundCheckContact']) || $_POST['dateBackgroundCheckContact'] === 'NULL') $db_law = 'NULL';
  else {
    $d_dbLaw = date_create($_POST['dateBackgroundCheckContact']);
    $db_law = date_format($d_dbLaw, 'Y-m-d');
  }

  if (empty($_POST['dateBirthContact']) || $_POST['dateBirthContact'] === 'NULL') $db = 'NULL';
  else {
    $d_db = date_create($_POST['dateBirthContact']);
    $db = date_format($d_db, 'Y-m-d');
  }
  $queryUpdateFamilyInformation = "UPDATE pe_home SET 
    allergies = '$_POST[allergies]',
    medic_f = '$_POST[medications]',
    health_f = '$_POST[healthProblems]',
    name_h= '$_POST[nameContact]',
    l_name_h = '$_POST[lastNameContact]',
    db = '$db',
    gender = '$_POST[genderContact]',
    cell = '$_POST[phoneContact]',
    occupation_m = '$_POST[occupationContact]',
    db_law = '$db_law',
    num_mem = '$_POST[totalMembers]',
    backg = '$_POST[background]',
    backl = '$_POST[backgroundLanguage]',
    religion = '$_POST[religion]',
    condition_m = '$_POST[condition]',
    misdemeanor = '$_POST[misdemeanor]',
    c_background = '$_POST[finalQuestion]' WHERE id_home = '$_POST[homestay_id]'  ";
  $updateFamilyInformation = $link->query($queryUpdateFamilyInformation);

  if ($updateFamilyInformation) $status = 'ok';
  else $status = 'fail';


  if (isset($_FILES['backgroundCheck'])) {
    $pdf = $_FILES['backgroundCheck'];
    $pdf['name'] = "background-check-main-$date.pdf";
    $folderName = $_POST['mail'];

    $updateBackgroundCheck = "UPDATE pe_home SET law = 'public/$folderName/$pdf[name]' WHERE id_home = '$_POST[homestay_id]'  ";

    if (!file_exists("../../public/$folderName")) mkdir("../../public/$folderName");
    $upload = move_uploaded_file($pdf['tmp_name'], "../../public/$folderName/$pdf[name]");

    if ($upload) {
      $updateFile = $link->query($updateBackgroundCheck);

      $resultFile = [$pdf['name'], "OK"];
    } else $resultFile = [$pdf['name'], "FAIL"];
  } else $resultFile = ["Background Check Main", "NOT FOUND"];


  $resultFamilyInformation = array(
    'status' => $status,
    'result' => array(
      'allergies' => $_POST['allergies'],
      'medications' => $_POST['medications'],
      'healthProblems' => $_POST['healthProblems'],
      'nameContact' => $_POST['nameContact'],
      'lastNameContact' => $_POST['lastNameContact'],
      'dateBirthContact' => $_POST['dateBirthContact'],
      'genderContact' => $_POST['genderContact'],
      'phoneContact' => $_POST['phoneContact'],
      'occupationContact' => $_POST['occupationContact'],
      'dateBackgroundCheckContact' => $_POST['dateBackgroundCheckContact'],
      'totalMembers' => $_POST['totalMembers'],
      'background' => $_POST['background'],
      'backgroundLanguage' => $_POST['backgroundLanguage'],
      'religion' => $_POST['religion'],
      'condition' => $_POST['condition'],
      'misdemeanor' => $_POST['misdemeanor'],
      'finalQuestion' => $_POST['finalQuestion'],
    ),
    'filesResult' => $resultFile,
  );

  echo json_encode($resultFamilyInformation);
} else if ($_POST['updateFamilyMembers']) {
  // TODO SAVE FAMILY MEMBERS
  $houseMail = $_POST['mail'];
  $idHome = $_POST['homestay_id'];

  $i = 1;
  while ($i <= 8) {
    if (isset($_POST["member" . $i . "_name"]) && isset($_POST["member" . $i . "_lastName"])) {
      if (empty($_POST["member" . $i . "_dateBirth"]) || $_POST["member" . $i . "_dateBirth"] == 'NULL') {
        $memberDb = 'NULL';
      } else {
        $d_memberDb = date_create($_POST["member" . $i . "_dateBirth"]);
        $memberDb = date_format($d_memberDb, 'Y-m-d');
      }
      if (empty($_POST["member" . $i . "_dateBackground"]) || $_POST["member" . $i . "_dateBackground"] == 'NULL') {
        $memberBc = 'NULL';
      } else {
        $d_memberBc = date_create($_POST["member" . $i . "_dateBackground"]);
        $memberBc = date_format($d_memberBc, 'Y-m-d');
      }

      $memberName = $_POST["member" . $i . "_name"];
      $memberLastName = $_POST["member" . $i . "_lastName"];
      $memberDateBirth = $memberDb;
      $memberGender = $_POST["member" . $i . "_gender"];
      $memberRelation = $_POST["member" . $i . "_relation"];
      $memberOccupation = $_POST["member" . $i . "_occupation"];
      $memberDateBackground = $memberBc;

      $queryUpdateFamilyMembers = "UPDATE mem_f SET 
        f_name$i = '$memberName',
        f_lname$i = '$memberLastName',
        db$i = '$memberDateBirth',
        gender$i = '$memberGender',
        re$i = '$memberRelation',
        occupation_f$i = '$memberOccupation' WHERE id_home = '$idHome'  ";
      $updateFamilyMembers = $link->query($queryUpdateFamilyMembers);

      if (!empty($_FILES["member" . $i . "_backgroundCheck"])) {
        $memberBackgroundCheck = $_FILES["member" . $i . "_backgroundCheck"];
        $memberBackgroundCheck['name'] = "member-$i-$date.pdf";

        if (!file_exists("../../public/$houseMail")) mkdir("../../public/$houseMail");
        $upload = move_uploaded_file($memberBackgroundCheck['tmp_name'], "../../public/$houseMail/$memberBackgroundCheck[name]");

        if ($upload) {
          $queryUpdateFamilyMembersPDF = "UPDATE mem_f SET lawf$i = 'public/$houseMail/$memberBackgroundCheck[name]', db_lawf$i = '$memberDateBackground' WHERE id_home = '$idHome'  ";
          $updateFamilyMembersPDF = $link->query($queryUpdateFamilyMembersPDF);
          if ($updateFamilyMembersPDF) $resultFile = [$memberBackgroundCheck['name'], 'OK'];
        }
      }


      if ($updateFamilyMembers) $status = 'ok';

      $resultFamilyMember = array(
        'status' => $status,
        'result' => array(
          'memberName' => $memberName,
          'memberLastName' => $memberLastName,
          'memberDateBirth' => $memberDateBirth,
          'memberGender' => $memberGender,
          'memberRelation' => $memberRelation,
          'memberOccupation' => $memberOccupation,
          'memberDateBackground' => $memberDateBackground,
        ),
        'resultFile' => $resultFile
      );

      echo json_encode($resultFamilyMember);
    } else echo json_encode("No hay nombre y apellido");
    $i++;
  }
} else if ($_POST['completeData']) {
  // TODO SAVE COMPLETE DATA
  $houseMail = $_POST['mail'];
  $dateControl = date('Y-m-d H:i:s');

  // ? INSERT WEB MASTER
  $queryWebMaster = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m) VALUES('$row_agent[a_mail]', 'Register a New Homestay', '$dateControl', '$_POST[mail]', '$row_agent[id_m]') ");


  // ? INSERT NOTIFICATION HOMESTAY
  $notificationQuery = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, status, extend) VALUES('$m_a_name', '$m_l_a_name', '$row_manager[mail]', 'NULL', 'NULL', 'NULL', 'NULL', '$houseMail', '$dateControl', '0', '0', 'Welcome to Homebor', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL')");
  
  // ? INSERT NOTIFICATION PROVIDER
  $notificationQuery = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, status, extend) VALUES('$_POST[nameNotification]', '$_POST[lastnameNotification]', '$row_agent[a_mail]', 'NULL', 'NULL', 'NULL', 'NULL', '$row_manager[mail]', '$dateControl', '0', '0', 'Register Homestay', 'NULL', 'NULL', '$houseMail', 'NULL', 'NULL')");


  // ? UPDATE USERS
  $newUserQuery =  $link->query("UPDATE users SET name = '$_POST[nameContact]', l_name = '$_POST[lastNameContact]' WHERE mail = '$_POST[mail]' ");

  if ($newUserQuery == true) $statusUser = "ok";
  else $statusUser = "fail";


  // ? UPDATE PROPERTIE CONTROL
  $queryPropertieControl = "UPDATE propertie_control SET 
  id_m = '$idAgency', 
  agency = '0', 
  id_ag = '$idAgent', 
  db = '$dateControl', 
  h_name = '$_POST[houseName]', 
  room = '$_POST[totalRooms]', 
  dir = '$_POST[address]', 
  city = '$_POST[city]', 
  g_pre = '$_POST[gender]', 
  pet = '$_POST[pet]', 
  ag_pre = '$_POST[age]', 
  status = 'Available', 
  certified = 'No', 
  smoke = '$_POST[smokers]', 
  vegetarians = '$_POST[vegetarians]', 
  halal = '$_POST[halal]', 
  kosher = '$_POST[kosher]', 
  lactose = '$_POST[lactose]', 
  gluten = '$_POST[gluten]', 
  pork = '$_POST[pork]', 
  none = 'NULL', 
  control = 'Active' WHERE id_home = '$_POST[homestay_id]' ";
  $updatePropertieControl = $link->query($queryPropertieControl);

  if ($updatePropertieControl == true) $statusControl = "ok";
  else $statusControl = "fail";

  $resultCompleteData = array(
    'statusUser' => $statusUser,
    'statusControl' => $statusControl,
  );

  echo json_encode($resultCompleteData);
}