<?php 
    include '../../xeon.php';
    session_start();

    // Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

    date_default_timezone_set("America/Toronto");
    $date = date('Y-m-d H:i:s');
    $dateImage = date('YmdHisv');


    if (empty($_POST['name_a'])) $name_a = 'NULL';
    else $name_a =  $_POST['name_a'];     
    
    if (empty($_POST['dir'])) $dir = 'NULL';
    else $dir =  $_POST['dir'];     
    
    if (empty($_POST['state'])) $state = 'NULL';
    else $state =  $_POST['state'];     
    
    if (empty($_POST['city'])) $city = 'NULL';
    else $city =  $_POST['city'];     
    
    if (empty($_POST['p_code'])) $p_code = 'NULL';
    else $p_code =  $_POST['p_code'];
    
    if (empty($_POST['acronym_s'])) $acronym_s = 'NULL';
    else $acronym_s =  $_POST['acronym_s'];
    
    if(empty($_POST['id_ac']) || $_POST['id_ac'] == 'NULL'){
      
      $register_aca = "INSERT INTO academy (name_a, acronyms, dir_a, state_a, city_a, p_code_a, id_photo, photo_a, status_a) VALUES ('$name_a', '$acronym_s', '$dir', '$state', '$city', '$p_code', 'NULL', 'NULL', 'Active')";
      $re_register=$link->query($register_aca);
      
      $academy_r = $link->query("SELECT * FROM academy ORDER BY id_ac DESC LIMIT 1");
      $r_acar=$academy_r->fetch_assoc();
      
      $n_a = $r_acar['id_ac'];
      
      $paths = "../../public_a/". $n_a;
      
      if(!empty($_FILES['photo_aca']['name'])){
        
        $schoolImage_tmp = $_FILES['photo_aca']['tmp_name'];
        $schoolImageType = stristr($_FILES['photo_aca']['type'], "/");
        $fileExtension = str_replace( "/", ".", $schoolImageType);
        $schoolImageUrl = "public_a/".$n_a."/SchoolImage-" . $dateImage . $fileExtension;
        $schoolImage = "SchoolImage-" . $dateImage . $fileExtension;
        
        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($schoolImage_tmp, $paths . '/' . $schoolImage);
          
      } else $schoolImageUrl = 'NULL';
        
      $upd_aca = "UPDATE academy SET id_photo='$r_acar[id_ac]', photo_a='$schoolImageUrl' WHERE id_ac='$r_acar[id_ac]'";
      $re_up=$link->query($upd_aca);

      echo '<script type="text/javascript">window.top.location="index";</script>'; exit;
        
    }else{

      $n_a = $_POST['id_ac'];

      $paths = "../../public_a/". $n_a;
        
      if(!empty($_FILES['photo_aca']['name'])){
          
        $schoolImage_tmp = $_FILES['photo_aca']['tmp_name'];
        $schoolImageType = stristr($_FILES['photo_aca']['type'], "/");
        $fileExtension = str_replace( "/", ".", $schoolImageType);
        $schoolImageUrl = "public_a/".$n_a."/SchoolImage-" . $dateImage . $fileExtension;
        $schoolImage = "SchoolImage-" . $dateImage . $fileExtension;

        if (!file_exists($paths)) mkdir($paths, 0777);
        move_uploaded_file($schoolImage_tmp, $paths . '/' . $schoolImage);
          
      } else $schoolImageUrl = 'NULL';

      date_default_timezone_set("America/Toronto");
      $dateweb = date('Y-m-d H:i:s');

      $query2 = "UPDATE academy SET name_a='$name_a', dir_a='$dir', state_a='$state', city_a='$city', p_code_a='$p_code', photo_a='$schoolImageUrl', acronyms='$acronym_s', status_a='Active' WHERE id_ac='$n_a'";
      $resultaca=$link->query($query2);
      $query3 = "INSERT INTO webmaster (user, activity, dates, edit_user, id_m) VALUES ('$usuario', 'Register a Academy', '$dateweb', '$mail_s', '$id_m')";

      echo '<script type="text/javascript">window.top.location="directory_students";</script>'; exit;

    }

?>