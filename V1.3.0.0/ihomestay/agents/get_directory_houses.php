<?php 

session_start();
include '../../xeon.php';
$usuario = $_SESSION['username'];

// TODO QUERIES

$agentsQuery = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario'");
$row_agents = $agentsQuery->fetch_assoc();

$managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_agents[id_m]'");
$row_manager = $managerQuery->fetch_assoc();

$houseQueryNum = $link->query("SELECT id_home FROM propertie_control WHERE id_m = '$row_agents[id_m]'");
$num_houses = mysqli_num_rows($houseQueryNum);

$houseQuery = $link->query("SELECT * FROM propertie_control WHERE id_m = '$row_agents[id_m]' AND certified = 'Yes' ORDER BY id_p DESC");

$jsonHouses = null;

if($num_houses > 0){
  while ($rowHouses = mysqli_fetch_array($houseQuery)) {

    $jsonEvents = null;

    $housesQuery = $link->query("SELECT * FROM pe_home WHERE id_home = '$rowHouses[id_home]'");
    $row_peHome = $housesQuery->fetch_assoc();
    
    $roomsHousesQuery = $link->query("SELECT * FROM room WHERE id_home = '$rowHouses[id_home]'");
    $row_roomHome = $roomsHousesQuery->fetch_assoc();
    
    $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$row_peHome[mail_h]'");

    
    while ($row_events = mysqli_fetch_array($eventsQuery)) {
      if($row_events['color'] == '#232159') $room = '1';
      if($row_events['color'] == '#982A72') $room = '2';
      if($row_events['color'] == '#394893') $room = '3';
      if($row_events['color'] == '#a54483') $room = '4';
      if($row_events['color'] == '#5d418d') $room = '5';
      if($row_events['color'] == '#392b84') $room = '6';
      if($row_events['color'] == '#b15391') $room = '7';
      if($row_events['color'] == '#4f177d') $room = '8';

      $jsonEvents[] = array(
        'e_title' => $row_events['title'],
        'e_room' => $room,
        'e_start' => $row_events['start'],
        'e_end' => $row_events['end'],
        'e_status' => $row_events['status'],
      );
    }

    if($rowHouses['h_name'] == 'NULL') $h_name = 'Empty';
    else $h_name = $rowHouses['h_name'];

    if($rowHouses['photo'] == 'NULL') $h_photo = 'Empty';
    else $h_photo = $rowHouses['photo'];
    
    if($rowHouses['dir'] == 'NULL' && $rowHouses['city'] != 'NULL') $h_address = $rowHouses['city'];
    else if($rowHouses['dir'] != 'NULL' && $rowHouses['city'] == 'NULL') $h_address = $rowHouses['dir'];
    else if($rowHouses['dir'] != 'NULL' && $rowHouses['city'] != 'NULL') $h_address = $rowHouses['dir'] . ', ' . $rowHouses['city'];
    else $h_address = 'Empty';

    if($rowHouses['g_pre'] == 'NULL') $h_gender = 'Empty';
    else $h_gender = $rowHouses['g_pre'];
    
    if($rowHouses['ag_pre'] == 'NULL') $h_age = 'Empty';
    else $h_age = $rowHouses['ag_pre'];
    
    if($rowHouses['room'] == 'NULL') $h_bedrooms = 'Empty';
    else $h_bedrooms = $rowHouses['room'];
    
    if($rowHouses['smoke'] == 'NULL') $h_smoke = 'Empty';
    else $h_smoke = $rowHouses['smoke'];

    if($rowHouses['pet'] == 'NULL') $h_pets = 'Empty';
    else $h_pets = $rowHouses['pet'];

    if($rowHouses['status'] == 'NULL') $h_status = 'Empty';
    else $h_status = $rowHouses['status'];
    
    if($row_peHome['vegetarians'] == 'yes') $h_vegetarians = 'vegetarians';
    else $h_vegetarians = "NULL";
    
    if($row_peHome['halal'] == 'yes') $h_halal = 'halal';
    else $h_halal = "NULL";
    
    if($row_peHome['kosher'] == 'yes') $h_kosher = 'kosher';
    else $h_kosher = "NULL";
    
    if($row_peHome['lactose'] == 'yes') $h_lactose = 'lactose';
    else $h_lactose = "NULL";
    
    if($row_peHome['gluten'] == 'yes') $h_gluten = 'gluten';
    else $h_gluten = "NULL";
    
    if($row_peHome['pork'] == 'yes') $h_pork = 'pork';
    else $h_pork = "NULL";
    
    if($row_peHome['vegetarians'] == 'yes' || $row_peHome['halal'] == 'yes' || $row_peHome['kosher'] == 'yes' || $row_peHome['lactose'] == 'yes' || $row_peHome['gluten'] == 'yes' || $row_peHome['pork'] == 'yes') $h_diet = 'Yes';
    else $h_diet = "No";


    if($row_peHome['backl'] == 'NULL') $h_language = 'Empty';
    else $h_language = $row_peHome['backl'];
    
    if($row_peHome['food_s'] == 'NULL') $h_foods = 'Empty';
    else $h_foods = $row_peHome['food_s'];
    
    if($row_peHome['m_city'] == 'NULL') $h_mCity = 'Empty';
    else $h_mCity = $row_peHome['m_city'];
    
    

    $jsonHouses[] = array(
      'response' => 'houses',
      'h_id' => $row_peHome['id_home'],
      'h_name' => $h_name,
      'h_photo' => $rowHouses['photo'],
      'h_rooms' => $row_roomHome,
      'h_address' => $h_address,
      'h_gender' => $h_gender,
      'h_age' => $h_age,
      'h_bedrooms' => $h_bedrooms,
      'h_smoke' => $h_smoke,
      'h_pets' => $h_pets,
      'h_status' => $h_status,
      'h_language' => $h_language,
      'h_foods' => $h_foods,
      'h_diet' => $h_diet,
      'h_vegetarians' => $h_vegetarians,
      'h_halal' => $h_halal,
      'h_kosher' => $h_kosher,
      'h_lactose' => $h_lactose,
      'h_gluten' => $h_gluten,
      'h_pork' => $h_pork,
      'h_mCity' => $h_mCity,
      'h_events' => $jsonEvents,
    );
  }
}else{
  $jsonHouses[] = array(
    'response' => 'not houses',
  );
}
echo json_encode($jsonHouses);




?>