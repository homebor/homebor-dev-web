<?php
session_start();
include_once('../../xeon.php');
include '../../cript.php';
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

$query = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
$row=$query->fetch_assoc();

$query3 = $link->query("SELECT id_user, usert, psw FROM users WHERE mail = '$usuario' ");
$row3=$query3->fetch_assoc();

$query2 = $link->query("SELECT * FROM manager INNER JOIN agents ON manager.id_m = agents.id_m AND agents.a_mail = '$usuario'");
$row2=$query2->fetch_assoc();

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/edit_agent.css">

  <!--Mapbox Links-->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Coordinator Edit</title>

</head>

<body>

  <?php include 'header.php' ?>

  <div class="container_register">

    <main class="card__register">
      <form id="form" class="ts-form" autocomplete="off" action="edit_agent_profile.php" method="post"
        enctype="multipart/form-data">

        <h1 class="title__register">Edit Coordinator Profile </h1>


        <div class="row m-0 p-0 center">

          <div class="col-md-4 column-1">

            <div class="card">

              <h3 class="form__group-title__col-4">Basic Information</h3>

              <div class="information__content">

                <div class="form__group__div__photo">
                  <img class="form__group__photo" id="preview" src="https://homebor.com/<?php echo $row['photo'] ?>" alt="">

                  <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_c"
                    id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                  <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i"
                    title="Change Frontage Photo"></label>
                </div>

                <div class="info"></div>



                <!-- Group Name -->

                <div class="form__group form__group__name" id="group__name">
                  <div class="form__group__icon-input">
                    <label for="name" class="form__label">Name *</label>

                    <label for="name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['name']) || $row['name'] == 'NULL'){ ?>

                    <input type="text" id="name" name="name" class="form__group-input names" placeholder="e.g. John">

                    <?php }else{ ?>

                    <input type="text" id="name" name="name" class="form__group-input names"
                      value="<?php echo $row['name'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>

                </div>

                <?php
                                echo '<input type="text" class="form-control" id="folder" name="folder"  value='.$row2['mail'].' hidden>'; ?>

                <!-- Group Last Name -->

                <div class="form__group form__group__l_name" id="group__l_name">
                  <div class="form__group__icon-input">

                    <label for="l_name" class="form__label">Last Name *</label>

                    <label for="l_name" class="icon"><i class="icon icon__names"></i></label>

                    <?php if(empty($row['l_name']) || $row['l_name'] == 'NULL'){ ?>

                    <input type="text" id="l_name" name="l_name" class="form__group-input names"
                      placeholder="e.g. Smith">

                    <?php }else{ ?>

                    <input type="text" id="l_name" name="l_name" class="form__group-input names"
                      value="<?php echo $row['l_name'] ?>">

                    <?php } ?>

                  </div>
                  <p class="form__group__input-error">Without special characters or numbers</p>
                </div>

                <!-- Group Destination City  -->
                <div class="form__group form__group__basic form__group__l_name" id="group__destination_c">

                  <div class="form__group__icon-input">

                    <label for="base_home" class="form__label smoker"> Base Home </label>

                    <label for="base_home" class="icon"><i class="icon icon__city"></i></label>

                    <select class="custom-select form__group-input smoke_s" id="base_home" name="base_home"
                      onchange="otherCity()">

                      <?php if(empty($row['base_home']) && $row['base_home'] == 'NULL'){ ?>

                      <option hidden="option" selected disabled>Select Option</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Toronto'){ ?>

                      <option value="Toronto" selected>Toronto</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Montreal'){ ?>

                      <option value="Montreal" selected>Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Ottawa'){ ?>

                      <option value="Ottawa" selected>Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Quebec'){ ?>

                      <option value="Quebec" selected>Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Calgary'){ ?>

                      <option value="Calgary" selected>Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Vancouver'){ ?>

                      <option value="Vancouver" selected>Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] == 'Victoria'){ ?>

                      <option value="Victoria" selected>Victoria</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>
                      <option value="Other">Other</option>

                      <?php }else if($row['base_home'] != 'Toronto' || $row['base_home'] != 'Montreal' && $row['base_home'] != 'Ottawa' || $row['base_home'] == 'Quebec' || $row['base_home'] != 'Calgary' || $row['base_home'] != 'Vancouver' || $row['base_home'] != 'Victoria'){ ?>

                      <option value="Other" selected>Other</option>
                      <option value="Victoria">Victoria</option>
                      <option value="Vancouver">Vancouver</option>
                      <option value="Calgary">Calgary</option>
                      <option value="Quebec">Quebec</option>
                      <option value="Ottawa">Ottawa</option>
                      <option value="Montreal">Montreal</option>
                      <option value="Toronto">Toronto</option>

                      <?php } ?>
                    </select>
                  </div>
                  <p class="form__group__input-error">Enter a valid Date</p>

                </div>

                <div class="form__group form__group__basic form__group__other_c d-none" id="group__other_c">

                  <label class="label__other__city">Specify the Other City</label>

                  <?php if($row['base_home'] != 'Victoria' || $row['base_home'] != 'Vancouver' || $row['base_home'] != 'Calgary' || $row['base_home'] != 'Quebec' || $row['base_home'] != 'Ottawa' || $row['base_home'] != 'Montreal' || $row['base_home'] != 'Toronto'){ ?>

                  <input class="input__other_city" id="text_destination" type="text" name="base_home_o"
                    value="<?php echo $row['base_home'] ?>">

                  <?php }else{ ?>

                  <input class="input__other_city" id="text_destination" type="text" name="base_home_o">

                  <?php } ?>

                </div>


                <script>
                const $city = document.querySelector("#base_home");

                function otherCity() {
                  const citiI = $city.selectedIndex;
                  if (citiI === -1) return; // Esto es cuando no hay elementos
                  const optionCity = $city.options[citiI];

                  if (optionCity.value == 'Other') {

                    document.getElementById('group__other_c').classList.remove('d-none');

                  } else {
                    document.getElementById('group__other_c').classList.add('d-none');
                    document.getElementById('text_destination').value = '';
                  }

                };

                otherCity();
                </script>


              </div>

            </div>

          </div>


          <input type="hidden" name="id_c" value="<?php echo $row3['id_user'] ?>">



          <div class="col-md-8 ">

            <div class="card column-2">

              <h3 class="form__group-title__col-4">Coordinator Information</h3>

              <div class="information__content information__content-col-8 mt-4">

                <div class="row form__group-basic-content mt-auto mb-autp">

                  <!-- Group Coordinator Mail* 
                                <div class="form__group col-md-4 form__group__basic form__group__a_mail" id="group__a_mail">
                                    <div class="form__group__icon-input">

                                        <label for="a_mail" class="form__label">Mail *</label>

                                        <label for="a_mail" class="icon"><i class="icon icon__a_name"></i></label>

                                        <?php if(empty($row['a_mail']) || $row['a_mail'] == 'NULL'){ ?>
                                        
                                            <input type="text" id="a_mail" name="a_mail" class="form__group-input a_mail" placeholder="e.g. johnsmith@gmail.com">

                                        <?php }else{ ?>

                                            <input type="text" id="a_mail" name="a_mail" class="form__group-input a_mail" value="<?php echo $row3['mail'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid email</p>
                                </div>-->



                  <!-- Coordinator Password -->
                  <div class="form__group col-md-4 form__group__basic form__group__pass" id="group__pass">
                    <div class="form__group__icon-input">

                      <label for="pass" class="form__label">Password *</label>

                      <label for="pass" class="icon"><i class="icon icon__password"></i></label>

                      <?php

                                        $psw = SED::decryption($row3['psw']);
                                        
                                        if(empty($psw) || $psw == 'NULL'){ ?>

                      <input type="password" id="pass" name="pass" class="form__group-input password"
                        placeholder="Enter your password">

                      <?php }else{ ?>

                      <input type="password" id="pass" name="pass" class="form__group-input password"
                        value="<?php echo $psw ?>" readonly>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Min. 4 characters</p>
                  </div>

                  <!-- Group Coordinator Contact -->
                  <div class="form__group col-md-4 form__group__basic form__group__num" id="group__num">
                    <div class="form__group__icon-input">

                      <label for="num" class="form__label">Contact Number</label>

                      <label for="num" class="icon"><i class="icon icon__num"></i></label>

                      <?php if(empty($row['num']) || $row['num'] == 'NULL'){ ?>

                      <input type="text" id="num" name="num" class="form__group-input num" placeholder="e.g. 5557891">

                      <?php }else{ ?>

                      <input type="text" id="num" name="num" class="form__group-input num"
                        value="<?php echo $row['num'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Coordinator Alternative Contact -->
                  <div class="form__group col-md-4 form__group__basic" id="group__num2">
                    <div class="form__group__icon-input">

                      <label for="num2" class="form__label">Alternative Contact</label>

                      <label for="num2" class="icon"><i class="icon icon__num"></i></label>

                      <?php if(empty($row['num2']) || $row['num2'] == 'NULL'){ ?>

                      <input type="text" id="num2" name="num2" class="form__group-input num2"
                        placeholder="e.g. 5557891"></input>

                      <?php }else{ ?>

                      <input type="text" id="num2" name="num2" class="form__group-input num2"
                        value="<?php echo $row['num2'] ?>"></input>

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Enter the number correctly</p>
                  </div>


                  <!-- Group Language Spoken -->
                  <div class="form__group col-md-4 form__group__basic form__group__language" id="group__language">
                    <div class="form__group__icon-input">

                      <label for="language" class="form__label">Language Spoken</label>

                      <label for="language" class="icon"><i class="icon icon__city"></i></label>

                      <?php if(empty($row['language']) || $row['language'] == 'NULL'){ ?>

                      <input type="text" id="language" name="language" class="form__group-input language"
                        placeholder="e.g. Spanish, English, etc.">

                      <?php }else{ ?>

                      <input type="text" id="language" name="language" class="form__group-input language"
                        value="<?php echo $row['language'] ?>">

                      <?php } ?>

                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>

                  <input type="hidden" class="form-control" id="agency" name="agency"
                    value="<?php echo "$row6[a_name]";?>">


                  <!-- Group Specialization -->
                  <div class="form__group col-md-4 form__group__basic form__group__specialization"
                    id="group__specialization">
                    <div class="form__group__icon-input">

                      <label for="specialization" class="form__label">Specialization</label>

                      <label for="specialization" class="icon"><i class="icon icon__spe"></i></label>

                      <select class="custom-select form__group-input specialization" id="specialization"
                        name="specialization">

                        <?php if(empty($row['specialization']) || $row['specialization'] == 'NULL'){ ?>

                        <option hidden="option">-- Select Option -- </option>
                        <option value="homestay">Homestay</option>
                        <option value="students">Students</option>
                        <option value="both">Both</option>

                        <?php }else if($row['specialization'] = 'homestay'){ ?>

                        <option value="homestay" selected>Homestay</option>
                        <option value="students">Students</option>
                        <option value="both">Both</option>

                        <?php }else if($row['specialization'] = 'students'){ ?>

                        <option value="students" selected>Students</option>
                        <option value="homestay">Homestay</option>
                        <option value="both">Both</option>

                        <?php }else if($row['specialization'] = 'both'){ ?>

                        <option value="both" selected>Both</option>
                        <option value="students">Students</option>
                        <option value="homestay">Homestay</option>

                        <?php } ?>

                      </select>



                    </div>
                    <p class="form__group__input-error">Only numbers</p>
                  </div>

                </div>

              </div>

            </div>

            <?php
                                
                                   
                                    echo'<input type="text" class="form-control" id="id_user" name="id_user" value="'.$usuario.'" hidden>
';
                             
                                ?>

            <?php
                                
                                   
                                    echo'<input type="text" class="form-control" id="id_ag" name="id_ag" value="'.$row['id_ag'].'" hidden>
';

                            echo'<input type="text" class="form-control" id="id_m" name="id_m" value="'.$row['id_m'].'" hidden>
';
                             
                                ?>


            <!--<div class="card">

                    <h3 class="form__group-title__col-4">Reputation</h3>

                    <div class="row justify-content-center">

                        <div class="col-md-10 p-0">

                            <p class="clasificacion text-center p-4 mb-0">
                                <input id="radio1" type="radio" name="estrellas" value="5">
                                <label for="radio1">★</label>
                                <input id="radio2" type="radio" name="estrellas" value="4">
                                <label for="radio2">★</label>
                                <input id="radio3" type="radio" name="estrellas" value="3">
                                <label for="radio3">★</label>
                            </p>

                            <h4 class="text-center">Recomended</h4>

                        </div>

                    </div>

                    </div>-->

          </div>


        </div><br>

        <div class="form__message" id="form__message">
          <p>Please fill in the fields correctly</p>
        </div>

        <div class="form__group__btn-send" align="center" id="btn__success">

          <button type="submit" name="update" class="form__btn save" id="btn_">Save Changes</button>
        </div>

        <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>




      </form>
    </main>

  </div>


  <div id="house_much" style="display: none;">
    <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-10" id="contentp">

        </div>
      </div>
    </div>

  </div>
  <div id="house_ass" style="display: none;">
    <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
    <div class="content_popup">

      <div class="content">
        <div class="col-2">
          <img class="img-stu" src="../images/icon.png" />
        </div>
        <div class="col-9" id="assh">

        </div>
      </div>
    </div>

  </div>


  <?php include 'footer.php' ?>

  <script>
  function previewImage() {
    var file = document.getElementById("file").files;

    if (file.length > 0) {
      var fileReader = new FileReader();

      fileReader.onload = function(event) {
        document.getElementById("preview").setAttribute("src", event.target.result);
      };

      fileReader.readAsDataURL(file[0]);

      var label_1 = document.getElementById("label-i");
      var label_1_1 = document.getElementById("label-i-i");

      label_1.style.display = 'none';
      label_1_1.style.display = 'inline';

    }

  }
  </script>

  <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
  <script>
  mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
  console.log(mapboxgl.accessToken);
  var client = new MapboxClient(mapboxgl.accessToken);
  console.log(client);


  var address = "<?php echo "$row2[dir], $row2[city], $row2[state] $row2[p_code]"; ?>"
  var test = client.geocodeForward(address, function(err, data, res) {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties

    console.log(res);
    console.log(res.url);
    console.log(data);

    var coordinates = data.features[0].center;

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v10',
      center: coordinates,
      zoom: 14
    });
    new mapboxgl.Marker()
      .setLngLat(coordinates)
      .addTo(map);


  });
  </script>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>

</body>

</html>