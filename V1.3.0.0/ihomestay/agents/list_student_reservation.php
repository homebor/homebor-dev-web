<?php 

session_start();

$usuario = $_SESSION['username'];

include '../../xeon.php';

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d');

// TODO VALUES FROM $_POTS[]

$id_homestay = $_POST['id_home'];
$room = substr($_POST['room-bed'], 0, 6);
$bed = substr($_POST['room-bed'], 6);

if($room == 'Room 1') $color = '#232159';
else if($room == 'Room 2') $color = '#982A72';
else if($room == 'Room 3') $color = '#394893';
else if($room == 'Room 4') $color = '#A54483';
else if($room == 'Room 5') $color = '#5D418D';
else if($room == 'Room 6') $color = '#392B84';
else if($room == 'Room 7') $color = '#B15391';
else if($room == 'Room 8') $color = '#4F177D';


// TODO QUERIES

$coordinator = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario'");
$rcoord=$coordinator->fetch_assoc();

$homestayQuery = $link->query("SELECT mail_h FROM pe_home WHERE id_home = '$id_homestay'");
$row_homestayQuery = $homestayQuery->fetch_assoc();

$student = $link->query("SELECT * FROM pe_student WHERE id_m = '$rcoord[id_m]' AND ac_confirm = 'Yes' ORDER BY id_student DESC");

/* $eventsQueryBeds = $link->query("SELECT * FROM events WHERE email = '$row_homestayQuery[mail_h]' AND color = '$color' AND bed = '$bed' AND status = 'Active'");
$num_events = mysqli_num_rows($eventsQueryBeds); */

// TODO VALUES TO QUERIES

// ? HOMESTAY

$h_mail = $row_homestayQuery['mail_h'];

$dataList = null;


while ($listStudent = mysqli_fetch_array($student)){

  if($listStudent['status'] == 'Search for Homestay' || $listStudent['status'] == 'Student for Select House' || $listStudent['status'] == 'House for Accept Student'){

    $events = null;
    $dataEvents = null;

    $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$h_mail' AND color = '$color' AND status = 'Active' AND bed = '$bed' ORDER BY id DESC");
    $num_events = mysqli_num_rows($eventsQuery);

    if($num_events > 0){

      while ($eventList = mysqli_fetch_array($eventsQuery)){
        if(empty($eventList['start']) || $eventList['start'] == 'NULL') $startDateEvents = 'Empty';
        else $startDateEvents = $eventList['start'];
        
        if(empty($eventList['end']) || $eventList['end'] == 'NULL') $endDateEvents = 'Empty';
        else $endDateEvents = $eventList['end'];
        
        if(empty($eventList['color']) || $eventList['color'] == 'NULL') $colorEvents = 'Empty';
        else $colorEvents = $eventList['color'];
        
        if(empty($eventList['bed']) || $eventList['bed'] == 'NULL') $bedEvents = 'Empty';
        else $bedEvents = $eventList['bed'];

        $dataEvents[] = array(
          // ** EVENTS INFORMATION
          'startDateEvents' => $startDateEvents,
          'endDateEvents' => $endDateEvents,
          'colorEvents' => $colorEvents,
          'bedEvents' => $bedEvents,
          'response' => 'event',
        );
      }
    }else{
      $dataEvents = 'Empty';
    }

    /* $row_eventsQuery = $eventsQuery->fetch_assoc(); */

    if(empty($listStudent['photo_s']) || $listStudent['photo_s'] == 'NULL') 
      $photo_s = '../assets/emptys/profile-student-empty.png';
    else $photo_s = "https://homebor.com/" . $listStudent['photo_s'];

    $dataList[] = array(
      // ** STUDENT INFO
      'id_student' => $listStudent['id_student'],
      'fullnames' => $listStudent['name_s'] . ' ' . $listStudent['l_name_s'],
      'photo_s' => $photo_s,
      'firstd' => $listStudent['firstd'],
      'lastd' => $listStudent['lastd'],

      'allEvents' => $dataEvents,

      // ** ADDITIONAL
      'colorRoom' => $color,
      'room' => $room,
      'date' => $date,
      'bed' => $bed,
    );

  }
  
}
echo json_encode($dataList);


?>