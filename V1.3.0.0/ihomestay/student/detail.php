<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

if (isset($_GET['art_id'])) {
  $id = $_GET['art_id'];

  $homestayQuery = $link->query("SELECT id_home, h_name, phome, dir, city, state, p_code FROM pe_home WHERE id_home = '$id' ");
  $row_homestay = $homestayQuery->fetch_assoc();

  $query4 = $link->query("SELECT fp, pliving, parea1, parea2, pbath1 FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
  $row4 = $query4->fetch_assoc();

  $query91 = $link->query("SELECT dir_a, city_a, state_a, p_code_a, name_a, photo_a FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a");
  $row91 = $query91->fetch_assoc();
}

?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/style.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/header.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/modal.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/css/rooms-cards.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/datepicker/css/rome.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/datepicker/css/style.css?ver=1.0.22">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/owl.theme.default.min.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/quick_info.css?ver=1.0.22">


  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.22"></script>

  <!-- // TODO Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js?ver=1.0.22'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css?ver=1.0.22' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js?ver=1.0.22"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css?ver=1.0.22" rel="stylesheet" />
  <script
    src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js?ver=1.0.22">
  </script>
  <link rel="stylesheet" href="../assets/css/mapbox_directionsdetail.css?ver=1.0.22">

  <!-- // TODO Datepicker -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js?ver=1.0.22"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js?ver=1.0.22"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js?ver=1.0.22">
  </script>
  <link rel="stylesheet" type="text/css"
    href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css?ver=1.0.22" />


  <title>Homebor - Detail Homestay</title>

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
</head>

<!--// TODO HEADER -->
<?php require 'header.php'; ?>


<body>
  <!-- // TODO MAIN -->
  <main id="ts-main">
    <!-- // TODO INPUTS DE UTILIDAD -->
    <input type="hidden" value="<?php echo $_GET['art_id'] ?>" id="homestay-id">

    <input type="hidden" value="<?php echo $row_homestay['id_home']; ?>" id="id-home">
    <input type="hidden" value="<?php echo $row_homestay['h_name']; ?>" id="home-name">
    <input type="hidden" value="<?php echo $row_homestay['phome']; ?>" id="home-photo">

    <input type="hidden" value="<?php echo $row91['dir_a']; ?>" id="dir-a">
    <input type="hidden" value="<?php echo $row91['city_a']; ?>" id="city-a">
    <input type="hidden" value="<?php echo $row91['state_a']; ?>" id="state-a">
    <input type="hidden" value="<?php echo $row91['p_code_a']; ?>" id="postal-code-a">
    <input type="hidden" value="<?php echo $row91['name_a']; ?>" id="name-a">
    <input type="hidden" value="<?php echo $row91['photo_a']; ?>" id="photo-a">

    <input type="hidden" value="<?php echo $row_homestay['dir']; ?>" id="dir">
    <input type="hidden" value="<?php echo $row_homestay['city']; ?>" id="city">
    <input type="hidden" value="<?php echo $row_homestay['state']; ?>" id="state">
    <input type="hidden" value="<?php echo $row_homestay['p_code']; ?>" id="postal-code">

    <br><br><br><br>

    <!--// TODO PAGE TITLE -->
    <section id="page-title" class="col-12 col-lg-8 mx-auto mt-4">
      <h1 id="homestay-name" class="mb-0 display-4 text-center text-lg-left"></h1>
      <h5 id="homestay-address" class="text-center text-lg-left ts-opacity__90"></h5>
    </section>


    <!-- // TODO GALLERY CAROUSEL -->
    <section id="gallery-carousel">

      <div class="owl-carousel">
        <div>
          <figure>
            <?php
            if (file_exists("../../$row_homestay[phome]")) $phome = "https://homebor.com/$row_homestay[phome]";
            else $phome = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $phome ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row4[fp]")) $fp = "https://homebor.com/$row4[fp]";
            else $fp = "../assets/emptys/family-empty.png";
            ?>
            <img src="<?php echo $fp ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row4[pliving]")) $phome = "https://homebor.com/$row4[pliving]";
            else $phome = "../assets/emptys/living-room-empty.png";
            ?>
            <img src="<?php echo $phome ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row4[parea1]")) $parea1 = "https://homebor.com/$row4[parea1]";
            else $parea1 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $parea1 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row4[parea2]")) $parea2 = "https://homebor.com/$row4[parea2]";
            else $parea2 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $parea2 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row4[pbath1]")) $pbath1 = "https://homebor.com/$row4[pbath1]";
            else $pbath1 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $pbath1 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>

      </div>

    </section>

    <br>

    <!--// TODO CONTENT -->
    <section class="container-fluid col-12 col-lg-10 mx-auto">
      <!--// TODO DESCRIPTION -->
      <article id="description" class="col-12 col-lg-8 mx-auto my-5">
        <h3 class="text-center text-lg-left">Description of the House</h3>

        <div class="container d-flex align-items-center bg-white p-4 rounded" style="box-shadow: 2px 2px 5px #D6D6D6;">

          <p id="homestay-description" class="m-0 p-0 text-muted"></p>

        </div>
      </article>


      <article class="d-flex flex-wrap">
        <!--// TODO LEFT SIDE -->
        <div class="col-md-5 col-lg-4">


          <!--// TODO DETAILS -->
          <section>
            <h3 class="text-center text-lg-left">Owner Details</h3>
            <article class="px-4 py-3 bg-white rounded shadow">

              <dl class="ts-description-list__line mb-0">
                <dt>Email:</dt>
                <dd id="email-home"></dd>

                <dt>Names:</dt>
                <dd id="fullname-home"></dd>

                <dt>Date of Birth:</dt>
                <dd id="date-birth-home"></dd>

                <dt>Gender:</dt>
                <dd id="gender-home"></dd>

                <dt>Phone Number:</dt>
                <dd id="phone-number-home"></dd>

                <dt>Occupation:</dt>
                <dd id="occupation-home"></dd>

                <dt>Background Check:</dt>
                <dd id="background-check-home"></dd>
              </dl>

            </article>
          </section>

          <br class="d-lg-none">

          <!--// TODO CONTACT THE AGENT -->
          <section class="contact-the-agent">
            <h3 class="text-center text-lg-left">Contact to the Coordinator</h3>

            <div class="ts-box">

              <!--Agent Image & Phone-->
              <div class="ts-center__vertical mb-4">

                <!--Image-->
                <a href="#" id="agency-link" class="ts-circle p-5 mr-4 ts-shadow__sm"
                  data-bg-image="../assets/logos/7.png"></a>

                <!--Phone contact-->
                <div class="mb-0">
                  <h5 class="mb-0" id="agency-name">Homebor</h5>

                  <p class="mb-0 d-none" id="agent-name">
                    <i class="fa fa-user ts-opacity__50 mr-2"></i>
                  </p>

                  <p class="mb-0" id="agency-mail">
                    <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                  </p>

                  <p class="mb-0 d-none" id="agency-number">
                    <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                  </p>

                  <p class="mb-0 d-none" id="agency-type">
                    <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                  </p>

                </div>
              </div>

              <form id="form-agent" class="ts-form">

                <!--Name-->
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="">
                </div>

                <!--Email-->
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="">
                </div>

                <!--Message-->
                <div class="form-group">
                  <textarea class="form-control" id="form-contact-message" rows="3" name="message"
                    placeholder="Hi, I want to have more information about property "></textarea>
                </div>

                <!--Submit button-->
                <div class="form-group clearfix mb-0">
                  <button id="button" type="submit" class="btn send-message float-right" id="form-contact-submit">Send
                    a Message
                  </button>
                </div>

              </form>

            </div>
          </section>

          <br class="d-lg-none">

          <!--// TODO LOCATION -->
          <section id="location">
            <h3 class="text-center text-lg-left">Location</h3>

            <div class="ts-box">

              <dl class="ts-description-list__line mb-0">

                <dt><i class="fa fa-home  mr-2"></i>Address:</dt>
                <dd class="border-bottom pb-2" id="address-home"></dd>

                <dt><i class="fa fa-building  mr-2"></i>City:</dt>
                <dd class="border-bottom pb-2" id="city-home"></dd>

                <dt><i class="fa fa-map-marker  mr-2"></i>State:</dt>
                <dd class="border-bottom pb-2" id="state-home"></dd>

                <dt><i class="fa fa-map-marker  mr-2"></i>Postal Code:</dt>
                <dd id="postal-code-home"></dd>

              </dl>

            </div>

          </section>


          <!-- // TODO ACTIONS -->
          <section id="actions" class="d-none">
            <div class="d-flex justify-content-between">

              <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top" title="Print">
                <i class="fa fa-print"></i>
              </a>

            </div>
          </section>


        </div>


        <!--// TODO RIGHT SIDE -->
        <div class="col-md-7 col-lg-8">
          <br class="d-lg-none">

          <!-- // TODO QUICK INFO -->
          <section id="quick-info">
            <h3 class="text-center text-lg-left">Quick Info</h3>

            <!-- // * QUIT INFO CONTENT -->
            <article class="d-flex flex-column shadow rounded bg-white quick-info-container">
              <!-- // ** ROW 1 -->
              <article class="d-flex quick-info-1">
                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Bedrooms</h6>
                  <b id="room-home">1</b>
                  <img src="../assets/icon/habitacion.png" id="room-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Experience as a Homestay</h6>
                  <b id="y-experience-home">Empty</b>
                  <img src=" ../assets/img/certificacion.png" id="years-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Background Language</h6>
                  <b id="backl-home">French</b>
                  <img src="../assets/img/language.png" id="language-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Pets</h6>
                  <b id="pet-home">Yes</b>
                  <img src="../assets/img/mascotas.png" id="pets-icon">
                </div>
              </article>


              <!-- // ** ROW 2 -->
              <article class="d-flex quick-info-2">
                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Age Preference</h6>
                  <b id="a-preference-home">Adult</b>
                  <img src="../assets/icon/family.png" id="age-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Gender Preference</h6>
                  <b id="genpre-home">Female</b>
                  <img src="../assets/img/genero.png" id="gender-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Food Service</h6>
                  <b id="foodservice-home">Empty</b>
                  <img src="../assets/icon/food2 64.png" id="food-icon">
                </div>

                <div
                  class="pt-4 pb-5 py-lg-5 px-3 d-flex flex-column align-items-center justify-content-center col-sm-3 border">
                  <h6 class="text-center text-muted">Special Diet</h6>
                  <b id="diet-home">No</b>
                  <img src="../assets/icon/special-diet.png" id="diet-icon">
                </div>
              </article>
            </article>
          </section>


          <br><br>


          <!--// ! MAP =========================================================-->
          <section id="map-location" class="col-12 col-lg-auto">

            <h3 class="text-center text-lg-left">Map</h3>
            <div id='map' style="height: 240px; box-shadow: 2px 2px 5px #D6D6D6; border-radius: 4px;"></div>

          </section>
          <!--// ! MAP =========================================================-->


          <br>


          <!-- // TODO FLOOR PLANS -->
          <section id="floor-plans" class="rooms-container mt-5">
            <h3 class="w-100 text-center text-lg-left">Bedrooms Info</h3>

            <div id="all-reserves"
              class="container-fluid d-flex justify-content-center align-items-center mt-5 rooms-list-students">
              <!-- // TODO ALL ROOMS CONTAINER -->
            </div>

          </section>
        </div>
      </article>
    </section>

  </main>


  <MODALS>
    <!-- // TODO MODAL DYNAMIC -->
    <div id="modal-dynamic" class="modal-dynamic">

      <section class="modal-content">
        <span class="close">X</span>
        <h3 class="text-center text-lg-left">Title</h3>
      </section>

    </div>


    <!-- // TODO MODAL ZOOM -->
    <div id="modal-zoom-image" class="align-items-center justify-content-center">
      <span>X</span>
      <figure class="m-0 p-0 shadow rounded"></figure>
    </div>
  </MODALS>


  <TEMPLATES>
    <template id="reserve-template">

      <!-- // TODO ROOM CONTAINER -->
      <div id="reserve"
        class="d-flex flex-column flex-lg-row mx-auto py-4 px-3 rounded align-items-center room-card-container-students">

        <!-- // TODO ROOM CARD -->
        <div class="mx-3 pb-5 bg-white room-card">

          <!-- // TODO ROOM HEADER -->
          <div id="reserve-header" class="w-100 d-flex align-items-center justify-content-between room-title">
            <h3 class="p-0 m-0 text-white">Room 1</h3>
            <p class="m-0 p-0 text-white">CAD$ 14</p>
          </div>


          <!-- // TODO ROOM PHOTO -->
          <div id="reserve-photo" class="d-flex justify-content-center mx-auto room-photo">
            <img src="" id="single-image-room" class="d-none">
            <!-- // TODO CAROUSEL FOTOS -->
          </div>



          <!-- // TODO INFO ROOM -->
          <div class="d-flex mb-4">

            <!-- // TODO TYPE ROOM -->
            <div class="col-6 p-0">
              <div class="feature">
                <img src="../assets/icon/acomodacion 64.png" title="Type Accomodation">
                <span id="type-room" class="px-2">Type</span>
              </div>
            </div>


            <!-- // TODO ROOM FOOD SERVICE -->
            <div class="col-6 p-0">
              <div class="feature">
                <img src="../assets/icon/food 64.png" rel="noopener" title="Food Service">
                <span id="food-service" class="px-2">Food Service</span>
              </div>
            </div>

          </div>


          <!-- // TODO ROOM RESERVED -->
          <div id="homestay-found" class="m-0 p-0 py-2 reserved-message d-none">
            <p class="w-100 text-white font-weight-bold text-center m-0 p-0">
              Room Reserved
            </p>
          </div>

          <!-- // TODO ROOM REQUIRED MESSAGE -->
          <div id="reserve-required" class="m-0 p-0 py-2 required-message d-none">
            <p class="w-100 text-white font-weight-bold text-center m-0 p-0">
              Request Send to Homestay
            </p>
          </div>

          <!-- // TODO RESERVES LIMIT MESSAGE -->
          <div id="reserve-limit" class="m-0 p-0 py-2 limit-message d-none">
            <p class="w-100 text-white font-weight-bold text-center m-0 p-0">
              Your Reserves has Exceeded
            </p>
          </div>


          <!-- // TODO ROOM OCCUPIED MESSAGE -->
          <div id="reserve-occupied" class="m-0 p-0 occupied-message d-none">
            <p class="w-100 text-white font-weight-bold text-center m-0 p-0">
              Occupied Room up to <br>
              13 May 2021
            </p>
          </div>


          <!-- // TODO ROOM RESERVE BUTTON -->
          <div class="m-0 d-flex align-items-center justify-content-center btn-reserve">
            <button type="button" id="reserve-btn" class="btn px-3 text-light" disabled>
              Reserve Now
            </button>
          </div>

        </div>


        <!-- // TODO BEDS CONTAINER -->
        <div id="beds-container" class="d-flex mt-5 mt-lg-0 mx-lg-3 flex-column align-items-center room-beds-students">
          <!-- // TODO BEDS -->
          <h3 class="d-none d-lg-block font-weight-light text-muted mt-4">Select bed</h3>

          <!-- // TODO IMAGE ONLY ONE BED -->
          <img id="students-img" src="../assets/img/student_home.png" rel="noopener" class="d-none" alt="students">
        </div>

      </div>

    </template>
    <template id="bed-template">
      <!-- // TODO BED -->
      <label for="select-bed" id="bed"
        class="w-100 my-3 my-lg-3 rounded d-flex flex-column align-items-center bed-info-students">
        <div class="d-flex align-items-center justify-content-center">
          <img src="../assets/icon/cama 64.png" width="25px" height="25px" alt="">
          <span id="bed-type" class="py-4 px-2 text-dark">Type</span>
        </div>

        <div class="p-0 d-flex align-items-center justify-content-center">
          <span id="bed-letter" class="text-white">Bed</span>
        </div>

        <input id="select-bed" type="checkbox" hidden>
      </label>

    </template>
  </TEMPLATES>

  <!-- // TODO FOOTER -->
  <?php require 'footer.php'; ?>


  <!-- // TODO SCRIPTS -->
  <!-- <script src="assets/js/modal_service.js?ver=1.0.22"></script> -->
  <script src="../assets/js/popper.min.js?ver=1.0.22"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.22"></script>
  <script src="../assets/js/jquery.magnific-popup.min.js?ver=1.0.22"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.22"></script>
  <!-- <script src="../assets/js/custom.js?ver=1.0.22"></script> -->
  <script src="assets/js/owl.carousel.min.js?ver=1.0.22"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.22"></script>
  <script src="assets/js/galery.js?ver=1.0.22"></script>
  <script src="assets/js/datepicker.js?ver=1.0.22"></script>

  <!-- // ? MAP -->
  <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js?ver=1.0.22'></script>
  <script
    src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js?ver=1.0.22'>
  </script>
  <script src="assets/js/map.js?ver=1.0.22"></script>

  <script src="assets/js/detail.js?ver=1.0.22" type="module"></script>
</body>

</html>