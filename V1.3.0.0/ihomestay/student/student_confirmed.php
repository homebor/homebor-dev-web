<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

include 'count.php';

$queryUser = $link->query("SELECT usert, status FROM users WHERE mail = '$usuario' ");
$row_user = $queryUser->fetch_assoc();
if (strtolower($row_user['usert']) != 'student' || $row_user['status'] != "Activate") header("location: ../logout.php");


$queryStudent = $link->query("SELECT name_s, l_name_s, status FROM pe_student WHERE mail_s = '$usuario'");
$row_student = $queryStudent->fetch_assoc();
if ($row_student['status'] != "Homestay Found") header('location: index');


$queryEvents = $link->query("SELECT email FROM events WHERE mail_s = '$usuario' AND status = 'Active' ");
$row_event = $queryEvents->fetch_assoc();
if ($queryEvents != true || !$row_event) header("location: ../logout.php");

$queryHome = $link->query("SELECT id_home, phome FROM pe_home WHERE mail_h = '$row_event[email]' ");
$row_home = $queryHome->fetch_assoc();

$queryPhotoHome = $link->query("SELECT fp, pliving, parea1, parea2, pbath1 FROM photo_home WHERE id_home = '$row_home[id_home]' ");
$row_home_image = $queryPhotoHome->fetch_assoc();

?>

<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.6">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.6">
  <link rel="stylesheet" type="text/css" href="assets/css/student_confirmed.css?ver=1.0.6">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.6">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.6">
  <link rel="stylesheet" href="assets/css/notification.css?ver=1.0.6">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.6">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css?ver=1.0.22">
  <link rel="stylesheet" href="assets/css/owl.theme.default.min.css?ver=1.0.22">

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css?ver=1.0.6"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js?ver=1.0.6"
    integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
    crossorigin=""></script>

  <!-- // TODO Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
</head>

<!--// TODO HEADER -->
<?php require 'header.php'; ?>


<body>
  <!-- // TODO MAIN -->
  <main>
    <!-- // TODO MAP -->
    <section id="ts-hero" class="mb-0" style="margin-top: 5.8rem !important; height: 400px;">
      <div class="ts-full-screen ts-has-horizontal-results w-1001 d-flex1 flex-column1" style="height: 100px;">
        <div class="ts-map ts-shadow__sm">
          <?php include 'mapboxstudentconfirmed.php' ?>
        </div>
      </div>
    </section>


    <br><br><br><br><br>


    <!-- // TODO GALLERY CAROUSEL -->
    <section id="gallery-carousel">

      <div class="owl-carousel">
        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home[phome]")) $phome = "https://homebor.com/$row_home[phome]";
            else $phome = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $phome ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home_image[fp]")) $fp = "https://homebor.com/$row_home_image[fp]";
            else $fp = "../assets/emptys/family-empty.png";
            ?>
            <img src="<?php echo $fp ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home_image[pliving]")) $phome = "https://homebor.com/$row_home_image[pliving]";
            else $phome = "../assets/emptys/living-room-empty.png";
            ?>
            <img src="<?php echo $phome ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home_image[parea1]")) $parea1 = "https://homebor.com/$row_home_image[parea1]";
            else $parea1 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $parea1 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home_image[parea2]")) $parea2 = "https://homebor.com/$row_home_image[parea2]";
            else $parea2 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $parea2 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>


        <div>
          <figure>
            <?php
            if (file_exists("../../$row_home_image[pbath1]")) $pbath1 = "https://homebor.com/$row_home_image[pbath1]";
            else $pbath1 = "../assets/emptys/frontage-empty.png";
            ?>
            <img src="<?php echo $pbath1 ?>">
          </figure>
          <button class="d-flex justify-content-between align-items-center zoom-image"><i></i> Zoom</button>
        </div>

      </div>

    </section>


    <br><br><br><br>

    <!-- // TODO HOUSES CONTAINER -->
    <section class="container-fluid my-5">
      <!-- // * LOADING HOUSES -->
      <article id="loading-houses" class="d-flex flex-column align-items-center justify-content-center loading-houses">
        <h4>Loading houses...</h4>
        <svg width="50" height="50" viewBox="0 0 38 38"">
          <g fill=" none" fill-rule="evenodd">
          <g transform="translate(1 1)" stroke-width="2">
            <circle stroke-opacity=".5" cx="18" cy="18" r="18" />
            <path d="M36 18c0-9.94-8.06-18-18-18">
              <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s"
                repeatCount="indefinite" />
            </path>
          </g>
          </g>
        </svg>
      </article>

      <!-- // * HOUSES -->
      <article
        class="col-12 col-lg-12 col-xl-10 mx-auto d-flex flex-column flex-lg-row justify-content-center house-container">
        <!-- // ** HOUSE -->
        <a href="#" class="mx-auto mb-4 mb-lg-0 pb-2 pb-lg-0 d-flex flex-column bg-white card-house" data-id-home>
          <!-- // ** CARD HEADER-->
          <div class="p-0 d-flex flex-column align-items-center card-header" title="See profile">
            <img src="../assets/emptys/frontage-empty.png" class="mx-auto" data-house-image alt="house, homestay">
          </div>

          <!-- // ** CARD BODY CONTAINER-->
          <div class="w-100 py-2 mb-2 card-body">
            <!-- // *** CARD MAIN INFORMATION-->
            <div class="p-0 main-information">
              <!-- // **** HOUSE NAME  -->
              <h5 class="mb-3 border-bottom" data-house-name>House Name</h5>

              <!-- // **** ROW 2  -->
              <dl class="m-0 mb-3 d-flex flex-column align-items-center">
                <dt>Address</dt>
                <dd class="m-0 px-2 text-center" data-house-direction>99 Harbour Sq, Toronto Ontario</dd>
              </dl>


              <!-- // **** ROW 3  -->
              <div class="pb-0 mb-2 text-center d-flex align-items-center justify-content-around">
                <dl class="m-0">
                  <dt>Gender</dt>
                  <dd class="m-0" data-house-gender-preference>Female</dd>
                </dl>

                <dl class="m-0">
                  <dt>Age</dt>
                  <dd class="m-0" data-house-age-preference>Adult</dd>
                </dl>
              </div>


              <!-- // **** ROW 4  -->
              <div class="pb-0 text-center d-flex align-items-center justify-content-around">
                <dl class="m-0">
                  <dt>Bedrooms</dt>
                  <dd class="m-0" data-house-rooms>3</dd>
                </dl>

                <dl class="m-0">
                  <dt>Language</dt>
                  <dd class="m-0" data-house-background-language="">English</dd>
                </dl>
              </div>

            </div>
          </div>
        </a>


        <!-- // ** HOUSE INFORMATION -->
        <aside
          class="col-12 col-md-10 col-lg-7 col-xl-8 mx-auto pt-5 pb-4 d-flex flex-column align-items-center justify-content-around shadow rounded bg-white house-information">
          <div class="w-100 p-0 py-2 m-0 d-flex justify-content-between align-items-center border-bottom">
            <p id="house-email"></p>
            <p id="house-phone"></p>
            <p id="house-postal-code"></p>
            <p id="house-residence-type"></p>
          </div>

          <br><br>

          <div class="w-100 p-0 py-2 m-0 d-flex justify-content-between align-items-center border-bottom">
            <p id="house-family-members"></p>
            <p id="house-nationality"></p>
            <p id="house-smoke"></p>
          </div>

          <br><br>

          <div class="w-100 p-0 py-2 m-0 d-flex justify-content-between align-items-center border-bottom">
            <p id="house-food"></p>
            <p id="house-diet"></p>
            <p id="house-pets"></p>
          </div>

          <br><br>

          <p id="house-description" class="m-0 p-0"></p>
        </aside>

      </article>

      <article class="mt-5 d-flex justify-content-center">
        <a href="#" class="px-5 btn btn-lg btn-more-info">More Info</a>
      </article>
    </section>
  </main>

  <MODALS>
    <!-- // TODO MODAL DYNAMIC -->
    <div id="modal-dynamic" class="modal-dynamic">

      <section class="modal-content">
        <span class="close">X</span>
        <h3 class="text-center text-lg-left">Title</h3>
      </section>

    </div>


    <!-- // TODO MODAL ZOOM -->
    <div id="modal-zoom-image" class="align-items-center justify-content-center">
      <span>X</span>
      <figure class="m-0 p-0 shadow rounded"></figure>
    </div>
  </MODALS>

  <!-- // TODO FOOTER -->
  <?php require 'footer.php' ?>

  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.6"></script>
  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.6"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.6"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.6"></script>
  <script src="../assets/js/jquery.scrollbar.min.js?ver=1.0.6"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.6"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.6"></script>
  <script src="assets/js/owl.carousel.min.js?ver=1.0.22"></script>

  <script src="assets/js/student_confirmed.js?ver=1.0.6" type="module"></script>

</body>

</html>