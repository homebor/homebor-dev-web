<?php

session_start();
error_reporting(0);
include_once('../xeon.php');
// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail_h FROM pe_home WHERE mail_h = '$usuario'";



if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_home WHERE id_home = '$id' ");
$row=$query->fetch_assoc();

$query2 = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
$row2=$query2->fetch_assoc();

$query3 = $link->query("SELECT * FROM mem_f INNER JOIN pe_home ON pe_home.id_home = '$id' and mem_f.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();


$query4 = $link->query("SELECT * FROM photo_home INNER JOIN pe_home ON pe_home.id_home = '$id' and photo_home.id_home = pe_home.id_home");
$row4 = $query4->fetch_assoc();

$query5 = $link->query("SELECT * FROM manager INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and manager.id_m = pe_student.id_m");
$row5 = $query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM agents INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and agents.id_ag = pe_student.id_ag");
$row6 = $query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
$row7 = $query7->fetch_assoc();

$query8 = $link->query("SELECT * FROM notification WHERE user_i_mail = '$usuario'");
$row8 = $query8->fetch_assoc();

$query10 = $link->query("SELECT * FROM events WHERE mail_s = '$usuario' AND email = '$row[mail_h]'");
$row10 = $query10->fetch_assoc();

$query91 = $link->query("SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a");
$row91 = $query91->fetch_assoc();

$query9 = $link->query("SELECT * FROM academy INNER JOIN pe_home ON pe_home.id_home = '$id' and academy.id_ac = pe_home.a_pre");
$row9 = $query9->fetch_assoc();



}

$studentd = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
    $sd=$studentd->fetch_assoc();
if ($sd['usert'] != 'student') {
        
    header("location: ../logout.php");   
   
}
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/header.css">
  <link rel="stylesheet" href="assets/css/style-detail.css">
  <link rel="stylesheet" href="assets/css/modal-i.css">
  <link rel="stylesheet" href="assets/css/main.css">
  <link rel="stylesheet" href="assets/css/detail-rooms.css?ver=1.0">
  <link rel="stylesheet" type="text/css" href="assets/css/detail-acepted.css?ver=1.0">

  <!-- Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js">
  </script>
  <link rel="stylesheet" href="../assets/css/mapbox_directionsdetail.css">

  <title>Homebor - Detail Homestay</title>

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">




</head>


<body>


  <div class="ts-page-wrapper ts-homepage" id="page-top">
    <!--HEADER ===============================================================-->
    <?php
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->


    <main id="ts-main">


      <!--PAGE TITLE
            =========================================================================================================-->
      <section id="page-title">
        <div class="container">

          <div class="d-block d-sm-flex justify-content-between">

            <!--Title-->
            <div class="ts-title mb-0">
              <br>
              <br>
              <br>
              <br>
              <h1><?php echo $row['h_name'] ?></h1>
              <h5 class="ts-opacity__90">
                <i class="fa fa-map-marker text-primary"></i>
                <?php echo $row['dir'] ?>, <?php echo $row['state'] ?> <?php echo $row['city'] ?>,
                <?php echo $row['p_code'] ?>
              </h5>
            </div>

          </div>

        </div>
      </section>

      <!--GALLERY CAROUSEL
            =========================================================================================================-->
      <section id="gallery-carousel">

        <div class="owl-carousel ts-gallery-carousel ts-gallery-carousel__multi" data-owl-dots="1" data-owl-items="3"
          data-owl-center="1" data-owl-loop="1">



          <?php
                        
                        if($row['phome'] == 'NULL'){
                           
                       }
                       else {
                           echo'<!--Slide-->
   <div class="slide">
   <div class="ts-image" data-bg-image="../'.$row['phome'].'">
           <a href="../'.$row['phome'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
       </div>
       </div>';}
                       
                       ?>

          <?php
                        
                         if($row4['pliving'] == 'NULL'){
                            
                        }
                        else {
                            echo'<!--Slide-->
    <div class="slide">
    <div class="ts-image" data-bg-image="../'.$row4['pliving'].'">
            <a href="../'.$row4['pliving'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
        </div>
        </div>';}
                        
                        ?>



          <?php
                        
                         if($row4['parea1'] == 'NULL'){
                            
                        }
                        else {
                            echo'<!--Slide-->
    <div class="slide">
    <div class="ts-image" data-bg-image="../'.$row4['parea1'].'">
            <a href="../'.$row4['parea1'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
        </div>
         </div>';}
                        
                        ?>



          <?php
                        
                         if($row4['parea2'] == 'NULL'){
                   
                        }
                        else {
                            echo'<!--Slide-->
    <div class="slide"><div class="ts-image" data-bg-image="../'.$row4['parea2'].'">

            <a href="../'.$row4['parea2'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
        </div>
        </div>';}
                        
                        ?>



          <?php
                        
                         if($row4['pbath1'] == 'NULL'){
                            
                        }
                        else {
                            echo'<!--Slide-->
    <div class="slide"><div class="ts-image" data-bg-image="../'.$row4['pbath1'].'">

            <a href="../'.$row4['pbath1'].'" class="ts-zoom popup-image"><i class="fa fa-search-plus"></i>Zoom</a>
        </div>
        </div>';}
                        
                        ?>


        </div>

      </section>

      <!--CONTENT
            =========================================================================================================-->
      <section id="content">
        <div class="container">
          <div class="row flex-wrap-reverse">

            <!--LEFT SIDE
                        =============================================================================================-->
            <div class="col-md-5 col-lg-4">

              <!--DETAILS
                            =========================================================================================-->
              <section>
                <h3>Details</h3>
                <div class="ts-box">

                  <dl class="ts-description-list__line mb-0">

                    <dt>ID:</dt>
                    <dd>#<?php echo $row['id_home'] ?></dd>

                    <dt>Property:</dt>
                    <dd><?php echo $row['name_h'] ?> <?php echo $row['l_name_h'] ?></dd>

                    <dt>Gender:</dt>
                    <dd><?php echo $row['gender'] ?></dd>

                    <dt>Age:</dt>
                    <dd><?php $from = new DateTime($row['db']);
                                    $to   = new DateTime('today');
                                    $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
                                    $sufix= " years old";   
                                        echo "$age $sufix" ?></dd>

                    <dt>Category:</dt>
                    <dd>Homestay</dd>

                    <dt>Status:</dt>
                    <dd><?php echo $row['status'] ?></dd>

                    <dt>Number of Family Member:</dt>
                    <dd><?php echo $row['num_mem'] ?></dd>

                    <dt>Background:</dt>
                    <dd><?php echo $row['backg'] ?></dd>

                    <dt>Background Language:</dt>
                    <dd><?php echo $row['backl'] ?></dd>

                    <dt>Smoke Politics:</dt>
                    <dd><?php echo $row['smoke'] ?></dd>

                    <dt>Stars:</dt>
                    <dd>1</dd>

                    <dt>Email:</dt>
                    <dd><?php echo $row['mail_h'] ?></dd>

                  </dl>

                </div>
              </section>


              <?php
                        	if ($row3['f_name1'] == 'NULL' AND $row3['f_name2'] == 'NULL' AND $row3['f_name3'] == 'NULL' AND $row3['f_name4'] == 'NULL' AND $row3['f_name5'] == 'NULL' AND $row3['f_name6'] == 'NULL' AND $row3['f_name7'] == 'NULL' AND $row3['f_name8'] == 'NULL') {
                        	} else {
                        		echo '<section>
                            	<h3>Family Information</h3>
                            	<div class="ts-box">
                            	<dl class="ts-description-list__line mb-0">';

                            	//Family Members
                        		if ($row3['f_name1'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name1'].' '.$row3['f_lname1'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db1'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender1'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re1'].'</dd>';
                        		}

                        		if ($row3['f_name2'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name2'].' '.$row3['f_lname2'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db2'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender2'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re2'].'</dd>';
                        		}
                        		if ($row3['f_name3'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name3'].' '.$row3['f_lname3'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db3'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender3'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re3'].'</dd>';
                        		}
                        		if ($row3['f_name4'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name4'].' '.$row3['f_lname4'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db4'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender4'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re4'].'</dd>';
                        		}

                        		if ($row3['f_name5'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name5'].' '.$row3['f_lname5'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db5'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender5'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re5'].'</dd>';
                        		}
                        		if ($row3['f_name6'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name6'].' '.$row3['f_lname6'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db6'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender6'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re6'].'</dd>';
                        		}
                        		if ($row3['f_name7'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name7'].' '.$row3['f_lname7'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db7'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender7'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re7'].'</dd>';
                        		}
                        		if ($row3['f_name8'] == 'NULL') {
                        			
                        		} else{ 
                        				echo '<dt>Complete Name:</dt>
                                    <dd>'.$row3['f_name8'].' '.$row3['f_lname8'].'</dd>

                                    <dt>Date of Birth:</dt>
                                    <dd>'.$row3['db8'].'</dd>
                                    <dt>Gender:</dt>
                                    <dd>'.$row3['gender8'].'</dd>

                                    <dt>Relation:</dt>
                                    <dd class="border-bottom">'.$row3['re8'].'</dd>';
                        		}
                        		echo '

                            </div>
                        </section>';
                        	}

                        ?>


              <!--CONTACT THE AGENT
                            =========================================================================================-->
              <?php
                            if ($row7['id_m'] == '0' AND $row7['id_ag'] == '0') { echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="#" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../assets/logos/7.png"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">Homebor</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            sebi@homebor.com
                                        </p>
                                    </figure>
                                </div>
                                <form id="form-agent" class="ts-form">

                                     <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7['name_s'].' '.$row7['l_name_s'].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7['mail_s'].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row['id_home'].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>


                            </div>
                        </section>';
                                # code...
                            }
                            if ($row7['id_m'] == '0' AND $row7['id_ag'] != '0') { echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="agent_info.php?art_id='.$row6["id_ag"].'" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../'.$row6['photo'].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row6['agency'].'</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-user ts-opacity__50 mr-2"></i>
                                            '.$row6['name'].' '.$row6['l_name'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            '.$row6['a_mail'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                                            '.$row6['num'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                                            Agent
                                        </p>
                                    </figure>
                                </div>

                                <form id="form-agent" class="ts-form">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7['name_s'].' '.$row7['l_name_s'].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7['mail_s'].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row['id_home'].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </section>';
                                # code...
                            }
                             else { 
                                if ($row7['id_ag'] == '0' AND $row7['id_m'] != '0') {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a  href="agency_info.php?art_id='.$row5["id_m"].'" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../'.$row5['photo'].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row5['a_name'].'</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            '.$row5['mail'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                                            '.$row5['num'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                                            Manager
                                        </p>
                                    </figure>
                                </div>

                                <form id="form-agent" class="ts-form">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7['name_s'].' '.$row7['l_name_s'].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7['mail_s'].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row['id_home'].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </section>';
                                } elseif ($row7['id_ag'] != '0' AND $row7['id_m'] != '0') {
                                    echo '<section class="contact-the-agent">
                            <h3>Contact the Agent</h3>

                            <div class="ts-box">

                                <!--Agent Image & Phone-->
                                <div class="ts-center__vertical mb-4">

                                    <!--Image-->
                                    <a href="agent_info.php?art_id='.$row6["id_ag"].'" class="ts-circle p-5 mr-4 ts-shadow__sm" data-bg-image="../'.$row6['photo'].'"></a>

                                    <!--Phone contact-->
                                    <figure class="mb-0">
                                        <h5 class="mb-0">'.$row6['agency'].'</h5>
                                        <p class="mb-0">
                                            <i class="fa fa-user ts-opacity__50 mr-2"></i>
                                            '.$row6['name'].' '.$row6['l_name'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-envelope ts-opacity__50 mr-2"></i>
                                            '.$row6['a_mail'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-phone ts-opacity__50 mr-2"></i>
                                            '.$row6['num'].'
                                        </p>
                                        <p class="mb-0">
                                            <i class="fa fa-address-card ts-opacity__50 mr-2"></i>
                                            Agent
                                        </p>
                                    </figure>
                                </div>

                                <form id="form-agent" class="ts-form">

                                    <!--Name-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="'.$row7['name_s'].' '.$row7['l_name_s'].'">
                                    </div>

                                    <!--Email-->
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="'.$row7['mail_s'].'">
                                    </div>

                                    <!--Message-->
                                    <div class="form-group">
                                        <textarea class="form-control" id="form-contact-message" rows="3" name="message" placeholder="Hi, I want to have more information about property #'.$row['id_home'].'"></textarea>
                                    </div>

                                    <!--Submit button-->
                                    <div class="form-group clearfix mb-0">
                                        <button id="button" type="submit" class="btn btn-primary float-right" id="form-contact-submit">Send a Message
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </section>';

                                }
                                
                            }

                            ?>



              <!--LOCATION
                        =============================================================================================-->
              <section id="location">
                <h3>Location</h3>

                <div class="ts-box">

                  <dl class="ts-description-list__line mb-0">

                    <dt><i class="fa fa-home  ts-opacity__30 mr-2"></i>Address:</dt>
                    <dd class="border-bottom pb-2"><?php echo $row['dir'] ?></dd>

                    <dt><i class="fa fa-building ts-opacity__30 mr-2"></i>City:</dt>
                    <dd class="border-bottom pb-2"><?php echo $row['city'] ?></dd>

                    <dt><i class="fa fa-map-marker ts-opacity__30 mr-2"></i>State:</dt>
                    <dd class="border-bottom pb-2"><a href="#"><?php echo $row['state'] ?></a></dd>

                    <dt><i class="fa fa-envelope ts-opacity__30 mr-2"></i>Postal Code:</dt>
                    <dd class="border-bottom pb-2"><a href="#"><?php echo $row['p_code'] ?></a></dd>

                    <dt><i class="fa fa-phone ts-opacity__30 mr-2"></i>Phone:</dt>
                    <dd><a href="#"><?php echo $row['num'] ?></a></dd>

                  </dl>

                </div>

              </section>

              <!--ACTIONS
                        =============================================================================================-->
              <section id="actions">

                <div class="d-flex justify-content-between">


                  <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top"
                    title="Add to favorites">
                    <i class="far fa-star"></i>
                  </a>

                  <a class="btn btn-light mr-2 w-100" data-toggle="tooltip" data-placement="top" title="Print">
                    <i class="fa fa-print"></i>
                  </a>

                </div>
                
                <div class="d-flex justify-content-center">
                  <a href="../student/voucher/Welcomeyourhouse?art_id=<?php echo $usuario ?>"
                    class="btn btn__pdf btn__brochure-for-students" id="btn__canadian-direct" target="_blank">Download Brochure for
                    Students</a>
                </div>

              </section>


            </div>
            <!--end col-md-4-->

            <!--RIGHT SIDE
                        =============================================================================================-->
            <div class="col-md-7 col-lg-8">

              <!--QUICK INFO
                            =========================================================================================-->
              <section id="quick-info">
                <h3>Quick Info</h3>

                <!--Quick Info-->
                <div class="ts-quick-info ts-box">

                  <!--Row-->
                  <div class="row no-gutters">

                    <!--Bathrooms-->
                    <div class="col-sm-3">
                      <div class="ts-quick-info__item" data-bg-image="../assets/img/habitacion.png">
                        <h6>Bedrooms</h6>
                        <figure><?php echo $row['room'] ?></figure>
                      </div>
                    </div>

                    <!--Bedrooms-->
                    <div class="col-sm-3">
                      <div class="ts-quick-info__item" data-bg-image="../assets/img/certificacion.png">
                        <h6>Years as Homestay</h6>
                        <?php

                                            date_default_timezone_set("America/Toronto");
                                            $date = date('Y-m-d H:i:s');
                                            
                                            $firstDate = $row['y_service'];
                                            $secondDate = $date;

                                            $dateDifference = abs(strtotime($secondDate) - strtotime($firstDate));
                                            $years  = floor($dateDifference / (365 * 60 * 60 * 24));

                                            ?>
                        <figure><?php echo $years ?></figure>
                      </div>
                    </div>

                    <!--Area-->
                    <div class="col-sm-3">
                      <div class="ts-quick-info__item" data-bg-image="../assets/img/mascotas.png">
                        <h6>Pets</h6>
                        <figure><?php echo $row['pet'] ?></figure>
                      </div>
                    </div>

                    <!--Garages-->
                    <div class="col-sm-3">
                      <div class="ts-quick-info__item" data-bg-image="../assets/img/academy.png">
                        <h6>Academy Preference</h6>
                        <figure><?php echo $row9['acronyms'] ?></figure>
                      </div>
                    </div>

                  </div>
                  <!--end row-->

                </div>
                <!--end ts-quick-info-->

              </section><br>

              <?php if($row['des'] == 'NULL'){}else{ ?>

              <!--DESCRIPTION
                            =========================================================================================-->
              <section id="description">

                <h3>Description</h3>

                <div class="container" id="container">

                  <p>
                    <?php echo $row['des'] ?>
                  </p>

                </div>

              </section><br>

              <?php }if($row9['name_a'] == 'NULL' && $row['g_pre'] == 'NULL' && $row['ag_pre'] == 'NULL'){}else{ ?>

              <!--FEATURES
                            =========================================================================================-->
              <section id="features">

                <h3>Features</h3>

                <div class="container text-center"
                  style="background: white; padding: 1em 1.5em; padding-top: 2em; box-shadow: 2px 2px 5px #D6D6D6; border-radius: 4px;">

                  <div class="row">

                    <div class="col-sm-4 mb-4" style="color: #888">
                      <i class="fa fa-university"></i>
                      <?php echo $row9['name_a'] ?>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3 mb-4" style="color: #888">
                      <i class="fa fa-female mr-2"></i><i class="fa fa-male"></i>
                      <?php echo $row['g_pre'] ?>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3 mb-4" style="color: #888">
                      <i class="fa fa-child mr-2"></i><i class="fa fa-male"></i>
                      <?php echo $row['ag_pre'] ?>
                    </div>

                  </div>

                </div>

              </section><br>

              <?php } if($row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "yes" || $row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{ ?>

              <!--AMENITIES
                            =========================================================================================-->
              <section id="amenities">

                <h3>Special Diet</h3>

                <div class="container" id="diet">

                  <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                    <?php
                                            if($row['vegetarians'] == "yes"){
                                            echo "<li>Vegetarians</li>";
                                            }
                                        else {
                                        }
                                    ?>
                    <?php
                                            if($row['halal'] == "yes"){
                                            echo "<li>Halal</li>";
                                            }
                                        else {
                                        }
                                    ?>
                    <?php
                                            if($row['kosher'] == "yes"){
                                            echo "<li>Kosher (Jews)</li>";
                                            }
                                        else {
                                        }
                                    ?>
                    <?php
                                            if($row['lactose'] == "yes"){
                                            echo "<li>Lacotose Intolerant</li>";
                                            }
                                        else {
                                        }
                                    ?>
                    <?php
                                            if($row['gluten'] == "yes"){
                                            echo "<li>Gluten Free Diet</li>";
                                            }
                                        else {
                                        }
                                    ?>
                    <?php
                                            if($row['pork'] == "no"){
                                            
                                            }
                                        else {
                                            echo "<li>No Pork</li>";
                                        }
                                    ?>
                    <?php
                                            if($row['none'] == "yes"){
                                            echo "<li>None</li>";
                                            }
                                        else {
                                            
                                        }
                                    ?>
                  </ul>

                </div>

              </section><br>

              <?php } if($row['pet_num'] == "0" && $row['dog'] == "no" && $row['cat'] == "no" && $row['other'] == "no"){}else{ ?>

              <section id="amenities">

                <h3>Pets Information</h3>

                <div class="container" id="pet-info">

                  <ul class="ts-list-colored-bullets ts-text-color-light ts-column-count-3 ts-column-count-md-2">
                    <?php
                                            if($row['pet_num'] == "0"){
                                            
                                            }
                                        else {
                                            echo "<p><i class='fa fa-paw'></i> ".$row['pet_num']."</p>";
                                            if ($row['type_pet'] == 'NULL') {
                                                
                                            } else { echo "<p>".$row['type_pet']."</p>";}
                                        }
                                    ?>
                    <?php
                                            if($row['dog'] == "no"){
                                            
                                            }
                                        else {
                                            echo "<li>Dog</li>";
                                        }
                                    ?>
                    <?php
                                            if($row['cat'] == "no"){
                                            }
                                        else {
                                            echo "<li>Cat</li>";
                                        }
                                    ?>
                    <?php
                                            if($row['other'] == "no"){
                                            }
                                        else {
                                            echo "<li>Others</li>";
                                        }
                                    ?>
                  </ul>

                </div>

              </section>

              <?php } ?>

              <!--MAP
                            =========================================================================================-->
              <section id="map-location">

                <!--Map-->

                <!--Map-->
                <div id='map' style="height: 340px; box-shadow: 2px 2px 5px #D6D6D6; border-radius: 4px;"></div>
                <!--end col-md-6-->

                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                <script
                  src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'>
                </script>

                <script>
                mapboxgl.accessToken =
                  'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                console.log(mapboxgl.accessToken);
                var client = new MapboxClient(mapboxgl.accessToken);
                console.log(client);

                var address = '<?php echo "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]"; ?>"'
                var test = client.geocodeForward(address, function(err, data, res) {
                  // data is the geocoding result as parsed JSON
                  // res is the http response, including: status, headers and entity properties

                  console.log(res);
                  console.log(res.url);
                  console.log(data);

                  var coordinates = data.features[0].center;

                  //Este bloque define el centro de tu mapa todas las direcciones deben tenerla de la misma manera y zoom o no encontrara el centro

                  var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    center: coordinates,
                    zoom: 10

                  });

                  //Para marcar varios puntos debes copiar el bloque de busqueda como tenemos abajo y es muy importante pegarlo antes de cerrar el primer parentesis debajo del addto(map), ademas el bloque completo de new mapboxgl.marker debe estar en la ultima consulta es decir el ultimo bloque interno sino la consulta fallara, las variables se deben subir al numero inmediato superior


                  var address9 = '<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"'
                  var test = client.geocodeForward(address9, function(err, data, res) {
                    // data is the geocoding result as parsed JSON
                    // res is the http response, including: status, headers and entity properties

                    console.log(res);
                    console.log(res.url);
                    console.log(data);

                    var coordinates9 = data.features[0].center;

                    var map = new mapboxgl.Map({
                      container: 'map',
                      style: 'mapbox://styles/mapbox/streets-v10',
                      center: coordinates,
                      zoom: 10
                    });


                    // Cantidad de Coordenadas
                    var popup9 = new mapboxgl.Popup({
                      offset: 25,
                      closeOnClick: false
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail-acepted.php?art_id=<?php echo "$row[id_home]"?>"><?php echo "$row[h_name]"?></a>.<br><strong>Direction: </strong><a href="detail-acepted.php?art_id=<?php echo "$row[id_home]"?>"><?php echo "$row[dir]"?></a>.<br><a href="detail-acepted.php?art_id=<?php echo "$row[id_home]"?>"><img src="../<?php echo "$row[phome]"?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ei = document.createElement('div');
                    ei.id = 'house';
                    //lo superior es el popup

                    new mapboxgl.Marker(ei)
                      .setLngLat(coordinates9)
                      .setPopup(popup9)
                      .addTo(map);

                    var directions = new MapboxDirections({
                      accessToken: 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg',
                      unit: 'metric',
                    });

                    map.addControl(directions, 'top-right');

                    map.on('load', function() {
                      directions.setOrigin(
                        '<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"'
                      ); // can be address in form setOrigin("12, Elm Street, NY")
                      directions.setDestination(
                        '<?php echo "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]"; ?>"'
                      ); // can be address
                    })

                    //PRIMERA CONSULTA CON EL POPUP AGREGADO
                    var popup = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><?php echo "$row91[name]"?>.<br><strong>Direction: </strong><?php echo "$row91[dir]"?>.<br><img src="../<?php echo "$row91[photo]"?>" id="map-img">'
                    );
                    // create DOM element for the marker
                    var el = document.createElement('div');
                    el.id = 'marker';
                    //lo superior es el popup

                    new mapboxgl.Marker(el) // marker en el mapa
                      .setLngLat(coordinates) // marker
                      .setPopup(popup) // pertenece a la funcion del popup
                      .addTo(map); // marker


                  }); //Por la cantidad de consultas que pongas

                });
                </script>

              </section><br>


              <!--FLOOR PLANS
                            =========================================================================================-->
              <section id="floor-plans">

                <h3> Room reserved for you </h3>

                <?php 

                $usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
                $u=$usu->fetch_assoc();

            ?>

                <div class="c-log">

                  <?php 
                    $queryEvents = $link->query("SELECT * FROM events WHERE mail_s = '$usuario' AND email = '$row[mail_h]' AND status = 'Active'");
                    $rc1 = mysqli_num_rows($queryEvents);

                    while ($rc1 = mysqli_fetch_array($queryEvents)) {

                        $rooms = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.id_home = '$id' and room.id_home = pe_home.id_home");
                        $ro=$rooms->fetch_assoc();

                        if ($rc1['room_e'] == 'room1') {
                                        
                            $type = $ro['type1'];

                            $bed = $ro['bed1'];

                            $date = $ro['date1'];

                            $food = $ro['food1'];

                            $proom = $row4['proom1'];

                            $proom_2 = $row4['proom1_2'];

                            $proom_3 = $row4['proom1_3'];

                            $room = '1';
                        }

                        if ($rc1['room_e'] == 'room2') {
                                        
                            $type = $ro['type2'];

                            $bed = $ro['bed2'];

                            $date = $ro['date2'];

                            $food = $ro['food2'];

                            $proom = $row4['proom2'];

                            $proom_2 = $row4['proom2_2'];

                            $proom_3 = $row4['proom2_3'];

                            $room = '2';
                        }

                        if ($rc1['room_e'] == 'room3') {
                                        
                            $type = $ro['type3'];

                            $bed = $ro['bed3'];

                            $date = $ro['date3'];

                            $food = $ro['food3'];

                            $proom = $row4['proom3'];

                            $proom_2 = $row4['proom3_2'];

                            $proom_3 = $row4['proom3_3'];
                            
                            $room = '3';
                        }

                        if ($rc1['room_e'] == 'room4') {
                                        
                            $type = $ro['type4'];

                            $bed = $ro['bed4'];

                            $date = $ro['date4'];

                            $food = $ro['food4'];

                            $proom = $row4['proom4'];

                            $proom_2 = $row4['proom4_2'];

                            $proom_3 = $row4['proom4_3'];

                            $room = '4';
                        }

                        if ($rc1['room_e'] == 'room5') {
                                        
                            $type = $ro['type5'];

                            $bed = $ro['bed5'];

                            $date = $ro['date5'];

                            $food = $ro['food5'];

                            $proom = $row4['proom5'];

                            $proom_2 = $row4['proom5_2'];

                            $proom_3 = $row4['proom5_3'];

                            $room = '5';
                        }

                        if ($rc1['room_e'] == 'room6') {
                                        
                            $type = $ro['type6'];

                            $bed = $ro['bed6'];

                            $date = $ro['date6'];

                            $food = $ro['food6'];

                            $proom = $row4['proom6'];

                            $proom_2 = $row4['proom6_2'];

                            $proom_3 = $row4['proom6_3'];

                            $room = '6';
                        }

                        if ($rc1['room_e'] == 'room7') {
                                        
                            $type = $ro['type7'];

                            $bed = $ro['bed7'];

                            $date = $ro['date7'];

                            $food = $ro['food7'];

                            $proom = $row4['proom7'];

                            $proom_2 = $row4['proom7_2'];

                            $proom_3 = $row4['proom7_3'];

                            $room = '7';
                        }

                        if ($rc1['room_e'] == 'room8') {
                                        
                            $type = $ro['type8'];

                            $bed = $ro['bed8'];

                            $date = $ro['date8'];

                            $food = $ro['food8'];

                            $proom = $row4['proom8'];

                            $proom_2 = $row4['proom8_2'];

                            $proom_3 = $row4['proom8_3'];

                            $room = '8';
                        }

                    ?>

                  <div class="wrap-login100">

                    <h3 class="title-room"> Room <?php echo $room; ?></h3>

                    <div class="div-img-r" align="center">

                      <?php 
                                if (empty($proom_2)) {
                            ?>

                      <img src="<?php echo '../'.$proom.''?>" alt="Room1" class="img-room">

                      <?php 
                                }else{
                            ?>

                      <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                          <?php if ($proom == 'NULL' OR $proom == '') {}else{ ?>

                          <li data-target="#carouselExampleIndicators-1" data-slide-to="0" class="active"></li>

                          <?php } if ($proom_2 == 'NULL' OR $proom_2 == '') {}else{?>

                          <li data-target="#carouselExampleIndicators-1" data-slide-to="1"></li>

                          <?php } if ($proom_3 == 'NULL' OR $proom_3 == '') {}else{ ?>

                          <li data-target="#carouselExampleIndicators-1" data-slide-to="2"></li> <?php } ?>
                        </ol>

                        <div class="carousel-inner">

                          <?php if ($proom == 'NULL' OR $proom == '') {}else{ ?>

                          <div class="carousel-item active">
                            <img src="<?php echo '../'.$row4['proom1'].''?>" alt="Photo Room 1" class="img-room">
                          </div>


                          <?php } if ($proom_2 == 'NULL' OR $proom_2 == '') {}else{?>
                          <div class="carousel-item">
                            <img src="<?php echo '../'.$row4['proom1_2'].''?>" alt="Photo Room 1" class="img-room">
                          </div>

                          <?php } if ($proom_3 == 'NULL' OR $proom_3 == '') {}else{ ?>
                          <div class="carousel-item">
                            <img src="<?php echo '../'.$row4['proom1_3'].''?>" alt="Photo Room 1" class="img-room">
                          </div>
                          <?php } ?>

                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button"
                          data-slide="prev">

                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                          <span class="sr-only">Previous</span>

                        </a>

                        <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button"
                          data-slide="next">

                          <span class="carousel-control-next-icon" aria-hidden="true"></span>

                          <span class="sr-only">Next</span>

                        </a>

                      </div>

                      <?php } ?>


                    </div><br>

                    <div class="container">

                      <div class="row" id="container2">

                        <div class="col-lg-1">

                          <img class="type-img" src="assets/images/acomodacion 64.png" alt="" title="Type Accomodation">

                        </div>


                        <div class="col-2">

                          <p> <?php echo $type ?> </p>

                        </div>

                        <div class="col-2"></div>

                        <div class="col-lg-1">

                          <img class="type-img" src="assets/images/cama 64.png" alt="" title="Type Accomodation">

                        </div>


                        <div class="col-2">

                          <p> <?php echo $bed ?> </p>

                        </div>

                      </div><br>


                      <div class="row" id="container2">
                        <div class="col-lg-1">

                          <img class="type-img" src="assets/images/availability.png" alt="" title="Type Accomodation">

                        </div>


                        <div class="col-2">

                          <p> <?php echo $date ?> </p>

                        </div>

                        <div class="col-2"></div>

                        <div class="col-lg-1">

                          <img class="type-img" src="assets/images/food 64.png" alt="" title="Type Accomodation">

                        </div>


                        <div class="col-2">

                          <p> <?php echo $food ?> </p>

                        </div>


                      </div>

                    </div><br>

                    <form method="POST" action="edit_date.php">

                      <div class="row" id="container3">

                        <div class="container" id="container4">

                          <label id="container4"> Change Departure Date </label>

                          <div id="flex">


                            <input type="date" class="form-control change" name="date_e">

                            <input type="hidden" name="mail_h" value="<?php echo $row['mail_h']; ?>">

                            <input type="hidden" name="bedroom" value="<?php echo $row8['bedrooms']; ?>">

                            <input type="hidden" name="title" value="<?php echo $row10['title']; ?>">

                            <input type="hidden" name="color" value="<?php echo $row10['color']; ?>">

                            <input type="hidden" name="start" value="<?php echo $row10['start']; ?>">

                            <input type="hidden" name="end" value="<?php echo $row10['end']; ?>">

                          </div><br>

                          <button class="btn-change" type="submit"> Change </button>


                        </div>

                      </div>
                    </form><br>

                  </div>

                </div>

                <?php
                }
            ?>

              </section><br>


            </div>
            <!--end col-md-8-->

          </div>
          <!--end row-->
        </div>
        <!--end container-->
      </section>


    </main>
    <!--end #ts-main-->

    <!--FOOTER ===============================================================-->
    <?php 
    include 'footer.php';
?>


  </div>
  <!--end page-->

  <script>
  const modalService = () => {
    const d = document;
    const body = d.querySelector('body');
    const buttons = d.querySelectorAll('[data-modal-trigger]');

    // attach click event to all modal triggers
    for (let button of buttons) {
      triggerEvent(button);
    }

    function triggerEvent(button) {
      button.addEventListener('click', () => {
        const trigger = button.getAttribute('data-modal-trigger');
        const modal = d.querySelector(`[data-modal=${trigger}]`);
        const modalBody = modal.querySelector('.modal-body');
        const closeBtn = modal.querySelector('.cancel');

        closeBtn.addEventListener('click', () => modal.classList.remove('is-open'))
        modal.addEventListener('click', () => modal.classList.remove('is-open'))

        modalBody.addEventListener('click', (e) => e.stopPropagation());

        modal.classList.toggle('is-open');

        // Close modal when hitting escape
        body.addEventListener('keydown', (e) => {
          if (e.keyCode === 27) {
            modal.classList.remove('is-open')
          }
        });
      });
    }
  }

  modalService();
  </script>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/owl.carousel.min.js"></script>
  <script src="../assets/js/jquery.magnific-popup.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>

</body>

</html>