<?php
$from = new DateTime($row_student['db_s']);
$to   = new DateTime('today');
$age = $from->diff($to)->y; //Devuelve la diferencia entre los DateTime


if ($age < 21) $age2 = 'Teenager';
else $age2 = 'Adult';


$search = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house1";
$found = $link->query($search);

$find = $found->fetch_assoc();

$search2 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house2";
$found2 = $link->query($search2);

$find2 = $found2->fetch_assoc();

$search3 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house3";
$found3 = $link->query($search3);

$find3 = $found3->fetch_assoc();

$search4 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house4";
$found4 = $link->query($search4);

$find4 = $found4->fetch_assoc();

$search5 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house5";
$found5 = $link->query($search5);

$find5 = $found5->fetch_assoc();

$search6 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house6";
$found6 = $link->query($search6);

$find6 = $found6->fetch_assoc();

$search7 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house7";
$found7 = $link->query($search7);

$find7 = $found7->fetch_assoc();

$search8 = "SELECT * FROM pe_home INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and pe_home.id_home = pe_student.house8";
$found8 = $link->query($search8);

$find8 = $found8->fetch_assoc();

$query91 = "SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$usuario' and academy.id_ac = pe_student.n_a";
$resultado91 = $link->query($query91);

$row91 = $resultado91->fetch_assoc();

$directionA = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";

//Direction 1
//Homebor's Student Direction Case
if ($row_student['house1'] != 'NULL') {
  $direction1 = "$find[dir], $find[city], $find[state] $find[p_code]";
} else {
  $direction1 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}


//Direction 2
//Homebor's Student Direction Case
if ($row_student['house2'] != 'NULL') {
  $direction2 = "$find2[dir], $find2[city], $find2[state] $find2[p_code]";
} else {
  $direction2 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}


//Direction 3
//Homebor's Student Direction Case
if ($row_student['house3'] != 'NULL') {
  $direction3 = "$find3[dir], $find3[city], $find3[state] $find3[p_code]";
} else {
  $direction3 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}

//Direction 4
//Homebor's Student Direction Case
if ($row_student['house4'] != 'NULL') {
  $direction4 = "$find4[dir], $find4[city], $find4[state] $find4[p_code]";
} else {
  $direction4 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}


//Direction 5
//Homebor's Student Direction Case
if ($row_student['house5'] != 'NULL') {
  $direction5 = "$find5[dir], $find5[city], $find5[state] $find5[p_code]";
} else {
  $direction5 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}

//Direction 6
//Homebor's Student Direction Case
if ($row_student['house6'] != 'NULL') {
  $direction6 = "$find6[dir], $find6[city], $find6[state] $find6[p_code]";
} else {
  $direction6 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}

//Direction 7
//Homebor's Student Direction Case
if ($row_student['house7'] != 'NULL') {
  $direction7 = "$find7[dir], $find7[city], $find7[state] $find7[p_code]";
} else {
  $direction7 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}

//Direction 8
//Homebor's Student Direction Case
if ($row_student['house8'] != 'NULL') {
  $direction8 = "$find8[dir], $find8[city], $find8[state] $find8[p_code]";
} else {
  $direction8 = "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]";
}



?>


<head>
  <link rel="stylesheet" type="text/css" href="assets/css/mapboxstudent.css">

  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
  <script src="../assets/js/mapbox_directions.js"></script>
  <link rel="stylesheet" href="../assets/css/mapbox_directions.css">

</head>

<div id='map'></div>
<script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
<script>
  mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
  var client = new MapboxClient(mapboxgl.accessToken);

  var address = '<?php echo $directionA ?>"'
  var test = client.geocodeForward(address, function(err, data, res) {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties


    var coordinates = data.features[0].center;

    //Este bloque define el centro de tu mapa todas las direcciones deben tenerla de la misma manera y zoom o no encontrara el centro

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v10',
      center: coordinates,
      zoom: 10

    });

    //Para marcar varios puntos debes copiar el bloque de busqueda como tenemos abajo y es muy importante pegarlo antes de cerrar el primer parentesis debajo del addto(map), ademas el bloque completo de new mapboxgl.marker debe estar en la ultima consulta es decir el ultimo bloque interno sino la consulta fallara, las variables se deben subir al numero inmediato superior

    var address2 = '<?php echo $direction1 ?>'
    var test = client.geocodeForward(address2, function(err, data, res) {
      // data is the geocoding result as parsed JSON
      // res is the http response, including: status, headers and entity properties


      var coordinates2 = data.features[0].center;

      var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        center: coordinates,
        zoom: 10
      });
      var address3 = '<?php echo $direction2 ?>'
      var test = client.geocodeForward(address3, function(err, data, res) {
        // data is the geocoding result as parsed JSON
        // res is the http response, including: status, headers and entity properties


        var coordinates3 = data.features[0].center;

        var map = new mapboxgl.Map({
          container: 'map',
          style: 'mapbox://styles/mapbox/streets-v10',
          center: coordinates,
          zoom: 10
        });

        var address4 = '<?php echo $direction3 ?>'
        var test = client.geocodeForward(address4, function(err, data, res) {
          // data is the geocoding result as parsed JSON
          // res is the http response, including: status, headers and entity properties


          var coordinates4 = data.features[0].center;

          var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            center: coordinates,
            zoom: 10
          });

          var address5 = '<?php echo $direction4 ?>'
          var test = client.geocodeForward(address5, function(err, data, res) {
            // data is the geocoding result as parsed JSON
            // res is the http response, including: status, headers and entity properties


            var coordinates5 = data.features[0].center;

            var map = new mapboxgl.Map({
              container: 'map',
              style: 'mapbox://styles/mapbox/streets-v10',
              center: coordinates,
              zoom: 10
            });

            var address6 = '<?php echo $direction5 ?>'
            var test = client.geocodeForward(address6, function(err, data, res) {
              // data is the geocoding result as parsed JSON
              // res is the http response, including: status, headers and entity properties


              var coordinates6 = data.features[0].center;

              var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v10',
                center: coordinates,
                zoom: 10
              });

              var address7 = '<?php echo $direction6 ?>'
              var test = client.geocodeForward(address7, function(err, data, res) {
                // data is the geocoding result as parsed JSON
                // res is the http response, including: status, headers and entity properties


                var coordinates7 = data.features[0].center;

                var map = new mapboxgl.Map({
                  container: 'map',
                  style: 'mapbox://styles/mapbox/streets-v10',
                  center: coordinates,
                  zoom: 10
                });

                var address8 = '<?php echo $direction7 ?>'
                var test = client.geocodeForward(address8, function(err, data, res) {
                  // data is the geocoding result as parsed JSON
                  // res is the http response, including: status, headers and entity properties


                  var coordinates8 = data.features[0].center;

                  var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v10',
                    center: coordinates,
                    zoom: 10
                  });

                  var address9 = '<?php echo $direction8 ?>'
                  var test = client.geocodeForward(address9, function(err, data, res) {
                    // data is the geocoding result as parsed JSON
                    // res is the http response, including: status, headers and entity properties


                    var coordinates9 = data.features[0].center;

                    var map = new mapboxgl.Map({
                      container: 'map',
                      style: 'mapbox://styles/mapbox/streets-v10',
                      center: coordinates,
                      zoom: 10
                    });

                    var popup2 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find[id_home]" ?>"><?php echo "$find[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find[id_home]" ?>"><?php echo "$find[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find[id_home]" ?>"><img src="../../<?php echo "$find[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ea = document.createElement('div');
                    ea.id = 'house';

                    new mapboxgl.Marker(ea)
                      .setLngLat(coordinates2)
                      .setPopup(popup2)
                      .addTo(map);

                    // Cantidad de Coordenadas
                    var popup9 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find8[id_home]" ?>"><?php echo "$find8[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find8[id_home]" ?>"><?php echo "$find8[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find8[id_home]" ?>"><img src="../../<?php echo "$find8[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var eh = document.createElement('div');
                    eh.id = 'house';

                    new mapboxgl.Marker(eh)
                      .setLngLat(coordinates9)
                      .setPopup(popup9)
                      .addTo(map);

                    var directions = new MapboxDirections({
                      accessToken: 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg',
                      unit: 'metric',
                    });

                    map.addControl(directions, 'top-right');

                    map.on('load', function() {
                      directions.setDestination(
                        '<?php echo "$row91[dir_a], $row91[city_a], $row91[state_a] $row91[p_code_a]"; ?>"'
                      ); // can be address
                    })





                    var popup8 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find7[id_home]" ?>"><?php echo "$find7[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find7[id_home]" ?>"><?php echo "$find7[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find7[id_home]" ?>"><img src="../../<?php echo "$find7[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var eg = document.createElement('div');
                    eg.id = 'house';

                    new mapboxgl.Marker(eg)
                      .setLngLat(coordinates8)
                      .setPopup(popup8)
                      .addTo(map);


                    var popup7 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find6[id_home]" ?>"><?php echo "$find6[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find6[id_home]" ?>"><?php echo "$find6[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find6[id_home]" ?>"><img src="../../<?php echo "$find6[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ef = document.createElement('div');
                    ef.id = 'house';

                    new mapboxgl.Marker(ef)
                      .setLngLat(coordinates7)
                      .setPopup(popup7)
                      .addTo(map);



                    var popup6 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find5[id_home]" ?>"><?php echo "$find5[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find5[id_home]" ?>"><?php echo "$find5[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find5[id_home]" ?>"><img src="../../<?php echo "$find5[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ee = document.createElement('div');
                    ee.id = 'house';

                    new mapboxgl.Marker(ee)
                      .setLngLat(coordinates6)
                      .setPopup(popup6)
                      .addTo(map);


                    var popup5 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find4[id_home]" ?>"><?php echo "$find4[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find4[id_home]" ?>"><?php echo "$find4[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find4[id_home]" ?>"><img src="../../<?php echo "$find4[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ed = document.createElement('div');
                    ed.id = 'house';

                    new mapboxgl.Marker(ed)
                      .setLngLat(coordinates5)
                      .setPopup(popup5)
                      .addTo(map);

                    var popup4 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find3[id_home]" ?>"><?php echo "$find3[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find3[id_home]" ?>"><?php echo "$find3[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find3[id_home]" ?>"><img src="../../<?php echo "$find3[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var ec = document.createElement('div');
                    ec.id = 'house';

                    new mapboxgl.Marker(ec)
                      .setLngLat(coordinates4)
                      .setPopup(popup4)
                      .addTo(map);


                    var popup3 = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><a href="detail.php?art_id=<?php echo "$find2[id_home]" ?>"><?php echo "$find2[h_name]" ?></a>.<br><strong>Direction: </strong><a href="detail.php?art_id=<?php echo "$find2[id_home]" ?>"><?php echo "$find2[dir]" ?></a>.<br><a href="detail.php?art_id=<?php echo "$find2[id_home]" ?>"><img src="../../<?php echo "$find2[phome]" ?>" id="map-img"></a>',
                    );
                    // create DOM element for the marker
                    var eb = document.createElement('div');
                    eb.id = 'house';

                    new mapboxgl.Marker(eb)
                      .setLngLat(coordinates3)
                      .setPopup(popup3)
                      .addTo(map);











                    //PRIMERA CONSULTA CON EL POPUP AGREGADO
                    var popup = new mapboxgl.Popup({
                      offset: 25
                    }).setHTML(
                      '<strong>Name: </strong><?php echo "$row91[name_a]" ?>.<br><strong>Direction: </strong><?php echo "$row91[dir_a]" ?>.<br><img src="../../<?php echo "$row91[photo_a]" ?>" id="map-img">',
                    );
                    // create DOM element for the marker
                    var el = document.createElement('div');
                    el.id = 'marker';
                    //lo superior es el popup
                    new mapboxgl.Marker(el) // marker en el mapa
                      .setLngLat(coordinates) // marker
                      .setPopup(popup) // pertenece a la funcion del popup
                      .addTo(map); // marker


                  }); //Por la cantidad de consultas que pongas

                });

              });

            });

          });

        });

      });

    });

  });
</script>