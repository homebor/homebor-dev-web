<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query = $link->query("SELECT * FROM pe_student WHERE id_student = '$id' ");
$row=$query->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.id_student = '$id' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();


$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM academy WHERE id_ac != '0'");
$row6=$query6->fetch_assoc();

$query1 = $link->query("SELECT * FROM pe_student INNER JOIN academy ON pe_student.id_student = '$id' AND pe_student.n_a = academy.id_ac");
$row1=$query1->fetch_assoc();



    if ($row5['usert'] == 'Admin') {
        $way = '../master/index.php';
    } elseif ($row5['usert'] == 'Cordinator') {
       $way = '../master/cordinator.php';
        echo '<style type="text/css"> a#admin{display:none;} </style>';
    }
}


if ($row5['usert'] != 'Admin') {
    if ($row5['usert'] != 'Cordinator') {
     header("location: ../logout.php");   
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_delete.css">
    <link rel="stylesheet" type="text/css" href="assets/css/student_edit.css">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Students Advance Options</title>

</head>
<body>


<header id="ts-header" class="fixed-top" style="background: white;">
        <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
       <?php include '../master/header.php'; ?>

    </header>
    <!--end Header-->


<div class="container_register">

    <main class="card__register">
        <form class="ts-form" action="editstudent.php" method="post" autocomplete="off" enctype="multipart/form-data" >

            <div class="row m-0 p-0">

                <div class="col-md-4 column-1">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Basic Information</h3>

                        <div class="information__content">

                            <div class="form__group__div__photo">

                                <?php if(empty($row['photo_s']) || $row['photo_s'] == 'NULL'){ ?>

                                    <img class="form__group__photo" id="preview" src="" alt="">

                                    <label for="file" class="photo-add" id="label-i"> <p class="form__group-l_title"> Add Profile Photo </p> </label>

                                    <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                                <?php }else{ ?>
                                
                                    <img class="form__group__photo" id="preview" src="../<?php echo $row['photo_s'] ?>" alt="">

                                    <input type="file" accept="image/*" onchange="previewImage();" style="display:none" name="profile_s" id="file" maxlength="1" accept="jpg|png|jpeg|pdf">

                                <?php } ?>


                                <?php if(empty($row['photo_s']) || $row['photo_s'] == 'NULL'){ ?>

                                    <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display: none;" title="Change Frontage Photo"></label>

                                <?php }else{ ?>

                                    <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" title="Change Frontage Photo"></label>

                                <?php } ?>

                                
                            </div>


                            <?php
                                echo '<input type="text" class="form-control" id="mail_s" name="mail_s"  value='.$row['mail_s'].' hidden>';
                            ?>
                            

                            

                                <!-- Group Name -->

                                <div class="form__group form__group__name" id="group__name">
                                    <div class="form__group__icon-input">
                                        <label for="name" class="form__label">First Name</label>
                                        
                                        <label for="name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="name" name="name" class="form__group-input names" placeholder="First Name" value="<?php echo "$row[name_s]"?>">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                    
                                </div>
                                
                                <!-- Group Last Name -->

                                <div class="form__group form__group__l_name" id="group__l_name">
                                    <div class="form__group__icon-input">

                                        <label for="l_name" class="form__label">Last Name</label>
                                        <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                                        <input type="text" id="l_name" name="l_name" class="form__group-input names" placeholder="Last Name" value="<?php echo "$row[l_name_s]"?>">

                                    </div>
                                    <p class="form__group__input-error">Without special characters or numbers</p>
                                </div>
                            
                            <!-- Group Number -->

                            <div class="form__group form__group__num_s" id="group__num_s">

                                <div class="form__group__icon-input">

                                    <label for="num_s" class="form__label">Phone Number</label>

                                    <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>

                                    <?php if(empty($row['num_s']) || $row['num_s'] == 'NULL'){  ?>

                                        <input type="num" id="num_s" name="num_s" class="form__group-input num_s" placeholder="Phone Number">
                                        
                                    <?php }else{ ?>
                                            
                                        <input type="num" id="num_s" name="num_s" class="form__group-input num_s" placeholder="Phone Number" value="<?php echo $row['num_s']; ?>">
                                    <?php } ?>

                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>
                        
                        </div>

                    </div>


                    <div class="card">


                        <h3 class="form__group-title__col-4">Flight Information</h3>

                        <div class="information__content">
                        
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_airline" id="group__n_airline">
                                <div class="form__group__icon-input">

                                    <label for="n_airline" class="form__label">Booking Confirmation</label>

                                    <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                                    <?php if(empty($row['n_airline']) || $row['n_airline'] == 'NULL'){  ?>

                                        <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline" placeholder="Booking Confirmation">

                                    <?php }else{ ?>

                                        <input type="text" id="n_airline" name="n_airline" class="form__group-input n_airline" placeholder="Booking Confirmation" value="<?php echo $row['n_airline']; ?>">

                                    <?php } ?>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>

                                </div>
                            </div>
                            
                            <!-- Group Booking Confirmation -->
                            <div class="form__group form__group__n_flight" id="group__n_flight">
                                <div class="form__group__icon-input">

                                    <label for="n_flight" class="form__label">Landing Flight Number</label>

                                    <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                                    <?php if(empty($row['n_flight']) || $row['n_flight'] == 'NULL'){  ?>
                                        
                                        <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight" placeholder="Landing Flight Number">

                                    <?php } else{ ?>

                                        <input type="text" id="n_flight" name="n_flight" class="form__group-input n_flight" placeholder="Landing Flight Number" value="<?php echo $row['n_flight']; ?>">

                                    <?php } ?>
                                </div>
                                <p class="form__group__input-error">Minimum 4 Characters</p>
                            </div>


                                <div class="form__group form__group-f_date ">
                                    <div class="form__group__icon-input">
                                        <label for="f_date" class="form__label">Flight Date</label>
                                        <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                                        <?php if(empty($row['departure_f']) || $row['departure_f'] == 'NULL'){  ?>

                                            <input type="date" id="f_date" name="f_date" class="form__group-input f_date" placeholder="Flight Date">

                                        <?php } else{ ?>

                                            <input type="date" id="f_date" name="f_date" class="form__group-input f_date" placeholder="Flight Date" value="<?php echo $row['departure_f']; ?>">
                                        
                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <div class="form__group form__group-h_date">

                                    <div class="form__group__icon-input">

                                        <label for="h_date" class="form__label">Arrival at the Homestay</label>
                                        <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                                        <?php if(empty($row['arrive_f']) || $row['arrive_f'] == 'NULL'){  ?>

                                            <input type="date" id="h_date" name="h_date" class="form__group-input h_date" placeholder="Arrival Date at the Homestay">

                                        <?php }else{ ?>

                                            <input type="date" id="h_date" name="h_date" class="form__group-input h_date" placeholder="Arrival Date at the Homestay" value="<?php echo $row['arrive_f']; ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>

                        </div>

                    </div>



                    <div class="card">

                        <h3 class="form__group-title__col-4">Emergency Contact</h3>

                        <div class="information__content">


                                <!-- Group Emergency Name -->
                                <div class="form__group form__group__cont_name" id="group__cont_name">
                                    <div class="form__group__icon-input">

                                        <label for="cont_name" class="form__label">Contact Name</label>

                                        <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>

                                        <?php if(empty($row['cont_name']) || $row['cont_name'] == 'NULL'){  ?>

                                            <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name" placeholder="Contact Name">

                                        <?php }else{ ?>

                                            <input type="text" id="cont_name" name="cont_name" class="form__group-input cont_name" placeholder="Contact Name" value="<?php echo $row['cont_name']; ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Emergency Last Name -->
                                <div class="form__group form__group__cont_lname" id="group__cont_lname">

                                    <div class="form__group__icon-input">

                                        <label for="cont_lname" class="form__label">Contact Last Name</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>

                                        <?php if(empty($row['cont_lname']) || $row['cont_lname'] == 'NULL'){  ?>

                                            <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname" placeholder="Contact Last Name">

                                        <?php } else{ ?>

                                            <input type="text" id="cont_lname" name="cont_lname" class="form__group-input cont_lname" placeholder="Contact Last Name" value="<?php echo $row['cont_lname'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                                <!-- Group Emergency phone -->
                                <div class="form__group form__group__num_conts" id="group__num_conts">

                                    <div class="form__group__icon-input">

                                        <label for="num_conts" class="form__label">Emergency Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                                        <?php if(empty($row['num_conts']) || $row['num_conts'] == 'NULL'){  ?>

                                            <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts" placeholder="Emergency Phone Number">

                                        <?php } else{ ?>

                                            <input type="text" id="num_conts" name="num_conts" class="form__group-input num_conts" placeholder="Emergency Phone Number" value="<?php echo $row['num_conts']; ?>">

                                        <?php } ?>
                                    </div>
                                        <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>
                                
                                <!-- Group Alternative Phone -->
                                <div class="form__group form__group__cell_s" id="group__cell_s">
                                    <div class="form__group__icon-input">

                                        <label for="cell_s" class="form__label">Alternative Phone Number</label>

                                        <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                                        <?php if(empty($row['cell_s']) || $row['cell_s'] == 'NULL'){  ?>

                                            <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s" placeholder="Alternative Phone Number">

                                        <?php }else{ ?>

                                            <input type="text" id="cell_s" name="cell_s" class="form__group-input cell_s" placeholder="Alternative Phone Number" value="<?php echo $row['cell_s'] ?>">

                                        <?php } ?>
                                    </div>
                                    <p class="form__group__input-error">Minimum 4 Characters</p>
                                </div>


                        </div>

                    </div>

                </div>







                <div class="col-md-8 column-2">

                    <div class="card">

                        <h3 class="form__group-title__col-4">Personal Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Date of Birth -->
                                <div class="form__group col-sm-4 form__group__db" id="group__db">
                                    <div class="form__group__icon-input">

                                        <label for="db" class="form__label">Date of Birth</label>
                                        <label for="db" class="icon"><i class="icon icon__date"></i></label>

                                        <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL'){  ?>

                                            <input type="date" id="db" name="db" class="form__group-input db" placeholder="Date of Birth">

                                        <?php }else{ ?>

                                            <input type="date" id="db" name="db" class="form__group-input db" placeholder="Date of Birth" value="<?php echo $row['db_s']; ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Gender -->
                                <div class="form__group col-sm-4 form__group__basic form__group__gender" id="group__gender">

                                    <div class="form__group__icon-input">

                                        <label for="gender" class="form__label">Gender</label>

                                        <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                                        <select class="custom-select form__group-input gender" id="gender" name="gender">
                                            <?php if(empty($row['gen_s']) || $row['gen_s'] == 'NULL'){  ?>

                                                <option hidden="option" selected disabled> Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Any">Any</option>

                                            <?php }else if($row['gen_s'] == 'Male'){ ?>

                                                <option value="Male" selected>Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Any">Any</option>
                                            
                                            <?php }else if($row['gen_s'] == 'Female'){ ?>

                                                <option value="Female"selected>Female</option>
                                                <option value="Male" >Male</option>
                                                <option value="Any">Any</option>
                                            
                                            <?php }else if($row['gen_s'] == 'Any'){ ?>

                                                <option value="Any" selected>Any</option>
                                                <option value="Female">Female</option>
                                                <option value="Male" >Male</option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a Gender</p>
                                </div>

                                
                                
                                <!-- Group Background -->
                                <div class="form__group col-sm-4 form__group__basic form__group__nacionality" id="group__nacionality">
                                    <div class="form__group__icon-input">

                                        <label for="nacionality" class="form__label">Background</label>

                                        <label for="nacionality" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <?php if(empty($row['nacionality']) || $row['nacionality'] == 'NULL'){  ?>
                                            <input type="text" id="nacionality" name="nacionality" class="form__group-input nacionality" placeholder="Background">

                                        <?php }else{ ?>

                                            <input type="text" id="nacionality" name="nacionality" class="form__group-input nacionality" placeholder="Background" value="<?php echo $row['nacionality'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Origin City -->
                                <div class="form__group col-sm-4 form__group__basic form__group__city" id="group__city">
                                    <div class="form__group__icon-input">

                                        <label for="city" class="form__label visa">Origin City</label>

                                        <label for="city" class="icon"><i class="icon icon__city"></i></label>

                                        <?php if(empty($row['city']) || $row['city'] == 'NULL'){  ?>
                                            <input type="text" id="city" name="city" class="form__group-input city" placeholder="Origin City">

                                        <?php }else{ ?>

                                            <input type="text" id="city" name="city" class="form__group-input city" placeholder="Origin City" value="<?php echo $row['city'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Passport -->
                                <div class="form__group col-sm-4 form__group__basic form__group__pass" id="group__pass">
                                    <div class="form__group__icon-input">

                                        <label for="pass" class="form__label visa">Passport</label>

                                        <label for="pass" class="icon"><i class="icon icon__pass"></i></label>

                                        <?php if(empty($row['passport']) || $row['passport'] == 'NULL'){  ?>
                                            <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport">
                                        <?php } else{ ?>

                                            <input type="text" id="pass" name="pass" class="form__group-input pass" placeholder="Passport" value="<?php echo $row['passport'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Visa Expiration Date -->
                                <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                                    <div class="form__group__icon-input">

                                        <label for="bl" class="form__label visa">Visa Expiration Date</label>
                                        <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                                        <?php if(empty($row['passport']) || $row['passport'] == 'NULL'){  ?>
                                            <input type="date" id="bl" name="bl" class="form__group-input bl" placeholder="Visa Expiration Date">

                                        <?php }else{ ?>

                                            <input type="date" id="bl" name="bl" class="form__group-input bl" placeholder="Visa Expiration Date" value="<?php echo $row['passport'] ?>">

                                        <?php } ?>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <div class="form__group col-sm-6 form__group__basic form__group__visa" id="group__visa">
                                    <?php if(empty($row['visa']) || $row['visa'] == 'NULL'){}else{  ?>

                                        <iframe class="embed-responsive-item" src="../<?php echo $row['visa'] ?>" id="visa"></iframe>

                                    <?php } ?>
                                    <div class="form__group__icon-input">
                                        

                                        <label for="visa" class="form__label visa">Visa</label>
                                        <label for="visa" class="icon"><i class="icon icon__city"></i></label>

                                        <?php if(empty($row['visa']) || $row['visa'] == 'NULL'){  ?>

                                            <input type="file" id="visa" name="visa" class="form__group-input visa" placeholder="Visa" maxlength="1" accept="pdf">

                                        <?php }else{ ?>


                                            <input type="file" id="visa" name="visa" class="form__group-input visa" placeholder="Visa" maxlength="1" accept="pdf" value="<?php echo $row['visa'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                            </div>

                        </div>

                    </div>


                    <div class="card">

                        <h3 class="form__group-title__col-4">School Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">


                                <!-- Group Academy Preference -->
                                <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                                    <div class="form__group__icon-input">

                                        <label for="n_a" class="form__label">School Preference</label>

                                        <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                                        <select class="custom-select form__group-input n_a" id="n_a" name="n_a">   
                                            <?php    
                                                while ( $row6 = $query6->fetch_array() )    
                                                    {

                                                    if(empty($row['n_a']) || $row['n_a'] == 'NULL'){
                                            ?>
                                                
                                                    <option hidden="option" selected disabled>School Preference</option>
                                                    <option value=" <?php echo $row6['id_ac'] ?> " >
                                                        <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                                                    </option>

                                                    <?php }else{ 
                                                    
                                                        $query7 = $link->query("SELECT * FROM academy WHERE id_ac = '$row[n_a]'");
                                                        $row7=$query7->fetch_assoc();
                                                    
                                                    ?>

                                                    <option value=" <?php echo $row7['id_ac'] ?> " selected ><?php echo $row7['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?></option>
                                                    <option value=" <?php echo $row6['id_ac'] ?> " >
                                                        <?php echo $row6['name_a']; ?><?php echo ', '?><?php echo $row6['dir_a']; ?>
                                                    </option>

                                                    <?php } ?>
            
                                            <?php
                                                    }    
                                            ?>       
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <!-- Group Origin Language -->
                                <div class="form__group col-sm-4 form__group__basic form__group__lang_s" id="group__lang_s">
                                    <div class="form__group__icon-input">

                                        <label for="lang_s" class="form__label">Origin Language</label>

                                        <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>
                                        <?php if(empty($row['lang_s']) || $row['lang_s'] == 'NULL'){  ?>
                                            <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s" placeholder="Origin Language">
                                        <?php }else{ ?>
                                            <input type="text" id="lang_s" name="lang_s" class="form__group-input lang_s" placeholder="Origin Language" value="<?php echo $row['lang_s'] ?>">
                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Type Student -->
                                <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                                    <div class="form__group__icon-input">

                                        <label for="type_s" class="form__label">Type Student</label>

                                        <label for="type_s" class="icon"><i class="icon icon__names"></i></label>
                                        <?php if(empty($row['type_s']) || $row['type_s'] == 'NULL'){  ?>
                                            <input type="text" id="type_s" name="type_s" class="form__group-input type_s" placeholder="Type Student">
                                        <?php } else{ ?>
                                            <input type="text" id="type_s" name="type_s" class="form__group-input type_s" placeholder="Type Student" value="<?php echo $row['type_s'] ?>">
                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>

                                <!-- Group Start Date of Stay -->
                                <div class="form__group col-sm-4 form__group__basic form__group__firstd" id="group__firstd">
                                    <div class="form__group__icon-input">
                                        
                                        <label for="firstd" class="form__label">Start Date of Stay</label>
                                        <label for="firstd" class="icon"><i class="icon icon__date"></i></label>
                                        <?php if(empty($row['firstd']) || $row['firstd'] == 'NULL'){  ?>
                                            <input type="date" id="firstd" name="firstd" class="form__group-input firstd" placeholder="Start Date of Stay">
                                        <?php }else{ ?>

                                            <input type="date" id="firstd" name="firstd" class="form__group-input firstd" placeholder="Start Date of Stay" value="<?php echo $row['firstd'] ?>">

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group End Date of stay -->
                                <div class="form__group col-sm-4 form__group__basic form__group__lastd" id="group__lastd">
                                    <div class="form__group__icon-input">
                                        
                                        <label for="lastd" class="form__label">End Date of Stay</label>
                                        <label for="lastd" class="icon"><i class="icon icon__date"></i></label>
                                        <?php if(empty($row['lastd']) || $row['lastd'] == 'NULL'){  ?>
                                            <input type="date" id="lastd" name="lastd" class="form__group-input lastd" placeholder="End Date of stay">
                                        <?php }else{ ?>
                                            <input type="date" id="lastd" name="lastd" class="form__group-input lastd" placeholder="End Date of stay" value="<?php echo $row['lastd'] ?>">
                                        <?php } ?>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                            
                            </div>

                        </div>

                    </div>


                    <div class="card">

                        <h3 class="form__group-title__col-4">Preferences Information</h3>

                        <div class="information__content information__content-col-8">

                            <div class="row form__group-basic-content m-0 p-0">

                                <!-- Group Smoker -->
                                <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                                    <div class="form__group__icon-input">

                                        <label for="smoke_s" class="form__label smoker">Smoker</label>

                                        <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                                        <select class="custom-select form__group-input smoke_s" id="smoke_s" name="smoke_s">
                                            <?php if(empty($row['smoke_s']) || $row['smoke_s'] == 'NULL'){  ?>
                                                <option hidden="option" selected disabled>Smoker</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            <?php }else if($row['smoke_s'] == 'Yes'){ ?>

                                                <option value="Yes" selected>Yes</option>
                                                <option value="No">No</option>

                                            <?php }else if($row['smoke_s'] == 'No'){ ?>

                                                <option value="No" selected>No</option>
                                                <option value="Yes">Yes</option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>

                                </div>
                                
                                <!-- Group Pets -->
                                <div class="form__group col-sm-4 form__group__basic form__group__pets" id="group__pets">

                                    <div class="form__group__icon-input">

                                        <label for="pets" class="form__label pets_l">Pets</label>

                                        <label for="pets" class="icon"><i class="icon icon__pets"></i></label>

                                        <select class="custom-select form__group-input pets" id="pets" name="pets">
                                            <?php if(empty($row['pets']) || $row['pets'] == 'NULL'){  ?>
                                                <option hidden="option" selected disabled>Pets</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            <?php }else if($row['pets'] == 'Yes'){ ?>

                                                <option value="Yes" selected>Yes</option>
                                                <option value="No">No</option>

                                            <?php }else if($row['pets'] == 'No'){ ?>

                                                <option value="No" selected>No</option>
                                                <option value="Yes">Yes</option>

                                            <?php } ?>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                <!-- Group Food -->
                                <div class="form__group col-sm-4 form__group__basic form__group__food" id="group__food">

                                    <div class="form__group__icon-input">

                                        <label for="food" class="form__label pets_l">Food</label>

                                        <label for="food" class="icon"><i class="icon icon__food"></i></label>

                                        <select class="custom-select form__group-input food" id="food" name="food">
                                            <?php if(empty($row['food']) || $row['food'] == 'NULL'){  ?>
                                                <option hidden="option" selected disabled>Food</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            <?php }else if($row['food'] == 'Yes'){ ?>

                                                <option value="Yes" selected>Yes</option>
                                                <option value="No">No</option>

                                            <?php }else if($row['food'] == 'No'){ ?>

                                                <option value="No" selected>No</option>
                                                <option value="Yes">Yes</option>

                                            <?php } ?>
                                        </select>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>
                                
                                
                                <!-- Group Allergies -->
                                <div class="form__group col-sm-12 form__group__basic form__group__allergies" id="group__allergies">

                                    <div class="form__group__icon-input">

                                        <label for="allergies" class="form__label allergies_l">Allergies</label>

                                        <label for="allergies" class="icon"><i class="icon icon__allergies"></i></label>

                                        <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL'){  ?>

                                            <textarea class="form__group-input allergies" id="allergies" rows="3" name="allergies" placeholder="Describe in a few words if you have any allergies (Peanuts, Fish, Seafood, etc.)."></textarea>

                                        <?php }else{ ?>

                                            <textarea class="form__group-input allergies" id="allergies" rows="3" name="allergies" placeholder="Describe in a few words if you have any allergies (Peanuts, Fish, Seafood, etc.)."><?php echo $row['allergies'] ?></textarea>

                                        <?php } ?>

                                    </div>
                                    <p class="form__group__input-error">Enter a valid Date</p>
                                </div>


                                <!-- Group Special -->
                                <div class="form__group form__group__basic form__group__special-diet" id="group__special-diet">
                                
                                    <div class="form__group__div-special__diet">
                                        <div class="form__group__icon-input">

                                            <label for="" class="icon"><i class="icon icon__diet"></i></label>
                                            <h4 class="title__special-diet">Special Diet</h4>

                                        </div>
                                    

                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">
                                                <?php if($row['vegetarians'] == "yes"){ ?>

                                                    <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes" checked>

                                                <?php } else{ ?>

                                                    <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians" name="vegetarians" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">

                                                <?php if($row['halal'] == "yes"){ ?>
                                                    
                                                    <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes" checked>

                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes">

                                                <?php } ?>

                                                <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                                            </div>

                                        </div>

                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">

                                                <?php if($row['kosher'] == "yes"){ ?>

                                                    <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes" checked>

                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">

                                                <?php if($row['lactose'] == "yes"){ ?>

                                                    <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes" checked>

                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                                            </div>

                                        </div>
                                        
                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox">

                                                <?php if($row['gluten'] == "yes"){ ?>

                                                    <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes" checked>

                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <?php if($row['pork'] == "yes"){ ?>
                                                    <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" checked>
                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="pork">No Pork</label>
                                            </div>

                                        </div>
                                        
                                        <div class="form__group__row-diet">
                                            
                                            <div class="custom-control custom-checkbox-none custom-checkbox">
                                                <?php if($row['none'] == "yes"){ ?>
                                                    <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" checked>
                                                <?php }else{ ?>

                                                    <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes">

                                                <?php } ?>
                                                <label class="custom-control-label" for="none">None</label>
                                            </div>

                                        </div>

                                    </div>

                                </div>


                            </div>

                        </div>

                    </div>

                </div>
            </div><br>

            <div class="form__group__btn-send" >
                <button type="submit" name="confirm" class="form__btn confirm_changes"><i class="fa fa-save ts-opacity__50 mr-2"></i>Confirm Information</button>
                <button type="submit" name="update" class="form__btn"><i class="fa fa-save ts-opacity__50 mr-2"></i>Save Changes</button>
                <p class="form__message-sucess" id="form__message-sucess"> form send sucefuly</p>
            </div>

            

            
        </form>

        <hr>

            <!--DELETE SECTION-->
            <div class="panel-group">
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse1"><i class="fa fa-cog" id="options"></i> Advance Options</a>
                                        </h4>
                                    </div>

                                    <div id="collapse1" class="panel-collapse collapse">
                                        <div class=" row text-center">

                                            <div class="col-sm-4">        

                                                <a data-toggle="collapse" href="#disable1" class="btn btn-info" style="background-color: #982a72; color: #fff; border: 1px solid #982a72" id="active"><i class="fa fa-user-times"></i> Disable Student</a>

                                                <div id="disable1" class="collapse">
                                                <br>

                                                    <h3>Disable Student</h3>

                                                    <p>The student will be disable immediately.<br/>

                                                        This action will <b>disable the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its files, information and events.<br/>

                                                        <b>If you wish disable this user account press the button?</b>
                                                    </p>
                                                    <a data-toggle="modal" class="btn btn-info" href="#myModal2" id="active" style="background-color:#982a72; color: #fff; border: 1px solid #982a72"><i class="fa fa-user-times" id="options" ></i> Disable Student</a>

                            
                                                </div>
                                            </div>

                                            <div class="col-sm-4"> 

                                                <a data-toggle="collapse" href="#activate" class="btn btn-info" style="background-color:#232159; color: #fff; border: 1px solid #232159" id="active"><i class="fa fa-user-plus"></i> Activate Student</a>

                                                <div id="activate" class="collapse">
                                                <br>
                                    
                                                    <h3>Activate Student</h3>
                                                    <p>The student will be reactivate immediately.<br/>

                                                        This action will <b>reactivate the</b> <?php echo "".$row['mail_s']."";?> <b>account</b>, including its files, information and events.<br/>

                                                        <b>If you wish reactivate this user account press the button?</b>
                                                    </p>

                                                    <a data-toggle="modal" class="btn btn-info" href="#myModal22" id="active" style="background-color:#232159; color: #fff; border: 1px solid #232159"><i class="fa fa-user-plus" id="options" ></i> Reactivate Student</a>


                                                    <br>        
                                                </div>
                                            </div>

                                            <div class="col-sm-4">

                                                <a data-toggle="collapse" href="#delete1" class="btn btn-info" style="background-color:#4f177d; color: #fff; border: 1px solid #4f177d" id="active"><i class="fa fa-eraser"></i> Delete Student</a>



                                                <div id="delete1" class="collapse">
                                                <!-- Trigger the modal with a button -->
                                                <br>
                                                <!-- Trigger the modal with a button -->

                                                    <h3 id="delete">Delete Student</h3>
                                                    <p>The Student will be permanently deleted immediately.<br/>

                                                        This action will <b>permanently delete</b> <?php echo "".$row['mail_s']."";?> <b>immediately</b>, including its files, information and events.<br/>

                                                        <b>Are you ABSOLUTELY SURE you wish to delete this student?</b>
                                                    </p>

                                                    <a data-toggle="modal" class="btn btn-info" href="#myModal" id="active" style="background-color:#4f177d; color: #fff; border: 1px solid #4f177d"><i class="fa fa-eraser" id="options" ></i> Delete Student</a>

                                                <br>
                                                </div>
                                            </div>
                                        </div>
                                    <div>
                                </div><br>
                            </div>
                                <!--END DELETE SECTION-->

                            

                                            
                            
                            </section>

                        </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

    <!-- Modal Disable Student -->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Disable Student</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>  
                </div>

                <div class="modal-body" id="background">
                    <div class="modal-body" id="body"><p>
                        <i class="fa fa-asterisk" id="options"></i> You are about to disable this student<br/>
                            Once a student is disable the user will not login in homebor until his user has been reactivated again. its files, all information and events will be not removing of the data.
                        </p>
                    </div>
                </div>
                
                <div class="modal-body" id="background">
                    <p><b>All Fields Required</b></p>
                    <p>Please type the following to confirm:<br>

                    <?php echo "<b>".$row['id_student']."</b>";?></p>
                    <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post" enctype="multipart/form-data">

                        <div class="row">

                            <!--House Name-->
                            <div class="col-sm-8">
                                <div class="form-group">

                                    <input type="text" class="form-control" id="del_id" name="del_id" required>

                                    <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>

                                    <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>

                                    <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>

                                    <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row7[id_m]";?>" hidden>

                                    <label for="des">Reason</label>

                                    <textarea class="form-control" id="des" rows="2" name="des" required></textarea>

                                </div>
                            </div>

                        </div>             

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
                            <button type="submit" id="submit" name="disable" class="btn btn-default">
                                <i class="fa fa-user-times"></i> Disable</button>
                        </div>
                    </form>
                </div>
                                    
            </div>

        </div>
    </div>

    <!-- Modal Reactivate Student -->
    <div id="myModal22" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reactivate Student</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>  
                </div>

                <div class="modal-body" id="background">
                    <div class="modal-body" id="body">
                        <p> <i class="fa fa-asterisk" id="options"></i> You are about to reactivate this student<br/>
                            Once a student is reactivate the user will login in homebor.</p>
                    </div>
                </div>

                <div class="modal-body" id="background">
                    <p><b>All Fields Required</b></p>
                    <p>Please type the following to confirm:<br>

                    <?php echo "<b>".$row['id_student']."</b>";?></p>
                    
                    <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post" enctype="multipart/form-data">

                        <div class="row">

                            <!--House Name-->
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="del_id" name="del_id" required>

                                    <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>

                                    <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>

                                    <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>

                                    <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row7[id_m]";?>" hidden>

                                    <label for="des">Reason</label>

                                    <textarea class="form-control" id="des" rows="2" name="des" required></textarea>
                                </div>
                            </div>
                        </div>
                                                    
                
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="submit">Close</button>
                            <button type="submit" id="active" name="reactivate" class="btn btn-default" id="submit">
                                    <i class="fa fa-user-plus"></i> Reactivate</button>
                        
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete">Delete Student</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>  
                </div>

                <div class="modal-body" id="background">
                    <div class="modal-body" id="body">
                        <p><i class="fa fa-asterisk" id="options"></i> You are about to permanently delete this student<br/>
                            Once a student is permanently deleted it cannot be recovered. Permanently deleting this student will immediately delete its files, all information and events.
                        </p>
                    </div>
                </div>

                <div class="modal-body" id="background">
                    <p><b>All Fields Required</b></p>
                    <p>This action cannot be undone. You will lose the student's information, events and all files.<br><br>

                        Please type the following to confirm:<br>

                    <?php echo "<b>".$row['id_student']."</b>";?></p>
                    <form id="form-submit" class="ts-form" autocomplete="off" action="delete_student_agency.php" method="post" enctype="multipart/form-data">
  
                        <div class="row">

                            <!--House Name-->
                            <div class="col-sm-8">
                                <div class="form-group">

                                    <input type="text" class="form-control" id="del_id" name="del_id" required>

                                    <input type="text" class="form-control" id="user" name="user" required value="<?php echo "$usuario";?>" hidden>

                                    <input type="text" class="form-control" id="id_s" name="id_s" required value="<?php echo "$row[id_student]";?>" hidden>

                                    <input type="text" class="form-control" id="name" name="name" required value="<?php echo "$row[mail_s]";?>" hidden>

                                    <input type="text" class="form-control" id="id_m" name="id_m" required value="<?php echo "$row7[id_m]";?>" hidden>

                                    <label for="des">Reason</label>

                                    <textarea class="form-control" id="des" rows="2" name="des" required></textarea>

                                </div>
                            </div>
                        </div>
                                                    
                                    
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
                            <button type="submit" id="submit" name="register" class="btn btn-default">
                                <i class="fa fa-eraser"></i> Delete</button>
                        </div>

                    </form>
                </div>
                                    
            </div>

        </div>
    </div>

            
    </main>

</div>


    <div id="house_much" style="display: none;">
        <div class="close_div"><a href="#" onclick="close()" id="close">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-10" id="contentp">
                    
                </div>
            </div>
        </div>

    </div>
    <div id="house_ass" style="display: none;">
        <div class="close_div"><a href="#" onclick="close2()" id="close2">X</a></div>
        <div class="content_popup" >
            
            <div class="content">
                <div class="col-2">
                    <img class="img-stu" src="../images/icon.png"/>
                </div>
                <div class="col-9" id="assh">
                    
                </div>
            </div>
        </div>

    </div>


<?php include '../master/footer.php'; ?>

<script>
                                function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
            var fileReader = new FileReader();

            fileReader.onload = function (event) {
                document.getElementById("preview").setAttribute("src", event.target.result);
            };

            fileReader.readAsDataURL(file[0]);

            var label_1 = document.getElementById("label-i");
            var label_1_1 = document.getElementById("label-i-i");

            label_1.style.display = 'none';
            label_1_1.style.display = 'inline';

        }

    }
                            </script>


<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>
<script src="assets/js/galery.js"></script>
<!--Date Input Safari-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script src="assets/js/date-safari.js"></script>

</body>
</html>