<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$queryStudent = $link->query("SELECT name_s, l_name_s, db_s, n_airline, n_flight, departure_f, arrive_f, house1, house2, house3, house4, house5, house6, house7, house8 FROM pe_student WHERE mail_s = '$usuario'");
$row_student = $queryStudent->fetch_assoc();

include 'ticket_validate.php';

?>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">


  <title>Homebor - Students</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.4">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.4">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.4">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.4">
  <link rel="stylesheet" type="text/css" href="assets/css/index.css?ver=1.0.4">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.4">


  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js?ver=1.0.4"
    integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
    crossorigin=""></script>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js?ver=1.0.4"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>

  <!-- // TODO Mapbox Link -->
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
</head>

<!-- // TODO HEADER -->
<?php require 'header.php' ?>

<!-- // TODO BODY -->

<body>

  <!-- // TODO MAIN -->
  <main>
    <!-- // TODO MAP -->
    <section id="ts-hero" class=" mb-0" style="height: 400px;">
      <div class="ts-full-screen ts-has-horizontal-results w-1001 d-flex1 flex-column1" style="height: 100px;">
        <div class="ts-map ts-shadow__sm"> <?php include 'mapboxstudent.php' ?> </div>
      </div>
    </section>


    <section class="container-fluid my-5">
      <article class="container mb-5">
        <h1 class="mb-0 text-center text-lg-left">Search Your Homestay</h1>
        <?php echo "<p class='text-center text-lg-left'>You are logged in as <b>$row_student[name_s] $row_student[l_name_s]</b>.</p>" ?>
      </article>

      <!-- // * LOADING HOUSES -->
      <article id="loading-houses" class="d-flex flex-column align-items-center justify-content-center loading-houses">
        <h4>Loading houses...</h4>
        <svg width="50" height="50" viewBox="0 0 38 38"">
          <g fill=" none" fill-rule="evenodd">
          <g transform="translate(1 1)" stroke-width="2">
            <circle stroke-opacity=".5" cx="18" cy="18" r="18" />
            <path d="M36 18c0-9.94-8.06-18-18-18">
              <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s"
                repeatCount="indefinite" />
            </path>
          </g>
          </g>
        </svg>
      </article>


      <article id="all-houses"
        class="col-12 col-lg-11 mx-auto d-flex flex-column flex-lg-row justify-content-center houses-container">
      </article>

    </section>
  </main>



  <TEMPLATES>
    <!-- // * TEMPLATES -->
    <template id="house-card-template">
      <a href="#" class="mb-5 mb-lg-4 pb-2 pb-lg-0 mx-auto d-flex flex-column bg-white card-house" data-id-home>
        <!-- // ** CARD HEADER-->
        <div class="p-0 d-flex flex-column align-items-center card-header" title="See profile">
          <img src="../assets/emptys/frontage-empty.png" class="mx-auto" data-house-image alt="house, homestay">
        </div>

        <!-- // ** CARD BODY CONTAINER-->
        <div class="w-100 py-2 mb-2 card-body">
          <!-- // *** CARD MAIN INFORMATION-->
          <div class="p-0 main-information">
            <!-- // **** HOUSE NAME  -->
            <h5 class="mb-3 border-bottom" data-house-name>House Name</h5>

            <!-- // **** ROW 2  -->
            <dl class="m-0 mb-3 d-flex flex-column align-items-center">
              <dt>Address</dt>
              <dd class="m-0 px-2 text-center" data-house-direction>99 Harbour Sq, Toronto Ontario</dd>
            </dl>


            <!-- // **** ROW 3  -->
            <div class="pb-0 mb-2 text-center d-flex align-items-center justify-content-around">
              <dl class="m-0">
                <dt>Gender</dt>
                <dd class="m-0" data-house-gender-preference>Female</dd>
              </dl>

              <dl class="m-0">
                <dt>Age</dt>
                <dd class="m-0" data-house-age-preference>Adult</dd>
              </dl>
            </div>


            <!-- // **** ROW 4  -->
            <div class="pb-0 text-center d-flex align-items-center justify-content-around">
              <dl class="m-0">
                <dt>Bedrooms</dt>
                <dd class="m-0" data-house-rooms>3</dd>
              </dl>

              <dl class="m-0">
                <dt>Language</dt>
                <dd class="m-0" data-house-background-language="">English</dd>
              </dl>
            </div>

          </div>
        </div>
      </a>
    </template>
  </TEMPLATES>

  <?php include 'footer.php' ?>
  <!-- // ? AXIOS -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?ver=1.0.4"></script>

  <script src="../assets/js/sly.min.js?ver=1.0.4"></script>
  <script src="../assets/js/dragscroll.js?ver=1.0.4"></script>
  <script src="../assets/js/jquery-3.3.1.min.js?ver=1.0.4"></script>
  <script src="../assets/js/popper.min.js?ver=1.0.4"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js?ver=1.0.4"></script>
  <script src="../assets/js/jquery.scrollbar.min.js?ver=1.0.4"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js?ver=1.0.4"></script>
  <script src="../assets/js/leaflet.js?ver=1.0.4"></script>
  <script src="../assets/js/map-leaflet.js?ver=1.0.4"></script>
  <script src="../assets/js/leaflet.markercluster.js?ver=1.0.4"></script>

  <script src="assets/js/houses-assigned.js?ver=1.0.4" type="module"></script>
</body>

</html>