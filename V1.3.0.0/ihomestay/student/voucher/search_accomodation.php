<?php
require('../../fpdf/fpdf.php');
include '../../xeon.php';
session_start();

define('Tag',chr(149));
define('Tag2',chr(155));

$usuario = $_SESSION['username'];

if(isset($_GET['art_id'])){
    $id = $_GET['art_id'];

$query="SELECT * FROM studentv WHERE email = '$usuario' AND id_v = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM manager WHERE id_m = '$row[id_m]'";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query3="SELECT *, DATE_FORMAT(arrive_g, '%d') as day_arrival, DATE_FORMAT(arrive_g, '%m') as month_arrival, DATE_FORMAT(arrive_g, '%Y') as year_arrival FROM pe_student INNER JOIN academy ON pe_student.mail_s = '$row[email]' AND pe_student.n_a = academy.id_ac";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$academy = $row3['name_a'].' - '.$row3['dir_a'];

if($row3['cont_name'] == 'NULL') {
    $nameEmergency = '';
} else {
    $nameEmergency = $row3['cont_name'];
}
if($row3['cont_lname'] == 'NULL') {
    $lastNameEmergency = '';
} else {
    $lastNameEmergency = $row3['cont_lname'];
}

$emergencyname = $nameEmergency.' '.$row3['cont_lname'];


    if($row3['db_s'] != 'NULL') {
        //Edad del estudiante
        $from = new DateTime($row3['db_s']);
        $to   = new DateTime('today');
        $age= $from->diff($to)->y; 
    }
    
    if($row3['firstd'] != 'NULL' && $row3['lastd'] != 'NULL') {

        //Contrated time
        $datetime1 = new DateTime($row3['lastd']);
        $datetime2 = new DateTime($row3['firstd']);
        $interval = $datetime1->diff($datetime2);
        $contratedTime2 = $interval->format('%a days');
        $contratedTime3 = $interval->format('%a')/7;

        if($contratedTime3 > 1) {
            $notation = "weeks";
        } else {
            $notation = "week";
        }

        $contratedTime = $contratedTime2.' '.$contratedTime3.' '.$notation;
    }

    if($row3['arrive_g'] != 'NULL' && $row3['lastd'] != 'NULL') {
        //Duration of Accommodation
        $datetime3 = new DateTime($row3['lastd']);
        $datetime4 = new DateTime($row3['arrive_g']);
        $interval = $datetime3->diff($datetime4);
        $durationAccomadion2 = $interval->format('%a days');
        $durationAccomadion3 = $interval->format('%a')/7;

        if($durationAccomadion3 > 1) {
            $notationAccomadion = "weeks";
        } else {
            $notationAccomadion = "week";
        }

        $durationAccomadion = $durationAccomadion2.' '.$durationAccomadion3.' '.$notationAccomadion;
    }
    
    if($row3['signature_s'] != 'NULL'){
        $photo_signature = 'https://homebor.com/'.$row3['signature_s'];
    }
    
    if($row3['a_price'] != 'NULL'){
        $prices_week = $row3['a_price']/4;
    }
    
    if($row3['arrive_g'] != 'NULL'){
        $Dayarrive = $row3['day_arrival'];
        $Montharrive = $row3['month_arrival'];
        $Yeararrive = $row3['year_arrival'];
    } else {
        $Dayarrive = '';
        $Montharrive = '';
        $Yeararrive = '';
    }
    
    
    

if ($usuario != $row['email']) {
	 header("location: ../index.php");
}
}

class PDF extends FPDF
{
/* footer */
function Footer()
{
    //Footer Frame
    $this->SetXY(0,-15);
    $this->SetFillColor(232,211,233,255);
    $this->SetFont('Arial','B',14);
    $this->Cell(210,15,'',0, 0, 'C', True);
    
    //Footer Message
    $this->SetXY(5,-8);
    $this->SetFont('Arial','B',12);
    $this->Cell(0,0,'Powered By Homebor',0,0,'C');
}
}


$pdf = new PDF();

$pdf->AddPage();

//Title
$pdf->SetTitle('Search Accomodation');

//Frame Up
$pdf->SetXY(0,0);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,15,'',0, 0, 'C', True);

//Arrival date
$pdf->SetXY(130,20);
$pdf->Cell(0,0,'ARRIVAL DATE:',0,0,'L');

//Day Cell
$pdf->SetXY(105,25);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25,8,'',0, 0, 'C', True);

//Moths Cell
$pdf->SetXY(135,25);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(30,8,'',0, 0, 'C', True);

//Year Cell
$pdf->SetXY(170,25);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25,8,'',0, 0, 'C', True);

//Day variable
$pdf->SetXY(20,29);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,$Dayarrive,0,0,'C');

//Months variable
$pdf->SetXY(80,29);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,$Montharrive,0,0,'C');

//Year variable
$pdf->SetXY(150,29);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,$Yeararrive,0,0,'C');

//Day Title
$pdf->SetXY(33,38);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'Day',0,0,'C');

//Month Title
$pdf->SetXY(100,38);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'Month',0,0,'C');

//Year Title
$pdf->SetXY(165,38);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'Year',0,0,'C');

//IHomestay Image
$pdf->Image('../../assets/img/ihomestay.png',10,20,-300);

//Document Title
$pdf->SetXY(0,55);
$pdf->SetFont('Arial','B',22);
$pdf->Cell(0,0,'"QUESTIONNIRE FOR',0,0,'C');

//Document Title
$pdf->SetXY(0,65);
$pdf->SetFont('Arial','B',22);
$pdf->Cell(0,0,'ACCOMMODATION IN CANADA"',0,0,'C');

//Personal Information Frame
$pdf->SetXY(0,78);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//Personal Information Title
$pdf->SetXY(0,83);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'PERSONAL INFORMATION',0,0,'C');

//Name Title
$pdf->SetXY(7,100);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Name (s):',0,0,'L');

//Name Cell
$pdf->SetXY(30,96);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(70,8,'',0, 0, 'C', True);

if($row3['name_s'] == 'NULL') {
    //Name Variable
    $pdf->SetXY(35,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Name Variable
    $pdf->SetXY(35,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['name_s']),0,0,'L');

}

//Last Name Title
$pdf->SetXY(105,100);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Last Name:',0,0,'L');

//Last Name Cell
$pdf->SetXY(130,96);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(70,8,'',0, 0, 'C', True);

if($row3['name_s'] == 'NULL') { 
    //Last Name Variable
    $pdf->SetXY(135,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Last Name Variable
    $pdf->SetXY(135,100);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['l_name_s']),0,0,'L');
}

//Age Title
$pdf->SetXY(16,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Age:',0,0,'L');

//Age Cell
$pdf->SetXY(30,111);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(30,8,'',0, 0, 'C', True);

if ($row3['db_s'] == 'NULL') {
    //Age Variable
    $pdf->SetXY(35,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$age,0,0,'L');
}else{
    //Age Variable
    $pdf->SetXY(35,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$age,0,0,'L');
}

//Gender Title
$pdf->SetXY(75,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Gender:',0,0,'L');

//Gender Cell
$pdf->SetXY(95,111);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(30,8,'',0, 0, 'C', True);

if ($row3['gen_s'] == 'NULL') {
    //Gender Variable
    $pdf->SetXY(100,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
}else{
    //Gender Variable
    $pdf->SetXY(100,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['gen_s'],0,0,'L');
}

//Date of Birth Title
$pdf->SetXY(140,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Date of Birth:',0,0,'L');

//Date of Birth Cell
$pdf->SetXY(170,111);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(30,8,'',0, 0, 'C', True);

if ($row3['db_s'] == 'NULL') {
    //Date of Birth Variable
    $pdf->SetXY(170,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Date of Birth Variable
    $pdf->SetXY(170,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['db_s'],0,0,'L');
}

//Background Title
$pdf->SetXY(5,130);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Citizenship:',0,0,'L');

//Background Cell
$pdf->SetXY(35,126);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(165,8,'',0, 0, 'C', True);

if($row3['nacionality'] == 'NULL') {
    //Background Variable
    $pdf->SetXY(40,130);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Background Variable
    $pdf->SetXY(40,130);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['nationality']),0,0,'L');
}

//Address Title
$pdf->SetXY(10,143);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Address:',0,0,'L');

//Address Cell
$pdf->SetXY(35,139);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(165,8,'',0, 0, 'C', True);

if($row3['dir_s'] == 'NULL') {
    //Address Variable
    $pdf->SetXY(40,143);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Address Variable
    $pdf->SetXY(40,143);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['dir_s']),0,0,'L');
}

//City Title
$pdf->SetXY(10,155);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'City:',0,0,'L');

//City Cell
$pdf->SetXY(35,151);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['country'] == 'NULL') {
    //City Variable
    $pdf->SetXY(40,155);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //City Variable
    $pdf->SetXY(40,155);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['city_s']),0,0,'L');
}

//State Title
$pdf->SetXY(102,155);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Province/State:',0,0,'L');

//State Cell
$pdf->SetXY(135,151);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['state_s'] == 'NULL') {
    //State Variable
    $pdf->SetXY(140,155);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //State Variable
    $pdf->SetXY(140,155);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['state_s']),0,0,'L');
}

//Postal Code Title
$pdf->SetXY(10,168);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Zip Code:',0,0,'L');

//Postal Code Cell
$pdf->SetXY(35,163);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['p_code_s'] == 'NULL') {
    //Postal Code Variable
    $pdf->SetXY(40,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Postal Code Variable
    $pdf->SetXY(40,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['p_code_s'],0,0,'L');
}

//Country Title
$pdf->SetXY(110,168);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Country:',0,0,'L');

//Country Cell
$pdf->SetXY(135,163);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['country'] == 'NULL') {
    //Country Variable
    $pdf->SetXY(140,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Country Variable
    $pdf->SetXY(140,168);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['country']),0,0,'L');
}

//Telephone Title
$pdf->SetXY(10,180);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Telephone:',0,0,'L');

//Telephone Cell
$pdf->SetXY(35,176);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['num_s'] == 'NULL') {
    //Telephone Variable
    $pdf->SetXY(40,180);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Telephone Variable
    $pdf->SetXY(40,180);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['num_s']),0,0,'L');
}

//Email Title
$pdf->SetXY(17,193);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Email:',0,0,'L');

//Email Cell
$pdf->SetXY(35,189);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(165,8,'',0, 0, 'C', True);

if($row3['mail_s'] == 'NULL') {
    //Email Variable
    $pdf->SetXY(40,193);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Email Variable
    $pdf->SetXY(40,193);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['mail_s']),0,0,'L');
}

//Passport Title
$pdf->SetXY(10,205);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Passport:',0,0,'L');

//Passport Cell
$pdf->SetXY(35,201);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['passport'] == 'NULL') {
    //Passport Variable
    $pdf->SetXY(40,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Passport Variable
    $pdf->SetXY(40,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['passport'],0,0,'L');
}

//Visa Title
$pdf->SetXY(100,205);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Visa Expiration:',0,0,'L');

//Visa Cell
$pdf->SetXY(135,201);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['db_visa'] == 'NULL') {
    //Visa Variable
    $pdf->SetXY(140,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Visa Variable
    $pdf->SetXY(140,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['db_visa'],0,0,'L');
}

//Tell us about you Title
$pdf->SetXY(10,215);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Tell us about you:',0,0,'L');

//Tell us about you Cell
$pdf->SetXY(30,220);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,45,'',0, 0, 'C', True);

if($row3['about_me'] == 'NULL') {
    //Tell us about you Variable
    $pdf->SetXY(32,220);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Tell us about you Variable
    $pdf->SetXY(32,220);
    $pdf->SetFont('Arial','',12);
    $pdf->SetMargins(5, 5);
    $pdf->MultiCell( 160, 7,utf8_decode($row3['about_me']), 0);
}

//Page 2
$pdf->AddPage();

//Frame Up
$pdf->SetXY(0,12);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//Health Information Title
$pdf->SetXY(0,17);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'HEALTH INFORMATION',0,0,'C');

//What is your state of health at the moment? Title
$pdf->SetXY(10,30);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'What is your state of health at the moment?:',0,0,'L');

//What is your state of health at the moment? Frame
$pdf->SetXY(30,35);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,25,'',0, 0, 'C', True);

if($row3['healt_s'] == 'NULL'){
    //What is your state of health at the moment? Variable
    $pdf->SetXY(32,40);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //What is your state of health at the moment? Variable
    $pdf->SetXY(32,35);
    $pdf->SetFont('Arial','',12);
    $pdf->SetMargins(5, 5);
    $pdf->MultiCell( 160, 7,utf8_decode($row3['healt_s']), 0);
}

//Do you suffer from any disease? Title
$pdf->SetXY(10,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Do you suffer from any disease?:',0,0,'L');

if($row3['disease'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',85,65,-1500);
} else {
    if($row3['disease'] != 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',85,65,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',85,65,-1500);
    }
}

if($row3['disease'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',110,65,-1500);
} else {
    if($row3['disease'] == 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',110,65,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',110,65,-1500);
    }
}

//Do you suffer from any disease? Yes
$pdf->SetXY(95,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

//Do you suffer from any disease? No
$pdf->SetXY(120,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

//Do you suffer from any disease? Frame
$pdf->SetXY(30,75);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,25,'',0, 0, 'C', True);

if($row3['disease'] == 'NULL') {
    //Do you suffer from any disease? Variable
    $pdf->SetXY(32,70);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    if($row3['disease'] == 'no') {
        //Do you suffer from any disease? Variable
        $pdf->SetXY(32,75);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'',0,0,'L');
    } else {
        //Do you suffer from any disease? Variable
        $pdf->SetXY(32,75);
        $pdf->SetFont('Arial','',12);
        $pdf->SetMargins(5, 5);
        $pdf->MultiCell( 160, 7,utf8_decode($row3['disease']), 0);

    }
}

//Are you under treatment or taking any medication? Title
$pdf->SetXY(10,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Are you under treatment or taking any medication?:',0,0,'L');

if($row3['treatment'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',117,105,-1500);
} else {
    if($row3['treatment'] != 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',117,105,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',117,105,-1500);
    }
}

//Are you under treatment or taking any medication? Yes
$pdf->SetXY(125,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['treatment'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',140,105,-1500);
} else {
    if($row3['treatment'] == 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',140,105,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',140,105,-1500);
    }
}

//Are you under treatment or taking any medication? No
$pdf->SetXY(150,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

//Are you under treatment or taking any medication? Frame
$pdf->SetXY(30,115);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,25,'',0, 0, 'C', True);

if($row3['treatment'] == 'NULL') {
    //Are you under treatment or taking any medication? Variable
    $pdf->SetXY(32,95);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    if($row3['treatment'] == 'no') {
        //Are you under treatment or taking any medication? Variable
        $pdf->SetXY(32,115);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'',0,0,'L');
    } else {
        //Are you under treatment or taking any medication? Variable
        $pdf->SetXY(32,115);
        $pdf->SetFont('Arial','',12);
        $pdf->SetMargins(5, 5);
        $pdf->MultiCell( 160, 7,utf8_decode($row3['treatment']), 0);
    }
}

//Are you or were you undergoing psychological or psychiatric treatment? Title
$pdf->SetXY(10,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Are you or were you undergoing psychological or psychiatric treatment?:',0,0,'L');

if($row3['treatment_p'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',160,145,-1500);
} else {
    if($row3['treatment_p'] != 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',160,145,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',160,145,-1500);
    }
}

//Are you or were you undergoing psychological or psychiatric treatment? Yes
$pdf->SetXY(170,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['treatment_p'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,145,-1500);
} else {
    if($row3['treatment_p'] == 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',180,145,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',180,145,-1500);
    }
}


//Are you or were you undergoing psychological or psychiatric treatment? No
$pdf->SetXY(190,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

//Are you or were you undergoing psychological or psychiatric treatment? Frame
$pdf->SetXY(30,155);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,25,'',0, 0, 'C', True);

if($row3['treatment_p'] == 'NULL') {
    //Are you or were you undergoing psychological or psychiatric treatment? Variable
    $pdf->SetXY(32,170);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    if ($row3['treatment_p'] == 'no') {
        //Are you or were you undergoing psychological or psychiatric treatment? Variable
        $pdf->SetXY(32,155);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'',0,0,'L');
    } else {
        //Are you or were you undergoing psychological or psychiatric treatment? Variable
        $pdf->SetXY(32,155);
        $pdf->SetFont('Arial','',12);
        $pdf->SetMargins(5, 5);
        $pdf->MultiCell( 160, 7,utf8_decode($row3['treatment_p']), 0);
    }
}

//Do you suffer from any allergies? Title
$pdf->SetXY(10,190);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Do you suffer from any allergies?:',0,0,'L');

//Do you suffer from any allergies? Yes
$pdf->SetXY(90,190);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['allergies'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',80,185,-1500);
} else {
    if($row3['allergies'] != 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',80,185,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',80,185,-1500);
    }
}

if($row3['allergies'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',100,185,-1500);
} else {
    if($row3['allergies'] == 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',100,185,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',100,185,-1500);
    }
}

//Do you suffer from any allergies? No
$pdf->SetXY(110,190);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');


//Do you suffer from any allergies? Frame
$pdf->SetXY(30,195);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(170,15,'',0, 0, 'C', True);

if($row3['allergies'] == 'NULL') {
    //Are you or were you undergoing psychological or psychiatric treatment? Variable
    $pdf->SetXY(32,195);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    if($row3['allergies'] == 'no') {
        //Are you or were you undergoing psychological or psychiatric treatment? Variable
        $pdf->SetXY(32,195);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'',0,0,'L');
    } else {
        //Are you or were you undergoing psychological or psychiatric treatment? Variable
        $pdf->SetXY(32,195);
        $pdf->SetFont('Arial','',12);
        $pdf->SetMargins(5, 5);
        $pdf->MultiCell( 160, 7,utf8_decode($row3['allergies']), 0);
    }
}

//Have you had any surgery in the 12 months prior to your trip? Title
$pdf->SetXY(10,220);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Have you had any surgery in the 12 months prior to your trip?:',0,0,'L');

if($row3['surgery'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',140,215,-1500);
} else {
    if($row3['surgery'] != 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',140,215,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',140,215,-1500);
    }
}

//Have you had any surgery in the 12 months prior to your trip? Yes
$pdf->SetXY(150,220);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['surgery'] == 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/circle.png',160,215,-1500);
} else {
    if($row3['surgery'] == 'no') {
        //Circle
        $pdf->Image('../../assets/img/Seleccircle.png',160,215,-1500);
    } else {
        //Circle
        $pdf->Image('../../assets/img/circle.png',160,215,-1500);
    }
}

//Have you had any surgery in the 12 months prior to your trip? No
$pdf->SetXY(170,220);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

//Have you had any surgery in the 12 months prior to your trip? Frame
$pdf->SetXY(30,225);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','',12);
$pdf->Cell(170,25,'',0, 0, 'C', True);


if($row3['surgery'] == 'NULL') {
    //Have you had any surgery in the 12 months prior to your trip? Variable
    $pdf->SetXY(32,225);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else { 
    if($row3['surgery'] == 'no') {
        //Have you had any surgery in the 12 months prior to your trip? Variable
        $pdf->SetXY(32,225);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'',0,0,'L');
    } else {
        //Have you had any surgery in the 12 months prior to your trip? Variable
        $pdf->SetXY(32,225);
        $pdf->SetMargins(5, 5);
        $pdf->MultiCell( 160, 7,utf8_decode($row3['surgery']), 0);
    }
    
}

//Do you Smoke? Title
$pdf->SetXY(10,260);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Do you Smoke?:',0,0,'L');

//Do you Smoke? Frame
$pdf->SetXY(45,255);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(40,8,'',0, 0, 'C', True);

if($row3['smoke_s'] == 'NULL') {
    //Do you Smoke? Variable
    $pdf->SetXY(45,260);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No',0,0,'L');
} else {
    //Do you Smoke? Variable
    $pdf->SetXY(45,260);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['smoke_s'],0,0,'L');
}


//Do you Drink Alcohol? Title
$pdf->SetXY(105,260);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Do you Drink Alcohol?:',0,0,'L');

//Do you Drink Alcohol? Frame
$pdf->SetXY(155,255);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(45,8,'',0, 0, 'C', True);

if($row3['drinks_alc'] == 'NULL') {
    //Do you Drink Alcohol? Variable
    $pdf->SetXY(157,260);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No',0,0,'L');
} else {
    if($row3['drinks_alc'] == 'no'){
        //Do you Drink Alcohol? Variable
        $pdf->SetXY(157,260);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'No',0,0,'L');
    } else {
        //Do you Drink Alcohol? Variable
        $pdf->SetXY(157,260);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'Yes',0,0,'L');
    }

}


//Have you used any drugs? Title
$pdf->SetXY(10,270);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Have you used any drugs?:',0,0,'L');

//Have you used any drugs? Frame
$pdf->SetXY(68,265);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(40,8,'',0, 0, 'C', True);

if($row3['drugs'] == 'NULL') {
    //Have you used any drugs? Variable
    $pdf->SetXY(70,270);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No',0,0,'L');
} else {
    if($row3['drugs'] == 'no'){
        //Have you used any drugs? Variable
        $pdf->SetXY(70,270);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'No',0,0,'L');
    } else {
        //Have you used any drugs? Variable
        $pdf->SetXY(70,270);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,0,'Yes',0,0,'L');
    }
}

//Page 3
$pdf->AddPage();

//Frame Up
$pdf->SetXY(0,12);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//Health Information Title
$pdf->SetXY(0,17);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'EMERGENCY CONTACTS',0,0,'C');

//Emergency Contact Title
$pdf->SetXY(10,35);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Name(s):',0,0,'L');

//Emergency Contact Cell
$pdf->SetXY(30,30);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['cont_name'] == 'NULL' && $row3['cont_name']) {
    //Emergency Contact Variable
    $pdf->SetXY(32,35);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Emergency Contact Variable
    $pdf->SetXY(32,35);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($emergencyname),0,0,'L');
}

//Relationship Title
$pdf->SetXY(102,35);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Relationship:',0,0,'L');

//Relationship Cell
$pdf->SetXY(135,30);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['relationship'] == 'NULL') {
    //Relationship Variable
    $pdf->SetXY(140,35);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Relationship Variable
    $pdf->SetXY(140,35);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['relationship']),0,0,'L');
}

//Email Contact Title
$pdf->SetXY(10,50);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Email:',0,0,'L');

//Email Contact Cell
$pdf->SetXY(30,45);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

//Phone number Title
$pdf->SetXY(102,50);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Phone number:',0,0,'L');

//Phone number Cell
$pdf->SetXY(135,45);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['num_conts'] == 'NULL') {
    //Phone number Variable
    $pdf->SetXY(140,50);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Phone number Variable
    $pdf->SetXY(140,50);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['num_conts']),0,0,'L');
}

//Frame Up
$pdf->SetXY(0,60);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//Health Information Title
$pdf->SetXY(0,65);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'PROFESSIONAL INFORMATION',0,0,'C');

//Native language Title
$pdf->SetXY(7,80);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Native Language:',0,0,'L');

//Native language Cell
$pdf->SetXY(45,75);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(155,8,'',0, 0, 'C', True);

if($row3['lang_s'] == 'NULL') {
    //Native language Variable
    $pdf->SetXY(45,80);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Native language Variable
    $pdf->SetXY(45,80);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['lang_s']),0,0,'L');

}

//Other language Title
$pdf->SetXY(7,95);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Others Languages:',0,0,'L');

//Other language Cell
$pdf->SetXY(48,90);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(153,8,'',0, 0, 'C', True);

if($row3['language_a'] == 'NULL') {
    //Other language Variable
    $pdf->SetXY(48,95);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Other language Variable
    $pdf->SetXY(48,95);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['language_a']),0,0,'L');

}

//Current level of English Title
$pdf->SetXY(7,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Current level of English:',0,0,'L');

//Current level of English Beginner
$pdf->SetXY(70,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Beginner',0,0,'L');

if($row3['english_l'] == 'Beginner') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',60,105,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',60,105,-1500);
}


//Current level of English Intermediate
$pdf->SetXY(105,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Intermediate',0,0,'L');

if($row3['english_l'] == 'Intermediate') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',95,105,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',95,105,-1500);
}

//Current level of English Advanced
$pdf->SetXY(145,110);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Advanced',0,0,'L');

if($row3['english_l'] == 'Advanced') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',135,105,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',135,105,-1500);
}



//Current level of studies Cell
$pdf->SetXY(48,120);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(153,8,'',0, 0, 'C', True);

if($row3['type_s'] == 'NULL') {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['type_s']),0,0,'L');

}

//Current level of English Title
$pdf->SetXY(7,125);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Others Languages:',0,0,'L');


//Current level of studies Cell
$pdf->SetXY(48,120);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(153,8,'',0, 0, 'C', True);

if($row3['type_s'] == 'NULL') {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['type_s']),0,0,'L');

}

//Destination City Title
$pdf->SetXY(7,140);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Select the destination you will arrive at:',0,0,'L');

//Destination City Toronto
$pdf->SetXY(100,140);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Toronto',0,0,'L');

if($row3['destination_c'] == 'Toronto') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',90,135,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',90,135,-1500);
}


//Destination City Montreal
$pdf->SetXY(130,140);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Montreal',0,0,'L');

if($row3['destination_c'] == 'Montreal') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',120,135,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',120,135,-1500);
}

//Destination City Ottawa
$pdf->SetXY(165,140);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Ottawa',0,0,'L');

if($row3['destination_c'] == 'Ottawa') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,135,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,135,-1500);
}

//Destination City Quebec
$pdf->SetXY(35,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Quebec',0,0,'L');

if($row3['destination_c'] == 'Quebec') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',25,145,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',25,145,-1500);
}

//Destination City Calgary
$pdf->SetXY(65,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Calgary',0,0,'L');

if($row3['destination_c'] == 'Calgary') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',55,145,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',55,145,-1500);
}

//Destination City Vancouver
$pdf->SetXY(95,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Vancouver',0,0,'L');

if($row3['destination_c'] == 'Vancouver') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',85,145,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',85,145,-1500);
}

//Destination City Victoria
$pdf->SetXY(130,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Victoria',0,0,'L');

if($row3['destination_c'] == 'Victoria') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',120,145,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',120,145,-1500);
}

//Destination City Victoria
$pdf->SetXY(160,150);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Other',0,0,'L');

if($row3['destination_c'] != 'Toronto' && $row3['destination_c'] != 'Montreal' && $row3['destination_c'] != 'Ottawa' && $row3['destination_c'] != 'Quebec' && $row3['destination_c'] != 'Calgary' && $row3['destination_c'] != 'Vancouver' && $row3['destination_c'] != 'Victoria') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',150,145,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',150,145,-1500);
}

//Destination Specify Victoria
$pdf->SetXY(7,160);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Specify:',0,0,'L');

//Destination city Cell
$pdf->SetXY(28,155);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(173,8,'',0, 0, 'C', True);

if($row3['type_s'] == 'NULL') {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Current level of studies Variable
    $pdf->SetXY(48,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['type_s']),0,0,'L');

}

//Contrated time title
$pdf->SetXY(7,175);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Contrated time:',0,0,'L');

//Contrated time cell
$pdf->SetXY(40,170);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(162,8,'',0, 0, 'C', True);

if($row3['firstd'] == 'NULL' && $row3['lastd'] == 'NULL') {
    //Contrated time variable
    $pdf->SetXY(48,175);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Contrated time variable
    $pdf->SetXY(48,175);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($contratedTime),0,0,'L');

}

//Name of school title
$pdf->SetXY(7,190);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Name of School that you will be attending:',0,0,'L');

//Name of school cell
$pdf->SetXY(98,185);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(104,8,'',0, 0, 'C', True);

if($row3['name_a'] == 'NULL') {
    //Name of school variable
    $pdf->SetXY(98,190);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Name of school variable
    $pdf->SetXY(98,190);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['name_a']),0,0,'L');

}

//School Address title
$pdf->SetXY(7,205);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'School Address:',0,0,'L');

//School Address cell
$pdf->SetXY(45,200);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(158,8,'',0, 0, 'C', True);

if($row3['dir_a'] == 'NULL') {
    //School Address variable
    $pdf->SetXY(45,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //School Address variable
    $pdf->SetXY(45,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['dir_a']),0,0,'L');

}

//Star of classes Title
$pdf->SetXY(7,220);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Star of classes:',0,0,'L');

//Star of classes Cell
$pdf->SetXY(40,215);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(60,8,'',0, 0, 'C', True);

if($row3['firstd'] == 'NULL') {
    //Star of classes Variable
    $pdf->SetXY(40,220);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Star of classes Variable
    $pdf->SetXY(40,220);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['firstd']),0,0,'L');
}

//End of classes Title
$pdf->SetXY(102,220);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'End of classes:',0,0,'L');

//End of classes Cell
$pdf->SetXY(135,215);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['lastd'] == 'NULL') {
    //End of classes Variable
    $pdf->SetXY(140,220);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //End of classes Variable
    $pdf->SetXY(140,220);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['lastd']),0,0,'L');
}

//Selected Time title
$pdf->SetXY(7,235);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Selected Time:',0,0,'L');

//Selected Time cell
$pdf->SetXY(45,230);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(158,8,'',0, 0, 'C', True);

if($row3['schedule'] == 'NULL') {
    //Selected Time variable
    $pdf->SetXY(45,235);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Selected Time variable
    $pdf->SetXY(45,235);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['schedule']),0,0,'L');

}

//Page 4
$pdf->AddPage();

//Frame Up
$pdf->SetXY(0,12);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,16,'',0, 0, 'C', True);

//INFORMATION ABOUT YOUR LODGING Title
$pdf->SetXY(0,17);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'INFORMATION ABOUT YOUR LODGING',0,0,'C');

$pdf->SetXY(0,23);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'(Accommodation with family Available all year).',0,0,'C');

//Arrival Date Title
$pdf->SetXY(10,37);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Arrival Date:',0,0,'L');

//Arrival Date Cell
$pdf->SetXY(37,32);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(60,8,'',0, 0, 'C', True);

if($row3['arrive_g'] == 'NULL') {
    //Arrival Date Variable
    $pdf->SetXY(40,37);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Arrival Date Variable
    $pdf->SetXY(40,37);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['arrive_g']),0,0,'L');
}

//Departure Date Title
$pdf->SetXY(102,37);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Departure Date:',0,0,'L');


//Departure Date Cell
$pdf->SetXY(135,32);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65,8,'',0, 0, 'C', True);

if($row3['lastd'] == 'NULL') {
    //Departure Date Variable
    $pdf->SetXY(140,37);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Departure Date Variable
    $pdf->SetXY(140,37);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['lastd']),0,0,'L');
}

//Duration of accommodation title
$pdf->SetXY(7,47);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Duration of accommodation (total number of nights):',0,0,'L');

//Duration of accommodation cell
$pdf->SetXY(120,43);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(80,8,'',0, 0, 'C', True);

if($row3['arrive_g'] == 'NULL' && $row3['lastd'] == 'NULL') {
    //Duration of accommodation variable
    $pdf->SetXY(121,47);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Duration of accommodation variable
    $pdf->SetXY(121,47);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($durationAccomadion),0,0,'L');

}

//Accommodation Placement Fee title
$pdf->SetXY(7,57);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Accommodation Placement Fee:',0,0,'L');

//Accommodation Placement Fee cell
$pdf->SetXY(75,53);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(125,8,'',0, 0, 'C', True);

if($row3['booking_fee'] == 'NULL') {
    //Accommodation Placement Fee variable
    $pdf->SetXY(80,57);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'',0,0,'L');
} else {
    //Accommodation Placement Fee variable
    $pdf->SetXY(80,57);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,utf8_decode($row3['booking_fee']),0,0,'L');

}


$pdf->SetXY(0,67);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,0,'Next, choose the option that best suits your needs:',0,0,'C');

//AGREGA LAS CONDICIONALES PARA QUE FUNCIONE ESTA PARTE
//Room Option
if($row3['lodging_type'] != 'NULL') {
    $pdf->SetXY(20,75);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(0,0,$row3['lodging_type'].' Room',0,0,'L');
}

if($row3['lodging_type'] != 'NULL') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',10,70,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',10,70,-1500);
}

if($row3['meal_p'] != 'NULL'){
    $pdf->SetXY(60,75);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['meal_p'].' included (breakfast, lunch, dinner)',0,0,'L');
}

if($row3['a_price'] != 'NULL'){
    $pdf->SetXY(145,75);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(0,0,$prices_week.' CAD per week',0,0,'L');
}

//* Prices are in Canadian dollars and per studen
$pdf->SetXY(7,85);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'* Prices are in Canadian dollars and per student',0,0,'L');

//** Shared room options available only with special condition (friends, siblings) prices per person.
$pdf->SetXY(7,90);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'** Shared room options available only with special condition (friends, siblings) prices per person.',0,0,'L');

//Frame Up
$pdf->SetXY(0,95);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//FAMILY PREFERENCES Title
$pdf->SetXY(0,100);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'FAMILY PREFERENCES',0,0,'C');

//Animal Allergies Title
$pdf->SetXY(10,115);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'Animal Allergies:',0,0,'L');

//Animal Allergies No
$pdf->SetXY(55,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['allergy_a'] == 'No') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',45,110,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',45,110,-1500);
}

//Animal Allergies Yes
$pdf->SetXY(80,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['allergy_a'] != 'No') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',70,110,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',70,110,-1500);
}

$pdf->SetXY(100,115);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Which?',0,0,'L');

//Animal Allergies Cell
$pdf->SetXY(120,110);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(82,8,'',0, 0, 'C', True);

if($row3['allergy_a'] != 'No') {
    $pdf->SetXY(122,115);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['allergy_a'],0,0,'L');
} else {

}

//Allergies Title
$pdf->SetXY(10,125);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'Enviromental Allergies:',0,0,'L');

//Allergies No
$pdf->SetXY(65,125);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['allergy_m'] == 'No') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',55,120,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',55,120,-1500);
}

//Allergies Yes
$pdf->SetXY(85,125);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['allergy_m'] != 'No') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',75,120,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',75,120,-1500);
}

$pdf->SetXY(100,125);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Which?',0,0,'L');

//Animal Allergies Cell
$pdf->SetXY(120,121);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(82,8,'',0, 0, 'C', True);

if($row3['allergy_m'] != 'No') {
    $pdf->SetXY(122,125);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,$row3['allergy_m'],0,0,'L');
} else {

}

//table
$pdf->SetFont('Arial','B',12);
$pdf->SetXY(10,135);
$width_cell=array(120,40,0,0);
$pdf->SetFillColor(217,217,217);
$pdf->SetTextColor(0,0,0); // Background color of header 


// First row of data 
$pdf->Cell($width_cell[0],10,'SELECT THE OPTION OF YOUR PREFERENCE',1,0,'L', false); // First column of row 1 
$pdf->Cell($width_cell[1],10,'Yes',1,0,'C',false); // Second column of row 1 
$pdf->Cell($width_cell[2],10,'No',1,0,'C',false); // Third column of row 1 
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // Fourth column of row 1 
//  First row of data is over 

//  Third row of data
$pdf->SetX(10);
$pdf->Cell($width_cell[0],10,'Can i live with smokers',1,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

//Smokers Yes
if($row3['smoke_l'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',145,146,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',145,146,-1600);
}

//Smokers No
if($row3['smoke_l'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',184,146,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',184,146,-1600);
}

$pdf->SetX(10);
$pdf->Cell($width_cell[0],10,'Can i live with kids',1,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

//Kids Yes
if($row3['children'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',145,156,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',145,156,-1600);
}

//Kids No
if($row3['children'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',184,156,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',184,156,-1600);
}

$pdf->SetX(10);
$pdf->Cell($width_cell[0],10,'Can i live with teenagers',1,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,' ',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

//Teenagers Yes
if($row3['teenagers'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',145,166,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',145,166,-1600);
}

//Teenagers No
if($row3['teenagers'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',184,166,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',184,166,-1600);
}

$pdf->SetX(10);
$pdf->Cell($width_cell[0],10,'Can i live with pets',1,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,'',1,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[2],10,'',1,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[3],10,'',1,1,'C',false); // fourth column of row 3

//Pets Yes
if($row3['pets'] == 'Yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',145,176,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',145,176,-1600);
}

//Pets No
if($row3['pets'] != 'Yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',184,176,-1600);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',184,176,-1600);
}

$pdf->Cell($width_cell[1],10,'',0,0,'L',false); // First column of row 3
$pdf->Cell($width_cell[1],10,'',0,0,'C',false); // Second column of row 3
$pdf->Cell($width_cell[1],10,'',0,0,'C',false); // Third column of row 3
$pdf->Cell($width_cell[1],10,'',0,1,'C',false); // fourth column of row 3


//Special diet Title
$pdf->SetXY(10,195);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'I require a special diet:',0,0,'L');

//Special diet Vegetarian
$pdf->SetXY(65,195);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Vegan free',0,0,'L');

if($row3['vegetarians'] != 'no') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',55,190,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',55,190,-1500);
}

//Special diet Gluten
$pdf->SetXY(105,195);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Gluten',0,0,'L');

if($row3['gluten'] != 'no') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',95,190,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',95,190,-1500);
}

//Special diet Specific
$pdf->SetXY(135,195);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Specific',0,0,'L');

if($row3['halal'] != 'no' || $row3['kosher'] != 'no' || $row3['lactose'] != 'no' || $row3['pork'] != 'no') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',125,190,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',125,190,-1500);
}

//Special diet Title
$pdf->SetXY(10,205);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'Details?',0,0,'L');

//Animal Allergies Cell
$pdf->SetXY(27,201);
$pdf->SetFillColor(250,245,250,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(176,8,'',0, 0, 'C', True);

if($row3['halal'] != 'no') {
    $pdf->SetXY(28,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'Halal',0,0,'L');
} else {

}

if($row3['kosher'] != 'no') {
    $pdf->SetXY(28,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'Kosher',0,0,'L');
} else {

}

if($row3['lactose'] != 'no') {
    $pdf->SetXY(28,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'Lactose Intolerant',0,0,'L');
} else {

}

if($row3['pork'] != 'no') {
    $pdf->SetXY(28,205);
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,0,'No Pork',0,0,'L');
} else {

}

//Special Diet Terms
$pdf->SetXY(10,220);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 5,utf8_decode('iHomestay will take into account your preferences; only restrictions and allergies will be guaranteed. If your preferences are not indica-ted, iHomestay will select the best family for the student, which may include pets and children. *A supplement applies of $16 CAD per day, by requiring a special diet.'), 0);


//Page 5
$pdf->AddPage();

//Frame Up
$pdf->SetXY(0,12);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//OPTIONAL SUPPLEMENTS Title
$pdf->SetXY(0,17);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'OPTIONAL SUPPLEMENTS',0,0,'C');

//Supplement for accommodation near the school (up to 30 minutes walking) Title
$pdf->SetXY(10,30);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'Supplement for accommodation near the school (up to 30 minutes walking):',0,0,'L');

//Supplement for accommodation near the school (up to 30 minutes walking) No
$pdf->SetXY(165,30);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['a_near'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,25,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,25,-1500);
}

//Supplement for accommodation near the school (up to 30 minutes walking) Yes
$pdf->SetXY(190,30);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['a_near'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',180,25,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,25,-1500);
}

//Supplement for accommodation near the school (up to 30 minutes walking) Price
$pdf->SetXY(10,38);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'Additional $35 CAD per night applies.',0,0,'L');

//TRANSPORTATION Title
$pdf->SetXY(10,50);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'TRANSPORTATION',0,0,'L');

//Reception at the airport and transfer to accommodation (Pick Up) Title
$pdf->SetXY(10,60);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'Reception at the airport and transfer to accommodation (Pick Up):',0,0,'L');

//Reception at the airport and transfer to accommodation (Pick Up) No
$pdf->SetXY(165,60);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['pick_up'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,55,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,55,-1500);
}

//Reception at the airport and transfer to accommodation (Pick Up) Yes
$pdf->SetXY(190,60);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['pick_up'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',180,55,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,55,-1500);
}

//Reception at the airport and transfer to accommodation (Pick Up) Title
$pdf->SetXY(10,70);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'Transfer from the accommodation to the airport (Drop off):',0,0,'L');

//Reception at the airport and transfer to accommodation (Pick Up) No
$pdf->SetXY(165,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['drop_off'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,65,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,65,-1500);
}

//Reception at the airport and transfer to accommodation (Pick Up) Yes
$pdf->SetXY(190,70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['drop_off'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',180,65,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,65,-1500);
}

//Reception at the airport and transfer to accommodation (Pick Up) Title
$pdf->SetXY(10,75);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,'Each service has a cost between $145 - $280 CAD depending on the distance',0,0,'L');

//Urgent accommodation Title
$pdf->SetXY(10,85);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'Urgent accommodation search (Less than 1 week) $150:',0,0,'L');

//Urgent accommodation No
$pdf->SetXY(165,85);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['a_urgent'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,80,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,80,-1500);
}

//Urgent accommodation Yes
$pdf->SetXY(190,85);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['a_urgent'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',180,80,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,80,-1500);
}

//Urgent accommodation Title
$pdf->SetXY(10,95);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'Guardianship $550:',0,0,'L');

//Urgent accommodation No
$pdf->SetXY(165,95);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'No',0,0,'L');

if($row3['a_guardianship'] != 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',155,90,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',155,90,-1500);
}

//Urgent accommodation Yes
$pdf->SetXY(190,95);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,'Yes',0,0,'L');

if($row3['a_guardianship'] == 'yes') {
    //Circle
    $pdf->Image('../../assets/img/Seleccircle.png',180,90,-1500);
} else {
    //Circle
    $pdf->Image('../../assets/img/circle.png',180,90,-1500);
}

//Urgent accommodation Terms
$pdf->SetXY(10,100);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 120, 5,'* Guardianship is a practical and moral support system for young students studying in a foreign country, far from their parents.', 0);

//Frame Up
$pdf->SetXY(0,115);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,5,'',0, 0, 'C', True);

//Family and location Title
$pdf->SetXY(10,125);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'FAMILY AND LOCATION',0,0,'L');

//Family and location Terms
$pdf->SetXY(10,130);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 4,'Student considers that the country has a different culture and the customs of your host family will be different from yours. Your host family may be native, have a foreign background, or come from a different Parts than the one you are traveling to. Keep in mind that the average distance from the accommodation to the school can vary between 45 to 90 minutes using public transport. (this will depend on your destination) It is also likely that other students live in the same house and/or that the family is made up of more than 1 member. When you arrive it is important that if any indication of the host family is not clear to you, you ask again until it is clear. remember that they have a different way of thinking than your country. Finally you should know that if during your stay you generate any damage or prejudice it will be your obligation to pay or fix it.', 0);

//Frame Up
$pdf->SetXY(0,160);
$pdf->SetFillColor(232,211,233,255);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(210,10,'',0, 0, 'C', True);

//OPTIONAL SUPPLEMENTS Title
$pdf->SetXY(0,165);
$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,0,'IMPORTANT POINTS TO CONSIDER DURING YOUR STAY',0,0,'C');

//FOOD Title
$pdf->SetXY(10,175);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'FOOD',0,0,'L');

//FOOD Terms
$pdf->SetXY(10,180);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 4,'You will receive 1,2 or 3 meals a day for 5 or 7 days a week (depending on the package you chose). You will have to coordinate with the host family the time of the meal (dinner) to avoid misunderstandings. In case there are days when you prefer to dine out, it is important that you know that there is no monetary refund for missed meals. Breakfast and lunch are normally prepared by the student; however, there are times when the family can prepare breakfast. Prior authorization from the family is required to use the kitchen. Sometimes your host family is not at home and you will have to cook on your own (you will have to coordinate with the family). If you require any special food (vegetarian, vegan, etc.) there will be an additional charge because it is necessary to discuss it with your iHomestay advisor before registering.', 0);

//Internet Title
$pdf->SetXY(10,215);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'INTERNET AND CELLPHONE',0,0,'L');

//Internet Terms
$pdf->SetXY(10,217);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 4,'It is important that before your trip you consider talking to your telephone provider about your international plan. the internet is included in the cost of accommodation, which is expected to be used responsibly by students.', 0);

//KEYS AND SECURITY Title
$pdf->SetXY(10,231);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'KEYS AND SECURITY',0,0,'L');

//KEYS AND SECURITY Terms
$pdf->SetXY(10,234);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 4,'It is important that before your trip you consider talking to your telephone provider about your international plan. the internet is included in the cost of accommodation, which is expected to be used responsibly by students.', 0);

//SHOWER Title
$pdf->SetXY(10,247);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,0,'SHOWER AND LAUNDRY',0,0,'L');

//KEYS AND SECURITY Terms
$pdf->SetXY(10,250);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 4,'You can take 1 shower per day 10-15 minutes. Keep the bathroom clean and tidy out of respect for the family and other inhabitants who use it, possibly you will share a bathroom with a family member or other students. Remember you are responsible for your personal bathroom items; It is recommended that you bring your own towel. As for the laundry, you will have to coordinate the logistics with your host family. as well as the days and the washing schedule.', 0);

//Page 6
$pdf->AddPage();

//Bedroom Title
$pdf->SetXY(7,15);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,0,'BEDROOM',0,0,'L');

$pdf->SetXY(7,25);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0,Tag.' Host family will provide you with bedding',0,0,'L');

$pdf->SetXY(7,35);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0,Tag.' Your room will be private or shared depending on what you chose',0,0,'L');

$pdf->SetXY(7,45);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0,Tag.' Keep your room tidy, clean and undamased',0,0,'L');

$pdf->SetXY(7,55);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0,Tag.' Forbidden to have guests in your room without permission',0,0,'L');

$pdf->SetXY(7,65);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0,Tag.' In case your need more space for your clothes talk with the Host Family',0,0,'L');

$pdf->SetXY(7,70);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,Tag.' Your room can be located on the first or second floor even in the basement, this depends directly on the construction of the house', 0);

$pdf->SetXY(7,90);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,0,'ADDITIONAL IMPORTANT INFORMATION',0,0,'L');

$pdf->SetXY(7,100);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'1. It is not allowed to bring guests to the house unless authorized by the host family', 0);

$pdf->SetXY(7,107);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'2.', 0);

$pdf->SetXY(11,108);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 5,'If you do not wish to continue your accommodation, you must notify iHomestay Canada (info@ihomestaycanada.com) by email with the reasons for the change at least 2-3 weeks before the date of the change.', 0);

$pdf->SetXY(7,119);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'3. ', 0);

$pdf->SetXY(11,120);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 5,'You will have 15 days, from your arrival, not to generate any additional charge (search fee and/or transfer) if a change is required. iHomestay Canada will evaluate the reasons why you request the change. Sometimes you will be asked to go directly to the accommodation department of the educational institution iHomestay Canada relies on overseas operators. The specifications and requirements of the accommodation may not be similar to your request since iHomestay Canada depends on the availability of the providers and service providers with which it works.', 0);

$pdf->SetXY(7,145);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'4. ', 0);

$pdf->SetXY(11,146);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 5,'The selection of Homestay is subject to various factors, such as the season of your trip, your preferences, the school of your choice, the time between your enrollment and the start of your program, among others.
Therefore, keep in mind that we will always do our best to select the most suitable family for you; based on all the data provided and your preferences. However, we cannot guarantee that the family will be 100% compatible with your requirements', 0);

$pdf->SetXY(7,175);
$pdf->SetFont('Arial','B',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'AUTORIZATION', 0);

$pdf->SetXY(7,182);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 5,'I authorize iHomestay to use photographs and videos of me for promotional purposes. By signing and submitting this form I accept the "iHomestay Terms, Conditions and Policies"', 0);

$pdf->SetXY(7,200);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'Signature Student:   _________________________________________', 0);

if ($row3['signature_s'] != 'NULL') {
    $pdf->Image($photo_signature,50,195,-400);
}

$pdf->SetXY(7,207);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'(Attach a valid official identification (INE, military card, passport, ID)', 0);

$pdf->SetXY(7,220);
$pdf->SetFont('Arial','',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'Signature of father or tuthor, in case if the student is minor:   _________________________________________', 0);

$pdf->SetXY(7,227);
$pdf->SetFont('Arial','',9);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'(Attach a valid official identification (INE, military card, passport, ID)', 0);

$pdf->SetXY(7,240);
$pdf->SetFont('Arial','B',10);
$pdf->SetMargins(5, 5);
$pdf->MultiCell( 190, 7,'APPLICATION DATE', 0);

$pdf->SetXY(0,250);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,0,'NOTE:',0,0,'C');

$pdf->SetXY(0,254);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'- Sending this completed form by email will be recorded as a digital signature',0,0,'C');

$pdf->SetXY(0,260);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'- Respect Host Family house rules and directions in regard to use facilities, aplicancio, etc.',0,0,'C');

$pdf->SetXY(0,265);
$pdf->SetFont('Arial','',12);
$pdf->Cell(0,0,'- Respect the Family privay, person propety and individual rights.',0,0,'C');







$pdf->Output();
?>