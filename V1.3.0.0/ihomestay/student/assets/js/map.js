const idHome = document.getElementById("id-home").value;
const homeName = document.getElementById("home-name").value;
const homePhoto = document.getElementById("home-photo").value;

const dir_a = document.getElementById("dir-a").value;
const city_a = document.getElementById("city-a").value;
const state_a = document.getElementById("state-a").value;
const postal_code_a = document.getElementById("postal-code-a").value;
const name_a = document.getElementById("name-a").value;
const photo_a = document.getElementById("photo-a").value;

const dir = document.getElementById("dir").value;
const city = document.getElementById("city").value;
const state = document.getElementById("state").value;
const postal_code = document.getElementById("postal-code").value;

mapboxgl.accessToken = "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg";

var client = new MapboxClient(mapboxgl.accessToken);

var address = `${dir_a}, ${city_a}, ${state_a}, ${postal_code_a}`;
var test = client.geocodeForward(address, (err, data, res) => {
  // data is the geocoding result as parsed JSON
  // res is the http response, including: status, headers and entity properties

  var coordinates = data.features[0].center;

  //Este bloque define el centro de tu mapa todas las direcciones deben tenerla de la misma manera y zoom o no encontrara el centro

  var map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v10",
    center: coordinates,
    zoom: 8,
  });

  //Para marcar varios puntos debes copiar el bloque de busqueda como tenemos abajo y es muy importante pegarlo antes de cerrar el primer parentesis debajo del addto(map), ademas el bloque completo de new mapboxgl.marker debe estar en la ultima consulta es decir el ultimo bloque interno sino la consulta fallara, las variables se deben subir al numero inmediato superior

  var address9 = `${dir} ${city} ${state} ${postal_code}`;
  var test = client.geocodeForward(address9, (err, data, res) => {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, headers and entity properties

    var coordinates9 = data.features[0].center;

    var map = new mapboxgl.Map({
      container: "map",
      style: "mapbox://styles/mapbox/streets-v10",
      center: coordinates,
      zoom: 10,
    });

    // Cantidad de Coordenadas
    var popup9 = new mapboxgl.Popup({
      offset: 25,
      closeOnClick: false,
    }).setHTML(
      `<strong>Name: </strong>
          <a href="detail-acepted.php?art_id=${idHome}">${homeName}</a>.<br>
          <strong>Direction: </strong>
          <a href="detail-acepted.php?art_id=${idHome}">${dir}</a>.<br>
          <a href="detail-acepted.php?art_id=${idHome}">
            <img src="../../${homePhoto}" style="width: 100px; height: 100px;">
          </a>`
    );
    // create DOM element for the marker
    var ei = document.createElement("div");
    ei.id = "house";
    //lo superior es el popup

    new mapboxgl.Marker(ei).setLngLat(coordinates9).setPopup(popup9).addTo(map);

    var directions = new MapboxDirections({
      accessToken: "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
      unit: "metric",
    });

    map.addControl(directions, "top-right");

    map.on("load", () => {
      directions.setOrigin(address9); // can be address in form setOrigin("12, Elm Street, NY")
      directions.setDestination(address); // can be address
    });

    //PRIMERA CONSULTA CON EL POPUP AGREGADO
    var popup = new mapboxgl.Popup({
      offset: 25,
    }).setHTML(
      `<strong>Name: </strong>${name_a}.<br>
          <strong>Direction: </strong>${dir_a}.<br>
          <img src="../../${photo_a}" width="100%" height="100%">`
    );
    // create DOM element for the marker
    var el = document.createElement("div");
    el.id = "marker";
    //lo superior es el popup

    new mapboxgl.Marker(el) // marker en el mapa
      .setLngLat(coordinates) // marker
      .setPopup(popup) // pertenece a la funcion del popup
      .addTo(map); // marker
  });
});
