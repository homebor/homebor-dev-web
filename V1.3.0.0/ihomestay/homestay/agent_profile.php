<?php
include '../xeon.php';
session_start();
error_reporting(0);

// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$sql = "SELECT mail FROM users WHERE mail = '$usuario'";

$query="SELECT * FROM agents WHERE a_mail = '$usuario'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

    $query3="SELECT * FROM manager INNER JOIN agents ON agents.a_mail = '$usuario' and manager.id_m = agents.id_m";
    $resultado3=$link->query($query3);

    $row3=$resultado3->fetch_assoc(); 

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">
     <!--Mapbox Links-->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Agent Profile</title>
    <style type="text/css">
        #map {width: 320px; height: 300px;}
         @media screen and (max-width: 600px) { 

        #map {margin-left: 20px; width: 280px; height: 150px;}

          @media screen and (min-width: 640px) { 

        /*REPONSIVE LOCATION*/
        #map{height: 270px;}

         /* MEDIA MEDIUM DEVICE*/
    @media screen and (min-width: 768px) { 

        #map{height: 270px; width: 300px;}

         /* MEDIA LARGE DEVICE DESKTOP AND LAPTOPS*/
    @media screen and (min-width: 800px) { 


        /*REPONSIVE LOCATION*/
        #map{height: 300px; width: 350px;}
    </style>

</head>
<body style="background-color: #F3F8FC;">

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER ===============================================================-->
<?php 
    include 'header.php';
?>
    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main" style="background-color: #F3F8FC;">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <h1>Agent Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
        <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

        <form id="form-submit" class="ts-form" autocomplete="off" action="action.php" method="post" enctype="multipart/form-data">

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Basic Information</h3>

                                <div class="text-center">
                                     <?php
                                    
                                        if($row['photo_s'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo'].'" width="200px;" height="200" style="margin-bottom: 20px;"/>';}
                                 
                                    ?>
                                </div>

                                 <div class="row">

                                    <!--Name-->
                                            <?php
                                            if($row['name'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agent Name </label>
                                        <p>".$row['name']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Last Name-->
                                   <?php
                                            if($row['l_name'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agent Last Name</label>
                                        <p>".$row['l_name']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Last Name-->
                                    <?php
                                            if($row['mail'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agent Mail</label>
                                        <p>".$row['a_mail']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                            </section>
                            <!--end #location-->

                             <!--LOCATION
                            =====================================================================================-->
                        <section id="location" class="mb-5">

                            <!--Title-->
                            <h3 class="text-muted border-bottom">Location</h3>

                            <div class="row">

                                <div class="col-sm-6">

                                    <!--Address-->
                                    <div class="input-group"><?php
                                            if($row['country'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>Country</label>
                                        <p>".$row['country']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                    <!--City-->
                                    <div class="form-group">
                                        <?php
                                            if($row['city'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>City</label>
                                        <p>".$row['city']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                    <!--State-->
                                    <div class="form-group">
                                        <?php
                                            if($row['state'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>State</label>
                                        <p>".$row['state']."</p>";
                                            
                                    }
                                    ?>
                                    </div>

                                </div>
                                <!--end col-md-6-->

                                <!--Map-->
                                <div id='map'></div>
                                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address ="<?php echo "$row[dir], $row[city], $row[state] $row[p_code]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 14
                                    });
                                    new mapboxgl.Marker( )
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
  </script>
                                <!--end col-md-6-->

                            </div>
                            <!--end row-->
                        </section>
                        <!--end #location-->

  <!--Academy Info
                                =====================================================================================-->
                            <section id="Academy_info" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Agency Information</h3>

                                <section id="academy-basic-info" class="mb-5">

                                <!--Row-->
                                <div class="row">

                                    <!--Academy Information-->
                                    <?php
                                            if($row3['a_name'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agency Name</label>
                                        <p>".$row3['a_name']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Origin Language-->
                                    <?php
                                            if($row['num'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agent Contact</label>
                                        <p>".$row['num']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Type Student-->
                                    <?php
                                            if($row['num2'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Agent Alternative Number</label>
                                        <p>".$row['num2']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <?php
                                            if($row['experience'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Years of Experience</label>
                                        <p>".$row['experience']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <?php
                                            if($row['language'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Language Spoken</label>
                                        <p>".$row['language']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <?php
                                            if($row['specialization'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Specialization</label>
                                        <p>".$row['specialization']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>



                                </div>
                                <!--end row-->
                                <!--end row-->
                            </section>

                            <section>
                                <?php
                                            if($row['des'] == "NULL"){
                                            }
                                        else {
                                            echo "<label>Description</label>
                                        <p>".$row['des']."</p>";
                                            
                                    }
                                    ?>

                            </section>

                             <section id="Academy_info" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Reasons to Work with Me</h3>

                                <section id="academy-basic-info" class="mb-5">

                                <!--Row-->
                                <div class="row">

                                    <!--Academy Information-->
                                    <?php
                                            if($row3['reason1'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Reason 1</label>
                                        <p>".$row3['reason1']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Origin Language-->
                                    <?php
                                            if($row['reason2'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Reason 2</label>
                                        <p>".$row['reason2']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                                    <!--Type Student-->
                                    <?php
                                            if($row['reason3'] == "NULL"){
                                            }
                                        else {
                                            echo "<div class='col-sm-4'>
                                        <div class='form-group'>
                                            <label>Reason 3</label>
                                        <p>".$row['reason3']."</p>
                                        </div>
                                    </div>";
                                            
                                    }
                                    ?>

                            
                                </div>
                                <!--end row-->
                                <!--end row-->
                            </section>
                            <!--end #location-->
                            <!--
                            <hr>
                            
                            <section class="py-3">
                                <button type="submit" id="submit" name="register" onclick="window.location=''" class="btn btn-primary ts-btn-arrow btn-lg float-right" style="color: white; background-color: #232159; border: 2px solid #232159;">
                                   <i class="fa fa-save mr-2"></i> Submit
                                </button>
                            </section>-->

                           </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>


</body>
</html>