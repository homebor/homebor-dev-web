<?php 

include '../../xeon.php';
session_start();

// TODO VARIABLE USER'S
$usuario = $_SESSION['username'];

// TODO NOTIFICATION ID
$id_not = $_POST['id_not'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');
$dateM = date('Y-m-d');

$homestayQuery = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario'");
$row_homestay = $homestayQuery->fetch_assoc();

// TODO INSERT / UPDATE 

  if($_POST['btn'] == 'reject') {
    // ? REJECT RESERVATION

    $notificationQuery = $link->query("SELECT * FROM notification WHERE id_not = '$id_not' AND user_r = '$usuario'");
    $row_notification = $notificationQuery->fetch_assoc();

    //* UPDATE NOTIFICATION
    $updateNotification = $link->query("UPDATE notification SET confirmed = '1', state = '1', status = 'Rejected' WHERE id_not = '$id_not'");

    //* UPDATE STATUS STUDENT'S
    $updateStatusStudent = $link->query("UPDATE pe_student SET status = 'Search for Homestay' WHERE mail_s = '$row_notification[user_i_mail]'");

    //* INSERT TO NOTI STUDENT
    $saveNotificationStudent = $link->query("INSERT INTO noti_student( h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, des) values ('$row_homestay[h_name]', '$row_homestay[name_h]', '$row_homestay[l_name_h]', '$usuario', '$row_notification[user_i_mail]', '$date', '0', '0', 'Rejected')");

    if($saveNotificationStudent) $response = "true";
    else $response = 'false';
  }

  else if($_POST['btn'] == 'confirm'){
    // ? CONFIRM RESERVATION
  
    $notificationQuery = $link->query("SELECT * FROM notification WHERE id_not = '$id_not' AND user_r = '$usuario'");
    $row_notification = $notificationQuery->fetch_assoc();

    $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_notification[user_i_mail]'");
    $row_student = $studentQuery->fetch_assoc();

    //* UPDATE NOTIFICATION RESERVE
    $updateNotification = $link->query("UPDATE notification SET confirmed = '1', state = '1' WHERE id_not = '$id_not'");

    //* UPDATE STATUS STUDENT'S
    $updateStatusStudent = $link->query("UPDATE pe_student SET status = 'Homestay Found' WHERE mail_s = '$row_notification[user_i_mail]'");

    //* INSERT TO WEBMASTER
    $saveWebmaster = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user) VALUES ('$usuario', 'Confirm Student', '$date', '$row_notification[user_i_mail]')");

    //* INSERT TO NOTI STUDENT
    $saveNotificationStudent = $link->query("INSERT INTO noti_student( h_name, user_i, user_i_l, user_i_mail, user_r, date_, state, confirmed, des) values ('$row_homestay[h_name]', '$row_homestay[name_h]', '$row_homestay[l_name_h]', '$usuario', '$row_notification[user_i_mail]', '$date', '0', '0', 'Student Confirmed')");

    //* QUERY ROOMS
    $roomHouse= $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and room.id_home = pe_home.id_home");
		$row_room =$roomHouse->fetch_assoc();

    //* BEDS RESERVATIONS
    if($row_notification['color'] == '#232159'){
      $room = '1';
      if($row_notification['des'] == 'A') $availability = 'date1';
      else if($row_notification['des'] == 'B') $availability = 'date1_2';
      else if($row_notification['des'] == 'C') $availability = 'date1_3';
      $reservation = 'reservations1';
      if($row_room['reservations1'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations1'] + 1;
    }else if($row_notification['color'] == '#982A72'){
      $room = '2';
      if($row_notification['des'] == 'A') $availability = 'date2';
      else if($row_notification['des'] == 'B') $availability = 'date2_2';
      else if($row_notification['des'] == 'C') $availability = 'date2_3';
      $reservation = 'reservations2';
      if($row_room['reservations2'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations2'] + 1;
    }else if($row_notification['color'] == '#394893'){
      $room = '3';
      if($row_notification['des'] == 'A') $availability = 'date3';
      else if($row_notification['des'] == 'B') $availability = 'date3_2';
      else if($row_notification['des'] == 'C') $availability = 'date3_3';
      $reservation = 'reservations3';
      if($row_room['reservations3'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations3'] + 1;
    }else if($row_notification['color'] == '#A54483'){
      $room = '4';
      if($row_notification['des'] == 'A') $availability = 'date4';
      else if($row_notification['des'] == 'B') $availability = 'date4_2';
      else if($row_notification['des'] == 'C') $availability = 'date4_3';
      $reservation = 'reservations4';
      if($row_room['reservations4'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations4'] + 1;
    }else if($row_notification['color'] == '#5D418D'){
      $room = '5';
      if($row_notification['des'] == 'A') $availability = 'date5';
      else if($row_notification['des'] == 'B') $availability = 'date5_2';
      else if($row_notification['des'] == 'C') $availability = 'date5_3';
      $reservation = 'reservations5';
      if($row_room['reservations5'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations5'] + 1;
    }else if($row_notification['color'] == '#392B84'){
      $room = '6';
      if($row_notification['des'] == 'A') $availability = 'date6';
      else if($row_notification['des'] == 'B') $availability = 'date6_2';
      else if($row_notification['des'] == 'C') $availability = 'date6_3';
      $reservation = 'reservations6';
      if($row_room['reservations6'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations6'] + 1;
    }else if($row_notification['color'] == '#B15391'){
      $room = '7';
      if($row_notification['des'] == 'A') $availability = 'date7';
      else if($row_notification['des'] == 'B') $availability = 'date7_2';
      else if($row_notification['des'] == 'C') $availability = 'date7_3';
      $reservation = 'reservations7';
      if($row_room['reservations7'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations7'] + 1;
    }else if($row_notification['color'] == '#4F177D'){
      $room = '8';
      if($row_notification['des'] == 'A') $availability = 'date8';
      else if($row_notification['des'] == 'B') $availability = 'date8_2';
      else if($row_notification['des'] == 'C') $availability = 'date8_3';
      $reservation = 'reservations8';
      if($row_room['reservations8'] == '0') $num_reserve = 0 + 1;
      else $num_reserve = $row_room['reservations8'] + 1;
    }

    //* UPDATE STATUS ROOM'S AND RESERVATIONS
    if($row_notification['end_'] > $date){
      $updateRoom = $link->query("UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$usuario' SET $availability= 'Occupied', $reservation = '$num_reserve'");
    }

    //* INSERT EVENTS
    if($row_notification['end_'] > $date){
      $insertEvents = $link->query("INSERT INTO events(title, start, startingDay, end, endingDay, color, room_e, bed, email, mail_s, id_m, status) values ('$row_student[name_s] $row_student[l_name_s]', '$row_notification[start]', 'true', '$row_notification[end_]', 'true', '$row_notification[color]', 'room$room', '$row_notification[des]', '$usuario', '$row_notification[user_i_mail]', '$row_homestay[id_m]', 'Active')");
		}else{
      $insertEvents = $link->query("INSERT INTO events(title, start, startingDay, end, endingDay, color, room_e, bed, email, mail_s, id_m, status) values ('$row_student[name_s] $row_student[l_name_s]', '$row_notification[start]', 'true', '$row_notification[end_]', 'true', '$row_notification[color]', 'room$room', '$row_notification[des]', '$usuario', '$row_notification[user_i_mail]', '$row_homestay[id_m]', 'Disabled')");
		}

    //! --------------------------------- THIS SECTION HAS NOT YET BEEN EDITED ---------------------------------------

    //* PAYMENTS

			$query4= $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario'");
			$row4=$query4->fetch_assoc();
			
			$query5= $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_notification[user_i_mail]'");
			$row5=$query5->fetch_assoc();
			
			$query6= $link->query("SELECT * FROM manager WHERE id_m = '$row5[id_m]'");
			$row6=$query6->fetch_assoc();

			$query7= $link->query("SELECT * FROM events WHERE email = '$usuario'");
			$row7=$query7->fetch_assoc();

			$query8= $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and room.id_home = pe_home.id_home");
			$row8=$query8->fetch_assoc();

			if($row7['room_e'] == 'room1') $aproxt = $row8['aprox1'] + $row8['aprox_a1'];
			else if($row7['room_e'] == 'room2') $aproxt = $row8['aprox2'] + $row8['aprox_a2'];
			else if($row7['room_e'] == 'room3') $aproxt = $row8['aprox3'] + $row8['aprox_a3'];
			else if($row7['room_e'] == 'room4') $aproxt = $row8['aprox4'] + $row8['aprox_a4'];
			else if($row7['room_e'] == 'room5') $aproxt = $row8['aprox5'] + $row8['aprox_a5'];
			else if($row7['room_e'] == 'room6') $aproxt = $row8['aprox6'] + $row8['aprox_a6'];
			else if($row7['room_e'] == 'room7') $aproxt = $row8['aprox7'] + $row8['aprox_a7'];
			else if($row7['room_e'] == 'room8') $aproxt = $row8['aprox8'] + $row8['aprox_a8'];
			
			if($row7['room_e'] == 'room1') $aproxt1 = $row8['aprox1'];
			else if($row7['room_e'] == 'room2') $aproxt1 = $row8['aprox2'];
			else if($row7['room_e'] == 'room3') $aproxt1 = $row8['aprox3'];
			else if($row7['room_e'] == 'room4') $aproxt1 = $row8['aprox4'];
			else if($row7['room_e'] == 'room5') $aproxt1 = $row8['aprox5'];
			else if($row7['room_e'] == 'room6') $aproxt1 = $row8['aprox6'];
			else if($row7['room_e'] == 'room7') $aproxt1 = $row8['aprox7'];
			else if($row7['room_e'] == 'room8') $aproxt1 = $row8['aprox8'];
			
      //* INSERT PAYMENTS
			if($dateM >= $row_notification['start']){

				$sql2 = "INSERT INTO payments (names, i_mail, r_mail, date_p, title_p, price_p, reserve_s, startr_p, endr_p, roomr_p, status_p, link_p) values ('$row4[name_h] $row4[l_name_h]', '$row4[mail_h]', '$row4[mail_h]', '$date', 'Student Arrival', '$aproxt1', '$row_student[mail_s]', '$row7[start]', '$row7[end]', '$row7[room_e]', 'Payable', 'NULL')";
				$rsql2=$link->query($sql2);
				
				$sql4 = "INSERT INTO payments (names, i_mail, r_mail, date_p, title_p, price_p, reserve_s, startr_p, endr_p, roomr_p, status_p, link_p) values ('$row4[name_h] $row4[l_name_h]', '$row4[mail_h]', '$row6[mail]', '$date', 'Student Arrival', '$aproxt', '$row_student[mail_s]', '$row7[start]', '$row7[end]', '$row7[room_e]', 'Payable', 'NULL')";
				$rsql4=$link->query($sql4);

				/*$sql3 = "INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, status, extend) values ('$row4[name_h]', '$row4[l_name_h]', '$row4[mail_h]', 'NULL', 'NULL', 'NULL', 'NULL', '$row6[mail]', '$date', '0', '0', 'Payments Arrival', '$row_student[mail_s]', 'NULL', 'NULL', 'NULL', 'NULL')";
				$rsql3=$link->query($sql3);*/

			}else {

				$sql2 = "INSERT INTO payments (names, i_mail, r_mail, date_p, title_p, price_p, reserve_s, startr_p, endr_p, roomr_p, status_p, link_p) values ('$row4[name_h] $row4[l_name_h]', '$row4[mail_h]', '$row4[mail_h]', '$date', 'Student Arrival', '$aproxt1', '$row_student[mail_s]', '$row7[start]', '$row7[end]', '$row7[room_e]', 'Budgeted', 'NULL')";
				$rsql2=$link->query($sql2);

				$sql4 = "INSERT INTO payments (names, i_mail, r_mail, date_p, title_p, price_p, reserve_s, startr_p, endr_p, roomr_p, status_p, link_p) values ('$row4[name_h] $row4[l_name_h]', '$row4[mail_h]', '$row6[mail]', '$date', 'Student Arrival', '$aproxt', '$row_student[mail_s]', '$row7[start]', '$row7[end]', '$row7[room_e]', 'Budgeted', 'NULL')";
				$rsql4=$link->query($sql4);

				/*$sql3 = "INSERT INTO notification (user_i, user_i_l, user_i_mail, start, end_, bedrooms, color, user_r, date_, state, confirmed, title, des, report_s, reserve_h, status, extend) values ('$row4[name_h]', '$row4[l_name_h]', '$row4[mail_h]', 'NULL', 'NULL', 'NULL', 'NULL', '$row6[mail]', '$date', '0', '0', 'Payments Arrival', '$row_student[mail_s]', 'NULL', 'NULL', 'NULL', 'NULL')";
				$rsql3=$link->query($sql3);*/

			}

      //! ----------------------------------------------------------------------------------------------------------

      if($insertEvents) $response = "true";
      else $response = 'false';

  }

  
  echo json_encode($response);
    


?>