<?php
// include 'ajax_noti.php';
require 'bdd.php';
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$req = $bdd->prepare("SELECT id, title, start, end, color FROM events WHERE email = '$usuario'");
$req->execute();
$events = $req->fetchAll();

$resultado = $link->query("SELECT id_home, mail_h FROM pe_home WHERE mail_h = '$usuario' ");
$row = $resultado->fetch_assoc();

$resultado2 = $link->query("SELECT mail_s FROM events WHERE email = '$usuario'");
$row2 = $resultado2->fetch_assoc();

$query3 = $link->query("SELECT name_h, date1, date2, date3, date4, date5, date6, date7, date8 FROM room INNER JOIN pe_home ON pe_home.mail_h = '$usuario' and room.id_home = pe_home.id_home");
$row3 = $query3->fetch_assoc();

$even =  $link->query("SELECT * FROM events WHERE email = '$usuario' AND status = 'Active'");
$ev = mysqli_num_rows($even);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="../assets/css/utility.css">
  <link rel="stylesheet" href="assets/css/modal.css">
  <link rel="stylesheet" href="assets/css/main.css">


  <title>Homebor - Homestay</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script src="assets/js/ajax.js"></script>

  <link href="assets/css/style-modal-i.css" rel="stylesheet">
  <!-- FullCalendar -->
  <link href='../assets/css/homestay/calendar/css/fullcalendar.css' rel='stylesheet' />
  <link href='assets/css/calendar1.css' rel='stylesheet' />
  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">



  <script>
  window.onkeydown = function(event) {
    if (event.keyCode === 27) document.getElementById('close').click();
  };
  </script>

</head>

<!-- // TODO HEADER -->
<?php require 'header.php' ?>

<style>
/* // * CORRECCION DEL INDEX POR CAMBIO DESCONOCIDO DE LA FUENTE  */
@media screen and (min-width: 992px) {

  .header-navbar {
    font-size: 15px !important;
    line-height: 22.5px !important;
  }

  .header-logo,
  .header-logo figure,
  .header-logo img {
    font-size: 15px !important;
    height: 38.5px !important;
    line-height: 22.5px !important;
  }

  .link-dropdown>div>span,
  .header-links a {
    font-size: 1.15rem !important;
  }
}
</style>

<body>
  <br>
  <section class="d-flex justify-content-center mb-3 ">
    <?php if ($ev == 0) { ?>
    <h3 class="mb-4 greets__message" style="color: #342359">
      <?php echo "Hello " . $row3['name_h'] . ", add activities to the calendar. Today is  " . $hoy = date("F j, Y"); ?>
    </h3>
    <?php } else { ?>
    <h3 class="mb-4 greets__message" style="color: #342359">
      <?php echo "Hello " . $row3['name_h'] . " Today is " . $hoy = date("F j, Y"); ?> </h3>
    <?php } ?>
  </section>

  <section class="row m-0 mx-auto d-flex justify-content-center align-items-center allcalendar">

    <div class="container__calendar-info col-12 col-sm-12 col-md-11 col-lg-9 col-xl-7 p-0">

      <a href="#popup" style="margin-left:auto;" class="d-none"><img src="assets/icon/help64.png" class="icon-help"
          alt="" title="Help"></a>

      <article class="container__calendar card">
        <h3 class="py-2 m-0 title__reservation_calendar d-flex justify-content-between">
          <?php
          if ($ev == 0) echo "You don't have reservations";
          else echo "You have " . $ev . " active reservations";
          ?>
        </h3>
        <div id="calendar" class="mx-auto p-3 py-sm-2 py-md-5 px-4 "></div>
      </article>

    </div>

    <?php if ($ev == 0) {
    } else { ?>
    <article class="col-md-11 col-lg-5 active__reservation">

      <input type="text" name="search_box" id="search_box" class="form-control"
        placeholder="Type your search query here" hidden>

      <div class="table-responsive w-auto" id="dynamic_content"></div>

    </article>
    <?php } ?>

  </section>





  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">


        <form action="addEvent.php" class="form-horizontal" method="POST">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b> Add New Event </b></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
          </div>

          <div class="modal-body new-event">

            <input type="hidden" name="mail_h" value="<?php echo $row['mail_h']; ?>">

            <div class="row row-fluid mb-4">
              <div class="col-xl-7">
                <label for="title" class="label-event">Event Name</label>
                <input type="text" name="title" id="title" class="input-bordeless" placeholder="e.g. Clean Room 1">
              </div>

              <div class="col-xl-5">
                <label for="color" class="label-event">Room</label>
                <select name="color" class="select input-bordeless custom-select" id="color">

                  <option selected disabled hidden="option"> Select Room </option>

                  <?php if ($row3['date1'] == 'NULL') {
                  } else { ?>

                  <option style="color:#342359;" value="#342359">&#9724; Room 1 </option>

                  <?php }
                  if ($row3['date2'] == 'NULL') {
                  } else { ?>

                  <option style="color:#982A72;" value="#982A72">&#9724; Room 2</option>

                  <?php }
                  if ($row3['date3'] == 'NULL') {
                  } else { ?>

                  <option style="color:#394893;" value="#394893">&#9724; Room 3</option>

                  <?php }
                  if ($row3['date4'] == 'NULL') {
                  } else { ?>
                  <option style="color:#A54483;" value="#A54483">&#9724; Room 4</option>

                  <?php }
                  if ($row3['date5'] == 'NULL') {
                  } else { ?>
                  <option style="color:#5D418D;" value="#5D418D">&#9724; Room 5</option>

                  <?php }
                  if ($row3['date6'] == 'NULL') {
                  } else { ?>
                  <option style="color:#392B84;" value="#392B84">&#9724; Room 6</option>

                  <?php }
                  if ($row3['date7'] == 'NULL') {
                  } else { ?>
                  <option style="color:#B15391;" value="#B15391">&#9724; Room 7</option>

                  <?php }
                  if ($row3['date8'] == 'NULL') {
                  } else { ?>
                  <option style="color:#4F177D;" value="#4F177D">&#9724; Room 8</option>

                  <?php } ?>
                  <option style="color:#4F177D;" value="other">&#9724; Other Activity </option>

                </select>
              </div>

            </div>

            <div class="row row-fluid mb-4">
              <div class="col-xl-5">
                <label for="start" class="label-event">Init Date</label>
                <input type="text" name="start" id="start" class="text-center input-bordeless" style="padding-left: 0"
                  readonly>
              </div>
              <div class="col-xl-5">
                <label for="end" class="label-event">End Date</label>
                <input type="text" name="end" id="end" class="text-center input-bordeless" style="padding-left: 0;"
                  readonly>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-can zoom" data-dismiss="modal"
              style="background-color: #342359; color: white;">Close</button>
            <button type="submit" class="btn btn-ace zoom"
              style="background-color: #982A72; color: white;">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <input type="hidden" id="mails" value="<?php echo $row2['mail_s'] ?>">


  <!-- Modal -->
  <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form class="form-horizontal" method="POST" action="editEventTitle.php">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Modify Event</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>

          </div>

          <div class="modal-body new-event">



            <div class="row row-fluid mb-4">
              <div class="col-xl-7">
                <label for="title" class="label-event">Event Name</label>
                <input type="text" name="title" id="title" class="input-bordeless" placeholder="e.g. Clean Room 1">
              </div>

              <div class="col-xl-5">
                <label for="color" class="label-event">Room</label>
                <select name="color" class="select input-bordeless custom-select" id="color">

                  <option selected disabled hidden="option"> Select Room </option>

                  <?php if ($row3['date1'] == 'NULL') {
                  } else { ?>

                  <option style="color:#342359;" value="#342359">&#9724; Room 1 </option>

                  <?php }
                  if ($row3['date2'] == 'NULL') {
                  } else { ?>

                  <option style="color:#982A72;" value="#982A72">&#9724; Room 2</option>

                  <?php }
                  if ($row3['date3'] == 'NULL') {
                  } else { ?>

                  <option style="color:#394893;" value="#394893">&#9724; Room 3</option>

                  <?php }
                  if ($row3['date4'] == 'NULL') {
                  } else { ?>
                  <option style="color:#A54483;" value="#A54483">&#9724; Room 4</option>

                  <?php }
                  if ($row3['date5'] == 'NULL') {
                  } else { ?>
                  <option style="color:#5D418D;" value="#5D418D">&#9724; Room 5</option>

                  <?php }
                  if ($row3['date6'] == 'NULL') {
                  } else { ?>
                  <option style="color:#392B84;" value="#392B84">&#9724; Room 6</option>

                  <?php }
                  if ($row3['date7'] == 'NULL') {
                  } else { ?>
                  <option style="color:#B15391;" value="#B15391">&#9724; Room 7</option>

                  <?php }
                  if ($row3['date8'] == 'NULL') {
                  } else { ?>
                  <option style="color:#4F177D;" value="#4F177D">&#9724; Room 8</option>

                  <?php } ?>
                  <option style="color:#4F177D;" value="other">&#9724; Other Activity </option>

                </select>
              </div>

              <input type="hidden" name="id" class="form-control" id="id">

            </div>
          </div>



          <div class="modal-footer">
            <button type="submit" name="delete" class="btn btn-del zoom" style="color: white;"> Delete </button>

            <button type="button" class="btn btn-can zoom" data-dismiss="modal"
              style="background-color: #342359; color: white;"> Close </button>

            <button type="submit" class="btn btn-ace zoom" style="background-color: #A54483; color: white;"> Save
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div><br><br>



  <div id="popup" class='modal1' role="dialog" aria-labelledby="Modal title" aria-describedby="Modal description">
    <div class="modal-content1" align="center">
      <div class="header1">
        <a href="#" id="close">
          <div class="box box3">
            <svg viewbox="0 0 40 40">
              <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
            </svg>
          </div>
        </a>
        <h2 class="i" align="center" style="color: white;"> Help - Calendar </h2>

      </div>

      <div class="container" style="width: 97%; background: white; margin: 0; padding: 0">

        <img src="images/calendar.png" style="width: 100%; height: auto;"> <br><br>

        <div class="footer" style="width: 18%; font-size: 18px;" align="center">

          <a href="#popup" class="active-i" title="Calendar Help"> 1 </a>

          <a href="#popup-i" style="border-radius: 20px; padding: 2% 5%;" title="Reservation Detail Help"> 2 </a>

          <a href="#popup-ii" style="border-radius: 20px; padding: 2% 5%;" title="View Reserve"> 3 </a>

          <a href="#popup-i" class="arrow" style="font-size: 22px;" title="Next Page"> > </a>

        </div><br>

      </div>


      <br>
    </div>

    <a href="#">
      <div class="overlay"></div>
    </a>
  </div>


  <div id="popup-i" class='modal1' role="dialog" aria-labelledby="Modal title" aria-describedby="Modal description">
    <div class="modal-content1" align="center">
      <div class="header1">
        <a href="#" id="close">
          <div class="box box3">
            <svg viewbox="0 0 40 40">
              <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
            </svg>
          </div>
        </a>
        <h2 class="i" align="center" style="color: white"> Help - Reservation Detail </h2>

      </div>

      <div class="container" style="width: 97%; background: white; margin: 0; padding: 0">

        <img src="images/reserve.png" style="width: 100%; height: auto;"> <br><br>

        <div class="footer" style="width: 18%; font-size: 18px;" align="center">

          <a href="#popup" class="arrow" style="font-size: 22px;" title="Previous Page">
            < </a>

              <a href="#popup" style="border-radius: 20px; padding: 2% 5%;" title="Calendar Help"> 1 </a>

              <a href="#popup-i" class="active-i" title="Reservation Detail Help"> 2 </a>

              <a href="#popup-ii" style="border-radius: 20px; padding: 2% 5%;" title="View Reserve"> 3 </a>

              <a href="#popup-ii" class="arrow" style="font-size: 22px;" title="Next Page"> > </a>

        </div><br>

      </div>


      <br>
    </div>

    <a href="#">
      <div class="overlay"></div>
    </a>
  </div>


  <div id="popup-ii" class='modal1' role="dialog" aria-labelledby="Modal title" aria-describedby="Modal description">
    <div class="modal-content1" align="center">
      <div class="header1">
        <a href="#" id="close">
          <div class="box box3">
            <svg viewbox="0 0 40 40">
              <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
            </svg>
          </div>
        </a>
        <h2 class="i" align="center" style="color: white"> Help - View Reservations </h2>

      </div>

      <div class="container" style="width: 97%; background: white; margin: 0; padding: 0">

        <img src="images/show.png" style="width: 100%; height: auto;"> <br><br>

        <div class="footer" style="width: 18%; font-size: 18px;" align="center">

          <a href="#popup-i" class="arrow" style="font-size: 22px;" title="Previous Page">
            < </a>

              <a href="#popup" style="border-radius: 20px; padding: 2% 5%;" title="Calendar Help"> 1 </a>

              <a href="#popup-i" style="border-radius: 20px; padding: 2% 5%;" title="Reservation Detail Help"> 2 </a>

              <a href="#popup-ii" class="active-i" title="View Reserve"> 3 </a>

        </div><br>

      </div>


      <br>
    </div>

    <a href="#">
      <div class="overlay"></div>
    </a>
  </div>




  </div><br>


  <input type="hidden" id="id_home" value="<?php echo $row['id_home'] ?>">





  <!--*********************************************************************************************************-->
  <!--************ FOOTER *************************************************************************************-->
  <!--*********************************************************************************************************-->

  </div>
  <?php require 'footer.php' ?>

  <script src="../assets/js/homestay/js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="../assets/js/homestay/js/bootstrap.min.js"></script>

  <!-- FullCalendar -->
  <script src='../assets/js/homestay/js/moment.min.js'></script>
  <script src='../assets/js/homestay/js/fullcalendar/fullcalendar.min.js'></script>
  <script src='../assets/js/homestay/js/fullcalendar/fullcalendar.js'></script>
  <script src='../assets/js/homestay/js/fullcalendar/locale/en-gb.js'></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/index.js" type="module"></script>

  <script>
  $(document).ready(function() {

    var date = new Date();
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() + 1).toString() : (date
      .getMonth() + 1).toString();
    var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate()).toString() : (date.getDate())
      .toString();



    $('#calendar').fullCalendar({
      header: {
        language: 'en',
        left: ' prev,today,next ',
        center: 'title',
        right: 'basicWeek,month',

      },
      defaultDate: yyyy + "-" + mm + "-" + dd,
      defaultView: 'month',
      firstDay: 1,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      selectable: true,
      selectHelper: true,
      select: function(start, end) {

        $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
        $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
        $('#ModalAdd').modal('show');
      },
      eventRender: function(event, element) {

        element.bind('dblclick', function() {
          $('#ModalEdit #id').val(event.id);
          $('#ModalEdit #title').val(event.title);
          $('#ModalEdit #color').val(event.color);
          $('#ModalEdit').modal('show');
        });
      },
      eventDrop: function(event, delta, revertFunc) { // si changement de position

        edit(event);

      },
      eventResize: function(event, dayDelta, minuteDelta, revertFunc) { // si changement de longueur

        edit(event);

      },
      events: [
        <?php foreach ($events as $event) :

            $start = explode(" ", $event['start']);
            $end = explode(" ", $event['end']);
            if ($start[1] == '00:00:00') {
              $start = $start[0];
            } else {
              $start = $event['start'];
            }
            if ($end[1] == '00:00:00') {
              $end = $end[0];
            } else {
              $end = $event['end'];
            }
          ?> {
          id: '<?php echo $event['id']; ?>',
          title: '<?php echo $event['title']; ?>',
          start: '<?php echo $start; ?>',
          end: '<?php echo $end; ?>',
          color: '<?php echo $event['color']; ?>',
        },
        <?php endforeach; ?>
      ]
    });

    function edit(event) {
      start = event.start.format('YYYY-MM-DD HH:mm:ss');
      if (event.end) end = event.end.format('YYYY-MM-DD HH:mm:ss');
      else end = start;

      id = event.id;

      Event = [];
      Event[0] = id;
      Event[1] = start;
      Event[2] = end;

      $.ajax({
        url: 'editEventDate.php',
        type: "POST",
        data: {
          Event: Event
        },
        success: function(rep) {
          if (rep == 'OK') alert('Event saved');
          else alert('Try Again');
        }
      });
    }

  });
  </script>

  <script>
  // ajax for the nots

  $(document).ready(function() {

    load_data(1);

    function load_data(page, query = '') {
      $.ajax({
        url: "not_ajax.php",
        method: "POST",
        data: {
          page: page,
          query: query
        },
        success: function(data) {
          $('#dynamic_content').html(data);

        }
      });
    }

    $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
    });

    $('#search_box').keyup(function() {
      var query = $('#search_box').val();
      load_data(1, query);
    });

  });
  </script>


  <!---<script>

  $(document).ready(function(){

    var id_home = $('#id_home').val();

    $.ajax({
      url:"ajax_noti.php",
      method:"POST",
      data:{id_home},
      success:function(home)
      {
        alert(home)
      }
    });

  });

</script>-->





</body>

</html>