<?php
include '../../xeon.php';
include 'ajax_noti.php';
session_start();


$usuario = $_SESSION['username'];

$query = $link->query("SELECT * FROM events WHERE email = '$usuario' AND mail_s != 'NULL' AND status = 'Active' ");


$query2 = $link->query("SELECT * FROM notification WHERE user_i_mail = '$usuario' AND report_s != 'NULL' ORDER BY date_ DESC");
$q = mysqli_num_rows($query2);


$e = ' ';


$homestay_p =  $link->query("SELECT * FROM users WHERE mail = '$usuario'");
$hu = $homestay_p->fetch_assoc();



if ($hu['usert'] != 'homestay') {
  header("location: ../logout.php");
}




/* Notifications */

$req =  $link->query("SELECT * FROM events WHERE email = '$usuario'");
$r = mysqli_num_rows($req);

$noti =  $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND state = '0' ");
$n = mysqli_num_rows($noti);

$usu = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$u = $usu->fetch_assoc();

$peh = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario' ");
$pe = $peh->fetch_assoc();

?>

<!DOCTYPE html>
<html>

<head>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Reports</title>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/notification.css">
  <link rel="stylesheet" href="assets/css/header-index.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="assets/css/carousel.css">
  <link rel="stylesheet" href="assets/css/reports.css">

  <script src="../assets/js/manager/jquery-latest.js"></script>
  <script src="../assets/js/jquery-3.3.1.min.js"></script>


  <!--===============================================================================================-->
</head>


<body style="background-image: url('../assets/img/wallpaper.jpg'); ">

  <header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
        =============================================================================================================-->
    <?php include 'header.php' ?>

  </header>
  <!--end Header-->

  <div class="line__top"></div>


  <main class="main__div">

    <div class="form__group__reports__div">
      <div class="col-md-4 form__group__reports__details">
        <div class="form__group__reports__titles reports_right">
          <h2 class="form__group__reports__title">Reports</h2>
          <a href="#" onclick="newReport()" class="form__group_reports_add" title="New Report">+</a>
        </div>

        <div class="form__goup__div__search__report">
          <label for="search_box" class="fa fa-search label__search"></label>
          <input type="text" name="search_box" id="search_box" class="input__search" placeholder="Search a Report">
        </div>


        <div class="form__group__reports__div2">

          <div class="form__group__reports__content content1"></div>
          <div class="form__group__reports__content2 content2"></div>

        </div>
      </div>

      <div class="col-md-8 form__group__reports__description">

      </div>
    </div>

  </main>

  <input type="hidden" id="mail_u" value="<?php echo $usuario ?>">



  <script>
    function newReport() {
      $('#view').modal("show");
    }



    $('.content2').hide();

    $('#search_box').keyup(function() {

      if ($('#search_box').val()) {

        $('.content1').hide();
        $('.content2').show();

        let search = $('#search_box').val();

        var searchLength = search.length;

        $.ajax({
          url: 'report_search.php',
          type: 'POST',
          data: {
            search,
          },
          success: function(response) {
            let reports = JSON.parse(response);
            let template = '';

            reports.forEach(report => {

              if (report.view_h == '0') {
                template += `
                                    <button class="reports div_new_report" id="reports" onclick="viewRep(${report.id_not})">

                                    <div class="new__report"></div>

                                        <p class="date_report new_report">${report.date}</p>
                                `;
              } else {
                template += `
                                    <button class="reports div_read_report" id="reports" onclick="viewRep(${report.id_not})">

                                        <p class="date_report">${report.date}</p>
                                `;
              }

              template += `
                                        <input type="hidden" name="id_not3" value="${report.id_not}" />
                                        <div class="col-2 p-0 img_profile_photo">
                                    `;
              template += `
                                            <img src="../${report.photo_s}" class="photo" alt="">

                                        </div>
                                    `;

              if (report.view_h == '0') {
                template += `
                                        <div class="col-10 mt-auto mb-auto description_rep ">
                                            <p class="reports__content__title new_report"> ${report.names} </p>
                                            <p class="reports__content read"><b>${report.title}</b></p>
                                        `;
              } else {
                template += `
                                        <div class="col-10 mt-auto mb-auto description_rep">
                                            <p class="reports__content__title read_report"> ${report.names} </p>
                                            <p class="reports__content read">${report.title}</p>
                                        `;
              }
              template += `
                                            <div class="status">
                                            `;

              if (report.status == 'Active') {

                template += `
                                                        <p class="p-0 m-0 status_active"> &#9724;${report.status}</p>
                                                    `;
              } else {

                template += `
                                                        <p class="p-0 m-0 status_close"> &#9724;${report.status}</p>
                                                    `;
              }

              template += `
                                            </div>
                                        </div>
                                    
                                    </button>

                                    
                                `;

            });

            $('.content2').html(template);

          }

        });

      } else {
        $('.content1').show();
        $('.content2').hide();
      }

    });

    $(document).on('click', '.page-link', function() {
      var page = $(this).data('page_number');
      var query = $('#search_box').val();
      load_data(page, query);
    });

    var mail_a = $('#mail_u').val();

    function report_list() {

      $.ajax({
        url: "report_list.php",
        method: "POST",
        data: {
          mail_a
        },
        success: function(response) {
          let students = JSON.parse(response);
          let template = '';


          students.forEach(student => {


            if (student.view_h == '0') {
              template += `
                                    <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                                    <div class="new__report"></div>

                                        <p class="date_report new_report">${student.date}</p>
                                `;
            } else {
              template += `
                                    <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                        <p class="date_report">${student.date}</p>
                                `;
            }

            template += `
                                        <input type="hidden" name="id_not3" value="${student.id_not}" />
                                        <div class="col-2 p-0 img_profile_photo">
                                    `;
            template += `
                                            <img src="../${student.photo_s}" class="photo" alt="">

                                        </div>
                                    `;

            if (student.view_h == '0') {
              template += `
                                        <div class="col-10 mt-auto mb-auto description_rep ">
                                            <p class="reports__content__title new_report"> ${student.names} </p>
                                            <p class="reports__content read"><b>${student.title}</b></p>
                                        `;
            } else {
              template += `
                                        <div class="col-10 mt-auto mb-auto description_rep">
                                            <p class="reports__content__title read_report"> ${student.names} </p>
                                            <p class="reports__content read">${student.title}</p>
                                        `;
            }
            template += `
                                            <div class="status">
                                            `;

            if (student.status == 'Active') {

              template += `
                                                        <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                                    `;
            } else {

              template += `
                                                        <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                                    `;
            }

            template += `
                                            </div>
                                        </div>
                                    
                                    </button>

                                    
                                `;

          });

          $('.form__group__reports__content').html(template);

        }
      });
    }

    setInterval(function() {

      report_list();

    }, 1000); //Actualizo cada 3 segundos

    function viewRep($s) {
      var id = $s;

      $.ajax({
        url: "feed_reports.php",
        method: "POST",
        data: {
          id
        },
        success: function(data) {
          $('.form__group__reports__description').html(data);

        }
      });

      var mail_a = $('#mail_u').val();

      $.ajax({
        url: "report_list.php",
        method: "POST",
        data: {
          mail_a
        },
        success: function(response) {
          let students = JSON.parse(response);
          let template = '';

          students.forEach(student => {


            if (student.view_h == '0') {
              template += `
                            <button class="reports div_new_report" id="reports" onclick="viewRep(${student.id_not})">

                            <div class="new__report"></div>

                                <p class="date_report new_report">${student.date}</p>
                        `;
            } else {
              template += `
                            <button class="reports div_read_report" id="reports" onclick="viewRep(${student.id_not})">

                                <p class="date_report ">${student.date}</p>
                        `;
            }

            template += `
                                
                                <div class="col-2 p-0 img_profile_photo">
                            `;
            template += `
                                    <img src="../${student.photo_s}" class="photo" alt="">

                                </div>
                            `;

            if (student.view_h == '0') {
              template += `
                                <div class="col-10 mt-auto mb-auto description_rep ">
                                    <p class="reports__content__title new_report"> ${student.names} </p>
                                    <p class="reports__content read"><b>${student.title}</b></p>
                                `;
            } else {
              template += `
                                <div class="col-10 mt-auto mb-auto description_rep">
                                    <p class="reports__content__title read_report"> ${student.names} </p>
                                    <p class="reports__content read">${student.title}</p>
                                `;
            }
            template += `
                                    <div class="status">
                                    `;

            if (student.status == 'Active') {

              template += `
                                                <p class="p-0 m-0 status_active"> &#9724;${student.status}</p>
                                            `;
            } else {

              template += `
                                                <p class="p-0 m-0 status_close"> &#9724;${student.status}</p>
                                            `;
            }

            template += `
                                    </div>
                                </div>
                            
                            </button>
                        `;

          });

          $('.form__group__reports__content').html(template);

        }
      });

      var textarea = document.querySelector('textarea');

      textarea.addEventListener('keydown', autosize);

      function autosize() {
        var el = this;
        setTimeout(function() {
          el.style.cssText = 'height:auto; padding:0';
          el.style.cssText = 'height:' + el.scrollHeight + 'px';
        }, 0);
      }


    }
  </script>







  <div class="modal fade" id="view" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">
      <form class="mt-3" action="ajax_delstu.php" method="POST" autocomplete="off" enctype="multipart/form-data">
        <div class="col-xl-7 modal-content report_div d-block ml-auto mr-auto p-0">
          <div class="modal-header header-report">
            <h4 class="m-0 p-0 ml-3 title_new_report">New Report</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white"><span style="color: white" aria-hidden="true">&times;</span></button>
          </div>
          <div class="content_report">
            <div class="container cont_report pt-3 pb-0">
              <div class="col-sm-12 student_reported">
                <label class="title_student_reported mr-2" for="name_stu">Student Reported:</label>
                <select name="name_stu" id="name_stu" class="custom-select stu-rep" id="title-rep">
                  <option disabled selected hidden="option"> - Select Student - </option>
                  <?php while ($student = mysqli_fetch_array($query)) {

                    $stu = $link->query("SELECT * FROM pe_student WHERE mail_s = '$student[mail_s]' ");
                    $rs = $stu->fetch_assoc();

                    $not_e = $link->query("SELECT * FROM notification WHERE user_i_mail = '$usuario' AND report_s = '$rs[mail_s]' AND status = 'Active'");
                    $ne = $not_e->fetch_assoc();

                    if ($student['mail_s'] == $ne['report_s']) {
                    } else {

                  ?>

                      <option value="<?php echo $rs['id_student'] ?>" class="option"><?php echo $rs['name_s'] . ' ' . $rs['l_name_s'] ?> </option>

                    <?php } ?>

                  <?php } ?>

                </select>
              </div>

              <div class="col-sm-12 student_reported mt-4 mb-4">
                <label class="title_student_report mr-2" for="title_rep">Report title:</label>
                <select name="title_rep" class="custom-select title-rep" id="title-rep">
                  <option disabled selected hidden="option"> - Select Title - </option>
                  <option value="Cancel Reservation" class="option"> Cancel Reservation </option>
                  <option value="Report Situation" class="option"> Report Situation </option>
                </select>
              </div>

              <div class="col-sm-12 p-0 " style="max-height: 300px">
                <div class="row m-0">
                  <div class="col-sm-3 image_report p-0">

                    <div class="carousel-item active p-0">

                      <img id="preview" class="add-photo" style="margin-top: 5%; margin-bottom: 2%;">

                      <input type="file" name="report_image" id="file" accept="image/*" onchange="previewImage();" style="display: none">


                      <label for="file" class="photo-add" id="label-i">
                        <p class="form__group-l_title text-center"> Add Report Image </p>
                      </label>

                      <label for="file" class="add-photo-i fa fa-pencil-alt" id="label-i-i" style="display:none;" title="Change Frontage Photo"></label>

                    </div>
                  </div>

                  <div class="col-sm-9 comment_report ">
                    <textarea rows="6" class="input-borderless" name="des" id="des" placeholder="Describe the problem. No special characters."></textarea>
                  </div>
                </div>

                <input id="id_stu" name="id_stu" type="hidden" value="<?php echo $row['id_student'] ?>">
                <input type="hidden" name="room" id="room" value="<?php echo $row6['bedrooms']; ?>" style="text-align:center;" readonly>
              </div>

            </div>
          </div>


          <div class="col-sm-12 div_button" align="right">
            <button type="submit" class="button_send_report" id="notify" onclick="report()">Send</button>
          </div>
        </div>
      </form>
    </div>

    <script>
      function previewImage() {
        var file = document.getElementById("file").files;

        if (file.length > 0) {
          var fileReader = new FileReader();

          fileReader.onload = function(event) {
            document.getElementById("preview").setAttribute("src", event.target.result);
          };

          fileReader.readAsDataURL(file[0]);

          var label_1 = document.getElementById("label-i");
          var label_1_1 = document.getElementById("label-i-i");

          label_1.style.display = 'none';
          label_1_1.style.display = 'inline';

        }

      }
    </script>

  </div>




  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/rooms.js"></script>
  <script src="assets/js/ajax_noti.js"></script>


  <?php include 'footer.php' ?>

</body>

</html>