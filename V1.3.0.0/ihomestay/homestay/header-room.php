<?php session_start(); ?>

<style>
    @media (min-width: 768px){
        .container-i{margin-bottom: -2em;}

        .roo{margin-top: .3em;}

        .log{ margin-top: -.2em; }

        .bar{margin-left: -2em;}

        .ts-child-i{margin-top: 1.5em;}

        .ts-child-ii{margin-top: 2em;}
    }

@media (max-width: 1000px){
    .hid{display: none;}
}

    @media (min-width: 900px){
        .bar{margin-left: .4em;}

        .ts-child-i{margin-top: 1em;}

        .ts-child-ii{margin-top: 1em;}
    }

    @media (min-width: 1000px){
        .log{margin-top: -.4em;}

        .ts-child-i{margin-top: 0em;}

        .ts-child-ii{margin-top: 0em;}

        .nav-item{margin-left: .7em;}

        .hid-i{display: none;}
    }

    @media (min-width: 1100px){

        .ts-child-i{margin-top: 0em;}

        .ts-child-ii{margin-top: 0em;}
    }

    @media (min-width: 1200px){
        .log{margin-top: -.4em;}

        .ts-child-i{margin-top: 0em;}

        .ts-child-ii{margin-top: 0em;}
    }

    @media (min-width: 1300px){
        .con{margin-top: -.2em;}

        .ts-child-i{margin-top: 0em;}

        .ts-child-ii{margin-top: 0em;}
    }

    @media (min-width: 1420px){
        .top-i{margin-top: .1em;}

        .container-i{margin-bottom: -2em;}

        .ts-child-i{margin-top: 0em;}

        .ts-child-ii{margin-top: 0em;}


    }

    @media (min-width: 1600px){
        .container-i{margin-left: 40em; width: auto;}
    }

    @media (min-width: 1700px){
        .container-i{margin-left: 41em; width: auto;}
    }

    @media (min-width: 1800px){
        .container-i{margin-left: 44em; width: auto;}
    }

    @media (min-width: 1900px){
        .container-i{margin-left: 45em; width: auto;}
    }
</style>

        <nav id="ts-primary-navigation" class="navbar navbar-expand-md navbar-light">
            <div class="container container-i" style="list-style: none;">

                <!--Brand Logo-->
                <a class="navbar-brand" href="index.php">
                    <img class="log" src="../assets/logos/page.png" alt="">
                </a>

                <?php include('notification-after.php'); ?>

                <!--Responsive Collapse Button-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!--Collapsing Navigation-->
                <div class="collapse navbar-collapse" id="navbarPrimary">

                    <!--LEFT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav bar" style="margin-top: 1.5%; ">
<!--Index (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="index.php" style="color: #982A72;">
                              <p class="bar-i"> Calendar </p>
                            </a>
                        </li>
                        <!--end Index nav-item-->
                        <!--Rooms Information (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link roo" href="rooms.php" style="color: #982A72;">
                              <p class="bar-i"> Rooms </p>
                            </a>
                        </li>
                        <!--end Rooms Information nav-item-->
                         <!--Voucher (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">

                            <!--Main level link-->
                            <a class="nav-link active voun hid" href="#" style="color: #982A72;">
                                <p class="bar-i"> Voucher &nbsp;</p>
                                <span class="sr-only">(current)</span>
                            </a>

                            <a class="nav-link active voun hid-i" href="#" style="color: #982A72;">
                                <p class="bar-i"> Voucher </p>
                                <span class="sr-only">(current)</span>
                            </a>

                            <!-- List (1st level) -->
                            <ul class="ts-child ts-child-i">

                                <!-- Certificated Voucher (1st level)
                                =====================================================================================-->
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                      <p class="bar-ii zoom"> Certificated Voucher </p>
                                    </a>
                                </li>
                                <!--end MAP (1st level)-->

                                <!-- SLIDER (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="#" class="nav-link">
                                      <p class="bar-ii zoom"> Students Voucher </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--end VOUCHER nav-item-->

                        <!--Host (Main level)
                        =============================================================================================-->
                        <li class="nav-item ts-has-child">
                            <a class="nav-link hid con" href="#" style="color: #982A72;">
                                <p class="bar-i top-i"> Configuration &nbsp;</p>
                            </a>

                            <a class="nav-link hid-i con" href="#" style="color: #982A72;">
                                <p class="bar-i top-i"> Configuration</p>
                            </a>

                            
                            

                            <!-- List (1st level) -->
                            <ul class="ts-child ts-child-ii">

                                <!-- Certificated Voucher (1st level)
                                =====================================================================================-->
                                <li class="nav-item">
                                    <a href="../logout.php" class="nav-link">
                                      <p class="bar-ii zoom"> Logout </p>
                                    </a>
                                </li>
                                <!--end MAP (1st level)-->

                                <!-- SLIDER (1st level)
                                =====================================================================================-->
                                <li class="nav-item">

                                    <a href="edit-property.php" class="nav-link">
                                      <p class="bar-ii zoom"> Edit Property </p>
                                    </a>
                                </li>

                                 <li class="nav-item">

                                    <a href="delete_property.php" class="nav-link">
                                      <p class="bar-ii zoom"> Disable Account </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--end Host nav-item-->

                        <!--HELP (Main level)
                        =============================================================================================-->
                        <li class="nav-item">
                            <a class="nav-link" href="#popup" style="color: #982A72; margin-top: .3em;">
                              <p class="bar-i top-ii"> Help </p>
                            </a>
                        </li>
                        <!--end HELP nav-item-->

                    </ul>
                    <!--end Left navigation main level-->

                    <!--RIGHT NAVIGATION MAIN LEVEL
                    =================================================================================================-->
                    <ul class="navbar-nav ml-auto right" style="margin-top: 2%;">

                      <?php include('notification-before.php') ?>

                      


                    </ul>
                    <!--end Right navigation-->

                </div>
                <!--end navbar-collapse-->
            </div>
            <!--end container-->
        </nav>
        <!--end #ts-primary-navigation.navbar-->