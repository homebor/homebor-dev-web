<?php
include '../../xeon.php';
session_start();


// Almacenando la variable de sesión
$usuario = $_SESSION['username'];


//Solicitud para mostrar solo la información y configuración del usuario en sesion
$id = $_GET['art_id'];

$query="SELECT * FROM pe_student WHERE id_student = '$id'";
$resultado=$link->query($query);

$row=$resultado->fetch_assoc();

$query2="SELECT * FROM academy INNER JOIN pe_student ON pe_student.mail_s = '$row[mail_s]' and academy.id_ac = pe_student.n_a";
$resultado2=$link->query($query2);

$row2=$resultado2->fetch_assoc();

$query3="SELECT * FROM academy WHERE name_a = '$row2[name_a]' ";
$resultado3=$link->query($query3);

$row3=$resultado3->fetch_assoc();

$query5 = $link->query("SELECT * FROM users WHERE mail = '$usuario' ");
$row5=$query5->fetch_assoc();

$query6 = $link->query("SELECT * FROM notification WHERE user_i_mail = '$row[mail_s]' AND user_r = '$usuario' ");
$row6=$query6->fetch_assoc();

$query7 = $link->query("SELECT * FROM events WHERE mail_s = '$row[mail_s]' AND email = '$usuario' AND status = 'Active'");
$row7=$query7->fetch_assoc();

$query8 = $link->query("SELECT * FROM notification WHERE user_i_mail = '$usuario' AND report_s = '$row[mail_s]' AND status = 'Active' ");
$row8=$query8->fetch_assoc();

if($row7['color'] == '#232159'){
    $bed='1';
}
if($row7['color'] == '#982A72'){
    $bed='2';
}
if($row7['color'] == '#394893'){
    $bed='3';
}
if($row7['color'] == '#A54483'){
    $bed='4';
}
if($row7['color'] == '#5D418D'){
    $bed='5';
}
if($row7['color'] == '#392B84'){
    $bed='6';
}
if($row7['color'] == '#B15391'){
    $bed='7';
}
if($row7['color'] == '#4F177D'){
    $bed='8';
}



$homestay_p =  $link->query("SELECT * FROM users WHERE mail = '$usuario'");
$hu=$homestay_p->fetch_assoc();



if ($hu['usert'] != 'homestay') {
    header("location: ../logout.php");   
}


            

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!--CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/leaflet.css">
  <link rel="stylesheet" href="assets/css/main.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/footer.css">
  
  <link rel="stylesheet" href="assets/css/header-index.css">
  <link rel="stylesheet" href="assets/css/student-info.css">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Students Profile</title>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>

</head>

<body style="background-color: #F3F8FC;">

  <header id="ts-header" class="fixed-top" style="background: white;">
    <!--PRIMARY NAVIGATION - BARRA DE NAVEGACIÓN
     =============================================================================================================-->
    <?php include 'header.php' ?>

  </header>
  <!--end Header-->


  <div class="container_register">

    <main class="card__register">
      <form class="ts-form" action="edit.php" method="POST" autocomplete="off" enctype="multipart/form-data">

        <div class="row m-0 p-0">

          <div class="col-md-4 column-1">

            <div class="card">

              <h3 class="form__group-title__col-4">Basic Information</h3>

              <div class="information__content">

                <div class="form__group__div__photo">

                  <img class="form__group__photo" id="preview" src="https://homebor.com/<?php echo $row['photo_s'] ?>" alt="">

                </div>

                <!-- Group Name -->

                <?php if(empty($row['name_s']) || $row['name_s'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__name" id="group__name">
                  <div class="form__group__icon-input">
                    <label for="name" class="form__label">First Name</label>

                    <label for="name" class="icon"><i class="icon icon__names"></i></label>
                    <p id="name" name="name" class="form__group-input names"><?php echo $row['name_s']?></p>

                  </div>

                </div>

                <?php } ?>


                <!-- Group Last Name -->

                <?php if(empty($row['l_name_s']) || $row['l_name_s'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__l_name" id="group__l_name">
                  <div class="form__group__icon-input">

                    <label for="l_name" class="form__label">Last Name</label>
                    <label for="l_name" class="icon"><i class="icon icon__names"></i></label>
                    <p id="l_name" name="l_name" class="form__group-input names"> <?php echo "$row[l_name_s]"?> </p>

                  </div>
                </div>

                <?php } ?>



                <!-- Group Mail -->

                <?php if(empty($row['mail_s']) || $row['mail_s'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__mail_s" id="group__mail_s">

                  <div class="form__group__icon-input">

                    <label for="mail_s" class="form__label">Email</label>

                    <label for="mail_s" class="icon"><i class="icon icon__mail"></i></label>

                    <p id="mail_s" name="mail_s" class="form__group-input mail_s"> <?php echo $row['mail_s']; ?> </p>


                  </div>
                </div>

                <?php } ?>

                <!-- Group Age -->

                <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL' ){}else{ 
                                    
                                $from = new DateTime($row['db_s']);
                                $to   = new DateTime('today');
                                $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
                                $sufix= " Years old";
                                
                            ?>

                <div class="form__group form__group__basic form__group__age" id="group__age">

                  <div class="form__group__icon-input">

                    <label for="age" class="form__label">Age</label>

                    <label for="age" class="icon"><i class="icon icon__date"></i></label>

                    <p id="age" name="age" class="form__group-input age"> <?php echo $age . $sufix ?> </p>

                  </div>
                </div>

                <?php } ?>

                <div class="div__btn-report">
                  <a href="reports" class="btn btn__report-stu"> Report Student </a>
                </div>

              </div>

            </div>

            <div class="card">


              <h3 class="form__group-title__col-4">Lodging Information</h3>

              <div class="information__content">

                <!-- Group Room -->

                <?php if(empty($row7['color']) || $row7['color'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__basic form__group__lastd" id="group__lastd">
                  <div class="form__group__icon-input">

                    <label for="lastd" class="form__label">Bedroom </label>
                    <label for="lastd" class="icon"><i class="icon icon__bed"></i></label>

                    <p id="lastd" class="form__group-input lastd"> <?php echo $bed ?> </p>
                  </div>

                </div>

                <?php } ?>

                <!-- Group Start Date of Stay -->

                <?php if(empty($row['firstd']) || $row['firstd'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__basic form__group__firstd" id="group__firstd">
                  <div class="form__group__icon-input">

                    <label for="firstd" class="form__label">Start Date of Stay</label>
                    <label for="firstd" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc5 = date_create($row['firstd']); ?>

                    <p id="firstd" name="firstd" class="form__group-input firstd">
                      <?php echo date_format($roc5, 'jS F Y'); ?> </p>

                  </div>

                </div>

                <?php } ?>

                <!-- Group End Date of stay -->

                <?php if(empty($row['lastd']) || $row['lastd'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__basic form__group__lastd" id="group__lastd">
                  <div class="form__group__icon-input">

                    <label for="lastd" class="form__label">End Date of Stay</label>
                    <label for="lastd" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc6 = date_create($row['lastd']); ?>

                    <p id="lastd" class="form__group-input lastd"> <?php echo date_format($roc6, 'jS F Y'); ?> </p>
                  </div>

                </div>

                <?php } ?>



              </div>

            </div>


            <?php if($row['n_airline'] == 'NULL' && $row['n_flight'] == 'NULL' && $row['departure_f'] == 'NULL' && $row['n_flight'] == 'NULL' ){}else{ ?>


            <div class="card">


              <h3 class="form__group-title__col-4">Flight Information</h3>

              <div class="information__content">

                <!-- Group Booking Confirmation -->

                <?php if(empty($row['n_airline']) || $row['n_airline'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__n_airline" id="group__n_airline">
                  <div class="form__group__icon-input">

                    <label for="n_airline" class="form__label">Booking Confirmation</label>

                    <label for="n_airline" class="icon"><i class="icon icon__n_airline"></i></label>

                    <p id="n_airline" name="n_airline" class="form__group-input n_airline">
                      <?php echo $row['n_airline']; ?> </p>

                  </div>
                </div>

                <?php } ?>

                <!-- Group Booking Confirmation -->

                <?php if(empty($row['n_flight']) || $row['n_flight'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__n_flight" id="group__n_flight">
                  <div class="form__group__icon-input">

                    <label for="n_flight" class="form__label">Landing Flight Number</label>

                    <label for="n_flight" class="icon"><i class="icon icon__n_flight"></i></label>

                    <p id="n_flight" name="n_flight" class="form__group-input n_flight"> <?php echo $row['n_flight']; ?>
                    </p>

                  </div>
                </div>

                <?php } ?>

                <?php if(empty($row['departure_f']) || $row['departure_f'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group-f_date ">
                  <div class="form__group__icon-input">
                    <label for="f_date" class="form__label">Flight Date</label>
                    <label for="f_date" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc1 = date_create($row['departure_f']); ?>

                    <p id="f_date" name="f_date" class="form__group-input f_date">
                      <?php echo date_format($roc1, 'jS F Y'); ?> </p>

                  </div>
                </div>

                <?php } 
                                
                                if(empty($row['arrive_f']) || $row['arrive_f'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group-h_date">

                  <div class="form__group__icon-input">

                    <label for="h_date" class="form__label">Arrival at the Homestay</label>
                    <label for="h_date" class="icon"><i class="icon icon__date"></i></label>

                    <?php $roc2 = date_create($row['arrive_f']); ?>

                    <p id="h_date" name="h_date" class="form__group-input h_date">
                      <?php echo date_format($roc2, 'jS F Y'); ?> </p>


                  </div>
                </div>

                <?php } ?>

              </div>

            </div>

            <?php } 
                    
                    
                    if($row['cont_name'] == 'NULL' && $row['cont_lname'] == 'NULL' && $row['num_conts'] == 'NULL' && $row['cell_s'] == 'NULL' ){}else{
                    ?>



            <div class="card">

              <h3 class="form__group-title__col-4">Emergency Contact</h3>

              <div class="information__content">


                <!-- Group Emergency Name -->

                <?php if(empty($row['cont_name']) || $row['cont_name'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__cont_name" id="group__cont_name">
                  <div class="form__group__icon-input">

                    <label for="cont_name" class="form__label">Contact Name</label>

                    <label for="cont_name" class="icon"><i class="icon icon__names"></i></label>

                    <p id="cont_name" name="cont_name" class="form__group-input cont_name">
                      <?php echo $row['cont_name']; ?> </p>

                  </div>
                </div>

                <?php } ?>

                <!-- Group Emergency Last Name -->

                <?php if(empty($row['cont_lname']) || $row['cont_lname'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__cont_lname" id="group__cont_lname">

                  <div class="form__group__icon-input">

                    <label for="cont_lname" class="form__label">Contact Last Name</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__names"></i></label>

                    <p id="cont_lname" name="cont_lname" class="form__group-input cont_lname">
                      <?php echo $row['cont_lname'] ?> </p>

                  </div>
                </div>

                <?php } ?>


                <!-- Group Emergency phone -->

                <?php if(empty($row['num_conts']) || $row['num_conts'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__num_conts" id="group__num_conts">

                  <div class="form__group__icon-input">

                    <label for="num_conts" class="form__label">Emergency Phone Number</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                    <p id="num_conts" name="num_conts" class="form__group-input num_conts">
                      <?php echo $row['num_conts']; ?> </p>

                  </div>
                </div>

                <?php } ?>

                <!-- Group Alternative Phone -->

                <?php if(empty($row['cell_s']) || $row['cell_s'] == 'NULL' ){}else{ ?>

                <div class="form__group form__group__cell_s" id="group__cell_s">
                  <div class="form__group__icon-input">

                    <label for="cell_s" class="form__label">Alternative Phone Number</label>

                    <label for="cont_lname" class="icon"><i class="icon icon__num_s"></i></label>

                    <p id="cell_s" name="cell_s" class="form__group-input cell_s"> <?php echo $row['cell_s'] ?> </p>

                  </div>
                </div>

                <?php } ?>


              </div>

            </div>

            <?php } ?>

          </div>



          <div class="col-md-8 column-2">

            <?php if($row['db_s'] == 'NULL' && $row['gen_s'] == 'NULL' && $row['nationality'] == 'NULL' && $row['passport'] == 'NULL' && $row['db_visa'] == 'NULL' && $row['visa'] == 'NULL' ){}else{ ?>

            <div class="card">

              <h3 class="form__group-title__col-4">Personal Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Date of Birth -->

                  <?php if(empty($row['db_s']) || $row['db_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__db" id="group__db">
                    <div class="form__group__icon-input">

                      <label for="db" class="form__label">Date of Birth</label>
                      <label for="db" class="icon"><i class="icon icon__date"></i></label>

                      <?php $roc3 = date_create($row['db_s']); ?>

                      <p id="db" name="db" class="form__group-input db"> <?php echo date_format($roc3, 'jS F Y'); ?>
                      </p>

                    </div>
                  </div>

                  <?php } ?>




                  <!-- Group Gender -->

                  <?php if(empty($row['gen_s']) || $row['gen_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__gender" id="group__gender">

                    <div class="form__group__icon-input">

                      <label for="gender" class="form__label">Gender</label>

                      <label for="gender" class="icon"><i class="icon icon__gender"></i></label>

                      <p id="gender" name="gender" class="form__group-input gender"> <?php echo $row['gen_s'] ?> </p>

                    </div>
                  </div>

                  <?php } ?>


                  <!-- Group Number -->

                  <?php if(empty($row['num_s']) || $row['num_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group form__group__num_s col-sm-4" id="group__num_s">

                    <div class="form__group__icon-input">

                      <label for="num_s" class="form__label">Phone Number</label>

                      <label for="num_s" class="icon"><i class="icon icon__num_s"></i></label>

                      <p id="num_s" name="num_s" class="form__group-input num_s"> <?php echo $row['num_s']; ?> </p>


                    </div>
                  </div>

                  <?php } ?>



                  <!-- Group Origin Language -->

                  <?php if(empty($row['nationality']) || $row['nationality'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__lang_s" id="group__lang_s">
                    <div class="form__group__icon-input">

                      <label for="lang_s" class="form__label">Background</label>

                      <label for="lang_s" class="icon"><i class="icon icon__nacionality"></i></label>

                      <p id="lang_s" name="lang_s" class="form__group-input lang_s"> <?php echo $row['nationality'] ?>
                      </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['lang_s']) || $row['lang_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Origin Language</label>

                      <label for="type_s" class="icon"><i class="icon icon__nacionality"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['lang_s'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>


                  <?php if(empty($row['language_a']) || $row['language_a'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Another Language</label>

                      <label for="type_s" class="icon"><i class="icon icon__nacionality"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['language_a'] ?>
                      </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Passport -->

                  <?php if(empty($row['passport']) || $row['passport'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__pass" id="group__pass">
                    <div class="form__group__icon-input">

                      <label for="pass" class="form__label">Passport</label>

                      <label for="pass" class="icon"><i class="icon icon__pass"></i></label>

                      <p id="pass" name="pass" class="form__group-input pass"> <?php echo $row['passport'] ?> </p>


                    </div>
                  </div>

                  <?php } ?>


                  <!-- Group Passport Expiration Date -->

                  <?php if(empty($row['exp_pass']) || $row['exp_pass'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                    <div class="form__group__icon-input">

                      <label for="bl" class="form__label">Passport Expiration Date</label>
                      <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                      <?php $exp_pass = date_create($row['exp_pass']); ?>

                      <p id="bl" name="bl" class="form__group-input bl"> <?php echo date_format($exp_pass, 'jS F Y'); ?>
                      </p>

                    </div>

                  </div>

                  <?php } ?>


                  <!-- Group Visa Expiration Date -->

                  <?php if(empty($row['db_visa']) || $row['db_visa'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__bl" id="group__bl">
                    <div class="form__group__icon-input">

                      <label for="bl" class="form__label">Visa Expiration Date</label>
                      <label for="bl" class="icon"><i class="icon icon__bl"></i></label>

                      <?php $roc4 = date_create($row['db_visa']); ?>

                      <p id="bl" name="bl" class="form__group-input bl"> <?php echo date_format($roc4, 'jS F Y'); ?>
                      </p>

                    </div>

                  </div>

                  <?php } 
                                    if(empty($row['visa']) || $row['visa'] == 'NULL' ){}else{ ?>


                  <div class="form__group col-sm-9 form__group__basic form__group__visa" id="group__visa">

                    <div class="form__group__icon-input">


                      <label for="visa" class="form__label visa">Visa</label>
                      <label for="visa" class="icon"><i class="icon icon__city icon__visa"></i></label>

                      <iframe class="embed-responsive-item w-100" src="https://homebor.com/<?php echo $row['visa'] ?>"
                        id="visa"></iframe>

                    </div>
                  </div>

                  <?php } ?>

                </div>

              </div>

            </div>

            <?php } 
                    
                    if($row['smoke_s'] == 'NULL' && $row['drinks_alc'] == 'NULL' && $row['drugs'] == 'NULL' && $row['disease'] == 'NULL' && $row['treatment'] == 'NULL' && $row['treatment_p'] == 'NULL' && $row['allergies'] == 'NULL' && $row['surgery'] == 'NULL'){}else{
                    
                    ?>

            <div class="card">

              <h3 class="form__group-title__col-4">Health Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Type Student -->

                  <?php if(empty($row['smoke_s']) || $row['smoke_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Smoke?</label>

                      <label for="type_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['smoke_s'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['drinks_alc']) || $row['drinks_alc'] == 'NULL' ){}else{ 

                                    if($row['drinks_alc'] == 'yes' ){
                                        $drinks_alc = 'Yes';
                                    }elseif($row['drinks_alc'] == 'no'){
                                        $drinks_alc = 'No';
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Drink Alcohol?</label>

                      <label for="type_s" class="icon"><i class="icon icon__drinks_alc"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $drinks_alc ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['drugs']) || $row['drugs'] == 'NULL' ){}else{ 

                                    if($row['drugs'] == 'yes' ){
                                        $drugs = 'Yes';
                                    }elseif($row['drugs'] == 'no'){
                                        $drugs = 'No';
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Use Drugs?</label>

                      <label for="type_s" class="icon"><i class="icon icon__drugs"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $drugs ?> </p>

                    </div>

                  </div>

                  <?php } ?>


                  <!-- Group Type Student -->

                  <?php if(empty($row['disease']) || $row['disease'] == 'NULL' ){}else{ 

                                    if($row['disease'] == 'yes' ){
                                        $disease = 'Yes';
                                    }elseif($row['disease'] == 'no'){
                                        $disease = 'No';
                                    }else{
                                        $disease = $row['disease'];
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Some Disease?</label>

                      <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $disease ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['treatment']) || $row['treatment'] == 'NULL' ){}else{ 

                                    if($row['treatment'] == 'yes' ){
                                        $treatment = 'Yes';
                                    }elseif($row['treatment'] == 'no'){
                                        $treatment = 'No';
                                    }else{
                                        $treatment = $row['treatment'];
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Medical Treatment?</label>

                      <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $treatment ?> </p>

                    </div>

                  </div>

                  <?php } ?>


                  <!-- Group Type Student -->

                  <?php if(empty($row['treatment_p']) || $row['treatment_p'] == 'NULL' ){}else{ 

                                    if($row['treatment_p'] == 'yes' ){
                                        $treatment_p = 'Yes';
                                    }elseif($row['treatment_p'] == 'no'){
                                        $treatment_p = 'No';
                                    }else{
                                        $treatment_p = $row['treatment_p'];
                                    }
                                                                        
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Psychological Treatment?</label>

                      <label for="type_s" class="icon"><i class="icon icon__healt_s"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $treatment_p ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['allergies']) || $row['allergies'] == 'NULL' ){}else{ 

                                    if($row['allergies'] == 'yes' ){
                                        $allergies = 'Yes';
                                    }elseif($row['allergies'] == 'no'){
                                        $allergies = 'No';
                                    }else{
                                        $allergies = $row['allergies'];
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Have Allergies?</label>

                      <label for="type_s" class="icon"><i class="icon icon__allergies"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $allergies ?> </p>

                    </div>

                  </div>

                  <?php } ?>


                  <?php if(empty($row['surgery']) || $row['surgery'] == 'NULL' ){}else{ 

                                    if($row['surgery'] == 'yes' ){
                                        $surgery = 'Yes';
                                    }elseif($row['surgery'] == 'no'){
                                        $surgery = 'No';
                                    }else{
                                        $surgery = $row['surgery'];
                                    }
                                    
                                ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Have you had Surgeries?</label>

                      <label for="type_s" class="icon"><i class="icon icon__allergies"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $surgery ?> </p>

                    </div>

                  </div>

                  <?php } ?>


                  <!-- Group Smoker -->

                  <?php if(empty($row['allergy_a']) || $row['allergy_a'] == 'NULL' ){}else{
                                        if($row['allergy_a'] == 'yes' ){
                                            $allergy = 'Yes';
                                        }elseif($row['allergy_a'] == 'no'){
                                            $allergy = 'No';
                                        }else{
                                          $allergy = $row['allergy_a'];
                                        }
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Allergy to Animals</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $allergy ?> </p>
                    </div>


                  </div>
                  <?php } ?>

                  <?php if(empty($row['allergy_m']) || $row['allergy_m'] == 'NULL' ){}else{
                                        if($row['allergy_m'] == 'yes' ){
                                            $allergyM = 'Yes';
                                        }elseif($row['allergy_m'] == 'no'){
                                            $allergyM = 'No';
                                        }else{
                                          $allergyM = $row['allergy_m'];
                                        }
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Dietary Restrictions</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__food"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $allergyM ?> </p>
                    </div>


                  </div>
                  <?php } ?>

                </div>

              </div>

            </div>

            <?php } if($row2['name_a'] == 'NULL' && $row['lang_s'] == 'NULL' && $row['language_a'] == 'NULL' && $row['type_s'] == 'NULL' && $row['schedule'] == 'NULL'){}else{
                    
                    ?>


            <div class="card">

              <h3 class="form__group-title__col-4">Professional Information</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">


                  <!-- Group Academy Preference -->

                  <?php if(empty($row2['name_a']) || $row2['name_a'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-12 form__group__basic form__group__n_a" id="group__n_a">

                    <div class="form__group__icon-input">

                      <label for="n_a" class="form__label">School Name</label>

                      <label for="n_a" class="icon"><i class="icon icon__n_a"></i></label>

                      <p id="n_a" name="n_a" class="form__group-input n_a"> <?php echo $row2['name_a'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Academy Dir -->

                  <?php if(empty($row2['dir_a']) || $row2['dir_a'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-12 form__group__basic form__group__address_aca"
                    id="group__address_aca">

                    <div class="form__group__icon-input">

                      <label for="address_aca" class="form__label">School Address</label>

                      <label for="address_aca" class="icon"><i class="icon icon__n_a"></i></label>

                      <p id="address_aca" name="address_aca" class="form__group-input address_aca">
                        <?php echo $row2['dir_a'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>



                  <!-- Group Type Student -->

                  <?php if(empty($row['type_s']) || $row['type_s'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Type Student</label>

                      <label for="type_s" class="icon"><i class="icon icon__names"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['type_s'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>

                  <!-- Group Type Student -->

                  <?php if(empty($row['schedule']) || $row['schedule'] == 'NULL' ){}else{ ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__type_s" id="group__type_s">
                    <div class="form__group__icon-input">

                      <label for="type_s" class="form__label">Schedule</label>

                      <label for="type_s" class="icon"><i class="icon icon__nacionality"></i></label>

                      <p id="type_s" name="type_s" class="form__group-input type_s"> <?php echo $row['schedule'] ?> </p>

                    </div>

                  </div>

                  <?php } ?>



                </div>

              </div>

            </div>

            <?php } 
                    
                    if($row['smoke_s'] == 'NULL' && $row['pets'] == 'NULL' && $row['food'] == 'NULL' && $row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{
                    
                    ?>


            <div class="card">

              <h3 class="form__group-title__col-4">House Preferences</h3>


              <h3 class="subtitle__section-card mt-3 mb-5">He can Share with</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">

                  <!-- Group Smoker -->

                  <?php if(empty($row['smoker_l']) || $row['smoker_l'] == 'NULL' ){}else{
                                        if($row['smoker_l'] == 'yes' ){
                                            $smoker_l = 'Yes';
                                        }elseif($row['smoker_l'] == 'no'){
                                            $smoker_l = 'No';
                                        }
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Smokers</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__smoke_s"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $smoker_l ?> </p>
                    </div>


                  </div>
                  <?php } ?>

                  <?php if(empty($row['children']) || $row['children'] == 'NULL' ){}else{
                                        if($row['children'] == 'yes' ){
                                            $children = 'Yes';
                                        }elseif($row['children'] == 'no'){
                                            $children = 'No';
                                        }
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Children</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__children"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $children ?> </p>
                    </div>


                  </div>
                  <?php } ?>


                  <?php if(empty($row['teenagers']) || $row['teenagers'] == 'NULL' ){}else{
                                        if($row['teenagers'] == 'yes' ){
                                            $teenagers = 'Yes';
                                        }elseif($row['teenagers'] == 'no'){
                                            $teenagers = 'No';
                                        }
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Teenagers</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__children"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $teenagers ?> </p>
                    </div>


                  </div>
                  <?php } ?>


                  <?php if(empty($row['pets']) || $row['pets'] == 'NULL' ){}else{
                                        ?>

                  <div class="form__group col-sm-4 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Pets</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $row['pets'] ?> </p>
                    </div>


                  </div>
                  <?php } ?>

                  <?php if(empty($row['lodging_type']) || $row['lodging_type'] == 'NULL' ){}else{

                                        if($row['lodging_type'] == 'Share1' ){

                                            $lodging_type = 'Share with Food Service';

                                        }elseif($row['lodging_type'] == 'Single1'){

                                            $lodging_type = 'Single with Food Service';

                                        }elseif($row['lodging_type'] == 'Single2'){

                                            $lodging_type = 'Single without Food Service';

                                        }elseif($row['lodging_type'] == 'Share2'){

                                            $lodging_type = 'Share without Food Service';

                                        }

                                    ?>

                  <div class="form__group col-sm-5 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Type of Accommodation</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__lodging"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $lodging_type ?> </p>
                    </div>


                  </div>
                  <?php } ?>


                  <?php if($row['lodging_type'] == 'Single2' || $row['lodging_type'] == 'Share2'){}else{
                                        ?>
                  <?php if(empty($row['food']) || $row['food'] == 'NULL' ){}else{
                                            ?>

                  <div class="form__group col-sm-5 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Do you require a special diet?</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__food"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $row['food'] ?> </p>
                    </div>


                  </div>
                  <?php } ?>


                  <?php if($row['food'] == "Yes"){ ?>

                  <?php if($row['vegetarians'] == "no" && $row['halal'] == "no" && $row['kosher'] == "no" && $row['lactose'] == "no" && $row['gluten'] == "no" && $row['pork'] == "no" && $row['none'] == "no"){}else{ ?>

                  <!-- Group Special -->
                  <div class="form__group form__group__basic form__group__special-diet" id="group__special-diet">

                    <div class="form__group__div-special__diet">
                      <div class="form__group__icon-input">

                        <label for="" class="icon"><i class="icon icon__diet"></i></label>
                        <h4 class="title__special-diet">Special Diet</h4>

                      </div>


                      <div class="form__group__row-diet">

                        <div class="custom-control custom-checkbox">
                          <?php if($row['vegetarians'] == "yes"){ ?>

                          <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians"
                            name="vegetarians" value="yes" checked>

                          <?php } else{ ?>

                          <input type="checkbox" class="custom-control-input vegetarians" id="vegetarians"
                            name="vegetarians" value="yes" disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="vegetarians">Vegetarians</label>
                        </div>

                        <div class="custom-control custom-checkbox">

                          <?php if($row['halal'] == "yes"){ ?>

                          <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes"
                            checked>

                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="halal" name="halal" value="yes"
                            disabled>

                          <?php } ?>

                          <label class="custom-control-label" for="halal">Halal (Muslims)</label>
                        </div>

                      </div>

                      <div class="form__group__row-diet">

                        <div class="custom-control custom-checkbox">

                          <?php if($row['kosher'] == "yes"){ ?>

                          <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes"
                            checked>

                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="kosher" name="kosher" value="yes"
                            disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="kosher">Kosher (Jews)</label>
                        </div>

                        <div class="custom-control custom-checkbox">

                          <?php if($row['lactose'] == "yes"){ ?>

                          <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes"
                            checked>

                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="lactose" name="lactose" value="yes"
                            disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="lactose">Lactose Intolerant</label>
                        </div>

                      </div>

                      <div class="form__group__row-diet">

                        <div class="custom-control custom-checkbox">

                          <?php if($row['gluten'] == "yes"){ ?>

                          <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes"
                            checked>

                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="gluten" name="gluten" value="yes"
                            disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="gluten">Gluten Free Diet</label>
                        </div>

                        <div class="custom-control custom-checkbox">
                          <?php if($row['pork'] == "yes"){ ?>
                          <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes" checked>
                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="pork" name="pork" value="yes"
                            disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="pork">No Pork</label>
                        </div>

                      </div>

                      <div class="form__group__row-diet">

                        <div class="custom-control custom-checkbox-none custom-checkbox">
                          <?php if($row['none'] == "yes"){ ?>
                          <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes" checked>
                          <?php }else{ ?>

                          <input type="checkbox" class="custom-control-input" id="none" name="none" value="yes"
                            disabled>

                          <?php } ?>
                          <label class="custom-control-label" for="none">None</label>
                        </div>

                      </div>

                    </div>

                  </div>

                  <?php }} ?>

                  <?php } ?>

                </div>

              </div>

              <h3 class="subtitle__section-card mt-3 mb-5">Transport</h3>

              <div class="information__content information__content-col-8">

                <div class="row form__group-basic-content m-0 p-0">


                  <?php if(empty($row['pick_up']) || $row['pick_up'] == 'NULL' ){}else{

                                        if($row['pick_up'] == 'yes' ){
                                            $pick_up = 'Yes';
                                        }elseif($row['pick_up'] == 'no'){
                                            $pick_up = 'No';
                                        }
                                    ?>

                  <div class="form__group col-sm-5 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Pick Up Service</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $pick_up ?> </p>
                    </div>


                  </div>
                  <?php } ?>

                  <?php if(empty($row['drop_off']) || $row['drop_off'] == 'NULL' ){}else{

                                        if($row['drop_off'] == 'yes' ){
                                            $drop_off = 'Yes';
                                        }elseif($row['drop_off'] == 'no'){
                                            $drop_off = 'No';
                                        }
                                    ?>

                  <div class="form__group col-sm-5 form__group__basic form__group__smoke_s" id="group__smoke_s">

                    <div class="form__group__icon-input">

                      <label for="smoke_s" class="form__label smoker">Drop Off Service</label>

                      <label for="smoke_s" class="icon"><i class="icon icon__pets"></i></label>

                      <p id="smoke_s" class="form__group-input smoke_s"> <?php echo $drop_off ?> </p>
                    </div>


                  </div>
                  <?php } ?>


                </div>

              </div>

            </div>

            <?php } ?>

          </div>
        </div><br>




      </form>
    </main>

  </div>


  <!--FOOTER ===============================================================-->
  <footer id="ts-footer">
    <?php 
    include 'footer.php';
?>
  </footer>
  <!--end page-->


  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <script src="../assets/js/custom.js"></script>
  <script src="../assets/js/map-leaflet.js"></script>

</body>

</html>