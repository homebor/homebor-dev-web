<?php
session_start();
include_once('../xeon.php');
error_reporting(0);
// Almacenando la variable de sesión
    $usuario = $_SESSION['username'];

$query = $link->query("SELECT * FROM agents WHERE a_mail = '$usuario' ");
$row=$query->fetch_assoc();
$query2 = $link->query("SELECT * FROM manager INNER JOIN agents ON manager.id_m = agents.id_m AND agents.a_mail = '$usuario'");
$row2=$query2->fetch_assoc();

 



?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!--CSS -->
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../assets/css/leaflet.css">
    <link rel="stylesheet" href="../assets/css/style.css">

    <!--Mapbox Links-->
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <title>Homebor - Agent Edit</title>
    <style type="text/css">
        #map {width: 400px; height: 300px;}
         @media screen and (max-width: 600px) { 

        div#map{margin-left: 20px; width: 280px; height: 200px; margin-top: 20px;}

          @media screen and (min-width: 640px) { 

        /*REPONSIVE LOCATION*/
        div#map{margin-left: 20px; width: 280px; height: 200px; margin-top: 20px;}

         /* MEDIA MEDIUM DEVICE*/
    @media screen and (min-width: 768px) { 

        div#map{margin-left: 0px; width: 180px; height: 280px; margin-top: 20px;}

         /* MEDIA LARGE DEVICE DESKTOP AND LAPTOPS*/
    @media screen and (min-width: 800px) { 


        /*REPONSIVE LOCATION*/
        div#map{margin-left: 0px; width: 340px; height: 320px; margin-top: 20px;}
    </style>

</head>
<body style="background-color: #F3F8FC;">

<!-- WRAPPER
    =================================================================================================================-->
<div id="page-top">

    <!--HEADER===============================================================-->
<?php 
    include 'header.php';
?>

    <!--*********************************************************************************************************-->
    <!-- MAIN ***************************************************************************************************-->
    <!--*********************************************************************************************************-->

    <main id="ts-main" style="background-color: #F3F8FC;">
        <!--PAGE TITLE
            =========================================================================================================-->
        <section id="page-title">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="ts-title">
                            <h1>Agent Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--SUBMIT FORM =========================================================================================================-->
        <section id="submit-form">
            <div class="container">
                <div class="row">

                    <div class="offset-lg-1 col-lg-10">

        <form id="form-submit" class="ts-form" autocomplete="off" action="edit_agent_profile.php" method="post" enctype="multipart/form-data">

             <!--Basic
                                =====================================================================================-->
                             <section id="Basic" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Basic Information</h3>

                                <div class="text-center">
                                     <?php
                                    
                                        if($row['photo'] == 'NULL'){
                                        
                                    }
                                    else {
                                        echo'<img class="rounded-circle" src="../'.$row['photo'].'" width="200px;" height="200" style="margin-bottom: 20px;"/>';}
                                 
                                    ?>
                                </div>

                                 <div class="row">

                                    <!--Name-->
                                    <?php
                                    
                                        if($row['name'] == 'NULL'){
                                          echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="name">Manager Name</label>
                                            <input type="text" class="form-control" id="name" name="name">
                                        </div>
                                    </div>';
                                        
                                    }
                                    else {
                                        echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="name">Manager Name</label>
                                            <input type="text" class="form-control" id="name" name="name"  value='.$row[name].'>
                                        </div>
                                    </div>';}

                                    echo '<input type="text" class="form-control" id="folder" name="folder"  value='.$row2[mail].' hidden>';
                                 
                                    ?>
                                   

                                    <!--Last Name-->
                                    <?php
                                    
                                        if($row['l_name'] == 'NULL'){
                                          echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="l_name">Manager Last Name</label>
                                            <input type="text" class="form-control" id="l_name" name="l_name">
                                        </div>
                                    </div>';
                                        
                                    }
                                    else {
                                        echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="l_name">Manager Last Name</label>
                                            <input type="text" class="form-control" id="l_name" name="l_name"  value='.$row[l_name].'>
                                        </div>
                                    </div>';}
                                 
                                    ?>
                                <!--Mail
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                             <label for="mail_s">Mail</label>
                                            <input type="text" class="form-control" id="mail_s" name="mail_s" value="<?php echo $row5['mail'] ?>">
                                        </div>
                                    </div>-->

                                    <!--Image-->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                             <label for="mail_s">Agency Photo</label>
                                            <div class="file-upload-previews"></div>
                                               <div class="panel-body">
                                                    <div class="input-group-text">
                                                    <input type="file" name="image[]" id="profile-img"  class="file-upload-input with-preview" multiple title="Click to add files" maxlength="1" accept="jpg|png|jpeg" style="width: 100%;" >
                                                    
                                                 </div>
                                    </div>
                                        </div>
                                    </div>

                                     

                            </section>
                            <!--end #location-->

                             <!--LOCATION
                                =====================================================================================-->
                            <section id="location" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Location</h3>

                                <div class="row">

                                    <div class="col-sm-6">

                                        <!--Address-->
                                        <?php
                                    
                                        if($row['country'] == 'NULL'){
                                            echo'  <div id="country" class="input-group">
                                            <label for="address">Country</label>
                                            <input type="text" name="country" id="country" class="form-control border-right-0" id="address">
                                            <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Find My Location">
                                                <a href="#" class="input-group-text bg-white border-left-0 ">
                                                    <i class="fa fa-map-marker ts-text-color-primary">
                                                        <style type="text/css">
                                                            i.fa-map-marker {
                                                                color: purple;
                                                            }
                                                        </style>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div id="country" class="input-group">
                                            <label for="address">Country</label>
                                            <input type="text" name="country" id="country" class="form-control border-right-0" id="address" value="'.$row[country].'">
                                            <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Find My Location">
                                                <a href="#" class="input-group-text bg-white border-left-0 ">
                                                    <i class="fa fa-map-marker ts-text-color-primary">
                                                        <style type="text/css">
                                                            i.fa-map-marker {
                                                                color: purple;
                                                            }
                                                        </style>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>';}
                                 
                                    ?>

                                        <!--City-->
                                        <?php
                                    
                                        if($row['city'] == 'NULL'){
                                            echo'  <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city">
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city" value="'.$row[city].'">
                                        </div>';}
                                 
                                    ?>

                                        <!--State-->
                                        <?php
                                    
                                        if($row['state'] == 'NULL'){
                                            echo'  <div class="form-group">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" id="state" name="state" >
                                        </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" id="state" name="state" value="'.$row['state'].'">
                                        </div>';}
                                 
                                    ?>
                                        

                                    </div>
                                    <!--end col-md-6-->

                                    <!--Map-->
                                         <div id='map'></div>
                                    <!--end col-md-6-->

                                </div>
                                <!--end row-->
                            </section>
                            <!--end #location-->
    <!--Map-->
                           
                                <script src='https://unpkg.com/mapbox@1.0.0-beta9/dist/mapbox-sdk.min.js'></script>
                                <script>
                                    mapboxgl.accessToken = 'pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg';
                                    console.log(mapboxgl.accessToken);
                                    var client = new MapboxClient(mapboxgl.accessToken);
                                    console.log(client);

                                    
                                    var address = "<?php echo "$row[country], $row[city], $row[state]"; ?>"
                                    var test = client.geocodeForward(address, function(err, data, res) {
                                    // data is the geocoding result as parsed JSON
                                    // res is the http response, including: status, headers and entity properties

                                    console.log(res);
                                    console.log(res.url);
                                    console.log(data);

                                    var coordinates = data.features[0].center;

                                    var map = new mapboxgl.Map({
                                        container: 'map',
                                        style: 'mapbox://styles/mapbox/streets-v10',
                                        center: coordinates,
                                        zoom: 6
                                    });
                                    new mapboxgl.Marker( )
                                    .setLngLat(coordinates)
                                    .addTo(map);


                                    });
  </script>
                                <!--end col-md-6-->

  <!--Agency Info
                                =====================================================================================-->
                            <section id="Academy_info" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Agent Information</h3>

                                <section id="academy-basic-info" class="mb-5">

                                    <!--Academy Information-->
                                    
                                        <!--Academy Information-->
                                    

                                <!--Row-->
                                <div class="row">

                                    <!--Type Student-->
                                     <?php
                                    
                                        if($row['num'] == 'NULL'){
                                          echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="num">Agent Contact</label>
                                            <input type="text" class="form-control" id="num" name="num">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="num">Agent Contact</label>
                                             <input type="text" class="form-control" id="num" name="num" value="'.$row[num].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <!--Arrive-->
                                     <?php
                                    
                                        if($row['num2'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="num2">Agent Alternative Contact</label>
                                            <input type="text" class="form-control" id="num2" name="num2">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="num2">Agent Alternative Contact</label>
                                             <input type="text" class="form-control" id="num2" name="num2" value="'.$row[num2].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <?php
                                    
                                        if($row['experience'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="experience">Years of Experience</label>
                                            <input type="number" class="form-control" id="experience" name="experience">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="experience">Years of Experience</label>
                                             <input type="number" class="form-control" id="experience" name="experience" value="'.$row[experience].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <?php
                                    
                                        if($row['language'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="language">Language Spoken</label>
                                            <input type="text" class="form-control" id="language" name="language">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="language">Language Spoken</label>
                                             <input type="text" class="form-control" id="language" name="language" value="'.$row[language].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <?php
                                    
                                        if($row['specialization'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div id="g_pre" class="form-group">
                                            <label for="specialization" style="margin-bottom: -4px;">Specialization</label>
                                            <select class="custom-select my-2" id="specialization" name="specialization">
                                                <option hidden="option"></option>
                                                <option value="homestay">Homestay</option>
                                                <option value="students">Students</option>
                                                <option value="both">Both</option>
                                            </select>
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div id="g_pre" class="form-group">
                                            <label for="specialization" style="margin-bottom: -4px;">Specialization</label>
                                            <select class="custom-select my-2" id="specialization" name="specialization">
                                                 <option value='.$row[specialization].'>'.$row[specialization].'</option>
                                                <option value="homestay">Homestay</option>
                                                <option value="students">Students</option>
                                                <option value="both">Both</option>
                                            </select>
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <?php
                                    
                                       
                                        echo'<input type="text" class="form-control" id="id_user" name="id_user" value="'.$usuario.'" hidden>
';
                                 
                                    ?>

                                     <?php
                                    
                                       
                                        echo'<input type="text" class="form-control" id="id_ag" name="id_ag" value="'.$row[id_ag].'" hidden>
';

                                echo'<input type="text" class="form-control" id="id_m" name="id_m" value="'.$row[id_m].'" hidden>
';
                                 
                                    ?>


                                </div>
                                <!--end row-->
                                 <?php
                                    
                                        if($row['des'] == 'NULL'){
                                            echo' <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="des" rows="4" name="des"></textarea>
                                </div>';
                                        
                                    }
                                    else {
                                        echo'  <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="des" rows="4" name="des">'.$row[des].'</textarea>
                                </div>';}
                                 
                                    ?>
                            </section>
                            <!--end #location-->

                              <!--Agency Info
                                =====================================================================================-->
                            <section id="Academy_info" class="mb-5">

                                <!--Title-->
                                <h3 class="text-muted border-bottom">Reasons to Work with Me</h3>

                                <section id="academy-basic-info" class="mb-5">

                                    <!--Academy Information-->
                                    
                                        <!--Academy Information-->
                                    

                                <!--Row-->
                                <div class="row">

                                    <!--Origin Language-->
                                     <?php
                                    
                                        if($row['reason1'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason1">Reason 1</label>
                                            <input type="text" class="form-control" id="reason1" name="reason1">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason1">Reason 1</label>
                                           <input type="text" class="form-control" id="reason1" name="reason1" value="'.$row[reason1].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                     <?php
                                    
                                        if($row['reason2'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason2">Reason 2</label>
                                            <input type="text" class="form-control" id="reason2" name="reason2">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason2">Reason 2</label>
                                           <input type="text" class="form-control" id="reason2" name="reason2" value="'.$row[reason2].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    <!--Type Student-->
                                     <?php
                                    
                                        if($row['reason3'] == 'NULL'){
                                            echo' <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason3">Reason 3</label>
                                            <input type="text" class="form-control" id="reason3" name="reason3">
                                        </div>
                                    </div>
';
                                        
                                    }
                                    else {
                                        echo'<div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="reason3">Reason 3</label>
                                           <input type="text" class="form-control" id="reason3" name="reason3" value="'.$row[reason3].'">
                                        </div>
                                    </div>
';}
                                 
                                    ?>

                                    
                            </section>
                            
                            <hr>
                            
                            <section class="py-3">
                                <button id="discard" class="btn btn-outline-secondary btn-lg float-left" name="discard">
                                    <i class="fa fa-times mr-2"></i>
                                    Discard Changes
                                </button>
                                <button id="submit" class="btn btn-primary btn-lg float-right" name="update" style="color: white; background-color: #232159; border: 2px solid #232159;">
                                    <i class="fa fa-save ts-opacity__50 mr-2"></i>
                                    Save Changes
                                </button>
                            </section>

                            <style type="text/css">
                                button.btn-outline-secondary {
                                    color: #5D418D; color: #5D418D; border: 2px solid #5D418D;
                                }
                                button.btn-outline-secondary:hover{ background-color: #982A72; border: 2px solid #982A72; }

                            </style>
                            </section>

                           </form>
                        <!--end #form-submit-->

                    </div>
                    <!--end offset-lg-1 col-lg-10-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>

    </main>
    <!--end #ts-main-->

<!--FOOTER ===============================================================-->
<?php 
    include 'footer.php';
?>
</div>
<!--end page-->

<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/leaflet.js"></script>
<script src="../assets/js/jQuery.MultiFile.min.js"></script>
<script src="../assets/js/custom.js"></script>
<script src="../assets/js/map-leaflet.js"></script>

</body>
</html>