// TODO IMPORTACIONES
import { MAP } from "./map.js";
import Messages from "../../../controllers/messages.js";
import BasicInformation from "./basic_information.js";
import HouseGallery from "./house_gallery.js?ver=1.0";
import AdditionalInformation from "./additional_information.js";
import FamilyInformation from "./family_information.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? GET ID OF HOUSE TO EDIT
const ID_HOME = D.getElementById("homestay-id").value;
// ? INSTANCIAS
const _Messages = new Messages();
const _BasicInformation = new BasicInformation();
const _HouseGallery = new HouseGallery();
const _AdditionalInformation = new AdditionalInformation();
const _FamilyInformation = new FamilyInformation();

// TODO FUNCIONES
/** // ? REPLACE BUTTON BY THIRD BED (PLUS) SIGN IF ITS VALUE IS EMPTY
 * @param {Element} select Elemento select a reemplazar por el icono +
 */
const fixBedType = (select) => {
  if (select.value === "Share") {
    const $lastBed =
      select.parentElement.parentElement.parentElement.parentElement.parentElement.querySelectorAll("#bed-type")[2];

    if ($lastBed.value === "NULL") {
      $lastBed.className = "px-2";
      $lastBed.parentElement.parentElement.parentElement.classList.remove("more");
    }
  }
};

// ? VERIFY THE STRUCTURE OF THE CODE
const checkItems = async () => {
  FamilyInformation.responseControl(D.querySelector("#religion").value, 1, "#family-responses");
  FamilyInformation.responseControl(D.querySelector("#condition").value, 2, "#family-responses");
  FamilyInformation.responseControl(D.querySelector("#misdemeanor").value, 3, "#family-responses");

  FamilyInformation.responseControl(D.querySelector("#allergies").value, 1, "#responses-any-members");
  FamilyInformation.responseControl(D.querySelector("#take-medication").value, 2, "#responses-any-members");
  FamilyInformation.responseControl(D.querySelector("#health-problems").value, 3, "#responses-any-members");

  if (
    !D.querySelector("#family-responses").classList.contains("response-1") &&
    !D.querySelector("#family-responses").classList.contains("response-2") &&
    !D.querySelector("#family-responses").classList.contains("response-3")
  )
    D.querySelector("#family-responses").classList.remove("show");

  if (
    !D.querySelector("#responses-any-members").classList.contains("response-1") &&
    !D.querySelector("#responses-any-members").classList.contains("response-2") &&
    !D.querySelector("#responses-any-members").classList.contains("response-3")
  )
    D.querySelector("#responses-any-members").classList.remove("show");
};

// ? SAVE ALL HOME DATA
const saveData = async (e) => {
  e.preventDefault();

  // * BEGIN LOADING
  _Messages.showMessage("Please Wait...", 1, false);

  // * GO TO SAVE BASIC INFORMATION
  if (D.querySelector("#basic-information")) {
    const $inputs = D.querySelectorAll("#basic-information input");
    const $selects = D.querySelectorAll("#basic-information select");
    await _BasicInformation.saveBasicInformation($inputs, $selects);
  }

  // * GO TO SAVE HOUSE GALLERY
  if (D.querySelector("#house-gallery")) {
    const $inputs = D.querySelectorAll("#house-gallery input");
    await _HouseGallery.saveHouseGallery($inputs);
  }

  // * GO TO SAVE ADDITIONAL INFORMATION
  if (D.querySelector("#additional-information")) {
    const $inputs = D.querySelectorAll("#additional-information input");
    const $selects = D.querySelectorAll("#additional-information select");
    const $textarea = D.querySelector("#additional-information textarea");
    await _AdditionalInformation.saveAdditionalInformation($inputs, $selects, $textarea);
  }

  // * GO TO SAVE FAMILY INFORMATION
  if (D.querySelector("#family-information")) {
    const $inputs = D.querySelectorAll("#family-information input");
    const $selects = D.querySelectorAll("#family-information select");
    await _FamilyInformation.saveFamilyInformation($inputs, $selects);
  }

  // ** END LOADING
  if (!D.querySelector("#modal-dynamic").classList.contains("show")) {
    _Messages.showMessage(`${D.querySelector("#house-name").value} has been successfully modified`, 2, 3);
    setTimeout(() => window.location.reload(), 2000);
  } else {
    D.querySelector("#modal-dynamic #save-changes-yes").disabled = false;
    D.querySelector("#modal-dynamic #save-changes-no").disabled = false;
  }
};

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await _BasicInformation.getBasicInformation();
  await _HouseGallery.getHouseGallery();
  await _AdditionalInformation.getAdditionalInformation();
  await _FamilyInformation.getFamilyInformation();
  await checkItems();

  D.querySelectorAll("#type-room").forEach((select) => fixBedType(select));

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
  }
  // ? SAVE (BASIC INFORMATION / HOUSE GALLERY / BEDROOMS/ADDITIONAL INFORMATION / FAMILY INFORMATION)
  if (e.target.id === "submit") saveData(e);
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  // ? VALIDATION INPUT PRICE
  if (e.target.matches(".edit-price input")) {
    if (/[^0-9$]/gi.test(e.target.value)) e.target.value = e.target.value.substr(0, e.target.value.length - 1);
    else e.target.style.borderColor = "";
  }
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? REMOVE DESIGN RED TO THE SELECTS
  D.querySelectorAll(".invalid").forEach((elem) => elem.classList.remove("invalid"));

  // ? PREVIEW IMAGES
  if (e.target.matches("#house-gallery input") || e.target.matches(".carousel-item.active input")) {
    const $currentImage = e.target.parentElement.querySelector("label img");
    const newImage = e.target.files[0].name;

    const Reader = new FileReader();
    Reader.readAsDataURL(e.target.files[0]);
    Reader.onload = (e) => ($currentImage.src = e.target.result);
  }
});
