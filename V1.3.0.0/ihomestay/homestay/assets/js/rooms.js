// TODO IMPORTACIONES
import BedRooms from "./bedrooms.js";
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;
// ? GET ID OF HOUSE TO EDIT
const ID_HOME = D.getElementById("homestay-id").value;
// ? CURRENT USER
const USER = D.getElementById("current-user").value;

// ? INSTANCIAS
const _Rooms = new BedRooms();
const _Messages = new Messages();

// TODO FUNCIONES

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await _Rooms.homestayRooms(ID_HOME);

  D.querySelectorAll(".carousel-item label").forEach((label) => (label.style.pointerEvents = "none"));
  D.querySelectorAll(".carousel-item input").forEach((input) => input.remove());

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? QUIT MODAL DYNAMIC
  if (e.target.matches(".close") || e.target.matches("#modal-dynamic") || e.target.matches("#see-all-reserves")) {
    D.querySelector("#modal-dynamic").classList.remove("show");
    D.querySelector("#modal-dynamic").style.cssText = "";
    D.querySelector("#modal-dynamic table tbody").innerHTML = "";
  }
  if (e.target.matches("#see-all-reserves")) {
    _Rooms.roomReservesList(
      e.target.parentElement.parentElement.querySelector("#reserve-header"),
      D.getElementById("modal-dynamic")
    );
  }
  // * STUDENT INFO ACTIONS
  if (e.target.matches(".student-info")) e.target.classList.toggle("show");

  if (e.target.matches("[data-student-profile] *")) {
    window.open(e.target.parentElement.dataset.studentProfile, "_blank");
  }
});

// ! KEYUP
D.addEventListener("keyup", (e) => {
  // ? VALIDATION INPUT PRICE
  if (e.target.matches(".edit-price input")) {
    if (/[^0-9$]/gi.test(e.target.value)) e.target.value = e.target.value.substr(0, e.target.value.length - 1);
    else e.target.style.borderColor = "";
  }
});

// ! CHANGE
D.addEventListener("change", (e) => {
  // ? REMOVE DESIGN RED TO THE SELECTS
  D.querySelectorAll(".invalid").forEach((elem) => elem.classList.remove("invalid"));
});
