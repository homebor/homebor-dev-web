// TODO IMPORTACIONES
import Notifications from "../../../controllers/notifications.js";
((D, W) => {
  // TODO VARIABLES
  // ? NOTIFICATION CONTAINER
  const $notificationList = D.getElementById("notification-list");

  // ? NOTIFICATION TEMPLATE
  const $notificationTemplate = D.getElementById("notification-template").content.cloneNode(true);

  // TODO FUNCIONES Y/O CLASES
  class HomestayNotifications extends Notifications {
    constructor() {
      super();
    }

    // ? NOTIFICATIONS CONTROLS
    async manageNotifications() {
      const allNotifications = await super.getNotifications();
      if (allNotifications.notifications) {
        allNotifications.notifications.forEach((notification) => {
          if (notification.title === "Reservation Request" || notification.title === "Reservation Provider") {
            this.insertNotification(notification, true);
          } else this.insertNotification(notification, false);
        });
      }
      this.evaluateNewNotifications(allNotifications.newNotifications);
    }

    // ? SAVE / REJECT RESERVATION
    async actionReserve(btnAction, not) {
      const actionReserve = new FormData();
      actionReserve.set("id_not", not.dataset.idNote);
      actionReserve.set("btn", btnAction);
      const DATA = { method: "POST", body: actionReserve };

      const jsonActionReserve = await fetch("./action-notification.php", DATA);
      const resultActionReserve = await jsonActionReserve.json();

      if (resultActionReserve === "true") W.location.reload();
    }
  }

  // TODO INSTANCIA
  const INSTANCE = new HomestayNotifications();

  // TODO EVENTOS
  // ! DOMContentLoaded
  D.addEventListener("DOMContentLoaded", async (e) => {
    await INSTANCE.manageNotifications();
  });

  // ! CLICK
  D.addEventListener("click", (e) => {
    // * MOSTRAR PANEL DE NOTIFICACIONES
    if (e.target.id === "bell-btn") INSTANCE.showPanel();
    if (e.target.id === "mobile-bell-btn" || e.target.matches(".close-notifications")) INSTANCE.showPanel();
    // * OCULTAR PANEL DE NOTIFICACIONES
    if (
      !e.target.id.includes("bell-btn") &&
      !e.target.id.includes("notification-list") &&
      !e.target.matches("#notification-list *") &&
      $notificationList.classList.contains("d-flex")
    ) {
      INSTANCE.showElement(false, $notificationList);
      INSTANCE.bellBtn.dataset.show = "false";
    }
    // * VER NOTIFICACION
    if (e.target.matches(".notification-item") || e.target.matches(".notification-item *")) {
      e.preventDefault();
      INSTANCE.markAsRead(e.target);
    }
    if (e.target.matches("[data-confirm-student]")) {
      e.preventDefault();
      console.log(e.target);
      INSTANCE.actionReserve("confirm", e.target.parentElement.parentElement.parentElement);
    }
    if (e.target.matches("[data-reject-student]")) {
      e.preventDefault();
      console.log(e.target);

      INSTANCE.actionReserve("reject", e.target.parentElement.parentElement.parentElement);
    }
  });

  // ! SCROLL
  D.addEventListener("scroll", (e) => {
    // * OCULTAR PANEL DE NOTIFICACIONES
    if (D.querySelector("#notification-list").classList.contains("show")) INSTANCE.showPanel();
  });

  // ! RESIZE
  W.addEventListener("resize", (e) => {
    // * OCULTAR PANEL DE NOTIFICACIONES
    if ($notificationList.classList.contains("d-flex")) {
      INSTANCE.showElement(false, $notificationList);
      INSTANCE.bellBtn.dataset.show = "false";
    }
  });

  // ! ERROR
})(document, window);
