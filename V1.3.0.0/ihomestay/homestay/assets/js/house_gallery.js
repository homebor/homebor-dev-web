// TODO VARIABLES
const D = document;
const W = window;

// ? GET ID OF HOUSE TO EDIT
const ID_HOME = D.getElementById("homestay-id").value;

// TODO FUNCIONES
export default class HouseGallery {
  constructor() {}

  // ? OBTENER DE LAS IMAGENES DE LA GALERIA
  async getHouseGallery() {
    try {
      const formData = new FormData();
      formData.set("homestay_id", ID_HOME);

      const OPTIONS = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("homedit_data.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";
      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

      await this.renderHouseGallery(data.houseGallery);
    } catch (error) {
      console.error(error);
    }
  }
  /** // ? FUNCION QUE RENDERIZA LA GALERIA
   * @param {Object} data Imagenes de la galeria
   */
  async renderHouseGallery(data) {
    let i = 0;
    const IMAGES = D.querySelectorAll(".gallery-photo-container img");

    for (const key in data) {
      // ? ARREGLOS DE LA HOUSE AREA 3
      if (data[key] && data[key] !== "NULL" && i === 5) {
        IMAGES[i].parentElement.parentElement.classList.replace("hide", "position-relative");
        D.querySelector("#add-area-3").classList.add("d-none");
        D.querySelector("#add-area-4").classList.remove("d-none");
        D.getElementById("house-area-4").parentElement.style.marginTop = "5rem";
      }
      // ? ARREGLOS DE LA HOUSE AREA 4
      if (data[key] && data[key] !== "NULL" && i === 6) {
        IMAGES[i].parentElement.parentElement.classList.replace("hide", "position-relative");
        D.querySelector("#add-area-4").classList.add("d-none");
        // * PONER LA FOTO DE AREA 3 SI LA AREA 4 TIENE FOTO
        D.querySelector("#add-area-3").classList.add("d-none");
        D.querySelector("#house-area-3").classList.replace("hide", "position-relative");
        D.querySelector("#house-area-4").parentElement.style.marginTop = "5rem";
      }
      // ? ARREGLOS DE BATHROOM PHOTO 3
      if (data[key] && data[key] !== "NULL" && i === 9) {
        IMAGES[i].parentElement.parentElement.classList.replace("hide", "position-relative");
        D.querySelector("#add-bathroom-3").classList.add("d-none");
        D.querySelector("#add-bathroom-4").classList.remove("d-none");
      }
      // ? ARREGLOS DE BATHROOM PHOTO 4
      if (data[key] && data[key] !== "NULL" && i === 10) {
        IMAGES[i].parentElement.parentElement.classList.replace("hide", "position-relative");
        D.querySelector("#add-bathroom-4").classList.add("d-none");
        // * PONER LA FOTO DE BATHROOM 3 SI LA DE BATHROOM 4 TIENE FOTO
        D.querySelector("#bathroom-3").classList.replace("hide", "position-relative");
        D.querySelector("#add-bathroom-3").classList.add("d-none");
      }
      // ? AGREGAR IMAGEN A LA ETIQUETA <img>
      if (data[key] && data[key] !== "NULL") IMAGES[i].src = `https://homebor.com/${data[key]}`;

      i++;
    }
  }

  /** // ? SAVE HOUSE GALLERY
   * @param {Array} images Imagenes de la House Gallery a guardar
   */
  async saveHouseGallery(images) {
    try {
      const roomData = new FormData();
      roomData.set("houseGallery", true);
      roomData.set("homestay_id", ID_HOME);
      if (images[0].files[0]) roomData.set("frontage", images[0].files[0]);
      if (images[1].files[0]) roomData.set("livingRoom", images[1].files[0]);
      if (images[2].files[0]) roomData.set("familyPicture", images[2].files[0]);
      if (images[3].files[0]) roomData.set("kitchen", images[3].files[0]);
      if (images[4].files[0]) roomData.set("diningRoom", images[4].files[0]);
      if (images[5].files[0]) roomData.set("houseArea3", images[5].files[0]);
      if (images[6].files[0]) roomData.set("houseArea4", images[6].files[0]);
      if (images[7].files[0]) roomData.set("bathroom1", images[7].files[0]);
      if (images[8].files[0]) roomData.set("bathroom2", images[8].files[0]);
      if (images[9].files[0]) roomData.set("bathroom3", images[9].files[0]);
      if (images[10].files[0]) roomData.set("bathroom4", images[10].files[0]);

      const options = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: roomData,
      };

      const res = await axios("./edit-admin-propertie.php", options);
      const data = await res.data;
    } catch (error) {
      console.error("Error en Basic Information", error);
    }
  }
}

// ? AADD PHOTO TO HOUSE GALLERY
const addPhoto = (e) => {
  e.preventDefault();
  if (e.target.parentElement.querySelector("h4").textContent.includes("House Area 3")) {
    D.getElementById("add-area-4").classList.remove("d-none");
    D.getElementById("house-area-4").parentElement.style.marginTop = "5rem";
  }
  if (e.target.parentElement.querySelector("h4").textContent.includes("Bathroom Photo 3")) {
    D.getElementById("add-bathroom-4").classList.remove("d-none");
  }
  e.target.parentElement.querySelector(".gallery-photo-container").classList.replace("hide", "position-relative");
  e.target.classList.add("d-none");
};

// ? REMOVE PHOTO OF HOUSE GALLERY
const removePhoto = (e) => {
  if (e.target.parentElement.textContent.includes("House Area 3")) {
    D.getElementById("add-area-4").classList.add("d-none");
    D.getElementById("house-area-4").classList.replace("position-relative", "hide");
    D.getElementById("house-area-4").parentElement.style.marginTop = "0rem";
  }
  if (e.target.parentElement.textContent.includes("Bathroom Photo 3")) {
    D.getElementById("add-bathroom-4").classList.add("d-none");
    D.getElementById("bathroom-4").classList.replace("position-relative", "hide");
  }
  e.target.parentElement.parentElement.classList.replace("position-relative", "hide");
  e.target.parentElement.parentElement.parentElement.querySelector("button").classList.remove("d-none");
};

// TODO EVENTOS
// ! CLICK
D.addEventListener("click", (e) => {
  if (e.target.matches(".add-photo-btn")) addPhoto(e);
  if (e.target.matches("h4 .close")) removePhoto(e);
});
