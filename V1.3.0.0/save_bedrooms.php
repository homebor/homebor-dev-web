<?php

if ($_POST) {
  try {
    include_once('xeon.php');
    session_start();
    error_reporting(0);

    date_default_timezone_set("America/Toronto");
    $date = date('YmdHisv');

    if ($_POST['fromHomestay']) {
      $queryHome = $link->query("SELECT * FROM pe_home WHERE id_home = '$_POST[homestay_id]' ");
      $row_home = $queryHome->fetch_assoc();
      $usuario = $row_home['mail_h'];
    } else $usuario = $_SESSION['username'];


    // TODO VALIDACIONES
    if (!$link) echo "No existe conexión a la base de datos", exit;

    // TODO ACTUALIZAR DATOS DE LA HABITACION
    $homestayId = $_POST['homestay_id'];
    $roomNumber = $_POST['roomNumber'];
    $typeRoom = $_POST['typeRoom'];
    $food = $_POST['food'];
    $bed1 = $_POST['bed1'];
    $bed2 = $_POST['bed2'];
    $bed3 = $_POST['bed3'];


    $date1 = "Available";
    $date2 = "Available";
    $date3 = "Available";


    if ($typeRoom == "NULL") echo json_encode(["Room $roomNumber", "no-type-room"]), exit;
    if ($typeRoom != "Share" && !$bed1 || $bed1 == "NULL") echo json_encode(["Room $roomNumber", "no-bed"]), exit;
    if ($typeRoom == "Share" && (!$bed1 || $bed1 == "NULL" || !$bed2 || $bed2 == "NULL")) echo json_encode(["Room $roomNumber", "no-shared-bed"]), exit;


    if ($typeRoom != "Share" && $bed1) {
      $bed2 = "Disabled";
      $bed3 = "Disabled";
      $date2 = "Disabled";
      $date3 = "Disabled";
    } else if ($typeRoom == "Share" && !$bed3) {
      $bed3 = "Disabled";
      $date3 = "Disabled";
    }

    $strBed1 = "bed$roomNumber";
    $strBed2 = "bed$roomNumber" . "_2";
    $strBed3 = "bed$roomNumber" . "_3";

    $strDate1 = "date$roomNumber";
    $strDate2 = "date$roomNumber" . "_2";
    $strDate3 = "date$roomNumber" . "_3";

    $strQuery = "UPDATE `room` SET 
    `type$roomNumber` = '$typeRoom', 
    `$strBed1` = '$bed1', 
    `$strBed2` = '$bed2', 
    `$strBed3` = '$bed3',
    `food$roomNumber` = '$food', 
    `$strDate1` = '$date1', 
    `$strDate2` = '$date2', 
    `$strDate3` = '$date3', 
    `reservations$roomNumber` = 0 WHERE id_home = '$homestayId' ";
    $roomUpdateQuery = $link->query($strQuery);


    if (isset($_POST['priceHomestay'])) {
      if ($_POST['priceHomestay'] > 0 && is_numeric($_POST['priceHomestay'])) {
        $priceHomestay = $_POST['priceHomestay'];
        $queryPrice = "UPDATE `room` SET `aprox$roomNumber` = '$priceHomestay' WHERE id_home = '$homestayId' ";
        $roomUpdatePrice = $link->query($queryPrice);
      } else echo json_encode(["Room $roomNumber", "price-homestay-invalid"]), exit;
    }

    if (isset($_POST['priceAgent'])) {
      if ($_POST['priceAgent'] >= 0 && is_numeric($_POST['priceAgent'])) {
        $priceAgent = $_POST['priceAgent'];
        $queryPrice = "UPDATE `room` SET `aprox_a$roomNumber` = '$priceAgent' WHERE id_home = '$homestayId' ";
        $roomUpdatePrice = $link->query($queryPrice);
      } else echo json_encode(["Room $roomNumber", "price-agent-invalid"]), exit;
    }


    if ($roomUpdateQuery) {
      $status = true;
      $message = "Room saved successfully";
      $room = "Room $roomNumber";
    }

    // TODO ACTUALIZAR DATOS DE LAS IMÁGENES DE LA HABITACIÓN
    $images = array(
      "proom$roomNumber" => $_FILES['image1'],
      "proom$roomNumber" . "_2" => $_FILES['image2'],
      "proom$roomNumber" . "_3" => $_FILES['image3'],
    );


    // * MOVE IMAGE TO FOLDER, REMOVE LAST IMAGE AND UPDATE DATABASE
    function insertImage($newImage, $lastImage, $proom, $destination, $homestayId, $link)
    {
      if (stristr($lastImage, substr($newImage['name'], 0, 7))) unlink("$lastImage");

      $upload = move_uploaded_file($newImage['tmp_name'],  $destination);


      if ($upload) {
        $updateImg = $link->query("UPDATE `photo_home` SET $proom = '$destination' WHERE id_home = '$homestayId' ");
        $statusImg = true;
        $messageImg = "Image moved success!";
      } else {
        $statusImg = false;
        $messageImg = "Error moving image";
      };
    }


    foreach ($images as $key => $value) {
      if (!$value['name']) continue;


      if (!file_exists("public/$usuario")) mkdir("public/$usuario");


      // * QUERYS
      $photoHomeQuery = $link->query("SELECT * FROM photo_home WHERE id_home = '$homestayId' ");
      $photo = $photoHomeQuery->fetch_assoc();


      // * FORMAT NAME IMAGE
      $imgDate = $date;
      if ($key == "proom$roomNumber") $imgBed = "a";
      if ($key == "proom$roomNumber" . "_2") $imgBed = "b";
      if ($key == "proom$roomNumber" . "_3") $imgBed = "c";
      $imgType =  str_replace("/", ".", stristr($value['type'], "/"));
      $value['name'] = "room$roomNumber-$imgBed-$imgDate$imgType";


      // * PATH DESTINATION
      $destination = "public/$usuario/$value[name]";


      foreach ($photo as $key2 => $value2) {
        if ($key == $key2) insertImage($value, $value2, $key, $destination, $homestayId, $link);
      }


      // --------------------------->
    }



    // * DATA TO SEND
    $jsonToSend = array(
      // * RESULTADO DE LOS INPUTS
      'status' => $status,
      'message' => $message,
      'room' => $room,
      // * RESULTADO DE LAS IMAGENES
      'statusImg' => $statusImg,
      'messageImg' => $messageImg
    );

    echo json_encode($jsonToSend);
  } catch (\Throwable $th) {
    echo json_encode("error");
  }
}