<?php
require '../../xeon.php';

session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');

if (!$usuario || !$link) echo "Insuficientes parametros", exit;

$usersQuery = $link->query("SELECT usert FROM users WHERE mail = '$usuario' ");
$row_user = $usersQuery->fetch_assoc();


// ? GET DATA (AGENT/MANAGER/HOMESTAY/STUDENT)
function getData($user, $whereField, $whereValue, $link)
{
  // ?? AGENT
  if ($user === "Agent") {
    $agentsQuery = $link->query("SELECT id_ag,id_m,name,l_name,photo FROM agents WHERE $whereField = '$whereValue' ");
    $row_agent = $agentsQuery->fetch_assoc();

    if (!$row_agent) $result = false;
    else $result = $row_agent;
  }
  // ?? MANAGER
  if ($user === "Manager") {
    $managerQuery = $link->query("SELECT id_m, name,l_name, mail, a_name, photo FROM manager WHERE $whereField = '$whereValue' ");
    $row_manager = $managerQuery->fetch_assoc();

    if (!$row_manager) $result = false;
    else $result = $row_manager;
  }
  // ?? HOMESTAY
  if ($user === "Homestay") {
    $homestayQuery = $link->query("SELECT id_home, name_h, l_name_h, h_name, phome FROM pe_home WHERE $whereField = '$whereValue' ");
    $row_homestay = $homestayQuery->fetch_assoc();

    if (!$row_homestay) $result = false;
    else $result = $row_homestay;
  }
  // ?? STUDENT
  if ($user === "Student") {
    $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, photo_s FROM pe_student WHERE $whereField = '$whereValue' ");
    $row_student = $studentQuery->fetch_assoc();

    if (!$row_student) $result = false;
    else $result = $row_student;
  }

  return $result;
}


// ? GET DATA NOTIFICATIONS
function getNotifications($user_r, $link)
{
  $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$user_r' AND state < '2' ORDER BY id_not DESC");
  $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$user_r' AND confirmed = '0' AND state = '0' ");
  $total_new_notifications = mysqli_num_rows($newNotifications);

  return [$queryNotifications, $total_new_notifications];
}



// TODO AGENT
if (strtolower($row_user['usert']) == "agent") {
  $agentData = getData("Agent", "a_mail", $usuario, $link);
  $agencyData = getData("Manager", "id_m", $agentData['id_m'], $link);
  $notificationsData = getNotifications($agencyData['mail'], $link)[0];
  $totalNotifications = getNotifications($agencyData['mail'], $link)[1];


  while ($notification = mysqli_fetch_array($notificationsData)) {
    // ? REGISTER HOMESTAY
    if ($notification['title'] == "Register Homestay") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);

      $description = "<b>$agentData[name] $agentData[l_name]</b> has registered a new house.";

      $notification['url'] = "detail?art_id=$homestayData[id_home]";
      $notification['issuerImage'] = $agentData['photo'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? CHANGE STUDENT
    if ($notification['title'] == "Change Student") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      $description = "<b>$agentData[name] $agentData[l_name]</b> has changed <b>$studentData[name_s] $studentData[l_name_s]</b> to <b>$homestayData[name_h] $homestayData[l_name_h]</b>.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $agentData['photo'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? CANCEL RESERVATION
    if ($notification['title'] == "Cancel Reservation") {
      $homestayData = getData("Homestay", "mail_h", $notification['user_i_mail'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if ($notification['status'] == "Reply") {
        $description = "<b>$homestayData[h_name]</b> wants to remove the <b>$studentData[name_s] $studentData[l_name_s]</b> reservation.";
      } else {
        $description = "<b>$homestayData[h_name]</b> has responded to your report.";
      }

      $notification['url'] = "reports";
      $notification['issuerImage'] = "$homestayData[phome]";
      $notification['description'] = $description;
      $notification['userImage1'] = "$studentData[photo_s]";
    }
    // ? REPORT SITUATION
    if ($notification['title'] == "Report Situation") {
      $homestayData = getData("Homestay", "mail_h", $notification['user_i_mail'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if ($notification['status'] == "Reply") {
        $description = "<b>$homestayData[h_name]</b> has responded to your report.";
      } else {
        $description = "<b>$homestayData[h_name]</b> has reported to <b>$studentData[name_s] $studentData[l_name_s].</b>";
      }

      $notification['url'] = "reports";
      $notification['issuerImage'] = "$homestayData[phome]";
      $notification['description'] = $description;
      $notification['userImage1'] = $studentData['photo_s'];
    }
    // ? FLIGHT DATA CONFIRMATION
    if ($notification['title'] == "Flight Data Confirmation") {
      $studentData = getData("Student", "mail_s", $notification['user_i_mail'], $link);

      $description = "<b>$studentData[name_s] $studentData[l_name_s]</b> has updated his flight information.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $studentData['photo_s'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? DELETE RESERVE
    if ($notification['title'] == "Delete Reserve") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      $description = "<b>$agentData[name] $agentData[l_name]</b> has deleted <b>$studentData[name_s] $studentData[l_name_s]</b> reservation in <b>$homestayData[name_h] $homestayData[l_name_h]</b> ";

      $notification['url'] = "reports";
      $notification['issuerImage'] = $agentData['photo'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? KEEP RESERVE
    if ($notification['title'] == "Keep Reserve") {
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      $description = "<b>$agentData[name] $agentData[l_name]</b> has kept the reserve of <b>$studentData[name_s] $studentData[l_name_s]</b> in <b>$homestayData[name_h] $homestayData[l_name_h]</b> ";

      $notification['url'] = "reports";
      $notification['issuerImage'] = $agentData['photo'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVATION PROVIDER
    if ($notification['title'] == "Reservation Provider") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "id_m", $agentData['id_m'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if (!file_exists("https://homebor.com/$agentData[photo]")) $image = $managerData['photo'];
      else $image = $agentData['photo'];

      $description = "<b>$agentData[name] $agentData[l_name]</b> has assigned <b>$studentData[name_s] $studentData[l_name_s]</b> in <b>$homestayData[name_h] $homestayData[l_name_h]</b>.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVATION PROVIDER REQUEST
    if ($notification['title'] == "Reservation Provider Request") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "id_m", $agentData['id_m'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if (!file_exists("https://homebor.com/$agentData[photo]")) $image = $managerData['photo'];
      else $image = $agentData['photo'];

      $description = "<b>$agentData[name] $agentData[l_name]</b> has requested a reservation for <b>$studentData[name_s] $studentData[l_name_s]</b> in <b>$homestayData[name_h] $homestayData[l_name_h]</b>.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? PAYMENTS ARRIVAL
    if ($notification['title'] == "Payments Arrival") {
      $totalNotifications -= 1;
      $notification = false;
    }
    // ? HOMESTAY DESCERTIFY
    if ($notification['title'] == "Homestay Decertify") {
      $agentData = getData("Agent", "a_mail", $notification['report_s'], $link);
      $managerData = getData("Manager", "id_m", $agentData['id_m'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['user_i_mail'], $link);

      if (!file_exists("https://homebor.com/$agentData[photo]")) $image = $managerData['photo'];
      else $image = $agentData['photo'];

      $description = "<b>$agentData[name] $agentData[l_name]</b> has decertified <b>$homestayData[name_h] $homestayData[l_name_h]</b>.";

      $notification['url'] = "detail?art_id=$homestayData[id_home]";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? STUDENT ASSIGNED
    if ($notification['title'] == "New Houses Assigned") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);


      if (!file_exists("https://homebor.com/$studentData[photo_s]")) $studentImage = "assets/emptys/profile-student-empty.png";
      else $studentImage = $studentData['photo_s'];

      if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";


      $description = "
        <b>$agentData[name] $agentData[l_name]</b> has assigned  <b>$notification[des]</b> houses to 
        <b>$studentData[name_s] $studentData[l_name_s]</b>.
      ";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $studentImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $agencyImage;
    }
    // ? HOMESTAY DISBLACK
    if ($notification['title'] == "Homestay Disblack") {
      $agentData = getData("Agent", "a_mail", $notification['report_s'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['user_i_mail'], $link);

      if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>$agentData[name] $agentData[l_name]</b> has disabled <b>$homestayData[name_h] $homestayData[l_name_h]</b>.";

      $notification['url'] = "detail?art_id=$homestayData[id_home]";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? HOMESTAY CERTIFIED
    if ($notification['title'] == "Homestay Certified") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has certified <b>". $homestayData['h_name']. "</b>";
      
      $notification['url'] = "detail?art_id=$homestayData[id_home]";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? REGISTER STUDENT
    if ($notification['title'] == "Register Student") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $studentData = getData("Student", "mail_s", $notification['reserve_h'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has registered a new student. click for details.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? REGISTER ACADEMY
    if ($notification['title'] == "Register Academy") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has registered a new Academy.";

      $notification['url'] = "#";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE NOW
    if ($notification['title'] == "Coordinator Reserve") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has assigned <b>" .$studentData['name_s']. " " .$studentData['l_name_s']. "</b> to <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE CONFIRMED A PENDING RESERVATION
    if ($notification['title'] == "Coordinator Condirmed Request") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has confirmed <b>" .$studentData['name_s']. "'s</b> reservation at <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE REJECT A PENDING RESERVATION
    if ($notification['title'] == "Coordinator Rejected Request") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has rejected <b>" .$studentData['name_s']. "'s</b> reservation at <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? DELETE STUDENT
    if ($notification['title'] == "Coordinator Delete Student") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has removed <b>" .$studentData['name_s']. "'s</b>.";

      $notification['url'] = "#";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }

    $allNotifications[] = $notification;
  }


  // ? DATA TO SEND
  $jsonToSend = array(
    'notifications' => $allNotifications,
    'newNotifications' => $totalNotifications,
  );
}



// TODO MANAGER
if (strtolower($row_user['usert']) === "manager") {
  // ? MANAGER QUERY
  $managersQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
  $row_manager = $managersQuery->fetch_assoc();

  // ? NOTIFICATIONS QUERY
  $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' AND state < '2' ORDER BY id_not DESC");
  $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_manager[mail]' AND confirmed = '0' AND state = '0' ");
  $total_new_notifications = mysqli_num_rows($newNotifications);


  while ($notification = mysqli_fetch_array($queryNotifications)) {
    // * CHANGE STUDENT
    if ($notification['title'] == "Change Student") {
      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
      $row_student = $studentQuery->fetch_assoc();

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
      $row_agent2 = $queryAgent2->fetch_assoc();
      if ($row_agent2) {
        $image = $row_agent2['photo'];
        $name = "$row_agent2[name] $row_agent2[l_name]";
      } else {
        $image = $row_manager['photo'];
        $name = "$row_manager[name] $row_manager[l_name]";
      }

      $description = "<b>$name</b> has changed <b>$row_student[name_s] $row_student[l_name_s]</b> to <b>$row_homestay[name_h] $row_homestay[l_name_h]</b>.";

      $notification['url'] = "student_info?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }

    // * HOMESTAY REGISTER PROVIDER
    if ($notification['title'] == "Register Homestay") {

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $queryAgent2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
      $row_agent2 = $queryAgent2->fetch_assoc();
      if ($row_agent2) {
        $image = $row_agent2['photo'];
        $name = "$row_agent2[name] $row_agent2[l_name]";
      } else {
        $image = $row_manager['photo'];
        $name = "$row_manager[name] $row_manager[l_name]";
      }

      $description = "<b>$name</b> has registered a new house.";

      $notification['url'] = "detail?art_id=$row_homestay[id_home]";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // *
    // *
    // *
    // *
    // *
    // * CANCEL RESERVATION
    if ($notification['title'] == "Cancel Reservation") {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
      $row_student = $studentQuery->fetch_assoc();

      if ($notification['status'] == "Reply") {
        $description = "<b>$row_homestay[h_name]</b> wants to remove the <b>$row_student[name_s] $row_student[l_name_s]</b> reservation.";
      } else $description = "<b>$row_homestay[h_name]</b> has responded to your report.";

      $notification['url'] = "reports";
      $notification['issuerImage'] = "$row_homestay[phome]";
      $notification['description'] = $description;
      $notification['userImage1'] = "$row_student[photo_s]";
    }
    // *
    // *
    // *
    // *
    // *
    // * REPORT SITUATION
    if ($notification['title'] == "Report Situation") {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
      $row_student = $studentQuery->fetch_assoc();

      if ($notification['status'] == "Reply") {
        $description = "<b>$row_homestay[h_name]</b> has responded to your report.";
      } else $description = "<b>$row_homestay[h_name]</b> has reported to <b>$row_student[name_s] $row_student[l_name_s].</b>";

      $notification['url'] = "reports";
      $notification['issuerImage'] = "$row_homestay[phome]";
      $notification['description'] = $description;
      $notification['userImage1'] = $row_student['photo_s'];
    }
    // *
    // *
    // *
    // *
    // *
    // * FLIGHT DATA CONFIRMATION
    if ($notification['title'] == "Flight Data Confirmation") {
      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[user_i_mail]' ");
      $row_student = $studentQuery->fetch_assoc();

      $description = "<b>$row_student[name_s] $row_student[l_name_s]</b> has updated his flight information.";

      $notification['url'] = "student_info?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $row_student['photo_s'];
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // *
    // *
    // *
    // *
    // *
    // * DELETE RESERVE
    if ($notification['title'] == "Delete Reserve") {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
      $row_agent2 = $agentsQuery2->fetch_assoc();
      if ($row_agent2) {
        $image = $row_agent2['photo'];
        $name = "$row_agent2[name] $row_agent2[l_name]";
      } else {
        $managersQuery2 = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
        $row_manager2 = $managersQuery2->fetch_assoc();
        $image = $row_manager2['photo'];
        $name = "$row_manager2[name] $row_manager2[l_name]";
      }

      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
      $row_student = $studentQuery->fetch_assoc();

      $description = "<b>$name</b> has deleted <b>$row_student[name_s] $row_student[l_name_s]</b> reservation in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b> ";


      $notification['url'] = "reports";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // *
    // *
    // *
    // *
    // *
    // * KEEP RESERVE
    if ($notification['title'] == "Keep Reserve") {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[reserve_h]' ");
      $row_homestay = $homestayQuery->fetch_assoc();

      $agentsQuery2 = $link->query("SELECT name, l_name, photo FROM agents WHERE a_mail = '$notification[user_i_mail]' ");
      $row_agent2 = $agentsQuery2->fetch_assoc();
      if ($row_agent2) {
        $image = $row_agent2['photo'];
        $name = "$row_agent2[name] $row_agent2[l_name]";
      } else {
        $managersQuery2 = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$usuario' ");
        $row_manager2 = $managersQuery2->fetch_assoc();
        $image = $row_manager2['photo'];
        $name = "$row_manager2[name] $row_manager2[l_name]";
      }

      $studentQuery = $link->query("SELECT name_s, l_name_s, photo_s, id_student FROM pe_student WHERE mail_s = '$notification[report_s]' ");
      $row_student = $studentQuery->fetch_assoc();

      $description = "<b>$name</b> has kept the reserve of <b>$row_student[name_s] $row_student[l_name_s]</b> in <b>$row_homestay[name_h] $row_homestay[l_name_h]</b> ";

      $notification['url'] = "reports";
      $notification['issuerImage'] = $image;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }

    // ? REGISTER STUDENT
    if ($notification['title'] == "Register Student") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $studentData = getData("Student", "mail_s", $notification['reserve_h'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has registered a new student. click for details.";

      $notification['url'] = "student_info?art_id=$studentData[id_student]";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? REGISTER ACADEMY
    if ($notification['title'] == "Register Academy") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has registered a new Academy.";

      $notification['url'] = "#";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE NOW
    if ($notification['title'] == "Coordinator Reserve") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has assigned <b>" .$studentData['name_s']. " " .$studentData['l_name_s']. "</b> to <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE CONFIRMED A PENDING RESERVATION
    if ($notification['title'] == "Coordinator Condirmed Request") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has confirmed <b>" .$studentData['name_s']. "'s</b> reservation at <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVE REJECT A PENDING RESERVATION
    if ($notification['title'] == "Coordinator Rejected Request") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);
      $studentData = getData("Student", "mail_s", $notification['des'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has rejected <b>" .$studentData['name_s']. "'s</b> reservation at <b>" .$homestayData['h_name'].".</b>";

      $notification['url'] = "detail?art_id=".$homestayData['id_home'];
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }

    // ? HOMESTAY CERTIFIED
    /*if ($notification['title'] == "Homestay Certified") {
      $agentData = getData("Agent", "a_mail", $notification['user_i_mail'], $link);
      $managerData = getData("Manager", "mail", $notification['user_r'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['reserve_h'], $link);

      if (file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $agentData['photo'];
      else if (!file_exists("https://homebor.com/$agentData[photo]")) $agencyImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $agentData['name']. " " .$agentData['l_name']. "</b> has certified <b>". $homestayData['h_name']. "</b>";
      
      $notification['url'] = "detail?art_id=$homestayData[id_home]";
      $notification['issuerImage'] = $agencyImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    } */

    $allNotifications[] = $notification;
  }


  // ? DATA TO SEND
  $jsonToSend = array(
    'notifications' => $allNotifications,
    'newNotifications' => $total_new_notifications,
  );
}



// TODO HOMESTAY
if (strtolower($row_user['usert']) === "homestay") {
  // ? SELECT PE HOME
  $homestayQuery = $link->query("SELECT mail_h, name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$usuario' ");
  $row_homestay = $homestayQuery->fetch_assoc();


  // ?? SELECT NOTIFICATION
  $queryNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_homestay[mail_h]' AND state < '2' ORDER BY date_ DESC");
  $newNotifications = $link->query("SELECT * FROM notification WHERE user_r = '$row_homestay[mail_h]' AND state = '0' ");
  $total_new_notifications = mysqli_num_rows($newNotifications);


  while ($notification = mysqli_fetch_array($queryNotifications)) {
    if ($notification['title'] == 'Reservation Provider') {
      // ?? SELECT MANAGER
      $managerQuery = $link->query("SELECT photo FROM manager WHERE mail = '$notification[report_s]'");
      $row_manager = $managerQuery->fetch_assoc();
      if (empty($row_manager)) {
        // ?? SELECT AGENTS
        $agentsQuery2 = $link->query("SELECT id_m FROM agents WHERE a_mail = '$notification[report_s]'");
        $row_agent2 = $agentsQuery2->fetch_assoc();

        // ?? SELECT MANAGER
        $managerQuery = $link->query("SELECT photo FROM manager WHERE id_m = '$row_agent2[id_m]'");
        $row_manager = $managerQuery->fetch_assoc();
      }


      // ?? SELECT PE STUDENT
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, firstd, lastd FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
      $row_student = $studentQuery->fetch_assoc();


      if ($notification['user_i_l'] != 'NULL') $managerName = "$notification[user_i] $notification[user_i_l]";
      else $managerName = $notification['user_i'];

      if ($row_student['name_s'] != 'NULL') $studentName = "$row_student[name_s] $row_student[l_name_s]";
      else $studentName = $notification['name_s'];


      // * ISSUER IMAGE
      if (!file_exists("../../$row_manager[photo]")) $issuerImage = 'assets/emptys/profile-agent-empty.png';
      else $issuerImage = $row_manager['photo'];


      if ($notification['des'] === "A") $notification['des'] = 1;
      if ($notification['des'] === "B") $notification['des'] = 2;
      if ($notification['des'] === "C") $notification['des'] = 3;

      $description = "<b>iHomestay</b> has assigned <b>$studentName</b> in your <b>Room $notification[bedrooms]</b> in <b>Bed $notification[des]</b>.";


      $notification['url'] = "student_info?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
      $notification['start'] = $notification['start'];
      $notification['end'] = $notification['end_'];
      /*if ($notification['confirmed'] == '1') {
        if ($notification['status'] !== 'Rejected') $notification['confirm'] = true;
        else $notification['confirm'] = false;
      }*/
    }
    if ($notification['title'] == 'Reservation Provider Request') {
      // ?? SELECT MANAGER
      $managerQuery = $link->query("SELECT photo FROM manager WHERE mail = '$notification[report_s]'");
      $row_manager = $managerQuery->fetch_assoc();
      if (empty($row_manager)) {
        // ?? SELECT AGENTS
        $agentsQuery2 = $link->query("SELECT id_m FROM agents WHERE a_mail = '$notification[report_s]'");
        $row_agent2 = $agentsQuery2->fetch_assoc();

        // ?? SELECT MANAGER
        $managerQuery = $link->query("SELECT photo FROM manager WHERE id_m = '$row_agent2[id_m]'");
        $row_manager = $managerQuery->fetch_assoc();
      }


      // ?? SELECT PE STUDENT
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, firstd, lastd FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
      $row_student = $studentQuery->fetch_assoc();


      if ($notification['user_i_l'] != 'NULL') $managerName = "$notification[user_i] $notification[user_i_l]";
      else $managerName = $notification['user_i'];

      if ($row_student['name_s'] != 'NULL') $studentName = "$row_student[name_s] $row_student[l_name_s]";
      else $studentName = $notification['name_s'];


      // * ISSUER IMAGE
      if (!file_exists("../../$row_manager[photo]")) $issuerImage = 'assets/emptys/profile-agent-empty.png';
      else $issuerImage = $row_manager['photo'];


      if ($notification['des'] === "A") $notification['des'] = 1;
      if ($notification['des'] === "B") $notification['des'] = 2;
      if ($notification['des'] === "C") $notification['des'] = 3;

      $description = "<b>iHomestay</b> has requested a reservation for <b>$studentName</b> in your <b>Room $notification[bedrooms]</b> in <b>Bed $notification[des]</b>.";


      $notification['url'] = "student_info_not?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
      $notification['start'] = $notification['start'];
      $notification['end'] = $notification['end_'];
      if ($notification['confirmed'] == '1') {
        if ($notification['status'] !== 'Rejected') $notification['confirm'] = true;
        else $notification['confirm'] = false;
      }
    }
    // ? RESERVATION REJECTED PROVIDER
    if ($notification['title'] == 'Reservation Rejected Provider') {
      // ?? SELECT PE STUDENT
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
      $row_student = $studentQuery->fetch_assoc();


      // * ISSUER IMAGE
      if (!file_exists("https://homebor.com/$row_student[photo_s]")) $issuerImage = 'assets/emptys/profile-student-empty.png';
      else $issuerImage = $row_student['photo_s'];


      $description = "<b>$notification[user_i] $notification[user_i_l]</b> has rejected <b>$row_student[name_s] $row_student[l_name_s]</b> reservation at your homestay.";


      $notification['url'] = "student_info?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? RESERVATION REQUEST
    if ($notification['title'] == 'Reservation Request') {
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[user_i_mail]'");
      $row_student = $studentQuery->fetch_assoc();

      if ($row_student['name_s'] != 'NULL') $s_fullnames = $row_student['name_s'] . ' ' . $row_student['l_name_s'];
      else $s_fullnames = $notification['name_s'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL') {
        $issuerImage = 'assets/emptys/profile-student-empty.png';
      } else $issuerImage = $row_student['photo_s'];

      if ($notification['des'] === "A") $notification['des'] = 1;
      if ($notification['des'] === "B") $notification['des'] = 2;
      if ($notification['des'] === "C") $notification['des'] = 3;

      $description = "<b>$s_fullnames</b> wants a reservation in your <b>Room $notification[bedrooms]</b> in <b>Bed $notification[des]</b>.";

      $notification['url'] = "student_info_not?art_id=$row_student[id_student]";
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
      $notification['start'] = $notification['start'];
      $notification['end'] = $notification['end_'];
      if ($notification['confirmed'] == '1') {
        if ($notification['status'] !== 'Rejected') $notification['confirm'] = true;
        else $notification['confirm'] = false;
      }
    }
    // ? DELETE RESERVE / KEEP RESERVE
    if (
      $notification['title'] == 'Delete Reserve' ||
      $notification['title'] == 'Keep Reserve'
    ) {
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[report_s]'");
      $row_student = $studentQuery->fetch_assoc();

      $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
        $userImage1 = 'assets/emptys/profile-student-empty.png';
      else $userImage1 = $row_student['photo_s'];

      if ($notification['title'] == 'Delete Reserve') {
        $description = 'The Student <b>' . $row_student['name_s'] . ' ' . $row['l_name_s'] . '</b> reservation was removed from your home.';
      } else if ($notification['title'] == 'Keep Reserve') {
        $description = "The reservation of <b>" . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . "</b> will continue to be active in your house.";
      }

      $url = 'reports';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $userImage1;
    }
    // ? RESERVATION CHANGE BEFORE / RESERVATION CHANGE AFTER 
    if (
      $notification['title'] == 'Reservation Change Before' ||
      $notification['title'] == 'Reservation Change After'
    ) {
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[reserve_h]'");
      $row_student = $studentQuery->fetch_assoc();

      $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
        $userImage1 = 'assets/emptys/profile-student-empty.png';
      else $userImage1 = $row_student['photo_s'];

      if ($notification['title'] == 'Reservation Change Before') {
        $description = '<b>' . $row_manager['a_name'] . '</b> has changed your reservation of <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b>';
      } else if ($notification['title'] == 'Reservation Change After') {
        $description = '<b>' . $row_manager['a_name'] . '</b> has changed the reservation from <b>' . $row_student['name_s'] . ' ' . $row['l_name_s'] . '</b> to your home.';
      }

      $url = 'student_info?art_id=' . $row_student['id_student'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $userImage1;
    }
    // ? STUDENT ARRIVAL 3W / STUDENT ARRIVAL 15D / STUDENT ARRIVAL 3D
    if (
      $notification['title'] == 'Student Arrival 3w' ||
      $notification['title'] == 'Student Arrival 15d' ||
      $notification['title'] == 'Student Arrival 3d'
    ) {
      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[reserve_h]'");
      $row_student = $studentQuery->fetch_assoc();

      $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL') {
        $issuerImage = 'assets/icon/home 64.png';
      } else $issuerImage = $row_manager['photo'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL') {
        $userImage1 = 'assets/emptys/profile-student-empty.png';
      } else $userImage1 = $row_student['photo_s'];

      $startEvent = date_create($notification['start']);
      $start = date_format($startEvent, 'jS F, Y ');

      $description = '<b>' . $row_manager['a_name'] . '</b> reminds you that <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b>, will arrive on <b>' . $start . '</b>';

      $url = 'student_info?art_id=' . $row_student['id_student'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $userImage1;
    }
    // ? REPLY
    if ($notification['status'] == 'Reply') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
        $userImage1 = 'assets/emptys/profile-student-empty.png';
      else $userImage1 = $row_student['photo_s'];

      $description = '<b>' . $row_manager['a_name'] . '</b>, has responded to your report. Click to see details.';

      $url = 'reports';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;

    }
    // ? STATUS CHANGED
    if ($notification['title'] == 'Status Changed') {
      $paymentsQuery = $link->query("SELECT status_p FROM payments WHERE i_mail = '$usuario' AND reserve_s = '$notification[des]'");
      $row_payments = $paymentsQuery->fetch_assoc();

      $studentQuery = $link->query("SELECT id_student, name_s, l_name_s, mail_s, firstd, lastd, photo_s FROM pe_student WHERE mail_s = '$notification[des]'");
      $row_student = $studentQuery->fetch_assoc();

      $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      if (empty($row_student['photo_s']) || $row_student['photo_s'] === 'NULL')
        $userImage1 = 'assets/emptys/profile-student-empty.png';
      else $userImage1 = $row_student['photo_s'];

      if ($row_payments['status_p'] == 'Paid' || $row_payments['status_p'] == 'Budgeted') {
        $description = '<b>' . $row_manager['a_name'] . '</b>, has responded to your report. Click to see details.';
      } else if ($row_payments['status_p'] == 'Payable') {
        $description = '<b>' . $row_manager['a_name'] . '</b> reminds you that you have not paid <b>' . $row_student['name_s'] . ' ' . $row_student['l_name_s'] . '</b> bill. Click to process payment';
      }

      $url = '#'/* 'homestay_payments' */;

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $userImage1;
    }
    // ? WOLCOME TO HOMEBOR
    if($notification['title'] == 'Welcome to Homebor'){
      $managerQuery = $link->query("SELECT * FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] === 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      $description = $row_manager['a_name'] . " welcomes you to our Platform. Create your own events in our calendar.";

      $notification['url'] = "#";
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }
    // ? HOMESTAY CERTIFIED
    if ($notification['title'] == "Homestay Certified") {
      $managerData = getData("Manager", "mail", $notification['user_i_mail'], $link);
      $homestayData = getData("Homestay", "mail_h", $notification['user_r'], $link);

      if (file_exists("https://homebor.com/$managerData[photo]")) $managerImage = $managerData['photo'];
      else $agencyImage = "assets/emptys/profile-agent-empty.png";

      $description = "<b>". $managerData['a_name']. "</b> has confirmed your information";
      
      $notification['url'] = "#";
      $notification['issuerImage'] = $managerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    // ? EXTEND RESERVATION
    if ($notification['title'] == 'Extend Reservation') {
    }

    $allNotifications[] = $notification;
  }


  // ? DATA TO SEND
  $jsonToSend = array(
    'notifications' => $allNotifications,
    'newNotifications' => $total_new_notifications,
  );
}



// TODO STUDENT
if (strtolower($row_user['usert']) === "student") {
  // ? STUDENT QUERY
  $studentsQuery = $link->query("SELECT id_student, name_s, l_name_s, photo_s, mail_s FROM pe_student WHERE mail_s = '$usuario' ");
  $row_student = $studentsQuery->fetch_assoc();

  // ? NOTIFICATIONS QUERY
  $queryNotifications = $link->query("SELECT * FROM noti_student WHERE user_r = '$row_student[mail_s]' AND state < '2' ORDER BY date_ DESC");
  $newNotifications = $link->query("SELECT * FROM noti_student WHERE user_r = '$row_student[mail_s]' AND confirmed = '0'
    AND state = '0' ");
  $total_new_notifications = mysqli_num_rows($newNotifications);


  while ($notification = mysqli_fetch_array($queryNotifications)) {

    if ($notification['des'] == 'Rejected') {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      $description = 'Sorry <b>' . $row_student['name_s'] . '! ' . $notification['h_name'] . ' </b>was rechazed your request';

      $url = 'detail.php?art_id=' . $row_homestay['id_home'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Reservation Change') {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      $managerQuery = $link->query("SELECT mail, id_m, a_name, photo FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      $description = '<b>' . $row_manager['a_name'] . '</b> has changed your stay to <b>' . $row_homestay['h_name'] . '</b>';

      $url = 'detail.php?art_id=' . $row_homestay['id_home'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Student Confirmed') {
      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      $description = '<b>' . $notification['h_name'] . '</b> has accepted your reservation request.';

      $url = 'detail.php?art_id=' . $row_homestay['id_home'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Delete Reserve' || $notification['des'] === 'Keep Reserve') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      if ($notification['des'] === 'Delete Reserve') {
        $description = 'Sorry <b>' . $row_student['name_s'] . '</b>! <b> Your reservation has been removed from ' . $notification['h_name'];
      } else if ($notification['des'] === 'Keep Reserve') {
        $description = 'Your reservation at <b>' . $notification['h_name'] . '</b> is still active.';
      }

      $url = '#';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Flight Information') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      $description = $row_student['name_s'] . ', fill in your details! Click here before reserving a room to enter the flight data';

      $url = 'edit_student';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Student Confirmed Provider' || $notification['des'] == 'Student Rejected Provider') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[change_house]'");
      $row_manager = $managerQuery->fetch_assoc();

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      if ($notification['des'] == 'Student Confirmed Provider') {
        $description = '<b>' . $row_manager['a_name'] . '</b> has assigned your reservation to ' . $row_homestay['h_name'] . ' Click for details';
      } else if ($notification['des'] == 'Student Rejected Provider') {
        $description = $row_manager['a_name'] . '</b> has rejected your request in ' . $notification['h_name'];
      }

      $url = 'detail?art_id=' . $row_homestay['id_home'];

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Reassigned') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      $description = $row_student['name_s'] . ', your provider has assigned you new houses, check the details of them.';

      $url = 'index';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    if ($notification['des'] == 'Cancel Reservation') {
      $managerQuery = $link->query("SELECT mail, photo name, l_name, a_name FROM manager WHERE mail = '$notification[user_i_mail]'");
      $row_manager = $managerQuery->fetch_assoc();

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[change_house]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      if (empty($row_homestay['phome']) || $row_homestay['phome'] === 'NULL')
        $userImage1 = 'assets/emptys/frontage-empty.png';
      else $userImage1 = $row_homestay['phome'];

      $description = '<b>' . $row_manager['a_name'] . '</b>, has started a report. click see the information.';

      $url = '#';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = $userImage1;
    }
    
    if ($notification['des'] == 'Welcome to Homebor') {
      $managerData = getData("Manager", "id_m", $agentData['id_m'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      $description = '<b>' . $managerData['a_name'] . '</b> welcomes you to our Platform. Check your information and edit it if necessary.';

      $url = '#';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }
    
    if ($notification['des'] == 'Coordinator Check') {
      $managerData = getData("Manager", "id_m", $agentData['id_m'], $link);
      $studentData = getData("Student", "mail_s", $notification['report_s'], $link);

      if (empty($row_manager['photo']) || $row_manager['photo'] == 'NULL')
        $issuerImage = 'assets/icon/home 64.png';
      else $issuerImage = $row_manager['photo'];

      $description = 'Wait for us to confirm your information to reserve the room of your choice!';

      $url = '#';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      $notification['userImage1'] = false;
    }

    if ($notification['des'] == 'Today' || $notification['des'] == '1 Day' || $notification['des'] == '2 Days' || $notification['des'] == '3 Days' || $notification['des'] == '4 Days' || $notification['des'] == '5 Days') {

      $homestayQuery = $link->query("SELECT name_h, l_name_h, phome, id_home, h_name FROM pe_home WHERE mail_h = '$notification[user_i_mail]'");
      $row_homestay = $homestayQuery->fetch_assoc();

      if (empty($row_homestay['phome']) || $row_homestay['phome'] == 'NULL')
        $issuerImage = 'assets/emptys/frontage-empty.png';
      else $issuerImage = $row_homestay['phome'];

      if ($notification['des'] == 'Today') $expirationDay = 'Today';
      if ($notification['des'] == '1 Day') $expirationDay = 'Tomorrow';
      if ($notification['des'] == '2 Days') $expirationDay = '2 Days';
      if ($notification['des'] == '3 Days') $expirationDay = '3 Days';
      if ($notification['des'] == '4 Days') $expirationDay = '4 Days';
      if ($notification['des'] == '5 Days') $expirationDay = '5 Days';

      $description = 'The reservation at <b>' . $notification['h_name'] . '</b> will expire ' . $expirationDay . '. Click here to extend session.';

      $url = '#';

      $notification['url'] = $url;
      $notification['issuerImage'] = $issuerImage;
      $notification['description'] = $description;
      // $notification['userImage1'] = $userImage1;
    }

    $allNotifications[] = $notification;
  }


  // ? DATA TO SEND
  $jsonToSend = array(
    'notifications' => $allNotifications,
    'newNotifications' => $total_new_notifications,
  );
}


echo json_encode($jsonToSend), exit;