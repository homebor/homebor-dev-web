// ! VARIABLES
const ACADEMYS = [];
let ACADEMYS_FOUND = [];
let ALL_ACADEMYS = [];
let academySelected = "";

// ! FUNCTIONS
// ? OBTENER LOS PARAMETROS DEL URL
const searchParams = new URLSearchParams(window.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
// console.log(searchParams);
const _ID = searchParams?.get("art_id");

async function getAcademys() {
  document.querySelector("#n_a").innerHTML = `<option hidden='option' selected disabled>Select School</option>`;
  const formData = new FormData();
  formData.set("id_user", _ID);
  const DATA = { method: "POST", body: formData };

  const jsonAcademy = await fetch("./../helpers/get_academys.php", DATA);
  const resultAcademy = await jsonAcademy.json();
  academySelected = resultAcademy.idAcademySelected?.trim();

  resultAcademy.academys.forEach((academy) => {
    if (academy.id_ac !== "0") {
      const $clone = document.createElement("option");
      for (const key in academy) {
        if (academy.id_ac === resultAcademy.idAcademySelected?.trim()) {
          $clone.selected = true;
        }
        $clone.textContent = `${academy.name_a}, ${academy.dir_a}`;
        $clone.value = `${academy.id_ac}`;
      }
      document.querySelector("#n_a").appendChild($clone);
      ACADEMYS.push(academy);
    }
  });
}

function getAcademysByCity(cityAcademy) {
  document.querySelector("#n_a").innerHTML = `
    <option hidden='option' selected disabled>Select School</option>
  `;
  ACADEMYS_FOUND = [];
  ALL_ACADEMYS = [];
  ACADEMYS.forEach((academy) => {
    if (academy.id_ac !== "0") {
      if (academy.city_a === cityAcademy) ACADEMYS_FOUND.push(academy);
      else if (academy.city_a !== cityAcademy) ALL_ACADEMYS.push(academy);
      if (academy.id_ac === academySelected) ACADEMYS_FOUND.push(academy);
    }
  });

  let SHOW_ACADEMYS;
  ACADEMYS_FOUND.length === 0 ? (SHOW_ACADEMYS = ALL_ACADEMYS) : (SHOW_ACADEMYS = ACADEMYS_FOUND);
  SHOW_ACADEMYS.push({ name_a: "Other", dir_a: "", id_ac: "Other" });

  SHOW_ACADEMYS.forEach((academy) => {
    const $clone = document.createElement("option");
    for (const key in academy) {
      if (academy.id_ac === academySelected) {
        $clone.selected = true;
      }
      $clone.textContent = `${academy.name_a}, ${academy.dir_a}`;
      $clone.value = `${academy.id_ac}`;
    }
    document.querySelector("#n_a").appendChild($clone);
  });
}

// ! EVENTS

document.addEventListener("DOMContentLoaded", () => {
  getAcademys();
});

document.addEventListener("change", (e) => {
  if (e.target.id === "destination_c") getAcademysByCity(e.target.value);
});
