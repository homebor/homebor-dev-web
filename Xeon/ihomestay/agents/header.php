<?php
$queryUser = $link->query("SELECT usert FROM users WHERE mail = '$usuario'");
$row_user = $queryUser->fetch_assoc();

if (strtolower($row_user['usert']) != 'agent') header("location: ../logout.php");
?>


<link rel="stylesheet" href="assets/css/header.css">
<link rel="stylesheet" href="../assets/css/notifications.css">

<header class="m-0 p-0 header-container">
  <nav class="m-0 d-flex align-items-center justify-content-around header-navbar">
    <!-- // * LOGO -->
    <a href="index" class="header-logo">
      <figure class="m-0 p-0">
        <img src="../assets/iHomestay/logos/iHomestay_logo.png" alt="ihomestay, homebor" class="w-100 h-auto">
      </figure>
    </a>

    <!-- // * LINKS -->
    <ul class="m-0 p-0 d-flex flex-column flex-lg-row align-items-center justify-content-lg-center header-links">
      <div class="d-lg-none"><br><br></div>

      <!-- // * HOMESTAYS -->
      <div class="m-0 mb-4 mb-lg-0 mr-lg-5 p-0 link-dropdown">
        <div class="m-0 p-0">
          <span>Homestays</span>
          <figure><img src="../assets/icon/arrow.png" alt="homebor, ihomestay" class="w-100 h-auto"></figure>
        </div>

        <div class="mb-0 p-0 p-2 d-flex flex-column">
          <a href="directory_homestay">Homestay Directory</a>
          <a href="non_certified_hom">Non-certified Homestay</a>
          <a href="homestay">Register New Homestay</a>
        </div>
      </div>


      <!-- // * STUDENTS -->
      <div class="m-0 mb-4 mb-lg-0 mr-lg-5 p-0 link-dropdown">
        <div class="m-0 p-0">
          <span>Students</span>
          <figure><img src="../assets/icon/arrow.png" alt="homebor, ihomestay" class="w-100 h-auto"></figure>
        </div>

        <div class="mb-0 p-0 p-2 d-flex flex-column">
          <a href="directory_students">Students Directory</a>
          <a href="directory_history">Students History</a>
          <a href="student_profile">Register New Student</a>
        </div>
      </div>


      <!-- // * SCHOOLS -->
      <div class="m-0 mb-4 mb-lg-0 mr-lg-5 p-0 link-dropdown">
        <div class="m-0 p-0">
          <span>Schools</span>
          <figure><img src="../assets/icon/arrow.png" alt="homebor, ihomestay" class="w-100 h-auto"></figure>
        </div>

        <div class="mb-0 p-0 p-2 d-flex flex-column">
          <a href="directory_schools">Directory Schools</a>
          <a href="academy_register">Register New School</a>
        </div>
      </div>


      <!-- // * UTILITIES -->
      <div class="m-0 mb-4 mb-lg-0 mr-lg-5 p-0 link-dropdown">
        <div class="m-0 p-0">
          <span>Utilities</span>
          <figure><img src="../assets/icon/arrow.png" alt="homebor, ihomestay" class="w-100 h-auto"></figure>
        </div>

        <div class="mb-0 p-0 p-2 d-flex flex-column">
          <a href="agent_profile">Profile</a>
          <a href="reports">Reports</a>
          <a href="directory_disabled">Students Disabled</a>
          <a href="list_delete_user">Users to Delete</a>
        </div>
      </div>


      <!-- // * NOTIFICATION BELL BUTTON -->
      <button id="bell-btn" class="d-none d-lg-block bg-transparent shadow-none notification-btn" data-show="false">
        <img src="../assets/icon/notification 64.png" class="m-0 p-0" width="32" height="32">
        <!-- // * NEW NOTIFICATIONS BADGE -->
        <span id="new-notifications" class="new-notifications"></span>
      </button>



      <div class="d-lg-none"><br></div>

      <!-- // * LOGOUT -->
      <a href="../logout" class="m-0 d-flex d-lg-none align-items-center justify-content-center header-logout">
        <span class="m-0 mr-2 p-0">Logout</span>
        <figure class="m-0 p-0 d-flex"><img src="../assets/icon/logout2.png" class="w-100 h-auto"></figure>
      </a>


      <div class="d-lg-none"><br><br></div>
    </ul>


    <!-- // * NOTIFICATION BELL BUTTON -->
    <button id="bell-btn" class="d-lg-none bg-transparent shadow-none notification-btn" data-show="false">
      <img src="../assets/icon/notification 64.png" class="m-0 p-0" width="32" height="32">
      <!-- // * NEW NOTIFICATIONS BADGE -->
      <span id="new-notifications" class="new-notifications"></span>
    </button>



    <!-- // * NOTIFICATIONS CONTAINER  -->
    <ul id="notification-list" class="m-0 p-0 pt-5 pt-lg-0 d-none flex-column notification-list"
      data-total-notifications="You have 0 notification">
      <button id="delete_all" class="btn_delete_all" type="button">Delete All</button>
      <span class="d-lg-none close-notifications">X</span>
      <!-- ALL NOTIFICATIONS -->
    </ul>


    <!-- // * SHOW NAVBAR MENU -->
    <button id="show-menu" class="d-lg-none show-menu">
      <figure><img src="../assets/icon/menu.png" width="100%" height="auto"></figure>
      <figure><i></i></figure>
    </button>

    <!-- // * LOGOUT -->
    <a href="../logout" class="m-0 d-none d-lg-flex align-items-center justify-content-center header-logout">
      <span class="m-0 mr-2 p-0">Logout</span>
      <figure class="m-0 p-0 d-flex"><img src="../assets/icon/logout2.png" class="w-100 h-auto"></figure>
    </a>

  </nav>
</header>

<input type="hidden" id="type_user" value="agent">


<template id="notification-template">
  <!-- // * NOTIFICATION ITEM  -->
  <a href="#" data-id-note="" class="m-0 p-0 d-flex flex-column justify-content-start notification-item">
    <!-- // ** NOTIFICATION HEADER  -->
    <!-- <div class="p-0 m-0 py-lg-0 py-4 d-flex w-100 align-items-center justify-content-between px-4 message-header">
                <h5 class="m-0 p-0" data-title>Message Title</h5>
                <small data-time-ago>3 days ago</small> -->
    <!-- <img src="../assets/svg/icon-trash.svg" title="Delete notification" alt="delete"> -->
    <!-- </div> -->



    <!-- // ** NOTIFICATION BODY  -->
    <div class="pl-4 d-flex align-items-center justify-content-start message-body">
      <img src="../assets/icon/home 64.png" class="mr-4" data-issuer-image>
      <img src="../assets/icon/home 64.png" class="m-0 p-0" data-image-1>
      <p class="text-left m-0 p-0 text-muted" data-message></p>
      <small class="d-none m-0 p-0 text-center text-muted" data-student-date></small>
    </div>



    <!-- // ** NOTIFICATION FOOTER  -->
    <div class="w-100 px-3 pb-2 d-flex justify-content-around align-items-center message-footer">
      <span data-time-ago></span>
      <div class="m-0 p-0 d-flex justify-content-center align-items-center message-buttons">
        <button class="btn mr-2 confirm-btn" data-confirm-student>Confirm</button>
        <button class="btn reject-btn" data-reject-student>Reject</button>
        <span class="confirmed-msg">Confirmed</span>
        <span class="rejected-msg">Rejected</span>
      </div>
    </div>

  </a>
</template>

<script src="https://unpkg.com/axios@1.0.0/dist/axios.min.js"></script>
<script src="assets/js/notifications.js" type="module"></script>
<script src="../assets/js/control_events.js?ver=1.0"></script>