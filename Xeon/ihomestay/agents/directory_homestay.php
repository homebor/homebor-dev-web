<?php

session_start();
$usuario = $_SESSION['username'];

include '../../xeon.php';

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.1">

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0.12">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0.12">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0.12">
  <link rel="stylesheet" href="../assets/css/leaflet.css?ver=1.0.12">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.12">
  <link rel="stylesheet" href="assets/css/directory_homestay.css?ver=1.0.12">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!-- //TODO DATERANGEPICKER -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <title>Homestay Directory</title>
</head>

<body>

  <!-- //TODO INCLUDE HEADER -->
  <?php include 'header.php' ?>

  <!-- //TODO CONTAINER ALL PAGE -->
  <main class="mx-auto main__directory" id="">

    <input type="hidden" id="mail_agent" value="<?php echo $usuario ?>">

    <!-- //TODO SEARCH FIELD -->
    <section class="section__search_container">
      <div class="d-flex justify-content-center container_input">
        <input type="text" id="input__search" class="input__search" placeholder="Type your House" name="search">
        <label for="input__search" class="d-flex m-0 p-0 align-items-center label__search fa fa-search"></label>
        <div class="div__button_filter d-flex d-sm-none">
          <button id="button__filter" class="btn btn__filter"><i class="icon__filter"></i></button>
        </div>
      </div>
    </section>

    <!-- //TODO SECTION HOUSE LIST -->
    <section id="section__directory" class="section__directory d-flex row justify-content-between">
      <section id="section__filters" class="section__filters d-none d-sm-block">
        <h3 class="m-0 p-0">Filter Houses by</h3>

        <!-- //TODO TAGS SEARCH -->
        <article id="filter__tags" class="filter__tags ml-0 row d-flex">
          <p class="filter__tags-search d-none" id="filter-status"></p>
          <p class="filter__tags-search d-none" id="filter-type-room"></p>
          <p class="filter__tags-search d-none" id="filter-gender"></p>
          <p class="filter__tags-search d-none" id="filter-food"></p>
          <p class="filter__tags-search d-none" id="filter-diet"></p>
          <p class="filter__tags-search d-none" id="filter-pets"></p>
          <p class="filter__tags-search d-none" id="filter-type-bed"></p>
          <p class="filter__tags-search d-none" id="filter-age"></p>
          <p class="filter__tags-search d-none" id="filter-smoke"></p>
        </article>

        <!-- //TODO AVAILABILITY FILTER -->
        <article id="filter__availability" class="filter__availability">
          <label for="" class="label__filtered"> Status House </label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_only_available"
                class="checkbox__filter checkbox__filter_only_available" value="true" data-filter="status">
              <label for="checkbox__filter_only_available" data-id="filter_only_available"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Only Avalible</p>
              </label>
            </div>

            <!-- //TODO DATE -->
            <article id="filter__date" class="filter__date justify-content-center d-none">
              <input type="text" id="input__date_filter" name="date_filter" class="rangepickerdate input__filter_type"
                data-filter="date" readonly>
            </article>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__show_all" class="checkbox__filter checkbox__show_all" value=""
                data-filter="status">

              <label for="checkbox__show_all" data-id="show_all"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Show All</p>
              </label>
            </div>

          </div>

        </article>

        <!-- //TODO BACKGROUND LANGUAGE FILTER -->
        <article id="filter__main_city" class="parent__check filter__main_city d-flex justify-content-center">
          <label for="" class="label__filtered">Main City</label>

          <select name="" id="input__background_language" class="custom-select mt-1" name="main_city"
            data-filter="mainCity">
            <option hidden="option" selected disabled>Select Option</option>
            <option value="Toronto">Toronto</option>
            <option value="Montreal">Montreal</option>
            <option value="Ottawa">Ottawa</option>
            <option value="Quebec">Quebec</option>
            <option value="Calgary">Calgary</option>
            <option value="Vancouver">Vancouver</option>
            <option value="Victoria">Victoria</option>
            <option value="Waterloo">Waterloo</option>
            <option value="Guelph">Guelph</option>
          </select>

        </article>

        <!-- //TODO TYPE OF ROOM FILTER -->
        <article id="filter__type_of_room" class="filter__type_of_room">
          <label for="" class="label__filtered"> Type Of Room </label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_single" class="checkbox__filter checkbox__filter_single"
                value="single" data-filter="typeRoom">
              <label for="checkbox__filter_single" data-id="filter_single"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Single</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_share" class="checkbox__filter checkbox__filter_share"
                value="share" data-filter="typeRoom">
              <label for="checkbox__filter_share" data-id="filter_share"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Share</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_executive" class="checkbox__filter checkbox__filter_executive"
                value="executive" data-filter="typeRoom">
              <label for="checkbox__filter_executive" data-id="filter_executive"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Executive</p>
              </label>
            </div>


          </div>
        </article>

        <!-- //TODO GENDER FILTER -->
        <article id="filter__gender" class="filter__gender">
          <label for="" class="label__filtered">Gender Preference</label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_male" class="checkbox__filter checkbox__filter_male"
                value="male" data-filter="gender">
              <label for="checkbox__filter_male" data-id="filter_male"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Male</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_female" class="checkbox__filter checkbox__filter_female"
                value="female" data-filter="gender">
              <label for="checkbox__filter_female" data-id="filter_female"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Female</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_any_gender"
                class="checkbox__filter checkbox__filter_any_gender" value="any" data-filter="gender">
              <label for="checkbox__filter_any_gender" data-id="filter_any_gender"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Any</p>
              </label>
            </div>

          </div>
        </article>

        <!-- //TODO FOOD SERVICES FILTER -->
        <article id="filter__smokers" class="filter__smokers">
          <label for="" class="label__filtered">Food Services </label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_food_y" class="checkbox__filter checkbox__filter_food_y"
                value="yes" data-filter="food_s">
              <label for="checkbox__filter_food_y" data-id="filter_food_y"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Yes</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_food_n" class="checkbox__filter checkbox__filter_food_n"
                value="no" data-filter="food_s">
              <label for="checkbox__filter_food_n" data-id="filter_food_n"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">No</p>
              </label>
            </div>

          </div>
        </article>

        <!-- //TODO FOOD DIET FILTER -->
        <article id="filter__food_fiet" class="filter__food_fiet d-none">
          <label for="" class="label__filtered"> Special Diet </label>
          <!-- <p class="sub_title_filter_all sub_title_filter">House Pets</p> -->

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_vegetarians"
                class="checkbox__filter checkbox__filter_vegetarians" value="vegetarians" data-filter="diet">
              <label for="checkbox__filter_vegetarians" data-id="filter_vegetarians"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Vegetarians</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_halal" class="checkbox__filter checkbox__filter_halal"
                value="halal" data-filter="diet">
              <label for="checkbox__filter_halal" data-id="filter_halal"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Halal</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_lactose" class="checkbox__filter checkbox__filter_lactose"
                value="lactose" data-filter="diet">
              <label for="checkbox__filter_lactose" data-id="filter_lactose"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Lactose Intolerant</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_gluten" class="checkbox__filter checkbox__filter_gluten"
                value="gluten" data-filter="diet">
              <label for="checkbox__filter_gluten" data-id="filter_gluten"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Gluten Free</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_kosher" class="checkbox__filter checkbox__filter_kosher"
                value="kosher" data-filter="diet">
              <label for="checkbox__filter_kosher" data-id="filter_kosher"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Kosher</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_pork" class="checkbox__filter checkbox__filter_pork"
                value="pork" data-filter="diet">
              <label for="checkbox__filter_pork" data-id="filter_pork"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Pork</p>
              </label>
            </div>

          </div>
        </article>

        <!-- //TODO PETS FILTER -->
        <article id="filter__pets" class="filter__pets d-flex justify-content-center">
          <label for="" class="">Pets</label>

          <div class="parent__check parent__pets">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_yes" class="checkbox__filter checkbox__filter_yes" value="yes"
                data-filter="pets">
              <label for="checkbox__filter_yes" data-id="filter_yes"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Yes</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_no" class="checkbox__filter checkbox__filter_no" value="no"
                data-filter="pets">
              <label for="checkbox__filter_no" data-id="filter_no"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">No</p>
              </label>
            </div>

          </div>
        </article>

        <!-- //TODO TYPE OF BED FILTER -->
        <article id="filter__type_of_bed" class="filter__type_of_bed">
          <label for="" class="label__filtered"> Type Of Bed</label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_twin" class="checkbox__filter checkbox__filter_twin"
                value="twin" data-filter="typeBed">
              <label for="checkbox__filter_twin" data-id="filter_twin"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Twin</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_double" class="checkbox__filter checkbox__filter_double"
                value="double" data-filter="typeBed">
              <label for="checkbox__filter_double" data-id="filter_double"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Double</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_bunker" class="checkbox__filter checkbox__filter_bunker"
                value="bunker" data-filter="typeBed">
              <label for="checkbox__filter_bunker" data-id="filter_bunker"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Bunk</p>
              </label>
            </div>


          </div>
        </article>

        <!-- //TODO AGE FILTER -->
        <article id="filter__age" class="filter__age">
          <label for="" class="label__filtered">Age Preference</label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_teenager" class="checkbox__filter checkbox__filter_teenager"
                value="teenager" data-filter="age">
              <label for="checkbox__filter_teenager" data-id="filter_teenager"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Teenager</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_adult" class="checkbox__filter checkbox__filter_adult"
                value="adult" data-filter="age">
              <label for="checkbox__filter_adult" data-id="filter_adult"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Adult</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_any_age" class="checkbox__filter checkbox__filter_any_age"
                value="any" data-filter="age">
              <label for="checkbox__filter_any_age" data-id="filter_any_age"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Any</p>
              </label>
            </div>

          </div>
        </article>

        <!-- //TODO BACKGROUND LANGUAGE FILTER -->
        <article id="filter__background_language"
          class="parent__check filter__background_language d-flex justify-content-center">
          <label for="" class="label__filtered">Background Language</label>

          <input type="text" id="input__background_language" class="mt-1 text-left"
            placeholder="Type Background Language" name="background_language" data-filter="language">
        </article>

        <!-- //TODO SMOKERS POLICIES FILTER -->
        <article id="filter__smokers" class="filter__smokers">
          <label for="" class="label__filtered">Smoking Policies </label>

          <div class="parent__check">

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_outside" class="checkbox__filter checkbox__filter_outside"
                value="Outside-OK" data-filter="smoke">
              <label for="checkbox__filter_outside" data-id="filter_outside"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Outside</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_inside" class="checkbox__filter checkbox__filter_inside"
                value="Inside-OK" data-filter="smoke">
              <label for="checkbox__filter_inside" data-id="filter_inside"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Inside</p>
              </label>
            </div>

            <div class="d-flex justify-content-start align-items-center">
              <input type="checkbox" id="checkbox__filter_strictly" class="checkbox__filter checkbox__filter_strictly"
                value="strictly Non-Smoking" data-filter="smoke">
              <label for="checkbox__filter_strictly" data-id="filter_strictly"
                class="d-flex justify-content-center align-items-center">
                <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" class="svg__check" aria-hidden="true"
                  role="presentation" focusable="false">
                  <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                </svg>

                <p class="m-0 px-3">Non-Smoking</p>
              </label>
            </div>

          </div>
        </article>

        <div class="div__button_close_filter d-flex justify-content-center d-sm-none">
          <button id="close_filter" class="fa fa-arrow-up"></button>
        </div>
      </section>
      <section class="houses">
        <section class="section__houses_list" data-all="no">
          <template id="template_house_list">
            <div class="d-flex flex-column flex-md-row mb-5 mb-lg-4 pb-2 pb-md-0 bg-white card-house">
              <!-- // ** CARD HEADER-->
              <div class="p-0 d-flex flex-column align-items-center card-header" data-house-profile title="See profile">
                <img src="../assets/emptys/frontage-empty.png" class="mx-auto img__house" alt="house, homestay">
                <div id="status" data-house-status>Available</div>
              </div>

              <!-- // ** CARD BODY CONTAINER-->
              <div class="w-100 px-0 py-2 card-body">
                <!-- // * CARD MAIN INFORMATION-->
                <div class="py-0 px-3 main-information">
                  <!-- // ** HOUSE NAME  -->
                  <label class="w-100 h4 m-0 d-flex justify-content-between align-items-center name_house"
                    data-house-name></label>

                  <hr class="my-2 pt-1">

                  <!-- // ** ROW 2  -->
                  <dl class="m-0 mb-3 d-flex align-items-center">
                    <dt>Address:</dt>
                    <dd class="m-0 px-2 text__direction" data-house-direction></dd>
                  </dl>


                  <!-- // ** ROW 3  -->
                  <div class="pb-0 mb-3 d-flex align-items-center justify-content-between">
                    <dl class="m-0">
                      <dt>Gender</dt>
                      <dd class="m-0 gender" data-house-gender-preference></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Age</dt>
                      <dd class="m-0 age" data-house-age-preference></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Bedrooms</dt>
                      <dd class="m-0 rooms" data-house-rooms></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Language</dt>
                      <dd class="m-0 language" data-house-background-language=""></dd>
                    </dl>
                  </div>


                  <!-- // ** ROW 4  -->
                  <div class="pb-0 mb-0 d-flex align-items-center justify-content-between">
                    <dl class="m-0">
                      <dt>Smoking Policies</dt>
                      <dd class="m-0 smokers_politics" data-house-smokers-preference></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Food Service</dt>
                      <dd class="m-0 food_s" data-house-food-service=""></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Special Diet</dt>
                      <dd class="m-0 diet" data-house-special-diet=""></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Pets</dt>
                      <dd class="m-0 pets" data-house-pets-preference></dd>
                    </dl>

                    <dl class="m-0">
                      <dt>Main City</dt>
                      <dd class="m-0 main_city"></dd>
                    </dl>

                    <dl class="m-0 d-none">
                      <dd class="m-0 type_room"></dd>
                    </dl>

                    <dl class="m-0 d-none">
                      <dd class="m-0 type_bed"></dd>
                    </dl>

                    <dl class="m-0 d-none">
                      <dd class="m-0 type_diet"></dd>
                    </dl>

                    <dl class="m-0 d-none">
                      <dd class="m-0 event_start"></dd>
                    </dl>

                  </div>



                </div>
                <div class="div__buttons_actions">
                  <a href="#" id="profile">Profile</a>
                  <a href="#" id="edit">Edit Home</a>
                </div>
              </div>
          </template>

        </section>
        <!-- //TODO HOUSE EMPTY -->
        <div class="d-none flex-column justify-content-center align-items-center house_empty_container">
          <img src="../assets/emptys/homestay-list-empty.png" alt="">
          <h4>Sorry, you don't have Houses with this filter, try another filter</h4>
        </div>
        <div class="pagination d-flex justify-content-center">
          <button id="btn__more" class="d-flex">More Houses</button>
          <p class="pag d-none"></p>
        </div>
      </section>
    </section>


  </main>

  <?php include 'footer.php' ?>

  <script>
  $(".rangepickerdate").daterangepicker({
      opens: "right",
    },
    function(start, end, label) {
      $(".result_date").val(start.format("YYYY-MM-DD") + " - " + end.format("YYYY-MM-DD"));
    }
  );
  </script>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/js/leaflet.js"></script>
  <script src="../assets/js/jQuery.MultiFile.min.js"></script>
  <!-- <script src="../assets/js/custom.js"></script> -->
  <script src="../assets/js/map-leaflet.js"></script>
  <script src="assets/js/directory_homestay.js?ver=1.0.12" type="module"></script>
</body>

</html>