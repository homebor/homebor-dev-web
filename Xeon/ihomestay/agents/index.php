<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/index.css">

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Home</title>
</head>

<body>
  <!-- // TODO HEADER -->
  <?php include 'header.php';  ?>

  <main class="main__homepage ml-auto mr-auto">
    <!-- //* FIRST ROW WELCOME -->
    <div class="div__border d-md-flex row ml-0 justify-content-between">
      <!-- // ? MESSAGES SECTION -->
      <article class="col-12 col-lg-5 col-xl-6 div__first-row">
        <input type="hidden" name="agency_id" id="agency_id">
        <h2 id="coordinator-name">Hi, Sophia Vergara</h2>
        <h5>Ready to start the day? let's review your students and homestays</h5>
        <!-- //TODO NEWS CONTAINER -->
        <div class="container__news d-none">
          <span class="bubble d-flex">
            <p class="m-0 p-0">1</p>
          </span>
          <label for="">Last News</label>

          <article id="news_notification" class="news_notification">
            <!-- // TODO NEWS -->
            <template id="template__news">
              <a id="notification-container" class="d-flex flex-column p-2 pl-3">
                <p class="title_notification">Reservation Request</p>
                <p class="content_notification"> georgemi has registeres a new house</p>
              </a>
            </template>
          </article>
        </div>
      </article>
      <!-- // ? IMAGE ROW -->
      <article class="col-lg-6 d-lg-flex justify-content-end align-items-center pr-0"><img
          src="../assets/img/first-column.png" alt="First Column Image" class="image first_row"></article>
    </div>

    <!-- //* TEXT ABOVE -->
    <p class="text__above">Status</p>

    <!-- //* SECON ROW STATISTICS-->
    <div class="div__border d-md-flex justify-content-center align-items-center">
      <article class="col-md-5 p-3">
        <div class="container__statistics-left mb-3 d-flex justify-content-center align-items-center">
          <div class="col-5 col-sm-3 col-md-6 col-xl-5 p-0">
            <img src="../assets/img/second-row-student.png" class="image second_row" alt="">
          </div>
          <div class="col-7 col-sm-5 col-md-6 col-xl-5 p-0 py-3 text-center position-relative">
            <h3 id="percent-student-confirmed" class="text-center mb-0 pb-0"></h3>
            <p class="mb-0">Students Confirmed</p>
          </div>
        </div>
        <div class="container__statistics-left d-flex justify-content-center align-items-center">
          <div class="col-5 col-sm-5 col-md-6 col-xl-5 p-0">
            <img src="../assets/img/second-row-homestay.png" class="image second_row" alt="">
          </div>
          <div class="col-7 col-sm-5 col-md-6 col-xl-5 p-0 py-3 text-center position-relative">
            <h3 id="percent-homestay-certified" class="text-center mb-0 pb-0"></h3>
            <p class="mb-0">Homestay Certified</p>
          </div>
        </div>
      </article>
      <article class="col-md-7 p-3">
        <div class="container__statistics-right">
          <article class="mt-md-0 py-3 py-xl-0 d-md-flex justify-content-around align-items-center agency-graphics">
            <!-- <aside class="position-absolute pie-legends">
          <span id="toronto-legend">Toronto</span>
          <span id="montreal-legend">Montreal</span>
          <span id="ottawa-legend">Ottawa</span>
          <span id="quebec-legend">Quebec</span>
          <span id="calgary-legend">Calgary</span>
          <span id="vancouver-legend">Vancouver</span>
          <span id="victoria-legend">Victoria</span>
          <span id="waterloo-legend">Waterloo</span>
          <span id="guelph-legend">Guelph</span>
        </aside> -->


            <div class="py-4 pl-md-4"><canvas id="forCity"
                class="d-flex justify-content-center align-items-center statistics-cake"></canvas></div>
            <div class="d-flex justify-content-center align-items-center w-100">
              <h3 class="title__statistics-right">Houses for City</h3>
            </div>
            <br class="d-lg-none">
            <!-- <div><canvas id="forCertified"></canvas></div> -->

          </article>
        </div>
      </article>
    </div>

    <p class="text__above">Overview</p>

    <!-- //* THIRD ROW OVERVIEW -->
    <div class="div__border d-flex justify-content-center align-items-center py-5">
      <div class="container__provider_overview d-flex row align-items-center mt-0 py-2">
        <article class="overview text-center col-md-3 px-0">
          <p>Total Students</p>
          <h3 id="total-student"></h3>
        </article>
        <article class="overview text-center col-md-3 px-0">
          <p>Active Students in Homestay</p>
          <h3 id="student-active-homestay">31</h3>
        </article>
        <article class="overview text-center col-md-3 px-0">
          <p>Total Homestay</p>
          <h3 id="total-homestay">120</h3>
        </article>
        <article class="overview text-center col-md-3 px-0">
          <p>House for Accept Student</p>
          <h3 id="homestay-for-accept">4</h3>
        </article>
      </div>
    </div>

    <p class="text__above">To Check</p>

    <!-- //** FOURTH ROW STUDENT TO CONFIRM -->
    <div class="div__border d-block p-3 position-relative">
      <h3 class="title_fourth_row">Student for Confirm Registration</h3>

      <div id="student_for_confirm" class="col-12 align-items-center justify-content-start student_for_confirm">
        <template id="template__student_for_confirm">
          <article class="card__student col-auto col-sm-11 col-md-8 p-0 mr-3 mb-3">
            <div class="card__body d-flex p-3 mb-5">
              <div class="col-5 col-md-5 d-flex p-0 h-100">
                <img src=" ../../public_s/monsedani9182@gamil.com/profilePhoto20220820124033000.jpeg"
                  class="img__student_profile" alt="">
              </div>
              <div class="col-7 col-md-7 p-0">
                <dt id="name_student">Empty</dt>
                <dt id="background-student">Empty</dt>
                <dt>Start the Day:</dt>
                <dd id="firstd_student">Empty</dd>
                <dt>School:</dt>
                <dd id="school_preference_student">Empty</dd>
              </div>
            </div>
            <div class="card__footer">
              <button type="button" id="button__more_details">More Details
                <span class="fa fa-arrow-right arrow__view" style="pointer-events:none"></span>
              </button>
            </div>
          </article>
        </template>

      </div>
      <!-- //** BUTTON FOR CAROUSEL */ -->
      <button type="button" id="button__prev" class="fa fa-arrow-left arrow__carousel"></button>
      <button type="button" id="button__next" class="fa fa-arrow-right arrow__carousel"></button>
    </div>

    <!-- //** FIFTH ROW HOMESTAY FOR CERTIFY -->
    <div class="div__border d-block p-3 position-relative">
      <h3 class="title_fifth_row">Homestay for Certify</h3>

      <div id="container__houses_for_certify"
        class="col-12 align-items-center justify-content-start container__houses_for_certify">
        <template id="template__homestay_for_certify">
          <article class="card_house col-auto col-sm-8 col-md-6 col-lg-5 p-0 mr-3 mb-3" id="slide-1">
            <div class="article__header">
              <img src="../../public/d@gmail.com/descarga.jpg" id="photo_home" alt="">
            </div>
            <div class="article__body text-center px-0 py-3">
              <dt id="house-name">Empty</dt>
              <dd id="house-address">Empty</dd>

              <div class="d-flex justify-content-between p-0 mt-4 text-center">
                <ol class="col-md-6 p-0">
                  <dt>Bedrooms</dt>
                  <dd id="homestay-rooms">Empty</dd>
                </ol>
                <ol class="col-md-6 p-0">
                  <dt>Pets</dt>
                  <dd id="homestay-accept-pets">Empty</dd>
                </ol>
              </div>

              <div class="d-flex justify-content-between p-0 mt-2">
                <ol class="col-md-6 p-0">
                  <dt class="ml-auto">Gender</dt>
                  <dd id="homestay-gender-preference">Empty</dd>
                </ol>
                <ol class="col-md-6 p-0">
                  <dt>Age</dt>
                  <dd id="homestay-age-preference">Empty</dd>
                </ol>
              </div>
            </div>
            <div class="article__footer">
              <button type="button" id="button__more_details">More Details
                <span class="fa fa-arrow-right arrow__view" style="pointer-events:none"></span>
              </button>
            </div>
          </article>
        </template>

      </div>
      <!-- //** BUTTON FOR CAROUSEL */ -->
      <button type="button" id="button__prev" class="fa fa-arrow-left arrow__carousel"></button>
      <button type="button" id="button__next" class="fa fa-arrow-right arrow__carousel"></button>
    </div>

  </main>

  <?php include 'footer.php'; ?>

  <!-- // ? CHARTS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js?ver=1.0.16"></script>

  <script src="assets/js/chart.js"></script>
  <script src="assets/js/index.js"></script>

</body>


</html>