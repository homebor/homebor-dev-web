<?php
require '../../xeon.php';
session_start();
error_reporting(0);

$usuario = $_SESSION['username'];

$query2 = "SELECT * FROM manager WHERE mail = '$usuario'";
$resultado2 = $link->query($query2);

$row2 = $resultado2->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="ThemeStarz">

  <!-- // TODO CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/simplebar.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.1">
  <link rel="stylesheet" type="text/css" href="assets/css/edit_propertie.css?ver=1.1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <!-- // TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Homebor - Administartor Panel</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
    integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
</head>

<?php require 'header.php' ?>

<body>
  <br><br>
  <main id="ts-main" class="mt-5">
    <section id="page-title">
      <div class="container">
        <div class="row py-4">
          <div class="col-lg-10">
            <div class="">
              <br>
              <h1 style="color: #342359">Non-Certified Homestays </h1>
              <?php $usuario = $_SESSION['username'] ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <hr>

    <div class="container">
      <br />
      <div class="d-none text-center message-empty">
        <h4 class='mt-2 mb-4 text-center'> You don't any houses for certify </h4>
        <button class="btn btn-back-directory">Back Homestay Directory</button>
      </div>
      <div class="card d-none card-houses">
        <div class="card-body">
          <div class="div__search_box">
            <label for="#" class="fa fa-search label__search"></label>
            <input type="text" name="search_box" class="input__search" id="search_box"
              placeholder="Type your search query here">
          </div>
          <div class="table-responsive" id="dynamic_content"></div>

          <div id="table-voun" class="table-responsive text-center ">
            <table class="table table-hover" id="vou">
              <thead class="thead-header">
                <tr>
                  <th class="text-center align-middle">Photo Home</th>
                  <th class="text-center align-middle">House Name</th>
                  <th class="text-center align-middle">City</th>
                  <th class="text-center align-middle">Direction</th>
                  <th class="text-center align-middle"></th>
                </tr>
              </thead>

              <tbody class="table-bordered" id="vouchers"></tbody>

            </table>
          </div>
        </div>
      </div>
    </div>


  </main>
</body>


<?php include 'footer.php' ?>


<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/non_certified_hom.js?ver=1.0" type="module"></script>

</body>

</html>