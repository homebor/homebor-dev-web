// Eye Function

const D = document;
const W = window;

let password = document.getElementById("pass");
let viewPassword = document.getElementById("btn__eye");
let click = false;

viewPassword.addEventListener("click", (e) => {
  if (!click) {
    password.type = "text";
    $("#btn__eye").removeClass("fa-eye").addClass("fa-eye-slash");
    click = true;
  } else if (click) {
    password.type = "password";
    $("#btn__eye").removeClass("fa-eye-slash").addClass("fa-eye");
    click = false;
  }
});

let password2 = document.getElementById("pass2");
let viewPassword2 = document.getElementById("btn__eye2");
let click2 = false;

viewPassword2.addEventListener("click", (e) => {
  if (!click2) {
    password2.type = "text";
    $("#btn__eye2").removeClass("fa-eye").addClass("fa-eye-slash");
    click2 = true;
  } else if (click2) {
    password2.type = "password";
    $("#btn__eye2").removeClass("fa-eye-slash").addClass("fa-eye");
    click2 = false;
  }
});

// Validate Iregister

const formulary = document.getElementById("form__register");
const inputs = document.querySelectorAll("#form__register input");

const expresiones = {
  // Basic Information

  name: /^[a-zA-Z0-9\_\-\s]{1,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  phone: /^\d{2,20}$/, // 2 a 14 numeros.
  rooms: /^\d{1,2}$/, // 1 a 14 numeros.
  dir: /^[a-zA-Z0-9\_\-\s\°]{4,100}$/, // Letras, numeros, guion y guion_bajo y espacios
  city: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  state: /^[a-zA-ZÀ-ÿ\-\_\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
  p_code: /^[a-zA-Z0-9\-\s]{3,10}$/,

  // Family Information
  name_h: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,

  nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\w])([A-Za-z\d\w]|[^ ]){8,15}$/, // 4 a 12 digitos.
  correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  date: /^([0-9]{2})[/]([0-9]{2})[/]([0-9]{4})$/,
  message: /^[a-zA-ZÀ-ÿ0-9\s]{1,500}$/, // Description message.
  register: /^\d{1,1}$/, // 1 a 14 numeros.
};

const campos = {
  name: true,
  email: true,
  pass: true,
  pass2: true,
  opt: true,
};

const validateFormulary = (e) => {
  switch (e.target.name) {
    case "name":
      validateField(expresiones.nombre, e.target, "name");
      break;

    case "l_name":
      validateField(expresiones.nombre, e.target, "l_name");
      break;

    case "email":
      validateField(expresiones.correo, e.target, "email");
      break;

    case "pass":
      validateField(expresiones.password, e.target, "pass");

      if (e.target.value !== "") validateLetterFunction(e.target.value);
      else {
        validateLetterPasword.upperCase = false;
        validateLetterPasword.specialCharacters = false;
        validateLetterPasword.numbers = false;
        validateLetterPasword.letters = false;
        document.querySelector("#low-password").classList.add("no-selected");
      }
      break;

    case "pass2":
      validatePassword2();
      break;

    case "opt":
      validateOption();
      break;
  }
};

const validateLetterPasword = {
  upperCase: false,
  specialCharacters: false,
  numbers: false,
  letters: false,
};

const calcPasswordValidation = (percent) => {
  if (percent > 0 && percent < 33) {
    document.querySelector("#low-password").classList.remove("no-selected");
    document.querySelector("#message-validator").classList.remove("d-none");
    document.querySelector("#message-validator").textContent = `Low password. Minimum 8 characters`;
  }
  if (percent > 33) {
    document.querySelector("#medium-password").classList.remove("no-selected");
    document.querySelector("#message-validator").classList.remove("d-none");
    document.querySelector("#message-validator").textContent = `Medium password. Minimum 8 characters`;
  } else document.querySelector("#medium-password").classList.add("no-selected");
  if (percent > 66) {
    document.querySelector("#hard-password").classList.remove("no-selected");
    document.querySelector("#message-validator").classList.add("d-none");
  } else document.querySelector("#hard-password").classList.add("no-selected");
};

const validateLetterFunction = (letter) => {
  for (let i = 0; i < letter.length; i++) {
    // var currentLetter = letter.charAt(i);
    // TODO => VALIDATE UPPERCASE
    if (/[A-Z]/.test(letter)) validateLetterPasword.upperCase = true;
    else validateLetterPasword.upperCase = false;

    // TODO => VALIDATE SPECIAL CHARACTERS
    if (/[\w]/.test(letter)) validateLetterPasword.specialCharacters = true;
    else validateLetterPasword.specialCharacters = false;

    // TODO => VALIDATE NUMBERS
    if (/[0-9]/.test(letter)) validateLetterPasword.numbers = true;
    else validateLetterPasword.numbers = false;

    // TODO => VALIDATE LETTERS
    if (/[a-z]/.test(letter)) validateLetterPasword.letters = true;
    else validateLetterPasword.letters = false;
  }
};

const validateField = (expresion, input, campo) => {
  if (expresion.test(input.value)) {
    document.getElementById(`group__${campo}`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  } else {
    document.getElementById(`group__${campo}`).classList.add("form__group-incorrect");
    document
      .querySelector(`#group__${campo} .form__group__input-error`)
      .classList.add("form__group__input-error-active");
    campos[campo] = false;
  }
};

const validatePassword2 = () => {
  const inputPassword1 = document.getElementById("pass");
  const inputPassword2 = document.getElementById("pass2");

  if (inputPassword1.value !== inputPassword2.value) {
    document.getElementById(`group__pass2`).classList.add("form__group-incorrect");
    document.querySelector(`#group__pass2 .form__group__input-error`).classList.add("form__group__input-error-active");
  } else {
    document.getElementById(`group__pass2`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__pass2 .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
  }
};

const validateOption = () => {
  var option = document.getElementById("opt");

  if (option.value === "0" || option.value === "") {
    document.getElementById(`group__pass2`).classList.add("form__group-incorrect");
    document.querySelector(`#group__pass2 .form__group__input-error`).classList.add("form__group__input-error-active");
    campos[campo] = false;
  } else {
    document.getElementById(`group__pass2`).classList.remove("form__group-incorrect");
    document
      .querySelector(`#group__pass2 .form__group__input-error`)
      .classList.remove("form__group__input-error-active");
    campos[campo] = true;
  }
};

inputs.forEach((input) => {
  input.addEventListener("keyup", validateFormulary);
  input.addEventListener("blur", validateFormulary);
});

document.addEventListener("keyup", (e) => {
  if (e.target.name === "pass") {
    let additional = 0;

    if (e.target.value.length >= 8 && e.target.value.length <= 12) additional = 20;
    else if (e.target.value.length > 12) additional = 40;

    if (
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === false) ||
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === false) ||
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === false) ||
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === true)
    )
      calcPasswordValidation("15");
    else if (
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === false) ||
      // * SECOND
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === false) ||
      // * THIRD
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === false)
    )
      calcPasswordValidation("30");
    else if (
      (validateLetterPasword.upperCase === false &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === false &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === false &&
        validateLetterPasword.letters === true) ||
      (validateLetterPasword.upperCase === true &&
        validateLetterPasword.specialCharacters === true &&
        validateLetterPasword.numbers === true &&
        validateLetterPasword.letters === false)
    )
      calcPasswordValidation("45");
    else if (
      validateLetterPasword.upperCase === true &&
      validateLetterPasword.specialCharacters === true &&
      validateLetterPasword.numbers === true &&
      validateLetterPasword.letters === true
    )
      calcPasswordValidation(additional + 60);
    else calcPasswordValidation("0");
  }
});

formulary.addEventListener("submit", (e) => {
  e.preventDefault();

  document.getElementById("btn_register").classList.add("btn_none");
  document.getElementById("div_btn_register").classList.add("spin");

  const termins = document.getElementById("termins");

  if (campos.name && campos.email && campos.pass && campos.pass2 === campos.pass && campos.opt && termins.checked) {
    $.ajax({
      url: "save.php",
      type: "POST",
      data: $("#form__register").serialize(),
      success: function (reg) {
        let registers = JSON.parse(reg);

        registers.forEach((register) => {
          if (register.response == "created house") {
            window.top.location = "homestay/homestay";

            formulary.reset();
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");
          } else if (register.response == "created student") {
            window.top.location = "student/profile";

            formulary.reset();
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");
          } else if (register.response == "created") {
            window.top.location = "login?ucrt=yes";

            formulary.reset();
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");
          } else if (register.response == "email exist") {
            grecaptcha.reset();

            setTimeout(() => {
              let message = "The email already exists, enter another";
              $("#message").html(message);

              setTimeout(() => {
                $("#message").html("");
              }, 5000);
            }, 100);
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");

            document.getElementById("group__email").classList.add("form__group-incorrect");
            document.getElementById("group__email").classList.remove("form__group-correct");
            document.getElementById("group__password").classList.add("form__group-incorrect");
            document.getElementById("group__password").classList.remove("form__group-correct");
            document
              .querySelector(".form__group__input-error-message")
              .classList.add("form__group__input-error-active");
          } else if (register.response == "error to create user") {
            grecaptcha.reset();
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");

            setTimeout(() => {
              let message = "Error to Create User";
              $("#message").html(message);

              setTimeout(() => {
                $("#message").html("");
              }, 5000);
            }, 100);

            document.getElementById("group__email").classList.add("form__group-incorrect");
            document.getElementById("group__email").classList.remove("form__group-correct");
            document.getElementById("group__password").classList.add("form__group-incorrect");
            document.getElementById("group__password").classList.remove("form__group-correct");
            document
              .querySelector(".form__group__input-error-message")
              .classList.add("form__group__input-error-active");
          } else if (register.response == "no validado captcha") {
            document.getElementById("btn_register").classList.remove("btn_none");
            document.getElementById("div_btn_register").classList.remove("spin");
            setTimeout(() => {
              let message = "reCaptcha not validated";
              $("#message").html(message);

              setTimeout(() => {
                $("#message").html("");
              }, 5000);
            }, 100);
          }
        });
      },
    });
  } else {
    setTimeout(() => {
      let message = "Please fill in the fields correctly";
      $("#message").html(message);

      setTimeout(() => {
        $("#message").html("");
      }, 5000);
    }, 100);

    document.getElementById("btn_register").classList.remove("btn_none");
    document.getElementById("div_btn_register").classList.remove("spin");
  }
});
