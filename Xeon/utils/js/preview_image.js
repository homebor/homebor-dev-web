// ! FILE TO PREVIEW IMAGE

// ! VARIABLES
const D = document;

// ! FUNCTIONS
function previewImage(input, file) {
  if (file.length > 0) {
    var fileReader = new FileReader();

    fileReader.onload = function (event) {
      D.querySelector("#image_preview").setAttribute("src", event.target.result);
    };
    fileReader.readAsDataURL(file[0]);
  }
}

// ! EVENTS
// ? ADD IMAGE IN INPUT
D.addEventListener("change", (e) => {
  if (e.target.matches("#attach_image_message")) previewImage(e.target.id, e.target.files);
});
