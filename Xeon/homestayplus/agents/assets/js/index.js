// TODO VARIABLES

const D = document;
const W = window;
// * STUDENTS ARRAYS
const STUDENTS = [];
const STUDENTS_CONFIRMED = [];
const STUDENTS_ACTIVE = [];
const STUDENTS_TO_CONFIRM = [];

// * HOMESTAY ARRAYS
const HOMESTAYS = [];
const HOMESTAYS_CERTIFIED = [];
const HOMESTAYS_FOR_ACCEPT = [];
const HOMESTAYS_FOR_CERTIFIED = [];

//* TEMPLATES

// ? TEMPLATE => NEWS
const $templateNews = D.querySelector("#template__news").content.cloneNode(true);

//? TEMPLATE => STUDENT FOR CONFIRM
const $templateStudentForConfirm = D.querySelector("#template__student_for_confirm").content.cloneNode(true);

//? TEMPLATE => STUDENT FOR CONFIRM
const $templateHomestayForCertified = D.querySelector("#template__homestay_for_certify").content.cloneNode(true);

const NOTIFICATIONS = [];

// TODO FUNCTIONS

/* function checkButtonsCarousel() {
  if (
    D.querySelector(".card_house").clientWidth * 4 + 20 >
    D.querySelector("#container__houses_for_certify").clientWidth
  ) {
    console.log("innecesarions");
    D.querySelector("#container__houses_for_certify").querySelector("#button__prev").classList.add("d-none");
  } else {
    console.log("necesarios");
  }
}
 */
// * CAROUSEL FUNCTION
function carouselFunction(buttonAction) {
  let $parentCarousel = "";
  let $widthCard = "";
  let $buttonPrev = "";
  let $buttonNext = "";

  if (buttonAction.parentElement.querySelector(".student_for_confirm")) {
    $parentCarousel = D.querySelector("#student_for_confirm");
    $widthCard = $parentCarousel.querySelector(".card__student").clientWidth;
    $buttonPrev = $parentCarousel.querySelector("#button__prev");
    $buttonNext = $parentCarousel.querySelector("#button__next");
    moveCarousel(buttonAction, $parentCarousel, $widthCard);
  } else if (buttonAction.parentElement.querySelector(".container__houses_for_certify")) {
    $parentCarousel = D.querySelector("#container__houses_for_certify");
    $widthCard = $parentCarousel.querySelector(".card_house").clientWidth;
    $buttonPrev = $parentCarousel.querySelector("#button__prev");
    $buttonNext = $parentCarousel.querySelector("#button__next");
    moveCarousel(buttonAction, $parentCarousel, $widthCard);
  }

  /* console.log(
    $parentCarousel.querySelector(".card_house").clientWidth,
    $widthCard * 4 + 20 > D.querySelector("#container__houses_for_certify").clientWidth
  ); */
}

function moveCarousel(buttonAction, $parentCarousel, $widthCard) {
  let position = 0;
  if (buttonAction.id === "button__prev") {
    $parentCarousel.scroll($parentCarousel.scrollLeft - $widthCard, 0);
  } else if (buttonAction.id === "button__next") {
    $parentCarousel.scroll($parentCarousel.scrollLeft + $widthCard, 0);
  }
}

// * GET COORDINATOR INFORMATION ============> FIRST ROW
async function getCoordinatorData() {
  const jsonCoordinatorData = await fetch("./get_coordinator_info.php");
  const resultCoordinatorData = await jsonCoordinatorData.json();

  D.querySelector(
    "#coordinator-name"
  ).textContent = `Hi, ${resultCoordinatorData.name} ${resultCoordinatorData.l_name}`;
  D.querySelector("#agency_id").value = resultCoordinatorData.id_m;
}

// * GET NEW NOTIFICATION ==============> FIRST ROW
async function getNews() {
  const formNews = new FormData();
  formNews.set("type_user", "agent");
  const DATA = { method: "POST", body: formNews };

  const jsonNews = await fetch("../helpers/get_notifications.php", DATA);
  const resultNews = await jsonNews.json();

  const $parentNews = D.querySelector("#news_notification");
  const $fragment = D.createDocumentFragment();

  if (resultNews.newNotifications === 0) D.querySelector(".container__news").classList.add("d-none");
  else D.querySelector(".container__news").classList.remove("d-none");

  resultNews.notifications.forEach((notification) => {
    if (notification.state === "0") NOTIFICATIONS.push(notification);
  });

  NOTIFICATIONS.forEach((notification, i) => {
    if (i <= 2) {
      const $clone = D.importNode($templateNews, true);

      $clone.querySelector("#notification-container").href = `${notification.url}`;
      $clone.querySelector(".title_notification").textContent = `${notification.title}`;
      $clone.querySelector(".content_notification").innerHTML = `${notification.description}`;

      $fragment.appendChild($clone);
    }

    $parentNews.appendChild($fragment);
  });
}

// * GET STUDENTS INFORMATION ============> SECOND ROW
async function getStudentInformation() {
  const jsonStudentsInformation = await fetch("./../helpers/students/all_students.php");
  const resultStudentsInformation = await jsonStudentsInformation.json();

  if (resultStudentsInformation !== null) {
    resultStudentsInformation.forEach((student) => {
      STUDENTS.push(student);
      if (student[0].ac_confirm === "Yes") STUDENTS_CONFIRMED.push(student);
      if (student[0].status === "Homestay Found") STUDENTS_ACTIVE.push(student);
      if (student[0].status === "Confirm Registration") STUDENTS_TO_CONFIRM.push(student);
    });

    const percentStudent = (STUDENTS_CONFIRMED.length * 100) / STUDENTS.length;

    // ? SHOW STUDENTS CONFIRMED
    D.querySelector("#percent-student-confirmed").textContent = `${percentStudent.toFixed()}%`;
  } else D.querySelector("#percent-student-confirmed").textContent = `0%`;
}

// * GET HOMESTAYS INFORMATION ============> SECOND ROW
async function getHomestayInformation() {
  const jsonHomestaysInformation = await fetch("./../helpers/homestay/get_all_homestays.php");
  const resultHomestaysInformation = await jsonHomestaysInformation.json();

  if (resultHomestaysInformation !== null) {
    resultHomestaysInformation.forEach((house) => {
      HOMESTAYS.push(house);
      if (house.certified === "Yes") HOMESTAYS_CERTIFIED.push(house);
      if (house.certified === "No") HOMESTAYS_FOR_CERTIFIED.push(house);
    });

    const percentHomestay = (HOMESTAYS_CERTIFIED.length * 100) / HOMESTAYS.length;

    // ? SHOW HOMESTAY CERTIFIED
    D.querySelector("#percent-homestay-certified").textContent = `${percentHomestay.toFixed()}%`;
  } else D.querySelector("#percent-homestay-certified").textContent = `No Homestays`;
}

// * OVERVIEW ===========> THIRD ROW
async function thirdRowData() {
  // ? TOTAL STUDENTS
  D.querySelector("#total-student").textContent = STUDENTS.length;

  // ? STUDENT ACTIVES IN HOMESTAY
  D.querySelector("#student-active-homestay").textContent = STUDENTS_ACTIVE.length;

  // ? TOTAL HOMESTAYS
  D.querySelector("#total-homestay").textContent = HOMESTAYS.length;

  const jsonHouseForAcceptStudent = await fetch("./../helpers/get_houses_for_accept_students.php");
  const resultHOuseForAcceptStudent = await jsonHouseForAcceptStudent.json();

  resultHOuseForAcceptStudent.forEach((notification) => {
    if (notification !== null) HOMESTAYS_FOR_ACCEPT.push(notification);
  });

  // ? TOTAL HOMESTAYS
  D.querySelector("#homestay-for-accept").textContent = HOMESTAYS_FOR_ACCEPT.length;
}

// * STUDENT FOR CONFIRM REGISTRATION =======> FOURTH ROW
function studentForConfirm() {
  const $hidenCarouselButtons = D.querySelector("#student_for_confirm")
    .parentElement.querySelectorAll(".arrow__carousel")
    .forEach((btnCarousel) => {
      btnCarousel.classList.add("d-none");
    });

  if (STUDENTS_TO_CONFIRM.length === 1) {
    if (screen.width > 575)
      D.querySelector("#student_for_confirm").classList.replace("justify-content-start", "justify-content-center");
  }

  if (screen.width < 767) $hidenCarouselButtons;
  else {
    if (STUDENTS_TO_CONFIRM.length <= 1) $hidenCarouselButtons;
    else if (screen.width > 1080 && STUDENTS_TO_CONFIRM.length <= 2) $hidenCarouselButtons;
    else {
      D.querySelector("#student_for_confirm")
        .parentElement.querySelectorAll(".arrow__carousel")
        .forEach((btnCarousel) => {
          btnCarousel.classList.remove("d-none");
        });
    }
  }

  if (STUDENTS_TO_CONFIRM.length === 0) {
    D.querySelector("#student_for_confirm").parentElement.classList.replace("d-block", "d-none");
  } else {
    STUDENTS_TO_CONFIRM.forEach((student) => {
      const $clone = D.importNode($templateStudentForConfirm, true);

      // ? PROFILE STUDENT IMAGE
      if (student[0].photo_s === "NULL" || student[0].photo_s === "")
        $clone.querySelector(".img__student_profile").src = "../assets/emptys/profile-student-empty.png";
      else $clone.querySelector(".img__student_profile").src = `../../${student[0].photo_s}`;

      // ? STUDENT FULLNAMES
      $clone.querySelector("#name_student").textContent = `${student[0].name_s} ${student[0].l_name_s}`;

      //? STUDENT BACKGROUND
      if (student[0].nationality === "") $clone.querySelector("#background-student").textContent = "Empty";
      else $clone.querySelector("#background-student").textContent = student[0].nationality;

      // ? STUDENT FIRSTDATE
      if (student[0].firstd === "NULL" || student[0].firstd === "")
        $clone.querySelector("#firstd_student").textContent = "Empty";
      else {
        const dateStart = student[0].firstd.split("-");
        $clone.querySelector("#firstd_student").textContent = `${dateStart[1]}/${dateStart[2]}/${dateStart[0]}`;
      }

      // ? SCHOOL PREFERENCE
      if (student[1] === "NULL" || student[1] === "")
        $clone.querySelector("#school_preference_student").textContent = "Empty";
      else $clone.querySelector("#school_preference_student").textContent = student[1];

      //? BUTTTON DETAILS
      $clone.querySelector("#button__more_details").dataset.idStudent = student[0].id_student;

      D.querySelector("#student_for_confirm").appendChild($clone);
    });
  }
}

//* HOMESTAY FOR CERTIFY ===============> FIFTH ROW
function homestayForCertified() {
  const $hidenCarouselButtons = D.querySelector("#container__houses_for_certify")
    .parentElement.querySelectorAll(".arrow__carousel")
    .forEach((btnCarousel) => {
      btnCarousel.classList.add("d-none");
    });

  if (HOMESTAYS_FOR_CERTIFIED.length === 1) {
    if (screen.width > 575)
      D.querySelector("#container__houses_for_certify").classList.replace(
        "justify-content-start",
        "justify-content-center"
      );
  }

  if (screen.width < 767) $hidenCarouselButtons;
  else {
    if (HOMESTAYS_FOR_CERTIFIED.length <= 1) $hidenCarouselButtons;
    else if (screen.width > 767 && HOMESTAYS_FOR_CERTIFIED.length <= 2) $hidenCarouselButtons;
    else if (screen.width > 1200 && HOMESTAYS_FOR_CERTIFIED.length <= 3) $hidenCarouselButtons;
    else {
      D.querySelector("#container__houses_for_certify")
        .parentElement.querySelectorAll(".arrow__carousel")
        .forEach((btnCarousel) => {
          btnCarousel.classList.remove("d-none");
        });
    }
  }

  if (HOMESTAYS_FOR_CERTIFIED.length === 0) {
    D.querySelector("#container__houses_for_certify").parentElement.classList.replace("d-block", "d-none");
  } else {
    D.querySelector("#container__houses_for_certify").classList.replace("d-none", "d-flex");
    HOMESTAYS_FOR_CERTIFIED.forEach((house) => {
      const $clone = D.importNode($templateHomestayForCertified, true);

      // ? HOUSE FRONTAGE IMAGE
      if (house.phome === "NULL" || house.phome === "")
        $clone.querySelector("#photo_home").src = "../assets/emptys/frontage-empty.png";
      else $clone.querySelector("#photo_home").src = `../../${house.phome}`;

      //? HOUSE NAME
      $clone.querySelector("#house-name").textContent = house.h_name;

      //? HOUSE ADDRESS
      if (house.dir === "NULL") $clone.querySelector("#house-address").textContent = "Empty";
      else {
        let address = house.dir.slice(1, 15);
        $clone.querySelector("#house-address").textContent = `${address}, ${house.city}`;
      }

      // ? HOUSE ROOMS
      $clone.querySelector("#homestay-rooms").textContent = house.room;

      // ? HOUSE PETS
      if (house.pet === "NULL" || house.pet === "") $clone.querySelector("#homestay-accept-pets").textContent = "Empty";
      else $clone.querySelector("#homestay-accept-pets").textContent = house.pet;

      // ? HOUSE GENDER PREFERENCE
      if (house.g_pre === "NULL" || house.g_pre === "")
        $clone.querySelector("#homestay-gender-preference").textContent = "NULL";
      else house.g_pre === "NULL" || house.g_pre === "";
      $clone.querySelector("#homestay-gender-preference").textContent = house.g_pre;

      // ? HOUSE AGE PREFERENCE
      if (house.ag_pre === "NULL" || house.ag_pre === "")
        $clone.querySelector("#homestay-age-preference").textContent = "NULL";
      else house.ag_pre === "NULL" || house.ag_pre === "";
      $clone.querySelector("#homestay-age-preference").textContent = house.ag_pre;

      $clone.querySelector("#button__more_details").dataset.idHome = house.id_home;

      D.querySelector("#container__houses_for_certify").appendChild($clone);
    });
  }
}

/* async function getStudentsInformation(){
  const 
} */

// TODO EVENTS
D.addEventListener("DOMContentLoaded", async (e) => {
  await getCoordinatorData();
  await getNews();
  await getStudentInformation();
  await getHomestayInformation();
  await thirdRowData();
  await studentForConfirm();
  await homestayForCertified();

  if (STUDENTS_TO_CONFIRM.length === 0 && HOMESTAYS_FOR_CERTIFIED.length === 0) {
    D.querySelectorAll(".text__above").forEach((textAbove) => {
      if (textAbove.textContent === "To Check") textAbove.classList.add("d-none");
      else textAbove.classList.remove("d-none");
    });
  }
  // checkButtonsCarousel();
});

D.addEventListener("click", (e) => {
  if (e.target.matches(".arrow__carousel")) carouselFunction(e.target);
  if (e.target.matches("#button__more_details")) {
    if (e.target.dataset.idStudent) W.top.location = `student_info?art_id=${e.target.dataset.idStudent}`;
    if (e.target.dataset.idHome) W.top.location = `detail?art_id=${e.target.dataset.idHome}`;
  }
});

D.addEventListener("mouseover", (e) => {
  if (e.target.matches("#button__more_details")) {
    e.target.querySelector(".arrow__view").classList.replace("arrow__view", "arrow__view-active");
  }
});

D.addEventListener("mouseout", (e) => {
  if (e.target.matches("#button__more_details")) {
    e.target.querySelector(".arrow__view-active").classList.replace("arrow__view-active", "arrow__view");
  }
});
