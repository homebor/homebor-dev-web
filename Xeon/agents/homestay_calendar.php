<?php 
require './../xeon.php';
session_start();

$usuario = $_SESSION['username'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

  <!-- // ! LINKS HOMEBOR -->
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/homestay_calendar.css">

  <!-- // ! LINKS CALENDAR -->


</head>

<body>

  <?php include 'header.php' ?>

  <main class="main__calendar">
    <div id="calendar">
      <article id="header_calendar" class="d-flex justify-content-between">
        <!-- // * SHOW YEARS -->
        <aside>
          <select name="" id="years"></select>
        </aside>

        <!-- // * SHOW MONTHS -->
        <aside id="parent_months">
          <template id="template_months">
            <span id="months"></span>
          </template>
        </aside>
      </article>

      <!-- // * EVENT'S CALENDAR -->
      <article class="table-responsive mt-2">
        <table class="table table-hover table-bordered homestay_calendar">
          <thead class="calendar_header">
            <tr id="header">
              <!-- // * SHOW DAYS -->
              <template id="template_days">
                <th id="days" class="text-center"></th>
              </template>
            </tr>
          </thead>

          <tbody class="calendar_body">
            <template id="template_events">
              <tr id="events">
                <td id="name_event"></td>
              </tr>
            </template>
          </tbody>
        </table>
      </article>
    </div>
  </main>


  <script type="module" src="assets/js/homestay_calendar.js"></script>
</body>

</html>