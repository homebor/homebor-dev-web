<?php 

require '../xeon.php';

$homestayQuery = $link->query("SELECT * FROM pe_home");

while ($row_homestay = mysqli_fetch_array($homestayQuery)){
  $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$row_homestay[mail_h]'");
  $num_events = mysqli_num_rows($eventsQuery);

  if($num_events > 0){
    $activities = null; 
    while ($row_events = mysqli_fetch_array($eventsQuery)){
      if($row_events["email"] === $row_homestay["mail_h"]){
        $activities[] = array(
          "name" => $row_events["title"],
          "intervals" => array([
            "start" => $row_events["start"],
            "end" => $row_events["end"],
            "minutesPerDay" => 0,
          ]),
          "fill" => $row_events["color"],
        );
      }
    }
    $events[] = array(
      "name" => $row_homestay["name_h"] . $row_homestay["l_name_h"],
      "description" => $row_homestay["dir"],
      "image" => '../'.$row_homestay["phome"],
      "activities" => $activities,
    );
  }
}

echo json_encode($events);

?>