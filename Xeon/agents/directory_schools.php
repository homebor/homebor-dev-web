<?php 
require '../xeon.php';

session_start();
$usuario = $_SESSION['username'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- // ! LINK CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css?ver=1.0">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css?ver=1.0">
  <link rel="stylesheet" href="../assets/css/style.css?ver=1.0">
  <link rel="stylesheet" href="assets/css/directory_schools.css?ver=1.0">
  <link rel="stylesheet" href="../assets/css/utility.css?ver=1.0.12">

  <!--// TODO Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <title>Academy Directory</title>
</head>

<body>

  <?php include 'header.php' ?>

  <main class="main-container">

    <!-- // TODO SECTION => SEARCH PANEL -->
    <section class="section__search-panel position-relative ">
      <h3>Search School</h3>
      <label for="" class="fa fa-search"></label>
      <article class="d-flex">
        <input type="text" class="input__search" id="search-school" placeholder="Type the name">
        <button class="float_btn d-flex">+ <p class="m-0 my-auto">&nbsp;&nbsp;Add New School</p></button>
      </article>
    </section>

    <!-- // TODO SECTION => SCHOOL PANEL -->
    <section
      class="section__school-panel d-flex justify-content-lg-start justify-content-md-center justify-content-sm-center justify-content-center row ml-0">
      <template id="template__schools">
        <!-- // TODO HOUSE CONTAINER -->
        <article class="school_container">
          <img src="../assets/emptys/frontage-empty.png" alt="" class="img_school">
          <div class="d-flex flex-column px-3 py-1 pt-2">
            <p class="" id="school_name">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
            <p class="" id="school_address">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
          </div>
        </article>
      </template>
    </section>

  </main>

  <script src="assets/js/directory_schools.js?ver=1.0" type="module"></script>

</body>

</html>