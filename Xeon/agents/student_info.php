<?php 
require '../xeon.php';
session_start();
$usuario = $_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="icon" type="image/png" sizes="152x152" href="../assets/logos/11.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!-- // TODO HOMEBOR STYLES -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/student_info.css?ver=1.0.8">

  <!-- // TODO DATERANGEPICKER -->

  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <title>Student Information</title>
</head>

<body>

  <!-- // ! HEADER INFORMATION -->
  <?php include 'header.php'; ?>

  <!-- // ! PAGE CONTENT -->
  <main class="main-container p-2">

    <!-- // ? FIRST SECTION -->
    <section class="section section__main-information row d-flex justify-content-around align-items-center">
      <article class="col-8 col-sm-5 col-md-4 col-lg-3 col-xl-3">
        <img src="../assets/emptys/profile-student-empty.png" alt="" data-photo-student>
      </article>
      <article class="col-11 col-sm-7 col-md-8 col-lg-9 col-xl-8 mt-3 mt-sm-0">
        <!-- // TODO => MAIN INFORMATION -->
        <h2 data-name-student class=" text-center text-sm-left mb-3 mb-sm-2"></h2>
        <button class="btn_download"><span id="icon_download"></span></button>
        <p class="main-subtitle icon_container purple d-flex align-items-center justify-content-center justify-content-sm-start"
          data-nationality></p>
        <p class="main-subtitle icon_container purple d-flex align-items-center justify-content-center justify-content-sm-start"
          data-status>
        </p>

        <!-- // TODO => ABOUT THE STUDENT -->
        <p class="main-subtitle dark opacity-6 mt-3">About the Student</p>
        <p class="text-container dark opacity-6" data-about-us>Lorem, ipsum dolor sit amet consectetur adipisicing elit.
          Reiciendis
          libero perferendis necessitatibus architecto, optio cupiditate assumenda, mollitia placeat dolorum
          voluptatibus enim quasi quisquam consequuntur. Enim eum eius iusto assumenda illum.</p>
      </article>
    </section>

    <!-- // ? SECOND SECTION -->
    <section class="section section__second-information row d-flex justify-content-around">
      <article class="col-11 col-sm-5 col-md-4 col-lg-3 col-xl-3">
        <!-- // * BOOKING PANEL -->
        <span class="subtitle__container d-flex justify-content-between align-items-center ">
          <p class="main-subtitle purple opacity-6 booking_panel">Booking Panel</p>
          <hr size="10px">
        </span>

        <div class="buttons-container mt-3 row d-flex flex-sm-column justify-content-center mb-5 mb-sm-4">
          <button type="button" class="mx-auto d-flex align-items-center justify-content-center" id="button-edit">
            <i class="icon__exit"></i>
            Edit Information
          </button>
          <button type="button" class="mx-auto d-flex align-items-center justify-content-center mt-sm-4"
            id="booking-edit">
            <i class="icon__booking"></i>
            Book House
          </button>

          <div class="d-none info_hide" id="warning">
            <p class="dark mb-0">Confirm the student to add Houses</p>
          </div>
        </div>

        <!-- // * ACCOMMODATION REQUEST -->
        <span class="subtitle__container d-flex justify-content-between align-items-center ">
          <p class="main-subtitle purple opacity-6 accom_request">Accommodation Request</p>
          <hr size="10px">
        </span>

        <div class="inputs-container d-flex flex-column justify-content-center" id="accommodation_request">
          <template id="template__accomodation_request">
            <aside class="d-flex flex-column mb-4">
              <label for="accommodation_request-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 dark" id="accommodation_request-text" readonly></p>
            </aside>
          </template>

        </div>

        <!-- // * ACTIVE RESERVATION -->
        <span class="subtitle__container d-none justify-content-between align-items-center mt-3">
          <p class="main-subtitle purple opacity-6 active_reservation">Active Reservation</p>
          <hr size="10px">
        </span>

        <aside class="house-selected house-container d-none flex-column justify-content-center mt-2">
          <div class="px-3 py-3 m-0">
            <img src="" alt="" data-image-house-selected>
            <span class="room_select" data-room-selected></span>
            <span class="icon__exit" data-id-home></span>
          </div>

          <p class="dark text-center name__house" data-house-name></p>

          <div class="d-flex justify-content-around px-3 mb-3">
            <aside class="col-5 d-flex justify-content-start align-items-center">
              <span class="icon_accommodation"></span>
              <p class="dark text-center mb-0" data-accommodation-selected></p>
            </aside>
            <aside class="col-5 d-flex justify-content-end align-items-center">
              <span class="icon_food"></span>
              <p class="dark text-center mb-0" data-food-selected></p>
            </aside>
          </div>

          <button id="change-house" class=" btn_change-house">Change House</button>
        </aside>

        <!-- // * NEXT RESERVATION -->
        <span class="subtitle__container d-none justify-content-between align-items-center mt-4">
          <p class="main-subtitle purple opacity-6 next_reservation">Next Reservation</p>
          <hr size="10px">
        </span>

        <aside class="house-next house-container d-none flex-column justify-content-center mt-2">
          <div class="px-3 py-3 m-0">
            <img src="" alt="" data-image-house-next>
            <span class="room_select" data-room-next></span>
            <span class="icon__exit" data-id-home-next></span>
          </div>

          <p class="dark text-center name__house" data-house-name-next></p>

          <div class="d-flex justify-content-around px-3 mb-3">
            <aside class="col-5 d-flex justify-content-start align-items-center">
              <span class="icon_accommodation"></span>
              <p class="dark text-center mb-0" data-accommodation-next></p>
            </aside>
            <aside class="col-5 d-flex justify-content-end align-items-center">
              <span class="icon_food"></span>
              <p class="dark text-center mb-0" data-food-next></p>
            </aside>
          </div>

          <button id="next-house" class="btn_change-house">Change House</button>
        </aside>

        <!-- // * LAST RESERVATION -->
        <span class="subtitle__container d-none justify-content-between align-items-center mt-4">
          <p class="main-subtitle purple opacity-6 last_reservation">Last Stay</p>
          <hr size="10px">
        </span>

        <aside class="house-last house-container d-none flex-column justify-content-center mt-2">
          <div class="px-3 py-3 m-0">
            <img src="" alt="" data-image-house-last>
            <span class="room_select" data-room-last></span>
            <span class="icon__exit" data-id-home-last></span>
          </div>

          <p class="dark text-center name__house" data-house-name-last></p>

          <div class="d-flex justify-content-around px-3 mb-3">
            <aside class="col-5 d-flex justify-content-start align-items-center">
              <span class="icon_accommodation"></span>
              <p class="dark text-center mb-0" data-accommodation-last></p>
            </aside>
            <aside class="col-5 d-flex justify-content-end align-items-center">
              <span class="icon_food"></span>
              <p class="dark text-center mb-0" data-food-last></p>
            </aside>
          </div>

          <!-- <button id="next-house" class="btn_change-house">Change House</button> -->
        </aside>

      </article>
      <article class="col-11 col-sm-7 col-md-8 col-lg-9 col-xl-8 mt-3 mt-sm-0">

        <!-- // * HOUSE REQUESTED -->
        <span class="subtitle__container d-none justify-content-between align-items-center ">
          <p class="main-subtitle dark house_catalog">Houses Catalog</p>
          <hr size="10px">
        </span>

        <div class="d-none row justify-content-center align-items-center mt-4 mb-5" id="parent_houses_requested">
          <template id="template__houses_requested">
            <aside
              class="house-container col-8 col-sm-5 col-md-5 col-lg-4 col-xl-3 d-flex flex-column justify-content-center mx-2 mt-2">
              <div class="px-1 py-3 m-0">
                <img src="" alt="" data-image-house-requested>
                <span class="room_select catalog d-none" data-requested></span>
                <span class="icon__exit" data-id-home></span>
              </div>

              <p class="dark text-center name__house mt-3" data-house-name-requested></p>
              <!-- <p class="dark text-center name__house m-0" data-room-requested></p> -->

              <!-- <div class="d-flex justify-content-around">
                <aside class="col-5 d-flex justify-content-start align-items-center">
                  <span class="icon_accommodation"></span>
                  <p class="dark text-center mb-0" data-accommodation-requested></p>
                </aside>
                <aside class="col-5 d-flex justify-content-end align-items-center">
                  <span class="icon_food"></span>
                  <p class="dark text-center mb-0" data-food-requested></p>
                </aside>
              </div> -->
            </aside>
          </template>
        </div>

        <!-- // * BASIC INFORMATION -->
        <span class="subtitle__container d-flex justify-content-between align-items-center ">
          <p class="main-subtitle dark basic_information">Basic Information</p>
          <hr size="10px">
        </span>

        <div class="inputs-container row d-flex justify-content-center" id="parent_basic_information">
          <template id="template__basic_information">
            <aside class="col-8 col-sm-6 col-md-4 col-xl-4 d-flex flex-column text-center">
              <label for="basic_information-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 text-center dark" id="basic_information-text" readonly></p>
            </aside>
          </template>
        </div>

        <!-- // * STUDENT ADDRESS -->
        <span class="subtitle__container d-flex justify-content-between align-items-center ">
          <p class="main-subtitle dark student_address">Personal Student Address</p>
          <hr size="10px">
        </span>

        <div class="inputs-container row d-flex justify-content-center" id="parent_student_address">
          <template id="template__student_address">
            <aside class="col-6 col-sm-6 col-md-4 col-xl-4 d-flex flex-column text-center">
              <label for="student_address-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 text-center dark" id="student_address-text" readonly></p>
            </aside>
          </template>
        </div>

        <!-- // * PROFESSIONAL INFORMATION -->
        <span class="subtitle__container d-flex justify-content-between align-items-center ">
          <p class="main-subtitle dark professional_information">Professional Information</p>
          <hr size="10px">
        </span>

        <div class="inputs-container row d-flex justify-content-center" id="parent_professional_information">
          <template id="template__professional_information">
            <aside class="col-5 d-flex flex-column text-center">
              <label for="professional_information-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 text-center dark" id="professional_information-text" readonly></p>
            </aside>
          </template>
        </div>

      </article>
    </section>

    <!-- // ? THIRD SECTION -->
    <section class="section section__third-information d-flex row justify-content-around">
      <!-- // * FLIHT INFORMATION -->
      <span class="subtitle__container d-flex justify-content-between align-items-center px-3 px-lg-0">
        <hr size="10px">
        <p class="main-subtitle dark flight_information">Flight Information</p>
        <hr size="10px">
      </span>

      <div class="inputs-container row d-flex justify-content-around" id="parent_flight_information">
        <template id="template__flight_information">
          <aside class="col-5 d-flex flex-column text-center">
            <label for="fligh_information-text" class="label__input opacity-6"></label>
            <p type="text" class="inputs opacity-6 text-center dark" readonly id="fligh_information-text"></p>
          </aside>
        </template>
      </div>
    </section>

    <!-- // ? FOURTH SECTION -->
    <section class="section section__fourth-information d-flex flex-column justify-content-around">
      <!-- // * HOUSE PREFERENCE -->
      <span class="subtitle__container d-flex justify-content-between align-items-center px-3 px-sm-0 px-lg-0">
        <hr size="10px">
        <p class="main-subtitle dark house_preference">House Preference</p>
        <hr size="10px">
      </span>

      <article class="row d-flex justify-content-center justify-content-sm-between pt-3">
        <div class="col-12 col-sm-6 col-xl-6 d-flex flex-column">
          <h3 for="" class="label__input label__subtitle text-center opacity-6">Can you share with:</h3>
          <div class="inputs-container row d-flex justify-content-around" id="parent_share_with">
            <template id="template__share_with">
              <aside class="col-5 d-flex flex-column text-center">
                <label for="share_with-text" class="label__input opacity-6"></label>
                <p type="text" class="inputs opacity-6 text-center dark" id="share_with-text" readonly></p>
              </aside>
            </template>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-xl-6">
          <h3 for="" class="label__input label__subtitle text-center opacity-6">Accommodation</h3>
          <div class="inputs-container row d-flex justify-content-around" id="parent_accommodation">
            <template id="template__accommodation">
              <aside class="col-5 d-flex flex-column text-center">
                <label for="accommodation-text" class="label__input opacity-6"></label>
                <p type="text" class="inputs opacity-6 text-center dark" id="accommodation-text" readonly></p>
              </aside>
            </template>
          </div>

        </div>
      </article>

      <article class="d-flex flex-column mt-4 parent_article">
        <!-- // * OPTIONAL SUPPLEMENTS -->
        <h3 for="" class="label__input label__subtitle text-center opacity-8">
          <span class="button__info">!</span>
          Optional supplements
        </h3>

        <div class="d-none info_hide" id="mostrar">
          <p class="dark"><b> - Accommodation near the school:</b> Additional <b>$35 CAD</b> per night applies (up to 30
            minutes walking).
          </p>
          <p class="dark"><b> - Urgent accommodation search: </b> Less than 1 week = <b>$150 CAD.</b></p>
          <p class="dark"><b> - Guardianship: </b> Guardianship is a practical and moral support system for young
            students studying in a foreign country, far from their parents = <b>$550 CAD.</b></p>
          <p class="dark"><b> - Minor fee (-18 years old): $75 CAD </b> per week. </p>
          <p class="dark"><b> - Summer fee: $45 CAD </b> per week. </p>
        </div>

        <div class="inputs-container row d-flex justify-content-around" id="parent_optional_supplements">
          <template id="template__optional_supplements">
            <aside class="col-6 col-sm-6 col-md-4 col-xl-4 d-flex flex-column text-center">
              <label for="optional_supplement-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 text-center dark" id="optional_supplement-text" readonly></p>
            </aside>
          </template>
        </div>

        <!-- // * TRANSPORT -->
        <h3 for="" class="label__input label__subtitle text-center opacity-8 mt-4"> Transport </h3>

        <div class="inputs-container row d-flex justify-content-around" id="parent_transport">
          <template id="template__transport">
            <aside class="col-6 col-sm-6 col-md-4 col-xl-4 d-flex flex-column text-center">
              <label for="transpor-text" class="label__input opacity-6"></label>
              <p type="text" class="inputs opacity-6 text-center dark" id="transpor-text" readonly></p>
            </aside>
          </template>
        </div>

        <div class="ml-auto d-flex flex-column justify-content-center">
          <h3 for="" class="label__input label__subtitle opacity-8 mt-4 mb-0 pb-0"> Total Accommmodation
          </h3>
          <p id="total_price" class="text-center dark"></p>
        </div>

      </article>
    </section>

    <!-- // ? FIFTH SECTION -->
    <section class="section section__fifth-information d-flex flex-column justify-content-around">
      <!-- // * HEALTH INFORMATION -->
      <span class="subtitle__container d-flex justify-content-between align-items-center ">
        <hr size="10px">
        <p class="main-subtitle dark health_information">Health Information</p>
        <hr size="10px">
      </span>

      <div class="inputs-container row d-flex justify-content-around mt-4" id="parent_health_information">
        <template id="template__health_information">
          <aside>
            <label for="health_information-text" class="label__input opacity-6"></label>
            <p type="text" class="inputs opacity-6 text-center dark" id="health_information-text" readonly></p>
          </aside>
        </template>
      </div>
    </section>

    <!-- // ? SIXTH SECTION -->
    <section class="section section__sixth-information d-flex flex-column justify-content-around">
      <!-- // * EMERGENCY CONTACT -->
      <span class="subtitle__container d-flex justify-content-between align-items-center ">
        <hr size="10px">
        <p class="main-subtitle dark emergency_contact">Emergency Contact</p>
        <hr size="10px">
      </span>

      <div class="inputs-container row d-flex justify-content-around mt-4" id="parent_emergency_contact">
        <template id="template__emergency_contact">
          <aside class="col-5 d-flex flex-column text-center">
            <label for="emergency_contact-text" class="label__input opacity-6"></label>
            <p type="text" class="inputs opacity-6 text-center dark" id="emergency_contact-text" readonly></p>
          </aside>
        </template>
      </div>
    </section>

    <!-- // ? SEVENTH SECTION -->
    <section class="section section__seventh-information d-flex flex-column justify-content-around">
      <!-- // * EMERGENCY CONTACT -->
      <span class="subtitle__container d-flex justify-content-between align-items-center ">
        <hr size="10px">
        <p class="main-subtitle dark attached_files">Attached Files</p>
        <hr size="10px">
      </span>

      <div class="image-container row d-flex justify-content-around mt-5" id="parent_attached_files">
        <template id="template__attached_files">
          <aside
            class="col-7 col-sm-5 col-md-4 col-lg-4 col-xl-3 d-flex flex-column justify-content-center align-content-center align-items-center text-center">
            <iframe src="" alt="" class="image_attached mb-2 d-none" id="attached_files-text" height="300px"></iframe>
            <img src="" alt="" class="image_attached mb-2 d-none" id="attached_files-text">
            <label for="attached_files-text" class="label__input opacity-6"></label>
          </aside>
        </template>
      </div>
    </section>
  </main>

  <!-- // TODO MODAL CHANGE HOUSE -->

  <div class="modal__reserve w-100 h-100 position-fixed d-none align-items-center justify-content-center"
    id="modal__reservation">
    <article class="justify-content-center align-items-center modal__div modal__div__reservation">
      <form action="#" id="form__reservation">
        <div class="modal__header d-flex justify-content-between">
          <h4 class="title__modal-header">Choose the House</h4>
          <button type="button" id="btn__close-modal" class="btn__close-modal">X</button>
        </div>

        <div class="modal__body">

          <!-- //TODO DATE / REASON TO RESERVE -->

          <div class="d-flex row ml-0 justify-content-center change-details w-100">
            <!-- // TODO INPUT DATE -->
            <div class="col-sm-5 col-md-6 date-change-container">
              <article class="text-center">
                <label for="date-change-reserve">Effective Date</label>
                <label for="date-change-reserve" class="icon icon__calendar"></label>
                <input type="text" id="date-change-reserve" name="date-change-reserve" class="input-date-change"
                  placeholder="Change Date" data-datepicker="true" data-empty readonly>
              </article>
            </div>

            <!-- // TODO REASON TO CHANGE -->
            <div class="col-sm-7 col-md-6 reason-date-change">
              <article class="">
                <label for="" class="text-left">Reason to Change Reserve</label>
                <textarea name="reason-change-reserve" id="reason-change-reserve" rows="4" maxlength="300"
                  class="textarea-change-reserve"></textarea>
              </article>
            </div>
          </div>

          <!-- FIELD SEARCH STUDENTS -->
          <div class="div__search-input">
            <label for="search__input" class="fa fa-search label__search"></label>
            <input type="text" id="search__input" class="search__input" placeholder="Type your Search">
          </div>

          <!-- STUDENTS LIST RESERVATION -->

          <ul id="list__student" class="ul__list-students">
            <template id="houses-list">
              <div class="d-flex flex-column flex-md-row mb-5 mb-lg-4 pb-2 pb-md-0 bg-white card-house">
                <!-- // ** CARD HEADER-->
                <div class="p-0 d-flex flex-column align-items-center card-header" data-house-profile
                  title="See profile">
                  <img src="../assets/emptys/frontage-empty.png" id="img-house" class="mx-auto img__house"
                    alt="house, homestay">
                  <div id="status" data-house-status>Available</div>
                </div>

                <!-- // ** CARD BODY CONTAINER-->
                <div class="w-100 px-0 py-2 card-body">
                  <!-- // * CARD MAIN INFORMATION-->
                  <div class="py-0 px-3 main-information">
                    <!-- // ** HOUSE NAME  -->
                    <label class="w-100 h4 m-0 d-flex justify-content-between align-items-center name_house"
                      data-house-name></label>

                    <hr class="my-2 pt-1">

                    <div class="d-flex row ml-0 justify-content-around house-beds"></div>
                  </div>
                </div>
              </div>

            </template>

            <template id="template-beds">
              <div class="beds-container w-100 d-none col-sm-6 mb-2" id="bed-i">
                <input type="checkbox" id="" class="checkbox__filter checkbox__filter_only_available" value="true"
                  data-filter="status">
                <label for="" id="" class="label-beds d-flex justify-content-start align-items-center">
                  <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" style="" class="svg__check"
                    aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                  </svg>

                  <p class="type-bed " id="p-bed"></p>
                </label>
              </div>
              <div class="beds-container w-100 d-none col-sm-6 mb-2" id="bed-ii">
                <input type="checkbox" id="" class="checkbox__filter checkbox__filter_only_available" value="true"
                  data-filter="status">
                <label for="" data-id="filter_only_available"
                  class="label-beds d-flex justify-content-start align-items-center">
                  <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" style="" class="svg__check"
                    aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                  </svg>

                  <p class="type-bed" id="p-bed"></p>
                </label>
              </div>
              <div class="beds-container w-100 d-none col-sm-6" id="bed-iii">
                <input type="checkbox" id="" class="checkbox__filter checkbox__filter_only_available" value="true"
                  data-filter="status">
                <label for="" data-id="filter_only_available"
                  class="label-beds d-flex justify-content-start align-items-center">
                  <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" style="" class="svg__check"
                    aria-hidden="true" role="presentation" focusable="false">
                    <path fill="none" d="m4 16.5 8 8 16-16" style="pointer-events: none !important"></path>
                  </svg>

                  <p class="type-bed" id="p-bed"></p>
                </label>
              </div>
            </template>
          </ul>
          <div class="d-none div__no_student justify-content-center align-items-center" id="div__no_student">
            <img class="img__no_student" src="../assets/emptys/calendar-empty-no-border.png" alt="">
            <h3 class="text__no_student">There are no Students for the Dates available for this bed.</h3>
          </div>

          <input type="text" id="id__house-selected" name="id-house">
          <input type="text" value="" id="room-selected" name="id-room">
          <input type="text" value="" id="id_homeBefore" name="id_homeBefore">


        </div>

        <div class="modal__footer d-flex justify-content-end" id="modal__footer_resevation">
          <div id="btn__success">
            <button type="submit" id="btn__submit-reservation" class="btn btn__add-reservation">Change
              Reserve</button>
          </div>
        </div>
      </form>
    </article>
  </div>

  <!-- // ! FOOTER INFORMATION -->
  <?php include 'footer.php'; ?>
  <script src="assets/js/student_info.js?ver=1.0.14"></script>
  <script src="../utils/js/daterangepicker.js"></script>
</body>

</html>