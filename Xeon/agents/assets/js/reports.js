// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;

// *  TEMPLATES
const $templateReportList = D.querySelector("#template__report-list").content.cloneNode(true);

const REPORTS = [];

// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES
async function getReportList() {
  const formReportList = new FormData();
  formReportList.set("request", "getReportList");
  const DATA = { method: "POST", body: formReportList };

  const jsonReportList = await fetch("../helpers/get_reports.php", DATA);
  const resultReportList = await jsonReportList.json();

  console.log(resultReportList);
  REPORTS.push(resultReportList);
  if (resultReportList === "0") {
    D.querySelector("#report_list").classList.add("justify-content-center");

    D.querySelector("#report_list").innerHTML = `
      <h4 class="text-center empty-reports">You don't have Reports Available</h4><br>
      <p class="text-center">Add New Report</p>
    `;
  } else {
    D.querySelector("#report_list").classList.remove("justify-content-center");
    const $parentReportList = D.querySelector("#report_list");
    const $fragment = D.createDocumentFragment();

    /* resultReportList.reportList.forEach((reportList) => {
      const $clone = D.importNode($templateReportList, true);

      // $clone.querySelector(".report > img").src = ;

      $fragment.appendChild($clone);
    }); */

    $parentReportList.appendChild($fragment);
  }
}

/* @--> AddNewReport() */
/* @--> SelectReport() */
/* @--> MarkReadReport() */
/* @--> AttachFile() */
/* @--> SendReport() */
/* @--> OPTIONAL -> MarkMessageReport() */
/* @--> CloseReport() */

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  _Messages.quitMessage();

  // ? CALLED FUNCTIONS
  getReportList();
});

D.addEventListener("click", (e) => {
  // if (e.target.matches(".button__add-report")) console.log("funciona");
});
