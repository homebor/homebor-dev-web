//TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

//TODO VARIABLES
const D = document;
const W = window;
// ? INSTANCIAS
const _Messages = new Messages();
// ? CONTAINERS
const $CONTAINERS = {
  search: D.getElementById("search-container"),
  searchDate: D.getElementById("search-date"),
  filtersSelected: D.getElementById("all-filters").children[0],
  students: D.getElementById("all-students"),
  seeMore: D.querySelector(".see-more"),
  showMore: D.querySelector(".show-more"),
  templateStudentCard: D.getElementById("student-card-template").content,
};

// TODO FUNCIONES
class AllStudents {
  constructor(CONTAINERS = $CONTAINERS) {
    // ? SEARCH INPUT
    this.searchInput$ = CONTAINERS.search.querySelector("#search-student");
    // ? SEARCH DATE INPUT
    this.searchDateInput$ = CONTAINERS.searchDate;

    // ? DOM CONTAINERS
    this.filterSelected$ = CONTAINERS.filtersSelected;
    this.studentsContainer$ = CONTAINERS.students;

    // ? PAGINATION
    this.seeMore$ = CONTAINERS.seeMore;
    this.showMore$ = CONTAINERS.showMore;

    // ? TEMPLATES
    this.templateStudentCard$ = CONTAINERS.templateStudentCard;

    // ? DATA SAVED
    this.filters = {
      situation: "",
      destinationCity: "",
      dateRange: "",
      date: "",
      typeAccommodation: "",
      gender: "",
      food: "",
      specialDiet: {
        vegetarians: "",
        halal: "",
        kosher: "",
        lactose: "",
        gluten: "",
        pork: "",
      },
      ageRange: "",
      pickUpService: "",
      dropOffService: "",
      academy: "",
      language: "",
      anotherLanguage: "",
      typeStudent: "",
      // * CAN SHARE WITH
      shareWithSmokers: "",
      shareWithChildren: "",
      shareWithTeenagers: "",
      shareWithPets: "",
    };
    this.allLanguages = new Set();
    this.allAnotherLanguages = new Set();
    this.allDestinationCity = new Set();
    this.pages = [];
    this.allStudents = [];
  }

  // ? GET ALL STUDENTS DATA
  async getStudents() {
    try {
      const getData = await axios("../helpers/students/all_students.php");
      const data = await getData.data;
      if (data && data instanceof Array && data[0] === "redirect") W.location.href = data[1];
      const allStudentsArray = await data.map((student) => {
        student[0].name_academy = student[1];
        student[0].date_register = student[2];
        return student[0];
      });

      // * CREATE PAGES
      for (let i = 0; i < allStudentsArray.length; i += 100) this.pages.push(allStudentsArray.slice(i, i + 100));

      // ! DEJAREMOS LOS 3 ADD ACA PARA AGREGAR ABSOLUTAMENTE TODOS LOS VALORES DE LOS SELECT DINAMICAMENTE DE LO CONTRARIO SOLO AGREGARA LOS QUE EXISTAN ENTRE LOS USUARIOS CARGADOS
      // // * SAVE ALL DYNAMIC FILTERS
      // allStudentsArray.forEach((data) => {
      //   // ** SAVE ALL DESTINATION CITY
      //   if (data.destination_c !== "NULL") this.allDestinationCity.add(data.destination_c.toLowerCase());
      //   // ** SAVE ALL LANGUAGES
      //   if (data.lang_s !== "NULL" && data.lang_s !== "None") this.allLanguages.add(data.lang_s.toLowerCase());
      //   // ** SAVE ALL ANOTHER LANGUAGES
      //   if (data.language_a !== "NULL" && data.language_a !== "None") {
      //     if (data.language_a.includes(",")) {
      //       data.language_a.split(",").forEach((lang) => this.allAnotherLanguages.add(lang.toLowerCase().trim()));
      //     } else this.allAnotherLanguages.add(data.language_a.toLowerCase());
      //   }
      // });

      // // * ADD ALL LANGUAGES
      // this.allLanguages.forEach((lang) => {
      //   const $lang = D.createElement("option");
      //   $lang.value = lang;
      //   $lang.textContent = lang;
      //   D.querySelector("#language").appendChild($lang);
      // });

      // // * ADD ALL ANOTHER LANGUAGES
      // this.allAnotherLanguages.forEach((lang) => {
      //   const $lang = D.createElement("option");
      //   $lang.value = lang;
      //   $lang.textContent = lang;
      //   D.querySelector("#another-language").appendChild($lang);
      // });
      // // * ADD ALL DESTINATION CITY
      // this.allDestinationCity.forEach((city) => {
      //   const $city = D.createElement("option");
      //   $city.value = city;
      //   $city.textContent = city;
      //   D.querySelector("#destination-city").appendChild($city);
      // });

      this.allStudents = allStudentsArray;
      await this.renderStudents(allStudentsArray.slice(0, 100));
      await this.filterStudents();
    } catch (error) {}
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? OBTENER ACADEMIAS
  async getAcademys() {
    try {
      const formData = new FormData();

      const OPTIONS = {
        method: "POST",
        header: { "Content-type": "application/json; charset=utf-8" },
        data: formData,
      };

      const req = await axios("../helpers/get_all_schools.php", OPTIONS);
      const data = await req.data;

      if (!data) throw "No se ha recibido datos";
      if (data && data instanceof Array && data[0] === "redirect") window.location.href = data[1];

      data.forEach((academy, i) => {
        const $clone = D.createElement("option");
        for (const key in academy[0]) {
          if (academy[0]["name_a"] !== "NULL") {
            $clone.textContent = academy[0]["name_a"];
            $clone.value = academy[0]["name_a"].toLowerCase();
          }
        }
        if ($clone.textContent) D.querySelector("#academy").appendChild($clone);
      });
    } catch (error) {
      console.error(error);
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? SEARCH STUDENT WITH TEXT
  /** @param {String} searchText text to search */
  searchStudent(searchText = this.searchInput$.value.toLowerCase()) {
    console.clear();
    this.studentsContainer$.querySelectorAll(".card-student").forEach((student, i) => student.remove());

    if (searchText) {
      if (this.studentsContainer$.childElementCount - 2 !== this.allStudents.length) {
        this.renderStudents(this.allStudents);
      }

      this.studentsContainer$.querySelectorAll(".card-student").forEach((student, i) => {
        const fullName = student.querySelector("[data-student-name]").textContent.toLowerCase();
        const lastName = student.querySelector("[data-student-name]").textContent.toLowerCase().split(" ")[1].trim();
        const email = student.querySelector("[data-student-name]").dataset.email.toLowerCase();
        email.includes(searchText) || fullName.includes(searchText) || lastName.includes(searchText)
          ? this.showElement(student, true)
          : this.showElement(student, false);
      });

      this.showElement(this.seeMore$, false);
      this.showElement(this.showMore$, false);
    } else {
      const currentPage = Number(this.seeMore$.dataset.page);
      for (let i = 0; i <= currentPage; i++) this.renderStudents(this.pages[i]);
      this.filterStudents();

      this.studentsContainer$.querySelectorAll(".card-student").forEach((student, i, totalStudents) => {
        if (i + 1 <= 100) this.showElement(student, true);
        else this.showElement(student, false);

        if (this.filterSelected$.querySelectorAll("[data-tag]").length === 0 && totalStudents.length > 100) {
          this.showElement(this.seeMore$, false);
          this.showElement(this.showMore$, true);
        } else this.paginationControl();
      });
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? SEARCH STUDENT BY [FIRSTDAY-LASTDAY]
  /** @param {Date} date Date range to evaluate */
  searchDateStudent(date = D.querySelector(".daterangepicker .drp-selected").textContent) {
    D.querySelector("#search-date").value = date;

    this.showElement(D.querySelector(".quit-date"), true);
    this.showElement(this.seeMore$, false);
    this.showElement(this.showMore$, false);

    const firstDay = date.split("-")[0];
    const lastDay = date.split("-")[1];
    const firstDayUnix = new Date(firstDay).getTime();
    const lastDayUnix = new Date(lastDay).getTime();

    // console.group("Evaluando Fechas");
    this.studentsContainer$.querySelectorAll(".card-student").forEach((student) => {
      const studentName = student.querySelector("[data-student-name]").textContent;
      const firstDayStudent = student.querySelector("[data-student-first-day]").textContent;
      const lastDayStudent = student.querySelector("[data-student-last-day]").textContent;
      const firstDayUnixStudent = new Date(firstDayStudent).getTime();
      const lastDayUnixStudent = new Date(lastDayStudent).getTime();

      let studentAvailable = false;
      if (firstDayUnixStudent >= firstDayUnix && lastDayUnixStudent <= lastDayUnix) studentAvailable = true;
      else if (firstDayUnixStudent >= firstDayUnix && firstDayUnixStudent <= lastDayUnix) studentAvailable = true;
      else if (lastDayUnixStudent >= firstDayUnix && lastDayUnixStudent <= lastDayUnix) studentAvailable = true;

      if (studentAvailable) {
        this.showElement(student, true);
        student.dataset.dateAvailable = true;
      } else {
        this.showElement(student, false);
        student.dataset.dateAvailable = false;
      }
    });
    // console.groupEnd();
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? FILTER STUDENTS
  /**  @param {Array} students All students to rendered */
  async renderStudents(students) {
    return new Promise((resolve, reject) => {
      const $fragment = D.createDocumentFragment();
      students.forEach((data, i) => {
        // ** SAVE ALL DESTINATION CITY
        if (data.destination_c !== "NULL") this.allDestinationCity.add(data.destination_c.toLowerCase());
        // ** SAVE ALL LANGUAGES
        if (data.lang_s !== "NULL" && data.lang_s !== "None") this.allLanguages.add(data.lang_s.toLowerCase());
        // ** SAVE ALL ANOTHER LANGUAGES
        if (data.language_a !== "NULL" && data.language_a !== "None") {
          if (data.language_a.includes(",")) {
            data.language_a
              .toLowerCase()
              .split(" and ")
              .forEach((lang) => this.allAnotherLanguages.add(lang.toLowerCase().trim()));
          } else if (data.language_a.toLowerCase().includes(" and ")) {
          } else this.allAnotherLanguages.add(data.language_a.toLowerCase());
        }

        const $cardStudent = D.importNode(this.templateStudentCard$, true);
        // * BASIC INFORMATION
        $cardStudent.querySelector(".card-student").dataset.dateRegister = new Date(data.date_register).getTime();
        $cardStudent.querySelector("[data-student-profile]").dataset.studentProfile = data.id_student;
        $cardStudent.querySelector("[data-student-image]").src = "../" + data.photo_s;
        $cardStudent.querySelector("[data-student-name]").textContent = `${data.name_s} ${data.l_name_s}`;
        $cardStudent.querySelector("[data-student-name]").dataset.email = data.mail_s;
        $cardStudent.querySelector("[data-student-gender]").textContent = data.gen_s;
        // -->
        if (data.db_s === "Empty") $cardStudent.querySelector("[data-student-age]").textContent = "Empty";
        else $cardStudent.querySelector("[data-student-age]").textContent = this.getAge(data.db_s);
        // -->
        if (data.name_academy === "NULL") data.name_academy = "Empty";
        $cardStudent.querySelector("[data-student-academy]").textContent = data.name_academy;

        // * NATIONALITY
        $cardStudent.querySelector("[data-student-nationality]").textContent = data.nationality;

        // * FIRST DAY / LAST DAY
        $cardStudent.querySelector("[data-student-first-day]").textContent = data.firstd;
        $cardStudent.querySelector("[data-student-last-day]").textContent = data.lastd;

        // * TIME STAY
        if (data.firstd && data.firstd !== "NULL" && data.lastd && data.lastd !== "NULL") {
          const days = this.getTimeStay(data.firstd, data.lastd);
          if (days % 7 === 0) {
            $cardStudent.querySelector("[data-student-stay]").textContent = "Weeks of Stay";
            $cardStudent.querySelector("[data-student-time-stay]").textContent = `${days / 7} Weeks`;
          } else {
            $cardStudent.querySelector("[data-student-stay]").textContent = "Days of Stay";
            $cardStudent.querySelector("[data-student-time-stay]").textContent = `${days} Days`;
          }

          // * ARRIVAL DATE
          const arrivalDate = this.getTimeStay(
            `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`,
            data.firstd
          );
          if (arrivalDate >= 0) {
            $cardStudent.querySelector("[data-arrival-date]").dataset.arrivalDate = arrivalDate;
          }
        } else $cardStudent.querySelector("[data-student-time-stay]").textContent = "Empty";

        // * SITUATION
        $cardStudent.querySelector("[data-student-situation]").textContent = data.status;

        // * TYPE OF STUDENT
        if (data.type_s.toLowerCase() === "camps") data.type_s = "Summer Camps";
        $cardStudent.querySelector("[data-student-type-student]").textContent = data.type_s;

        // * LANGUAGES
        if (data.language_a === "NULL") $cardStudent.querySelector("[data-student-language]").textContent = data.lang_s;
        else $cardStudent.querySelector("[data-student-language]").textContent = `${data.lang_s}, ${data.language_a}`;

        // * DESTINATION CITY
        $cardStudent.querySelector("[data-student-destination-city]").textContent = data.destination_c;

        // * TYPE ACCOMMODATION
        $cardStudent.querySelector("[data-student-type-accommodation]").textContent = data.lodging_type;

        // * FOOD SERVICE / MEAL PLAN
        $cardStudent.querySelector("[data-student-meal-plan]").textContent = data.food;

        // * SPECIAL DIET
        if (
          (data.vegetarians === "NULL" || data.vegetarians.toLowerCase() === "no") &&
          (data.halal === "NULL" || data.halal.toLowerCase() === "no") &&
          (data.kosher === "NULL" || data.kosher.toLowerCase() === "no") &&
          (data.lactose === "NULL" || data.lactose.toLowerCase() === "no") &&
          (data.gluten === "NULL" || data.gluten.toLowerCase() === "no") &&
          (data.pork === "NULL" || data.pork.toLowerCase() === "no")
        ) {
          $cardStudent.querySelector("[data-student-special-diet]").textContent = "No";
        } else {
          $cardStudent.querySelector("[data-student-special-diet]").textContent = "Yes";
          const diet = [];
          if (data.vegetarians.toLowerCase() === "yes") diet.push("vegetarians");
          if (data.halal.toLowerCase() === "yes") diet.push("halal");
          if (data.kosher.toLowerCase() === "yes") diet.push("kosher");
          if (data.lactose.toLowerCase() === "yes") diet.push("lactose");
          if (data.gluten.toLowerCase() === "yes") diet.push("gluten");
          if (data.pork.toLowerCase() === "yes") diet.push("pork");
          $cardStudent.querySelector("[data-student-special-diet]").dataset.specialDiet = diet.join(", ");
        }
        $cardStudent.querySelector("[data-student-smokers]").textContent = data.smoke_s;
        $cardStudent.querySelector("[data-student-children]").textContent = data.children;
        $cardStudent.querySelector("[data-student-teenagers]").textContent = data.teenagers;
        $cardStudent.querySelector("[data-student-pets]").textContent = data.pets;
        $cardStudent.querySelector("[data-student-pick-up]").textContent = data.pick_up;
        $cardStudent.querySelector("[data-student-drop-off]").textContent = data.drop_off;

        $fragment.appendChild($cardStudent);
      });
      this.studentsContainer$.appendChild($fragment);

      // * ADD ALL LANGUAGES
      D.querySelector("#language").innerHTML = `<option value="" hidden>Select Language</option>`;
      this.allLanguages.forEach((lang) => {
        const $lang = D.createElement("option");
        $lang.value = lang;
        $lang.textContent = lang;
        D.querySelector("#language").appendChild($lang);
      });
      // * ADD ALL ANOTHER LANGUAGES
      D.querySelector("#another-language").innerHTML = `<option value="" hidden>Select Another Language</option>`;
      this.allAnotherLanguages.forEach((lang) => {
        const $lang = D.createElement("option");
        $lang.value = lang;
        $lang.textContent = lang;
        D.querySelector("#another-language").appendChild($lang);
      });
      // * ADD ALL DESTINATION CITY
      D.querySelector("#destination-city").innerHTML = `<option value="" hidden>Select Destination City</option>`;
      this.allDestinationCity.forEach((city) => {
        if (city !== "empty") {
          const $city = D.createElement("option");
          $city.value = city;
          $city.textContent = city;
          D.querySelector("#destination-city").appendChild($city);
        }
      });

      this.paginationControl();

      resolve(true);
    });
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? FILTER STUDENTS
  async filterStudents() {
    if (D.querySelector(".not-student-result")) D.querySelector(".not-student-result").remove();

    const studentsFiltered = {
      0: new Set(),
      1: new Set(),
      2: new Set(),
      3: new Set(),
      4: new Set(),
      5: new Set(),
      6: new Set(),
      7: new Set(),
      8: new Set(),
      9: new Set(),
      10: new Set(),
      11: new Set(),
      12: new Set(),
      13: new Set(),
      14: new Set(),
      15: new Set(),
      16: new Set(),
      17: new Set(),
      18: new Set(),
      19: new Set(),
      20: new Set(),
      21: new Set(),
      22: new Set(),
      23: new Set(),
    };

    D.querySelectorAll("#all-students > div").forEach((student) => {
      const studentName = student.querySelector("[data-student-name]").textContent;
      // * BASIC INFORMATION
      const situation = student.querySelector(`[data-student-situation]`).textContent.toLowerCase();
      const city = student.querySelector("[data-student-destination-city]").textContent.toLowerCase();
      const dateRange = student.querySelector(`[data-arrival-date]`).dataset.arrivalDate;
      const typeRoom = student.querySelector("[data-student-type-accommodation]").textContent.toLowerCase();
      const gender = student.querySelector("[data-student-gender]").textContent.toLowerCase();
      const food = student.querySelector("[data-student-meal-plan]").textContent.toLowerCase();
      const diet = student.querySelector("[data-student-special-diet]").dataset.specialDiet;
      const age = student.querySelector("[data-student-age]").textContent.split(" ")[0];
      const pickUpService = student.querySelector("[data-student-pick-up]").textContent.toLowerCase();
      const dropOffService = student.querySelector("[data-student-drop-off]").textContent.toLowerCase();
      const academy = student.querySelector(`[data-student-academy]`).textContent.toLowerCase();
      // ** LANGUAGE
      const language = student.querySelector(`[data-student-language]`).textContent.toLowerCase();
      // ** END LANGUAGE
      const typeStudent = student.querySelector("[data-student-type-student]").textContent.toLowerCase();
      // ** CAN SHARE WITH
      const shareSmokers = student.querySelector("[data-student-smokers]").textContent.toLowerCase();
      const shareChildren = student.querySelector("[data-student-children]").textContent.toLowerCase();
      const shareTeenager = student.querySelector("[data-student-teenagers]").textContent.toLowerCase();
      const sharePets = student.querySelector("[data-student-pets]").textContent.toLowerCase();

      const filtersFound = {};
      let filtersTraveled = 0;
      const filterKey = Object.keys(this.filters);
      Object.values(this.filters).forEach((filter, i) => {
        if (filter instanceof Object) {
          const dietKeys = Object.keys(filter);
          Object.values(filter).forEach((dietFilter, i) => {
            if (dietFilter) {
              if (dietKeys[i] === "vegetarians" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              if (dietKeys[i] === "halal" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              if (dietKeys[i] === "kosher" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              if (dietKeys[i] === "lactose" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              if (dietKeys[i] === "gluten" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              if (dietKeys[i] === "pork" && diet.includes(dietFilter)) filtersFound[dietKeys[i]] = dietFilter;
              filtersTraveled++;
            }
          });
        } else if (filter) {
          if (filterKey[i] === "situation" && situation.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "destinationCity" && city.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "dateRange") {
            if (filter === "1 - 5 days" && dateRange >= 1 && dateRange <= 5) filtersFound[filterKey[i]] = filter;
            if (filter === "6 - 14 days" && dateRange >= 6 && dateRange <= 14) filtersFound[filterKey[i]] = filter;
            if (filter === "15 - 30 days" && dateRange >= 15 && dateRange <= 30) filtersFound[filterKey[i]] = filter;
            if (filter === "30 days onwards" && dateRange >= 30) filtersFound[filterKey[i]] = filter;
          }
          if (filterKey[i] === "typeAccommodation" && typeRoom.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "gender" && gender === filter) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "food" && filter.includes(food)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "ageRange") {
            if (filter === "0 - 8 years" && age > 0 && age <= 8) filtersFound[filterKey[i]] = filter;
            if (filter === "8 - 14 years" && age >= 8 && age <= 14) filtersFound[filterKey[i]] = filter;
            if (filter === "14 - 21 years" && age >= 14 && age <= 21) filtersFound[filterKey[i]] = filter;
            if (filter === "22 years onwards" && age >= 22) filtersFound[filterKey[i]] = filter;
          }
          if (filterKey[i] === "pickUpService" && filter.includes(pickUpService)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "dropOffService" && filter.includes(dropOffService)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "academy" && academy.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "language" && language.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "anotherLanguage" && language.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "typeStudent" && typeStudent.includes(filter)) filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "shareWithSmokers" && shareSmokers === "yes") filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "shareWithChildren" && shareChildren === "yes") filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "shareWithTeenager" && shareTeenager === "yes") filtersFound[filterKey[i]] = filter;
          if (filterKey[i] === "shareWithPets" && sharePets === "yes") filtersFound[filterKey[i]] = filter;
          if (filter !== "show all") filtersTraveled++;
        }
      });

      studentsFiltered[Object.keys(filtersFound).length].add(student);
      if (Object.keys(filtersFound).length === filtersTraveled) student.dataset.perfect = "yes";
      else student.dataset.perfect = "no";
    });

    let rank = 23;
    while (rank >= 0) {
      studentsFiltered[rank].forEach((student) => this.studentsContainer$.appendChild(student));
      rank--;
    }

    if (D.querySelector(".see-more")) this.studentsContainer$.appendChild(D.querySelector(".see-more"));
    if (D.querySelector(".show-more")) this.studentsContainer$.appendChild(D.querySelector(".show-more"));

    if (this.searchDateInput$.value) this.searchDateStudent(this.searchDateInput$.value);

    // DEMON FUNCION XD
    const showAllControl = (checked) => {
      D.querySelectorAll(".card-student").forEach((student) => {
        if (this.searchDateInput$.value) {
          this.showElement(this.seeMore$, false);
          this.showElement(this.showMore$, false);
          if (checked) {
            student.dataset.dateAvailable === "true"
              ? this.showElement(student, true)
              : this.showElement(student, false);
          } else {
            student.dataset.dateAvailable === "true" && student.dataset.perfect === "yes"
              ? this.showElement(student, true)
              : this.showElement(student, false);
          }
        } else {
          if (checked) this.showElement(student, true);
          else {
            student.dataset.perfect === "yes" ? this.showElement(student, true) : this.showElement(student, false);
          }
        }
      });
    };

    D.querySelector("#search-all").checked ? showAllControl(true) : showAllControl(false);

    if (this.searchInput$.value) this.searchStudent();

    if (D.querySelectorAll("#quit-filter").length) {
      this.showElement(D.querySelector(".see-more"), false);
      if (!this.studentsContainer$.querySelectorAll(".card-student.d-flex").length) {
        this.studentsContainer$.innerHTML += `
        <div class="mt-5 d-flex flex-lg-column justify-content-center align-items-center not-student-result">
            <p class="h3 mb-1 text-muted">Not Student Results</p>
            <p>Remove some filters to improve the search</p>
            <button class="btn clear-all-filters w-75">Clear all filters</button>
            </div>
        `;
      }
    } else {
      if (this.seeMore$.dataset.page == this.pages.length - 1) this.showElement(D.querySelector(".see-more"), false);
      else this.showElement(D.querySelector(".see-more"), true);
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? SPECIAL DIET FILTER CONTROL
   * @param {Element} btn Button to rate
   */
  foodControl(btn) {
    if (btn.dataset.filter === "food") {
      if (btn.value.includes("yes") && btn.checked) D.querySelector(".search-special-diet").classList.add("show");
      else {
        if (D.querySelector('[data-tag="vegetarians"]'))
          D.querySelector('[data-tag="vegetarians"] #quit-filter').click();
        if (D.querySelector('[data-tag="halal"]')) D.querySelector('[data-tag="halal"] #quit-filter').click();
        if (D.querySelector('[data-tag="kosher"]')) D.querySelector('[data-tag="kosher"] #quit-filter').click();
        if (D.querySelector('[data-tag="lactose"]')) D.querySelector('[data-tag="lactose"] #quit-filter').click();
        if (D.querySelector('[data-tag="gluten"]')) D.querySelector('[data-tag="gluten"] #quit-filter').click();
        if (D.querySelector('[data-tag="pork"]')) D.querySelector('[data-tag="pork"] #quit-filter').click();
        this.filters.specialDiet.vegetarians = "";
        this.filters.specialDiet.halal = "";
        this.filters.specialDiet.kosher = "";
        this.filters.specialDiet.lactose = "";
        this.filters.specialDiet.gluten = "";
        this.filters.specialDiet.pork = "";
        D.querySelector(".search-special-diet").classList.remove("show");
      }

      const $anotherInputs = btn.parentElement.parentElement.querySelectorAll("input");
      $anotherInputs.forEach((input) => {
        if (btn.checked && btn !== input) input.checked = false;
      });
    }

    this.filterUtility(btn);
  }
  // !
  // !
  // !
  // !
  // !
  // !
  // ! UTILITY
  /** // ? SHOW OR HIDE ELEMENT
   * @param {Element} elem Element to show or hide
   * @param {Boolean} show (True = show element || False = hide element)
   */
  showElement(elem, show) {
    if (elem) show ? elem.classList.replace("d-none", "d-flex") : elem.classList.replace("d-flex", "d-none");
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  // ? PAGINATION AND BEHAVIOR CONTROL
  paginationControl(showMore, seeMore) {
    let currentPage = this.seeMore$.dataset.page;
    if (seeMore) {
      currentPage++;
      if (this.pages[currentPage].length) this.renderStudents(this.pages[currentPage]);
      this.seeMore$.dataset.page = currentPage;
      currentPage = this.seeMore$.dataset.page;
    }

    if (this.pages[currentPage].length === 100 && this.pages[Number(currentPage) + 1].length > 0) {
      this.showElement(this.seeMore$, true);
    } else this.showElement(this.seeMore$, false);

    if (showMore) {
      this.showElement(this.showMore$, false);
      this.studentsContainer$.querySelectorAll(".card-student").forEach((student) => this.showElement(student, true));
    }

    if (this.seeMore$.dataset.page == this.pages.length - 1) this.seeMore$.remove();
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? GET AGE
   * @param {Date} date Date birth of the student
   * @returns Age Example: [24 - Adult] / [17 - Teenager] / [5 - Child]
   */
  getAge(date) {
    if (!date) return "Empty";
    if (Number(date.split("-")[0]) >= new Date().getFullYear()) return `Not Born Yet`;

    const currentDate = new Date();
    const dateBirth = new Date(date);
    let age = currentDate.getFullYear() - dateBirth.getFullYear();
    const month = currentDate.getMonth() - dateBirth.getMonth();

    if (month < 0 || (month === 0 && currentDate.getDate() < dateBirth.getDate())) age--;

    if (age > 0 && age <= 8) return `${age} - Child`;
    if (age > 8 && age <= 21) return `${age} - Teenager`;
    if (age > 21) return `${age} - Adult`;
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? GET DAYS left:
   * @param {Date} initialDate Initial date to count
   * @param {Date} finalDate Final date to count
   * @returns Remaining days
   */
  getTimeStay(initialDate, finalDate) {
    let firstDate = initialDate.split("-");
    let lastDate = finalDate.split("-");
    let firstDateResult = Date.UTC(firstDate[0], firstDate[1] - 1, firstDate[2]);
    let lastDateResult = Date.UTC(lastDate[0], lastDate[1] - 1, lastDate[2]);
    const CALCULATION = lastDateResult - firstDateResult;
    const DAYS = Math.floor(CALCULATION / (1000 * 60 * 60 * 24));

    return DAYS;
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? MOBILE SHOW OR HIDE ALL FILTERS
   * @param {Boolean} show True=Show filters / False=Hide filters
   */
  allFiltersMobile(show) {
    if (show) {
      D.body.style.overflowY = "hidden";
      D.querySelector("#all-filters").classList.add("show");
    } else {
      D.body.style.overflowY = "scroll";
      D.querySelector("#all-filters").classList.remove("show");
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? ADD FILTER
   * @param {String} dataTag Type of filter to add
   * @param {String} value Value of the filter
   */
  addFilter(dataTag, value) {
    if (!this.filterSelected$.querySelector(`[data-tag="${dataTag}"]`) || dataTag.includes("share")) {
      const $filterTag = D.createElement("span");
      $filterTag.className = "mb-1 mr-2";
      $filterTag.dataset.tag = dataTag;
      $filterTag.innerHTML = `${value} <span id="quit-filter">X</span>`;
      this.filterSelected$.appendChild($filterTag);
    } else {
      this.filterSelected$.querySelector(`[data-tag="${dataTag}"]`).innerHTML = `
        ${value} <span id="quit-filter">X</span>
      `;
    }

    if (dataTag === "academy") {
      this.filterSelected$.querySelector(`[data-tag="${dataTag}"]`).title = "See academy";
      this.filterSelected$.querySelector(`[data-tag="${dataTag}"]`).classList.add("filter-scroll");
      this.filterSelected$.querySelector(`[data-tag="${dataTag}"]`).innerHTML = `
        School <span id="quit-filter">X</span>
      `;
    }

    if (["vegetarians", "halal", "kosher", "lactose", "gluten", "pork"].includes(dataTag)) {
      this.filters.specialDiet[dataTag] = value;
    } else this.filters[dataTag] = value;

    if (this.filterSelected$.querySelectorAll("#quit-filter").length) {
      this.showElement(this.filterSelected$.querySelector("div"), true);
      this.filterSelected$.appendChild(this.filterSelected$.querySelector("div"));
    }

    this.filterStudents();
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? QUIT FILTER
   * @param {Element} btn Element to rate
   */
  quitFilter(btn) {
    const $parent = btn.parentElement;
    const dataTag = $parent.dataset.tag;

    let value;
    btn.tagName === "SELECT" ? (value = btn.value) : (value = $parent.textContent.replace(" X", "").trim());

    if (dataTag === "academy") D.querySelector("#academy").value = "";
    else {
      D.querySelectorAll(`[data-filter="${dataTag}"]`).forEach((input) => {
        if (input.value === value) input.tagName === "SELECT" ? (input.value = "") : (input.checked = false);
      });
    }

    if (value === "food yes") {
      if (D.querySelector('[data-tag="vegetarians"]')) D.querySelector('[data-tag="vegetarians"] #quit-filter').click();
      if (D.querySelector('[data-tag="halal"]')) D.querySelector('[data-tag="halal"] #quit-filter').click();
      if (D.querySelector('[data-tag="kosher"]')) D.querySelector('[data-tag="kosher"] #quit-filter').click();
      if (D.querySelector('[data-tag="lactose"]')) D.querySelector('[data-tag="lactose"] #quit-filter').click();
      if (D.querySelector('[data-tag="gluten"]')) D.querySelector('[data-tag="gluten"] #quit-filter').click();
      if (D.querySelector('[data-tag="pork"]')) D.querySelector('[data-tag="pork"] #quit-filter').click();
      this.filters.specialDiet.vegetarians = "";
      this.filters.specialDiet.halal = "";
      this.filters.specialDiet.kosher = "";
      this.filters.specialDiet.lactose = "";
      this.filters.specialDiet.gluten = "";
      this.filters.specialDiet.pork = "";
      D.querySelector(".search-special-diet").classList.remove("show");
    }

    this.filters[dataTag] = "";
    $parent.remove();

    if (this.filterSelected$.querySelectorAll("#quit-filter").length === 0) {
      this.showElement(this.filterSelected$.querySelector("div"), false);
    }

    this.filterStudents();
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  filterUtility(button) {
    if (button.checked) this.addFilter(button.dataset.filter, button.value);
    else {
      this.filters[button.dataset.filter] = "";
      if (this.filters.specialDiet[button.dataset.filter]) this.filters.specialDiet[button.dataset.filter] = "";
      D.querySelector(`[data-tag="${button.dataset.filter}"]`).remove();
      this.filterStudents();
    }

    if (this.filterSelected$.querySelectorAll("#quit-filter").length === 0) {
      this.showElement(this.filterSelected$.querySelector("div"), false);
    }
  }
  // ?
  // ?
  // ?
  // ?
  // ?
  // ?
  /** // ? CHECKBOX FILTER CONTROL
   * @param {Element} btn Button to rate
   */
  checkboxControl(btn) {
    const $anotherInputs = btn.parentElement.parentElement.querySelectorAll("input");
    $anotherInputs.forEach((input) => {
      if (btn.checked && btn !== input) input.checked = false;
    });

    this.filterUtility(btn);
  }
}

// TODO INSTANCIA
const INSTANCE = new AllStudents();

// TODO EVENTOS
// ! DOMContentLoaded
D.addEventListener("DOMContentLoaded", async (e) => {
  await INSTANCE.getStudents();
  await INSTANCE.getAcademys();

  _Messages.quitMessage();
});

// ! CLICK
D.addEventListener("click", (e) => {
  // ? SHOW MORE INFORMATION ABOUT THE STUDENT
  if (e.target.matches("[data-more-info-button]")) {
    e.target.parentElement.parentElement.parentElement.classList.toggle("show-more-info");
  }

  // ? LINK TO STUDENT PROFILE
  if (e.target.matches("[data-student-profile]")) {
    window.location.href = `student_info?art_id=${e.target.dataset.studentProfile}`;
  }

  // ? QUIT FILTER
  if (e.target.matches("#quit-filter")) INSTANCE.quitFilter(e.target);
  // ? QUIT ALL FILTERS
  if (e.target.matches(".clear-all-filters")) {
    D.querySelector("#search-date").value = "";
    INSTANCE.filterSelected$.querySelectorAll("#quit-filter").forEach((tag) => tag.click());
    INSTANCE.filterStudents();
  }

  // ? MOBILE SHOW ALL FILTERS
  if (e.target.matches(".btn-filter")) INSTANCE.allFiltersMobile(true);
  // ? MOBILE HIDE ALL FILTERS
  if (e.target.matches(".hide-filters")) INSTANCE.allFiltersMobile(false);

  // ? CHECKBOX CONTROL
  if (e.target.matches(`input[type="checkbox"]`) && e.target.id.includes("search")) {
    if (e.target.dataset.filter.includes("share")) INSTANCE.filterUtility(e.target);
    else if (e.target.id.includes("food-service") || e.target.parentElement.parentElement.className.includes("diet")) {
      INSTANCE.foodControl(e.target);
    } else INSTANCE.checkboxControl(e.target);
  }

  // ? SEARCH STUDENT DATE
  if (e.target.matches(".applyBtn")) INSTANCE.searchDateStudent();
  // ? CLEAR SEARCH DATE INPUT
  if (e.target.matches(".quit-date") && D.querySelector("#search-date").value) {
    D.querySelector("#search-date").value = "";
    INSTANCE.showElement(e.target, false);
    INSTANCE.filterStudents();
  }

  // ? SCROLL TO
  if (e.target.matches(".filter-scroll")) {
    D.querySelector(".search-academy").classList.add("highlight");
    D.querySelector("#academy").scrollIntoView({ block: "center", behavior: "smooth" });
    setTimeout(() => D.querySelector(".search-academy").classList.remove("highlight"), 2000);
  }

  // ? SEE MORE STUDENTS
  if (e.target.matches(".see-more")) INSTANCE.paginationControl(null, true);
  // ? SHOW STUDENTS HIDE
  if (e.target.matches(".show-more")) INSTANCE.paginationControl(true, null);
});

// ! CHANGE
D.addEventListener("change", (e) => {
  if (e.target.matches("select[data-filter]")) INSTANCE.addFilter(e.target.dataset.filter, e.target.value);
});

// ! KEY UP
D.addEventListener("keyup", (e) => {
  if (e.target.matches("#search-student")) INSTANCE.searchStudent();
});

// ! SCROLL
D.getElementById("all-filters").addEventListener(
  "scroll",
  (e) => {
    if (D.querySelector(".daterangepicker").style.cssText) {
      D.querySelector(".daterangepicker").style.cssText = "";
      D.querySelector(".cancelBtn").click();
    }
  },
  true
);

// ! ERROR
W.addEventListener(
  "error",
  (e) => {
    if (e.target.tagName === "IMG") e.target.src = "../assets/emptys/profile-student-empty.png";
  },
  true
);
