const D = document;
const W = window;

// ! MONTHS INFORMATION
const $templateMonths = D.querySelector("#template_months").content.cloneNode(true);
const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

// ! DAYS INFORMATION
const $templateDays = D.querySelector("#template_days").content.cloneNode(true);
var diasSemana = ["Saturday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Sunday"];

// ! TEMPLATE EVENTS
const $templateEvents = D.querySelector("#template_events").content.cloneNode(true);
const EVENTS = [];

let currentYear = "";
let yearsAvailable = [];

async function getEvents() {
  const formGetEvents = new FormData();
  formGetEvents.set("request", "getEvents");
  const DATA = { method: "POST", body: formGetEvents };

  const jsonGetEvents = await fetch("../helpers/get_all_events.php");
  const resultGetEvents = await jsonGetEvents.json();

  resultGetEvents.forEach((event) => EVENTS.push(event));
}

function daysOfTHeMonths(month, year) {
  return new Date(year, month, 0).getDate();
}

function showYears() {
  var myDate = new Date();

  var year = myDate.getFullYear();

  EVENTS.forEach((event) => {
    const ArrayStartEvent = event.start.split("-");
    const ArrayEndEvent = event.end.split("-");

    yearsAvailable.push(ArrayEndEvent[0]);
    yearsAvailable.push(ArrayStartEvent[0]);
  });

  var min = Math.min(...yearsAvailable);
  var max = Math.max(...yearsAvailable);

  console.log(yearsAvailable, min, max);

  for (var i = min; i < max + 1; i++) {
    D.querySelector("#years").innerHTML += '<option value="' + i + '">' + i + "</option>";
  }

  currentYear = year;

  getMonths(currentYear);
}

function getMonths(year) {
  D.querySelector("#parent_months").innerHTML = "";
  const $fragment = D.createDocumentFragment();

  months.forEach((month, i) => {
    const $clone = D.importNode($templateMonths, true);
    $clone.querySelector("#months").textContent = month.substring(0, 3);
    $clone.querySelector("#months").dataset.month = month;
    $clone.querySelector("#months").dataset.year = year;
    $clone.querySelector("#months").dataset.numberMonth = i + 1;

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_months").appendChild($fragment);
}

function showDays(numberMonth, year) {
  let totalDays = daysOfTHeMonths(numberMonth, year);
  D.querySelector("#header").innerHTML = "<th class='row_reservation'></th>";

  const $fragment = D.createDocumentFragment();
  for (var i = 1; i < totalDays + 1; i++) {
    const $clone = D.importNode($templateDays, true);
    var indice = new Date(year, numberMonth - 1, i).getDay();
    // console.log(`El día número ${i} del mes ${numberMonth} del año ${year} es ${}`);

    $clone.querySelector("#days").innerHTML = `
      <span>${diasSemana[indice].substring(0, 3)}</span>
      ${i}
    `;
    $fragment.appendChild($clone);
  }

  D.querySelector("#header").appendChild($fragment);

  renderEvents(numberMonth, year, totalDays);
}

function renderEvents(numberMonth, year, totalDays) {
  // console.log(EVENTS, numberMonth, year);
  D.querySelector(".calendar_body").innerHTML = "";
  const $fragment = D.createDocumentFragment();
  EVENTS.forEach((event) => {
    console.log(event);
    const $clone = D.importNode($templateEvents, true);

    $clone.querySelector("#name_event").textContent = event.houseName;

    for (var i = 1; i < totalDays + 1; i++) {
      if(i === event.start) 

      const celdas = D.createElement("td");
      $clone.querySelector("#events").appendChild(celdas);
    }
    $fragment.appendChild($clone);
    // $fragment.appendChild($templateDaysEvents);
  });

  D.querySelector(".calendar_body").appendChild($fragment);
}

D.addEventListener("DOMContentLoaded", async (e) => {
  await getEvents();
  showYears();
});

D.addEventListener("click", (e) => {
  if (e.target.matches("#months")) showDays(e.target.dataset.numberMonth, e.target.dataset.year);
});

D.addEventListener("change", (e) => {
  if (e.target.matches("#years")) getMonths(e.target.value);
});

// $clone;
