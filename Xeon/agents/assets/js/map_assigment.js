export default async function MAP(allDataHouses) {
  const D = document;
  const W = window;

  // ? OBTENER LOS PARAMETROS DEL URL
  const searchParams = new URLSearchParams(W.location.search);
  // ? OBTENER EL PARAMETRO art_id DE LA URL
  const ID_STUDENT = searchParams.get("art_id");

  const formData = new FormData();
  formData.set("student_id", ID_STUDENT);

  const OPTIONS = {
    method: "POST",
    header: { "content-type": "application/json; charset=utf-8" },
    data: formData,
  };
  const req = await axios("../helpers/agents/map_assigment.php", OPTIONS);
  const academy = await req.data;
  // console.log(academy);

  mapboxgl.accessToken = "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg";
  var client = new MapboxClient(mapboxgl.accessToken);

  // console.log(allDataHouses);

  var directionA;
  academy.address === "NULL" ? (directionA = "Toronto") : (directionA = academy.address);
  var test = await client.geocodeForward(directionA, async (err, data, res) => {
    // data is the geocoding result as parsed JSON
    // res is the http response, including: status, header and entity properties

    var coordinatesA = data.features[0].center;
    //Esta variable define el centro de tu mapa todas las direcciones deben tenerla de la misma manera y zoom o no encontrara el centro
    const mapOption = {
      container: "map",
      style: "mapbox://styles/mapbox/streets-v10",
      center: coordinatesA,
      zoom: 10,
    };
    var map = new mapboxgl.Map(mapOption);

    // * Directions box
    var directions = new MapboxDirections({
      accessToken: "pk.eyJ1IjoibGF3aXgxMCIsImEiOiJjamJlOGE1bmcyZ2V5MzNtcmlyaWRzcDZlIn0.ZRQ73zzVxwcADIPvsqB6mg",
      unit: "metric",
    });
    map.addControl(directions, "top-right");
    D.querySelector(".directions-icon-depart").textContent = "House";
    D.querySelector(".directions-icon-arrive").textContent = "Academy";

    // * POPUP ACADEMY
    var popupA = new mapboxgl.Popup({ offset: 25 }).setHTML(
      `<strong>Name: </strong>${academy.name}.<br><strong>Direction: </strong>${academy.address}.<br><img src="${academy.photo}" class="w-100" id="map-img">`
    );
    var el = document.createElement("div");
    el.id = "marker";
    new mapboxgl.Marker(el).setLngLat(coordinatesA).setPopup(popupA).addTo(map);

    Object.values(allDataHouses).forEach((houses) => {
      console.log(houses);
      if (academy.address !== "NULL") {
        map.on("load", function () {
          directions.setOrigin(houses.address);
          directions.setDestination(directionA);
        });
      }
      var test = client.geocodeForward(houses.address, (err, data, res) => {
        console.log(data.features);
        // console.log(data.features[0].place_name);
        var popup5 = new mapboxgl.Popup({
          offset: 25,
        }).setHTML(`
          <img src="${houses.photo}" class="w-100" id="map-img">
          <article class="px-3 popup_container">
            <h5>${houses.name}</h5>
            <p>${houses.address}</p>
          </article>
        `);
        // create DOM element for the marker
        var ef = document.createElement("div");
        ef.id = "house";
        new mapboxgl.Marker(ef).setLngLat(data.features[0].center).setPopup(popup5).addTo(map);
      });
      // ! revisar ti en data features esta la direccion de la calle que se esta seleccionando
    });
  });
}
/* 
<strong>Name: </strong> <a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.name}</a>.<br><strong>Direction: </strong><a href="detail.php?art_id=${allDataHouses.house1.id}">${allDataHouses.house1.address}</a>.<br><a href="detail.php?art_id=${allDataHouses.house1.id}"><img src="${allDataHouses.house1.photo}" class="w-100" id="map-img"></a>
*/
