//! IMPORTS
import Messages from "../../../controllers/messages.js";

//! VARIABLES
const D = document;
const W = window;
const SCHOOLS = [];

const _Messages = new Messages();

const $templateSchool = D.querySelector("#template__schools").content.cloneNode(true);

// const $parentSchoolDirectory;

//! FUNCTIONS
async function getDirectorySchools() {
  const jsonSchoolList = await fetch("../helpers/agents/get_academys.php");
  const resultSchoolList = await jsonSchoolList.json();

  const $parentSchools = D.querySelector(".section__school-panel");
  const $fragment = D.createDocumentFragment();

  resultSchoolList.forEach((school) => SCHOOLS.push(...school));
  const SCHOOL = SCHOOLS.reverse();
  SCHOOL.forEach((school) => {
    if (school.id_ac !== "0") {
      const $clone = D.importNode($templateSchool, true);

      //TODO SCHOOL PHOTO
      if (school.photo_a !== "NULL") $clone.querySelector(".img_school").src = `../${school.photo_a}`;
      else $clone.querySelector(".img_school").src = `../assets/emptys/academy-empty.jpg`;

      //TODO SCHOOL NAME
      if (school.name_a !== "NULL") $clone.querySelector("#school_name").textContent = school.name_a;
      else $clone.querySelector("#school_name").textContent = "Empty";

      //TODO SCHOOL ADDRESS
      if (school.dir_a !== "NULL" && school.city_a !== "NULL")
        $clone.querySelector("#school_address").textContent = `${school.dir_a}, ${school.city_a}`;
      else $clone.querySelector("#school_address").textContent = "Empty";

      $fragment.appendChild($clone);
    }
  });
  $parentSchools.appendChild($fragment);
}

function searchSchool(search) {
  if (search) {
    D.querySelectorAll(".section__school-panel > article").forEach((school) => {
      const schoolName = school.querySelector("#school_name").textContent.toLowerCase();
      const schoolAddress = school.querySelector("#school_address").textContent.toLowerCase();

      if (schoolName.includes(search) || schoolAddress.includes(search)) school.classList.remove("d-none");
      else school.classList.add("d-none");
    });
  } else D.querySelectorAll(".section__school-panel > article").forEach((school) => school.classList.remove("d-none"));
}

//! EVENTS
D.addEventListener("DOMContentLoaded", (e) => {
  getDirectorySchools();
  _Messages.quitMessage();
});

D.addEventListener("click", (e) => {
  if (e.target.matches(".float_btn")) W.top.location = "academy_register";
});

D.addEventListener("keyup", (e) => searchSchool(e.target.value));
// D.addEventListener("keydown", (e) => searchSchool(e.target.value));
// D.addEventListener("blur", (e) => searchSchool(e.target.value));
