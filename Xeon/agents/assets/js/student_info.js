// ! VARIABLES

const D = document;
const W = window;

let ALL_LIST = undefined;
const TYPE_ROOM = new Map();
const $formChangeHouse = D.querySelector("#form__reservation");
const $buttonSubmitChange = D.querySelector("#btn__submit-reservation");

const searchParams = new URLSearchParams(W.location.search);
// ? OBTENER EL PARAMETRO art_id DE LA URL
const ID = searchParams.get("art_id");

const studentInformation = [];

// TODO BASIC INFORMATION
const $templateBasicInformation = D.querySelector("#template__basic_information").content.cloneNode(true);
const basicLabelLabels = [
  "Email",
  "Destination City",
  "Arrive Date",
  "Date of Birth",
  "Gender",
  "Phone Number",
  "Origin Language",
  "Anoter Language",
  "Passport",
  "Passport Expiration Date",
  "Visa Expiration Date",
];

// TODO ACCOMMODATION REQUEST
const $templateAccommodationRequest = D.querySelector("#template__accomodation_request").content.cloneNode(true);
const accommodationRequestLabels = ["Start Date of Stay", "End Date of Stay"];

// TODO STUDENT ADDRESS
const $templateAddress = D.querySelector("#template__student_address").content.cloneNode(true);
const addressLabels = ["Country", "City", "Province / State", "Postal Code", "Address"];

// TODO PROFESSIONAL INFORMATION
const $templateProfessionalInformation = D.querySelector("#template__professional_information").content.cloneNode(true);
const professionalInformationLabels = [
  "Current English Level",
  "Type Student",
  "Program to Study",
  "Schedule",
  "School Selected",
];

// TODO FLIGHT INFORMATION
const $templateFlightInformation = D.querySelector("#template__flight_information").content.cloneNode(true);
const flightInformationLabels = [
  "Booking Information",
  "Landing Flight Number",
  "Flight Date",
  "Arrival at the Homestay",
];

// TODO SHARE WITH
const $templateShareWith = D.querySelector("#template__share_with").content.cloneNode(true);
const shareWithLabels = ["Smokers?", "Children?", "Teenagers?", "Pets?"];

// TODO ACCOMMODATION
const $templateAccommodation = D.querySelector("#template__accommodation").content.cloneNode(true);
const accommodationLabels = ["Type of Accommodation", "Meal Plan", "Booking fee", "Total weckly price"];

// TODO SUPPLEMENTS
const $templateSupplements = D.querySelector("#template__optional_supplements").content.cloneNode(true);
const supplementsLabels = [
  "Accommodation near the school",
  "Urgent accommodation search",
  "Do you want the Summer Fee plan",
  "Are you under 18?",
  "Guardianship",
];

// TODO TRANSPORT
const $templateTransport = D.querySelector("#template__transport").content.cloneNode(true);
const transportLabels = ["Pick up service", "Drop off service", "Transport price"];

// TODO HEALTH INFORMATION
const $templateHealthInformation = D.querySelector("#template__health_information").content.cloneNode(true);
const healthInformationLabels = [
  "Do you smoke?",
  "Do you drink alcohol?",
  "Have you used any drugs",
  "Any allergy to Animals",
  "Do you suffer from any disease?",
  "Any treatment or Medication?",
  "Any Psychological Treatment?",
  "Any allergies?",
  "Any surgery in the last 12 months?",
  "What is your state if health at the moment",
];

// TODO EMERGENCY CONTACT
const $templateEmergencyContact = D.querySelector("#template__emergency_contact").content.cloneNode(true);
const emergencyInformationLabels = ["Name", "Last Name", "Phone Number", "Relationship"];

// TODO ATTACHED FILES
const $templateAttachedFiles = D.querySelector("#template__attached_files").content.cloneNode(true);
const attachedFilesLabels = ["Passport photo", "Visa photo", "Flight Ticket Image", "Signature"];

// TODO HOUSES REQUESTED
const $templateHousesAssigned = D.querySelector("#template__houses_requested").content.cloneNode(true);

// TODO CHANGE HOUSE
const $templateHouseList = D.querySelector("#houses-list").content.cloneNode(true);
let isChangeHouse = "";

// ! FUNCTIONS
async function getStudentInformation() {
  const formData = new FormData();
  formData.set("student_id", ID);
  formData.set("request", "getStudentInformation");
  const DATA = { method: "POST", body: formData };

  const jsonStudentInformation = await fetch("../helpers/students/student_information.php", DATA);
  const resultStudentInformation = await jsonStudentInformation.json();

  Object.values(resultStudentInformation).forEach((information) => studentInformation.push(information));
}

// TODO FUNCTION => REDER INFORMATION
function renderBasicInformation() {
  // ? STUDENT PHOTO
  if (studentInformation[0].photo_s !== "NULL") {
    D.querySelector("[data-photo-student]").src = `https://homebor.com/${studentInformation[0].photo_s}`;
  } else D.querySelector("[data-photo-student]").src = `https://homebor.com/assets/emptys/profile-student-empty.png`;

  // ? STUDENT NAME
  if (studentInformation[0].name_s !== "NULL" && studentInformation[0].l_name_s !== "NULL") {
    D.querySelector(
      "[data-name-student]"
    ).textContent = `${studentInformation[0].name_s} ${studentInformation[0].l_name_s}`;
  } else if (studentInformation[0].name_s === "NULL") {
    D.querySelector("[data-name-student]").textContent = `${studentInformation[0].l_name_s}`;
  } else if (studentInformation[0].l_name_s === "NULL") {
    D.querySelector("[data-name-student]").textContent = `${studentInformation[0].name_s}`;
  } else D.querySelector("[data-name-student]").textContent = "Empty";

  // ? STUDENT NATIONALITY
  if (studentInformation[0].nationality !== "NULL") {
    D.querySelector("[data-nationality]").innerHTML = `
      <span class="icon__location"></span>
      ${studentInformation[0].nationality}
    `;
  } else {
    D.querySelector("[data-nationality]").innerHTML = `
      <span class="icon__location"></span>
      Empty
    `;
  }

  // ? STUDENT STATUS
  if (studentInformation[0].status !== "NULL") {
    D.querySelector("[data-status]").innerHTML = `
      <span class="icon__status"></span>
      ${studentInformation[0].status}
    `;
    if (studentInformation[0].status !== "Confirm Registration" && studentInformation[0].status !== "Homestay Found")
      D.querySelector("#booking-edit").disabled = false;
    else {
      D.querySelector("#booking-edit").classList.add("disabled-button");
      D.querySelector("#booking-edit").disabled = true;
      D.querySelector("#booking-edit").dataset.studentSituation = studentInformation[0].status;
    }
  } else {
    D.querySelector("[data-status]").innerHTML = `
      <span class="icon__status"></span>
      Empty
    `;
  }

  // ? STUDENT ABOUT US
  if (studentInformation[0].about_me !== "NULL") {
    D.querySelector("[data-about-us]").textContent = studentInformation[0].about_me;
  } else D.querySelector("[data-about-us]").textContent = "Empty";

  basicInformation();
  accommodationRequest();
  addressInformation();
  professionalInformation();
  flightInformation();
  shareWithInformation();
  accommodation();
  supplementsInformation();
  transportInformation();
  healthInformation();
  emergencyContactInformation();
  attachedFiles();

  if (studentInformation[0].status !== "Confirm Registration") getHouses();
}

// TODO FUNCTION => BASIC INFORMATION
function basicInformation() {
  const $fragment = D.createDocumentFragment();
  basicLabelLabels.forEach((name) => {
    const $clone = D.importNode($templateBasicInformation, true);
    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? EMAIL
    validateInformation($clone, "Email", studentInformation[0].mail_s);

    // ? DESTINATION CITY
    validateInformation($clone, "Destination City", studentInformation[0].destination_c);

    // ? ARRIVE DATE
    validateInformation($clone, "Arrive Date", studentInformation[0].arrival_general);

    // ? DATE OF BIRTH
    validateInformation($clone, "Date of Birth", studentInformation[0].dateBirth);

    // ? GENDER
    validateInformation($clone, "Gender", studentInformation[0].gen_s);

    // ? NUMBER
    validateInformation($clone, "Phone Number", studentInformation[0].num_s);

    // ? ORIGIN LANGUAGE
    validateInformation($clone, "Origin Language", studentInformation[0].lang_s);

    // ? ANOTHER LANGUAGE
    validateInformation($clone, "Anoter Language", studentInformation[0].language_a);

    // ? PASSPORT
    validateInformation($clone, "Passport", studentInformation[0].passport);

    // ? PASSPORT EXPIRATION DATE
    validateInformation($clone, "Passport Expiration Date", studentInformation[0].passExpiration);

    // ? VISA EXPIRATION DATE
    validateInformation($clone, "Visa Expiration Date", studentInformation[0].visaExpiration);

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_basic_information").appendChild($fragment);
}

// TODO FUNCTION => ACCOMMODATION REQUEST
function accommodationRequest() {
  const $fragment = D.createDocumentFragment();

  accommodationRequestLabels.forEach((name) => {
    const $clone = D.importNode($templateAccommodationRequest, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? START DATE
    validateInformation($clone, "Start Date of Stay", studentInformation[0].startDate);

    // ? END DATE
    validateInformation($clone, "End Date of Stay", studentInformation[0].endDate);

    $fragment.appendChild($clone);
  });

  const showWeek = D.createElement("p");
  showWeek.className = "dark font-bold";
  showWeek.textContent = studentInformation[0].timeStudentReservation;

  D.querySelector("#accommodation_request").appendChild($fragment);
  D.querySelector("#accommodation_request").appendChild(showWeek);
}

// TODO FUNCTION => ADDRESS
function addressInformation() {
  const $fragment = D.createDocumentFragment();
  addressLabels.forEach((name) => {
    const $clone = D.importNode($templateAddress, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? COUNTRY
    validateInformation($clone, "Country", studentInformation[0].country);

    // ? CITY
    validateInformation($clone, "City", studentInformation[0].city_s);

    // ? PROVINCE / STATE
    validateInformation($clone, "Province / State", studentInformation[0].state_s);

    // ? POSTAL CODE
    validateInformation($clone, "Postal Code", studentInformation[0].p_code_s);

    // ? ADDRESS
    validateInformation($clone, "Address", studentInformation[0].dir_s);

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_student_address").appendChild($fragment);
}

// TODO FUNCTION => PROFESSIONAL INFORMATION
function professionalInformation() {
  const $fragment = D.createDocumentFragment();

  professionalInformationLabels.forEach((name) => {
    const $clone = D.importNode($templateProfessionalInformation, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? CURRENT ENGLISH LEVEL
    validateInformation($clone, "Current English Level", studentInformation[0].english_l);

    // ? TYPE STUDENT
    validateInformation($clone, "Type Student", studentInformation[0].type_s);

    // ? PROGRAM TO STUDY
    validateInformation($clone, "Program to Study", studentInformation[0].prog_selec);

    // ? SCHEDULE
    validateInformation($clone, "Schedule", studentInformation[0].schedule);

    // ? SCHOOL
    validateInformation($clone, "School Selected", studentInformation[0].schoolName);

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_professional_information").appendChild($fragment);
}

// TODO FUNCTION => FLIGHT INFORMATION
function flightInformation() {
  const $fragment = D.createDocumentFragment();

  flightInformationLabels.forEach((name) => {
    const $clone = D.importNode($templateFlightInformation, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? BOOKING INFORMATION
    validateInformation($clone, "Booking Information", studentInformation[0].n_airline);

    // ? LANDING FLIGHT NUMBER
    validateInformation($clone, "Landing Flight Number", studentInformation[0].n_flight);

    // ? FLIGHT DATE
    validateInformation($clone, "Flight Date", studentInformation[0].flight_date);

    // ? ARRIVAL AT THE HOMESTAY
    validateInformation($clone, "Arrival at the Homestay", studentInformation[0].arrival_homestay);

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_flight_information").appendChild($fragment);
}

// TODO FUNCTION => SHARE WITH
function shareWithInformation() {
  const $fragment = D.createDocumentFragment();

  shareWithLabels.forEach((name) => {
    const $clone = D.importNode($templateShareWith, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? SMOKERS
    validateInformation(
      $clone,
      "Smokers?",
      studentInformation[0].smoker_l[0].toUpperCase() + studentInformation[0].smoker_l.substring(1)
    );

    // ? CHILDREN
    validateInformation(
      $clone,
      "Children?",
      studentInformation[0].children[0].toUpperCase() + studentInformation[0].children.substring(1)
    );

    // ? TEENAGERS
    validateInformation(
      $clone,
      "Teenagers?",
      studentInformation[0].teenagers[0].toUpperCase() + studentInformation[0].teenagers.substring(1)
    );

    // ? PETS
    validateInformation(
      $clone,
      "Pets?",
      studentInformation[0].pets[0].toUpperCase() + studentInformation[0].pets.substring(1)
    );

    $fragment.appendChild($clone);
  });

  D.querySelector("#total_price").textContent =
    parseFloat(studentInformation[0].booking_fee) +
    parseFloat(studentInformation[0].a_price) +
    parseFloat(studentInformation[0].t_price) +
    "$";

  D.querySelector("#parent_share_with").appendChild($fragment);
}

// TODO FUNCTION => ACCOMMODATION
function accommodation() {
  const $fragment = D.createDocumentFragment();

  accommodationLabels.forEach((name) => {
    const $clone = D.importNode($templateAccommodation, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? TYPE OF ACCOMMODATION
    validateInformation($clone, "Type of Accommodation", studentInformation[0].lodging_type);

    // ? MEAL PLAN
    validateInformation($clone, "Meal Plan", studentInformation[0].meal_p);

    // ? BOOKING FEE
    validateInformation($clone, "Booking fee", studentInformation[0].booking_fee + "$");

    // ? TOTAL WECKLY PRICE
    validateInformation($clone, "Total weckly price", studentInformation[0].a_price / 4 + "$");

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_accommodation").appendChild($fragment);
}

// TODO FUNCTION => SUPPLEMENTS
function supplementsInformation() {
  const $fragment = D.createDocumentFragment();

  supplementsLabels.forEach((name) => {
    const $clone = D.importNode($templateSupplements, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? ACCOMMODATION NEAR TO THE SCHOOL
    validateInformation(
      $clone,
      "Accommodation near the school",
      studentInformation[0].a_near[0].toUpperCase() + studentInformation[0].a_near.substring(1)
    );

    // ? URGENT ACCOMMODATION SEARCH
    validateInformation(
      $clone,
      "Urgent accommodation search",
      studentInformation[0].a_urgent[0].toUpperCase() + studentInformation[0].a_urgent.substring(1)
    );

    // ? DO YOU WANT THE SUMMER FEE PLAN
    validateInformation(
      $clone,
      "Do you want the Summer Fee plan",
      studentInformation[0].summer_fee[0].toUpperCase() + studentInformation[0].summer_fee.substring(1)
    );

    // ? ARE YOU UNDER 18
    validateInformation(
      $clone,
      "Are you under 18?",
      studentInformation[0].minor_fee[0].toUpperCase() + studentInformation[0].minor_fee.substring(1)
    );

    // ? GUARDIANSHIP
    validateInformation(
      $clone,
      "Guardianship",
      studentInformation[0].a_guardianship[0].toUpperCase() + studentInformation[0].a_guardianship.substring(1)
    );

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_optional_supplements").appendChild($fragment);
}

// TODO FUNCTION => TRANSPORT
function transportInformation() {
  const $fragment = D.createDocumentFragment();

  transportLabels.forEach((name) => {
    const $clone = D.importNode($templateTransport, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? PICK UP
    validateInformation(
      $clone,
      "Pick up service",
      studentInformation[0].pick_up[0].toUpperCase() + studentInformation[0].pick_up.substring(1)
    );

    // ? DROP OFF
    validateInformation(
      $clone,
      "Drop off service",
      studentInformation[0].drop_off[0].toUpperCase() + studentInformation[0].drop_off.substring(1)
    );

    // ? TRANSPORT PRICE
    validateInformation($clone, "Transport price", studentInformation[0].t_price + "$");

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_transport").appendChild($fragment);
}

// TODO FUNCTION => HEALTH INFORMATION
function healthInformation() {
  const $fragment = D.createDocumentFragment();

  healthInformationLabels.forEach((name) => {
    const $clone = D.importNode($templateHealthInformation, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? DO YOU SMOKE
    validateInformation(
      $clone,
      "Do you smoke?",
      studentInformation[0].smoke_s[0].toUpperCase() + studentInformation[0].smoke_s.substring(1)
    );

    // ? DRINK ALCOHOL
    validateInformation(
      $clone,
      "Do you drink alcohol?",
      studentInformation[0].drinks_alc[0].toUpperCase() + studentInformation[0].drinks_alc.substring(1)
    );

    // ? DRUGS
    validateInformation(
      $clone,
      "Have you used any drugs",
      studentInformation[0].drugs[0].toUpperCase() + studentInformation[0].drugs.substring(1)
    );

    // ? ALLEGY TO ANIMALS
    validateInformation(
      $clone,
      "Any allergy to Animals",
      studentInformation[0].allergy_a[0].toUpperCase() + studentInformation[0].allergy_a.substring(1)
    );

    // ? DISEASE
    validateInformation(
      $clone,
      "Do you suffer from any disease?",
      studentInformation[0].disease[0].toUpperCase() + studentInformation[0].disease.substring(1)
    );

    // ? TREATMENT
    validateInformation(
      $clone,
      "Any treatment or Medication?",
      studentInformation[0].treatment[0].toUpperCase() + studentInformation[0].treatment.substring(1)
    );

    // ? PSYCHOLOGICAL TREATMENT
    validateInformation(
      $clone,
      "Any Psychological Treatment?",
      studentInformation[0].treatment_p[0].toUpperCase() + studentInformation[0].treatment_p.substring(1)
    );

    // ? ALLERGIES
    validateInformation(
      $clone,
      "Any allergies?",
      studentInformation[0].allergies[0].toUpperCase() + studentInformation[0].allergies.substring(1)
    );

    // ? SURGERY
    validateInformation(
      $clone,
      "Any surgery in the last 12 months?",
      studentInformation[0].surgery[0].toUpperCase() + studentInformation[0].surgery.substring(1)
    );

    // ? CURRENT HEALTH
    validateInformation(
      $clone,
      "What is your state if health at the moment",
      studentInformation[0].healt_s[0].toUpperCase() + studentInformation[0].healt_s.substring(1)
    );

    if (
      name === "Do you smoke?" ||
      name === "Do you drink alcohol?" ||
      name === "Have you used any drugs" ||
      name === "Any allergy to Animals"
    )
      $clone.querySelector(".label__input").parentElement.className = "col-5 col-sm-3 d-flex flex-column text-center";

    if (
      name === "Do you suffer from any disease?" ||
      name === "Any treatment or Medication?" ||
      name === "Any Psychological Treatment?"
    )
      $clone.querySelector(".label__input").parentElement.className = "col-5 col-sm-4 d-flex flex-column text-center";

    if (name === "Any allergies?" || name === "Any surgery in the last 12 months?")
      $clone.querySelector(".label__input").parentElement.className = "col-5 col-sm-5 d-flex flex-column text-center";

    if (name === "What is your state if health at the moment")
      $clone.querySelector(".label__input").parentElement.className = "col-8 col-sm-7 d-flex flex-column text-center";

    // studentInformation[0].allergies[0].toUpperCase() + studentInformation[0].allergies.substring(1);)
    // console.log($clone.querySelector(".label__input").parentElement);

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_health_information").appendChild($fragment);
}

// TODO FUNCTION => EMERGENCY CONTACT
function emergencyContactInformation() {
  const $fragment = D.createDocumentFragment();

  emergencyInformationLabels.forEach((name) => {
    const $clone = D.importNode($templateEmergencyContact, true);

    $clone.querySelector(".label__input").textContent = name;
    $clone.querySelector(".label__input").dataset.name = name;

    // ? CONTACT NAME
    validateInformation(
      $clone,
      "Name",
      studentInformation[0].cont_name[0].toUpperCase() + studentInformation[0].cont_name.substring(1)
    );

    // ? CONTACT LAST NAME
    validateInformation(
      $clone,
      "Last Name",
      studentInformation[0].cont_lname[0].toUpperCase() + studentInformation[0].cont_lname.substring(1)
    );

    // ? CONTACT PHONE NUMBER
    validateInformation(
      $clone,
      "Phone Number",
      studentInformation[0].num_conts[0].toUpperCase() + studentInformation[0].num_conts.substring(1)
    );

    // ? CONTACT RELATIONSHIP
    validateInformation(
      $clone,
      "Relationship",
      studentInformation[0].relationship[0].toUpperCase() + studentInformation[0].relationship.substring(1)
    );

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_emergency_contact").appendChild($fragment);
}

// TODO FUNCTION => VALIDATE INPUTS
function validateInformation($clone, name, value) {
  if ($clone.querySelector(".label__input").dataset.name === name) {
    if (value === "NULL") $clone.querySelector(".inputs").textContent = "Empty";
    else $clone.querySelector(".inputs").textContent = value;
  }
}

// TODO FUNCTION => ATTACHED FILES
function attachedFiles() {
  const $fragment = D.createDocumentFragment();

  attachedFilesLabels.forEach((name) => {
    const $clone = D.importNode($templateAttachedFiles, true);

    $clone.querySelector(".label__input").textContent = name;

    // ? PASSPORT PHOTO
    if (name === "Passport photo") {
      if (studentInformation[0].pass_photo === "NULL") {
        $clone.querySelector("img").classList.remove("d-none");
        $clone.querySelector("img").src = "https://homebor.com/assets/emptys/profile-student-empty.png";
      } else {
        const fileExtension = getFileExtension1(studentInformation[0].pass_photo);

        if (fileExtension === "pdf") {
          $clone.querySelector("iframe").classList.remove("d-none");
          $clone.querySelector("iframe").src = `https://homebor.com/${studentInformation[0].pass_photo}`;
        } else {
          $clone.querySelector("img").classList.remove("d-none");
          $clone.querySelector("img").src = `https://homebor.com/${studentInformation[0].pass_photo}`;
        }
      }
    }

    // ? VISA PHOTO
    if (name === "Visa photo") {
      if (studentInformation[0].visa === "NULL") {
        $clone.querySelector("img").classList.remove("d-none");
        $clone.querySelector("img").src = "https://homebor.com/assets/emptys/calendar-empty.png";
      } else {
        const fileExtension = getFileExtension1(studentInformation[0].visa);

        if (fileExtension === "pdf") {
          $clone.querySelector("iframe").classList.remove("d-none");
          $clone.querySelector("iframe").src = `https://homebor.com/${studentInformation[0].visa}`;
        } else {
          $clone.querySelector("img").classList.remove("d-none");
          $clone.querySelector("img").src = `https://homebor.com/${studentInformation[0].visa}`;
        }
      }
    }

    if (name === "Flight Ticket Image") {
      if (studentInformation[0].flight_image === "NULL") {
        $clone.querySelector("img").classList.remove("d-none");
        $clone.querySelector("img").src = "https://homebor.com/assets/emptys/student-list-empty2.png";
      } else {
        const fileExtension = getFileExtension1(studentInformation[0].flight_image);

        if (fileExtension === "pdf") {
          $clone.querySelector("iframe").classList.remove("d-none");
          $clone.querySelector("iframe").src = `https://homebor.com/${studentInformation[0].flight_image}`;
        } else {
          $clone.querySelector("img").classList.remove("d-none");
          $clone.querySelector("img").src = `https://homebor.com/${studentInformation[0].flight_image}`;
        }
      }
    }

    if (name === "Signature") {
      if (studentInformation[0].signature_s === "NULL") {
        $clone.querySelector("img").classList.remove("d-none");
        $clone.querySelector("img").src = "https://homebor.com/assets/emptys/profile-student-empty.png";
      } else {
        const fileExtension = getFileExtension1(studentInformation[0].signature_s);

        if (fileExtension === "pdf") {
          $clone.querySelector("iframe").classList.remove("d-none");
          $clone.querySelector("iframe").src = `https://homebor.com/${studentInformation[0].signature_s}`;
        } else {
          $clone.querySelector("img").classList.remove("d-none");
          $clone.querySelector("img").src = `https://homebor.com/${studentInformation[0].signature_s}`;
        }
      }
    }

    $fragment.appendChild($clone);
  });

  D.querySelector("#parent_attached_files").appendChild($fragment);
}

// TODO FUNCTION => FILE EXTENSIONS
function getFileExtension1(filename) {
  return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
}

// TODO FUNCTION => IF THE STUDENT HAS AN ACTIVE RESERVE
async function activeReserve() {
  const formActiveReserve = new FormData();
  formActiveReserve.set("student_id", ID);
  formActiveReserve.set("request", "getActiveReserve");
  const DATA = { method: "POST", body: formActiveReserve };

  const jsonActiveReserve = await fetch("../helpers/students/student_information.php", DATA);
  const resultActiveReserve = await jsonActiveReserve.json();

  if (resultActiveReserve !== "No Active") {
    if (
      resultActiveReserve[0].accommodation === null &&
      resultActiveReserve[0].food === null &&
      resultActiveReserve[0].isChange === false &&
      resultActiveReserve[0].room_selected === null
    ) {
    } else {
      D.querySelector(".active_reservation").parentElement.classList.replace("d-none", "d-flex");
      D.querySelector(".house-selected").classList.replace("d-none", "d-flex");

      Object.values(resultActiveReserve).forEach((detail) => {
        D.querySelector("[data-image-house-selected]").src = `../${detail.phome}`;
        D.querySelector("[data-room-selected]").textContent = detail.room_selected;
        D.querySelector("[data-house-name]").textContent = detail.h_name;
        D.querySelector("[data-accommodation-selected]").textContent = detail.accommodation;
        D.querySelector("[data-food-selected]").textContent = detail.food;
        D.querySelector("[data-id-home]").dataset.idHome = detail.id_home;
        if (detail.isChange) D.querySelector("#change-house").classList.add("d-none");

        isChangeHouse = detail.isChange;
      });
    }
  }
}

async function nextReservation() {
  if (isChangeHouse) {
    const formNextHouse = new FormData();
    formNextHouse.set("student_id", ID);
    formNextHouse.set("request", "nextReservation");
    const DATA = { method: "POST", body: formNextHouse };

    const jsonNextHouse = await fetch("../helpers/students/student_information.php", DATA);
    const resultNextHouse = await jsonNextHouse.json();

    console.log(resultNextHouse);

    D.querySelector(".next_reservation").parentElement.classList.replace("d-none", "d-flex");
    D.querySelector(".house-next").classList.replace("d-none", "d-flex");

    Object.values(resultNextHouse).forEach((detail) => {
      D.querySelector("[data-image-house-next]").src = `../${detail.phome}`;
      D.querySelector("[data-room-next]").textContent = detail.room_selected;
      D.querySelector("[data-house-name-next]").textContent = detail.h_name;
      D.querySelector("[data-accommodation-next]").textContent = detail.accommodation;
      D.querySelector("[data-food-next]").textContent = detail.food;
      D.querySelector("[data-id-home-next]").dataset.idHome = detail.id_home;
      // if (detail.isChange) D.querySelector("#change-house").classList.add("d-none");

      // isChangeHouse = detail.isChange;
    });
  } else {
    D.querySelector(".next_reservation").parentElement.classList.replace("d-flex", "d-none");
    D.querySelector(".house-next").classList.replace("d-flex", "d-none");
  }
}

async function changeNextReservation() {
  const formNextReservation = new FormData($formChangeHouse);
  formNextReservation.set("id-student", ID);
  formNextReservation.set("request", "nextReservation");
  const DATA = { method: "POST", body: formNextReservation };

  const jsonNextReservation = await fetch("./get-student-info.php", DATA);
  const resultNextReservation = await jsonNextReservation.json();
}

async function lastReservation() {
  const formLastReservation = new FormData($formChangeHouse);
  formLastReservation.set("student_id", ID);
  formLastReservation.set("request", "lastReservation");
  const DATA = { method: "POST", body: formLastReservation };

  const jsonLastReservation = await fetch("../helpers/students/student_information.php", DATA);
  const resultLastReservation = await jsonLastReservation.json();

  if (resultLastReservation[0].statusStudent === "Finished Stay") {
    D.querySelector(".last_reservation").parentElement.classList.replace("d-none", "d-flex");
    D.querySelector(".house-last").classList.replace("d-none", "d-flex");

    Object.values(resultLastReservation).forEach((detail) => {
      D.querySelector("[data-image-house-last]").src = `../${detail.phome}`;
      D.querySelector("[data-room-last]").textContent = detail.room_selected;
      D.querySelector("[data-house-name-last]").textContent = detail.h_name;
      D.querySelector("[data-accommodation-last]").textContent = detail.accommodation;
      D.querySelector("[data-food-last]").textContent = detail.food;
      D.querySelector("[data-id-home-last]").dataset.idHome = detail.id_home;
      // if (detail.isChange) D.querySelector("#change-house").classList.add("d-none");

      // isChangeHouse = detail.isChange;
    });
  } else {
    D.querySelector(".last_reservation").parentElement.classList.replace("d-flex", "d-none");
    D.querySelector(".house-last").classList.replace("d-flex", "d-none");
  }
}

// TODO FUNCTION => GET HOUSES REQUESTED
async function housesAssigned() {
  const formHousesAssigned = new FormData();
  formHousesAssigned.set("student_id", ID);
  formHousesAssigned.set("request", "getHousesAssigned");
  const DATA = { method: "POST", body: formHousesAssigned };

  const jsonHousesAssigned = await fetch("../helpers/students/student_information.php", DATA);
  const resultHousesAssigned = await jsonHousesAssigned.json();

  if (
    resultHousesAssigned[1] !== "Homestay Found" &&
    resultHousesAssigned[1] !== "Confirm Registration" &&
    resultHousesAssigned[1] !== "Finished Stay"
  ) {
    const $fragment = D.createDocumentFragment();
    D.querySelector("#parent_houses_requested").classList.replace("d-none", "d-flex");
    D.querySelector(".house_catalog").parentElement.classList.replace("d-none", "d-flex");

    if (resultHousesAssigned[0] !== null) {
      resultHousesAssigned[0].forEach((house) => {
        const $clone = D.importNode($templateHousesAssigned, true);

        $clone.querySelector("[data-image-house-requested]").src = `../${house.phome}`;
        $clone.querySelector("[data-house-name-requested]").textContent = house.h_name;
        // $clone.querySelector("[data-accommodation-requested]").textContent = house.accommodation;
        // $clone.querySelector("[data-food-requested]").textContent = house.food;
        $clone.querySelector("[data-id-home]").dataset.idHome = house.id_home;

        if (house.isNotificationExist) {
          $clone.querySelector("[data-requested]").classList.remove("d-none");
          $clone.querySelector("[data-requested]").textContent = "Request Send";
          // $clone.querySelector("[data-room-requested]").textContent = `Room ${house.roomSelected}`;
        }

        $fragment.appendChild($clone);
      });
    } else {
      D.querySelector("#parent_houses_requested").classList.replace("d-flex", "d-none");
      D.querySelector(".house_catalog").parentElement.classList.replace("d-flex", "d-none");
    }

    D.querySelector("#parent_houses_requested").appendChild($fragment);
  }
}

// * CHANGE HOUSE
async function getHouses() {
  const formData = new FormData();
  formData.set("request", "getHouses");
  const DATA = { method: "POST", body: formData };

  const jsonFormHouses = await fetch("./get-student-info", DATA);
  const resultFormHouses = await jsonFormHouses.json();
  const $fragment = D.createDocumentFragment();

  resultFormHouses.forEach((house) => {
    const $clone = D.importNode($templateHouseList, true);
    let $contentRoom = false;

    TYPE_ROOM.set("Room 1", house.h_room.type1);
    TYPE_ROOM.set("Room 2", house.h_room.type2);
    TYPE_ROOM.set("Room 3", house.h_room.type3);
    TYPE_ROOM.set("Room 4", house.h_room.type4);
    TYPE_ROOM.set("Room 5", house.h_room.type5);
    TYPE_ROOM.set("Room 6", house.h_room.type6);
    TYPE_ROOM.set("Room 7", house.h_room.type7);
    TYPE_ROOM.set("Room 8", house.h_room.type8);

    if (house.h_photo === "NULL") $clone.querySelector("#img-house").src = "../assets/emptys/frontage-empty.png";
    else $clone.querySelector("#img-house").src = "../" + house.h_photo;

    if (house.h_status === "Avalible") $clone.querySelector("#status").textContent = "Available";
    else $clone.querySelector("#status").textContent = house.h_status;
    $clone.querySelector(".name_house").textContent = house.h_name;

    TYPE_ROOM.forEach((roomHouse, i) => {
      if (roomHouse === "NULL") return;

      const $houseBeds = $clone.querySelector(".house-beds");
      const numberRoom = i.split(" ");
      const $divRoom = D.createElement("div");
      const $divBeds = D.createElement("div");
      /* const $divBeds; */
      $divRoom.className = "col-sm-6 col-6 text-center font-weight-bold";
      $divRoom.textContent = i;
      $divRoom.dataset.room = numberRoom[1];
      $divBeds.className = "d-flex row justify-content-around";

      for (let i = 1; i <= numberRoom[1]; i++) {
        if ($divRoom.dataset.room == i) {
          if (
            house.h_room[`bed${i}`] !== "NULL" ||
            house.h_room[`bed${i}_2`] !== "NULL" ||
            house.h_room[`bed${i}_3`] !== "NULL"
          ) {
            let nameBed = "";
            const $bedsHouse = D.querySelector("#template-beds").content.cloneNode(true);
            const $bed1 = $bedsHouse.querySelector("#bed-i");
            const $bed2 = $bedsHouse.querySelector("#bed-ii");
            const $bed3 = $bedsHouse.querySelector("#bed-iii");
            if (house.h_room[`bed${i}`] !== "NULL") {
              $bed1.classList.remove("d-none");
              if (`${house.h_room[`bed${i}`]}` === "Bunk-bed") nameBed = "Bunk";
              else nameBed = `${house.h_room[`bed${i}`]}`;
              $bedsHouse.querySelector("#bed-i #p-bed").textContent = nameBed;
              $bedsHouse.querySelector("#bed-i #p-bed").id = `p-bed${i}`;
              $bedsHouse.querySelector("#bed-i input").id = `${house.h_id}-bed${i}`;
              $bedsHouse.querySelector("#bed-i label").setAttribute("for", `${house.h_id}-bed${i}`);
              $bedsHouse.querySelector("#bed-i").classList.remove("d-none");
              if (house.h_room[`date${i}`] === "Available") {
                $bedsHouse.querySelector("#bed-i p").classList.add("type-bed-available");
              }
              if (house.h_room[`date${i}`] === "Occupied") {
                $bedsHouse.querySelector("#bed-i p").classList.add("type-bed-occupied");
                if (W.innerWidth >= "575") {
                  $bedsHouse.querySelector("#bed-i p").style.marginLeft = "16px";
                } else {
                  $bedsHouse.querySelector("#bed-i p").style.marginLeft = "32px";
                }
                $bedsHouse.querySelector("#bed-i svg").classList.add("d-none");
              }
              $divBeds.appendChild($bed1);
            }

            if (house.h_room[`bed${i}_2`] !== "NULL") {
              $bed2.classList.remove("d-none");
              if (`${house.h_room[`bed${i}_2`]}` === "Bunk-bed") nameBed = "Bunk";
              else nameBed = `${house.h_room[`bed${i}_2`]}`;
              $bedsHouse.querySelector("#bed-ii #p-bed").textContent = nameBed;
              $bedsHouse.querySelector("#bed-ii #p-bed").id = `p-bed${i}_2`;
              $bedsHouse.querySelector("#bed-ii input").id = `${house.h_id}-bed${i}_2`;
              $bedsHouse.querySelector("#bed-ii label").setAttribute("for", `${house.h_id}-bed${i}_2`);
              $bedsHouse.querySelector("#bed-ii").classList.remove("d-none");
              if (house.h_room[`date${i}_2`] === "Available") {
                $bedsHouse.querySelector("#bed-ii p").classList.add("type-bed-available");
              }
              if (house.h_room[`date${i}_2`] === "Occupied") {
                $bedsHouse.querySelector("#bed-ii p").classList.add("type-bed-occupied");
                if (W.innerWidth >= "575") {
                  $bedsHouse.querySelector("#bed-ii p").style.marginLeft = "16px";
                } else {
                  $bedsHouse.querySelector("#bed-ii p").style.marginLeft = "32px";
                }
                $bedsHouse.querySelector("#bed-ii svg").classList.add("d-none");
              }
              $divBeds.appendChild($bed2);
            }

            if (house.h_room[`bed${i}_3`] !== "NULL") {
              $bed3.classList.remove("d-none");
              if (`${house.h_room[`bed${i}_3`]}` === "Bunk-bed") nameBed = "Bunk";
              else nameBed = `${house.h_room[`bed${i}_3`]}`;
              $bedsHouse.querySelector("#bed-iii #p-bed").textContent = nameBed;
              $bedsHouse.querySelector("#bed-iii #p-bed").id = `p-bed${i}_3`;
              $bedsHouse.querySelector("#bed-iii input").id = `${house.h_id}-bed${i}_3`;
              $bedsHouse.querySelector("#bed-iii label").setAttribute("for", `${house.h_id}-bed${i}_3`);
              $bedsHouse.querySelector("#bed-iii").classList.remove("d-none");
              if (house.h_room[`date${i}_3`] === "Available") {
                $bedsHouse.querySelector("#bed-iii p").classList.add("type-bed-available");
              }
              if (house.h_room[`date${i}_3`] === "Occupied") {
                $bedsHouse.querySelector("#bed-iii p").classList.add("type-bed-occupied");
                if (W.innerWidth >= "575") {
                  $bedsHouse.querySelector("#bed-iii p").style.marginLeft = "14px";
                } else {
                  $bedsHouse.querySelector("#bed-iii p").style.marginLeft = "32px";
                }
                $bedsHouse.querySelector("#bed-iii svg").classList.add("d-none");
              }
              $divBeds.appendChild($bed3);
            }
          }
        }
      }
      $divRoom.appendChild($divBeds);
      $houseBeds.appendChild($divRoom);
      $contentRoom = true;
    });

    if ($contentRoom == false) {
      const $houseBeds = $clone.querySelector(".house-beds");
      const $imgEmpty = D.createElement("img");
      const $pText = D.createElement("p");
      $houseBeds.className = "d-block mx-auto text-center w-75";
      $imgEmpty.className = "img-empty";
      $imgEmpty.src = "../assets/emptys/room-empty.png";
      $pText.className = "text-not-room";
      $pText.textContent = "This house are not have rooms";

      $houseBeds.appendChild($imgEmpty);
      $houseBeds.appendChild($pText);
    }

    $fragment.appendChild($clone);
  });

  D.querySelector("#list__student").appendChild($fragment);
  ALL_LIST = D.querySelectorAll(".card-house");
}

function checkboxCheck(inputCheck, bedsContainer) {
  console.log(ALL_LIST);
  for (let i = 0; i < ALL_LIST.length; i++) {
    const $input = ALL_LIST[i].querySelectorAll("input");
    const idHouse = inputCheck.id.split("-");

    $input.forEach((input) => {
      if (inputCheck.id === input.id) {
        if (input.parentElement.querySelector("label > p.type-bed-occupied") === null)
          $buttonSubmitChange.disabled = false;
        input.checked = true;
      } else input.checked = false;
    });

    D.querySelector("#id__house-selected").value = idHouse[0];
    D.querySelector("#room-selected").value = idHouse[1];
  }
}

function validateChangeReservation() {
  if (D.querySelector("#reason-change-reserve").value === "" || D.querySelector("#date-change-reserve").value === "") {
    D.querySelector(".ul__list-students").scroll(0, 0);

    const $reasonChange = D.querySelector("#reason-change-reserve");
    const $dateChange = D.querySelector("#date-change-reserve");
    if ($reasonChange.value === "") {
      $reasonChange.setAttribute("placeholder", "Please type the Reason");
      $reasonChange.classList.add("input-error");
    }
    if ($dateChange.value === "") {
      $dateChange.setAttribute("placeholder", "Please type the Date");
      $dateChange.classList.add("input-error");
    }
  } else {
    // if ($formChangeHouse.querySelector("#id_homeDeleteNoti").value === "") sendReservation();
    // else changeNextReservation();

    sendReservation();
  }
}

async function sendReservation() {
  const formChangeReserve = new FormData($formChangeHouse);
  formChangeReserve.set("id-student", ID);
  formChangeReserve.set("request", "changeReserve");
  const DATA = { method: "POST", body: formChangeReserve };

  const jsonChangeReserve = await fetch("./get-student-info.php", DATA);
  const resultChangeReserve = await jsonChangeReserve.json();

  console.log(resultChangeReserve);

  if (resultChangeReserve === "Error") {
    setTimeout(() => {
      $("#house_much").fadeIn("slow");

      setTimeout(() => {
        $("#house_much").fadeOut("slow");
      }, 5000);
    }, 100);

    $("#close").on("click", function close() {
      event.preventDefault();
      $("#house_much").fadeOut("slow");
    });
    document.getElementById("contentp").innerHTML = "There was an Error, please try again.";
    document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
    document.getElementById("contentp").style.marginTop = "auto";
    document.getElementById("contentp").style.marginBottom = "auto";
    document.getElementById("contentp").style.color = "#000";
  } else if (resultChangeReserve === "Success") W.top.location = "directory_students";
}

// ! EVENTS

D.addEventListener("DOMContentLoaded", async () => {
  await getStudentInformation();
  await renderBasicInformation();
  await activeReserve();
  housesAssigned();
  nextReservation();
  lastReservation();
  $buttonSubmitChange.disabled = true;
});

D.addEventListener("click", (e) => {
  if (e.target.id === "button-edit") W.top.location = `student_edit?art_id=${ID}`;
  if (e.target.id === "booking-edit") W.top.location = `edit_assigment?art_id=${ID}`;
  if (e.target.matches(".icon__exit")) W.top.location = `detail?art_id=${e.target.dataset.idHome}`;
  if (e.target.matches("#icon_download")) W.top.location = `../agents/vouches/studentinfo?art_id=${ID}`;

  if (e.target.matches("#change-house") || e.target.matches("#next-house")) {
    D.querySelector("#id_homeBefore").value = e.target.parentElement.querySelector(".icon__exit").dataset.idHome;
    // * BUTTON => CHANGE HOUSE
    D.body.style.overflow = "hidden";
    D.querySelector("#modal__reservation").classList.replace("d-none", "d-flex");
  }
  if (e.target.matches(".checkbox__filter")) checkboxCheck(e.target, e.target.parentElement.parentElement);
  if (e.target.matches("#btn__submit-reservation")) {
    e.preventDefault();
    validateChangeReservation();
  }
  if (e.target.matches("#btn__close-modal")) {
    D.body.style.overflowY = "scroll";
    D.querySelector("#modal__reservation").classList.replace("d-flex", "d-none");
  }
});

D.addEventListener("mouseover", (e) => {
  if (e.target.matches(".button__info")) D.querySelector("#mostrar").classList.remove("d-none");
  else D.querySelector("#mostrar").classList.add("d-none");
  if (
    e.target.matches("#booking-edit") &&
    e.target.disabled === true &&
    e.target.dataset.studentSituation === "Confirm Registration"
  )
    D.querySelector("#warning").classList.remove("d-none");
  else D.querySelector("#warning").classList.add("d-none");
});

D.addEventListener("touchstart", (e) => {
  if (e.target.matches(".button__info")) D.querySelector("#mostrar").classList.remove("d-none");
  else D.querySelector("#mostrar").classList.add("d-none");
  if (
    e.target.matches("#booking-edit") &&
    e.target.disabled === true &&
    e.target.dataset.studentSituation === "Confirm Registration"
  )
    D.querySelector("#warning").classList.remove("d-none");
  else D.querySelector("#warning").classList.add("d-none");
});

// ! CHANGE HOUSE
D.querySelector(".ul__list-students").addEventListener("scroll", (e) => {
  if (W.innerWidth >= "767") {
    if (D.querySelector(".ul__list-students").scrollTop != 0) {
      D.querySelector(".change-details").style.height = "0%";
      D.querySelector("#list__student").style.height = "90%";
      D.querySelector(".date-change-container").classList.add("d-none");
      D.querySelector(".reason-date-change label").classList.add("d-none");
      D.querySelector(".reason-date-change textarea").classList.add("d-none");
      /* D.querySelector(".change-details .date-change-container").classList.replace("p-5", "p-0");
    D.querySelector(".change-details .reason-date-change").classList.replace("p-5", "p-0"); */
      /* D.querySelector(".change-details").style.borderBottom = "none"; */
      /* D.querySelector("#list__student").classList.replace("list_houses-scroll-0", "list_houses-scroll-1"); */
    } else {
      D.querySelector(".change-details").style.height = "37%";
      D.querySelector("#list__student").style.height = "53%";
      D.querySelector(".date-change-container").classList.remove("d-none");
      D.querySelector(".reason-date-change label").classList.remove("d-none");
      D.querySelector(".reason-date-change textarea").classList.remove("d-none");
      /* D.querySelector(".change-details *").classList.replace("p-0", "p-5"); */
      /* D.querySelector(".change-details").style.height = "auto"; */
      /* D.querySelector(".change-details").style.borderBottom = "1px solid #e7e7e7";
    D.querySelector("#list__student").classList.replace("list_houses-scroll-1", "list_houses-scroll-0");   */
    }
  }
});
