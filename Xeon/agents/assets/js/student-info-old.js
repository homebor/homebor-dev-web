//! VARIABLES

const D = document;
const W = window;

const $templateHouseList = D.querySelector("#houses-list").content.cloneNode(true);
/* const $templateBeds = D.querySelector("#template-beds").content.cloneNode(true); */
const rooms = new Map();
const beds = new Map();
const TYPE_ROOM = new Map();
const $formChangeHouse = D.querySelector("#form__reservation");
const $buttonSubmitChange = D.querySelector("#btn__submit-reservation");

console.log(D.querySelector("#date-change-reserve").value, D.querySelector("#reason-change-reserve").value);

//! FUNCTIONS

class profileStudents {
  constructor() {
    this.$houseBeforeReserve = D.querySelector("#id__house-before").value;
    this.$allList = undefined;
  }

  async getHouses() {
    const formData = new FormData();
    formData.set("request", "getHouses");
    const DATA = { method: "POST", body: formData };

    const jsonFormHouses = await fetch("./get-student-info", DATA);
    const resultFormHouses = await jsonFormHouses.json();
    const $fragment = D.createDocumentFragment();

    resultFormHouses.forEach((house) => {
      const $clone = D.importNode($templateHouseList, true);
      let $contentRoom = false;

      TYPE_ROOM.set("Room 1", house.h_room.type1);
      TYPE_ROOM.set("Room 2", house.h_room.type2);
      TYPE_ROOM.set("Room 3", house.h_room.type3);
      TYPE_ROOM.set("Room 4", house.h_room.type4);
      TYPE_ROOM.set("Room 5", house.h_room.type5);
      TYPE_ROOM.set("Room 6", house.h_room.type6);
      TYPE_ROOM.set("Room 7", house.h_room.type7);
      TYPE_ROOM.set("Room 8", house.h_room.type8);

      if (house.h_photo === "NULL") $clone.querySelector("#img-house").src = "../assets/emptys/frontage-empty.png";
      else $clone.querySelector("#img-house").src = "../" + house.h_photo;

      $clone.querySelector("#status").textContent = house.h_status;
      $clone.querySelector(".name_house").textContent = house.h_name;

      TYPE_ROOM.forEach((roomHouse, i) => {
        if (roomHouse === "NULL") return;

        const $houseBeds = $clone.querySelector(".house-beds");
        const numberRoom = i.split(" ");
        const $divRoom = D.createElement("div");
        const $divBeds = D.createElement("div");
        /* const $divBeds; */
        $divRoom.className = "col-sm-6 col-6 text-center font-weight-bold";
        $divRoom.textContent = i;
        $divRoom.dataset.room = numberRoom[1];
        $divBeds.className = "d-flex row justify-content-around";

        for (let i = 1; i <= numberRoom[1]; i++) {
          if ($divRoom.dataset.room == i) {
            if (
              house.h_room[`bed${i}`] !== "NULL" ||
              house.h_room[`bed${i}_2`] !== "NULL" ||
              house.h_room[`bed${i}_3`] !== "NULL"
            ) {
              let nameBed = "";
              const $bedsHouse = D.querySelector("#template-beds").content.cloneNode(true);
              const $bed1 = $bedsHouse.querySelector("#bed-i");
              const $bed2 = $bedsHouse.querySelector("#bed-ii");
              const $bed3 = $bedsHouse.querySelector("#bed-iii");
              if (house.h_room[`bed${i}`] !== "NULL") {
                $bed1.classList.remove("d-none");
                if (`${house.h_room[`bed${i}`]}` === "Bunk-bed") nameBed = "Bunk";
                else nameBed = `${house.h_room[`bed${i}`]}`;
                $bedsHouse.querySelector("#bed-i #p-bed").textContent = nameBed;
                $bedsHouse.querySelector("#bed-i #p-bed").id = `p-bed${i}`;
                $bedsHouse.querySelector("#bed-i input").id = `${house.h_id}-bed${i}`;
                $bedsHouse.querySelector("#bed-i label").setAttribute("for", `${house.h_id}-bed${i}`);
                $bedsHouse.querySelector("#bed-i").classList.remove("d-none");
                if (house.h_room[`date${i}`] === "Available") {
                  $bedsHouse.querySelector("#bed-i p").classList.add("type-bed-available");
                }
                if (house.h_room[`date${i}`] === "Occupied") {
                  $bedsHouse.querySelector("#bed-i p").classList.add("type-bed-occupied");
                  if (W.innerWidth >= "575") {
                    $bedsHouse.querySelector("#bed-i p").style.marginLeft = "16px";
                  } else {
                    $bedsHouse.querySelector("#bed-i p").style.marginLeft = "32px";
                  }
                  $bedsHouse.querySelector("#bed-i svg").classList.add("d-none");
                }
                $divBeds.appendChild($bed1);
              }

              if (house.h_room[`bed${i}_2`] !== "NULL") {
                $bed2.classList.remove("d-none");
                if (`${house.h_room[`bed${i}_2`]}` === "Bunk-bed") nameBed = "Bunk";
                else nameBed = `${house.h_room[`bed${i}_2`]}`;
                $bedsHouse.querySelector("#bed-ii #p-bed").textContent = nameBed;
                $bedsHouse.querySelector("#bed-ii #p-bed").id = `p-bed${i}_2`;
                $bedsHouse.querySelector("#bed-ii input").id = `${house.h_id}-bed${i}_2`;
                $bedsHouse.querySelector("#bed-ii label").setAttribute("for", `${house.h_id}-bed${i}_2`);
                $bedsHouse.querySelector("#bed-ii").classList.remove("d-none");
                if (house.h_room[`date${i}_2`] === "Available") {
                  $bedsHouse.querySelector("#bed-ii p").classList.add("type-bed-available");
                }
                if (house.h_room[`date${i}_2`] === "Occupied") {
                  $bedsHouse.querySelector("#bed-ii p").classList.add("type-bed-occupied");
                  if (W.innerWidth >= "575") {
                    $bedsHouse.querySelector("#bed-ii p").style.marginLeft = "16px";
                  } else {
                    $bedsHouse.querySelector("#bed-ii p").style.marginLeft = "32px";
                  }
                  $bedsHouse.querySelector("#bed-ii svg").classList.add("d-none");
                }
                $divBeds.appendChild($bed2);
              }

              if (house.h_room[`bed${i}_3`] !== "NULL") {
                $bed3.classList.remove("d-none");
                if (`${house.h_room[`bed${i}_3`]}` === "Bunk-bed") nameBed = "Bunk";
                else nameBed = `${house.h_room[`bed${i}_3`]}`;
                $bedsHouse.querySelector("#bed-iii #p-bed").textContent = nameBed;
                $bedsHouse.querySelector("#bed-iii #p-bed").id = `p-bed${i}_3`;
                $bedsHouse.querySelector("#bed-iii input").id = `${house.h_id}-bed${i}_3`;
                $bedsHouse.querySelector("#bed-iii label").setAttribute("for", `${house.h_id}-bed${i}_3`);
                $bedsHouse.querySelector("#bed-iii").classList.remove("d-none");
                if (house.h_room[`date${i}_3`] === "Available") {
                  $bedsHouse.querySelector("#bed-iii p").classList.add("type-bed-available");
                }
                if (house.h_room[`date${i}_3`] === "Occupied") {
                  $bedsHouse.querySelector("#bed-iii p").classList.add("type-bed-occupied");
                  if (W.innerWidth >= "575") {
                    $bedsHouse.querySelector("#bed-iii p").style.marginLeft = "14px";
                  } else {
                    $bedsHouse.querySelector("#bed-iii p").style.marginLeft = "32px";
                  }
                  $bedsHouse.querySelector("#bed-iii svg").classList.add("d-none");
                }
                $divBeds.appendChild($bed3);
              }
            }
          }
        }
        $divRoom.appendChild($divBeds);
        $houseBeds.appendChild($divRoom);
        $contentRoom = true;
      });

      if ($contentRoom == false) {
        const $houseBeds = $clone.querySelector(".house-beds");
        const $imgEmpty = D.createElement("img");
        const $pText = D.createElement("p");
        $houseBeds.className = "d-block mx-auto text-center w-75";
        $imgEmpty.className = "img-empty";
        $imgEmpty.src = "../assets/emptys/room-empty.png";
        $pText.className = "text-not-room";
        $pText.textContent = "This house are not have rooms";

        $houseBeds.appendChild($imgEmpty);
        $houseBeds.appendChild($pText);
      }

      $fragment.appendChild($clone);
    });

    D.querySelector("#list__student").appendChild($fragment);
    this.$allList = D.querySelectorAll(".card-house");
  }

  houseRooms(roomsHouse) {
    if (roomsHouse.type1 !== "NULL") rooms.set("room1", roomsHouse.type1);
    if (roomsHouse.type2 !== "NULL") rooms.set("room2", roomsHouse.type2);
    if (roomsHouse.type3 !== "NULL") rooms.set("room3", roomsHouse.type3);
    if (roomsHouse.type4 !== "NULL") rooms.set("room4", roomsHouse.type4);
    if (roomsHouse.type5 !== "NULL") rooms.set("room5", roomsHouse.type5);
    if (roomsHouse.type6 !== "NULL") rooms.set("room6", roomsHouse.type6);
    if (roomsHouse.type7 !== "NULL") rooms.set("room7", roomsHouse.type7);
    if (roomsHouse.type8 !== "NULL") rooms.set("room8", roomsHouse.type8);
  }

  checkboxCheck(inputCheck, bedsContainer) {
    for (let i = 0; i < this.$allList.length; i++) {
      const $input = this.$allList[i].querySelectorAll("input");
      const idHouse = inputCheck.id.split("-");

      $input.forEach((input) => {
        if (inputCheck.id === input.id) {
          $buttonSubmitChange.disabled = false;
          input.checked = true;
        } else input.checked = false;
      });

      D.querySelector("#id__house-selected").value = idHouse[0];
      D.querySelector("#room-selected").value = idHouse[1];
    }
  }

  async sendReservation() {
    if (
      D.querySelector("#reason-change-reserve").value === "" ||
      D.querySelector("#date-change-reserve").value === ""
    ) {
      D.querySelector(".ul__list-students").scroll(0, 0);

      const $reasonChange = D.querySelector("#reason-change-reserve");
      const $dateChange = D.querySelector("#date-change-reserve");
      if ($reasonChange.value === "") {
        $reasonChange.setAttribute("placeholder", "Please type the Reason");
        $reasonChange.classList.add("input-error");
      }
      if ($dateChange.value === "") {
        $dateChange.setAttribute("placeholder", "Please type the Date");
        $dateChange.classList.add("input-error");
      }
    } else {
      const formChangeReserve = new FormData($formChangeHouse);
      formChangeReserve.set("request", "changeReserve");
      const DATA = { method: "POST", body: formChangeReserve };

      const jsonChangeReserve = await fetch("./get-student-info.php", DATA);
      const resultChangeReserve = await jsonChangeReserve.json();

      console.log(resultChangeReserve);

      if (resultChangeReserve === "Error") {
        setTimeout(() => {
          $("#house_much").fadeIn("slow");

          setTimeout(() => {
            $("#house_much").fadeOut("slow");
          }, 5000);
        }, 100);

        $("#close").on("click", function close() {
          event.preventDefault();
          $("#house_much").fadeOut("slow");
        });
        document.getElementById("contentp").innerHTML = "There was an Error, please try again.";
        document.getElementById("house_much").style["boxShadow"] = "0px 0px 11px -3px rgb(179, 40, 40)";
        document.getElementById("contentp").style.marginTop = "auto";
        document.getElementById("contentp").style.marginBottom = "auto";
        document.getElementById("contentp").style.color = "#000";
      } else if (resultChangeReserve === "Success") W.top.location = "directory_students";
    }
  }

  searchHouse(textSearch) {
    const $housesNames = D.querySelectorAll(".name_house");

    $housesNames.forEach((house) => {
      house.parentElement.parentElement.parentElement.classList.replace("d-flex", "d-none");
      if (house.textContent.toLowerCase().includes(textSearch.toLowerCase())) {
        house.parentElement.parentElement.parentElement.classList.replace("d-none", "d-flex");
      }
    });
  }

  zoomfiles(btnZoom) {
    D.querySelector("#show-file").innerHTML = "";
    D.querySelectorAll("[data-zoom]").forEach((file) => {
      if (btnZoom.dataset.zoomFile === file.dataset.zoom) {
        D.querySelector("#modal-zoom").classList.remove("d-none");
        const extensions = /(.pdf)$/i;
        if (extensions.exec(file.src)) {
          const $iframe = D.createElement("iframe");
          $iframe.className = "view_file";
          $iframe.src = file.src;
          D.querySelector("#show-file").parentElement.classList.remove("container__img");
          D.querySelector("#show-file").parentElement.classList.add("container__iframe");
          D.querySelector("#show-file").appendChild($iframe);
        } else {
          const $img = D.createElement("img");
          $img.className = "view_file";
          $img.src = file.src;
          D.querySelector("#show-file").parentElement.classList.remove("container__iframe");
          D.querySelector("#show-file").parentElement.classList.add("container__img");
          D.querySelector("#show-file").appendChild($img);
        }
      }
    });
  }

  async activateStudent(btn) {
    const $id_student = D.querySelector("#id_student").value;
    const formActivateStudent = new FormData();
    formActivateStudent.set("student_id", $id_student);
    formActivateStudent.set("action_btn", btn.dataset.action);
    const DATA = { method: "POST", body: formActivateStudent };

    const jsonActivateStudent = await fetch("../helpers/agents/activate_student.php", DATA);
    const resultActivateStudent = await jsonActivateStudent.json();

    if (resultActivateStudent === "activated") W.location.reload();
    else if (resultActivateStudent === "deleted") W.top.location = "directory_disabled";
    else if (resultActivateStudent === "no agency") alert("This student does not belong to your Agency");
    else alert("There was a problem please try again");
  }
}

//! EVENTS

const INSTANCE = new profileStudents();

D.addEventListener("DOMContentLoaded", (e) => {
  INSTANCE.getHouses();
  $buttonSubmitChange.disabled = true;
});

D.addEventListener("click", (e) => {
  if (e.target.matches("#change-house")) {
    D.body.style.overflow = "hidden";
    D.querySelector("#modal__reservation").classList.replace("d-none", "d-flex");
  }
  if (e.target.matches("#btn__close-modal")) {
    D.body.style.overflowY = "scroll";
    D.querySelector("#modal__reservation").classList.replace("d-flex", "d-none");
  }
  if (e.target.matches(".rooms-container")) {
    const $divBeds = e.target.nextSibling;
    if ($divBeds.classList.contains("d-none")) $divBeds.classList.replace("d-none", "d-flex");
    else $divBeds.classList.replace("d-flex", "d-none");
  }
  if (e.target.matches(".checkbox__filter")) INSTANCE.checkboxCheck(e.target, e.target.parentElement.parentElement);
  if (e.target.matches("#btn__submit-reservation")) {
    e.preventDefault();
    INSTANCE.sendReservation();
  }
  // * ZOOM FILE
  if (e.target.matches("[data-zoom-file]")) INSTANCE.zoomfiles(e.target);
  if (e.target.matches(".btn_close")) D.querySelector("#modal-zoom").classList.add("d-none");
  // * ACTIVATE STUDENT
  if (e.target.matches(".btn__activate_student") || e.target.matches(".btn__delete_student"))
    INSTANCE.activateStudent(e.target);
});

D.addEventListener("keyup", (e) => {
  if (e.target.matches("#search__input")) INSTANCE.searchHouse(e.target.value);
});

D.addEventListener("mouseover", (e) => {
  if (e.target.matches("#a-view-profile-home") || e.target.matches("#change-house")) {
    e.target.querySelector(".arrow__view").classList.replace("arrow__view", "arrow__view-active");
  }
});

D.addEventListener("mouseout", (e) => {
  if (e.target.matches("#a-view-profile-home") || e.target.matches("#change-house")) {
    e.target.querySelector(".arrow__view-active").classList.replace("arrow__view-active", "arrow__view");
  }
});

D.querySelector(".ul__list-students").addEventListener("scroll", (e) => {
  if (W.innerWidth >= "767") {
    if (D.querySelector(".ul__list-students").scrollTop != 0) {
      D.querySelector(".change-details").style.height = "0%";
      D.querySelector("#list__student").style.height = "90%";
      D.querySelector(".date-change-container").classList.add("d-none");
      D.querySelector(".reason-date-change label").classList.add("d-none");
      D.querySelector(".reason-date-change textarea").classList.add("d-none");
      /* D.querySelector(".change-details .date-change-container").classList.replace("p-5", "p-0");
    D.querySelector(".change-details .reason-date-change").classList.replace("p-5", "p-0"); */
      /* D.querySelector(".change-details").style.borderBottom = "none"; */
      /* D.querySelector("#list__student").classList.replace("list_houses-scroll-0", "list_houses-scroll-1"); */
    } else {
      D.querySelector(".change-details").style.height = "37%";
      D.querySelector("#list__student").style.height = "53%";
      D.querySelector(".date-change-container").classList.remove("d-none");
      D.querySelector(".reason-date-change label").classList.remove("d-none");
      D.querySelector(".reason-date-change textarea").classList.remove("d-none");
      /* D.querySelector(".change-details *").classList.replace("p-0", "p-5"); */
      /* D.querySelector(".change-details").style.height = "auto"; */
      /* D.querySelector(".change-details").style.borderBottom = "1px solid #e7e7e7";
    D.querySelector("#list__student").classList.replace("list_houses-scroll-1", "list_houses-scroll-0");   */
    }
  }
});
