<?php 
require '../xeon.php';

session_start();
$usuario = $_SESSION['username'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- // ! LINKS CSS -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="assets/css/reports.css">

  <title>Reports</title>
</head>

<body class="pr-0" style="background-image: url('../assets/img/wallpaper.jpg');">

  <?php include 'header.php' ?>

  <main class="main__report-container">
    <div class="banner__provider"></div>
    <div class="reports__container d-flex">
      <article class="container__report-list">
        <!-- // * HEADER COLUMN -->
        <div class="container__header d-flex justify-content-between">
          <h3>Reports</h3>
          <button type="button" class="button__add-report d-flex align-items-center" data-toggle="modal" data-target="#addNewReport"><i
              title="Add New Report"></i></button>
        </div>
        <!-- // * CONTENT COLUMN -->
        <article class="container__search-report d-flex justify-content-center">
          <label for="" class="fa fa-search"></label>
          <input type="text" class="input__search-report" placeholder="Type report name">
        </article>
        <div class="d-flex flex-column" id="report_list">
          <template id="template__report-list">
            <article class="report d-flex justify-content-center align-items-center">
              <img src="../public/stan@hotmail.com/bb1.jpg" alt="">
              <div class="d-flex">
                <div>
                  <h4>Remi House</h4>
                  <p>Report Situation</p>
                </div>
                <div class="d-flex flex-column">
                  <span class="status__report closed">Closed</span>
                  <span class="date__report"> 12:30 p.m</span>
                  <span class="bubble"></span>
                </div>
              </div>
            </article>
            <hr>
          </template>
          <article class="report d-flex justify-content-center align-items-center">
            <img src="../public/stan@hotmail.com/bb1.jpg" alt="">
            <div class="d-flex">
              <div>
                <h4>Remi House</h4>
                <p>Report Situation</p>
              </div>
              <div class="d-flex flex-column">
                <span class="status__report closed">Closed</span>
                <span class="date__report"> 12:30 p.m</span>
                <span class="bubble"></span>
              </div>
            </div>
          </article>
          <hr>
          <article class="report d-flex justify-content-center align-items-center">
            <img src="../public/stan@hotmail.com/bb1.jpg" alt="">
            <div class="d-flex">
              <div>
                <h4>Remi House</h4>
                <p>Report Situation</p>
              </div>
              <div class="d-flex flex-column">
                <span class="status__report closed">Closed</span>
                <span class="date__report"> 12:30 p.m</span>
                <span class="bubble"></span>
              </div>
            </div>
          </article>
          <hr>
          <article class="report d-flex justify-content-center align-items-center">
            <img src="../public/stan@hotmail.com/bb1.jpg" alt="">
            <div class="d-flex">
              <div>
                <h4>Remi House</h4>
                <p>Report Situation</p>
              </div>
              <div class="d-flex flex-column">
                <span class="status__report closed">Closed</span>
                <span class="date__report"> 12:30 p.m</span>
                <span class="bubble"></span>
              </div>
            </div>
          </article>
          <hr>
        </div>
      </article>
      <article class="container__report-content">
        <!-- // * HEADER COLUMN -->
        <div class="container__header d-flex justify-content-between">
          <div class="d-flex justify-content-start align-items-center">
            <img src="../assets/emptys/frontage-empty.png" alt="">
            <h4>Smith house</h4>
          </div>
          <div class=""></div>
        </div>
        <div class="container__report-messages attach">
          <article class="d-flex justify-content-start issue">
            <img src="../public/remi@protonmail.com/hh1.jpg" alt="">
            <p class="message first__message">
              <span class="title__message">George Sanderson</span><br>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero veniam
              consequuntur repudiandae? Dolore quidem temporibus, inventore suscipit molestiae rerum doloremque nobis
              quisquam praesentium explicabo, aliquid, expedita accusamus soluta autem magni.
              <span class="time__message">12:30 p.m <i class="icon_check no-view"></i></span>
            </p>
          </article>
          <article class="d-flex justify-content-end receptor">
            <p class="message first__message">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero veniam
              consequuntur repudiandae? Dolore quidem temporibus, inventore suscipit molestiae rerum doloremque nobis
              quisquam praesentium explicabo, aliquid, expedita accusamus soluta autem magni.
              <span class="time__message">12:30 p.m <i class="icon_check view"></i></span>
            </p>
            <img src="../public/b@gmail.com/frontage-photo-20220921151019000.jpeg" class="image_views"></img>
          </article>
        </div>
        <div class="container__attach_file">
          <article>
            <img src="../public/a@gmail.com/family-picture-20220929004425000.jpeg" alt="" id="preview-imge"
            class="image__attach">
            <span>X</span>
          </article>
        </div>
        <div class="container__footer d-flex align-items-center">
          <input type="file" id="attach_image" hidden>
          <label for="attach_image" class="fa fa-paperclip attach__image"></label>
          <input type="text" class="input__message-report">
        </div>
      </article>
    </div>
  </main>


  <!-- //TODO MODAL =====> ADD NEW REPORT -->
  <div class="modal fade add_new_report pr-0" id="addNewReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="">New Report</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- // ? SECTION STUDENT / TYPE REPORT -->
          <section class="d-flex justify-content-between">
            <article class="mr-3">
              <label for="select_student">Select Student</label>
              <select name="" id="select_student" class="custom-select">
                <option value="NULL" hidden="option" selected>Select Option</option>
              </select>
            </article>
            <article>
              <label for="type_report">Type Report</label>
              <select name="" id="type_report" class="custom-select">
                <option value="NULL" hidden="option" selected>Select Option</option>
                <option value="Report Situation">Report Situation</option>
                <option value="Cancel Reservation">Cancel Reservation</option>
              </select>
            </article>
          </section>
          <section class="d-flex justify-content-center">
            <label for="reason_report">Reason to Report</label>
            <textarea name="" id="reason_report" rows="4" placeholder="Type Reason . . ."></textarea>
          </section>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Send</button>
        </div>
      </div>
    </div>
  </div>

  <script src="../assets/js/jquery-3.3.1.min.js"></script>
  <script src="../assets/js/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/reports.js" type="module"></script>

</body>

</html>