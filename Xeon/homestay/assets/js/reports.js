// TODO IMPORTACIONES
import Messages from "../../../controllers/messages.js";

// TODO VARIABLES
const D = document;
const W = window;

// *  TEMPLATES
const $templateReportList = D.querySelector("#template__report-list").content.cloneNode(true);

const REPORTS = [];

// ? INSTANCIAS
const _Messages = new Messages();

// TODO FUNCIONES

// ? GET STUDENT'S NAMES ==> ADD REPORT
async function getReservations() {
  const formGetReservations = new FormData();
  formGetReservations.set("request", "studentsReservation");
  const DATA = { method: "POST", body: formGetReservations };

  const jsonGetReservations = await fetch("../helpers/get_reports_info.php", DATA);
  const resultGetReservations = await jsonGetReservations.json();

  resultGetReservations.forEach((student) => {
    const option = D.createElement("option");

    option.textContent = student.fullnames;
    option.value = student.mail_s;

    D.querySelector("#select_student").appendChild(option);
  });
}

// ? ADD NEW REPORT
async function addNewReport(form) {
  const formNewReport = new FormData(form);
  formNewReport.set("request", "addNewReport");
  const DATA = { method: "POST", body: formNewReport };

  const jsonNewReport = await fetch("../helpers/get_reports_info.php", DATA);
  const resultNewReport = await jsonNewReport.json();

  console.log(resultNewReport);
}

// ? GET REPORT LIST
async function getReportList() {
  const formReportList = new FormData();
  formReportList.set("request", "getReportList");
  const DATA = { method: "POST", body: formReportList };

  const jsonReportList = await fetch("../helpers/get_reports_info.php", DATA);
  const resultReportList = await jsonReportList.json();

  console.log(resultReportList);
  // REPORTS.push(resultReportList);
  if (resultReportList === "0") {
    D.querySelector("#report_list").classList.add("justify-content-center");

    D.querySelector("#report_list").innerHTML = `
      <h4 class="text-center empty-reports">You don't have Reports Available</h4><br>
      <p class="text-center">Add New Report</p>
    `;
  } else {
    D.querySelector("#report_list").classList.remove("justify-content-center");
    const $parentReportList = D.querySelector("#report_list");
    const $fragment = D.createDocumentFragment();

    resultReportList.reportList.forEach((report) => {
      const $clone = D.importNode($templateReportList, true);
      $clone.querySelector(".report").dataset.report = report.id_r;
      $clone.querySelector(".report").dataset.notification = report.id_not;
      $clone.querySelector("img").src = `../${report.photo_list}`;
      $clone.querySelector("#name_issuer").textContent = report.name_list;
      $clone.querySelector("#title_report").textContent = report.title;
      if (report.status === "Active") $clone.querySelector(".status__report").classList.add("active");
      else $clone.querySelector(".status__report").classList.add("closed");
      $clone.querySelector(".status__report").textContent = report.status;
      if (report.view === "0") {
        $clone.querySelector(".bubble").classList.remove("d-none");
        $clone.querySelector(".report").classList.add("unread");
      } else {
        $clone.querySelector(".bubble").classList.add("d-none");
        $clone.querySelector(".report").classList.remove("unread");
      }

      $fragment.appendChild($clone);
    });

    $parentReportList.appendChild($fragment);
  }
}

// ? MARK READ NOTIFICATION
async function markReadMessage(report) {
  // e.target.classList.add("active");
  const formMarkRead = new FormData();
  formMarkRead.set("id_r", report.dataset.report);
  formMarkRead.set("id_not", report.dataset.notification);
  formMarkRead.set("request", "markReadMessage");
  const DATA = { method: "POST", body: formMarkRead };

  const jsonMarkRead = await fetch("../helpers/get_reports_info.php", DATA);
  const resultMarkRead = await jsonMarkRead.json();

  if (resultMarkRead === "viewed") {
    report.classList.remove("unread");
    report.querySelector(".bubble").classList.add("d-none");
  }
}

/* @--> AddNewReport() */
/* @--> SelectReport() */
/* @--> MarkReadReport() */
/* @--> AttachFile() */
/* @--> SendReport() */
/* @--> OPTIONAL -> MarkMessageReport() */
/* @--> CloseReport() */

// TODO EVENTOS
D.addEventListener("DOMContentLoaded", (e) => {
  _Messages.quitMessage();

  // ? CALLED FUNCTIONS
  getReservations();
  getReportList();
});

D.addEventListener("click", (e) => {
  // ? ADD NEW REPORT
  if (e.target.matches("#send_report")) addNewReport(e.target.parentElement.parentElement.parentElement);
  if (e.target.matches(".attach_new_image")) D.querySelector("#container_image").classList.add("show");
  if (e.target.matches(".close_image")) {
    e.target.parentElement.querySelector("img").src = "../assets/emptys/frontage-empty.png";
    D.querySelector("#attach_image_message").value = "";
    D.querySelector("#container_image").classList.remove("show");
  }

  if (e.target.matches(".report")) markReadMessage(e.target);
});
