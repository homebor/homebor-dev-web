<?php
require '../xeon.php';
session_start();

// TODO DATE
date_default_timezone_set("America/Toronto");
$date = date('Y-m-d');

// TODO USER VARIABLE
$usuario = $_SESSION['username'];

// TODO VARIABLE TYPE USER
$typeUser = $_POST['type-user'];

if ($typeUser == 'agent') {
  $agentQuery = $link->query("SELECT id_m FROM agents WHERE a_mail = '$usuario'");
  $row_agents = $agentQuery->fetch_assoc();

  $eventsQuery = $link->query("SELECT * FROM events WHERE id_m = '$row_agents[id_m]'");
  $num_event = mysqli_num_rows($eventsQuery);
}
if ($typeUser == 'homestay') {
  $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$usuario'");
  $num_event = mysqli_num_rows($eventsQuery);
}
if ($typeUser == 'student') {
  $eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$usuario'");
  $num_event = mysqli_num_rows($eventsQuery);
}


$response = null;
if ($num_event > 0) {
  while ($row_events = mysqli_fetch_array($eventsQuery)) {
    if ($row_events['end'] < $date) {

      $eventStudentQuery = $link->query("SELECT * FROM events WHERE mail_s = '$row_events[mail_s]' ORDER BY id DESC");
      $row_eventsStudent = $eventStudentQuery->fetch_assoc();

      if($row_eventsStudent['status'] !== "Active"){
        if ($row_events['mail_s'] != 'NULL') {
            $up_stu = "UPDATE pe_student SET status= 'Finished Stay' WHERE mail_s = '$row_events[mail_s]' ";
            $up_s = mysqli_query($link, $up_stu);        
        }

        if ($row_events['end'] < $date && $row_events['status'] == 'Active') {

          $homestayQuery = $link->query("SELECT * FROM pe_home WHERE mail_h = '$row_events[email]'");
          $row_homestay = $homestayQuery->fetch_assoc();

          $houseRoomQuery = $link->query("SELECT * FROM room INNER JOIN pe_home ON pe_home.mail_h = '$row_events[email]' and room.id_home = pe_home.id_home");
          $row_room = $houseRoomQuery->fetch_assoc();

          if ($row_events['color'] == '#232159')  $room = '1';
          if ($row_events['color'] == '#982A72')  $room = '2';
          if ($row_events['color'] == '#394893')  $room = '3';
          if ($row_events['color'] == '#A54483')  $room = '4';
          if ($row_events['color'] == '#5D418D')  $room = '5';
          if ($row_events['color'] == '#392B84')  $room = '6';
          if ($row_events['color'] == '#B15391')  $room = '7';
          if ($row_events['color'] == '#4F177D')  $room = '8';

          if ($row_events['bed'] == 'A') $availability = 'date' . $room;
          else if ($row_events['bed'] == 'B') $availability = 'date' . $room . '_2';
          else if ($row_events['bed'] == 'C') $availability = 'date' . $room . '_3';
          $reservation = 'reservations' . $room;
          if ($row_room['reservations' . $room] == '0') $num_reserve = 0;
          else $num_reserve = $row_room['reservations' . $room] - 1;


          $updateReservation = $link->query("UPDATE room INNER JOIN pe_home ON pe_home.id_home=room.id_home AND pe_home.mail_h='$row_events[email]' SET $reservation = '$num_reserve'");

          if ($num_reserve == '0')
            $updateStatus = $link->query("UPDATE room SET $availability= 'Available' WHERE id_home = '$row_homestay[id_home]'");
          else
            $updateStatus = $link->query("UPDATE room SET $availability= 'Occupied' WHERE id_home = '$row_homestay[id_home]'");
        }

        $updateEvent = $link->query("UPDATE events SET status = 'Disabled' WHERE id = '$row_events[id]'");
        
        if ($updateEvent) $response = "saved";
        else $response = "error";
      }
    }
  }
}
echo json_encode($response);