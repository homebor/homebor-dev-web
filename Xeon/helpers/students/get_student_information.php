<?php 

// ! IF THIS FILE NO EXIST THE FRONT NOT WORKING
require '../../xeon.php';

// ! SESSION VARIABLES
session_start();
$usuario = $_SESSION['username'];

// ! VARIABLES

// ? POST VARIABLES
$student_id = $_POST["student_id"];

// ? QUERIES

$studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$student_id'");
$rowStudent = $studentQuery->fetch_assoc();

$notificationQuery = $link->query("SELECT * FROM notification WHERE user_r = '$usuario' AND user_i_mail = '$rowStudent[mail_s]' AND title = 'Reservation Request' AND confirmed = '0' OR user_r = '$usuario' AND user_i_mail = '$rowStudent[mail_s]' AND title = 'Reservation Provider Request' AND confirmed = '0' ORDER BY id_not DESC limit 1");
$rowNotification = $notificationQuery->fetch_assoc();

$eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$rowStudent[mail_s]' AND status = 'Active' ORDER BY id DESC limit 1");
$rowEvent = $eventsQuery->fetch_assoc();

// ! FUNCTIONS

//* DATE FORMAT
function dateFormat($dateFormat, $format){
  $dateCreated = date_create($dateFormat);
  $date = date_format($dateCreated, $format);
  return $date;
}

//* CALC WEEK
function calcWeeks($startDate, $endDate){
  $start = new DateTime($startDate);
  $end = new DateTime($endDate);
  $interval = $start->diff($end);
  
  if ($interval->format("%a") % 7 == '0') $total = floor(($interval->format("%a") / 7)) . " weeks";
  else if (floor(($interval->format("%a") / 7)) / 7 == '0') $total = floor(($interval->format("%a") % 7) ." days");
  else $total = floor(($interval->format("%a") / 7)) . " weeks and " . ($interval->format("%a") % 7) . " days";

  return $total;
}

// ! CONDITIONALS

// ? STUDENT
// * STUDENT AGE
if($rowStudent){
  $from = new DateTime($rowStudent['db_s']);
  $to   = new DateTime('today');
  $age= $from->diff($to)->y; //Devuelve la diferencia entre los DateTime
  $sufix= " Years old";
  $rowStudent['student_age'] = $age . $sufix;
}

// * DATE TRANSFORM

// ? STUDENT INFORMATION
if($rowStudent){
  if($rowStudent['departure_f'] !== "NULL")
    $rowStudent['flight_date'] = dateFormat($rowStudent['departure_f'], "m/d/Y g:i A");
  else $rowStudent['flight_date'] = "Empty";
  
  if($rowStudent['arrive_f'] !== "NULL")
    $rowStudent['arrival_homestay'] = dateFormat($rowStudent['arrive_f'], "jS F Y");
  else $rowStudent['arrival_homestay'] = "Empty";
  
  if($rowStudent['arrive_g'] !== "NULL")
    $rowStudent['arrival_general'] = dateFormat($rowStudent['arrive_g'], "j F Y");
  else $rowStudent['arrival_general'] = "Empty";
  
  if($rowStudent['db_s'] !== "NULL")
    $rowStudent['dateBirth'] = dateFormat($rowStudent['db_s'], "j F Y");
  else $rowStudent['dateBirth'] = "Empty";
  
  if($rowStudent['exp_pass'] !== "NULL")
    $rowStudent['passExpiration'] = dateFormat($rowStudent['exp_pass'], "j F Y");
  else $rowStudent['passExpiration'] = "Empty";
  
  if($rowStudent['db_visa'] !== "NULL")
    $rowStudent['visaExpiration'] = dateFormat($rowStudent['db_visa'], "j F Y");
  else $rowStudent['visaExpiration'] = "Empty";
  
  if($rowStudent['firstd'] !== "NULL")
    $rowStudent['startDate'] = dateFormat($rowStudent['firstd'], "j F Y");
  else $rowStudent['startDate'] = "Empty";
  
  if($rowStudent['lastd'] !== "NULL")
    $rowStudent['endDate'] = dateFormat($rowStudent['lastd'], "j F Y");
  else $rowStudent['endDate'] = "Empty";
  
  if($rowStudent['firstd'] !== "NULL" && $rowStudent['lastd'] !== "NULL")
    $rowStudent['timeStudentReservation'] = calcWeeks($rowStudent['firstd'], $rowStudent['lastd']);
  else $rowStudent['timeStudentReservation'] = "Empty";
}


// ? NOTIFICATION
if($rowNotification){
  if($rowNotification['start'] !== "NULL")
    $rowNotification['start_date'] = dateFormat($rowNotification['start'], "jS F Y");
  else $rowStudent['start_date'] = "Empty";
  if($rowNotification['end_'] !== "NULL")
    $rowNotification['end_date'] = dateFormat($rowNotification['end_'], "jS F Y");
  else $rowStudent['end_date'] = "Empty";
  if($rowNotification['start'] !== "NULL" && $rowNotification['end_'] !== "NULL")
    $rowNotification['time_reservation'] = calcWeeks($rowNotification['start'], $rowNotification['end_']);
  else $rowStudent['time_reservation'] = "Empty";
}

// ? EVENT
if($rowEvent){
  if($rowNotification['start'] !== "NULL")
    $rowEvent['start_date'] = dateFormat($rowEvent['start'], "jS F Y");
  else $rowStudent['start_date'] = "Empty";

  if($rowNotification['end'] !== "NULL")
    $rowEvent['end_date'] = dateFormat($rowNotification['end'], "jS F Y");
  else $rowStudent['end_date'] = "Empty";

  if($rowNotification['start'] !== "NULL" && $rowNotification['end_'] !== "NULL")
    $rowEvent['time_reservation'] = calcWeeks($rowEvent['start'], $rowEvent['end_']);
  else $rowStudent['time_reservation'] = "Empty";
}


// ! JSON SENT

$jsonStudent = array(
  'studentDetails' => $rowStudent,
  'notificationDetails' => $rowNotification,
  'eventDetails' => $rowEvent
);


echo json_encode($jsonStudent);

?>