<?php 
require '../../xeon.php';
session_start();

if(!$_SESSION['username']) echo "No active session";

$usuario = $_SESSION['username'];

$id = $_POST['student_id'];

date_default_timezone_set("America/Toronto");
$date = date('Y-m-d');

// function queries ($link, $search, $table, $comparative, $equalTo, $value){
//   $query = $link->query("SELECT $search FROM $table WHERE $comparative $equalTo $value");
//   $row_query = $query->fetch_assoc();
//   return $row_query;
// }

$studentQuery = $link->query("SELECT * FROM pe_student WHERE id_student = '$id'");
$row_student = $studentQuery->fetch_assoc();

// ! FUNCTIONS

//* DATE FORMAT
function dateFormat($dateFormat, $format){
  $dateCreated = date_create($dateFormat);
  $date = date_format($dateCreated, $format);
  return $date;
}

//* CALC WEEK
function calcWeeks($startDate, $endDate){
  $start = new DateTime($startDate);
  $end = new DateTime($endDate);
  $interval = $start->diff($end);
  
  if ($interval->format("%a") % 7 == '0') $total = floor(($interval->format("%a") / 7)) . " weeks";
  else if (floor(($interval->format("%a") / 7)) / 7 == '0') $total = floor(($interval->format("%a") % 7) ." days");
  else $total = floor(($interval->format("%a") / 7)) . " weeks and " . ($interval->format("%a") % 7) . " days";

  return $total;
}

function roomSelected($colorRoom){
  if($colorRoom === '#232159') return "Room 1";
  else if($colorRoom === '#982A72') return "Room 2";
  else if($colorRoom === '#394893') return "Room 3";
  else if($colorRoom === '#A54483') return "Room 4";
  else if($colorRoom === '#5D418D') return "Room 5";
  else if($colorRoom === '#392B84') return "Room 6";
  else if($colorRoom === '#B15391') return "Room 7";
  else if($colorRoom === '#4F177D') return "Room 8";
}

function getStudentInformation ($link, $id, $row_student){
  
  if($row_student){
    if($row_student['departure_f'] !== "NULL")
      $row_student['flight_date'] = dateFormat($row_student['departure_f'], "m/d/Y g:i A");
    else $row_student['flight_date'] = "Empty";
    
    if($row_student['arrive_f'] !== "NULL")
      $row_student['arrival_homestay'] = dateFormat($row_student['arrive_f'], "jS F Y");
    else $row_student['arrival_homestay'] = "Empty";
    
    if($row_student['arrive_g'] !== "NULL")
      $row_student['arrival_general'] = dateFormat($row_student['arrive_g'], "j F Y");
    else $row_student['arrival_general'] = "Empty";
    
    if($row_student['db_s'] !== "NULL")
      $row_student['dateBirth'] = dateFormat($row_student['db_s'], "j F Y");
    else $row_student['dateBirth'] = "Empty";
    
    if($row_student['exp_pass'] !== "NULL")
      $row_student['passExpiration'] = dateFormat($row_student['exp_pass'], "j F Y");
    else $row_student['passExpiration'] = "Empty";
    
    if($row_student['db_visa'] !== "NULL")
      $row_student['visaExpiration'] = dateFormat($row_student['db_visa'], "j F Y");
    else $row_student['visaExpiration'] = "Empty";
    
    if($row_student['firstd'] !== "NULL")
      $row_student['startDate'] = dateFormat($row_student['firstd'], "j F Y");
    else $row_student['startDate'] = "Empty";
    
    if($row_student['lastd'] !== "NULL")
      $row_student['endDate'] = dateFormat($row_student['lastd'], "j F Y");
    else $row_student['endDate'] = "Empty";
    
    if($row_student['firstd'] !== "NULL" && $row_student['lastd'] !== "NULL")
      $row_student['timeStudentReservation'] = calcWeeks($row_student['firstd'], $row_student['lastd']);
    else $row_student['timeStudentReservation'] = "Empty";

    if($row_student['n_a'] !== "NULL"){
      $academyQuery = $link->query("SELECT * FROM academy WHERE id_ac = '$row_student[n_a]'");
      $row_academy = $academyQuery->fetch_assoc();

      if($row_academy === null || $row_academy === "NULL" || empty($row_academy)) $row_student['schoolName'] = "Empty";
      else $row_student['schoolName'] = $row_academy['name_a'] . ', ' . $row_academy['dir_a']. ', '. $row_academy["city_a"];
    }
  }

  $jsonSend = array($row_student);

  echo json_encode($jsonSend);
}

function getActiveReserve($link, $id, $row_student, $date){
  if($row_student['status'] !== 'Homestay Found') {
    echo json_encode("No Active");
    return;
  }
  
  $eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$row_student[mail_s]' AND status = 'Active' ORDER BY id DESC");
  $num_events = mysqli_num_rows($eventsQuery);

  
  if($num_events > 1){
    while ($row_events = mysqli_fetch_array($eventsQuery)) {
      if($row_events['end'] >= $date){
        $homestayQuery = $link->query("SELECT * FROM pe_home INNER JOIN room ON pe_home.mail_h = '$row_events[email]' AND pe_home.id_home = room.id_home");
        $row_homestay = $homestayQuery->fetch_assoc();

        $row_homestay['room_selected'] = roomSelected($row_events['color']);
        $number = explode(" ", $row_homestay['room_selected']);
        $row_homestay['accommodation'] = $row_homestay['type' . $number[1]];
        $row_homestay['food'] = $row_homestay['food' . $number[1]];
        $row_homestay['isChange'] = true;
      }
    };
  }else{
    $row_events = $eventsQuery->fetch_assoc();
    $homestayQuery = $link->query("SELECT * FROM pe_home INNER JOIN room ON pe_home.mail_h = '$row_events[email]' AND pe_home.id_home = room.id_home");
    $row_homestay = $homestayQuery->fetch_assoc();
    
    $row_homestay['room_selected'] = roomSelected($row_events['color']);
    $number = explode(" ", $row_homestay['room_selected']);
    $row_homestay['accommodation'] = $row_homestay['type' . $number[1]];
    $row_homestay['food'] = $row_homestay['food' . $number[1]];
    $row_homestay['isChange'] = false;
  }
  
  echo json_encode(array($row_homestay));
  // echo json_encode(array($row_homestay));
}

function nextReservation($link, $id, $row_student, $date){
  $eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$row_student[mail_s]' AND status = 'Active' ORDER BY id DESC limit 1");
  $row_events = $eventsQuery->fetch_assoc();

  $homestayQuery = $link->query("SELECT * FROM pe_home INNER JOIN room ON pe_home.mail_h = '$row_events[email]' AND pe_home.id_home = room.id_home");
  $row_homestay = $homestayQuery->fetch_assoc();

  $row_homestay['room_selected'] = roomSelected($row_events['color']);
  $number = explode(" ", $row_homestay['room_selected']);
  $row_homestay['accommodation'] = $row_homestay['type' . $number[1]];
  $row_homestay['food'] = $row_homestay['food' . $number[1]];

  echo json_encode(array($row_homestay));
}

function lastReservation($link, $id, $row_student, $date){
  $eventsQuery = $link->query("SELECT * FROM events WHERE mail_s = '$row_student[mail_s]' AND status = 'Disabled' ORDER BY id DESC");
  $row_events = $eventsQuery->fetch_assoc();

  $homestayQuery = $link->query("SELECT * FROM pe_home INNER JOIN room ON pe_home.mail_h = '$row_events[email]' AND pe_home.id_home = room.id_home");
  $row_homestay = $homestayQuery->fetch_assoc();

  $row_homestay['room_selected'] = roomSelected($row_events['color']);
  $number = explode(" ", $row_homestay['room_selected']);
  $row_homestay['accommodation'] = $row_homestay['type' . $number[1]];
  $row_homestay['food'] = $row_homestay['food' . $number[1]];
  $row_homestay['statusStudent'] = $row_student['status'];

  echo json_encode(array($row_homestay));

}

function getHousesAssigned($link, $id, $row_student){
  $studentQuery = $link->query("SELECT house1, house2, house3, house4, house5 FROM pe_student WHERE id_student = '$id'");
  $row_housesAssigned = $studentQuery->fetch_assoc();

  for ($i=1; $i <= 5; $i++) { 
    if($row_housesAssigned['house'.$i] !== 'NULL') $houses = $row_housesAssigned['house'.$i];
    else break;

    $homestayQuery = $link->query("SELECT * FROM pe_home INNER JOIN room ON pe_home.id_home = '$houses' AND pe_home.id_home = room.id_home");
    
    while($row_homestay = mysqli_fetch_array($homestayQuery)){
      if(empty($row_homestay['phome']) || $row_homestay['phome'] === 'NULL') 
        $phome = "../../assets/emptys/frontage-empty.png";
      else $phome = $row_homestay['phome'];

      $notificationQuery = $link->query("SELECT * FROM notification WHERE title = 'Reservation Request' AND confirmed = '0' AND user_i_mail = '$row_student[mail_s]' AND user_r = '$row_homestay[mail_h]' OR title = 'Reservation Provider Request' AND confirmed = '0' AND user_i_mail = '$row_student[mail_s]' AND user_r = '$row_homestay[mail_h]'");
      $num_notification = mysqli_num_rows($notificationQuery);
      $row_notification = $notificationQuery->fetch_assoc();

      if($num_notification > 0) $isNotificationExist = true;
      else $isNotificationExist = false;

      if(empty($row_notification['bedrooms']) || $row_notification['bedrooms'] === 'NULL') {
        $bedrooms = "Empty";
        $accommodation = "Empty";
        $food = "Empty";
      }else {
        $bedrooms = $row_notification['bedrooms'];
        $accommodation = $row_homestay['type' . $row_notification['bedrooms']];
        $food = $row_homestay['food' . $row_notification['bedrooms']];
      }
      if(empty($row_notification['des']) || $row_notification['des'] === 'NULL') 
        $bed = "Empty";
      else $bed = $row_notification['des'];
      
      $jsonHousesAssigned[] = array(
        'id_home' => $row_homestay['id_home'],
        'h_name' => $row_homestay['h_name'],
        'phome' => $phome,
        'roomSelected' => $bedrooms,
        'bedSelected' => $bed,
        'accommodation' => $accommodation,
        'food' => $food,
        'isNotificationExist' => $isNotificationExist
      );
    }
  }
  echo json_encode([$jsonHousesAssigned, $row_student['status']]);

}

if($_POST['request'] === "getStudentInformation") getStudentInformation($link, $id, $row_student);
if($_POST['request'] === "getActiveReserve") getActiveReserve($link, $id, $row_student, $date);
if($_POST['request'] === "nextReservation") nextReservation($link, $id, $row_student, $date);
if($_POST['request'] === "lastReservation") lastReservation($link, $id, $row_student, $date);
if($_POST['request'] === "getHousesAssigned") getHousesAssigned($link, $id, $row_student);


?>