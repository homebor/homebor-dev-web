<?php 

include '../xeon.php';

session_start();
$usuario = $_SESSION['username'];

// TODO DATE
date_default_timezone_set("America/Toronto");
$date = date('Y-m-d H:i:s');

// ? CALL FUNCTIONS
if($_POST['request'] === 'studentsReservation') getStudentsReservation($link, $usuario);
if($_POST['request'] === 'addNewReport') addNewReport($link, $usuario, $date);
if($_POST['request'] === 'getReportList') getReportList($link, $usuario, $date);
if($_POST['request'] === 'markReadMessage') markReadMessage($link, $usuario, $date);

// ? FUNCTION TO DO QUERIES
function queries($link, $search, $table, $column, $indi, $value, $typeQuery){
  $query = $link->query("SELECT $search FROM $table WHERE $column $indi '$value'");

  if($typeQuery === 'only-query') return $query;
  else if ($typeQuery === 'fetch') return $row_query = $query->fetch_assoc();
}

// ? FUNCTION GET TYPE USER
function typeUser($link, $usuario){
  $usersQuery = $link->query("SELECT * FROM users WHERE mail = '$usuario'");
  $row_users = $usersQuery->fetch_assoc();

  if ($row_users['usert'] == 'agent') {
    $agentQuery = $link->query("SELECT id_m, a_mail FROM agents WHERE a_mail = '$usuario'");
    $row_agents = $agentQuery->fetch_assoc();

    $managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_agents[id_m]'");
    $row_manager = $managerQuery->fetch_assoc();

    $row_user['mailCreate'] = $row_agents['a_mail'];
    $row_user['mail'] = $row_manager['mail'];
    $row_user['f_name'] = $row_agents['name'];
    $row_user['l_name'] = $row_agents['l_name'];
    $row_user['id_m'] = $row_manager['id_m'];
    $row_user['photo'] = $row_manager['photo'];
    $row_user['usert'] = $row_users['usert'];
    $row_user['general_name'] = $row_manager['a_name'];
    
    // $eventsQuery = $link->query("SELECT * FROM events WHERE id_m = '$row_agents[id_m]'");
    // $num_event = mysqli_num_rows($eventsQuery);
  }
  else if ($row_users['usert'] == 'Manager') {
    $managerQuery = $link->query("SELECT * FROM manager WHERE mail = '$usuario'");
    $row_manager = $managerQuery->fetch_assoc();
    
    if(strpos($row_manager['a_name'], " ")){
      $row_user['f_name'] = strstr($row_manager['a_name'], ' ', true);
      $row_user['l_name'] = strstr($row_manager['a_name'], ' ');
    }else{
      $row_user['f_name'] = $row_manager['a_name'];
      $row_user['l_name'] = "";
    }
    
    $row_user['mailCreate'] = $row_manager['mail'];
    $row_user['mail'] = $row_manager['mail'];
    $row_user['id_m'] = $row_manager['id_m'];
    $row_user['photo'] = $row_manager['photo'];
    $row_user['usert'] = $row_users['usert'];
    $row_user['general_name'] = $row_manager['a_name'];
    
    // $eventsQuery = $link->query("SELECT * FROM events WHERE id_m = '$row_agents[id_m]'");
    // $num_event = mysqli_num_rows($eventsQuery);
  }
  else if ($row_users['usert'] == 'homestay') {
    $homestayQuery = $link->query("SELECT * FROM pe_home WHERE mail_h = '$usuario'");
    $row_homestay = $homestayQuery->fetch_assoc();
    
    $row_user['f_name'] = $row_homestay['name_h'];
    $row_user['l_name'] = $row_homestay['l_name_h'];
    $row_user['mail'] = $row_homestay['mail_h'];
    $row_user['mailCreate'] = $row_homestay['mail_h'];
    $row_user['id_m'] = $row_homestay['id_m'];
    $row_user['usert'] = $row_users['usert'];
    $row_user['photo'] = $row_homestay['phome'];
    $row_user['general_name'] = $row_homestay['h_name'];
  }
  else if ($row_users['usert'] == 'student') {
    $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$usuario'");
    $row_student = $studentQuery->fetch_assoc();
    
    $row_user['f_name'] = $row_student['name_s'];
    $row_user['l_name'] = $row_student['l_name_s'];
    $row_user['mail'] = $row_student['mail_s'];
    $row_user['id_m'] = $row_student['id_m'];
    $row_user['usert'] = $row_users['usert'];
    $row_user['photo'] = $row_homestay['photo_s'];
    $row_user['usert'] = $row_users['usert'];
  }
  
  return $row_user;

}

// ? FUNCTION GET STUDENT TO REPORT
function getStudentsReservation($link, $usuario){
  $eventsQuery = $link->query("SELECT * FROM events WHERE email = '$usuario' AND mail_s != 'NULL' AND status = 'Active'");
  $num_event = mysqli_num_rows($eventsQuery);

  $response = null;
  if($num_event > 0){
    while ($row_events = mysqli_fetch_array($eventsQuery)){
      $studentQuery = $link->query("SELECT * FROM pe_student WHERE mail_s = '$row_events[mail_s]'");
      $row_student = $studentQuery->fetch_assoc();

      $notificationQuery = $link->query("SELECT * FROM notification WHERE report_s = '$row_student[mail_s]' AND status = 'Active'");
      $num_notification = mysqli_num_rows($notificationQuery);

      if($num_notification === 0){
        $response[] = array(
          'fullnames' => $row_student['name_s']. ' ' .$row_student['l_name_s'],
          'mail_s' => $row_student['mail_s'],
        );
      }
    }
  }

  echo json_encode($response);
}

// ? FUNCTION ADD NEW REPORT
function addNewReport($link, $usuario, $date){
  /* TABLAS A TOCAR 
    => NOTIFICATION
    => WEBMASTER
    => REPORTS
    => (OPTIONAL) NOTI_STUDENT
  */
  
  // TODO VARIABLES
  $mail_report = $_POST['mail_report'];
  
  // TODO QUERYS
  $userQuery = $link->query("SELECT usert FROM users WHERE mail = '$mail_report'");
  $row_users = $userQuery->fetch_assoc();

  $row_issuer = typeUser($link, $usuario);
  $row_mailReport = typeUser($link, $mail_report);

  $fullnamesIssuer = $row_issuer['f_name']. ' ' .$row_issuer['l_name'];

  $managerQuery = $link->query("SELECT * FROM manager WHERE id_m = '$row_mailReport[id_m]'");
  $row_manager = $managerQuery->fetch_assoc();

  if($row_issuer['usert'] === 'homestay') {
    $row_receptor['fullnames'] = $row_manager['a_name'];
    $row_receptor['mail'] = $row_manager['mail'];
    $row_receptor['id_m'] = $row_manager['id_m'];
    $studentReport = $row_mailReport['mail'];
    $folder = "public";
  }else{
    $studentReport = 'NULL';
  }

  $reportReason = addslashes($_POST['report_reason']); 

  // TODO INSERTIONS / GET ID NOTIFICATION
  // ? ==> NOTIFICATIONS
  $notificationInsert = $link->query("INSERT INTO notification (user_i, user_i_l, user_i_mail, user_r, date_, title, report_s, status) VALUES('$row_issuer[f_name]', '$row_issuer[l_name]', '$row_issuer[mail]', '$row_receptor[mail]', '$date', '$_POST[report_type]', '$studentReport', 'Active')");

  // ? GET ID NOTIFICATION
  $notificationQuery = $link->query("SELECT id_not FROM notification WHERE id_not = Last_insert_id()");
  $row_notification = $notificationQuery->fetch_assoc();

  if(!empty($_FILES['attach_image']['name'])){
    $reportImage = $_FILES['attach_image']['name'];
    $reportImage_tmp1 = $_FILES['attach_image']['tmp_name'];
    $paths = '../'.$folder.'/'.$row_issuer['mail'].'/Reports';
    $path = $folder.'/'.$row_issuer['mail'].'/Reports';
    $paths2 = $paths.'/'.$row_notification['id_not'];
    $path2 = $path.'/'.$row_notification['id_not'];

    $reportImageUrl = $path2.'/'.$reportImage;

    if (file_exists($paths2)) move_uploaded_file($reportImage_tmp1, $paths2.'/'.$reportImage);
    else {
      mkdir('../'.$folder.'/'.$row_issuer['mail'].'/Reports', 0777);
      mkdir($paths.'/'.$row_notification['id_not'], 0777);
      move_uploaded_file($reportImage_tmp1, $paths2.'/'.$reportImage);
    }
  }else $reportImageUrl = 'NULL';

  // ? ==> REPORTS
  $reportInsert = $link->query("INSERT INTO reports (names_i, mail_i, names_r, mail_r, stu_rep, title, des, date, status, id_not, chatmail, report_img) VALUES('$fullnamesIssuer', '$row_issuer[mail]', '$row_receptor[fullnames]', '$row_receptor[mail]', '$studentReport', '$_POST[report_type]', '$reportReason', '$date', 'Active', Last_insert_id(), '$row_issuer[mailCreate]', '$reportImageUrl')");

  // ? ==> WEBMASTER
  $webmasterInsert = $link->query("INSERT INTO webmaster (user, activity, dates, edit_user, id_m, report_s, reason) VALUES('$row_issuer[mail]', 'New Report', '$date', '$row_receptor[mail]', '$row_receptor[id_m]', '$studentReport', '$_POST[report_reason]')");

  echo json_encode($row_receptor);
}

// ? FUNCTION READ NEW MESSAGE
function fieldUserReport($link, $typeUser){
  if($typeUser === 'agent' || $typeUser === 'Manager') $field = 'view_a';
  else if($typeUser === 'homestay') $field = 'view_h';
  else if($typeUser === 'student') $field = 'view_s';
  return $field;
}

// ? FUNCTION REPORT LIST
function getReportList($link, $usuario, $date){
  // * GET USER INFORMATION
  $row_receptor = typeUser($link, $usuario);
  
  // * GET FIELD VIEW
  $userField = fieldUserReport($link, $row_receptor['usert']);

  $reportsQuery = $link->query("SELECT * FROM reports WHERE mail_r = '$row_receptor[mail]' OR mail_i = '$row_receptor[mail]' GROUP BY id_not DESC");
  $num_reports = mysqli_num_rows($reportsQuery);

  if($num_reports === 0) $jsonReportsList = "0";
  else {
    while ($reports = mysqli_fetch_array($reportsQuery)) {
      $idReportNot = $reports['id_not'];

      // * QUERY NOTIFICATION
      $notificationIssuer = queries($link, "*", "notification", "id_not", "=", $idReportNot, "fetch");      
      
      if($notificationIssuer !== null){
        
        if($notificationIssuer['user_i_mail'] === $row_receptor['mail']) 
          $row_reportList = typeUser($link, $notificationIssuer['user_r']);
        else if($notificationIssuer['user_r'] === $row_receptor['mail']) 
          $row_reportList = typeUser($link, $notificationIssuer['user_i_mail']);
        
        
        $reports['photo_list'] = $row_reportList['photo'];
        $reports['name_list'] = $row_reportList['general_name'];
        $reports['view'] = $reports[$userField];

        $jsonReportsList[] = $reports;
      }
    }
  }

  $jsonReports = array(
    'reportList' => $jsonReportsList,
    'receptorInformation' => $row_receptor,
  );

  echo json_encode($jsonReports);
}

function markReadMessage($link, $usuario, $date){
  // * GET USER INFORMATION
  $row_user = typeUser($link, $usuario);

  // * GET FIELD VIEW
  $userField = fieldUserReport($link, $row_user['usert']);

  // * VERIFY IF REPORT HAS READ BY THIS USER
  $isReadNotification = queries($link, $userField, "reports", "id_not", "=", $_POST['id_not'], "only-query");
  $jsonRead = null;
  while ($row_read = mysqli_fetch_array($isReadNotification)) {
    $markReadMessage = $link->query("UPDATE reports SET $userField = '1'");

    if($markReadMessage) $jsonRead = 'viewed';
    else $jsonRead = 'error';
  }

  echo json_encode($jsonRead);
}


?>